# r3 #

## Index ##
* [Pre-Requisites](#pre-requisites)
* [Setup](#setup)
* [Development](#development)
* [Seed](#seed)
* [Deploy](#deploy)
* [Issues](#issues)
* [Testing](#testing)

## Pre-Requisites ##
* GIT: [Download](https://git-scm.com/downloads)
* Angular CLI: `sudo npm install -g @angular/cli`
* NPM: `sudo npm install -g npm`
* PHP 7.3+
* Redis: [Download](https://redis.io/download)
* Supervisord

TODO: TAG
git describe --tags

## Development ##
* `git clone git@bitbucket.org:uabshp/r3.git`

### Frontend ###
* `cd apply/ && npm install && cd ../main/ && npm install && cd ../laravel/ && npm install && cd public/ && npm install`

### Socket ###
* `cd socket-server`
* `npm install`

### Backend ###
* `cd laravel`
* `composer install --optimize-autoloader --ignore-platform-reqs`

#### Development ####
* `php artisan dusk:install`

## Seed ##
`php artisan db:seed --class=Uab\\Database\\Seeds\\DemoSeeder`

## Build ##

   * `cd apply/ && ionic build --prod`
   * `cd main/ && ionic build --prod`

### Documentation ###

    * `cd main/ && compodoc -p src/tsconfig.app.json --theme laravel --output documentation --hideGenerator --disableSourceCode`

## Deploy ##

Clone outside of main repo: `git clone git@bitbucket.org:uabshp/r3.git`

Install Dependencies: `cd apply/ && npm install && cd ../main/ && npm install`

Move build files to root directory (`cp -fr r3/apply/www/* screen.scipe.org && cp -fr r3/main/www/* app.scipeme.org`)

Create config files for apply frontend: `cp /www/virtualhosts/screen.scipe.org/assets/config.prod.json /www/virtualhosts/screen.scipe.org/assets/config.json`

Create config files for main frontend: `cp /www/virtualhosts/app.scipeme.org/assets/config.prod.json /www/virtualhosts/app.scipeme.org/assets/config.json`

Create symlink: `ln -sf /www/virtualhosts/r3-prod/laravel/public/ laravel`
* **Tip** View symlinks: `find . -type l -ls`

Run socket: `forever start forever.js`
* **Tip** View running: `forever list`

Add configuration to supervisord: `/usr/bin/supervisord -c supervisord-confs/r3-worker.ini`
* *Tip* Restart queues: `php artisan queue:restart`
* *Tip* List supervisord processes: `ps -ef | grep supervisord`
* *Tip* Kill supervisord process: `kill -s SIGTERM 6010`
* *Tip* Kill node process: `kill -9 SIGTERM 6010`
* * * * * cd /www/virtualhosts/r3/laravel && php artisan schedule:run >> /dev/null 2>&1

Add to crontab:
* `crontab -e`
* `* * * * * cd /www/virtualhosts/r3/laravel && php artisan schedule:run >> /dev/null 2>&1`
* `@reboot /usr/bin/supervisord -c /www/virtualhosts/r3/laravel/supervisord-confs/r3-worker.ini`

### Update ###

* Code and Dependencies

`cd r3 && git pull && cd apply && npm install && cd ../../screen.scipe.org/ && rm -f *.js && rm -f *.html && rm -f *.json && rm -f *.css && rm -f *.woff && rm -f *.ttf && rm -f *.svg && rm -f *.eot && rm -f *.woff2 && rm -rf assets/ && rm -rf svg/ && cp -fr ../r3/apply/www/* . && cp -f assets/config.prod.json assets/config.json && cd ../main/ && npm install && cd ../../app.scipeme.org/ && rm -f *.js && rm -f *.html && rm -f *.json && rm -f *.css && rm -f *.woff && rm -f *.ttf && rm -f *.svg && rm -f *.eot && rm -f *.woff2 && rm -rf assets/ && rm -rf svg/ && cp -fr ../r3/main/www/* . && cp -f assets/config.prod.json assets/config.json && cd ..`

* Just Code

`cd r3 && git pull && cd ../screen.scipe.org/ && rm -f *.js && rm -f *.html && rm -f *.json && rm -f *.css && rm -f *.woff && rm -f *.ttf && rm -f *.svg && rm -f *.eot && rm -f *.woff2 && rm -fr assets/ && rm -fr svg/ && cp -fr ../r3/apply/www/* . && cp -f assets/config.prod.json assets/config.json && cd ../main/ && npm install && cd ../../app.scipeme.org/ && rm -f *.js && rm -f *.html && rm -f *.json && rm -f *.css && rm -f *.woff && rm -f *.ttf && rm -f *.svg && rm -f *.eot && rm -f *.woff2 && rm -rf assets/ && rm -rf svg/ && cp -fr ../r3/main/www/* . && cp -f assets/config.prod.json assets/config.json && cd ..`

## Testing ##
`brew cask install chromedriver` && `php artisan dusk:chrome-driver --all`

### Run Single Test ###
`brew install phpunit`
`phpunit tests/Api/ArticleTests.php`
