<?php

return [
    'image' => [
        'directory' => env('UPLOAD_IMAGE_DIRECTORY'),
    ],
    'video' => [
        'directory' => env('UPLOAD_VIDEO_DIRECTORY'),
    ],
];
