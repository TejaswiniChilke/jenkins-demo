<?php

return [
    'api' => [
        'key' => env('QUALTRICS_API_KEY', null),
        'user' => env('QUALTRICS_API_USER', null),
    ],
    'scrape' => [
        'days' => env('QUALTRICS_SCRAPE_DAYS', 7),
    ],
];
