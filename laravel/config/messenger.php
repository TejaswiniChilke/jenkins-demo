<?php

return [
    'user_model'    => Uab\Http\Models\Users::class,
//    'user_model'    => Cmgmyr\Messenger\Models\Participant::class,
    'message_model' => Cmgmyr\Messenger\Models\Message::class,
    'participant_model' => Cmgmyr\Messenger\Models\Participant::class,
    'thread_model' => Cmgmyr\Messenger\Models\Thread::class,

    /**
     * Define custom database table names - without prefixes.
     */
    'messages_table' => null,
    'participants_table' => 'message_participants',
    'threads_table' => null,
];
