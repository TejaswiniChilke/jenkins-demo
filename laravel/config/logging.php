<?php
use Freshbitsweb\LaravelLogEnhancer\LogEnhancer;
use Monolog\Handler\StreamHandler;

return [
    /*
    |--------------------------------------------------------------------------
    | Default Log Channel
    |--------------------------------------------------------------------------
    |
    | This option defines the default log channel that gets used when writing
    | messages to the logs. The name specified in this option should match
    | one of the channels defined in the "channels" configuration array.
    |
    */
    'default' => env('LOG_CHANNEL', 'stack'),

    /*
    |--------------------------------------------------------------------------
    | Log Channels
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log channels for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Drivers: "single", "daily", "slack", "syslog",
    |                    "errorlog", "monolog",
    |                    "custom", "stack"
    |
    */
    'channels' => [
        'local' => [
            'driver' => 'stack',
            'tap' => [
                LogEnhancer::class
            ],
            'channels' => [
                'daily'
            ],
        ],

        'prod' => [
            'driver' => 'stack',
            'tap' => [
                LogEnhancer::class
            ],
            'channels' => [
                'daily',
                'slack'
            ],
        ],

        'single' => [
            'driver' => 'single',
            'path' => env('LOGGING_PATH', storage_path('logs/laravel.log')),
            'level' => 'debug',
        ],

        'daily' => [
            'driver' => 'daily',
            'path' => env('LOGGING_PATH', storage_path('logs/laravel.log')),
            'level' => 'debug',
            'days' => 7,
        ],

        'slack' => [
            'driver' => 'slack',
            'url' => env('SLACK_WEBHOOK_URL'),
            'username' => 'Laravel Log',
            'emoji' => ':boom:',
            'level' => 'error',
        ],

        'teamsinfo' => [
            'driver'    => 'custom',
            'via'       => \MargaTampu\LaravelTeamsLogging\LoggerChannel::class,
            'level'     => 'debug',
            'url'       => env('TEAMS_WEBHOOK'),
            'style'     => 'simple',    // Available style is 'simple' and 'card', default is 'simple'
        ],

        'stderr' => [
            'driver' => 'monolog',
            'handler' => StreamHandler::class,
            'with' => [
                'stream' => 'php://stderr',
            ],
        ],

        'syslog' => [
            'driver' => 'syslog',
            'level' => 'debug',
        ],

        'errorlog' => [
            'driver' => 'errorlog',
            'level' => 'debug',
        ],
    ],
];
