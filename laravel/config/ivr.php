<?php

return [
    'phone' => [
        'goal' => env('IVR_GOAL_NUMBER', null),
        'sct' => env('IVR_SCT_NUMBER', null),
        'tracking' => env('IVR_TRACKING_NUMBER', null),
    ],
    'survey' => [
        'goal' => env('IVR_GOAL_SURVEY', null),
        'sct' => env('IVR_SCT_SURVEY', null),
        'tracking' => env('IVR_TRACKING_SURVEY', null),
    ]
];
