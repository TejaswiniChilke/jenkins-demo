<?php

return [
    'queue' => [
        'limit' => env('QUALITY_QUEUE_LIMIT', 100),
    ],
];
