<?php

return [
    'wait' => [
        'shortest' => env('WAIT_SHORTEST', 100),
        'shorter' => env('WAIT_SHORTER', 500),
        'short' => env('WAIT_SHORT', 1000),
        'medium' => env('WAIT_MEDIUM', 3000),
        'long' => env('WAIT_LONG', 5000),
        'longer' => env('WAIT_LONGER', 8000),
        'longest' => env('WAIT_LONGEST', 130000),
    ]
];
