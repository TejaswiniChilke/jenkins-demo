<?php

return [
    'account' => [
        'sid' => env('TWILIO_ACCOUNT_SID', null),
    ],
    'auth' => [
        'token' => env('TWILIO_AUTH_TOKEN', null),
    ],
    'phone_number' => env('TWILIO_PHONE_NUMBER', null)
];
