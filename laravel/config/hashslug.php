<?php

return [
    'alphabet'      => 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789',
    'appsalt'       => 'jeremy-kireeti-venkat',
    'minSlugLength' => 7
];
