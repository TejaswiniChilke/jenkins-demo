<?php
namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Tests\Browser\Components\Alert;
use Tests\Browser\Pages\Traits\HackDuskTrait;
use Facebook\WebDriver\Exception\TimeOutException;

class BugReportPage extends NgPage {
    use HackDuskTrait;

    public function assert(Browser $browser, $fragmentSuffix = null) {
        parent::assert($browser, $fragmentSuffix);

        $browser->assertVisible($this->selectors('@instruction-text'));
    }

    /**
     * @return string
     */
    public function fragment():string {
        return '/bug-report';
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements():array {
        return [
            '@create-button' => '#create-button',
            '@instruction-text' => '#bug-report-instructions',
            '@message-text-input' => '#bug-report-message',
        ];
    }

    /**
     * @param Browser $browser
     * @param string $message
     *
     * @return void
     *
     * @throws TimeOutException
     */
    public function submitBug(Browser $browser, string $message) {
        $browser->value($this->selectors('@message-text-input'), $message);
        $browser->click($this->selectors('@create-button'));

        $browser->assertSee('Reporting...');

        $alert = new Alert();
        $alert->assert($browser, 'Thank you for submitting a bug report.');
        $alert->dismiss($browser);
    }
}
