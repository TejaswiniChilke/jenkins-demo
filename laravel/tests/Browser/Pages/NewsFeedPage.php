<?php


namespace Tests\Browser\Pages;


use Facebook\WebDriver\Exception\TimeoutException;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Traits\HackDuskTrait;
use Tests\Enums\Wait;


class NewsFeedPage extends NgPage
{
    use HackDuskTrait;

    /**
     * @return string
     */
    public function fragment(): string
    {
        return '/newsfeed';
    }


    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@my-newsfeed' => '#my-newsfeed',
            '@post-create' => '.post-create',
            '@post-msg' => '#post-create-message',
            '@post-create-btn' => '#post-create-button',
            '@post-like-btn' => '#menu-content > uab-post-page > ion-content > uab-post > div > ion-row.post-feedback.md.hydrated > ion-col.show-hand-cursor.post-like-button.md.hydrated',
            '@post-like-button' => '#menu-content > uab-newsfeed-page > ion-content > uab-newsfeed > uab-post-list > ion-list > ion-card:nth-child(2) > uab-post > div > ion-row > ion-col.show-hand-cursor.post-like-button.md.hydrated',
            '@post-content' => '#menu-content > uab-newsfeed-page > ion-content > uab-newsfeed > uab-post-list > ion-list > ion-card:nth-child(2) > uab-post > div > ion-card-content > div > uab-post-text-content',
        ];

    }

    /**
     * @param Browser $browser
     * @param string $post
     *
     * @return void
     *
     * @throws TimeOutException
     */
    public function createPost(Browser $browser, string $post)
    {
        $browser->value($this->selectors('@post-msg'), $post);
        // TODO
        $browser->waitFor($this->selectors('@post-create-btn'), Wait::$MEDIUM);
        $browser->press($this->selectors('@post-create-btn'));
        $browser->assertSee($post);
    }
}
