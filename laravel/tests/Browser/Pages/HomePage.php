<?php
namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Traits\HackDuskTrait;
use Tests\Enums\Wait;

class HomePage extends NgPage
{
    use HackDuskTrait;

    /**
     * @return string
     */
    public function fragment(): string
    {
        return '/home';
    }

    public function assert(Browser $browser, $fragmentSuffix = null) {
        $browser->waitFor($this->selectors('@main-menu'));
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements(): array
    {
        return [
            '@main-menu' => '#main-menu',
            '@newsfeed' => '#newsfeed',
            '@earned-badges-done-button' => 'uab-earned-badge-modal > ion-content > ion-card > uab-button-group > ion-row > ion-col > div > ion-button'
        ];
    }
    /**
     * @param Browser $browser
     * @return void
     *
     * @throws
     */

    public function verifyLoginBadge(Browser $browser)
    {
        $browser->waitFor($this->selectors('@earned-badges-done-button'),Wait::$MEDIUM);
        $browser->press($this->selectors('@earned-badges-done-button'));

        $homePage = new HomePage();
        $homePage->assert($browser);
    }

}
