<?php
namespace Tests\Browser\Pages;

use Tests\Browser\Pages\Traits\HackDuskTrait;

class BadgeListPage extends NgPage {
    use HackDuskTrait;

    /**
     * @return string
     */
    public function fragment() {
        return '/badge-list';
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements() {
        return [
            '@all-segment' => '#segment-all',
            '@earned-segment' => '#segment-earned',

        ];
    }
}
