<?php


namespace Tests\Browser\Pages;


use Facebook\WebDriver\Exception\TimeOutException;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Traits\HackDuskTrait;
use Tests\Enums\Wait;

class UserConfirmPage extends NgPage
{
    use HackDuskTrait;


    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url():string {
        return config('screen.url');
    }

    /**
     * @param Browser $browser
     * @param null $fragmentSuffix
     *
     * @throws TimeOutException
     */
    public function assert(Browser $browser, $fragmentSuffix = null) {
        $browser->assertSee('Confirming...');
        $browser->waitUntilMissingText('Confirming...', Wait::$LONG);
    }


    public function fragment(): string
    {
        return '/confirm-email';
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements(): array
    {
        return [
            '@accept-button' => '.accept-button',

        ];
    }
}
