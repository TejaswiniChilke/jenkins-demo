<?php


namespace Tests\Browser\Pages;

use Tests\Browser\Pages\Traits\HackDuskTrait;

class VideoManagementPage extends NgPage
{
    use HackDuskTrait;

    public function fragment(): string
    {
        return '/video-management';
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements(): array
    {
        return [

        ];
    }

}
