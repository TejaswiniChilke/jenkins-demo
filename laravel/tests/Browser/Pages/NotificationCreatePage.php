<?php


namespace Tests\Browser\Pages;


use Facebook\WebDriver\Exception\TimeoutException;
use Laravel\Dusk\Browser;
use Tests\Browser\Components\Alert;
use Tests\Browser\Pages\Traits\HackDuskTrait;
use Tests\Enums\Wait;

class NotificationCreatePage extends NgPage
{
    use HackDuskTrait;

    public function assert(Browser $browser, $fragmentSuffix = null)
    {
        parent::assert($browser, $fragmentSuffix);

        $browser->assertSee('Create Notification');

        $browser->waitFor($this->selectors('@create-button'), Wait::$MEDIUM);

    }


    /**
     * @return string
     */
    public function fragment(): string
    {
        return '/notification-create';
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements(): array
    {
        return [
            '@create-button'=> 'uab-notification-create > ion-content > ion-list > ion-row:nth-child(4) > ion-col:nth-child(2) > ion-button',
            '@to_user_label'=>'uab-notification-create > ion-content > ion-list > uab-user-select > ion-row > ion-col.textarea-label',
            '@user-input'=>'ion-searchbar div.searchbar-input-container',
            '@url-input'=>'uab-notification-create > ion-content > ion-list > ion-row:nth-child(2) > ion-col:nth-child(2) > ion-input',
            '@message-input'=>'#menu-content > uab-notification-create > ion-content > ion-list > ion-row:nth-child(3) > ion-col > ion-item > ion-input',
        ];
    }

    /**
     * @param Browser $browser
     * @param string $url
     * @param string $message
     * @return void
     * @throws TimeOutException
     */

    public function createNotification(Browser $browser, string $url, string $message)
    {
        $browser->click($this->selectors('@user-input'));
        $browser->waitFor('li.focus',Wait::$LONG);
        $browser->click($this->selectors('@to_user_label'));
        $browser->click($this->selectors('@user-input'));

        $browser->value($this->selectors('@user-input'),'a');
        $browser->waitFor('li.focus',Wait::$LONG);
        $browser->click('li:nth-child(2)');
        $browser->value($this->selectors('@url-input'), $url);
        $browser->value($this->selectors('@message-input'), $message);
        $browser->click($this->selectors('@create-button'));
        $browser->assertSee('CREATING...');
        $browser->waitUntilMissingText('CREATING..',Wait::$MEDIUM);

        $alert = new Alert();
        $alert->assert($browser, 'Notification created.');
        $alert->dismiss($browser);
    }

}
