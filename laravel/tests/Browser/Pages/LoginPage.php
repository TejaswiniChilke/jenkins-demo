<?php
namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Tests\Browser\Components\SplashScreen;
use Tests\Browser\Pages\Traits\HackDuskTrait;
use Facebook\WebDriver\Exception\TimeOutException;
use Tests\Enums\Wait;

class LoginPage extends NgPage {
    use HackDuskTrait;

    /**
     * @return string
     */
    public function fragment():string {
        return '/login';
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements():array {
        return [
            '@forgot-password-button' => '#forgot-password-button',
            '@login-button' => '#login-button',
            '@login-page' => '#login-page',
            '@password-input' => '#password',
            '@username-input' => '#username',
        ];
    }

    /**
     * @param Browser $browser
     * @param string|null $fragmentSuffix
     *
     * @throws TimeOutException
     */
    public function assert(Browser $browser, ?string $fragmentSuffix = null) {
        $browser->waitFor($this->selectors('@login-page'), Wait::$MEDIUM);
    }

    /**
     * @param $browser
     * @param string|null $username
     * @param string|null $password
     *
     * @return mixed
     *
     * @throws TimeOutException
     */
    public function fillInLogin($browser, ?string $username, ?string $password) {
        $browser->waitFor($this->selectors('@login-page'), Wait::$SHORT);

        if (!is_null($username)) {
            $browser->type('username', $username);
        }

        if (!is_null($password)) {
            $browser->type('password', $password);
        }

        $browser->press('@login-button');

        $splash = new SplashScreen();
        $splash->assert($browser);
        $splash->assertGone($browser);
//
//        $homePage = new HomePage();
//        $homePage->assert($browser);
//
        return $browser;
    }
}
