<?php
namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Traits\HackDuskTrait;
use \Facebook\WebDriver\Exception\TimeOutException;

class WordPressPage extends Page {
    use HackDuskTrait;

    /**
     * @return string
     */
    public function url() {
        return 'https://scipe.org/';
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements() {
        return [
            '@login-button' => 'a#login',
            '@username-input' => '#user_login_email',
            '@username-submit-button' => '#user_login_popup_button',
        ];
    }
}
