<?php


namespace Tests\Browser\Pages;


use Tests\Browser\Pages\Traits\HackDuskTrait;

class ArticleEditPage extends NgPage
{
    use HackDuskTrait;

    public function fragment():string
    {
        return '/article-edit';
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements() {
        return [
            '@delete-button'=> '.decline-button',
            '@delete-article'=>'#menu-content > uab-article-edit-page > ion-content > uab-article-edit > ion-list > uab-button-group > ion-row > ion-col:nth-child(1) > div > ion-button',
        ];
    }
}


