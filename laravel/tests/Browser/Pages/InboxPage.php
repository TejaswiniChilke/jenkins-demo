<?php
namespace Tests\Browser\Pages;

use Tests\Browser\Pages\Traits\HackDuskTrait;

class InboxPage extends NgPage {
    use HackDuskTrait;

    /**
     * @return string
     */
    public function fragment():string {
        return '/inbox';
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements():array {
        return [
            '@main-menu' => '#main-menu',
        ];
    }
}
