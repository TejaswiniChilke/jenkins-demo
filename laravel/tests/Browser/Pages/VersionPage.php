<?php
namespace Tests\Browser\Pages;

use Tests\Browser\Pages\Traits\HackDuskTrait;

class VersionPage extends NgPage {
    use HackDuskTrait;

    /**
     * @return string
     */
    public function fragment():string {
        return '/version';
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements():array {
        return [
            '@version' => '#app-version'
        ];
    }
}
