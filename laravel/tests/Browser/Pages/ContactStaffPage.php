<?php
namespace Tests\Browser\Pages;

use Facebook\WebDriver\Exception\TimeoutException;
use Laravel\Dusk\Browser;
use Tests\Browser\Components\Alert;
use Tests\Browser\Pages\Traits\HackDuskTrait;

class ContactStaffPage extends NgPage {
    use HackDuskTrait;

    /**
     * @return string
     */
    public function fragment():string {
        return '/contact-staff';
    }

    public function assert(Browser $browser, $fragmentSuffix = null) {
        parent::assert($browser, $fragmentSuffix);

        $browser->assertVisible($this->selectors('@instruction-text'));
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements():array {
        return [
            '@cancel-button' => '#contact-staff-cancel-button',
            '@send-button' => '#send-contact-staff-button',
            '@instruction-text' => '#contact-staff-instructions',
            '@message' => '#contact-staff-message',
        ];
    }

    /**
     * @param Browser $browser
     * @param string $message
     *
     * @return void
     *
     * @throws TimeOutException
     */
    public function sendMessage(Browser $browser, string $message) {
        $browser->value($this->selectors('@message-text-input'), $message);
        $browser->click($this->selectors('@create-button'));

        $browser->assertSee('Sending...');
        $browser->assertDontSee('Sending...');

        $alert = new Alert();
        $alert->assert($browser, 'Your message has been sent.');
        $alert->dismiss($browser);
    }
}
