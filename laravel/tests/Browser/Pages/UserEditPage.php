<?php
namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Traits\HackDuskTrait;

class UserEditPage extends NgPage {
    use HackDuskTrait;

    public function assert(Browser $browser, $fragmentSuffix = null) {
        parent::assert($browser, $fragmentSuffix);

        $browser->waitFor($this->selectors('@deactivate-button'));
    }

    /**
     * @return string
     */
    public function fragment():string {
        return '/user-edit';
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements():array {
        return [
            '@cancel-button' => '#cancel-button',
            '@edit-button' => '#edit-button',
            '@email-input' => '#email-input',
            '@delete-button' => '#delete-button',
            '@mobile-number-input' => '#mobile-number-input',
            '@password-input' => '#password-input',
            '@password-confirm-input' => '#password-confirm-input',
            '@security-answer-question' => '#security-answer-question',
            '@security-answer-input' => '#security-answer-input',
            '@user-type-select' => '#user-type-select',
            '@deactivate-button' =>'#menu-content > uab-user-edit-page > ion-content > ion-list > ion-card > uab-user-edit > ion-list > uab-button-group:nth-child(12) > ion-row > ion-col > div > ion-button',
        ];
    }
}
