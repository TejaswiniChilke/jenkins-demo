<?php


namespace Tests\Browser\Pages;


use Tests\Browser\Pages\Traits\HackDuskTrait;

class SetPasswordPage extends NgPage
{
    use HackDuskTrait;

    /**
     * @return string
     */
    public function fragment():string {
        return '/set-password';
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements(): array
    {
        return [
            '@email-input' => '#email-input',
            '@mobile-number-input' => '#mobile-number-input',
            '@password-input' => '#password-input',
            '@password-confirm-input' => '#password-confirm-input',
            '@password-warning-message' => '#password-warning-message',
            '@password-error-message' => '#password-error-message',
            '@save-button' => '#save-button',
            '@security-question-select' => '#security-question-select ion-searchbar',
            '@security-answer-input' => '#security-answer-input',
        ];
    }


}
