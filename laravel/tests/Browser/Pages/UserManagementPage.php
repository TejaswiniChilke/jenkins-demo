<?php
namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Traits\HackDuskTrait;
use Tests\Enums\Wait;

class UserManagementPage extends NgPage {
    use HackDuskTrait;

    public function assert(Browser $browser, $fragmentSuffix = null)
    {
        parent::assert($browser, $fragmentSuffix);

        $browser->waitFor($this->selectors('@daysInStatus-first-row'),Wait::$MEDIUM);
    }
    /**
     * @return string
     */
    public function fragment():string {
        return '/user-management';
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements():array {
        return [
            '@email' => '#applicants-data-table > div > datatable-header > div > div.datatable-row-center > datatable-header-cell:nth-child(1) > div',
            '@daysInStatus-first-row'=>'datatable-row-wrapper:nth-child(1) > datatable-body-row > div.datatable-row-center.datatable-row-group > datatable-body-cell:nth-child(5) > div > div > div > div',

        ];
    }
}
