<?php
namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Traits\HackDuskTrait;
use \Facebook\WebDriver\Exception\TimeOutException;
use Tests\Enums\Wait;

class ForgotPasswordPage extends NgPage {
    use HackDuskTrait;

    /**
     * @return string
     */
    public function fragment():string {
        return '/forgot-password';
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements():array {
        return [
            '@password-input' => '#password-input',
            '@password-confirm-input' => '#password-confirm-input',
            '@reset-password-cancel-button' => '#reset-password-cancel-button',
            '@reset-password-submit-button' => '#reset-password-submit-button',
            '@security-answer-input' => '#security-answer-input',
            '@security-question-text' => '#security-question-text',
            //'@username-input' => '#username-input',
            '@username-cancel-button' => '#username-cancel-button',
            '@username-submit-button' => '#username-submit-button',
            '@username-input'=>'uab-input input.native-input',
        ];
    }

    /**
     * @param Browser $browser
     * @param string $username
     *
     * @throws TimeOutException
     */
    public function enterUsername(Browser $browser, string $username) {
        $this->assert($browser);
        $browser->waitFor($this->selectors('@username-input'), Wait::$MEDIUM);

        $browser->value($this->selectors('@username-input'), $username);
        $browser->press($this->selectors('@username-submit-button'));
        $browser->waitForText('Loading...', 5);
        $browser->waitForText('Submit', 10);
    }

    /**
     * @param Browser $browser
     * @param string $securityQuestion
     * @param string $securityAnswer
     * @param string $newPassword
     *
     * @throws TimeOutException
     */
    public function answerSecurity(Browser $browser, string $securityQuestion, string $securityAnswer, string $newPassword) {
        $browser->waitFor($this->selectors('@security-question-text'), 10);
        $browser->assertSee($securityQuestion);

        $browser->value($this->selectors('@security-answer-input'), $securityAnswer);
        $browser->value($this->selectors('@password-input'), $newPassword);
        $browser->value($this->selectors('@password-confirm-input'), $newPassword);
        $browser->press($this->selectors('@reset-password-submit-button'));
    }
}
