<?php


namespace Tests\Browser\Pages;


use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Traits\HackDuskTrait;

class PostManagementPage extends NgPage
{
    use HackDuskTrait;


    public function assert(Browser $browser, $fragmentSuffix = null) {
        parent::assert($browser, $fragmentSuffix);

        $browser->assertSeeIn($this->selectors('@page-title'),'Reported Post Management');

    }

    public function fragment(): string
    {
        return '/reported-post-management';
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements():array {
        return [
            '@create-button' => '#create-button',
            '@instruction-text' => '#bug-report-instructions',
            '@message-text-input' => '#bug-report-message',
            '@page-title'=>'#page-title',
        ];
    }

}
