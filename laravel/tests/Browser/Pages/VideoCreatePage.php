<?php


namespace Tests\Browser\Pages;


use Laravel\Dusk\Browser;
use Tests\Browser\Components\Alert;
use Tests\Browser\Pages\Traits\HackDuskTrait;
use \Facebook\WebDriver\Exception\TimeOutException;
use Tests\Enums\Wait;

class VideoCreatePage extends NgPage
{
    use HackDuskTrait;

    public function assert(Browser $browser, $fragmentSuffix = null) {
        parent::assert($browser, $fragmentSuffix);

        $browser->waitFor($this->selectors('@url-input'));

    }

    public function fragment(): string
    {
        return '/video-create';
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements(): array
    {
        return [
            '@url-input' => 'uab-video-create > ion-list > div > ion-row > ion-col > ion-item > ion-input',
            '@continue-button' => 'uab-video-create .accept-button',
            '@video-title-input' => 'uab-video-create > ion-list > div > ion-row:nth-child(1) > ion-col > ion-item > ion-input',
            '@video-create-button' => 'uab-video-create > ion-list > div > uab-button-group > ion-row > ion-col:nth-child(2) > div > ion-button',
        ];
    }

    /**
     * @param Browser $browser
     * @param string $url
     * @param string $title
     * @return void
     *
     * @throws TimeOutException
     */
    public function createVideo(Browser $browser, string $url,string $title)
    {
        $browser->value($this->selectors('@url-input'), $url);
        $browser->press($this->selectors('@continue-button'));
        $browser->assertSee('Loading..');
        $browser->waitUntilMissingText('Loading..',Wait::$MEDIUM);

        $browser->value($this->selectors('@video-title-input'),$title);
        //TODO
        //$this->scrollTo($browser,'@video-create-button');
        $browser->press($this->selectors('@video-create-button'));
        $browser->assertSee('Creating..');

        $alert = new Alert();
        $alert->assert($browser);
        $alert->dismiss($browser);

    }

}
