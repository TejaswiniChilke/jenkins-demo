<?php


namespace Tests\Browser\Pages;


use Facebook\WebDriver\Exception\TimeOutException;
use Laravel\Dusk\Browser;
use Tests\Browser\Components\Alert;
use Tests\Browser\Components\Confirmation;
use Tests\Browser\Pages\Traits\HackDuskTrait;
use Tests\Enums\Wait;

class PlaylistEditPage extends NgPage
{
    use HackDuskTrait;

    public function assert(Browser $browser, $fragmentSuffix = null)
    {
        parent::assert($browser, $fragmentSuffix);

        $browser->waitFor($this->selectors('@details-button'), Wait::$MEDIUM);

        $browser->assertSee('Edit Playlist');
    }


    /**
     * @return string
     */
    public function fragment(): string
    {
        return '/playlist-edit';
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements(): array
    {
        return [
            '@details-button'=> '#segment-details',
            '@videos-button' => '#segment-videos',
            '@delete-button'=>'.decline-button',
        ];
    }

    /**
     * @param Browser $browser
     *
     * @return void
     *
     *
     * @throws TimeOutException
     */
    public function deletePlaylist(Browser $browser){

        $browser->click($this->selectors('@delete-button'));
        $confirmation = new Confirmation();
        $confirmation->assert($browser);
        $confirmation->accept($browser);

        $browser->assertSee('Deleting..');

        $alert = new Alert();
        $alert->assert($browser);
        $alert->dismiss($browser);
    }
}
