<?php


namespace Tests\Browser\Pages;


use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Traits\HackDuskTrait;
use Tests\Enums\Wait;

class CurrentPlaylistPage extends NgPage
{
    use HackDuskTrait;

    public function assert(Browser $browser, $fragmentSuffix = null)
    {
        parent::assert($browser, $fragmentSuffix);


        $browser->waitFor($this->selectors('@video-next-button'), Wait::$MEDIUM);

        //$browser->assertVisible($this->selectors('@video-frame'));

    }

    /**
     * @return string
     */
    public function fragment(): string
    {
        return '/current-playlist';
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements(): array
    {
        return [
            '@video-frame' => 'uab-video-page #q6q83hk',
            '@video-next-button' => 'uab-playlist-video-list > ion-row:nth-child(1) > ion-col:nth-child(2) > ion-button',
            '@video-previous-button' => 'uab-video > uab-playlist-video-list > ion-row:nth-child(1) > ion-col:nth-child(1) > ion-button',
        ];

    }

    /**
     * @param Browser $browser
     * @return void
     *
     * @throws
     */

    public function verifyVideoPlaylistForWeek6(Browser $browser)
    {
        $browser->assertSee('SET Shoulder Pain');
        $browser->press($this->selectors('@video-next-button'));

        $browser->assertSee('(1) Range of Motion Routine I_Upper Body');
        $browser->press($this->selectors('@video-next-button'));

        $browser->assertSee('(2) Range of Motion Routine II_Lower Body');
        $browser->press($this->selectors('@video-next-button'));

        $browser->assertSee('(3) Strength Routine II');
        $browser->press($this->selectors('@video-next-button'));

        $browser->assertSee('(4) Aerobic Routine I');
        $browser->press($this->selectors('@video-next-button'));

        $browser->assertSee('(5) Aerobic Routine II');
        $browser->press($this->selectors('@video-next-button'));

        $browser->assertSee('(7) Functional Strength Routine II');
        $browser->press($this->selectors('@video-next-button'));

        $browser->assertSee('(8) Cool Down');
        $browser->assertMissing($this->selectors('@video-next-button'));
    }

    /**
     * @param Browser $browser
     * @return void
     *
     * @throws
     */
    public function verifyVideoPlaylistForWeek2(Browser $browser)
    {
        $browser->assertSee('1. Participant: Range of Motion Routine I_Upper Body');
        $browser->press($this->selectors('@video-next-button'));

        $browser->assertSee('2. Instruction: Range of Motion Routine II_Lower Body');
        $browser->assertMissing($this->selectors('@video-next-button'));
    }
}
