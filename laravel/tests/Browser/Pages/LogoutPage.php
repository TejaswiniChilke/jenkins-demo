<?php
namespace Tests\Browser\Pages;

use Tests\Browser\Pages\Traits\HackDuskTrait;

class LogoutPage extends NgPage {
    use HackDuskTrait;

    /**
     * @return string
     */
    public function fragment() {
        return '/logout';
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements() {
        return [

        ];
    }
}
