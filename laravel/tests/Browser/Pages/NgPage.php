<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;


abstract class NgPage extends Page {
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url():string {
        return config('app.frontend');
    }




    /**
     * @return string
     */
    abstract public function fragment():string;

    /**
     * @return string
     */
    public function fullUrl():string {
        return $this->url() . '/#' . $this->fragment();
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param Browser $browser
     * @param string|null $fragmentSuffix
     *
     * @return void
     *
     */
    public function assert(Browser $browser, ?string $fragmentSuffix = null) {

        $browser->assertUrlIs($this->url() . '/');

        $fragment = $this->fragment();
        if (!is_null($fragmentSuffix)) {
            $fragment .= '/' . $fragmentSuffix;
        }

        $browser->assertFragmentIs($fragment);
    }
}
