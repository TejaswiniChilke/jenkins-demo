<?php

namespace Tests\Browser\Pages;

use Facebook\WebDriver\Exception\TimeOutException;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Traits\HackDuskTrait;
use Tests\Enums\Wait;

class SciApplicationPage extends NgPage
{
    use HackDuskTrait;

    public function assert(Browser $browser, $fragmentSuffix = null)
    {
        $browser->waitFor($this->selectors('@application-section'), Wait::$MEDIUM);
        $browser->waitUntilMissingText('Thank You for confirming your email.', Wait::$MEDIUM);
        $browser->assertVisible($this->selectors('@application-section'));
    }

    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url(): string
    {
        return config('screen.url');
    }

    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function fragment(): string
    {
        return '/apply';
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements(): array
    {
        return [
            '@application-section' => '#router-content > uab-apply-page > ion-content > ion-card:nth-child(4)',
            '@firstNameInput' => '#firstName',
            '@lastNameInput' => '#lastName',
            '@ageToggle-yes' => '#ageToggle > ion-item > div > ion-button:nth-child(1)',
            '@ageToggle-no' => '#ageToggle > ion-item > div > ion-button:nth-child(2)',
            '@genderToggle-male' => '#genderToggle > ion-item > div > ion-button:nth-child(1)',
            '@genderToggle-female' => '#genderToggle > ion-item > div > ion-button:nth-child(2)',
            '@sciToggle-yes' => '#sciToggle > ion-item > div > ion-button:nth-child(1)',
            '@sciToggle-no' => '#sciToggle > ion-item > div > ion-button:nth-child(2)',
            '@mobileNumberInput' => '#mobileNumber ion-input',
            '@timePreferenceInput' => '.ngx-picker input',
            'time-picker-close-button'=>'.calendar--footer > button.ngx-picker--btn.ngx-picker--btn__close',
            '@contactPreferenceToggle-phone' => '#contactPreferenceToggle > ion-item > div > ion-button:nth-child(1)',
            '@contactPreferenceToggle-email' => '#contactPreferenceToggle > ion-item > div > ion-button:nth-child(2)',
            '@addressLineOneInput' => '#addressLineOne',
            '@addressLineTwoInput' => '#addressLineTwo',
            '@cityInput' => '#city',
            '@stateInput' => '#state ion-input',
            '@zipInput' => '#zipcode ion-input',
            '@cancelButton' => '#cancelButton',
            '@applyButton' => '#applyButton',
            'success-message-card'=>'#menu-content > uab-apply-page > ion-content > ion-card:nth-child(6)',
        ];
    }

    /**
     * @param Browser $browser
     * @param string $firstName
     * @param string $lastName
     *
     * @return void
     *
     */

    public function enterFirstNameLastName(Browser $browser, string $firstName, string $lastName)
    {
        $browser->value($this->selectors('@firstNameInput'), $firstName);
        $browser->value($this->selectors('@lastNameInput'), $lastName);
    }

    /**
     * @param Browser $browser
     * @param string $mobile
     * @param string $time
     *
     * @return void
     *
     *
     * @throws TimeOutException
     */

    public function validateTimePreferenceWithTimeInput(Browser $browser,string $mobile,string $time)
    {

        $browser->press($this->selectors('@ageToggle-yes'));
        $browser->press($this->selectors('@genderToggle-male'));
        $browser->press($this->selectors('@sciToggle-yes'));
        $browser->value($this->selectors('@mobileNumberInput'), $mobile);
        $browser->type($this->selectors('@timePreferenceInput'), $time);
        $browser->waitFor($this->selectors('time-picker-close-button'));
        $browser->press($this->selectors('time-picker-close-button'));
    }

    /**
     * @param Browser $browser
     * @param string $addressLine1
     * @param string $addressLine2
     * @param string $city
     * @param string $state
     * @param string $zipcode
     * @return void
     *
     */

    public function submitApplication(Browser $browser, string $addressLine1, string $addressLine2, string $city, string $state, string $zipcode)
    {
        $browser->press($this->selectors('@contactPreferenceToggle-email'));
        $browser->value($this->selectors('@addressLineOneInput'), $addressLine1);
        $browser->value($this->selectors('@addressLineTwoInput'), $addressLine2);
        $browser->value($this->selectors('@cityInput'), $city);
        $browser->value($this->selectors('@stateInput'), $state);
        $browser->value($this->selectors('@zipInput'), $zipcode);
        $browser->assertButtonEnabled($this->selectors('@applyButton'));
        $browser->click($this->selectors('@applyButton'));
        $browser->assertSee('Applying...');
        $browser->waitUntilMissingText('Applying...',Wait::$MEDIUM);
    }

}




