<?php
namespace Tests\Browser\Pages;

use Tests\Browser\Pages\Traits\HackDuskTrait;

class ArticleManagementPage extends NgPage {
    use HackDuskTrait;

    /**
     * @return string
     */
    public function fragment():string {
        return '/article-management';
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements():array {
        return [
            '@settings-button' => '#data-table-settings',
            '@bulk-delete-button' => '#data-table-delete-button',
            '@bulk-delete-toggle' => '#data-table-delete-toggle',
        ];
    }
}
