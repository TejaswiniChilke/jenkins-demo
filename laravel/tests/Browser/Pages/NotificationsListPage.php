<?php


namespace Tests\Browser\Pages;



use Tests\Browser\Pages\Traits\HackDuskTrait;


class NotificationsListPage extends NgPage
{
    use HackDuskTrait;


    /**
     * @return string
     */
    public function fragment(): string
    {
        return '/notifications';
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements(): array
    {
        return [
            '@notification'=>'uab-notification-list-page > ion-content > uab-notification-list > ion-list > uab-long-list > ion-list > ion-row > ion-col:nth-child(1) > ion-item',
        ];
    }
}
