<?php


namespace Tests\Browser\Pages;


use Tests\Browser\Pages\Traits\HackDuskTrait;

class ApplicationManagementPage extends NgPage
{
    use HackDuskTrait;

    public function fragment(): string
    {
        return '/application-management';
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements():array {
        return [
            '@email-sort' => '#applicants-data-table > div > datatable-header > div > div.datatable-row-center > datatable-header-cell:nth-child(1) > div',
            '@accept-button' => '#accept-button',
            '@hide-button' => '#applicants-data-table > div > datatable-body > datatable-selection > datatable-scroller > datatable-row-wrapper:nth-child(1) > datatable-body-row > div.datatable-row-group.datatable-row-left > datatable-body-cell:nth-child(2) > div > ion-button',
        ];
    }
}
