<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Traits\HackDuskTrait;
use Tests\Enums\Wait;

class ApplyPage extends NgPage
{
    use HackDuskTrait;

    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url(): string
    {
        return config('screen.url');
    }

    public function assert(Browser $browser, $fragmentSuffix = null)
    {

        parent::assert($browser, $fragmentSuffix);
        $browser->waitFor($this->selectors('@welcome-card'), Wait::$MEDIUM);
        $browser->assertVisible($this->selectors('@welcome-card'));
    }


    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function fragment(): string
    {
        return '/apply';
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements(): array
    {
        return [
            '@alert' => 'ion-backdrop',
            '@alert-message' => '.alert-message',
            '@accept-button' => '#accept-button',
            '@confirm-email' => '.confirm-email-card',
            '@email-input' => '[name="email"]',
            '@resend-button' => '#resend-button',
            '@welcome-card' => '#menu-content > uab-apply-page > ion-content > ion-card:nth-child(1) > ion-card-content',
            '@terms-and-conditions' => '.terms-and-conditions',
            '@application-section' => '#menu-content > uab-apply-page > ion-content > ion-card:nth-child(4)',

        ];
    }
    /**
     * @param Browser $browser
     * @param string $email
     *
     * @return void
     *
     *
     */
    public function applyForStudy(Browser $browser, string $email)
    {
        $browser->value($this->selectors('@email-input'), $email);
        $browser->assertEnabled($this->selectors('@accept-button'));
        $browser->press($this->selectors('@accept-button'));
        $browser->assertSee('Accepting...');
    }


}
