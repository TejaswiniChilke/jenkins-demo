<?php


namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Traits\HackDuskTrait;
use Tests\Enums\Wait;

class PostPage extends NgPage
{
    use HackDuskTrait;

    public function assert(Browser $browser, $fragmentSuffix = null) {
        parent::assert($browser, $fragmentSuffix);

        $browser->waitFor($this->selectors('@post-like-btn'),Wait::$MEDIUM);

        $browser->assertSeeIn($this->selectors('@page-title'),'Post');
    }


    /**
     * @return string
     */
    public function fragment():String
    {
        return '/post';
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements():array {
        return [
            '@page-title'=>'#page-title',
            '@post-page-feedback-btn'=>'.post-feedback',
            '@post-like-btn'=>'#menu-content > uab-post-page > ion-content > uab-post > div > ion-row.post-feedback.md.hydrated > ion-col.show-hand-cursor.post-like-button.md.hydrated',
            '@post-unlike-btn'=>'#menu-content > uab-post-page > ion-content > uab-post > div > ion-row.post-feedback.md.hydrated > ion-col.show-hand-cursor.post-like-button.md.hydrated',
            '@comment-text-input'=>'.new-comment-content div textarea',
            '@comment-btn'=>'uab-button-group .accept-button',
            '@post-flag-btn'=>'#menu-content > uab-post-page > ion-content > uab-post > div > ion-row.post-feedback.md.hydrated > ion-col.show-hand-cursor.post-flag-button.md.hydrated'
            ];
    }
}
