<?php
namespace Tests\Browser\Pages;

use Tests\Browser\Pages\Traits\HackDuskTrait;

class ArticleCreatePage extends NgPage {
    use HackDuskTrait;

    /**
     * @return string
     */
    public function fragment():string {
        return '/article-create';
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements():array {
        return [
            '@cancel-button' => '#cancel-button',
            '@create-button' => '#create-button',
            '@title' => '#title',
            '@startWeek' => '#start-week',
            '@description' => '#description',
            '@content' => '#content',
            '@thumbnail' => '#thumbnail-image',
            '@infographic' => '#content-image',
        ];
    }
}
