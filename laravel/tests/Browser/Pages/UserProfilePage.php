<?php


namespace Tests\Browser\Pages;


use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Traits\HackDuskTrait;

class UserProfilePage extends NgPage
{
    use HackDuskTrait;

    public function assert(Browser $browser, $fragmentSuffix = null) {
        parent::assert($browser, $fragmentSuffix);

        $browser->waitFor($this->selectors('@post-input'),35);

        $browser->assertVisible($this->selectors('@post-input'));
    }

    /**
     * @return string
     */
    public function fragment(): string
    {
        return '/profile';
    }


    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements(): array
    {
        return [
            '@post-input'=>'.post-create',
            '@post-msg'=>'#post-create-message',
            '@post-create-btn'=>'#post-create-button',
        ];
    }
}
