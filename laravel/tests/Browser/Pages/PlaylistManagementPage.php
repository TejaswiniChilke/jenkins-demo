<?php


namespace Tests\Browser\Pages;


use Facebook\WebDriver\Exception\TimeoutException;
use Laravel\Dusk\Browser;
use Tests\Browser\Components\Confirmation;
use Tests\Browser\Pages\Traits\HackDuskTrait;
use Tests\Enums\Wait;

class PlaylistManagementPage extends NgPage
{
    use HackDuskTrait;

    public function assert(Browser $browser, $fragmentSuffix = null)
    {
        parent::assert($browser, $fragmentSuffix);

        $browser->waitFor($this->selectors('@playlist-table'), Wait::$MEDIUM);

        $browser->waitFor($this->selectors('@playlist-copy-button'),Wait::$MEDIUM);

        $browser->assertSee('Playlist Management');
    }


    /**
     * @return string
     */
    public function fragment(): string
    {
        return '/playlist-management';
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements(): array
    {
        return [
            '@playlist-table'=> '.datatable-body',
            '@datatable-week-sort'=>'datatable-header-cell:nth-child(2) > div > span.datatable-header-cell-wrapper > span',
            '@playlist-copy-button'=>'datatable-row-wrapper:nth-child(1) .data-table-copy-button',
        ];
    }

    /**
     * @param Browser $browser
     * @return void
     * @throws TimeOutException
     */

    public function copyPlaylist(Browser $browser)
    {
        $browser->click($this->selectors('@playlist-copy-button'));

        $confirmation = new Confirmation();
        $confirmation->assert($browser);
        $confirmation->accept($browser);
    }
}
