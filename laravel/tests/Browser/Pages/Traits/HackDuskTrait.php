<?php
namespace Tests\Browser\Pages\Traits;

use \ArrayAccess;
use Illuminate\Support\Arr;
use Laravel\Dusk\Browser;


trait HackDuskTrait {
    /**
     * @param Browser $browser
     */
    public function injectJquery(Browser $browser) {
        $script = '
              var newscript = document.createElement("script");
              newscript.type = "text/javascript";
              newscript.async = true;
              newscript.src = "https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js";
              (document.getElementsByTagName("head")[0]||document.getElementsByTagName("body")[0]).appendChild(newscript);
        ';

        $browser->script($script);
    }

    /**
     * @return string[]
     */
    abstract public function elements();

    /**
     * @param Browser $browser
     * @param string  $element
     */
    public function clickViaScript(Browser $browser, string $element) {
        $script = '$("'.$this->selectors($element).'").click();';



        $browser->script($script);
    }

    /**
     * @param string $key
     *
     * @return array|ArrayAccess|mixed
     */
    public function selectors(string $key) {
        $elements = $this->elements();

        return Arr::get($elements, $key, null);
    }

    /**
     * @param Browser $browser
     * @param string  $element
     */
    public function scrollTo(Browser $browser, string $element) {
        $script = '$("'.$this->selectors($element).'").scrollIntoView();';

        $browser->script($script);

    }

}
