<?php
namespace Tests\Browser\Pages;

use Tests\Browser\Pages\Traits\HackDuskTrait;

class UserCreatePage extends NgPage {
    use HackDuskTrait;

    /**
     * @return string
     */
    public function fragment():string{
        return '/user-create';
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements() {
        return [
            '@cancel-button' => '#cancel-button',
            '@create-button' => '#create-button',
            '@email-input' => '#email-input',
            '@user-type-select' => '#user-type-select',
        ];
    }
}
