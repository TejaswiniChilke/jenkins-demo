<?php
namespace Tests\Browser\Pages;

use Tests\Browser\Pages\Traits\HackDuskTrait;

class FaqPage extends NgPage {
    use HackDuskTrait;

    /**
     * @return string
     */
    public function fragment():string {
        return '/home';
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements():array {
        return [
            '@collapse-buttons' => '.collapse-faq',
            '@expand-buttons' => '.expand-faq',
            '@faq-answers' => '.faq-answer',
            '@faq-questions' => '.faq-question',
        ];
    }
}
