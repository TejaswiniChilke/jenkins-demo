<?php
namespace Tests\Browser\Components;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Component as BaseComponent;
use \Facebook\WebDriver\Exception\TimeOutException;
use Tests\Browser\Pages\Traits\HackDuskTrait;

class Header extends BaseComponent {
    use HackDuskTrait;

    /**
     * Get the root selector for the component.
     *
     * @return string
     */
    public function selector():string {
        return $this->selectors('@header');
    }

    /**
     * Assert that the browser page contains the component.
     *
     * @param Browser $browser
     *
     * @return void
     *
     * @throws TimeOutException
     */
    public function assert(Browser $browser) {
        $browser->waitFor($this->selector());
        $browser->assertVisible($this->selector());
    }

    /**
     * Get the element shortcuts for the component.
     *
     * @return array
     */
    public function elements():array {
        return [
            '@header' => 'ion-header',
            '@header-buttons' => '#header-buttons',
            '@menu' => '#header-menu-button',
            '@menu-icon' => '#header-menu-icon',
            '@menu-label' => '#header-menu-label',
            '@notifications' => '#header-notifications',
            '@notification-count' => '#header-notification-count',
            '@notification-icon' => '#header-notification-icon',
            '@notification-label' => '#header-notification-label',
            '@title' => '#page-title',
        ];
    }
}
