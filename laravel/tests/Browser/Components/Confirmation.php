<?php
namespace Tests\Browser\Components;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Component as BaseComponent;
use \Facebook\WebDriver\Exception\TimeOutException;
use Tests\Browser\Pages\Traits\HackDuskTrait;

class Confirmation extends BaseComponent {
    use HackDuskTrait;

    /**
     * Get the root selector for the component.
     *
     * @return string
     */
    public function selector() {
        return $this->selectors('@confirmation-message');
    }

    /**
     * Assert that the browser page contains the component.
     *
     * @param Browser $browser
     * @param string|null $message
     *
     * @return void
     *
     * @throws TimeOutException
     */
    public function assert(Browser $browser, ?string $message = null) {
        $browser->waitFor($this->selector());
        $browser->assertVisible($this->selector());

        if (!is_null($message)) {
            $browser->assertSee($message);
        }
    }

    /**
     * Get the element shortcuts for the component.
     *
     * @return array
     */
    public function elements():array {
        return [
            '@confirmation-accept-button' => 'ion-button#confirmation-accept-button',
            '@confirmation-decline-button' => '#confirmation-decline-button',
            '@confirmation-message' => '#confirmation-modal-message',
            '@confirmation-title' => '#confirmation-modal-header ion-title',
        ];
    }

    /**
     * @param Browser $browser
     *
     * @return void
     *
     * @throws TimeOutException
     */
    public function accept(Browser $browser) {
        //$browser->waitFor('@confirmation-accept-button');
        $browser->click($this->selectors('@confirmation-accept-button'));
    }

    /**
     * @param Browser $browser
     *
     * @return void
     *
     * @throws TimeOutException
     */
    public function decline(Browser $browser) {
        $browser->waitFor('@confirmation-decline-button');
        $browser->click($this->selectors('@confirmation-decline-button'));
    }
}
