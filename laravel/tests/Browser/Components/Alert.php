<?php
namespace Tests\Browser\Components;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Component as BaseComponent;
use \Facebook\WebDriver\Exception\TimeOutException;
use Tests\Browser\Pages\Traits\HackDuskTrait;

class Alert extends BaseComponent {
    use HackDuskTrait;

    /**
     * Get the root selector for the component.
     *
     * @return string
     */
    public function selector() {
        return $this->selectors('@alert-button');
    }

    /**
     * Assert that the browser page contains the component.
     *
     * @param Browser $browser
     * @param string|null $message
     *
     * @return void
     *
     * @throws TimeOutException
     */
    public function assert(Browser $browser, ?string $message = null) {
        $browser->waitFor($this->selectors('@alert-message'));
        $browser->assertVisible($this->selector());

        if (!is_null($message)) {
            $browser->assertSee($message);
        }
    }

    /**
     * Get the element shortcuts for the component.
     *
     * @return array
     */
    public function elements(){
        return [
            '@alert-button' => '.alert-button',
            '@alert-message' => '.alert-message',
        ];
    }

    /**
     * @param $browser
     */
    public function dismiss($browser) {
        $browser->waitFor($this->selectors('@alert-message'));
        $browser->waitFor($this->selectors('@alert-button'));
        $browser->click($this->selectors('@alert-button'));
    }
}
