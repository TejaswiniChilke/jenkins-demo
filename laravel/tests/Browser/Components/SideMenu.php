<?php
namespace Tests\Browser\Components;

use Facebook\WebDriver\Remote\RemoteWebElement;
use Facebook\WebDriver\WebDriverBy;
use Laravel\Dusk\Browser;
use Laravel\Dusk\Component as BaseComponent;
use \Facebook\WebDriver\Exception\TimeOutException;
use PHPUnit\Framework\Assert as PHPUnit;
use Tests\Browser\Pages\Traits\HackDuskTrait;

class SideMenu extends BaseComponent {
    use HackDuskTrait;

    /**
     * Get the root selector for the component.
     *
     * @return string
     */
    public function selector():string {
        return $this->selectors('@main-menu');
    }

    /**
     * Assert that the browser page contains the component.
     *
     * @param Browser $browser
     *
     * @return void
     *
     * @throws TimeOutException
     */
    public function assert(Browser $browser) {
        $browser->waitFor($this->selector());
        $browser->assertVisible($this->selector());

        $this->getHighlightedElement($browser);
    }

    /**
     * Get the element shortcuts for the component.
     *
     * @return array
     */
    public function elements():array {
        return [
            '@main-menu' => '#main-menu',
            '@menu-application'=>'#application-management',
            '@menu-button-newsfeed'=>'#newsfeed',
            '@menu-button-logout' => '.menu-button#logout div',
            '@menu-button-bug-report' => '.menu-button#bug-report div',
            '@menu-button-contact-staff' => '.menu-button#contact-staff div',
            '@menu-button-profile'=> '#profile',
            '@menu-button-user-management' => '#user-management',
            '@menu-button-article-management'=>'#article-management',
            '@menu-button-reported-post-management'=>'#reported-post-management div',
            '@menu-button-playlist'=>'#current-playlist div',
            '@menu-button-playlist-management'=>'#playlist-management div',
        ];
    }

    /**
     * @param Browser $browser
     *
     * @return void
     *
     * @throws TimeOutException
     */
    public function logout(Browser $browser) {
        $browser->waitFor($this->selectors('@main-menu'));
        $browser->click($this->selectors('@menu-button-logout'));

        $confirm = new Confirmation();
        $confirm->assert($browser, 'Are you sure you want to logout?');
        $confirm->accept($browser);
    }

    public function getHighlightedElement(Browser $browser):RemoteWebElement {
        $elements = $browser->driver->findElements(
            WebDriverBy::className('active-menu-item-color')
        );

        PHPUnit::assertCount(1, $elements);

        return $elements[0];
    }
}
