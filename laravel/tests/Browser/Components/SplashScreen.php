<?php
namespace Tests\Browser\Components;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Component as BaseComponent;
use \Facebook\WebDriver\Exception\TimeOutException;
use Tests\Browser\Pages\Traits\HackDuskTrait;
use Tests\Enums\Wait;

class SplashScreen extends BaseComponent {
    use HackDuskTrait;

    /**
     * Get the root selector for the component.
     *
     * @return string
     */
    public function selector():string {
        return $this->selectors('@splash-screen');
    }

    /**
     * Assert that the browser page contains the component.
     *
     * @param Browser $browser
     *
     * @return void
     *
     * @throws TimeOutException
     */
    public function assert(Browser $browser) {
        $browser->waitFor($this->selector(),Wait::$MEDIUM);
        $browser->assertVisible($this->selector());

        $browser->waitForText('Initializing...', Wait::$MEDIUM);
    }

    /**
     * Assert that the browser page contains the component.
     *
     * @param Browser $browser
     *
     * @return void
     *
     * @throws TimeOutException
     */
    public function assertGone(Browser $browser) {
        $browser->waitUntilMissingText('Initializing...', Wait::$MEDIUM);

        $browser->waitUntilMissing($this->selector());
        $browser->assertMissing($this->selector());
    }

    /**
     * Get the element shortcuts for the component.
     *
     * @return array
     */
    public function elements():array {
        return [
            '@splash-screen' => '#splash-screen',
        ];
    }
}
