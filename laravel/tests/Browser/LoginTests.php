<?php
namespace Tests\Browser;

use Tests\Browser\Components\Alert;
use Tests\Browser\Pages\LoginPage;
use Tests\TestCase\DuskTestCase;
use Laravel\Dusk\Browser;
use Throwable;

class LoginTests extends DuskTestCase {
    /**
     * A Dusk test example.
     *
     * @return void
     *
     * @throws Throwable
     */
    public function testLoginWithWrongPassword() {
        $this->browse(
            function(Browser $browser) {
                $loginPage = new LoginPage();
                $browser->visit($loginPage);
                $loginPage->assert($browser);
                $loginPage->fillInLogin($browser, 'jrquick', 'kireeti');

                $alert = new Alert();
                $alert->assert($browser, 'Wrong username or password');
                $alert->dismiss($browser);

                $loginPage->assert($browser);
            }
        );
    }

    /**
     * A Dusk test example.
     *
     * @return void
     *
     * @throws Throwable
     */
    public function testLoginWithWrongUsername() {
        $this->browse(
            function(Browser $browser) {
                $loginPage = new LoginPage();
                $browser->visit($loginPage);
                $loginPage->assert($browser);
                $loginPage->fillInLogin($browser, 'vwdverwvwervwev', 'admin');

                $alert = new Alert();
                $alert->assert($browser, 'Wrong username or password');
                $alert->dismiss($browser);

                $loginPage->assert($browser);
            }
        );
    }

}
