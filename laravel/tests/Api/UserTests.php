<?php
namespace Tests\Api;

use DateTime;
use Tests\TestCase\TestCase;
use Uab\Http\Enums\HttpResponseCodesEnum;
use Uab\Http\Enums\UserTypesEnum;
use Uab\Http\Models\Users;
use Uab\Http\Models\Followers;
use Uab\Providers\UsersServiceProvider;

class UserTests extends TestCase {
    public function testFullNameUser() {
        // Setup data
        $firstName = 'Dusk';
        $lastName = 'Test';

        /** @var Users $user */
        $user = Users::query()
            ->create(
                [
                    'username' => 'dusk-test-' . rand(),
                    'first_name' => $firstName,
                    'last_name' => $lastName
                ]
            );

        $userSlug = $user->slug();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get('/api/users/get/' . $userSlug, $headers);

        // Validate
        $fullName = $firstName . ' ' . $user['last_name'];

        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment(
            [
                'id' => $userSlug,
                'full_name' => $fullName
            ]
        );

        // Delete temporary data
        $user->forceDelete();
    }

    public function testUserAge() {
        // Setup data
        /** @var Users $user */
        $user = Users::query()
            ->create(
                [
                    'first_name' => 'Dusk',
                    'last_name' => 'Age',
                    'dob' => '1995-08-20'
                ]
            );

        $formattedDob = date("Y-m-d",strtotime($user['dob']));
        $from = new DateTime($formattedDob);
        $to = new DateTime();
        $age = $from->diff($to)->y;

        $slug = $user->slug();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get(
            '/api/users/get/' . $slug,
            $headers
        );

        // Delete temporary data
        $user->forceDelete();

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment(
            [
                'id' => $slug,
                'age' => $age
            ]
        );
    }

    public function testUserIsFriend() {
        // Setup data
        $activeUser = $this->getLoggedInUser();

        /** @var Users $userTwo */
        $userTwo = Users::query()
            ->create(
                [
                    'username' => 'dusk-test-' . rand(),
                    'first_name' => 'Dusk',
                    'last_name' => 'Friend2'
                ]
            );

        /** @var Followers $followerOne */
        $followerOne = Followers::query()
            ->create(
                [
                    'user_id' => $activeUser->id,
                    'following_id' => $userTwo->id
                ]
            );

        /** @var Followers $followerTwo */
        $followerTwo = Followers::query()
            ->create(
                [
                    'user_id' => $userTwo->id,
                    'following_id' => $activeUser->id
                ]
            );

        $slugOne = $userTwo->slug();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get('/api/users/get/' . $slugOne, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment(
            [
                'id' => $slugOne,
                'is_friend' => true
            ]
        );

        // Delete temporary data
        $userTwo->forceDelete();
        $followerOne->forceDelete();
        $followerTwo->forceDelete();
    }

    public function testUserIsNotFriendWithOnlyFollow() {
        // Setup data
        $activeUser = $this->getLoggedInUser();

        /** @var Users $userTwo */
        $userTwo = Users::query()
            ->create(
                [
                    'username' => 'dusk-test-' . rand(),
                    'first_name' => 'Dusk',
                    'last_name' => 'Friend2'
                ]
            );

        /** @var Followers $followerOne */
        $followerOne = Followers::query()
            ->create(
                [
                    'user_id' => $activeUser->id,
                    'following_id' => $userTwo->id
                ]
            );

        $slugOne = $userTwo->slug();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get('/api/users/get/' . $slugOne, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment(
            [
                'id' => $slugOne,
                'is_friend' => false
            ]
        );

        // Delete temporary data
        $userTwo->forceDelete();
        $followerOne->forceDelete();
    }

    public function testUserIsNotFriend() {
        // Setup data
        /** @var Users $userTwo */
        $userTwo = Users::query()
            ->create(
                [
                    'username' => 'dusk-test-' . rand(),
                    'first_name' => 'Dusk',
                    'last_name' => 'Friend2'
                ]
            );

        $userTwoSlug = $userTwo->slug();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get('/api/users/get/' . $userTwoSlug, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment(
            [
                'id' => $userTwoSlug,
                'is_friend' => false
            ]
        );

        // Delete temporary data
        $userTwo->forceDelete();
    }

    public function testUserWithNoFriends() {
        // Setup data
        $activeUser = $this->createUser();

        $slugOne = $activeUser->slug();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get(
            '/api/friends/get/' . $slugOne,
            $headers
        );

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);

        $userService = new UsersServiceProvider();
        $adminCount = $userService->getUsersByUserTypeNames([ UserTypesEnum::ADMIN ])->count();

        $response->assertJsonCount($adminCount,'friends');
    }

    public function testUserWithFriends() {
        // Setup data
        $activeUser = $this->getLoggedInUser();

        $userOne = Users::query()
            ->create(
                [
                    'username' => 'dusk-test-' . rand(),
                    'first_name' => 'Dusk',
                    'last_name' => 'Friend1'
                ]
            );

        $followOne = Followers::query()
            ->create(
                [
                    'user_id' => $activeUser->id,
                    'following_id' => $userOne->id
                ]
            );

        $followBackOne = Followers::query()
            ->create(
                [
                    'user_id' => $userOne->id,
                    'following_id' => $activeUser->id
                ]
            );

        $activeUserSlug = $activeUser->slug();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get('/api/friends/get/' . $activeUserSlug, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);

        $userService = new UsersServiceProvider();
        $adminCount = $userService->getUsersByUserTypeNames([ UserTypesEnum::ADMIN ])->count();

        $response->assertJsonCount($adminCount + 1, 'friends');

        // Delete temporary data
        $userOne->forceDelete();
        $followOne->forceDelete();
        $followBackOne->forceDelete();
    }

    public function testHashId() {
        // Setup data
        /** @var Users $userTwo */
        $userTwo = Users::query()
            ->create(
                [
                    'username' => 'dusk-test-' . rand(),
                    'first_name' => 'Dusk',
                    'last_name' => 'Friend2'
                ]
            );

        $userTwoSlug = $userTwo->slug();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get('/api/users/get/' . $userTwoSlug, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertSeeText(
            $response->decodeResponseJson()['users'][0]['id']
        );

        // Delete temporary data
        $userTwo->forceDelete();
    }

    public function testUserWithNoFriendRequests() {
        // Setup data
        /** @var Users $user */
        $user = Users::query()
            ->create(
                [
                    'username' => 'dusk-test-' . rand(),
                    'first_name' => 'Dusk',
                    'last_name' => 'Friend2'
                ]
            );

        $userSlug = $user->slug();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get('/api/friend_requests/get/' . $userSlug, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonCount(0,'friend_requests');
    }

    public function testUserWithFriendRequests() {
        // Setup data
        $activeUser = $this->getLoggedInUser();

        /** @var Users $userOne */
        $userOne = Users::query()
            ->create(
                [
                    'username' => 'dusk-test-' . rand(),
                    'first_name' => 'Dusk',
                    'last_name' => 'Friend1'
                ]
            );

        /** @var Followers $followerOne */
        $followerOne = Followers::query()
            ->create(
                [
                    'user_id' => $activeUser->id,
                    'following_id' => $userOne->id
                ]
            );

        $slugOne = $activeUser->slug();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get('/api/friends/get/' . $slugOne, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonCount(1, 'friends');

        // Delete temporary data
        $userOne->forceDelete();
        $followerOne->forceDelete();
    }

    public function testOauthSecurityQuestion() {
        $question = 'Dusk Question ' . uniqid() . '?';

        // Setup data
        $userData = [
            'first_name' => 'Dusk',
            'username' => 'Stest',
            'last_name' => 'Friend1',
            'security_question' => $question
        ];

        $user = $this->createUser($userData);

        // Build request
        $headers = $this->getHeaders();

        $userNameData = [
            'username' => 'Stest',
        ];

        // Make request
        $response = $this->post(
            '/oauth/security_questions/get/',
            $userNameData,
            $headers
        );

        // Delete temporary data
        $user->forceDelete();

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment(
            [
                'security_questions' => $question
            ]
        );
    }

    public function testUserGetById() {
        // Setup data
        /** @var Users $user */
        $user = Users::query()
            ->create(
                [
                    'username' => 'dusk-test-' . rand(),
                    'first_name' => 'Dusk',
                    'last_name' => 'Friend'
                ]
            );

        $userSlug = $user->slug();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get('/api/users/get/' . $userSlug, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment(
            [
                'id' => $userSlug
            ]
        );

        // Delete temporary data
        $user->forceDelete();
    }

    public function testUserCount() {
        //get the existing users count
        /** @var Users $usersCount */
        $usersCount = Users::query()
            ->count();

        //create user
        /** @var Users $user */
        $user = Users::query()
            ->create(
                [
                    'first_name' => 'Dusk',
                    'last_name' => 'Test-user'
                ]
            );

        //get the new count
        /** @var Users $newCount */
        $newCount= Users::query()
            ->count();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get(
            '/api/users/count/', $headers
        );

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);

        $this->assertEquals($usersCount+1, $newCount,"verified users count");

        // Delete temporary data
        $user->forceDelete();
    }

    public function testUserMatching() {
        //set up data
        $userData= [
            'first_name' => 'Dusk',
            'last_name' => 'Test-user'
        ];
        //create user
        /** @var Users $user1 */
        $userOne = Users::query()
            ->create($userData);

        /** @var Users $user2 */
        $userTwo = Users::query()
            ->create($userData);

        $userSlug = $userOne->slug();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->post('/api/users/matching/'.$userSlug,$headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
//        $response->assertJsonFragment(
//            [
//                'first_name' => 'Dusk',
//            ]
//        );

        // Delete temporary data
        $userTwo->forceDelete();
        $userOne->forceDelete();
    }

}
