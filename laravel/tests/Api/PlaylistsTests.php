<?php
namespace Tests\Api;

use Tests\TestCase\TestCase;
use Uab\Http\Enums\HttpResponseCodesEnum;
use Uab\Http\Models\Playlists;
use Uab\Http\Models\PlaylistsTags;
use Uab\Http\Models\PlaylistsVideos;
use Uab\Http\Models\Tags;
use Uab\Http\Models\Videos;

class PlaylistsTests extends TestCase {
    public function testPlaylistCopy() {
        // Setup data
        /** @var Playlists $playlist */
        $playlist = Playlists::query()
            ->create(
                [
                    'title'       => 'Delete Dusk Test Playlist',
                    'description' => 'Delete Temporary playlist to be deleted.',
                    'week'        => 1000
                ]
            );

        /** @var Tags $tag */
        $tag = Tags::query()
            ->firstOrCreate(
                [
                    'name' => 'Dusk Tag',
                ]
            );

        /** @var Videos $video */
        $video = Videos::query()
            ->create(
                [
                    'title'     => 'Dusk Test Video',
                    'file_path' => 'http://dusk.test.com'
                ]
            );

        /** @var PlaylistsTags $playlistTag */
        $playlistTag = PlaylistsTags::query()
            ->create(
                [
                    'playlist_id' => $playlist->id,
                    'tag_id'      => $tag->id
                ]
            );

        /** @var PlaylistsVideos $playlistVideo */
        $playlistVideo = PlaylistsVideos::query()
            ->create(
                [
                    'playlist_id' => $playlist->id,
                    'video_id' => $video->id
                ]
            );

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $responseOne = $this->get(
            '/api/playlists/copy/' . $playlist->slug() . '?joins=<playlists_tags.tags,<images,<playlists_videos',
            $headers
        );

        $id = $responseOne->decodeResponseJson()['playlists']['id'];
        $videoCount =  $responseOne->decodeResponseJson()['playlists']['video_count'];

        $responseTwo = $this->get(
            '/api/playlists/get/' . $id.'?joins=<playlists_tags.tags,<images,<playlists_videos',
            $headers
        );

        // Delete temporary data
        $playlist->forceDelete();
        $tag->forceDelete();
        $video->forceDelete();
        $playlistTag->forceDelete();
        $playlistVideo->forceDelete();

        // Validate
        $responseTwo->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);

        $responseTwo->assertJsonFragment(
            [
                'playlist_id' => $id,
                'tag_id'      => $playlistTag->tag_id,
                'video_count' => $videoCount
            ]
        );

        self::assertTrue(true);
    }
}
