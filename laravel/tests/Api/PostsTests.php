<?php
namespace Tests\Api;

use Tests\TestCase\TestCase;
use Uab\Http\Enums\HttpResponseCodesEnum;
use Uab\Http\Models\Users;
use Uab\Http\Models\Posts;
use Uab\Http\Models\PostLikes;
use Uab\Http\Models\ReportedPosts;

class PostsTests extends TestCase {
    public function testPostHasLiked() {
        // Setup data
        $activeUser = $this->getLoggedInUser();

        /** @var Posts $post */
        $post = Posts::query()
            ->create(
                [
                    'from_user_id' => $activeUser->id,
                    'to_user_id' => $activeUser->id,
                    'message' => 'Dust post message liked'
                ]
            );

        /** @var PostLikes $postLike */
        $postLike = PostLikes::query()
            ->create(
                [
                    'post_id' => $post->id,
                    'user_id' => $activeUser->id
                ]
            );

        $postSlug = $post->slug();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get('/api/posts/get/' . $postSlug, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment(
            [
                'id' => $postSlug,
                'has_liked' => true
            ]
        );

        // Delete temporary data
        $post->forceDelete();
        $postLike->forceDelete();
    }

    public function testPostHasNotLiked() {
        // Setup data
        $activeUser = $this->getLoggedInUser();

        /** @var Posts $post */
        $post = Posts::query()
            ->create(
                [
                    'from_user_id' => $activeUser->id,
                    'to_user_id' => $activeUser->id,
                    'message' => 'Dust post message not liked'
                ]
            );

        $postSlug = $post->slug();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get('/api/posts/get/' . $postSlug, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment(
            [
                'id' => $postSlug,
                'has_liked' => false
            ]
        );

        // Delete temporary data
        $post->forceDelete();
    }

    public function testPostLikeCount() {
        // Setup data
        $activeUser = $this->getLoggedInUser();

        /** @var Posts $post */
        $post = Posts::query()
            ->create(
                [
                    'from_user_id' => $activeUser->id,
                    'to_user_id' => $activeUser->id,
                    'message' => 'Dust post message like count'
                ]
            );

        /** @var PostLikes $postLike */
        $postLike = PostLikes::query()
            ->create(
                [
                    'post_id' => $post->id,
                    'user_id' => $activeUser->id
                ]
            );

        $postSlug = $post->slug();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get('/api/posts/get/' . $postSlug, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment(
            [
                'id' => $postSlug,
                'like_count' => 1
            ]
        );

        // Delete temporary data
        $post->forceDelete();
        $postLike->forceDelete();
    }

    public function testPostHasFlagged() {
        // Setup data
        /** @var Users $user */
        $user = Users::query()
            ->create(
                [
                    'first_name' => 'Dusk',
                    'last_name' => 'ReportFlag'
                ]
            );

        $activeUser = $this->getLoggedInUser();

        /** @var Posts $post */
        $post = Posts::query()
            ->create(
                [
                    'from_user_id' => $user->id,
                    'to_user_id' => $user->id,
                    'message' => 'Dust post message report flag'
                ]
            );

        /** @var ReportedPosts $reportedPost */
        $reportedPost = ReportedPosts::query()
            ->create(
                [
                    'post_id' => $post->id,
                    'user_id' => $activeUser->id
                ]
            );

        $postSlug = $post->slug();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get(
            '/api/posts/get/' . $postSlug,
            $headers
        );

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment(
            [
                'id' => $postSlug,
                'has_flagged' => true
            ]
        );

        // Delete temporary data
        $user->forceDelete();
        $post->forceDelete();
        $reportedPost->forceDelete();
    }

    public function testPostHasNotFlagged() {
        // Setup data
        $activeUser = $this->getLoggedInUser();

        /** @var Posts $post */
        $post = Posts::query()
            ->create(
                [
                    'from_user_id' => $activeUser->id,
                    'to_user_id' => $activeUser->id,
                    'message' => 'Dust post message not reported'
                ]
            );

        $postSlug = $post->slug();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get('/api/posts/get/' . $postSlug, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment(
            [
                'id' => $postSlug,
                'has_flagged' => false
            ]
        );

        // Delete temporary data
        $post->forceDelete();
    }

    public function testPostCount() {
        //get the existing posts count
        /** @var Posts $postsCount */
        $postsCount = Posts::query()
            ->count();

        // Setup data
        $activeUser = $this->getLoggedInUser();

        /** @var Posts $post */
        $post = Posts::query()
            ->create(
                [
                    'from_user_id' => $activeUser->id,
                    'to_user_id' => $activeUser->id,
                    'message' => 'Dust post message'
                ]
            );
        //get the new count
        /** @var Posts $newCount */
        $newCount= Posts::query()
            ->count();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get(
            '/api/posts/count/', $headers
        );
        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);

        $this->assertEquals($postsCount+1, $newCount,"verified posts count");

        // Delete temporary data
        $post->forceDelete();
    }
}

