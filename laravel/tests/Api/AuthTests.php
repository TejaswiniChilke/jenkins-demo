<?php
namespace Tests\Api;

use Tests\TestCase\TestCase;
use Uab\Http\Enums\HttpResponseCodesEnum;
use Uab\Http\Enums\UserTypesEnum;

class AuthTests extends TestCase {
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUnauthenticated() {
        $response = $this->get('/api/users/get/');

        $response->assertStatus(HttpResponseCodesEnum::CLIENT_ERROR_UNAUTHORIZED);
    }

    public function testLoginByUsernameSuccess() {
        $password = 'password-'.rand();

        $user = $this->createUser(
            [
                'password' => $password,
                'user_type_id' => UserTypesEnum::ADMIN
            ]
        );

        $response = $this->login($user->username, $password);

        $json = [
            'id' => $user->slug()
        ];

        $user->forceDelete();

        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertDontSee($password);
        $response->assertJsonFragment($json);
    }

    public function testLoginByEmailSuccess() {
        $password = 'password-' . rand();

        $user = $this->createUser(
            [
                'password' => $password,
                'user_type_id' => UserTypesEnum::ADMIN
            ]
        );

        $response = $this->login($user->email, $password);

        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertDontSee($password);
        $response->assertJsonFragment(
            [
                'id' => $user->slug()
            ]
        );
    }
}
