<?php
namespace Tests\Api\Generated;

use Tests\TestCase\TestCase;
use Uab\Http\Enums\HttpResponseCodesEnum;
use Uab\Http\Models\BlacklistedWords;
use \Exception;

class BlacklistedWordsTests extends TestCase {
    /**
     * Test getting blacklisted_words based on slug from the API
     */
    public function testBlacklistedWordsGetBySlug() {
        // Setup data
        /** @var BlacklistedWords $model */
        $model = factory(BlacklistedWords::class)->create();

        $slug = $model->slug();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get(
            '/api/blacklisted_words/get/' . $slug,
            $headers
        );

        // Validate
        $expectedData['id'] = $slug;

        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment($expectedData);
    }

    /**
     * Test creating blacklisted_words from the API
     */
    public function testBlacklistedWordsCreate() {
        $modelData = factory(BlacklistedWords::class)->raw();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->post('/api/blacklisted_words/create/', $modelData, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_CREATED);
        $response->assertJsonFragment($modelData);
    }

    /**
     * Test editing blacklisted_words from the API
     */
    public function testBlacklistedWordsEdit() {
        // Setup data
        $modelData = [

        ];

        /** @var BlacklistedWords $model */
        $model = factory(BlacklistedWords::class)->create();

        $slug = $model->slug();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->post('/api/blacklisted_words/edit/' . $slug, $modelData, $headers);

        // Validate
        $modelData['id'] = $model->slug();

        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment($modelData);
    }

    /**
     * Test deleting blacklisted_words from the API
     */
    public function testBlacklistedWordsDelete() {
        // Setup data
        /** @var BlacklistedWords $model */
        $model = factory(BlacklistedWords::class)->create();

        $slug = $model->slug();
        $id = $model->safeDecode();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->post(
            '/api/blacklisted_words/delete/' . $slug,
            [],
            $headers
        );

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_NO_CONTENT);

        $databaseObject = BlacklistedWords::query()->find($id);

        $this->assertNull($databaseObject);
    }

    /**
     * Test converting blacklisted_words slug into an id from the API
     */
    public function testBlacklistedWordsIdRoute() {
        // Setup data
        /** @var BlacklistedWords $model */
        $model = BlacklistedWords::query()->get()->random(1)->first();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get('/open/blacklisted_words/id/' . $model->slug(), $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment(
            [
                'id' => $model->id,
            ]
        );
    }

    /**
     * Test converting blacklisted_words an id into a slug from the API
     */
    public function testBlacklistedWordsSlugRoute() {
        // Setup data
        /** @var BlacklistedWords $model */
        $model = BlacklistedWords::query()->get()->random(1)->first();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get('/open/blacklisted_words/slug/' . $model->id, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment(
            [
                'slug' => $model->slug(),
            ]
        );
    }

    /**
     * Test checking if existing blacklisted_words are detected by API
     */
    public function testBlacklistedWordsExists() {
        // Setup data
        /** @var BlacklistedWords $model */
        $model = factory(BlacklistedWords::class)->create();

        $slug = $model->slug();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get('/api/blacklisted_words/exists/' . $slug, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment(['exists' => true]);
    }

    /**
     * Test checking if non-existent blacklisted_words are not detected by API
     *
     * @throws Exception
     */
    public function testBlacklistedWordsDoesNotExist() {
        // Setup data
        /** @var BlacklistedWords $model */
        $model = factory(BlacklistedWords::class)->create();

        $slug = $model->slug();

        $model->delete();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get('/api/blacklisted_words/exists/' . $slug, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment(['exists' => false]);
    }

    /**
     * Test findOrCreate when blacklisted_words already exists
     */
    public function testBlacklistedWordsFindOrCreateWhenExists() {
        // Setup data
        $model = factory(BlacklistedWords::class)->create();

        $findByFields = $model->toArray();
        unset($findByFields['id']);

        $expected = $model->toArray();
        $expected['id'] = $model->slug();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->post('/api/blacklisted_words/findOrCreate/', $findByFields, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment($expected);
    }

    /**
     * @throws Exception
     */
    public function testBlacklistedWordsFindOrCreateWhenDoesNotExist() {
        // Setup data
        /** @var BlacklistedWords $model */
        $model = factory(BlacklistedWords::class)->create();

        $findByFields = $model->toArray();
        unset($findByFields['id']);

        $slug = $model->slug();

        $model->delete();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->post('/api/blacklisted_words/findOrCreate/', $findByFields, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment($findByFields);
        $response->assertDontSeeText($slug);
    }
}
