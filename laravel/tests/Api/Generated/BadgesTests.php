<?php
namespace Tests\Api\Generated;

use Tests\TestCase\TestCase;
use Uab\Http\Enums\HttpResponseCodesEnum;
use Uab\Http\Models\Badges;
use \Exception;

class BadgesTests extends TestCase {
    /**
     * Test getting badges based on slug from the API
     */
    public function testBadgesGetBySlug() {
        // Setup data
        /** @var Badges $model */
        $model = factory(Badges::class)->create();

        $slug = $model->slug();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get(
            '/api/badges/get/' . $slug,
            $headers
        );

        // Validate
        $expectedData['id'] = $slug;

        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment($expectedData);
    }

    /**
     * Test creating badges from the API
     */
    public function testBadgesCreate() {
        $modelData = factory(Badges::class)->raw();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->post('/api/badges/create/', $modelData, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_CREATED);
        $response->assertJsonFragment($modelData);
    }

    /**
     * Test editing badges from the API
     */
    public function testBadgesEdit() {
        // Setup data
        $modelData = [

        ];

        /** @var Badges $model */
        $model = factory(Badges::class)->create();

        $slug = $model->slug();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->post('/api/badges/edit/' . $slug, $modelData, $headers);

        // Validate
        $modelData['id'] = $model->slug();

        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment($modelData);
    }

    /**
     * Test deleting badges from the API
     */
    public function testBadgesDelete() {
        // Setup data
        /** @var Badges $model */
        $model = factory(Badges::class)->create();

        $slug = $model->slug();
        $id = $model->safeDecode();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->post(
            '/api/badges/delete/' . $slug,
            [],
            $headers
        );

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_NO_CONTENT);

        $databaseObject = Badges::query()->find($id);

        $this->assertNull($databaseObject);
    }

    /**
     * Test converting badges slug into an id from the API
     */
    public function testBadgesIdRoute() {
        // Setup data
        /** @var Badges $model */
        $model = Badges::query()->get()->random(1)->first();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get('/open/badges/id/' . $model->slug(), $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment(
            [
                'id' => $model->id,
            ]
        );
    }

    /**
     * Test converting badges an id into a slug from the API
     */
    public function testBadgesSlugRoute() {
        // Setup data
        /** @var Badges $model */
        $model = Badges::query()->get()->random(1)->first();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get('/open/badges/slug/' . $model->id, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment(
            [
                'slug' => $model->slug(),
            ]
        );
    }

    /**
     * Test checking if existing badges are detected by API
     */
    public function testBadgesExists() {
        // Setup data
        /** @var Badges $model */
        $model = factory(Badges::class)->create();

        $slug = $model->slug();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get('/api/badges/exists/' . $slug, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment(['exists' => true]);
    }

    /**
     * Test checking if non-existent badges are not detected by API
     *
     * @throws Exception
     */
    public function testBadgesDoesNotExist() {
        // Setup data
        /** @var Badges $model */
        $model = factory(Badges::class)->create();

        $slug = $model->slug();

        $model->delete();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get('/api/badges/exists/' . $slug, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment(['exists' => false]);
    }

    /**
     * Test findOrCreate when badges already exists
     */
    public function testBadgesFindOrCreateWhenExists() {
        // Setup data
        $model = factory(Badges::class)->create();

        $findByFields = $model->toArray();
        unset($findByFields['id']);

        $expected = $model->toArray();
        $expected['id'] = $model->slug();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->post('/api/badges/findOrCreate/', $findByFields, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment($expected);
    }

    /**
     * @throws Exception
     */
    public function testBadgesFindOrCreateWhenDoesNotExist() {
        // Setup data
        /** @var Badges $model */
        $model = factory(Badges::class)->create();

        $findByFields = $model->toArray();
        unset($findByFields['id']);

        $slug = $model->slug();

        $model->delete();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->post('/api/badges/findOrCreate/', $findByFields, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment($findByFields);
        $response->assertDontSeeText($slug);
    }
}
