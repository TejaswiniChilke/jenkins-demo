<?php
namespace Tests\Api\Generated;

use Tests\TestCase\TestCase;
use Uab\Http\Enums\HttpResponseCodesEnum;
use Uab\Http\Models\TermsAndConditions;
use \Exception;

class TermsAndConditionsTests extends TestCase {
    /**
     * Test getting terms_and_conditions based on slug from the API
     */
    public function testTermsAndConditionsGetBySlug() {
        // Setup data
        /** @var TermsAndConditions $model */
        $model = factory(TermsAndConditions::class)->create();

        $slug = $model->slug();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get(
            '/api/terms_and_conditions/get/' . $slug,
            $headers
        );

        // Validate
        $expectedData['id'] = $slug;

        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment($expectedData);
    }

    /**
     * Test creating terms_and_conditions from the API
     */
    public function testTermsAndConditionsCreate() {
        $modelData = factory(TermsAndConditions::class)->raw();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->post('/api/terms_and_conditions/create/', $modelData, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_CREATED);
        $response->assertJsonFragment($modelData);
    }

    /**
     * Test editing terms_and_conditions from the API
     */
    public function testTermsAndConditionsEdit() {
        // Setup data
        $modelData = [

        ];

        /** @var TermsAndConditions $model */
        $model = factory(TermsAndConditions::class)->create();

        $slug = $model->slug();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->post('/api/terms_and_conditions/edit/' . $slug, $modelData, $headers);

        // Validate
        $modelData['id'] = $model->slug();

        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment($modelData);
    }

    /**
     * Test deleting terms_and_conditions from the API
     */
    public function testTermsAndConditionsDelete() {
        // Setup data
        /** @var TermsAndConditions $model */
        $model = factory(TermsAndConditions::class)->create();

        $slug = $model->slug();
        $id = $model->safeDecode();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->post(
            '/api/terms_and_conditions/delete/' . $slug,
            [],
            $headers
        );

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_NO_CONTENT);

        $databaseObject = TermsAndConditions::query()->find($id);

        $this->assertNull($databaseObject);
    }

    /**
     * Test converting terms_and_conditions slug into an id from the API
     */
    public function testTermsAndConditionsIdRoute() {
        // Setup data
        /** @var TermsAndConditions $model */
        $model = TermsAndConditions::query()->get()->random(1)->first();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get('/open/terms_and_conditions/id/' . $model->slug(), $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment(
            [
                'id' => $model->id,
            ]
        );
    }

    /**
     * Test converting terms_and_conditions an id into a slug from the API
     */
    public function testTermsAndConditionsSlugRoute() {
        // Setup data
        /** @var TermsAndConditions $model */
        $model = TermsAndConditions::query()->get()->random(1)->first();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get('/open/terms_and_conditions/slug/' . $model->id, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment(
            [
                'slug' => $model->slug(),
            ]
        );
    }

    /**
     * Test checking if existing terms_and_conditions are detected by API
     */
    public function testTermsAndConditionsExists() {
        // Setup data
        /** @var TermsAndConditions $model */
        $model = factory(TermsAndConditions::class)->create();

        $slug = $model->slug();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get('/api/terms_and_conditions/exists/' . $slug, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment(['exists' => true]);
    }

    /**
     * Test checking if non-existent terms_and_conditions are not detected by API
     *
     * @throws Exception
     */
    public function testTermsAndConditionsDoesNotExist() {
        // Setup data
        /** @var TermsAndConditions $model */
        $model = factory(TermsAndConditions::class)->create();

        $slug = $model->slug();

        $model->delete();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get('/api/terms_and_conditions/exists/' . $slug, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment(['exists' => false]);
    }

    /**
     * Test findOrCreate when terms_and_conditions already exists
     */
    public function testTermsAndConditionsFindOrCreateWhenExists() {
        // Setup data
        $model = factory(TermsAndConditions::class)->create();

        $findByFields = $model->toArray();
        unset($findByFields['id']);

        $expected = $model->toArray();
        $expected['id'] = $model->slug();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->post('/api/terms_and_conditions/findOrCreate/', $findByFields, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment($expected);
    }

    /**
     * @throws Exception
     */
    public function testTermsAndConditionsFindOrCreateWhenDoesNotExist() {
        // Setup data
        /** @var TermsAndConditions $model */
        $model = factory(TermsAndConditions::class)->create();

        $findByFields = $model->toArray();
        unset($findByFields['id']);

        $slug = $model->slug();

        $model->delete();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->post('/api/terms_and_conditions/findOrCreate/', $findByFields, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment($findByFields);
        $response->assertDontSeeText($slug);
    }
}
