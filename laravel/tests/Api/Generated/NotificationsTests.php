<?php
namespace Tests\Api\Generated;

use Tests\TestCase\TestCase;
use Uab\Http\Enums\HttpResponseCodesEnum;
use Uab\Http\Models\Notifications;
use \Exception;

class NotificationsTests extends TestCase {
    /**
     * Test getting notifications based on slug from the API
     */
    public function testNotificationsGetBySlug() {
        // Setup data
        /** @var Notifications $model */
        $model = factory(Notifications::class)->create();

        $slug = $model->slug();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get(
            '/api/notifications/get/' . $slug,
            $headers
        );

        // Validate
        $expectedData['id'] = $slug;

        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment($expectedData);
    }

    /**
     * Test creating notifications from the API
     */
    public function testNotificationsCreate() {
        $modelData = factory(Notifications::class)->raw();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->post('/api/notifications/create/', $modelData, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_CREATED);
        $response->assertJsonFragment($modelData);
    }

    /**
     * Test editing notifications from the API
     */
    public function testNotificationsEdit() {
        // Setup data
        $modelData = [

        ];

        /** @var Notifications $model */
        $model = factory(Notifications::class)->create();

        $slug = $model->slug();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->post('/api/notifications/edit/' . $slug, $modelData, $headers);

        // Validate
        $modelData['id'] = $model->slug();

        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment($modelData);
    }

    /**
     * Test deleting notifications from the API
     */
    public function testNotificationsDelete() {
        // Setup data
        /** @var Notifications $model */
        $model = factory(Notifications::class)->create();

        $slug = $model->slug();
        $id = $model->safeDecode();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->post(
            '/api/notifications/delete/' . $slug,
            [],
            $headers
        );

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_NO_CONTENT);

        $databaseObject = Notifications::query()->find($id);

        $this->assertNull($databaseObject);
    }

    /**
     * Test converting notifications slug into an id from the API
     */
    public function testNotificationsIdRoute() {
        // Setup data
        /** @var Notifications $model */
        $model = Notifications::query()->get()->random(1)->first();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get('/open/notifications/id/' . $model->slug(), $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment(
            [
                'id' => $model->id,
            ]
        );
    }

    /**
     * Test converting notifications an id into a slug from the API
     */
    public function testNotificationsSlugRoute() {
        // Setup data
        /** @var Notifications $model */
        $model = Notifications::query()->get()->random(1)->first();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get('/open/notifications/slug/' . $model->id, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment(
            [
                'slug' => $model->slug(),
            ]
        );
    }

    /**
     * Test checking if existing notifications are detected by API
     */
    public function testNotificationsExists() {
        // Setup data
        /** @var Notifications $model */
        $model = factory(Notifications::class)->create();

        $slug = $model->slug();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get('/api/notifications/exists/' . $slug, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment(['exists' => true]);
    }

    /**
     * Test checking if non-existent notifications are not detected by API
     *
     * @throws Exception
     */
    public function testNotificationsDoesNotExist() {
        // Setup data
        /** @var Notifications $model */
        $model = factory(Notifications::class)->create();

        $slug = $model->slug();

        $model->delete();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get('/api/notifications/exists/' . $slug, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment(['exists' => false]);
    }

    /**
     * Test findOrCreate when notifications already exists
     */
    public function testNotificationsFindOrCreateWhenExists() {
        // Setup data
        $model = factory(Notifications::class)->create();

        $findByFields = $model->toArray();
        unset($findByFields['id']);

        $expected = $model->toArray();
        $expected['id'] = $model->slug();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->post('/api/notifications/findOrCreate/', $findByFields, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment($expected);
    }

    /**
     * @throws Exception
     */
    public function testNotificationsFindOrCreateWhenDoesNotExist() {
        // Setup data
        /** @var Notifications $model */
        $model = factory(Notifications::class)->create();

        $findByFields = $model->toArray();
        unset($findByFields['id']);

        $slug = $model->slug();

        $model->delete();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->post('/api/notifications/findOrCreate/', $findByFields, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment($findByFields);
        $response->assertDontSeeText($slug);
    }
}
