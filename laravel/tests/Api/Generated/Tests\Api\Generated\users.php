<?php
namespace Tests\Api\GeneratedTests\Api\Generated\users;

use Tests\TestCase\TestCase;
use Uab\Http\Enums\HttpResponseCodesEnum;
use Uab\Http\Models\Articles;

class users extends TestCase {
    /**
     *  Test getting a users based on slug from the API
     */
    public function test{{DummyModel}}GetBySlug() {
        // Setup data
        // TODO
        /** @var {{DummyModel}} $model */
        $model = {{DummyModel}}::query()->create();

        $slug = $model->slug();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get(
            '/api/users/get/' . $slug,
            $headers
        );

        // Validate
        $expectedData['id'] = $slug;

        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment($expectedData);
    }

    /**
     *  Test creating a users from the API
     */
    public function test{{DummyModel}}Create() {
        // Setup data
        $modelData = [
            // TODO
        ];

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->post('/api/users/create/', $modelData, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_CREATED);
        $response->assertJsonFragment($modelData);
    }

    public function testArticleEdit() {
        // Setup data
        $modelData = [
            // TODO
        ];

        /** @var {{DummyModel}} $article */
        $model = {{DummyModel}}::query()->create($articleData);

        $slug = $model->slug();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->post('/api/users/edit/' . $slug, $articleData, $headers);

        // Validate
        $modelData['id'] = $article->slug();

        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment($articleData);
    }

    public function testArticleDelete() {
        // Setup data
        $modelData = [
            // TODO
        ];

        /** @var {{DummyModel}} $model */
        $model = {{DummyModel}}::query()
            ->create($modelData);

        $slug = $article->slug();
        $id = $article->safeDecode();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->post(
            '/api/articles/delete/' . $slug,
            [],
            $headers
        );

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_NO_CONTENT);

        $databaseObject = {{DummyModel}}::query()->find($id)->first();

        $this->assertNull($databaseObject);
    }
}
