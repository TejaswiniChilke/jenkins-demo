<?php
namespace Tests\Api\Generated;

use Tests\TestCase\TestCase;
use Uab\Http\Enums\HttpResponseCodesEnum;
use Uab\Http\Models\Surveys;
use \Exception;

class SurveysTests extends TestCase {
    /**
     * Test getting surveys based on slug from the API
     */
    public function testSurveysGetBySlug() {
        // Setup data
        /** @var Surveys $model */
        $model = factory(Surveys::class)->create();

        $slug = $model->slug();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get(
            '/api/surveys/get/' . $slug,
            $headers
        );

        // Validate
        $expectedData['id'] = $slug;

        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment($expectedData);
    }

    /**
     * Test creating surveys from the API
     */
    public function testSurveysCreate() {
        $modelData = factory(Surveys::class)->raw();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->post('/api/surveys/create/', $modelData, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_CREATED);
        $response->assertJsonFragment($modelData);
    }

    /**
     * Test editing surveys from the API
     */
    public function testSurveysEdit() {
        // Setup data
        $modelData = [

        ];

        /** @var Surveys $model */
        $model = factory(Surveys::class)->create();

        $slug = $model->slug();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->post('/api/surveys/edit/' . $slug, $modelData, $headers);

        // Validate
        $modelData['id'] = $model->slug();

        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment($modelData);
    }

    /**
     * Test deleting surveys from the API
     */
    public function testSurveysDelete() {
        // Setup data
        /** @var Surveys $model */
        $model = factory(Surveys::class)->create();

        $slug = $model->slug();
        $id = $model->safeDecode();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->post(
            '/api/surveys/delete/' . $slug,
            [],
            $headers
        );

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_NO_CONTENT);

        $databaseObject = Surveys::query()->find($id);

        $this->assertNull($databaseObject);
    }

    /**
     * Test converting surveys slug into an id from the API
     */
    public function testSurveysIdRoute() {
        // Setup data
        /** @var Surveys $model */
        $model = Surveys::query()->get()->random(1)->first();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get('/open/surveys/id/' . $model->slug(), $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment(
            [
                'id' => $model->id,
            ]
        );
    }

    /**
     * Test converting surveys an id into a slug from the API
     */
    public function testSurveysSlugRoute() {
        // Setup data
        /** @var Surveys $model */
        $model = Surveys::query()->get()->random(1)->first();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get('/open/surveys/slug/' . $model->id, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment(
            [
                'slug' => $model->slug(),
            ]
        );
    }

    /**
     * Test checking if existing surveys are detected by API
     */
    public function testSurveysExists() {
        // Setup data
        /** @var Surveys $model */
        $model = factory(Surveys::class)->create();

        $slug = $model->slug();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get('/api/surveys/exists/' . $slug, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment(['exists' => true]);
    }

    /**
     * Test checking if non-existent surveys are not detected by API
     *
     * @throws Exception
     */
    public function testSurveysDoesNotExist() {
        // Setup data
        /** @var Surveys $model */
        $model = factory(Surveys::class)->create();

        $slug = $model->slug();

        $model->delete();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get('/api/surveys/exists/' . $slug, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment(['exists' => false]);
    }

    /**
     * Test findOrCreate when surveys already exists
     */
    public function testSurveysFindOrCreateWhenExists() {
        // Setup data
        $model = factory(Surveys::class)->create();

        $findByFields = $model->toArray();
        unset($findByFields['id']);

        $expected = $model->toArray();
        $expected['id'] = $model->slug();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->post('/api/surveys/findOrCreate/', $findByFields, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment($expected);
    }

    /**
     * @throws Exception
     */
    public function testSurveysFindOrCreateWhenDoesNotExist() {
        // Setup data
        /** @var Surveys $model */
        $model = factory(Surveys::class)->create();

        $findByFields = $model->toArray();
        unset($findByFields['id']);

        $slug = $model->slug();

        $model->delete();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->post('/api/surveys/findOrCreate/', $findByFields, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment($findByFields);
        $response->assertDontSeeText($slug);
    }
}
