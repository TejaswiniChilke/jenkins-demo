<?php
namespace Tests\Api\Generated;

use Tests\TestCase\TestCase;
use Uab\Http\Enums\HttpResponseCodesEnum;
use Uab\Http\Models\Playlists;
use \Exception;

class PlaylistsTests extends TestCase {
    /**
     * Test getting playlists based on slug from the API
     */
    public function testPlaylistsGetBySlug() {
        // Setup data
        /** @var Playlists $model */
        $model = factory(Playlists::class)->create();

        $slug = $model->slug();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get(
            '/api/playlists/get/' . $slug,
            $headers
        );

        // Validate
        $expectedData['id'] = $slug;

        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment($expectedData);
    }

    /**
     * Test creating playlists from the API
     */
    public function testPlaylistsCreate() {
        $modelData = factory(Playlists::class)->raw();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->post('/api/playlists/create/', $modelData, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_CREATED);
        $response->assertJsonFragment($modelData);
    }

    /**
     * Test editing playlists from the API
     */
    public function testPlaylistsEdit() {
        // Setup data
        $modelData = [

        ];

        /** @var Playlists $model */
        $model = factory(Playlists::class)->create();

        $slug = $model->slug();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->post('/api/playlists/edit/' . $slug, $modelData, $headers);

        // Validate
        $modelData['id'] = $model->slug();

        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment($modelData);
    }

    /**
     * Test deleting playlists from the API
     */
    public function testPlaylistsDelete() {
        // Setup data
        /** @var Playlists $model */
        $model = factory(Playlists::class)->create();

        $slug = $model->slug();
        $id = $model->safeDecode();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->post(
            '/api/playlists/delete/' . $slug,
            [],
            $headers
        );

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_NO_CONTENT);

        $databaseObject = Playlists::query()->find($id);

        $this->assertNull($databaseObject);
    }

    /**
     * Test converting playlists slug into an id from the API
     */
    public function testPlaylistsIdRoute() {
        // Setup data
        /** @var Playlists $model */
        $model = Playlists::query()->get()->random(1)->first();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get('/open/playlists/id/' . $model->slug(), $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment(
            [
                'id' => $model->id,
            ]
        );
    }

    /**
     * Test converting playlists an id into a slug from the API
     */
    public function testPlaylistsSlugRoute() {
        // Setup data
        /** @var Playlists $model */
        $model = Playlists::query()->get()->random(1)->first();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get('/open/playlists/slug/' . $model->id, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment(
            [
                'slug' => $model->slug(),
            ]
        );
    }

    /**
     * Test checking if existing playlists are detected by API
     */
    public function testPlaylistsExists() {
        // Setup data
        /** @var Playlists $model */
        $model = factory(Playlists::class)->create();

        $slug = $model->slug();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get('/api/playlists/exists/' . $slug, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment(['exists' => true]);
    }

    /**
     * Test checking if non-existent playlists are not detected by API
     *
     * @throws Exception
     */
    public function testPlaylistsDoesNotExist() {
        // Setup data
        /** @var Playlists $model */
        $model = factory(Playlists::class)->create();

        $slug = $model->slug();

        $model->delete();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get('/api/playlists/exists/' . $slug, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment(['exists' => false]);
    }

    /**
     * Test findOrCreate when playlists already exists
     */
    public function testPlaylistsFindOrCreateWhenExists() {
        // Setup data
        $model = factory(Playlists::class)->create();

        $findByFields = $model->toArray();
        unset($findByFields['id']);

        $expected = $model->toArray();
        $expected['id'] = $model->slug();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->post('/api/playlists/findOrCreate/', $findByFields, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment($expected);
    }

    /**
     * @throws Exception
     */
    public function testPlaylistsFindOrCreateWhenDoesNotExist() {
        // Setup data
        /** @var Playlists $model */
        $model = factory(Playlists::class)->create();

        $findByFields = $model->toArray();
        unset($findByFields['id']);

        $slug = $model->slug();

        $model->delete();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->post('/api/playlists/findOrCreate/', $findByFields, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment($findByFields);
        $response->assertDontSeeText($slug);
    }
}
