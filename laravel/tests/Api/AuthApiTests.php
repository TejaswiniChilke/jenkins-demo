<?php
namespace Tests\Api;

use Tests\TestCase\TestCase;
use Uab\Http\Enums\HttpResponseCodesEnum;
use Uab\Http\Enums\UserTypesEnum;
use \Exception;

class AuthApiTests extends TestCase {
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testIncorrectCredentials() {
        $response = $this->login('test', 'password1234');

        $response->assertStatus(HttpResponseCodesEnum::CLIENT_ERROR_FORBIDDEN);
        $response->assertSeeText('Wrong username or password');
    }

    /**
     * A basic test example.
     *
     * @return void
     *
     * @throws Exception
     */
    public function testAuthSuccess() {
        $password = 'password-' . rand();

        $user = $this->createUser(
            [
                'password' => $password,
                'user_type_id' => UserTypesEnum::ADMIN,
            ]
        );

        $response = $this->login($user->email, $password);

        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertSeeText('expires_in');
        $response->assertSeeText('token');
    }

    /**
     * A basic test example.
     *
     * @return void
     *
     * @throws Exception
     */
    public function testAuthIncorrectGrantType() {
        $password = 'password-' . rand();

        $user = $this->createUser(
            [
                'password' => $password,
                'user_type_id' => UserTypesEnum::ADMIN,
            ]
        );

        $response = $this->post(
            '/oauth/login',
            [
                'username'      => $user->email,
                'password'      => $password,
                'scope'         => self::$OAUTH_SCOPE,
                'client_secret' => self::$OAUTH_CLIENT_SECRET,
                'grant_type'    => 'fsfsf',
                'client_id'     => self::$OAUTH_CLIENT_ID,
            ]
        );

        $response->assertStatus(HttpResponseCodesEnum::CLIENT_ERROR_FORBIDDEN);
        $response->assertSeeText('Wrong');
    }

    /**
     * A basic test example.
     *
     * @return void
     *
     * @throws Exception
     */
    public function testAuthIncorrectScope() {
        $password = 'password-' . rand();

        $user = $this->createUser(
            [
                'password' => $password,
                'user_type_id' => UserTypesEnum::ADMIN,
            ]
        );

        $response = $this->post(
            '/oauth/login',
            [
                'username'      => $user->email,
                'password'      => $password,
                'scope'         => 'gwrwrgr',
                'client_secret' => self::$OAUTH_CLIENT_SECRET,
                'grant_type'    => self::$OAUTH_GRANT_TYPE,
                'client_id'     => self::$OAUTH_CLIENT_ID,
            ]
        );

        $response->assertStatus(HttpResponseCodesEnum::CLIENT_ERROR_FORBIDDEN);
        $response->assertSeeText('Wrong');
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testAuthIncorrectSecret() {
        $response = $this->post(
            '/oauth/login',
            [
                'username'      => 'uaccgeneraluser',
                'password'      => 'bRpEBb33CqvG8Wax',
                'scope'         => self::$OAUTH_SCOPE,
                'client_secret' => 'gwregwr',
                'grant_type'    => self::$OAUTH_GRANT_TYPE,
                'client_id'     => self::$OAUTH_CLIENT_ID,
            ]
        );

        $response->assertStatus(HttpResponseCodesEnum::CLIENT_ERROR_FORBIDDEN);
        $response->assertSeeText('Wrong');
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testAuthIncorrectClientId() {
        $response = $this->post(
            '/oauth/login',
            [
                'username'      => 'uaccgeneraluser',
                'password'      => 'bRpEBb33CqvG8Wax',
                'scope'         => self::$OAUTH_SCOPE,
                'client_secret' => self::$OAUTH_CLIENT_SECRET,
                'grant_type'    => self::$OAUTH_GRANT_TYPE,
                'client_id'     => -1,
            ]
        );

        $response->assertStatus(HttpResponseCodesEnum::CLIENT_ERROR_FORBIDDEN);
        $response->assertSeeText('Wrong');
    }
}
