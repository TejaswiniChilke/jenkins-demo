<?php
namespace Tests\Api;

use Tests\TestCase\TestCase;
use Uab\Http\Enums\HttpResponseCodesEnum;
use Uab\Http\Models\YoutubeVideoInfos;

class YoutubeVideoTests extends TestCase {
    public function testYoutubeExistingVideoIdRoute() {
        // Setup data
        /** @var YoutubeVideoInfos $youtubeVideo */
        $youtubeVideo= YoutubeVideoInfos::query()
            ->get()->random(1)->pluck('video_id')
            ->first();

        // Build request
        $headers=$this->getHeaders();

        // Make request
        $response=$this->get('/youtube/video_id/get/' .$youtubeVideo->slug(),$headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment(
            [
                'id' => $youtubeVideo->id,
            ]
        );
    }
}
