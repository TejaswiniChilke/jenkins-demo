<?php
namespace Tests\Api;

use Tests\TestCase\TestCase;
use Uab\Http\Enums\HttpResponseCodesEnum;
use Uab\Http\Models\Articles;

class ArticlesTests extends TestCase {
    public function testArticlesCount() {
        //get the existing articles count
        /** @var Articles $articlesCount */
        $articlesCount = Articles::query()
            ->count();

        // Setup data and create one article
        $articleData = [
            'title' => 'Dusk Test Article'
        ];

        /** @var Articles $article */
        $article = Articles::query()->create($articleData);

        //get the new count
        /** @var Articles $newCount */
        $newCount = Articles::query()
            ->count();

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get(
            '/api/articles/count/', $headers
        );

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);

        $this->assertEquals($articlesCount + 1, $newCount, "verified article count");

        // Delete temporary data
        $article->forceDelete();
    }
}
