<?php
namespace Tests\Api;

use Cmgmyr\Messenger\Models\Message;
use Tests\TestCase\TestCase;
use Uab\Http\Enums\HttpResponseCodesEnum;
use Uab\Http\Models\Threads;
use Exception;

class MessagingTests extends TestCase {
    /**
     * @throws Exception
     */
    public function testThreadGetById() {
        // Setup data
        $fromUser = $this->createUser();
        $toUser = $this->createUser();

        $thread = new Threads();
        $thread->subject = 'Dusk Test';
        $thread->save();

        $thread->addParticipant($fromUser->id);
        $thread->addParticipant($toUser->id);

        $message = Message::query()
            ->create(
                [
                    'thread_id' => $thread->id,
                    'user_id'   => $fromUser->id,
                    'body'      => 'This is a Dusk test.'
                ]
            );

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get(
            '/messaging/get/' . $thread->slug(),
            $headers
        );

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment(
            [
                'id' => $thread->slug(),
            ]
        );
    }
}
