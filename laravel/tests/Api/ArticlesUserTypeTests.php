<?php
namespace Tests\Api;

use Tests\TestCase\TestCase;
use Uab\Http\Enums\HttpResponseCodesEnum;
use Uab\Http\Models\ArticlesUserTypes;
use Uab\Http\Models\Articles;
use Uab\Http\Models\UserTypes;

class ArticlesUserTypeTests extends TestCase {
    public function __construct($name = null, array $data = [], $dataName = '') {
        parent::__construct($name, $data, $dataName);

//        $this->clearTestData(new Articles());
//        $this->clearTestData(new UserTypes());
//        $this->clearTestData(new ArticlesUserTypes());
    }

    public function testArticlesUserTypeGetById() {
        // Setup data
        /** @var Articles $article */
        $article = Articles::query()
            ->create(
                [
                    'title' => 'Dusk Test Article 1',
                    'week'  =>  1000
                ]
            );

        /** @var UserTypes $userType */
        $userType = UserTypes::query()
            ->create(
                [
                    'name'         => 'Dusk Test User Type 1',
                    'display_name' => 'Dusk Test User Type Display Name',
                    'project_id'   => $this->getProject()->id
                ]
            );

        $articlesUserTypeData = [
            'article_id'   => $article->id,
            'user_type_id' => $userType->id,
            'week'         => 50,
        ];

        /** @var ArticlesUserTypes $articlesUserType */
        $articlesUserType = ArticlesUserTypes::query()->create($articlesUserTypeData);

        // Build request
        $headers = $this->getHeaders();

        // Make request
        $response = $this->get('/api/articles_user_types/get/' . $articlesUserType->id, $headers);

        // Validate
        $response->assertStatus(HttpResponseCodesEnum::SUCCESS_OK);
        $response->assertJsonFragment(
            [
                'id'           => $articlesUserType->slug(),
                'article_id'   => $article->slug(),
                'user_type_id' => $userType->slug()
            ]
        );

        // Delete temporary data
        $article->forceDelete();
        $userType->forceDelete();
        $articlesUserType->forceDelete();
    }
}
