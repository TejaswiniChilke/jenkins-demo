<?php
namespace Tests\Unit\Providers;

use Carbon\Carbon;
use Tests\TestCase\TestCase;
use Uab\Providers\DurationServiceProvider;

class DurationServiceProviderTests extends TestCase {
    public function testStartTimeFirstDay() {
        $service = new DurationServiceProvider();

        $week = $service->getWeek(Carbon::now()->subHour());

        $this->assertEquals(1, $week);
    }

    public function testStartTimeWeekTwo() {
        $service = new DurationServiceProvider();

        $week = $service->getWeek(Carbon::now()->subWeek()->subDay());

        $this->assertEquals(2, $week);
    }

    public function testStartTimeTomorrow() {
        $service = new DurationServiceProvider();

        $week = $service->getWeek(Carbon::now()->addDay());

        $this->assertNull($week);
    }

    public function testStartTimeNull() {
        $service = new DurationServiceProvider();

        $week = $service->getWeek(null);

        $this->assertNull($week);
    }
}
