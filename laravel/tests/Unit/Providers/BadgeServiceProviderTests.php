<?php
namespace Tests\Unit\Providers;

use \Exception;
use Mockery;
use Tests\TestCase\TestCase;
use Uab\Http\Enums\BadgeConditionTypesEnum;
use Uab\Http\Enums\UserTypesEnum;
use Uab\Http\Models\BadgeConditions;
use Uab\Http\Models\BadgeConditionTypes;
use Uab\Http\Models\Badges;
use Uab\Http\Models\BadgesUsers;
use Uab\Providers\BadgeServiceProvider;
use Uab\Providers\UsersServiceProvider;

class BadgeServiceProviderTests extends TestCase {
    /**
     * @throws Exception
     */
    public function testGettingCompletedBadge() {
        $user = $this->createUser();

        $badge = Badges::query()->get()->random(1)->first();

        $badgeUser = BadgesUsers::query()->create(
            [
                'badge_id' => $badge->id,
                'user_id' => $user->id,
            ]
        );

        $service = new BadgeServiceProvider();
        $completed = $service->getCompletedBadges($badge, $user);

        $this->assertCount(1, $completed);
    }

    public function testGettingNoCompletedBadge() {
        $user = $this->createUser();

        $badge = Badges::query()->get()->random(1)->first();

        $service = new BadgeServiceProvider();
        $completed = $service->getCompletedBadges($badge, $user);

        $this->assertCount(0, $completed);
    }

    /**
     * @throws Exception
     */
    public function testGettingNoConditionsByBadge() {
        $badge = new Badges();
        $badge->title = 'Dusk Test - ' . uniqid();
        $badge->save();

        $service = new BadgeServiceProvider();

        $conditions = $service->getConditionsByBadgeId($badge->id);

        $this->assertCount(0, $conditions);
    }

    /**
     * @throws Exception
     */
    public function testGettingSomeConditionsByBadge() {
        $badge = Badges::query()->get()->random(1)->first();

        $service = new BadgeServiceProvider();

        $conditions = $service->getConditionsByBadgeId($badge->id);

        $this->assertGreaterThan(0, $conditions->count());
    }

    public function testGettingConditionTypeByName() {
        $name = BadgeConditionTypesEnum::random();

        $service = new BadgeServiceProvider();

        $type = $service->getConditionTypeByName($name);

        $this->assertEquals($type->name, $name);
    }

    public function testGettingConditionTypeNonexistentName() {
        $service = new BadgeServiceProvider();

        $type = $service->getConditionTypeByName(uniqid());

        $this->assertNull($type);
    }

    /**
     * @throws Exception
     */
    public function testGettingNoCompletedCondition() {
        $user = $this->createUser();

        $badgeCondition = BadgeConditions::query()->get()->random(1)->first();

        $service = new BadgeServiceProvider();

        $completed = $service->getCompletedConditions($badgeCondition, $user);

        $this->assertCount(0, $completed);
    }

    public function testGettingCompletedConditionWithoutUser() {
        $badgeCondition = BadgeConditions::query()->get()->random(1)->first();

        $service = new BadgeServiceProvider();

        $completed = $service->getCompletedConditions($badgeCondition);

        $this->assertGreaterThan(0, $completed->count());
    }

    public function testGettingParticipantBadges() {
        $userService = new UsersServiceProvider();
        $userType = $userService->getUserTypesByName(UserTypesEnum::PARTICIPANT);

        $badgeConditionType = BadgeConditionTypes::query()->get()->random(1)->first();

        $service = new BadgeServiceProvider();

        $assignedBadges = $service->getUserTypeBadges($userType, $badgeConditionType);

        $this->assertGreaterThan(0, $assignedBadges->count());
    }

    /**
     * @throws Exception
     */
    public function testCheckAndRewardWhenOverMax() {
        $user = $this->createUser();

        /** @var Badges $badge */
        $badge = Badges::query()
            ->where('max_completions', '>=', 1)
            ->get()
            ->random(1)
            ->first();

        $badgeUsers = [];

        for ($i = $badge->max_completions; $i >= 0; $i--) {
            $badgeUsers[] = BadgesUsers::query()->create(
                [
                    'badge_id' => $badge->id,
                    'user_id' => $user->id,
                ]
            );
        }

        $service = new BadgeServiceProvider();

        $result = $service->checkAndRewardBadge($badge, $user);

        $this->assertFalse($result);
    }

    /**
     * @throws Exception
     */
    public function testCheckAndRewardWhenNotEarned() {
        $spy = $this->spy(BadgeServiceProvider::class);

        $user = $this->createUser();

        /** @var Badges $badge */
        $badge = Badges::query()
            ->where('max_completions', '>=', 1)
            ->get()
            ->random(1)
            ->first();

        $service = new BadgeServiceProvider();

        $service->checkAndRewardBadge($badge, $user);

        $spy->shouldHaveRecieved('checkAndRewardCondition');

        Mockery::close();
    }
}
