<?php
namespace Tests\TestCase;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\TestResponse;
use Tests\CreatesApplication;
use Tests\TestCase\Traits\AuthTestCaseTrait;

abstract class TestCase extends BaseTestCase {
    use AuthTestCaseTrait;
    use CreatesApplication;

    private $accessToken = null;
    private $loggedInUser = null;
    private $project = null;

    /**
     * @return void
     */
    protected function setUp():void {
        parent::setUp();

        $this->clearData();
    }

    /**
     * @return void
     */
    protected function tearDown():void {
        $this->clearData();
    }

    /**
     * @param string|null $username
     * @param string|null $password
     *
     * @return TestResponse
     */
    protected function login(?string $username, ?string $password):TestResponse {
        $payload = [
            'username'      => $username,
            'password'      => $password,
            'scope'         => '',
            'client_secret' => '39k95ArDZaFUKiVPPi2ndD2qbvtmdlzjf9owgzEn',
            'grant_type'    => 'password',
            'client_id'     => 3,
        ];

        return $this->post('/oauth/login', $payload);
    }

    /**
     * @return string[]
     */
    protected function getHeaders():array {
        $token = $this->getAccessToken();

        return [
            'Authorization' => 'Bearer '.$token
        ];
    }
}
