<?php
namespace Tests\TestCase\Traits;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Support\Arr;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\HomePage;
use Tests\Browser\Pages\LoginPage;
use Uab\Http\Enums\UserTypesEnum;
use Uab\Http\Models\Base\BaseModel;
use Uab\Http\Models\Projects;
use Uab\Http\Models\Users;
use Uab\Http\Models\UserTypes;
use Facebook\WebDriver\Exception\TimeOutException;

trait AuthTestCaseTrait {
    public static $OAUTH_SCOPE = '';
    public static $OAUTH_CLIENT_ID = 3;
    public static $OAUTH_CLIENT_SECRET = '39k95ArDZaFUKiVPPi2ndD2qbvtmdlzjf9owgzEn';
    public static $OAUTH_GRANT_TYPE = 'password';

    private $accessToken = null;
    private $loggedInUser = null;
    private $project = null;

    /**
     * @param BaseModel $model
     *
     * @return boolean|null
     */
    protected function clearTestData($model) {
        $column = $model->getTestColumn();
        $key = $model->getTestKey();

        if (!is_null($column) && !is_null($key)) {
            return $model::query()
                ->where($column, 'LIKE', '%' . $key . '%')
                ->delete();
        }

        return null;
    }

    private function clearData() {
        $this->clearTestData(new Projects());
        $this->clearTestData(new Users());
    }

    protected function setUp():void {
        parent::setUp();

        $this->clearData();
    }

    protected function tearDown():void {
       $this->clearData();
    }

    /**
     * @param mixed[] $attributes
     *
     * @return Users|Model
     */
    protected function createUser($attributes = []) {
        if (array_key_exists('user_type_id', $attributes)) {
            if (!is_numeric($attributes['user_type_id'])) {
                /** @var UserTypes $userType */
                $userType = UserTypes::query()
                    ->where('name', '=', $attributes['user_type_id'])
                    ->first();

                $attributes['user_type_id'] = $userType->id;
            }
        }

        return factory(Users::class)->create($attributes);
    }

    /**
     * @param string|null $username
     * @param string|null $password
     *
     * @return TestResponse
     */
    protected function login(?string $username, ?string $password):TestResponse {
        $payload = [
            'username'      => $username,
            'password'      => $password,
            'scope'         => self::$OAUTH_SCOPE,
            'client_secret' => self::$OAUTH_CLIENT_SECRET,
            'grant_type'    => self::$OAUTH_GRANT_TYPE,
            'client_id'     => self::$OAUTH_CLIENT_ID,
        ];

        return $this->post('oauth/login', $payload);
    }

    protected function getAccessToken() {
        if (is_null($this->accessToken)) {
            $user = $this->getLoggedInUser();

            $token = $user->createToken('auth');
            $this->accessToken = $token->accessToken;
        }

        return $this->accessToken;
    }

    protected function getHeaders() {
        $token = $this->getAccessToken();

        return [
            'Authorization' => 'Bearer '.$token
        ];
    }

    protected function getLoggedInUser() {
        if (is_null($this->loggedInUser)) {
            $this->loggedInUser = $this->createUser();

//            $username = $user->getAttributeValue('username');
//            $password = 'password';
//
//            $response = $this->login($username, $password);
//
//            $token = $response->json('token');
//            if (array_key_exists('access_token', $token)) {
//                $this->loggedInUser = $user;
//                $this->accessToken = $token['access_token'];
//            }
        }

        return $this->loggedInUser;
    }

    protected function getProject() {
        if (is_null($this->project)) {
            $project = new Projects();
            $project->name = 'Dusk Test';
            $success = $project->save();

            if ($success) {
                $this->project = $project;
            }
        }

        return $this->project;
    }

    /**
     * @param Browser $browser
     * @param array $attributes
     *
     * @return Users
     *
     * @throws TimeOutException
     */
    public function createUserAndLogin(Browser $browser, $attributes = []):Users {
        $password = Arr::get($attributes, 'password', null);
        if (is_null($password)) {
            $password = 'password-' . rand();

            $attributes['password'] = $password;
        }

        if (!Arr::has($attributes, 'user_type_id')) {
            $attributes['user_type_id'] = UserTypesEnum::M2M;
        }

        $user = $this->createUser($attributes);

        $loginPage = new LoginPage();
        $browser->visit($loginPage);
        $loginPage->assert($browser);
        $loginPage->fillInLogin($browser, $user->email, $password);

        $homePage = new HomePage();
        $homePage->assert($browser);

        return $user;
    }
}
