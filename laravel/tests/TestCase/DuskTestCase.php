<?php
namespace Tests\TestCase;

use Laravel\Dusk\TestCase as BaseTestCase;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Tests\CreatesApplication;
use Tests\TestCase\Traits\AuthTestCaseTrait;

abstract class DuskTestCase extends BaseTestCase {
    //use AuthTestCaseTrait;
    use CreatesApplication;

    public function __construct($name = null, array $data = [], $dataName = '') {
        parent::__construct($name, $data, $dataName);
    }

    /**
     * Prepare for Dusk test execution.
     *
     * @beforeClass
     * @return void
     */
    public static function prepare() {
        static::startChromeDriver();
    }

    /**
     * Create the RemoteWebDriver instance.
     *
     * @return RemoteWebDriver
     */
    protected function driver() {

        $options = (new ChromeOptions)->addArguments([
//            '--disable-gpu',
            '--headless',
            '--window-size=1920,1080',
            '--disable-notifications'
        ]);


        return RemoteWebDriver::create(
            'http://localhost:9515',
            DesiredCapabilities::chrome()->setCapability(
                ChromeOptions::CAPABILITY,
                $options
            )
        );
    }
}
