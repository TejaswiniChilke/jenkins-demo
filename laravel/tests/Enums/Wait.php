<?php
namespace Tests\Enums;

class Wait {
    public static $SHORTEST;
    public static $SHORTER;
    public static $SHORT;
    public static $MEDIUM;
    public static $LONG;
    public static $LONGER;
    public static $LONGEST;

    public function __construct() {
        self::$SHORTEST = intval(config('tests.wait.shortest'));
        self::$SHORTER = intval(config('tests.wait.shorter'));
        self::$SHORT = intval(config('tests.wait.short'));
        self::$MEDIUM = intval(config('tests.wait.medium'));
        self::$LONG = intval(config('tests.wait.long'));
        self::$LONGER = intval(config('tests.wait.longer'));
        self::$LONGEST = intval(config('tests.wait.longest'));
    }
}
