<?php
/*
|--------------------------------------------------------------------------
| OAuth Routes
|--------------------------------------------------------------------------
*/
Route::any('/email/exists/{email?}', 'Base\\OAuthController@emailExists');

Route::any('/registered/{username?}', 'Base\\OAuthController@checkRegistry');
Route::any('/getToken', 'Base\\OAuthController@getToken');
Route::post('/login', 'Base\\OAuthController@login');
Route::any('/logout','Base\\OAuthController@logout')->middleware('auth:api');

Route::post('/password/update', 'Base\\OAuthController@updatePassword');

Route::any('/security_questions/get', 'Base\\OAuthController@getSecurityQuestion');
