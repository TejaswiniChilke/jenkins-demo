<?php
/*
|--------------------------------------------------------------------------
| Test Routes
|--------------------------------------------------------------------------
|
| Here is where you can register test routes for your application. These
| routes are loaded by the RouteServiceProvider
|
*/
Route::get('/emails/send', 'Communication\\EmailsController@sendEmail')->name('test.emails.send');
Route::get('/texts/send', 'Communication\\TextsController@sendSms')->name('test.texts.send');

Route::get('/ics/get/{id?}', 'IcsController@get')->name('test.ics.get');

Route::any('/make/{command}/', 'TestController@make')->name('test.redis.make');

Route::get('/redis/get/{key}', 'TestController@redisGet')->name('test.redis.get');
Route::get('/redis/set/{key}/{value}', 'TestController@redisSet')->name('test.redis.set');

Route::get('/users/tags/{id}', 'Base\\ApiController@getTags')->name('test.tags');
