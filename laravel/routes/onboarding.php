<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "onboarding" middleware group. Enjoy building your API!
|
*/
Route::any('/browser/block/{browser?}', 'OnboardingController@blockBrowser')->name('onboarding.blockBrowser');

Route::any('/users/apply', 'OnboardingController@apply')->name('onboarding.apply');

Route::any('/browser/check/{id}', 'OnboardingController@checkBrowser')->name('onboarding.checkBrowser');
Route::any('/speed/check/{id}', 'OnboardingController@speedCheck')->name('onboarding.speedCheck');

Route::any('/users/finalize/{id}', 'OnboardingController@finalize')->name('onboarding.finalize');

Route::any('/users/setPassword/{id}/{password}', 'OnboardingController@setPassword')->name('onboarding.setPassword');

Route::any('/{table}/get/{id?}', 'Base\\ApiController@get')->where(
    [
        'table' => 'terms_and_conditions'
    ]
)->name('onboarding.getTermsAndConditions');
