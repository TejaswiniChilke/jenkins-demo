<?php
/*
|--------------------------------------------------------------------------
| WebSocket Routes
|--------------------------------------------------------------------------
|
| Here is where you can register WebSocket routes for your application.
| Based for "Symfony Routing Component".
|
| Example: $socket->route('/myclass', new MyClass, ['*']);
|
*/
Route::any('/chat/{action}', 'ChatController@socket');

Route::any('/earned-badge/join/{activeUserId}', 'EarnedBadgeController@join');

Route::any('/notification-count/join/{activeUserId}', 'NotificationCountController@join');

