<?php
/*
|--------------------------------------------------------------------------
| Messaging Routes
|--------------------------------------------------------------------------
*/
Route::post('/create', 'MessagingController@store');
Route::post('/reply/{id}', 'MessagingController@update');
Route::get('/get/{id?}', 'MessagingController@index');
Route::get('/sent_messages/{id?}', 'MessagingController@sentMessages');
