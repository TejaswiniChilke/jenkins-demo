<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::any('/active_user/get', 'OAuthController@loadActiveUser');

Route::get('/address/validate', 'UspsController@validateAddress')->name('usps.validate');

Route::get('/applicants/get', 'UsersController@getApplicants')->name('api.applicants.get');

Route::any('/badges/markRead', 'BadgesController@markRead')->name('api.badges.markRead');

Route::any('/cache/flush', 'CacheController@flush')->name('api.cache.flush');

Route::get('/charts', 'ReportController@index');

Route::post('/emails/send', 'R3EmailsController@sendEmail')->name('api.emails.send');
Route::get('/emails/accept/{id?}', 'R3EmailsController@sendAcceptEmail')->name('api.emails.sendAccept');

Route::any('/friends/add/{id?}', 'FriendsController@addFriend');
Route::any('/friends/cancel/{id?}', 'FriendsController@cancelFriendRequest');
Route::any('/friends/confirm/{id?}', 'FriendsController@confirmFriend');
Route::any('/friends/decline/{id?}', 'FriendsController@declineFriendRequest');
Route::get('/friends/get/{id?}', 'FriendsController@getFriends');
Route::any('/friends/remove/{id?}', 'FriendsController@remove');
Route::any('/friends/unfollow/{id?}', 'FriendsController@unfollow');

Route::get('/friend_requests/get/{id?}', 'FriendsController@getFriendRequests');

Route::get('/posts/newsfeed', 'PostsController@getNewsfeed')->name('api.newsfeed');
Route::get('/reports/{name}', 'ReportController@getReport')->name('api.report');

Route::get('/settings/get', 'SettingsController@getSettings')->name('api.settings.get');
Route::post('/settings/update', 'SettingsController@updateSettings')->name('api.settings.update');

Route::get('/survey/complete-profile/{userId?}', 'R3SurveyController@getCompleteProfileSurvey')->name('api.survey.get');

Route::get('/survey/get/{id?}', 'Surveys\\SurveyController@getSurvey')->name('api.survey.get');
Route::get('/survey/page/get/{id}', 'Surveys\\SurveyController@getSurveyPage')->name('api.survey.page.get');
Route::get('/survey/page/next/{id}', 'Surveys\\SurveyController@getNextPage')->name('api.survey.page.next');
Route::get('/survey/page/prev/{id}', 'Surveys\\SurveyController@getPrevPage')->name('api.survey.page.prev');
Route::post('/survey/save/{id}', 'Surveys\\SurveyController@saveSurvey')->name('api.survey.save');

Route::post('/sessions/create', 'SessionsController@create')->name('api.sessions.create');

Route::get('/tags/users/{id?}', 'Base\\ApiController@getUserTags')->name('api.tags');

Route::any('/text_messages/send', 'Communication\\TextsController@sendSms')->name('api.text.send');

Route::get('/users/active', 'UsersController@orderByActivity')->name('api.users.orderByActivity');

Route::post('/user_statuses/create', 'UserStatusesController@create')->name('api.userStatuses.create');
Route::post('/user_statuses/delete/{id?}', 'UserStatusesController@delete')->name('api.userStatuses.delete');
Route::post('/user_statuses/edit/{id?}', 'UserStatusesController@edit')->name('api.userStatuses.edit');
Route::any('/user_statuses/get/{id?}', 'UserStatusesController@get')->name('api.userStatuses.get');

Route::post('/user_types/{id}/add/badges', 'UserTypesController@addBadges')->name('api.addBadgesToUserType');
Route::post('/user_types/notify/{user_type}', 'UserTypesController@notify')->name('api.notifyUserTypes');

Route::get('/youtube/video/get/{id}', 'YouTubeController@getVideoInfo')->name('youtube.getVideoInfo');

Route::any('/videos/getByPlaylistTags', 'PlaylistsController@getByPlaylistTags')->name('api.videos.getByTag');
Route::any('/videos/getByPlaylistWIthInstructorTag', 'PlaylistsController@getByPlaylistWIthInstructorTag')->name('api.videos.instructor');

Route::any('/{table}/columns', 'Base\\ApiController@columns')->name('api.columns');
Route::any('/{table}/comment/{id}', 'Base\\ApiController@createComment')->name('api.comment.create');
Route::any('/{table}/comments/{id}', 'Base\\ApiController@getComments')->name('api.comment.get');
Route::any('/{table}/copy/{id}', 'Base\\ApiController@copy')->name('api.copy');
Route::get('/{table}/count', 'Base\\ApiController@count')->name('api.count');
Route::post('/{table}/create', 'Base\\ApiController@create')->name('api.create');
Route::any('/{table}/delete/{id?}', 'Base\\ApiController@delete')->name('api.delete');
Route::post('/{table}/edit/{id?}', 'Base\\ApiController@edit')->name('api.edit');
Route::get('/{table}/exists/{id?}', 'Base\\ApiController@exists')->name('api.exists');
Route::get('/{table}/export/{id?}', 'Base\\ApiController@export')->name('api.export');
Route::any('/{table}/findOrCreate', 'Base\\ApiController@findOrCreate')->name('api.findOrCreate');
Route::get('/{table}/get/{id?}', 'Base\\ApiController@get')->name('api.get');
Route::any('/{table}/getByTag/{id?}', 'Base\\ApiController@getByTag')->name('api.getByTag');
Route::any('/{table}/matching/{id?}', 'Base\\ApiController@matching')->name('api.matching');
Route::any('/{table}/exact/matching/{id?}', 'Base\\ApiController@exactMatching')->name('api.exactMatching');
Route::any('/{table}/like/{id?}', 'Base\\ApiController@like')->name('api.like');
Route::get('/{table}/similar/get/{id?}', 'Base\\ApiController@similar')->name('api.similar');
Route::get('/{table}/similar/exists/{id}', 'Base\\ApiController@similarExists')->name('api.similarExists');
Route::any('/{table}/unlike/{id}', 'Base\\ApiController@unlike')->name('api.unlike');

Route::get('/{table}/within/{unit}/{distance}', 'Base\\ApiController@within')->where(
    [
        'unit'     => 'km|mi',
        'distance' => '[0-9]+'
    ]
)->name('api.within');

Route::any('/{parentTable}/{parentId}/{action}/{childTable}/{childId?}', 'Base\\ApiController@joint')->name('api.joint');
