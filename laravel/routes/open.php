<?php
/*
|--------------------------------------------------------------------------
| Open Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "open" middleware group. Enjoy building your API!
|
*/
Route::get('/color_templates/get', 'ColorTemplatesController@getRequest')->name('open.colorTemplates');

Route::get('/lingos/get', 'LingosController@getRequest')->name('open.lingos');

Route::get('/images/{filename}', 'UploadController@getFile')->name('open.get.image');

Route::any('/confirm/email/{id}', 'UsersController@confirmEmail')->name('open.confirm.email');
Route::any('/confirm/resend/{id}', 'OnboardingController@resend')->name('open.confirm.resend');

Route::any('/cache/flush/{secret}', 'CacheController@flush')->name('open.flush');

Route::get('/emails/check/{id}', 'Communication\\EmailsController@checkEmail');

Route::any('/settings/get', 'SettingsController@getSettings')->name('open.settings.get');

Route::get('/reports/playlist_adherence_rate', 'PlaylistsController@getAdherenceRate')->name('open.adherenceRate');

Route::any('/text_messages/error', 'Communication\\TextsController@receiveError')->name('api.text.error');
Route::any('/text_messages/receive', 'Communication\\TextsController@receiveSms')->name('api.text.receive');
Route::any('/text_messages/status/{id?}', 'Communication\\TextsController@receiveError')->name('api.text.error');
Route::any('/text_messages/status/{id?}', 'Communication\\TextsController@receiveStatus')->name('api.text.status');

Route::post('/images/upload', 'UploadController@upload')->name('open.upload');

Route::post('/wysiwyg/upload', 'UploadController@wysiwygUpload')->name('open.wysiwygUpload');

Route::post('/{table}/edit/{id}', 'Base\\ApiController@edit')->where(
    [
        'table' => 'users',
    ]
)->name('open.edit');

Route::get('/{table}/get/{id}', 'Base\\ApiController@get')->where(
    [
        'table' => 'security_questions|users',
    ]
)->name('open.getById');

Route::any('/{table}/get', 'Base\\ApiController@get')->where(
    [
        'table' => 'security_questions|states',
    ]
)->name('open.get');

Route::post('/user/validate_address', 'UspsController@validateUserAddress')->name('usps.validateUser');

Route::get('/videos/{filename}', 'UploadController@getVideo')->name('open.get.video');

Route::get('/{model}/id/{slug?}', 'TestController@id')->name('test.id');
Route::get('/{model}/slug/{id?}', 'TestController@slug')->name('test.slug');
