<?php
namespace Uab\Console;

use \Illuminate\Foundation\Application as ParentApplication;

class Application extends ParentApplication {

    /**
     * this is the default for the application path
     */
    protected $appBasePath = 'app';

    public function __construct($basePath = null) {
        parent::__construct($basePath);

        $this->registerBaseBindings();

        $this->registerBaseServiceProviders();

        $this->registerCoreContainerAliases();

        if ($basePath) {
            $this->setBasePath($basePath);
        }
    }

    public function setAppPath($path) {
        $this->appBasePath = $path;

        return app()->__set('path', $this->path());
    }

    /**
     * Get the path to the application "app" directory.
     *
     * @param string $path
     *
     * @return string
     */
    public function path($path = '') {
        return $this->basePath.DIRECTORY_SEPARATOR.$this->appBasePath;
    }
}
