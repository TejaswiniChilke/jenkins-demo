<?php
namespace Uab\Console;

use Uab\Console\Commands\Badges\CheckMissedBadges;
use Uab\Console\Commands\Clean\CleanImages;
use Uab\Console\Commands\Email\ResendSetPasswordEmail;
use Uab\Console\Commands\Make\Automation\MakeAutomation;
use Uab\Console\Commands\Make\Factories\MakeFactory;
use Uab\Console\Commands\Run\Build\BuildLocal;
use Uab\Console\Commands\Run\Build\BuildServer;
use Uab\Console\Commands\Make\Migrations\MakeMigrationCreate;
use Uab\Console\Commands\Make\Migrations\MakeMigrations;
use Uab\Console\Commands\Queue\CountQueue;
use Uab\Console\Commands\Notify\SendWeeklyReminders;
use Uab\Console\Commands\Make\Controllers\MakeControllerGenerated;
use Uab\Console\Commands\Make\Controllers\MakeControllers;
use Uab\Console\Commands\Make\Events\MakeAfterCreate;
use Uab\Console\Commands\Make\Events\MakeAfterEdit;
use Uab\Console\Commands\Make\MakeAll;
use Uab\Console\Commands\Make\Controllers\MakeControllerEmpty;
use Uab\Console\Commands\Make\Models\MakeModelEmpty;
use Uab\Console\Commands\Make\Models\MakeModelGenerated;
use Uab\Console\Commands\Make\Models\MakeModels;
use Uab\Console\Commands\Make\Views\MakeViewCreate;
use Uab\Console\Commands\Make\Views\MakeViewEdit;
use Uab\Console\Commands\Make\Views\MakeViewIndex;
use Uab\Console\Commands\Make\Views\MakeViews;
use Uab\Console\Commands\Make\Views\MakeViewShow;
use Uab\Console\Commands\Notify\NotifyNotLoggingIn;
use Uab\Console\Commands\Notify\NotifyUnactivatedAccounts;
use Uab\Console\Commands\Queue\TestQueue;
use Uab\Console\Commands\Queue\TrackQueue;
use Uab\Console\Commands\R3\R3ServerQualityCheck;
use Uab\Console\Commands\Run\Services\RunBuild;
use Uab\Console\Commands\Run\Services\RunDeploy;
use Uab\Console\Commands\Run\Services\RunInstall;
use Uab\Console\Commands\Run\Services\RunServices;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Uab\Console\Commands\Run\Services\RunUpdate;
use Uab\Console\Commands\Updates\UpdateVideoImages;
use Uab\Console\Commands\Updates\UpdateYouTubeInfo;

class Kernel extends ConsoleKernel{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        CheckMissedBadges::class,
        CleanImages::class,
        CountQueue::class,
        BuildLocal::class,
        BuildServer::class,
        MakeAfterCreate::class,
        MakeAfterEdit::class,
        MakeAll::class,
        MakeAutomation::class,
        MakeControllers::class,
        MakeControllerEmpty::class,
        MakeControllerGenerated::class,
        MakeFactory::class,
        MakeMigrationCreate::class,
        MakeMigrations::class,
        MakeModels::class,
        MakeModelEmpty::class,
        MakeModelGenerated::class,
        MakeViewCreate::class,
        MakeViewEdit::class,
        MakeViewIndex::class,
        MakeViewShow::class,
        MakeViews::class,
        NotifyNotLoggingIn::class,
        NotifyUnactivatedAccounts::class,
        R3ServerQualityCheck::class,
        ResendSetPasswordEmail::class,
        RunBuild::class,
        RunDeploy::class,
        RunInstall::class,
        RunServices::class,
        RunUpdate::class,
        SendWeeklyReminders::class,
        TestQueue::class,
        TrackQueue::class,
        UpdateVideoImages::class,
        UpdateYouTubeInfo::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  Schedule  $schedule
     *
     * @return void
     */
    protected function schedule(Schedule $schedule) {
        // Base
        $schedule->command('run:quality')->everyFiveMinutes();
        $schedule->command('run:r3-quality')->hourly();

        $schedule->command('backup:run')->daily();
        $schedule->command('backup:clean')->daily();
        $schedule->command('backup:monitor')->daily();

        $schedule->command('clean:images')->dailyAt('1:00');

        $schedule->command('queue:retry all')->dailyAt('2:00');
        $schedule->command('queue:flush')->dailyAt('3:00');

        $schedule->command('auth:clear-expired')->dailyAt('4:00');

        // R3
        $schedule->command('email:resend-set-password')->daily();

        $schedule->command('update:video-images')->daily();
        $schedule->command('update:youtube-info')->daily();

        // $schedule->command('notify:not-logging-in')->daily();
        $schedule->command('notify:unactivated-accounts')->daily();

        $schedule->command('send:r3-reminders')->dailyAt('1:00');

        $schedule->command('badges:missed')->hourlyAt(30);
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands() {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
