<?php
namespace Uab\Console\Commands\Email;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Uab\Http\Enums\UserStatusesEnum;
use Uab\Http\Enums\UserTypesEnum;
use Uab\Http\Models\Emails;
use Uab\Providers\R3\R3EmailServiceProvider;
use Uab\Providers\UsersServiceProvider;

class ResendSetPasswordEmail extends Command {
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'email:resend-set-password';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Automatically resend set password emails.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle() {
        $emailService = new R3EmailServiceProvider();
        $userService = new UsersServiceProvider();

        $userTypes = [
            UserTypesEnum::CONTROL,
            UserTypesEnum::M2M,
            UserTypesEnum::SET,
        ];

        foreach ($userTypes as $userType) {
            $users = $userService->getUsersByUserTypeNames($userType);

            foreach ($users as $user) {
                if ($user->status === UserStatusesEnum::APPLICATION_APPROVED && (!isset($user->password) || strlen($user->password) === 0)) {
                    $subject = 'Congratulations! We would like to invite you as a new SCIPE user';

                    $query = Emails::query()
                        ->where(
                            [
                                'subject'    => $subject,
                                'to_user_id' => $user->id
                            ]
                        );

                    $count = $query->count();

                    $resendMax = config('email.approved.resend', 3);

                    if ($count < $resendMax) {
                        $days = config('email.approved.resend.days', 1);

                        $query->where('created', '<=', Carbon::now()->subDays($days));

                        if (!$query->exists()) {
                            $this->info('Resending email to '.$user->email.' for the '.($count + 1).' time.');

                            $emailService->sendAcceptedEmail($user);
                        }
                    }
                }
            }
        }
    }
}
