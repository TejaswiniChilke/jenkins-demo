<?php
namespace Uab\Console\Commands\Email;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Uab\Http\Models\EmailTemplates;
use Uab\Http\Models\UserTypes;
use Uab\Jobs\Emails\SendEmailTemplate;
use Uab\Providers\UsersServiceProvider;

class EmailWeeklyTemplates extends Command {
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'email:weekly-templates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send weekly emails based on user type to participants.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $service = new UsersServiceProvider();

        /** @var UserTypes[] $userTypes */
        $userTypes = UserTypes::get();

        foreach ($userTypes as $userType) {
            $users = $service->getUsersByUserTypeNames($userType);

            foreach ($users as $user) {
                /** @var EmailTemplates[] $templates */
                $templates = EmailTemplates::where(
                    [
                        'user_type_id' => $user->user_type_id,
                        'week'         => $user->getWeek()
                    ]
                )->get();

                foreach ($templates as $template) {
                    if ($user->preferred_contact_time < Carbon::now()) {
                        SendEmailTemplate::dispatch($template, $user)->onQueue(QueueTypes::LOW);
                    }
                }
            }
        }
    }
}
