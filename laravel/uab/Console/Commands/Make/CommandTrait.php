<?php
namespace Uab\Console\Commands\Make;

use DB;
use Illuminate\Support\Str;

trait CommandTrait {
    protected function getNamespace($name) {
        return $this->getLaravel()->getNamespace();
    }

    protected function getModels($name = 'all') {
        $models = [];

        if ($name === 'all') {
            $models = DB::select(
                'SELECT TABLE_NAME AS name' .
                '  FROM information_schema.tables ' .
                ' WHERE table_schema = DATABASE()'
            );

            foreach ($models as &$model) {
                $model = $model->name;
            }
        } else {
            $names = explode(',', $name);

            foreach ($names as $name) {
                $models[] = $name;
            }
        }

        return $models;
    }

    protected function _getTable($name) {
        $table = explode('\\', $name);
        $table = end($table);
        $table = Str::snake($table);
        $table = strtolower($table);

        return $table;
    }
}
