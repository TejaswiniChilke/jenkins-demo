<?php
namespace Uab\Console\Commands\Make;

use Artisan;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputOption;

class MakeAll extends GeneratorCommand {
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'make:all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new model, controller, and view class';

    /**
     * Build the model class with the given name.
     *
     * @param  string $name
     */
    protected function buildClass($name) {
        $name = Str::replaceFirst($this->rootNamespace(), '', $name);

        $models = $this->getModels($name);

        $bar = $this->output->createProgressBar(count($models) * 6);

        foreach ($models as $model) {
            $commands = [];

            $localOptions =  [
                'name' => Str::snake($model)
            ];

            if ($this->option('force') !== false) {
                $localOptions['--force'] = 'true';
            }

            $commands['make:automation'] = $localOptions;
            $commands['make:controllers'] = $localOptions;
            $commands['make:events'] = $localOptions;
            $commands['make:factory'] = $localOptions;
            $commands['make:models'] = $localOptions;
            $commands['make:views'] = $localOptions;

            foreach ($commands as $command => $args) {
                Artisan::call($command, $args);

                $bar->advance();
            }
        }

        $bar->finish();
    }

    protected function getStub() {
        return false;
    }
}
