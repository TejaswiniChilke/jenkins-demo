<?php
namespace Uab\Console\Commands\Make\Automation;

use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Uab\Console\Commands\Make\GeneratorCommand;

class MakeAutomation extends GeneratorCommand {
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'make:automation';

    protected $signature = 'make:automation {name} {--f|force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create automation tests for model class';

    /**
     * Build the model class with the given name.
     *
     * @param  string $name
     *
     * @return string
     *
     * @throws FileNotFoundException
     */
    protected function buildClass($name) {
        $this->info('Generating automation tests for \''.$name.'\'.');

        $stub = parent::buildClass($name);

        $this->replaceTable($stub);
        $stub = $this->_replaceInStub($stub, 'DummyModel', $this->qualifyClass($name));

        return $stub;
    }

    protected function getClass($name):string {
        return parent::getClass($name) . 'Tests';
    }

    /**
     * @param string $name
     *
     * @return string
     */
    protected function getPath($name):string {
        return $this->laravel['path'].'/../tests/Api/Generated/'.$this->getClass($name).'.php';
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub():string {
        return __DIR__ . '/Stubs/automation.stub';
    }

    /**
     * @param string $stub
     * @param string $name
     *
     * @return $this
     */
    protected function replaceNamespace(&$stub, $name):MakeAutomation {
        $namespace = 'Tests\\Api\\Generated';

        $stub = $this->_replaceInStub($stub, 'DummyNamespace', $namespace);

        return $this;
    }

    /**
     * Replace the table for the given stub.
     *
     * @param string $stub
     *
     * @return MakeAutomation
     */
    protected function replaceTable(string &$stub):MakeAutomation {
        $stub = str_replace('{{DummyTable}}', $this->table, $stub);

        return $this;
    }

    /**
     * Get the root namespace for the class.
     *
     * @return string
     */
    protected function rootNamespace() {
        return 'Tests\\Api\\Generated';
    }

    protected function getNamespace($name = ''):string {
        return $this->rootNamespace();
    }
}
