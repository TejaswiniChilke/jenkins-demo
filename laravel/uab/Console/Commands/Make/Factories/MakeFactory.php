<?php
namespace Uab\Console\Commands\Make\Factories;

use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Str;
use Uab\Console\Commands\Make\GeneratorCommand;

class MakeFactory extends GeneratorCommand {
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'make:factory';

    protected $signature = 'make:factory {name} {--f|force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new factory';

    /**
     * Build the model class with the given name.
     *
     * @param  string $name
     *
     * @return string
     *
     * @throws FileNotFoundException
     */
    protected function buildClass($name):string {
        $this->info('Generating factory for \''.$name.'\'.');

        $stub = parent::buildClass($name);

        $this->replaceTable($stub);
        $this->replaceRelationshipUses($stub);
        $stub = $this->_replaceInStub($stub, 'DummyModel', $this->getClass($name));

        $this->replaceDefinitions($stub);

        return $stub;
    }

    /**
     * @param string $name
     *
     * @return string
     */
    protected function getPath($name):string {
        return str_replace('laravel/uab', 'laravel', $this->laravel['path']).'/database/factories/'.$this->getClass($name).'Factory.php';
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub():string {
        return __DIR__ . '/Stubs/factory.stub';
    }

    /**
     * Replace the dates for the given stub.
     *
     * @param string  $stub
     *
     * @return $this
     */
    protected function replaceDates(string &$stub):self {
        $columns = $this->_getDates($this->table);

        $stub = $this->_replaceColumns($stub, 'dates', $columns);

        return $this;
    }

    /**
     * Replace the table for the given stub.
     *
     * @param string $stub
     *
     * @return $this
     */
    protected function replaceTable(string &$stub):self {
        $stub = str_replace('{{table}}', $this->table, $stub);

        return $this;
    }

    /**
     * @param string $table
     * @param bool|null $addQuotes
     *
     * @return array
     */
    protected function getRelationships(string $table, ?bool $addQuotes = false):array {
        $include = [];

        $columns = $this->_getColumns($table);
        foreach ($columns as $column) {
            $matches = preg_match('/_id$/', $column);
            if ($matches !== 0) {
                $otherTable = substr($column, 0, -3);
                $otherTable = Str::plural($otherTable);

                if ($addQuotes) {
                    $otherTable = $this->_addQuotes($otherTable);
                }

                $include[$otherTable] = $column;
            }
        }

        return $include;
    }

    /**
     * @param string $stub
     *
     * @return $this
     */
    protected function replaceDefinitions(string &$stub):self {
        $include = $this->getRelationships($this->table, false);

        $definitions = '';
        foreach ($include as $table => $column) {
            $model = Str::ucfirst(Str::studly($table));

            $definitions .= "\t\t'{$column}' => factory({$model}::class)->make(),\n";
        }

        $stub = $this->_replaceInStub($stub, 'definitions', $definitions);

        return $this;
    }

    /**
     * @param string $stub
     *
     * @return $this
     */
    protected function replaceRelationshipUses(string &$stub):self {
        $include = $this->getRelationships($this->table, false);

        $uses = '';
        foreach ($include as $table => $column) {
            $model = Str::ucfirst(Str::studly($table));

            $uses .= "use Uab\\Http\\Models\\".$model.";\n";
        }

        $stub = $this->_replaceInStub($stub, 'usesRelationships', $uses);

        return $this;
    }
}
