<?php
namespace Uab\Console\Commands\Make\Events;

use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Str;
use Uab\Console\Commands\Make\GeneratorCommand;

class MakeAfterEdit extends GeneratorCommand {
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'make:after-edit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new after edit class for the model.';

    /**
     * Build the model class with the given name.
     *
     * @param  string $name
     *
     * @return string
     *
     * @throws FileNotFoundException
     */
    protected function buildClass($name):string {
        $this->info('Generating after edit event for \''.$name.'\'.');

        $stub = $this->files->get($this->getStub());

        $this->replaceNamespace($stub, $name);

        $stub = $this->replaceClass($stub, $name);

        return $stub;
    }

    protected function getClass($name):string {
        return parent::getClass($name).'AfterEdit';
    }

    protected function getPath($name):string {
        $name = Str::plural(Str::replaceFirst($this->rootNamespace(), '', $name));

        $camelCase = ucfirst(Str::camel($name));

        return $this->laravel['path'].'/Events/AfterEdit/'.$camelCase.'AfterEdit.php';
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub() {
        return __DIR__ . '/Stubs/after-edit.stub';
    }
}
