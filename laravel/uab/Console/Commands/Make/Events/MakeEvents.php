<?php
namespace Uab\Console\Commands\Make\Events;

use Artisan;
use Illuminate\Support\Str;
use Uab\Console\Commands\Make\Command;

class MakeEvents extends Command {
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'make:events';

    protected $signature = 'make:events {name} {--f|force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create event files.';

    /**
     * Build the model class with the given name.
     */
    public function handle() {
        $name = $this->argument('name');

        parent::buildClass($name);

        $this->info('Generating events for \''.$name.'\'.');

        $models = $this->getModels($name);

        $bar = $this->output->createProgressBar(count($models) * 2);

        $options = [];
        if ($this->input->hasOption('force')) {
            $options['--force'] = true;
        }

        foreach ($models as $model) {
            $commands = [];

            $localOptions =  array_merge(
                [
                    'name' => Str::studly($model)
                ],
                $options
            );

            $commands['make:after-create'] = $localOptions;
            $commands['make:after-edit'] = $localOptions;

            foreach ($commands as $command => $args) {
                Artisan::call($command, $args);

                $bar->advance();
            }
        }

        $bar->finish();
    }
}
