<?php
namespace Uab\Console\Commands\Make\Events;

use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Str;
use Uab\Console\Commands\Make\GeneratorCommand;

class MakeAfterCreate extends GeneratorCommand {
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'make:after-create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new after create class for the model.';

    /**
     * Build the model class with the given name.
     *
     * @param  string $name
     *
     * @return string
     *
     * @throws FileNotFoundException
     */
    protected function buildClass($name):string {
        $this->info('Generating after create event for \''.$name.'\'.');

        $stub = $this->files->get($this->getStub());

        $this->replaceNamespace($stub, $name);

        $stub = $this->replaceClass($stub, $name);

        return $stub;
    }

    protected function getClass($name):string {
        return parent::getClass($name).'AfterCreate';
    }

    protected function getPath($name) {
        $name = Str::plural(Str::replaceFirst($this->rootNamespace(), '', $name));

        $camelCase = ucfirst(Str::camel($name));

        return $this->laravel['path'].'/Events/AfterCreate/'.$camelCase.'AfterCreate.php';
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub() {
        return __DIR__ . '/Stubs/after-create.stub';
    }

    /**
     * Replace the attributes for the given stub.
     *
     * @param string $stub
     * @param string $table
     *
     * @return $this
     */
    protected function replaceAttributes(&$stub, $table) {
        $columns = $this->_getColumnDefaults($table);

        $attributes = [];
        foreach ($columns as $column => $value) {
            if (is_null($value)) {
                $value = 'null';
            } else if (!is_numeric($value)) {
                $value = $this->_addQuotes($value);
            }

            $attributes[] = '        '.$this->_addQuotes($column).' => '.$value;
        }

        $attributes = implode(",\n", $attributes);

        $stub = $this->_replaceInStub($stub, 'attributes', $attributes);

        return $this;
    }

    /**
     * Replace the dates for the given stub.
     *
     * @param  string  $stub
     * @param string   $table
     *
     * @return $this
     */
    protected function replaceDates(&$stub, $table) {
        $columns = $this->_getDates($table);

        $stub = $this->_replaceColumns($stub, 'dates', $columns);

        return $this;
    }

    protected function replaceNamespace(&$stub, $name) {
        $namespace = $this->getLaravel()->getNamespace() . 'Events\\AfterCreate';

        $stub = $this->_replaceInStub($stub, 'DummyNamespace', $namespace);

        return $this;
    }
}
