<?php
namespace Uab\Console\Commands\Make;

use DB;
use Schema;
use Illuminate\Support\Str;

trait MakeCommandTrait {
    private $columns = [];

    protected function _addQuotes($columns) {
        if (is_array($columns)) {
            array_walk(
                $columns,
                function (&$value) {
                    $value = $this->_addQuotes($value);
                }
            );
        } else {
            $columns = "'" . $columns . "'";
        }

        return $columns;
    }

    protected function _excludeColumns($columns, $exclude) {
        return array_filter(
            $columns,
            function($value) use ($exclude) {
                return !in_array($value, $exclude);
            }
        );
    }

    protected function _getColumns($table, $exclude = []) {
        if (!array_key_exists($table, $this->columns)) {
            $columns = Schema::getColumnListing($table);

            $this->columns[$table] = $columns;
        }

        return $this->_excludeColumns($this->columns[$table], $exclude);
    }

    protected function _getDates($table) {
        $columns = $this->_getColumns($table);

        $exclude= [];
        foreach($columns as $column) {
            $type = strtolower(Schema::getColumnType($table, $column));

            if ($type !== 'date' && $type !== 'datetime') {
                $exclude[] = $column;
            }
        }

        return $this->_getColumns($table, $exclude);
    }

    protected function _getFillables($table, $exclude = []) {
        $exclude = array_merge(
            $exclude,
            [
                'id',
                'created',
                'modified'
            ]
        );

        return $this->_getColumns($table, $exclude);
    }

    protected function _getColumnDetails($table) {
        $returnVar = [];

        $details = DB::select('
            SELECT *
              FROM `information_schema`.`columns`
             WHERE `table_name` = ?
               AND `table_schema` = ?
            ',
            [
                $table,
                config('database.connections.mysql.database')
            ]
        );

        foreach ($details as $detail) {
            if ($this->_hasColumn($table, $detail->COLUMN_NAME)) {
                $returnVar[] = $detail;
            }
        }

        return collect($returnVar);
    }

    protected function _getColumnDefaults($table) {
        $results = $this->_getColumnDetails($table);

        $columns = $results->pluck('COLUMN_NAME');
        $defaults = $results->pluck('COLUMN_DEFAULT');
        $isNullables = $results->pluck('IS_NULLABLE');

        $results = [];

        foreach ($columns as $i => $column) {
            $value = $defaults[$i];

            if (!is_null($value) || strtolower($isNullables[$i]) === 'yes') {
                $results[$column] = $value;
            }
        }

        return $results;
    }

    protected function _getColumnTypes($table) {
        $results = $this->_getColumnDetails($table);

        $columns = $results->pluck('COLUMN_NAME');
        $types = $results->pluck('DATA_TYPE');

        $columnsWithTypes = [];

        foreach ($columns as $i => $column) {
            $columnsWithTypes[$column] = $this->_toPhpType($types[$i]);
        }

        return $columnsWithTypes;
    }

    protected function _getColumnCasts($table) {
        $results = $this->_getColumnDetails($table);

        $columns = $results->pluck('COLUMN_NAME');
        $types = $results->pluck('DATA_TYPE');

        $columnsWithCasts = [];

        foreach ($columns as $i => $column) {
            $columnsWithCasts[$column] = $this->_toMysqlTypes($types[$i]);
        }

        return $columnsWithCasts;
    }

    protected function _hasColumn($table, $candidateColumn) {
        $columns = $this->_getColumns($table);
        foreach ($columns as $column) {
            if ($column == $candidateColumn) {
                return true;
            }
        }

        return false;
    }

    protected function _toPhpType($mysqlType) {
        $mysqlType = strtolower($mysqlType);

        switch ($mysqlType) {
            case 'decimal':
            case 'int':
                return 'number';
            case 'tinyint':
                return 'boolean';
            case 'datetime':
            case 'string':
            case 'text':
            case 'varchar':
                return 'string';
            default:
                return 'mixed';
        }
    }

    protected function _toMysqlTypes($mysqlType) {
        $mysqlType = strtolower($mysqlType);

        switch ($mysqlType) {
            case 'int':
                return 'integer';
            case 'decimal':
                return 'decimal:12';
            case 'tinyint':
                return 'boolean';
            case 'date':
            case 'datetime':
            case 'timestamp':
                return $mysqlType;
            case 'string':
            case 'text':
            case 'varchar':
            default:
                return 'string';
        }
    }

    protected function _humanize($table) {
        return Str::title(str_replace('_', ' ', $table));
    }

    /**
     * Replace a {{$key}} in $stub with columns from $table excluding $exclude
     *
     * @param string   $stub
     * @param string   $key
     * @param string[] $columns
     *
     * @return $this
     */
    protected function _replaceColumns(&$stub, $key, $columns) {
        foreach ($columns as &$column) {
            $column = $this->_addQuotes($column);
            $column = "\t\t".$column;
        }

        $columns = implode(",\n", $columns);
        $columns = "\n".$columns."\n\t";

        return $this->_replaceInStub($stub, $key, $columns);
    }

    protected function _replaceInStub($stub, $key, $value) {
        return str_replace(
            '{{'.$key.'}}',
            isset($value) ? $value : '',
            $stub
        );
    }
}
