<?php
namespace Uab\Console\Commands\Make;

use Illuminate\Console\Command as ParentCommand;
use Symfony\Component\Console\Input\InputOption;

abstract class Command extends ParentCommand {
    use CommandTrait;

    protected $table;
    protected $type;

    /**
     * Build the model class with the given name.
     *
     * @param string $name
     *
     * @return string
     */
    protected function buildClass($name) {
        $this->setTable($name);
        $this->setType($name);
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    public function getOptions() {
        return [
            [
                'force',
                null,
                InputOption::VALUE_NONE,
                'Force create files.',
                null
            ]
        ];
    }

    private function setTable($name) {
        $this->table = $this->_getTable($name);
    }

    private function setType($namespace) {
        $this->type = $namespace;
    }
}
