<?php
namespace Uab\Console\Commands\Make\Migrations;

use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use Uab\Console\Commands\Make\GeneratorCommand;

class MakeMigrationCreate extends GeneratorCommand {
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'make:migration-create';

    protected $signature = 'make:migration-create {name} {--f|force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Build the model class with the given name.
     *
     * @param  string $name
     *
     * @return string
     *
     * @throws FileNotFoundException
     */
    protected function buildClass($name) {
        $name = ucfirst(Str::plural(Str::replaceFirst($this->rootNamespace(), '', $name)));

        $this->info('Generating model for \''.$name.'\'.');

        $stub = parent::buildClass($name);

        $this->replaceTable($stub);
        $this->replaceColumns($stub);

        return $stub;
    }

    protected function getPath($name) {
        $name = Str::lower(Str::snake(Str::plural(Str::replaceFirst($this->rootNamespace(), '', $name))));

        return './database/migrations/' . Carbon::now()->format('Y_m_d_') . '_create_' . $name . '_table.php';
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub() {
        return __DIR__ . '/Stubs/migration-create.stub';
    }

    protected function replaceNamespace(&$stub, $name) {
        $namespace = $this->getLaravel()->getNamespace() . 'Http\\Models\\Generated';

        $stub = $this->_replaceInStub($stub, 'DummyNamespace', $namespace);

        return $this;
    }

    /**
     * Replace the table for the given stub.
     *
     * @param string $stub
     *
     * @return $this
     */
    protected function replaceTable(&$stub) {
        $stub = str_replace('{{table}}', $this->table, $stub);

        return $this;
    }

    /**
     * Replace the fillable for the given stub.
     *
     * @param  string  $stub
     *
     * @return $this
     */
    protected function replaceColumns(&$stub) {
        $code = '';

        $columns = $this->_getColumnDetails($this->table);
        foreach ($columns as $columnDetails) {
            $code .= "\t\t\t";

            $name = $this->getStdProperty($columnDetails, 'COLUMN_NAME');
            $type = $this->getStdProperty($columnDetails, 'DATA_TYPE');

            if ($name === 'id' && $type === 'int') {
                $isAutoIncrement = $this->doesStdPropertyContain($columnDetails, 'EXTRA', 'auto_increment');
                if ($isAutoIncrement) {
                    $code .= '$table->bigIncrements(\'id\')';
                } else {
                    $code .= '$table->integer(\'id\')';
                }
            } else if ($type === 'char' || $type === 'varchar') {
                $length = $this->getStdProperty($columnDetails, 'CHARACTER_MAXIMUM_LENGTH');
                if (is_null($length)) {
                    $length = 255;
                }

                $code .= '$table->char(\''.$name.'\', '.$length.')';
            } else if ($type === 'date') {
                $code .= '$table->date(\''.$name.'\')';
            } else if ($type === 'datetime') {
                $code .= '$table->dateTime(\''.$name.'\')';
            } else if ($type === 'int') {
                $length = $this->getStdProperty($columnDetails, 'CHARACTER_MAXIMUM_LENGTH');
                if (is_null($length)) {
                    $length = 255;
                }

                $code .= '$table->integer(\''.$name.'\', '.$length.')';
            } else if ($type === 'text') {
                $code .= '$table->text(\''.$name.'\')';
            } else if ($type === 'time') {
                $code .= '$table->time(\''.$name.'\')';
            } else if ($type === 'timestamp') {
                $code .= '$table->timestamp(\''.$name.'\')';
            } else if ($type === 'tinyint') {
                $code .= '$table->boolean(\''.$name.'\')';
            }

            $isNullable = $this->getStdProperty($columnDetails, 'IS_NULLABLE') !== 'NO';
            if ($isNullable) {
                $code .= '->nullable()';
            }

            $default = $this->getStdProperty($columnDetails, 'COLUMN_DEFAULT');
            if (!is_null($default) || $isNullable && is_null($default)) {
                $code .= '->default(\''.$default.'\')';
            }

            $code .= ";\n";
        }

        $stub = $this->_replaceInStub($stub, 'columns', $code);

        return $this;
    }

    private function doesStdPropertyContain($std, $property, $needle) {
        $contains = false;

        $value = $this->getStdProperty($std, $property);
        if (!is_null($value)) {
            if (is_string($value)) {
                $contains = str_contains($value, $needle);
            }
        }

        return $contains;
    }

    private function getStdProperty($std, $property) {
        $value = null;
        if (isset($std->$property)) {
            $value = $std->$property;
        }

        return $value;
    }
}
