<?php
namespace Uab\Console\Commands\Make\Migrations;

use Artisan;
use Illuminate\Support\Str;
use Uab\Console\Commands\Make\Command;

class MakeMigrations extends Command {
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'make:migrations';

    protected $signature = 'make:migrations {name} {--f|force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Build the model class with the given name.
     */
    public function handle() {
        $name = $this->argument('name');

        parent::buildClass($name);

        $this->info('Generating models for \''.$name.'\'.');

        $models = $this->getModels($name);

        $bar = $this->output->createProgressBar(count($models));

        $options = [];
        if ($this->input->hasOption('force')) {
            $options['--force'] = true;
        }

        foreach ($models as $model) {
            $commands = [];

            $localOptions =  array_merge(
                [
                    'name' => Str::studly($model)
                ],
                $options
            );

            $commands['make:migration-create'] = $localOptions;

            foreach ($commands as $command => $args) {
                Artisan::call($command, $args);

                $bar->advance();
            }
        }

        $bar->finish();
    }
}
