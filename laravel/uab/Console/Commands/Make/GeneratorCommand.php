<?php
namespace Uab\Console\Commands\Make;

use Illuminate\Console\GeneratorCommand as ParentGeneratorCommand;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputOption;

abstract class GeneratorCommand extends ParentGeneratorCommand {
    use CommandTrait;
    use MakeCommandTrait;

    protected $table;

    /**
     * Build the model class with the given name.
     *
     * @param string $name
     *
     * @return string
     *
     * @throws FileNotFoundException
     */
    protected function buildClass($name) {
        $this->setTable($name);
        $this->setType($name);

        return parent::buildClass($name);
    }

    protected function qualifyClass($name):string {
        $name = Arr::last(explode('\\', $name));

        $name = Str::ucfirst(Str::camel(Str::plural($name)));

        return $name;
    }

    protected function getClass($name):string {
        return $this->qualifyClass($name);
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    public function getOptions() {
        return [
            [
                'force',
                null,
                InputOption::VALUE_NONE,
                'Force create files.',
                null
            ]
        ];
    }

    protected function getNamespace($name = '') {
        return $this->getLaravel()->getNamespace();
    }

    private function setTable($name) {
        $this->table = $this->_getTable($name);
    }

    private function setType($name) {
        $name = Str::studly($name);

        $namespace = $this->rootNamespace() . '\\' . $name;

        $this->type = $namespace;
    }
}
