<?php
namespace Uab\Console\Commands\Make\Models;

use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Str;
use Uab\Console\Commands\Make\GeneratorCommand;

class MakeModelGenerated extends GeneratorCommand {
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'make:model-generated';

    protected $signature = 'make:model-generated {name} {--f|force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new model class';

    const HIDDEN_COLUMNS = [
        'password',
        'remember_token'
    ];

    /**
     * Build the model class with the given name.
     *
     * @param  string $name
     *
     * @return string
     *
     * @throws FileNotFoundException
     */
    protected function buildClass($name) {
        $this->info('Generating model for \''.$name.'\'.');

        $stub = parent::buildClass($name);

        $this->replaceAttributes($stub);
        $this->replaceTable($stub);
        $this->replaceColumns($stub);
        $this->replaceFillable($stub);
        $this->replaceDates($stub);
        $this->replaceCasts($stub);
        $this->replaceProperties($stub);
        $this->replaceRelationships($stub);
        $this->replaceRelationshipFunctions($stub);
        $this->replaceRelationshipUses($stub);
        $this->replaceTimestamps($stub);
        $this->replaceTraits($stub);
        $this->replaceOptional($stub);

        return $stub;
    }

    protected function replaceOptional(&$stub) {
        $optional = "\n";

        $optional = $this->replaceHiddenFields($optional, $this->table);

        $stub = $this->_replaceInStub($stub, 'optional', $optional);

        return $this;
    }

    protected function replaceHiddenFields($optional, $table) {
        $columns = $this->_getColumns($table);

        $hiddenColumns = [];
        foreach ($columns as $column) {
            if (in_array($column, self::HIDDEN_COLUMNS)) {
                $hiddenColumns[] = $column;
            }
        }

        $count = count($hiddenColumns);
        if ($count !== 0) {
            $optional .= "\tprotected \$hidden = [\n";

            foreach ($hiddenColumns as $i => $hiddenColumn) {
                $optional .= "\t\t".$this->_addQuotes($hiddenColumn);

                if ($i < $count - 1) {
                    $optional .= ",\n";
                }
            }

            $optional .= "\n\t];\n";
        }

        return $optional;
    }

    protected function getPath($name) {
        $name = Str::plural(Str::replaceFirst($this->rootNamespace(), '', $name));

        $camelCase = ucfirst(Str::camel($name));

        return $this->laravel['path'].'/Http/Models/Generated/'.$camelCase.'.php';
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub() {
        return __DIR__ . '/Stubs/model-generated.stub';
    }

    /**
     * Replace the attributes for the given stub.
     *
     * @param string $stub
     *
     * @return $this
     */
    protected function replaceAttributes(&$stub) {
        $columns = $this->_getColumnDefaults($this->table);

        $attributes = [];
        foreach ($columns as $column => $value) {
            if (is_null($value)) {
                $value = 'null';
            } else if (!is_numeric($value)) {
                $value = $this->_addQuotes($value);
            }

            $attributes[] = '        '.$this->_addQuotes($column).' => '.$value;
        }

        $attributes = implode(",\n", $attributes);

        $stub = $this->_replaceInStub($stub, 'attributes', $attributes);

        return $this;
    }

    /**
     * Replace the dates for the given stub.
     *
     * @param string  $stub
     *
     * @return $this
     */
    protected function replaceDates(string &$stub):self {
        $columns = $this->_getDates($this->table);

        $stub = $this->_replaceColumns($stub, 'dates', $columns);

        return $this;
    }

    /**
     * @param string $stub
     * @param string $name
     *
     * @return $this
     */
    protected function replaceNamespace(&$stub, $name):self {
        $namespace = $this->getNamespace($name) . 'Http\\Models\\Generated';

        $stub = $this->_replaceInStub($stub, 'DummyNamespace', $namespace);

        return $this;
    }

    /**
     * Replace the table for the given stub.
     *
     * @param string $stub
     *
     * @return $this
     */
    protected function replaceTable(string &$stub):self {
        $stub = str_replace('{{table}}', $this->table, $stub);

        return $this;
    }

    /**
     * Replace the fillable for the given stub.
     *
     * @param  string  $stub
     *
     * @return $this
     */
    protected function replaceColumns(string &$stub):self {
        $columns = $this->_getColumns($this->table);

        $stub = $this->_replaceColumns($stub, 'columns', $columns);

        return $this;
    }

    /**
     * Replace the fillable for the given stub.
     *
     * @param  string  $stub
     *
     * @return $this
     */
    protected function replaceFillable(string &$stub):self {
        $columns = $this->_getFillables($this->table);

        $stub = $this->_replaceColumns($stub, 'fillable', $columns);

        return $this;
    }

    /**
     * Replace the properties for the given stub.
     *
     * @param  string  $stub
     *
     * @return $this
     */
    protected function replaceProperties(string &$stub):self {
        $properties = [];

        $columns = $this->_getColumnTypes($this->table);

        foreach ($columns as $column => $type) {
            $properties[] = ' * @property '.$type.' '.$column;
        }

        $properties = implode("\n", $properties);

        $stub = $this->_replaceInStub($stub, 'properties', $properties);

        return $this;
    }

    /**
     * @param string $stub
     *
     * @return $this
     */
    protected function replaceCasts(string &$stub):self {
        $casts = [];

        $columns = $this->_getColumnCasts($this->table);

        foreach ($columns as $column => $type) {
            $casts[] = "\t\t".$this->_addQuotes($column).' => '.$this->_addQuotes($type);
        }

        $casts = implode(",\n", $casts);

        $stub = $this->_replaceInStub($stub, 'casts', $casts);

        return $this;
    }

    /**
     * @param string $stub
     *
     * @return $this
     */
    protected function replaceRelationships(string &$stub):self {
        $include = $this->getRelationships($this->table, true);

        $include = $this->_addQuotes($include);

        $replace = '';
        foreach ($include as $key => $value) {
            if (array_keys($include)[0] !== $key) {
                $replace .=  ",\n\t\t";
            }

            $replace .= $key.' => '.$value;
        }

        $stub = $this->_replaceInStub($stub, 'relationships', $replace);

        return $this;
    }

    /**
     * @param string $table
     * @param bool|null $addQuotes
     *
     * @return array
     */
    protected function getRelationships(string $table, ?bool $addQuotes = false):array {
        $include = [];

        $columns = $this->_getColumns($table);
        foreach ($columns as $column) {
            $matches = preg_match('/_id$/', $column);
            if ($matches !== 0) {
                $otherTable = substr($column, 0, -3);
                $otherTable = Str::plural($otherTable);

                if ($addQuotes) {
                    $otherTable = $this->_addQuotes($otherTable);
                }

                $include[$otherTable] = $column;
            }
        }

        return $include;
    }

    /**
     * @param string $stub
     *
     * @return $this
     */
    protected function replaceRelationshipFunctions(string &$stub):self {
        $include = $this->getRelationships($this->table, false);

        $functions = '';
        foreach ($include as $table => $column) {
            $model = Str::ucfirst(Str::studly($table));

            $functionName = str_replace('_id', '', $column);
            $functionName = lcFirst(Str::studly($functionName));

            $functions .= "\n\t/**\n";
            $functions .= "\t * @return ".$model."|Model|null\n";
            $functions .= "\t*/\n";
            $functions .= "\tpublic function ".$functionName."() {\n";
            $functions .= "\t\t\$id = \$this->getAttributeValue('".$column."');\n";
            $functions .= "\t\t\n";
            $functions .= "\t\tif (is_null(\$id)) {\n";
            $functions .= "\t\t\treturn null;\n";
            $functions .= "\t\t}\n";
            $functions .= "\n\t";
            $functions .= "\treturn ".$model."::query()->find(\$id);\n";
            $functions .= "\t}\n";
        }

        $stub = $this->_replaceInStub($stub, 'optionalFunctions', $functions);

        return $this;
    }

    /**
     * @param string $stub
     *
     * @return $this
     */
    protected function replaceRelationshipUses(string &$stub):self {
        $include = $this->getRelationships($this->table, false);

        $uses = '';
        foreach ($include as $table => $column) {
            $model = Str::ucfirst(Str::studly($table));

            $uses .= "use Uab\\Http\\Models\\".$model.";\n";
        }

        $stub = $this->_replaceInStub($stub, 'usesRelationships', $uses);

        return $this;
    }

    protected function replaceTimestamps(string &$stub):self {
        $created = 'created_at';
        $deleted = 'deleted_at';
        $modified = 'modified_at';

        $columns = $this->_getColumns($this->table);
        foreach ($columns as $column) {
            if (in_array($column, ['created'])) {
                $created = $column;
            } else if (in_array($column, ['modified'])) {
                $modified = $column;
            } else if (in_array($column, ['deleted'])) {
                $deleted = $column;
            }
        }

        $created = $this->_addQuotes($created);
        $deleted = $this->_addQuotes($deleted);
        $modified = $this->_addQuotes($modified);

        $stub = $this->_replaceInStub($stub, 'created', $created);
        $stub = $this->_replaceInStub($stub, 'deleted', $deleted);
        $stub = $this->_replaceInStub($stub, 'modified', $modified);

        return $this;
    }

    /**
     * @param string $stub
     *
     * @return $this
     */
    protected function replaceTraits(string &$stub):self {
        $hasDeletedAt = $this->_hasColumn($this->table, 'deleted_at');

        $traits = "";
        if ($hasDeletedAt) {
            $traits .= "use SoftDeletes;\n";

            $stub = $this->_replaceInStub($stub, 'usesSoftDelete', 'use Illuminate\Database\Eloquent\SoftDeletes;');
        } else {
            $stub = $this->_replaceInStub($stub, 'usesSoftDelete', "\r");
        }

        $stub = $this->_replaceInStub($stub, 'additionalTraits', $traits);

        return $this;
    }
}
