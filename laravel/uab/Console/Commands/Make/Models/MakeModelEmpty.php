<?php
namespace Uab\Console\Commands\Make\Models;

use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Str;
use Uab\Console\Commands\Make\GeneratorCommand;

class MakeModelEmpty extends GeneratorCommand {
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'make:model-empty';

    protected $signature = 'make:model-empty {name} {--f|force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new empty model to be used for customization.';

    /**
     * Build the model class with the given name.
     *
     * @param string $name
     *
     * @return string
     *
     * @throws FileNotFoundException
     */
    protected function buildClass($name) {
        $this->info('Generating empty model for \''.$name.'\'.');

        $stub = parent::buildClass($name);

        return $stub;
    }

    protected function replaceNamespace(&$stub, $name) {
        $namespace = $this->getLaravel()->getNamespace() . 'Http\\Models';

        $stub = $this->_replaceInStub($stub, 'DummyNamespace', $namespace);

        return $this;
    }

    protected function getPath($name) {
        $name = Str::plural(Str::replaceFirst($this->rootNamespace(), '', $name));

        $camelCase = ucfirst(Str::camel($name));

        return $this->laravel['path'].'/Http/Models/'.$camelCase.'.php';
    }

    protected function getStub() {
        return __DIR__ . '/Stubs/model-empty.stub';
    }

    protected function replaceClass($stub, $name) {
        $class = $this->getClass($name);

        return parent::replaceClass($stub, $class);
    }
}
