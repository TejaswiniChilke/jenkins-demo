<?php
namespace Uab\Console\Commands\Make\Views;

use Symfony\Component\Console\Input\InputOption;
use Uab\Console\Commands\Make\Command;

class MakeViews extends Command {
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'make:views';

    protected $signature = 'make:views {name} {--f|force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create CRUD views for a model.';

    /**
     * Build the views for the given model name.
     *
     * @return MakeViews
     */
    public function handle() {
        $name = $this->argument('name');

        parent::buildClass($name);

        $args = [
            'name' => $name
        ];

        if ($this->option('force') !== false) {
            $args['--force'] = 'true';
        }

        $this->info('Generating views for \''.$name.'\'.');

        $this->call('make:view-create', $args);
        $this->call('make:view-edit', $args);
        $this->call('make:view-index', $args);
        $this->call('make:view-show', $args);

        return $this;
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    public function getOptions() {
        return [
            [
                'force',
                null,
                InputOption::VALUE_NONE,
                'Force create files.',
                null
            ]
        ];
    }

    public function getStub() {
        return '';
    }
}
