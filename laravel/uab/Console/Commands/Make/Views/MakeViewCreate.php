<?php
namespace Uab\Console\Commands\Make\Views;

use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Uab\Console\Commands\Make\Views\MakeViewCommand;

class MakeViewCreate extends MakeViewCommand {
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'make:view-create';

    protected $signature = 'make:view-create {name} {--f|force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a create view for the model.';

    /**
     * Build the model class with the given name.
     *
     * @param  string $name
     *
     * @return string
     *
     * @throws FileNotFoundException
     */
    protected function buildClass($name) {
        parent::buildClass($name);

        $this->info('Generating create view for \''.$name.'\'.');

        $stub = $this->files->get($this->getStub());

        $this->replaceTableModel($stub);
        $this->replaceHumanModel($stub);
        $this->replaceFormElements($stub);

        return $stub;
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub() {
        return __DIR__ . '/Stubs/view-create.stub';
    }

    protected function getPath($name) {
        $path = $this->_getViewPath($name);

        return $path.'create.blade.php';
    }
}
