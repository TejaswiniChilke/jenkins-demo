<?php
namespace Uab\Console\Commands\Make\Views;

use Uab\Console\Commands\Make\Views\MakeViewCommand;

class MakeViewIndex extends MakeViewCommand {
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'make:view-index';

    protected $signature = 'make:view-index {name} {--f|force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create an edit view for the model.';

    /**
     * Build the model class with the given name.
     *
     * @param  string $name
     *
     * @return string
     *
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function buildClass($name) {
        parent::buildClass($name);

        $this->info('Generating index view for \''.$name.'\'.');

        $stub = $this->files->get($this->getStub());

        $this->replaceTableModel($stub);
        $this->replaceHumanModel($stub);
        $this->replaceColumnHeaders($stub);
        $this->replaceColumnValues($stub);

        return $stub;
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub() {
        return __DIR__ . '/Stubs/view-index.stub';
    }

    protected function replaceColumnHeaders(&$stub) {
        $columns = $this->_getColumns($this->table);

        $tds = '';
        foreach ($columns as $column) {
            $column = $this->_humanize($column);
            $tds .= '<td>'.$column.'</td>'."\n";
        }

        $stub = $this->_replaceInStub($stub, 'columnHeaders', $tds);

        return $this;
    }

    protected function replaceColumnValues(&$stub) {
        $columns = $this->_getColumns($this->table);

        $tds = '';
        foreach ($columns as $column) {
            $tds .= '<td>{{ $object->'.$column.' }}</td>'."\n";
        }

        $stub = $this->_replaceInStub($stub, 'columnValues', $tds);

        return $this;
    }

    protected function getPath($name) {
        $path = $this->_getViewPath($name);

        return $path.'index.blade.php';
    }
}
