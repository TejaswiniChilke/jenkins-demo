<?php
namespace Uab\Console\Commands\Make\Views;

use Illuminate\Support\Str;
use Uab\Console\Commands\Make\GeneratorCommand;
use Uab\Console\Commands\Make\MakeCommandTrait;

abstract class MakeViewCommand extends GeneratorCommand {
    use MakeCommandTrait;

    protected function _getViewPath($name) {
        $name = Str::replaceFirst($this->rootNamespace(), '', $name);

        return $this->laravel['path'].'/../resources/views/'.$name.'/';
    }

    protected function replaceHumanModel(&$stub) {
        $human = $this->_humanize($this->table);

        $stub = $this->_replaceInStub($stub, 'modelHuman', $human);

        return $this;
    }

    protected function replaceTableModel(&$stub) {
        $stub = $this->_replaceInStub($stub, 'modelTable', $this->table);

        return $this;
    }

    protected function replaceFormElements(&$stub) {
        $columns = $this->_getColumnDetails($this->table);

        $formElements = '';

        foreach ($columns as $column) {
            $name = $column->COLUMN_NAME;
            $humanized = $this->_humanize($name);
            $type = $column->DATA_TYPE;

            $function = 'text';
            if ($type === 'email') {
                $function = 'email';
            }

            if ($type === 'datetime') {
                $function = 'dateTimePicker';
            }

            if ($type === 'tinyint') {
                $function = 'toggleButton';
            }

            if ($type === 'date') {
                $function = 'datePicker';
            }

            if ($type === 'time') {
                $function = 'timePicker';
            }

            if ($type === 'varchar') {
                if ($name === 'mobile_number' || $name === 'home_number' || $name === 'phone_number') {
                    $function = 'formatPhoneNumber';
                }
            }

            $columnNameLength = strlen($name);
            if ($columnNameLength >= 3) {
                if (substr($name, -3) === '_id') {
                    $function = 'autoComplete';
                }
            }

            $formElements .=
                '<div class="form-group">'.
                '    {{ Form::label(\''.$name.'\', \''.$humanized.'\') }}'.
                '    {{ Form::'.$function.'(\''.$name.'\', Input::old(\''.$name.'\'), array(\'class\' => \'form-control\')) }}'.
                '</div>'."\n\n";
        }

        $stub = $this->_replaceInStub($stub, 'formElements', $formElements);

        return $this;
    }
}
