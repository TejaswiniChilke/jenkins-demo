<?php
namespace Uab\Console\Commands\Make\Views;

use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Uab\Console\Commands\Make\Views\MakeViewCommand;

class MakeViewShow extends MakeViewCommand {
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'make:view-show';

    protected $signature = 'make:view-show {name} {--f|force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a show view for the model.';

    /**
     * Build the model class with the given name.
     *
     * @param  string $name
     *
     * @return string
     *
     * @throws FileNotFoundException
     */
    protected function buildClass($name) {
        parent::buildClass($name);

        $this->info('Generating show view for \''.$name.'\'.');

        $stub = $this->files->get($this->getStub());

        $this->replaceTableModel($stub);
        $this->replaceHumanModel($stub);
        $this->replaceNamePairs($stub);

        return $stub;
    }

    protected function getPath($name) {
        $path = $this->_getViewPath($name);

        return $path.'show.blade.php';
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub() {
        return __DIR__ . '/Stubs/view-show.stub';
    }

    protected function replaceNamePairs(&$stub) {
        $namePairs = '';

        $columns = $this->_getColumns($this->table);
        foreach ($columns as $column) {
            $humanize = $this->_humanize($column);

            $namePairs .= '<strong>'.$humanize.':</strong> {{ $object->'.$column.' }}<br>'."\n";
        }

        $stub = $this->_replaceInStub($stub, 'nameValuePairs', $namePairs);

        return $this;
    }
}
