<?php
namespace Uab\Console\Commands\Make\Controllers;

use Illuminate\Support\Str;
use Uab\Console\Commands\Make\GeneratorCommand;
use Uab\Console\Commands\Make\MakeCommandTrait;

abstract class MakeControllerCommand extends GeneratorCommand {
    use MakeCommandTrait;

    /**
     * Replace the attributes for the given stub.
     *
     * @param string $stub
     * @param string $name
     *
     * @return $this
     */
    protected function replaceModel(&$stub, $name) {
        $model = explode('\\', $name);
        $model = end($model);
        $model = str_replace('Controller', '', $model);
        $model = Str::snake($model);
        $model = strtolower($model);

        $stub = $this->_replaceInStub($stub, 'model', $model);

        return $this;
    }
}
