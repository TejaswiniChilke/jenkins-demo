<?php
namespace Uab\Console\Commands\Make\Controllers;

use Artisan;
use Illuminate\Support\Str;
use Uab\Console\Commands\Make\Command;

class MakeControllers extends Command {
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'make:controllers';

    protected $signature = 'make:controllers {name} {--f|force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new empty controller and generated controller files.';

    /**
     * Build the model class with the given name.
     */
    public function handle() {
        $name = $this->argument('name');

        parent::buildClass($name);

        $this->info('Generating controllers for \''.$name.'\'.');

        $models = $this->getModels($name);

        $bar = $this->output->createProgressBar(count($models) * 2);

        $options = [];
        if ($this->input->hasOption('force')) {
            $options['--force'] = true;
        }

        foreach ($models as $model) {
            $commands = [];

            $localOptions =  array_merge(
                [
                    'name' => Str::studly($model)
                ],
                $options
            );

            $commands['make:controller-empty'] = $localOptions;
            $commands['make:controller-generated'] = $localOptions;

            foreach ($commands as $command => $args) {
                Artisan::call($command, $args);

                $bar->advance();
            }
        }

        $bar->finish();
    }
}
