<?php
namespace Uab\Console\Commands\Make\Controllers;

use Illuminate\Contracts\Filesystem\FileNotFoundException;

class MakeControllerEmpty extends MakeControllerCommand {
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'make:controller-empty';

    protected $signature = 'make:controller-empty {name} {--f|force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new empty controller class for customization.';

    /**
     * Build the model class with the given name.
     *
     * @param  string $name
     *
     * @return string
     *
     * @throws FileNotFoundException
     */
    protected function buildClass($name) {
        $this->info('Generating empty controller for \''.$name.'\'.');

        $stub = parent::buildClass($name);

        $this->replaceModel($stub, $name);

        return $stub;
    }

    protected function getClass($name):string {
        return parent::getClass($name).'Controller';
    }

    protected function replaceNamespace(&$stub, $name) {
        $namespace = $this->getLaravel()->getNamespace().'Controllers';

        $stub = $this->_replaceInStub($stub, 'DummyNamespace', $namespace);

        return $this;
    }

    protected function getPath($name) {
        $class = $this->getClass($name);

        return $this->laravel['path'].'/Controllers/'.$class.'.php';
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub() {
        return __DIR__ . '/Stubs/controller-empty.stub';
    }
}
