<?php
namespace Uab\Console\Commands\Make\Controllers;

use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputOption;

class MakeControllerGenerated extends MakeControllerCommand {
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'make:controller-generated';

    protected $signature = 'make:controller-generated {name} {--f|force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new generated controller class for basic functionality.';

    /**
     * Build the model class with the given name.
     *
     * @param  string $name
     *
     * @return string
     *
     * @throws FileNotFoundException
     */
    protected function buildClass($name) {
        $this->info('Generating empty controller for \''.$name.'\'.');

        $model = explode('\\', $name);
        $model = end($model);
        $model = str_replace('Controller', '', $model);
        $model = Str::snake($model);
        $model = strtolower($model);

        $stub = parent::buildClass($name);

        $this->replaceModel($stub, $name);

        return $stub;
    }

    protected function getClass($name):string {
        return parent::getClass($name).'Controller';
    }

    protected function replaceNamespace(&$stub, $name) {
        $namespace = $this->getLaravel()->getNamespace().'Controllers\Generated';

        $stub = $this->_replaceInStub($stub, 'DummyNamespace', $namespace);

        return $this;
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    public function getOptions() {
        return [
            [
                'force',
                null,
                InputOption::VALUE_NONE,
                'Force create files.',
                null
            ]
        ];
    }

    protected function getPath($name) {
        $class = $this->getClass($name);

        return $this->laravel['path'].'/Controllers/Generated/'.$class.'.php';
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub() {
        return __DIR__ . '/Stubs/controller-generated.stub';
    }
}
