<?php
namespace Uab\Console\Commands;

use Carbon\Carbon;
use Uab\Http\Models\OauthAccessTokens;
use Illuminate\Console\Command;

class DeleteOauthTokens extends Command {
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'auth:clear-expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete expired tokens.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle() {
        OauthAccessTokens::query()
            ->where('expires_at', '<', Carbon::now())
            ->delete();
    }
}
