<?php
namespace Uab\Console\Commands\Updates;

use DB;
use Illuminate\Console\Command;
use Uab\Providers\YouTubeServiceProvider;

class UpdateVideoImages extends Command {
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'update:video-images';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'If a video is missing an image then cache the youtube video info with thumbnails.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $service = new YouTubeServiceProvider();

        $filePaths = DB::table('videos as v')
            ->leftJoin('youtube_video_infos as y', 'v.id', '=', 'y.video_id')
            ->where('v.file_path', 'LIKE', '%youtu%')
            ->whereNull('v.image_id')
            ->whereNull('y.id')
            ->get()->pluck('file_path');

        $bar = $this->output->createProgressBar($filePaths->count());

        foreach ($filePaths as $filePath) {
            $youtubeId = $service->getYouTubeId($filePath);

            $service->getVideoInfo($youtubeId);

            $bar->advance();
        }
    }
}
