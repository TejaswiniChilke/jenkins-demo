<?php
namespace Uab\Console\Commands\Updates;

use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Uab\Http\Models\Videos;
use Uab\Providers\YouTubeServiceProvider;

class UpdateYouTubeInfo extends Command {
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'update:youtube-info';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update videos youtube info.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     *
     * @throws \Exception
     */
    public function handle() {
        $service = new YouTubeServiceProvider();

        /** @var Builder $query */
        $query = Videos::where('videos.file_path', 'LIKE', '%youtu%');

        $bar = $this->output->createProgressBar($query->count());

        /** @var Videos[] $videos */
        $videos = $query->get();

        foreach ($videos as $video) {
            $youtubeId = $service->getYouTubeId($video->file_path);

            $info = $service->getVideoInfo($youtubeId);

            if ($info) {
                $service->patchVideo($video, $info);
            }

            $bar->advance();
        }

        $bar->finish();
    }
}
