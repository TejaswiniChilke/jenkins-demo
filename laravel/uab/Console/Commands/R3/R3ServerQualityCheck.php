<?php
namespace Uab\Console\Commands\R3;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Uab\Console\Commands\Run\Quality\ServerQualityCheck;
use Uab\Console\Commands\Run\Quality\Traits\QualityFrontendTrait;
use Uab\Console\Commands\Run\Quality\Traits\QualityQueueTrait;
use Uab\Console\Commands\Run\Quality\Traits\QualityRedisTrait;
use \Exception;
use \Throwable;
use Uab\Http\Enums\UserStatusesEnum;
use Uab\Http\Enums\UserTypesEnum;
use Uab\Http\Models\BugReports;
use Uab\Http\Models\ContactStaffMessages;
use Uab\Http\Models\Emails;
use Uab\Http\Models\Users;
use Uab\Providers\UsersServiceProvider;

class R3ServerQualityCheck extends ServerQualityCheck {
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;
    use QualityFrontendTrait;
    use QualityQueueTrait;
    use QualityRedisTrait;

    protected $signature = 'run:r3-quality';

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Handle the event.
     *
     * @return void
     *
     * @throws Exception
     * @throws Throwable
     */
    public function handle() {
        $this->confirmUserStatusReceivedMessage(
            UserStatusesEnum::AWAITING_EMAIL_CONFIRMATION,
            'to continue with the application process'
        );

        $this->confirmUserStatusReceivedMessage(
            UserStatusesEnum::NEED_PROFILE_COMPLETE,
            '/#/set-password/'
        );

        $this->confirmUserStatusReceivedMessage(
            UserStatusesEnum::PROFILE_COMPLETE,
            'accepting the calendar invitation'
        );

        $this->confirmAdminReceivedBugReportMessage();

        $this->confirmAdminReceivedContactStaffMessage();

        $doesScreenFrontLoad = $this->checkFrontendLoads(config('screen.url'));
        if (!$doesScreenFrontLoad) {
            $this->notifyError(
                'Screen frontend is not loading.'
            );
        }
    }

    /**
     * Confirm all users with $userStatus have received an email
     * containing $emailSnippet after $since
     *
     * @param string $userStatus
     * @param string $emailSnippet
     * @param Carbon|null $since
     *
     * @return void
     */
    private function confirmUserStatusReceivedMessage(string $userStatus, string $emailSnippet, $since = null) {
        if (is_null($since)) {
            $since = Carbon::now()->subWeek()->subDay();
        }

        $users = Users::query()
            ->select('users.*')
            ->join('user_status_changes', 'user_status_changes.user_id', '=', 'users.id')
            ->where(
                [
                    'users.status' => $userStatus,
                    'user_status_changes.status' => $userStatus,
                ]
            )->where('user_status_changes.created', '>', $since)
            ->orderBy('user_status_changes.created', 'DESC')
            ->get();

        foreach ($users as $user) {
            $sentEmail = $this->hasBeenSentEmailSnippet($user->id, $emailSnippet, $since);

            if ($sentEmail === false) {
                $this->notifyError(
                    'A user (' . $user->id . ') was not sent an email containing "'. $emailSnippet . '".'
                );
            }
        }
    }

    /**
     * @param integer $userId
     * @param string $snippet
     * @param Carbon $since
     *
     * @return bool
     */
    private function hasBeenSentEmailSnippet(int $userId, string $snippet, Carbon $since):bool {
        return Emails::query()
            ->where('to_user_id', '=', $userId)
            ->where('message', 'LIKE', '%' . $snippet . '%')
            ->where('created', '>', $since)
            ->whereNotNull('header')
            ->exists();
    }

    /**
     * Confirm an admin received an email about bug reports
     *
     * @return void
     */
    public function confirmAdminReceivedBugReportMessage() {
        $userService = new UsersServiceProvider();

        $admin = $userService->getUsersByUserTypeNames([ UserTypesEnum::ADMIN ])
            ->random(1)->first();

        $since = Carbon::now()->subWeek()->subDay();

        /** @var BugReports[] $bugReports */
        $bugReports = BugReports::query()
            ->where('created', '>', $since)
            ->get();

        foreach ($bugReports as $bugReport) {
            $reportingUser = $bugReport->user();

            if (is_null($reportingUser)) {
                continue;
            }
            if (is_null($reportingUser->email)) {
                continue;
            }

            $snippet = 'Bug reported.';

            $sentEmail = $this->hasBeenSentEmailSnippet($admin->id, $snippet, $since);

            if ($sentEmail === false) {
                $this->notifyError(
                    'An admin (' . $admin->email . ') was not sent an email containing "'. $snippet . '".'
                );
            }
        }
    }

    /**
     * Confirm an admin received an email about bug reports
     *
     * @return void
     */
    public function confirmAdminReceivedContactStaffMessage() {
        $userService = new UsersServiceProvider();

        $admin = $userService->getUsersByUserTypeNames([ UserTypesEnum::ADMIN ])
            ->random(1)->first();

        $since = Carbon::now()->subWeek()->subDay();

        /** @var ContactStaffMessages[] $contactStaffMessages */
        $contactStaffMessages = ContactStaffMessages::query()
            ->where('created', '>', $since)
            ->get();

        foreach ($contactStaffMessages as $contactStaffMessage) {
            $fromUser = $contactStaffMessage->user();

            if (is_null($fromUser)) {
                continue;
            }
            if (is_null($fromUser->email)) {
                continue;
            }

            $snippet = '\'Contact Staff\' Initiated.';

            $sentEmail = $this->hasBeenSentEmailSnippet($admin->id, $snippet, $since);

            if ($sentEmail === false) {
                $this->notifyError(
                    'An admin (' . $admin->email . ') was not sent an email containing "'. $snippet . '".'
                );
            }
        }
    }
}
