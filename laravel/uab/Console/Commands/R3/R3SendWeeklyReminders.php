<?php
namespace Uab\Console\Commands\R3;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Pressutto\LaravelSlack\Facades\Slack;
use Uab\Http\Enums\QueueTypesEnum;
use Uab\Http\Enums\UserTypesEnum;
use Uab\Http\Models\Emails;
use Uab\Http\Models\TextMessages;
use Uab\Http\Models\UserTypes;
use Uab\Jobs\Emails\SendAndCreateEmail;
use Uab\Jobs\Emails\SendEmail;
use Uab\Jobs\Texts\SendAndCreateText;
use Uab\Providers\UsersServiceProvider;

class R3SendWeeklyReminders extends Command {
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'send:r3-reminders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send weekly email/text reminders to participants.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle() {
        $now = Carbon::now();
        $day = strtolower($now->dayName);

        $service = new UsersServiceProvider();

        /** @var UserTypes[] $userTypes */
        $userTypes = [
            UserTypesEnum::CONTROL,
            UserTypesEnum::M2M,
            UserTypesEnum::SET
        ];

        $users = $service->getUsersByUserTypeNames($userTypes);

        foreach ($users as $user) {
            $startDay = strtolower(Carbon::parse($user->start_time)->dayName);

            if ($startDay === $day) {
                $week = $user->getWeek();
                $lastWeek = config('app.weeks');

                if (!is_null($week) && $week >= 0 && $week <= $lastWeek) {
                    $startTime = Carbon::parse($user->start_time);

                    $preferredTime = Carbon::now()->setHour($startTime->hour);
                    $preferredTime = $preferredTime->setMinute($startTime->minute);

                    if (Carbon::now()->isAfter($preferredTime)) {
                        $preferredTime = Carbon::now()->addMinutes(3);
                    }

                    $preferredType = strtolower($user->preferred_contact_type);

                    $env = str_replace('https://', '', config('app.frontend'));

                    $message = 'New content in week ' . $week . ' is now available on the SCIPE website! You can continue by logging into your account at ' . $env . '/#/login. Thank you for your participation!';

                    if ($preferredType === 'text' || $preferredType === 'phone') {
                        $text = new TextMessages();
                        $text->to_user_id = $user->id;
                        $text->to_number = $user->mobile_number;
                        $text->message = $message;

                        SendAndCreateText::dispatch($text)
                            ->delay($preferredTime)
                            ->onQueue(QueueTypesEnum::TEXT);
                    } else {
                        $email = new Emails();
                        $email->to_user_id = $user->id;
                        $email->to_email = $user->email;
                        $email->subject = 'New content is available on the SCIPE Website!';
                        $email->message = $message;
                        $email->save();

                        SendEmail::dispatch($email)
                            ->delay($preferredTime)
                            ->onQueue(QueueTypesEnum::EMAIL);
                    }
                }
            }
        }
    }
}
