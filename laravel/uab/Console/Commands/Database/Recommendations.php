<?php
namespace Uab\Console\Commands\Database;

use Illuminate\Support\Arr;
use Symfony\Component\Console\Input\InputOption;

class Recommendations extends DatabaseCommand {
    private $details = [];

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'database:recommendations 
                                {from : The database to compare (use key in connections array in database.php} 
                                {to : The database to compare to (use key in connections array in database.php} 
                                {--D|--debug? : Run XDEBUG} 
                                {--F|--force? : Just do it!}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Recommend table changes based on other database.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $primary = $this->argument('from');
        $primaryTables = $this->getTables($primary);

        $other = $this->argument('to');
        $otherTables = $this->getTables($other);

        $missing = $this->getMissing($otherTables, $primaryTables);

        if (count($missing) === 0) {
            echo 'All good!';
        } else {
            foreach ($missing as $table => $columns) {
                $query = "";

                $hasId = Arr::has($columns, 'id');
                if ($hasId) {
                    $query .= "ALTER";
                } else {
                    $query .= "CREATE";
                }

                $query .= " TABLE `{$table}` (\n";

                foreach ($columns as $column) {
                    if (!array_key_exists($table, $this->details)) {
                        $this->details[$table] = [];

                        $details = $this->getColumnDetails($table, 'mysql');

                        foreach ($details as $detail) {
                            $this->details[$table][$detail->COLUMN_NAME] = $detail;
                        }
                    }

                    $details = $this->details[$table][$column];

                    if (!isset($details->COLUMN_TYPE)) {
                        echo "Column type not found for column {$table} ({$column})\n";
                        continue;
                    }

                    $type = $details->COLUMN_TYPE;

                    $default = isset($details->COLUMN_DEFAULT) ?
                        $details->COLUMN_DEFAULT : null;

                    $extra = isset($details->EXTRA) ?
                        $details->EXTRA : null;

                    $nullable = isset($details->IS_NULLABLE) ?
                        ($details->IS_NULLABLE === 'Yes' ? true : false) : false;

                    $query .= "\t";
                    if ($hasId) {
                        $query .= "ADD COLUMN ";
                    }

                    $query .= "`{$column}` {$type}";

                    if (!$nullable) {
                        $query .= ' NOT NULL ';
                    }

                    if (!is_null($default)) {
                        $query .= " {$default}";
                    }

                    if (!is_null($extra)) {
                        $query .= " {$extra}";
                    }

                    $query .= ",\n";
                }

                if (!$hasId) {
                    $query .= "\tPRIMARY KEY (`id`)\n";
                }

                $query .= ") ENGINE=InnoDB DEFAULT CHARSET=utf8;\n\n";

                echo $query;
            }
        }
    }

    public function getOptions() {
        return [
            [
                'debug',
                null,
                InputOption::VALUE_NONE,
                'Add debug variables.',
                false
            ],
            [
                'force',
                null,
                InputOption::VALUE_NONE,
                'Force run services.',
                null
            ]
        ];
    }
}
