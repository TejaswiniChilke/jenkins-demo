<?php
namespace Uab\Console\Commands\Database;

use DB;
use Illuminate\Support\Arr;
use Schema;
use Uab\Console\Commands\BaseCommand;

abstract class DatabaseCommand extends BaseCommand {
    private $seen = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * @param $masterTables
     * @param $currentTables
     *
     * @return string[][]
     */
    protected function getMissing($masterTables, $currentTables) {
        $missing = [];

        foreach ($masterTables as $masterTable) {
            if (!Arr::has($this->seen, $masterTable)) {
                $this->seen[] = $masterTable;

                $masterColumns = Schema::getColumnListing($masterTable);

                $tableIndex = array_search($masterTable, $currentTables);

                if ($tableIndex !== false) {
                    $primaryColumns = $columns = Schema::getColumnListing($masterTable);

                    foreach ($masterColumns as $masterColumn) {
                        if (array_search($masterColumn, $primaryColumns) === false) {
                            if (array_key_exists($masterTable, $missing)) {
                                $missing[$masterTable] = [];
                            }

                            $missing[$masterTable][] = $masterColumn;
                        }
                    }
                } else {
                    $missing[$masterTable] = $masterColumns;
                }
            }
        }

        return $missing;
    }

    protected function getTables($connection = 'mysql') {
        $tables = DB::connection($connection)->select(
            'SELECT TABLE_NAME AS name' .
            '  FROM information_schema.tables ' .
            ' WHERE table_schema = DATABASE()'
        );

        foreach ($tables as &$table) {
            $table = $table->name;
        }

        return $tables;
    }

    protected function getColumnDetails($table, $connection = 'mysql') {
        return DB::connection($connection)->select(
            "SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = '{$table}'"
        );
    }
}
