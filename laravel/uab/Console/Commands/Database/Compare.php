<?php
namespace Uab\Console\Commands\Database;

use Symfony\Component\Console\Input\InputOption;

class Compare extends DatabaseCommand {
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'database:compare
                                {other : The other database to compare (use key in connections array in database.php}
                                {showFound=false : Show all tables and columns even if not missing}
                                {--D|--debug? : Run XDEBUG}
                                {--F|--force? : Just do it!}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run all of the services required for next.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $primaryTables = $this->getTables('mysql');

        $other = $this->argument('other');
        $otherTables = $this->getTables($other);

        $missing = $this->getMissing($primaryTables, $otherTables);

        $missing = array_merge($this->getMissing($otherTables, $primaryTables), $missing);

        echo json_encode($missing);
    }

    public function getOptions() {
        return [
            [
                'debug',
                null,
                InputOption::VALUE_NONE,
                'Add debug variables.',
                false
            ],
            [
                'force',
                null,
                InputOption::VALUE_NONE,
                'Force run services.',
                null
            ]
        ];
    }
}
