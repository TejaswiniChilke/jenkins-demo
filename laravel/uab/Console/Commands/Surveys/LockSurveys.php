<?php
namespace Uab\Console\Commands\Surveys;

use Illuminate\Console\Command;
use Uab\Http\Models\SurveysUsers;
use \Exception;

class LockSurveys extends Command {
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'surveys:lock';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Lock surveys.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     *
     * @throws Exception
     */
    public function handle() {
        SurveysUsers::query()
            ->where('is_locked', '=', false)
            ->update(
                [
                    'is_locked' => true
                ]
            );
    }
}
