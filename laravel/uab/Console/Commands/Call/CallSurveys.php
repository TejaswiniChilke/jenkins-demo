<?php
namespace Uab\Console\Commands\Call;

use Illuminate\Console\Command;
use Uab\Http\Enums\QueueTypesEnum;
use Uab\Http\Enums\UserStatusesEnum;
use Uab\Http\Enums\UserTypesEnum;
use Uab\Http\Models\AnalyticTrackCalls;
use Uab\Jobs\Calls\MakeCall;
use Uab\Providers\UsersServiceProvider;
use Carbon\Carbon;

class CallSurveys extends Command {
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'call:surveys';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Call users to deliver surveys.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    private function hasNoCompletedCallToday($user) {
        $today = Carbon::today();
        $formattedDate = $today->toDateString();

        $userInComingCompletedCalls = AnalyticTrackCalls::where(
            [
                'from_user_id' => $user->id,
                'status'       => 'completed'
            ]
        )->where('created', '>', $formattedDate)->first();

        $userOutgoingCompletedCalls = AnalyticTrackCalls::where(
            [
                'to_user_id'   => $user->id,
                'status'       => 'completed'
            ]
        )->where('created', '>', $formattedDate)->first();
        return is_null($userInComingCompletedCalls) && is_null($userOutgoingCompletedCalls);
    }

    private function hasNoInProgressCallToday($user) {
        $today = Carbon::today();
        $formattedDate = $today->toDateString();
        $userInProgressOutgoingCall = AnalyticTrackCalls::where(
            [
                'to_user_id' => $user->id,
                'status'     => 'in-progress'
            ]
        )->where('created', '>', $formattedDate)->first();

        $userInProgressIncomingCall = AnalyticTrackCalls::where(
            [
                'from_user_id' => $user->id,
                'status'     => 'in-progress'
            ]
        )->where('created', '>', $formattedDate)->first();
        return is_null($userInProgressOutgoingCall) && is_null($userInProgressIncomingCall);
    }

    private function getTodayMissedCallData($user) {
        $today = Carbon::today();
        $formattedDate = $today->toDateString();
        $surveyUserMissedCallQuery = AnalyticTrackCalls::where(
            [
                'to_user_id'   => $user->id,
                'status'       => 'missed'
            ]
        )->where('created', '>', $formattedDate);
        return $surveyUserMissedCallQuery;
    }

    private function isTimeForMissedCall($currentTime, $userContactTime) {
        return (strtotime($currentTime) >= (strtotime("+30 minutes", strtotime($userContactTime))))
            && (strtotime($currentTime) < (strtotime("+45 minutes", strtotime($userContactTime))));
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $userService = new UsersServiceProvider();
        $users = $userService->getUsersByUserTypeNames(
            [
                UserTypesEnum::PARTICIPANT
            ]
        );

        foreach ($users as $user) {
            $isTimeToCall = false;
            if ($user->status !== UserStatusesEnum::DEACTIVATED) {
                $userContactTime = Carbon::createFromTimeString($user->preferred_contact_time);
                $currentTime = Carbon::now();

                if ($currentTime->greaterThanOrEqualTo($userContactTime)) {

                    $hasNoCompletedCallToday = $this->hasNoCompletedCallToday($user);

                    if ($hasNoCompletedCallToday) {

                        $hasNoInProgressCallToday = $this->hasNoInProgressCallToday($user);

                        if ($hasNoInProgressCallToday) {
                            $getTodayMissedCallData = $this->getTodayMissedCallData($user);

                            $userMissedCallCount = $getTodayMissedCallData->count();
                            $userMissedCall = $getTodayMissedCallData->first();

                            if (!is_null($userMissedCall)) {
                                $currentWeek = $user->getWeek();
                                $isTImeForMissedCall = $this->isTimeForMissedCall($currentTime, $userContactTime);

                                if ($currentWeek <= 12) {
                                    if ($isTImeForMissedCall) {
                                        $isTimeToCall = true;
                                    }
                                } else {
                                    if ($userMissedCallCount === 1) {
                                        if ($isTImeForMissedCall) {
                                            $isTimeToCall = true;
                                        }
                                        if ($userMissedCallCount === 2) {
                                            // TODO: Venkat -- call same as today for next day
                                        }
                                    }
                                }
                            } else {
                                $isTimeToCall = true;
                            }
                            if ($isTimeToCall) {
                                MakeCall::dispatch($user)->onQueue(QueueTypesEnum::CALL);
                            }
                        }
                    }
                }
            }
        }
    }
}
