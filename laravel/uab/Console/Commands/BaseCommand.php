<?php
namespace Uab\Console\Commands;

use Illuminate\Console\Command;

abstract class BaseCommand extends Command {
    public function getApiDirectory() {
        return exec('pwd');
    }

    public function runInNewTab($command) {
        $pwd = $this->getApiDirectory();

        $debug = $this->input->hasOption('debug');

        if ($debug && strstr($command, 'php ')) {
            $command = 'export XDEBUG_CONFIG=\'idekey=PHPSTORM\' && '.$command;
        }

        exec('
            osascript -e \'tell application "Terminal" to activate\' -e \'tell application "System Events" to tell process "Terminal" to keystroke "t" using command down\' -e \'tell application "Terminal" to do script "cd '.$pwd.' && '.$command.'" in selected tab of the front window\'
        ');
    }
}
