<?php
namespace Uab\Console\Commands\Clean;

use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Uab\Http\Enums\QueueTypesEnum;
use Uab\Http\Models\Badges;
use Uab\Http\Models\Images;
use Uab\Jobs\Badges\CheckMissedBadge;

class CleanImages extends Command {
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'clean:images';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete images not attached to objects in DB.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle() {
        $uploadDirectory = storage_path('app/public');

        $imageDirectory = $uploadDirectory . '/' . config('upload.image.directory');

        $filenames = scandir($imageDirectory);
        foreach($filenames as $filename) {
            $filePath = config('upload.image.directory') . '/' . $filename;

            if (Str::substr($filename, 0, 1) === '.') {
                $this->info('Ignoring "' . $filePath . '"');
                continue;
            }

            $hasImage = Images::query()
                ->where('file_path', '=', $filePath)
                ->exists();

            if ($hasImage) {
                $this->info('Keeping "' . $filePath . '"');
            } else {
                $this->info('Deleting "' . $filePath . '"');

                exec('rm "' . $imageDirectory . '/' . $filename . '"');
            }
        }
    }
}
