<?php
namespace Uab\Console\Commands\Badges;

use Illuminate\Console\Command;
use Uab\Http\Enums\QueueTypesEnum;
use Uab\Http\Models\Badges;
use Uab\Jobs\Badges\CheckMissedBadge;

class CheckMissedBadges extends Command {
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'badges:missed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Award missed badges.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle() {
        $badges = Badges::query()
            ->orderBy('start_week', 'ASC')
            ->get();

        foreach ($badges as $badge) {
            CheckMissedBadge::dispatch($badge->id)
                ->onQueue(QueueTypesEnum::LOW);
        }
    }
}
