<?php
namespace Uab\Console\Commands\Notify;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Uab\Http\Enums\QueueTypesEnum;
use Uab\Http\Enums\UserTypesEnum;
use Uab\Http\Models\Users;
use Uab\Jobs\Emails\EmailUserType;

class NotifyNotLoggingIn extends Command {
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'notify:not-logging-in';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send a notification to admin to notify them of accounts which have not logged in for awhile.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle() {
        $daysAgo = config('auth.options.notify-no-use');

        if (!is_null($daysAgo)) {
            $date = Carbon::now()->subDays($daysAgo);

            $users = Users::query()
                ->join(
                    'login_attempts',
                    'login_attempts.user_id',
                    '=',
                    'users.id'
                )->join(
                    'user_types',
                    'users.user_type_id',
                    '=',
                    'user_types.id'
                )->where(
                    'login_attempts.attempted_at',
                    '>',
                    $date
                )->whereNotIn(
                    'user_types.name',
                    [
                        UserTypesEnum::ADMIN,
                        UserTypesEnum::CONTROL,
                        UserTypesEnum::SUPER_ADMIN,
                    ]
                )->get();

            $count = $users->count();
            if ($count !== 0) {
                $this->info('Sending an email for '.$count.' participants not logging in.');

                $subject = 'Participants not logging in.';

                $message = 'The following participants have not logged in within the last ' . $daysAgo . ' days:' . "\n\n";

                foreach ($users as $user) {
                    $message .= $user->full_name.' <'.$user->email.'>'."\n";
                }

                EmailUserType::dispatch(
                    UserTypesEnum::ADMIN,
                    $message,
                    $subject
                )->onQueue(QueueTypesEnum::LOW);
            }
        }
    }
}
