<?php
namespace Uab\Console\Commands\Notify;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Uab\Http\Enums\QueueTypesEnum;
use Uab\Http\Enums\UserTypesEnum;
use Uab\Http\Models\Emails;
use Uab\Http\Models\TextMessages;
use Uab\Http\Models\UsersWeeklyReminders;
use Uab\Http\Models\UserTypes;
use Uab\Http\Models\WeeklyReminders;
use Uab\Jobs\Base\InsertObjectJob;
use Uab\Jobs\Emails\SendAndCreateEmail;
use Uab\Jobs\Emails\SendEmail;
use Uab\Jobs\Texts\SendAndCreateText;
use Uab\Providers\UsersServiceProvider;

class SendWeeklyReminders extends Command {
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'send:weekly-reminders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send weekly email/text reminders to participants.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle() {
        $now = Carbon::now();
        $day = strtolower($now->dayName);

        $service = new UsersServiceProvider();

        /** @var UserTypes[] $userTypes */
        $userTypes = [
            UserTypesEnum::M2M,
            UserTypesEnum::SET
        ];

        foreach ($userTypes as $userType) {
            $users = $service->getUsersByUserTypeNames($userType);

            foreach ($users as $user) {
                $startDay = strtolower(Carbon::parse($user->start_time)->dayName);

                if ($startDay === $day) {
                    $week = $user->getWeek();
                    $lastWeek = config('app.weeks');

                    if ($week <= $lastWeek) {
                        $preferredTime = $user->preferred_contact_time;

                        if ($preferredTime >= $now) {
                            $weeklyRemindersToSend = [];

                            /** @var WeeklyReminders[] $weeklyReminders */
                            $allWeeklyReminders = WeeklyReminders::all();
                            foreach ($allWeeklyReminders as $weeklyReminder) {
                                if ($week >= $weeklyReminder->start_week || $weeklyReminder->start_week) {
                                    if ($week <= $weeklyReminder->end_week || $weeklyReminder->end_week) {
                                        $exists = UsersWeeklyReminders::query()
                                            ->where(
                                                [
                                                    'user_id'            => $user->id,
                                                    'weekly_reminder_id' => $weeklyReminder->id,
                                                    'week'               => $week
                                                ]
                                            )->exists();

                                        if (!$exists) {
                                            $weeklyRemindersToSend[] = $weeklyReminder;
                                        }
                                    }
                                }
                            }

                            $preferredType = strtolower($user->preferred_contact_type);

                            foreach ($weeklyRemindersToSend as $weeklyReminder) {
                                if ($preferredType === 'text' || $preferredType === 'phone') {
                                    $email = new Emails();
                                    $email->to_user_id = $user->id;
                                    $email->to_email = $user->email;
                                    $email->subject = 'New exercise contents are available on SCIPE Website';
                                    $email->message = $weeklyReminder->message;
                                    $email->save();

                                    SendEmail::dispatch($email)->onQueue(QueueTypesEnum::LOW);
                                } else {
                                    $text = new TextMessages();
                                    $text->to_user_id = $user->id;
                                    $text->to_number = $user->mobile_number;
                                    $text->message = $weeklyReminder->message;

                                    SendAndCreateText::dispatch($text)->onQueue(QueueTypesEnum::LOW);
                                }

                                $userWeeklyReminder = new UsersWeeklyReminders();
                                $userWeeklyReminder->user_id = $user->id;
                                $userWeeklyReminder->week = $week;
                                $userWeeklyReminder->weekly_reminder_id = $weeklyReminder->id;

                                InsertObjectJob::dispatch($userWeeklyReminder)->onQueue(QueueTypesEnum::LOW);
                            }
                        }
                    }
                }
            }
        }
    }
}
