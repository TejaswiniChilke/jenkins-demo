<?php
namespace Uab\Console\Commands\Notify;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Uab\Http\Enums\QueueTypesEnum;
use Uab\Http\Enums\UserTypesEnum;
use Uab\Http\Models\Users;
use Uab\Jobs\Emails\EmailUserType;

class NotifyUnactivatedAccounts extends Command {
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'notify:unactivated-accounts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send a notification to admin to notify them of unactivated accounts.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle() {
        $users = Users::query()
            ->where(
                'activation_email_sent_date', '<', Carbon::now()->subDays(3)
            )->where(
                    'requires_approval', '=', 1
            )->whereNotNull(
                'approved_on'
            )->whereNull(
                'password'
            )->get();

        $count = $users->count();
        if ($count) {
            $this->info('Sending an email for '.$count.' participants not activating account.');

            $message = 'The following participants have been approved for the study but have not yet activated their accounts:' . "\n\n";

            foreach ($users as $user) {
                $message .= $user->full_name . ' <' . $user->email . '>' . "\n";
            }

            EmailUserType::dispatch(
                UserTypesEnum::ADMIN,
                $message,
                'Participants have yet to activate account.'
            )->onQueue(QueueTypesEnum::LOW);
        }
    }
}
