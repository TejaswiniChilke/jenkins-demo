<?php
namespace Uab\Console\Commands\Run\Quality\Traits;

trait QualityFrontendTrait {
    /**
     * @param string|null $url
     *
     * @return bool
     */
    public function checkFrontendLoads(?string $url = null):bool {
        if (is_null($url)) {
            $url = config('app.frontend', null);
            if (is_null($url)) {
                return false;
            }
        }

        if (!filter_var($url, FILTER_VALIDATE_URL)) {
            return false;
        }

        $curlInit = curl_init($url);

        curl_setopt($curlInit,CURLOPT_CONNECTTIMEOUT,30);

        $response = curl_exec($curlInit);

        curl_close($curlInit);

        return $response ? true : false;
    }
}
