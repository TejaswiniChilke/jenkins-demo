<?php
namespace Uab\Console\Commands\Run\Quality\Traits;

use Queue;

trait QualityQueueTrait {
    /**
     * @param int|null $limit
     *
     * @return bool
     */
    public function checkQueue(?int $limit = null):bool {
        if (is_null($limit)) {
            $limit = config('quality.queue.limit', 100);
        }

        $queueSize = 0;

        $queues = explode(',', 'high,normal,default,low,text,email,call');
        foreach ($queues as $queue) {
            $queueSize += Queue::size($queue);
        }

        if ($queueSize > $limit) {
            return false;
        }

        return true;
    }
}
