<?php
namespace Uab\Console\Commands\Run\Quality\Traits;

use \Exception;
use Illuminate\Support\Facades\Redis;

trait QualityRedisTrait {
    /**
     * @return bool
     */
    public function checkRedisConnection():bool {
        try {
            Redis::connect('127.0.0.1', 6379);
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function checkRedisGetSet():bool {
        try {
            $key = uniqid();
            $value = uniqid();

            Redis::set($key, $value);

            $redisValue = Redis::get($key);

            return $redisValue === $value;
        } catch (Exception $e){
            return false;
        }
    }
}
