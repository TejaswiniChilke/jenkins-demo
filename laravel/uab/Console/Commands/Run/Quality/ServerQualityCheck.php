<?php
namespace Uab\Console\Commands\Run\Quality;

use Illuminate\Bus\Queueable;
use Pressutto\LaravelSlack\Facades\Slack;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Log;
use Uab\Console\Commands\Run\Quality\Traits\QualityFrontendTrait;
use Uab\Console\Commands\Run\Quality\Traits\QualityQueueTrait;
use Uab\Console\Commands\Run\Quality\Traits\QualityRedisTrait;
use \Exception;
use \Throwable;
use Uab\Http\Models\TextMessages;
use Uab\Providers\TextMessageServiceProvider;

class ServerQualityCheck extends Command {
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;
    use QualityFrontendTrait;
    use QualityQueueTrait;
    use QualityRedisTrait;

    protected $signature = 'run:quality';

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Handle the event.
     *
     * @return void
     *
     * @throws Exception
     * @throws Throwable
     */
    public function handle() {
        try {
            $isRedisConnectionGood = $this->checkRedisConnection();
            if (!$isRedisConnectionGood) {
                $this->notifyError(
                    'Redis is down.'
                );
            }

            $isQueueGood = $this->checkQueue();
            if (!$isQueueGood) {
                $this->notifyError(
                    'There are currently over ' . config('quality.queue.limit', 100) . ' jobs on the queues.'
                );
            }

            $isRedisConnectionGood = $this->checkRedisGetSet();
            if (!$isRedisConnectionGood) {
                $this->notifyError(
                    'Cannot add or retrieve from redis server.'
                );
            }

            // TODO: Fails on r3 app
            // $doesFrontLoad = $this->checkFrontendLoads(config('app.frontend'));
            // if (!$doesFrontLoad) {
            //     $this->notifyError(
            //         'App frontend is not loading.'
            //     );
            // }
        } catch (Exception $e) {
            $this->notifyError('Something went wrong during the quality check.');
        }
    }

    protected function notifyError($message) {
        $message = 'The following error occurred for ' . config('app.name') . ' on the ' . config('app.env') . ' environment. ' . $message;

        Log::channel('teamsinfo')->info($message);
        Slack::to(config('SLACK_CHANNEL'))->send($message);

        $adminPhoneNumbers = [
            '+12342816999',
            '+13122187740',
            '+12053320786',
            '+16149660315',
        ];

        $textService = new TextMessageServiceProvider();

        foreach ($adminPhoneNumbers as $phoneNumber) {
            $text = new TextMessages();
            $text->to_number = $phoneNumber;
            $text->from_number = config('twilio.phone_number');
            $text->message = $message;
            $text->save();

            $textService->sendSms($text);
        }
    }
}
