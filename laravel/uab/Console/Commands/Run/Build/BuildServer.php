<?php
namespace Uab\Console\Commands\Run\Build;

use Uab\Console\Commands\BaseCommand;

class BuildServer extends BaseCommand {
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'build:server';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Build next locally.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $pwd = $this->getApiDirectory();

        exec('cd '.$pwd);
        exec('composer install');
        exec('npm install');
        exec('cd ../socket-server/ && npm install');
        exec('cd .. && npm install');
        exec('cp assets/config.example.server.json assets/config.json');

        return true;
    }
}
