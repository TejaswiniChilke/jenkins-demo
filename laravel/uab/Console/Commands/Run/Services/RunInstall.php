<?php
namespace Uab\Console\Commands\Run\Services;

use Uab\Console\Commands\BaseCommand;

class RunInstall extends BaseCommand {
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'run:install {--hard}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install dependencies.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $isHard = $this->option('hard');

        if ($isHard) {
            exec('cd ' . $this->getApiDirectory() . ' && rm -rf vendor/ && composer install --ignore-platform-reqs');
        }
        exec('cd ' . $this->getApiDirectory() . ' && composer install --ignore-platform-reqs');

        if ($isHard) {
            exec('cd ' . $this->getApiDirectory() . ' && rm -rf node_modules/ && npm install');
        }
        exec('cd ' . $this->getApiDirectory() . ' && npm install');

        if ($isHard) {
            exec('cd ' . $this->getApiDirectory() . ' && rm -rf node_modules/ && npm install');
        }
        exec('cd ' . $this->getApiDirectory() . ' && cd public/ && npm install');

        if ($isHard) {
            exec('cd ' . $this->getApiDirectory() . ' && cd ../socket-server/ && rm -rf node_modules/ && npm install');
        }
        exec('cd ' . $this->getApiDirectory() . ' && cd ../socket-server/ && npm install');

        if ($isHard) {
            exec('cd ' . $this->getApiDirectory() . ' && cd ../main/ && rm -rf node_modules/ && npm install');
        }
        exec('cd ' . $this->getApiDirectory() . ' && cd ../main/ && npm install');

        if ($isHard) {
            exec('cd ' . $this->getApiDirectory() . ' && cd ../apply/ && rm -rf node_modules/ && npm install');
        }
        exec('cd ' . $this->getApiDirectory() . ' && cd ../apply/ && npm install');

        return true;
    }
}
