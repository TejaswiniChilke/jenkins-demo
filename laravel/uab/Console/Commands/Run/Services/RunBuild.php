<?php
namespace Uab\Console\Commands\Run\Services;

use Uab\Console\Commands\BaseCommand;

class RunBuild extends BaseCommand {
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'run:build {--test} {--prod}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Build app for deploy.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return bool
     */
    public function handle():bool {
        $isTest = $this->option('test');
        if (!$isTest) {
            $this->info('Building main prod...');
            exec('cd ' . $this->getApiDirectory() . ' && cd ../main/ && ng build --prod --output-path=wwwprod --output-hashing=all');

            $this->info('Building apply prod...');
            exec('cd ' . $this->getApiDirectory() . ' && cd ../apply/ && ng build --prod --output-path=wwwprod --output-hashing=all');
        }

        $isProd = $this->option('prod');
        if (!$isProd) {
            $this->info('Building main test...');
            exec('cd ' . $this->getApiDirectory() . ' && cd ../main/ && ng build --configuration test --output-path=wwwtest --output-hashing=all');

            $this->info('Building apply test...');
            exec('cd ' . $this->getApiDirectory() . ' && cd ../apply/ && ng build --configuration test --output-path=wwwtest --output-hashing=all');
        }

        return true;
    }
}
