<?php
namespace Uab\Console\Commands\Run\Services;

use Illuminate\Support\Str;
use Uab\Console\Commands\BaseCommand;
use \Exception;

class RunDeploy extends BaseCommand {
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'run:deploy {--hard} {--install} {--debug} {--step}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deploy project.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     *
     * @throws Exception
     */
    public function handle() {
        $isProd = config('app.env') === 'prod';
        $environment = $isProd ? 'prod' : 'test';

        $shouldContinue = $this->confirm('Detected ' . $environment . ' environment. Do you want to continue?');
        if (!$shouldContinue) {
            return;
        }

        $contentDirectory = config('app.directory');
        if (is_null($contentDirectory) || Str::length($contentDirectory) === 0) {
            throw new Exception('Content directory not set in env.');
        }

        $rootAppDirectory = config('app.root');
        if (is_null($rootAppDirectory) || Str::length($rootAppDirectory) === 0) {
            throw new Exception('Root not set in env.');
        }

        $this->_pullCode();
        $this->_handleLaravel($environment);
        $this->_handleSocket($environment, $contentDirectory);
        $this->_handleMain($rootAppDirectory, $contentDirectory, $environment);
        $this->_handleApply($isProd, $contentDirectory, $environment);

        $this->info('Done');
    }

    /**
     * @param string $command
     */
    private function exec(string $command) {
        $isStep = $this->option('step');
        if ($isStep) {
            $shouldContinue = $this->confirm('Do you want to execute "' . $command . '"?');
            if (!$shouldContinue) {
                return;
            }
        }

        $isDebug = $this->option('debug');
        if ($isDebug) {
            $this->comment('Executing: ' . $command);
        }

        exec($command);
    }

    private function _pullCode():void {
        $this->info('Updating project...');

        $this->exec('cd ' . $this->getApiDirectory() . ' && cd .. && git pull');
    }

    /**
     * @param string $environment
     */
    private function _handleLaravel(string $environment):void {
        $isHard = $this->option('hard');
        $isInstall = $this->option('install');

        $this->info('Copying ' . $environment . ' environment...');

        $this->exec('cd ' . $this->getApiDirectory() . ' && cp -f .env.' . $environment . ' .env');

        if ($isInstall || $isHard) {
            $this->info('Installing Laravel dependencies...');

            if ($isHard) {
                $this->exec('cd ' . $this->getApiDirectory() . ' && rm -rf node_modules/');
            }

            $this->exec('cd ' . $this->getApiDirectory() . ' && npm install');

            $this->info('Installing Laravel frontend dependencies...');

            if ($isHard) {
                $this->exec('cd ' . $this->getApiDirectory() . ' && cd public/ && rm -rf node_modules/');
            }

            $this->exec('cd ' . $this->getApiDirectory() . ' && npm install && cd public/ && npm install');

            $this->info('Installing Laravel backend dependencies...');

            $this->exec('cd ' . $this->getApiDirectory() . ' && composer install --ignore-platform-reqs');
        }

        $this->info('Restarting Laravel services');

        $this->exec('cd ' . $this->getApiDirectory() . ' && php artisan optimize && php artisan queue:restart');
    }

    /**
     * @param string $environment
     * @param string $contentDirectory
     */
    private function _handleSocket(string $environment, string $contentDirectory):void {
        $isHard = $this->option('hard');
        $isInstall = $this->option('install');

        $this->info('Copying socket server env...');

        exec('cd ' . $this->getApiDirectory() . ' && cd ../socket-server/src/assets/ && cp -f config.' . $environment . '.json config.json');

        if ($isInstall || $isHard) {
            $this->info('Installing socket server dependencies...');

            if ($isHard) {
                exec('cd ' . $this->getApiDirectory() . ' && cd ../socket-server && rm -rf node_modules/');
            }

            exec('cd ' . $this->getApiDirectory() . ' && cd ../socket-server && npm install && cd ..');
        }

        $this->info('Restarting supervisord process...');

        $this->exec('cd ' . $this->getApiDirectory() . ' && kill $(ps -ef | grep ' . config('app.name') . '-' . $environment . '-worker | awk \'{print $2}\')');

        $this->exec('/usr/bin/supervisord -c ' . $contentDirectory . '/laravel/supervisord-confs/' . config('app.name') . '-' . $environment . '-worker.ini');
    }

    /**
     * @param bool $isProd
     * @param string $contentDirectory
     * @param string $environment
     */
    private function _handleApply(bool $isProd, string $contentDirectory, string $environment):void {
        $isHard = $this->option('hard');
        $isInstall = $this->option('install');

        if ($isInstall || $isHard) {
            $this->info('Installing Ionic dependencies...');

            if ($isHard) {
                $this->exec('cd ' . $this->getApiDirectory() . ' && cd ../apply && rm -rf node_modules/');
            }

            $this->exec('cd ' . $this->getApiDirectory() . ' && cd ../apply && npm install');
        }

        $this->info('Cleaning old build...');

        $this->exec('cd ' . $this->getApiDirectory() . ' && cd ../../screen' . ($isProd ? '' : 'test') . '.scipe.org && rm -f *.js && rm -f *.html && rm -f *.json && rm -f *.css && rm -f *.woff && rm -f *.ttf && rm -f *.svg && rm -f *.eot && rm -f *.woff2 && rm -fr assets/ && rm -fr svg/');

        $this->info('Moving build...');

        $this->exec('cd ' . $this->getApiDirectory() . ' && cd ../../screen' . ($isProd ? '' : 'test') . '.scipe.org && cp -fr ' . $contentDirectory . '/apply/www' . $environment . '/* .');
    }

    /**
     * @param string $rootAppDirectory
     * @param string $contentDirectory
     * @param string $environment
     */
    private function _handleMain(string $rootAppDirectory, string $contentDirectory, string $environment):void {
        $isHard = $this->option('hard');
        $isInstall = $this->option('install');

        if ($isInstall || $isHard) {
            $this->info('Installing Ionic dependencies...');

            if ($isHard) {
                $this->exec('cd ' . $this->getApiDirectory() . ' && cd ../main && rm -rf node_modules/');
            }

            $this->exec('cd ' . $this->getApiDirectory() . ' && cd ../main && npm install');
        }

        $this->info('Cleaning old build...');

        $this->exec('cd ' . $rootAppDirectory . ' && rm -f *.js && rm -f *.html && rm -f *.json && rm -f *.css && rm -f *.woff && rm -f *.ttf && rm -f *.svg && rm -f *.eot && rm -f *.woff2 && rm -fr assets/ && rm -fr svg/');

        $this->info('Moving build...');

        $this->exec('cd ' . $rootAppDirectory . ' && cp -fr ' . $contentDirectory . '/main/www' . $environment . '/* .');
    }
}
