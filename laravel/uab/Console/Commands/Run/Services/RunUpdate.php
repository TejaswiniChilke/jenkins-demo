<?php
namespace Uab\Console\Commands\Run\Services;

use Uab\Console\Commands\BaseCommand;

class RunUpdate extends BaseCommand {
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'run:update {--hard}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update next dependencies.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $isHard = $this->option('hard');

        if ($isHard) {
            $this->info('Removing backend dependencies');
            exec('cd ' . $this->getApiDirectory() . ' && rm -f composer.lock && rm -rf vendor/ && composer install --ignore-platform-reqs');
        }
        $this->info('Updating backend dependencies');
        exec('cd ' . $this->getApiDirectory() . ' && composer update --ignore-platform-reqs');

        if ($isHard) {
            $this->info('Removing backend NPM dependencies');
            exec('cd ' . $this->getApiDirectory() . ' && rm -f package-lock.json && rm -rf node_modules/ && npm install');
        }
        $this->info('Updating backend NPM dependencies');
        exec('cd ' . $this->getApiDirectory() . ' && npm update && npm audit fix');

        if ($isHard) {
            $this->info('Removing Laravel frontend dependencies');
            exec('cd ' . $this->getApiDirectory() . ' && rm -f package-lock.json && rm -rf node_modules/ && npm install');
        }
        $this->info('Updating Laravel frontend dependencies');
        exec('cd ' . $this->getApiDirectory() . ' && cd public/ && npm update && npm audit fix');

        if ($isHard) {
            $this->info('Removing socket server dependencies');
            exec('cd ' . $this->getApiDirectory() . ' && cd ../socket-server/ && rm -f package-lock.json && rm -rf node_modules/ && npm install');
        }
        $this->info('Updating socket server dependencies');
        exec('cd ' . $this->getApiDirectory() . ' && cd ../socket-server/ && npm update && npm audit fix');

        if ($isHard) {
            $this->info('Removing main app dependencies');
            exec('cd ' . $this->getApiDirectory() . ' && cd ../main/ && rm -f package-lock.json && rm -rf node_modules/ && npm install');
        }
        $this->info('Updating main app dependencies');
        exec('cd ' . $this->getApiDirectory() . ' && cd ../main/ && npm update && npm audit fix');

        if ($isHard) {
            $this->info('Removing screen app dependencies');
            exec('cd ' . $this->getApiDirectory() . ' && cd ../apply/ && rm -f package-lock.json && rm -rf node_modules/ && npm install');
        }
        $this->info('Updating screen app dependencies');
        exec('cd ' . $this->getApiDirectory() . ' && cd ../apply/ && npm update && npm audit fix');

        return true;
    }
}
