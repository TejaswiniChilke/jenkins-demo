<?php
namespace Uab\Console\Commands\Run\Services;

use Symfony\Component\Console\Input\InputOption;
use Uab\Console\Commands\BaseCommand;

class RunServices extends BaseCommand {
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'run:services {--D|debug} {--F|force?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run all of the services required for next.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return bool
     */
    public function handle():bool {
        $this->runInNewTab('php artisan cache:clear && php artisan optimize && php artisan serve --port 8001');
        $this->runInNewTab('php artisan queue:work --queue=high,normal,default,email,text,call,low --tries=3');
        $this->runInNewTab('cd ../socket-server/src/ && forever --minUptime 1000 --spinSleepTime 1000 forever.js');
        $this->runInNewTab('cd ../main/ && ionic serve --port=8100');
        $this->runInNewTab('cd ../apply/ && ionic serve --port=8101');

        return true;
    }

    /**
     * @return array[]
     */
    public function getOptions():array {
        return [
            [
                'debug',
                null,
                InputOption::VALUE_NONE,
                'Add debug variables.',
                false
            ],
            [
                'force',
                null,
                InputOption::VALUE_NONE,
                'Force run services.',
                null
            ]
        ];
    }
}
