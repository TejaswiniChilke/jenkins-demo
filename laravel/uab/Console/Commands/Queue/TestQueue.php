<?php
namespace Uab\Console\Commands\Queue;

use Carbon\Carbon;
use Uab\Console\Commands\BaseCommand;
use Uab\Jobs\Queue\TestQueueEvent;

class TestQueue extends BaseCommand {
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'queue:test {--queue=} {--delay=} {--repeat=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add test event to the queue.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle() {
        $queues = $this->option('queue');
        if (is_null($queues)) {
            $queues = ['default'];
        } else {
            $queues = explode(',', $queues);
        }

        $delay = $this->option('delay');
        if (is_null($delay)) {
            $delay = 0;
        }

        $repeat = $this->option('repeat');
        if (is_null($repeat)) {
            $repeat = 1;
        }

        for ($i = 0; $i < $repeat; $i++) {
            foreach ($queues as $queue) {
                TestQueueEvent::dispatch()
                    ->onQueue($queue)
                    ->delay(Carbon::now()->addSeconds($delay));
            }
        }
    }
}
