<?php
namespace Uab\Console\Commands\Queue;

use Queue;
use Uab\Console\Commands\BaseCommand;

class CountQueue extends BaseCommand {
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'queue:count {--queue=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Count queue size.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle() {
        $queue = $this->option('queue');

        if (is_null($queue)) {
            $message = 'Your queues count: ';

            $this->info($message . Queue::size());
        } else {
            $queues = explode(',', $queue);

            foreach ($queues as $queue) {
                $message = "The '$queue' queue size is: ";

                $this->info($message . Queue::size($queue));
            }
        }
    }
}
