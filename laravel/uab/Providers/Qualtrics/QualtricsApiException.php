<?php
namespace Uab\Providers\Qualtrics;

use \Exception;

class QualtricsApiException extends Exception {
    /**
     * @inheritdoc
     */
    public function __construct($message = "", $code = 0, Exception $previous = null) {
        if (substr($message, 0, 1) == '{') {
            $messageObject = json_decode($message);

            $errorCode = 500;
            if (
                isset($messageObject->meta)
                && isset($messageObject->meta->httpStatus)
            ) {
                $errorCode = $messageObject->meta->httpStatus;
            } else if (
                isset($messageObject->meta)
                && isset($messageObject->meta->error)
                && isset($messageObject->meta->error->errorCode)
            ) {
                $errorCode = $messageObject->meta->error->errorCode;
            }

            $error = 'Qualtrics error';
            if (
                isset($messageObject->meta)
                && isset($messageObject->meta->error)
                && isset($messageObject->meta->error->errorMessage)
            ) {
                $error = $messageObject->meta->error->errorMessage;
            }

            $message = $errorCode . ': ' . $error;

            if (!empty($messageObject->meta->error)) {
                $message .= ' ' . serialize($messageObject->meta->error);
            }
        }

        parent::__construct($message, $code, $previous);
    }
}
