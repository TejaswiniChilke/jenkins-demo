<?php
namespace Uab\Providers\Qualtrics\Providers;

use File;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Arr;
use SplFileInfo;
use Storage;
use Uab\Providers\Qualtrics\QualtricsApiException;
use Uab\Providers\Qualtrics\QualtricsServiceProvider;
use ZipArchive;

class QualtricsRequestServiceProvider {
    /**
     * @var string $debugErrorCode
     *     A Qualtrics API error code to return with every API response.
     *        Used for testing / debugging error handling.
     *        See ERROR_CODE_* constants.
     */
    private $debugErrorCode;

    /**
     * @var string $endpoint
     *        The REST API endpoint.
     */
    protected $endpoint = 'https://co1.qualtrics.com/API/v3';

    /**
     * @var string $apiKey
     *     The Qualtrics API key to authenticate with.
     */
    private $apiKey;

    /**
     * @var string $apiUser
     *        The Qualtrics API username to authenticate with.
     */
    private $apiUser;

    /**
     * @var Client $client
     *        The GuzzleHttp Client.
     */
    protected $client;

    public function __construct($apiKey, $apiUser, $timeout = 10) {
        $this->apiKey = $apiKey;
        $this->apiUser = $apiUser;

        $this->client = new Client([
            'timeout' => $timeout,
        ]);
    }

    /**
     * Makes a request to the Qualtrics API.
     *
     * @param string $method
     *     The REST method to use when making the request.
     * @param string $path
     *     The API path to request.
     * @param array $tokens
     *     Associative array of tokens and values to replace in the path.
     * @param array $parameters
     *     Associative array of parameters to send in the request body.
     *
     * @return object
     *
     * @throws QualtricsApiException
     */
    public function request($method, $path, $tokens = null, $parameters = null) {
        if (is_null($parameters)) {
            $parameters = [];
        }

        if (!empty($tokens)) {
            foreach ($tokens as $key => $value) {
                $path = str_replace('{' . $key . '}', $value, $path);
            }
        }

        $options = [
            'headers' => [
                'X-API-TOKEN' => $this->apiKey,
            ],
        ];

        if (!empty($this->debugErrorCode)) {
            $options['headers']['X-Trigger-Error'] = $this->debugErrorCode;
        }

        if (!empty($parameters)) {
            if ($method == 'GET') {
                $options['query'] = $parameters;
            }
            else {
                $options['json'] = (object) $parameters;
            }
        }

        if (!Arr::has($parameters, 'stream')) {
            $parameters['stream'] = true;
        }

        try {
            $response = $this->client->request($method, $this->endpoint . $path, $options);

            $stream = $response->getBody();

            $data = '';
            while (!$stream->eof()) {
                $data .= $stream->read(1024);
            }

            if (!is_string($data)) {
                return null;
            } else {
                if (strtolower(substr($data, 0, 2)) === 'pk') {
                    $zipName = 'surveys-' . time() . '.zip';

                    Storage::put($zipName, $data);

                    $zipPath = Storage::path($zipName);
                    $unzipPath = storage_path() . '/surveys-'. time();

                    $zip = new ZipArchive();
                    $res = $zip->open($zipPath);
                    if ($res === TRUE) {
                        $zip->extractTo($unzipPath);
                        $zip->close();

                        /** @var SplFileInfo $file */
                        $file = Arr::first(File::files($unzipPath));

                        if (is_null($file)) {
                            $data = null;
                        } else {
                            $data = file_get_contents($file->getRealPath());
                        }

                        File::deleteDirectory($unzipPath);
                    }

                    Storage::delete($zipName);
                }

                return json_decode($data);
            }
        } catch (RequestException $e) {
            $response = $e->getResponse();
            if (!empty($response)) {
                $message = $e->getResponse()->getBody();
            } else {
                $message = $e->getMessage();
            }

            throw new QualtricsApiException($message, $e->getCode(), $e);
        }
    }

    public function getApiUser() {
        return $this->apiUser;
    }

    /**
     * Gets the ID of the data center associated with an API key.
     *
     * @param string $api_key
     *     The Qualtrics API key.
     *
     * @return string
     *     The data center ID.
     */
    private function getDataCenter($api_key) {
        $api_key_parts = explode('-', $api_key);

        return (isset($api_key_parts[1])) ? $api_key_parts[1] : QualtricsServiceProvider::DEFAULT_DATA_CENTER;
    }

    /**
     * Sets a Qualtrics error code to be returned by all requests.
     * Used to test and debug error handling.
     *
     * @param string $errorCode
     *     The Qualtrics error code.
     */
    public function setDebugErrorCode($errorCode) {
        $this->debugErrorCode = $errorCode;
    }
}
