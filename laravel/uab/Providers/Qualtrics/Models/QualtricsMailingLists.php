<?php
namespace Uab\Providers\Qualtrics\Models;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Uab\Providers\Qualtrics\QualtricsApiException;
use Uab\Providers\Qualtrics\QualtricsServiceProvider;

class QualtricsMailingLists {
    const VERSION = '0.1-alpha';
    const DEFAULT_DATA_CENTER = 'co1';

    /**
     * @var Client $client
     *        The GuzzleHttp Client.
     */
    protected $client;

    /**
     * @var string $endpoint
     *        The REST API endpoint.
     */
    protected $endpoint = 'https://co1.qualtrics.com/API/v3';

    /**
     * @var string $apiKey
     *     The Qualtrics API key to authenticate with.
     */
    private $apiKey;

    /**
     * @var string $apiUser
     *        The Qualtrics API username to authenticate with.
     */
    private $apiUser;

    /**
     * @var string $debugErrorCode
     *     A Qualtrics API error code to return with every API response.
     *        Used for testing / debugging error handling.
     *        See ERROR_CODE_* constants.
     */
    private $debugErrorCode;

    /**
     * Qualtrics constructor.
     *
     * @param string $apiKey
     *     The Qualtrics API key.
     *
     * @param string $apiUser
     *     The Qualtrics API username.
     *
     * @param int $timeout
     *     Maximum request time in seconds.
     */
    public function __construct($apiKey, $apiUser, $timeout = 10) {
        $this->apiKey = $apiKey;
        $this->apiUser = $apiUser;

        $this->client = new Client([
            'timeout' => $timeout,
        ]);
    }

    /**
     * Sets a Qualtrics error code to be returned by all requests.
     * Used to test and debug error handling.
     *
     * @param string $errorCode
     *     The Qualtrics error code.
     */
    public function setDebugErrorCode($errorCode) {
        $this->debugErrorCode = $errorCode;
    }

    /**
     * Gets Qualtrics account information for the authenticated account.
     *
     * @return object
     *
     * @throws QualtricsApiException
     * @see https://api.qualtrics.com/docs/get-user
     */
    public function getAccount() {

        $tokens = [
            'userid' => $this->apiUser,
        ];

        return $this->request('GET', '/users/{userid}', $tokens);
    }

    /**
     * Makes a request to the Qualtrics API.
     *
     * @param string $method
     *     The REST method to use when making the request.
     * @param string $path
     *     The API path to request.
     * @param array $tokens
     *     Associative array of tokens and values to replace in the path.
     * @param array $parameters
     *     Associative array of parameters to send in the request body.
     *
     * @return object
     *
     * @throws QualtricsApiException
     */
    protected function request($method, $path, $tokens = null, $parameters = null) {
        if (!empty($tokens)) {
            foreach ($tokens as $key => $value) {
                $path = str_replace('{' . $key . '}', $value, $path);
            }
        }

        // Set default request options with auth header.
        $options = [
            'headers' => [
                'X-API-TOKEN' => $this->apiKey,
            ],
        ];

        // Add trigger error header if a debug error code has been set.
        if (!empty($this->debugErrorCode)) {
            $options['headers']['X-Trigger-Error'] = $this->debugErrorCode;
        }

        if (!empty($parameters)) {
            if ($method == 'GET') {
                // Send parameters as query string parameters.
                $options['query'] = $parameters;
            }
            else {
                // Send parameters as JSON in request body.
                $options['json'] = (object) $parameters;
            }
        }

        try {
            $response = $this->client->request($method, $this->endpoint . $path, $options);
            $data = json_decode($response->getBody());

            return $data;

        }
        catch (RequestException $e) {
            $response = $e->getResponse();
            if (!empty($response)) {
                $message = $e->getResponse()->getBody();
            }
            else {
                $message = $e->getMessage();
            }

            throw new QualtricsApiException($message, $e->getCode(), $e);
        }
    }

    /**
     * Gets the ID of the data center associated with an API key.
     *
     * @param string $api_key
     *     The Qualtrics API key.
     *
     * @return string
     *     The data center ID.
     */
    private function getDataCenter($api_key) {
        $api_key_parts = explode('-', $api_key);

        return (isset($api_key_parts[1])) ? $api_key_parts[1] : QualtricsServiceProvider::DEFAULT_DATA_CENTER;
    }

    /**
     * Gets information about all lists owned by the authenticated account.
     *
     * @param array $parameters
     *     Associative array of optional request parameters.
     *
     * @return object
     *
     * @throws QualtricsApiException
     *
     * @see https://api.qualtrics.com/docs/list-mailing-lists
     */
    public function getMailingLists($parameters = []) {
        return $this->request('GET', '/mailinglists', null, $parameters);
    }

    /**
     * Gets a Qualtrics list.
     *
     * @param string $listId
     *   The ID of the list.
     * @param array $parameters
     *   Associative array of optional request parameters.
     *
     * @return object
     *
     * @throws QualtricsApiException
     *
     * @see https://api.qualtrics.com/docs/get-mailing-list
     */
    public function getMailingList($listId, $parameters = []) {
        $tokens = [
            'mailingListId' => $listId,
        ];

        $this->request('GET', '/mailinglists/{mailingListId}', $tokens, $parameters);

        return QualtricsMailingLists::getMailingLists();
    }
}
