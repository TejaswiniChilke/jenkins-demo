<?php
namespace Uab\Providers\Qualtrics\Enums;

class QualtricsMemberStatusEnum {
    const MEMBER_STATUS_SUBSCRIBED = 'subscribed';
    const MEMBER_STATUS_UNSUBSCRIBED = 'unsubscribed';
}
