<?php
namespace Uab\Providers\Qualtrics\Enums;

class QualtricsDistributionLinkTypeEnum {
    const INDIVIDUAL = 'Individual';
    const MULTIPLE = 'Multiple';
    const ANONYMOUS = 'Anonymous';
}
