<?php
namespace Uab\Providers\Qualtrics\Enums;

class QualtricsErrorCodeEnum {
    const ERROR_CODE_BAD_REQUEST = 'BadRequest';
    const ERROR_CODE_INVALID_ACTION = 'InvalidAction';
    const ERROR_CODE_INVALID_RESOURCE = 'InvalidResource';
    const ERROR_CODE_JSON_PARSE_ERROR = 'JSONParseError';
    const ERROR_CODE_API_KEY_MISSING = 'APIKeyMissing';
    const ERROR_CODE_API_KEY_INVALID = 'APIKeyInvalid';
    const ERROR_CODE_FORBIDDEN = 'Forbidden';
    const ERROR_CODE_USER_DISABLED = 'UserDisabled';
    const ERROR_CODE_WRONG_DATACENTER = 'WrongDatacenter';
    const ERROR_CODE_RESOURCE_NOT_FOUND = 'ResourceNotFound';
    const ERROR_CODE_METHOD_NOT_ALLOWED = 'MethodNotAllowed';
    const ERROR_CODE_RESOURCE_NESTING_TOO_DEEP = 'ResourceNestingTooDeep';
    const ERROR_CODE_INVALID_METHOD_OVERRIDE = 'InvalidMethodOverride';
    const ERROR_CODE_REQUESTED_FIELDS_INVALID = 'RequestedFieldsInvalid';
    const ERROR_CODE_TOO_MANY_REQUESTS = 'TooManyRequests';
    const ERROR_CODE_INTERNAL_SERVER_ERROR = 'InternalServerError';
    const ERROR_CODE_COMPLIANCE_RELATED = 'ComplianceRelated';
}
