<?php
namespace Uab\Providers\Qualtrics\Enums;

class QualtricsLibraryCategoryEnum {
    const INVITE = 'invite';
    const INACTIVE_SURVEY = 'inactiveSurvey';
    const REMINDER = 'reminder';
    const THANK_YOU = 'thankYou';
    const END_OF_SURVEY = 'endOfSurvey';
    const GENERAL = 'general';
    const LOOK_AND_FEEL = 'lookAndFeel';
    const EMAIL_SUBJECT = 'emailSubject';
    const SMS_INVITE = 'smsInvite';
}
