<?php
namespace Uab\Providers\Qualtrics;

use stdClass;
use Uab\Providers\Qualtrics\Providers\QualtricsRequestServiceProvider;

class QualtricsServiceProvider {
    const VERSION = '0.1-alpha';
    const DEFAULT_DATA_CENTER = 'co1';

    private $requestService;

    /**
     * Qualtrics constructor.
     *
     * @param int $timeout
     *     Maximum request time in seconds.
     */
    public function __construct($timeout = 10) {
        $apiKey = config('qualtrics.api.key');
        $apiUser = config('qualtrics.api.user');

        $this->requestService = new QualtricsRequestServiceProvider($apiKey, $apiUser, $timeout);
    }

    /**
     * Gets Qualtrics account information for the authenticated account.
     *
     * @return object
     *
     * @throws QualtricsApiException
     *
     * @see https://api.qualtrics.com/docs/get-user
     */
    public function getAccount() {
        $tokens = [
            'userid' => $this->requestService->getApiUser(),
        ];

        return $this->requestService->request('GET', '/users/{userid}', $tokens);
    }

    /**
     * Gets information about all lists owned by the authenticated account.
     *
     * @param array $parameters
     *     Associative array of optional request parameters.
     *
     * @return object
     *
     * @throws QualtricsApiException
     *
     * @see https://api.qualtrics.com/docs/list-mailing-lists
     */
    public function getMailingLists($parameters = []) {
        return $this->requestService->request('GET', '/mailinglists', null, $parameters);
    }

    /**
     * Gets a Qualtrics list.
     *
     * @param string $listId
     *   The ID of the list.
     * @param array $parameters
     *   Associative array of optional request parameters.
     *
     * @return object
     *
     * @throws QualtricsApiException
     *
     * @see https://api.qualtrics.com/docs/get-mailing-list
     */
    public function getMailingList($listId, $parameters = []) {
        $tokens = [
            'mailingListId' => $listId,
        ];

        return $this->requestService->request('GET', '/mailinglists/{mailingListId}', $tokens, $parameters);
    }

    /**
     * Gets information about all members of a Qualtrics list.
     *
     * @param string $list_id
     *   The ID of the list.
     * @param array $parameters
     *   Associative array of optional request parameters.
     *
     * @return object
     *
     * @throws QualtricsApiException
     *
     * @see https://api.qualtrics.com/docs/list-contacts
     */
    public function getMailingListContacts($list_id, $parameters = []) {
        $tokens = [
            'mailingListId' => $list_id,
        ];

        return $this->requestService->request('GET', '/mailinglists/{mailingListId}/contacts', $tokens, $parameters);
    }

    /**
     * Gets information about a contact of a Qualtrics list.
     *
     * @param string $list_id
     *   The ID of the list.
     * @param string $contact_id
     *   Contact id
     * @param array $parameters
     *   Associative array of optional request parameters.
     *
     * @return object
     *
     * @throws QualtricsApiException
     *
     * @see https://api.qualtrics.com/docs/get-contact
     */
    public function getMailingListContactInfo($list_id, $contact_id, $parameters = []) {
        $tokens = [
            'mailingListId' => $list_id,
            'contactId' => $contact_id,
        ];

        return $this->requestService->request('GET', '/mailinglists/{mailingListId}/contacts/{contactId}', $tokens, $parameters);
    }

    /**
     * Gets information about all templates owned by the authenticated account.
     *
     * @param array $parameters
     *   Associative array of optional request parameters.
     *
     * @return stdClass[]
     *
     * @throws QualtricsApiException
     *
     * @see https://api.qualtrics.com/docs/list-surveys
     */
    public function getSurveys($parameters = []) {
        $response = $this->requestService->request('GET', '/surveys', NULL, $parameters);

        $surveys = [];

        if (isset($response->result)) {
            if (is_array($response->result->elements)) {
                $surveys = $response->result->elements;
            }
        }

        return $surveys;
    }

    /**
     * Gets information a specific survey.
     *
     * @param string $surveyId
     *   The ID of the template.
     * @param array $parameters
     *   Associative array of optional request parameters.
     *
     * @return stdClass
     *
     * @throws QualtricsApiException
     *
     * @see https://api.qualtrics.com/docs/get-survey
     */
    public function getSurvey($surveyId, $parameters = []) {
        $tokens = [
            'surveyId' => $surveyId,
        ];

        $response = $this->requestService->request('GET', '/surveys/{surveyId}', $tokens, $parameters);

        $survey = null;
        if (isset($response->result)) {
            $survey = $response->result;
        }

        return $survey;
    }

    /**
     * @param string $surveyId
     * @param array  $parameters
     *
     * @return array|object
     *
     * @throws QualtricsApiException
     */
    public function exportResponses($surveyId, $parameters = []) {
        $tokens = [
            'surveyId' => $surveyId,
        ];

        $response = $this->requestService->request(
            'POST',
            '/surveys/'.$surveyId.'/export-responses',
            $tokens,
            $parameters
        );

        if (isset($response->result)) {
            $response = $response->result;
        }

        return $response;
    }

    /**
     * @param string  $surveyId
     * @param string  $progressId
     * @param mixed[] $parameters
     *
     * @return object
     *
     * @throws QualtricsApiException
     */
    public function checkExportProgress($surveyId, $progressId, $parameters = []) {
        $tokens = [
            'progressId' => $progressId,
            'surveyId'   => $surveyId,
        ];

        $response = $this->requestService->request(
            'GET',
            '/surveys/'.$surveyId.'/export-responses/'.$progressId,
            $tokens,
            $parameters
        );

        if (isset($response->result)) {
            $response = $response->result;
        }

        return $response;
    }

    /**
     * @param string  $surveyId
     * @param string  $fileId
     * @param mixed[] $parameters
     *
     * @return object
     *
     * @throws QualtricsApiException
     */
    public function getResponseExportFile($surveyId, $fileId, $parameters = []) {
        $tokens = [
            'fileId'   => $fileId,
            'surveyId' => $surveyId
        ];

        $response = $this->requestService->request(
            'GET',
            '/surveys/'.$surveyId.'/export-responses/'.$fileId.'/file',
            $tokens,
            $parameters
        );

        if (isset($response->result)) {
            $response = $response->result;
        }

        return $response;
    }
}
