<?php
namespace Uab\Providers;

use Uab\Http\Enums\AddressProvidersEnum;
use Uab\Providers\AddressProviders\GoogleAddressServiceProvider;
use Uab\Providers\AddressProviders\UspsServiceProvider;

class AddressServiceProvider {
    public function __construct() {

    }

    public function getProvider($provider) {
        switch ($provider) {
            case AddressProvidersEnum::GOOGLE:
                return new GoogleAddressServiceProvider();
            case AddressProvidersEnum::USPS:
                return new UspsServiceProvider();
            default:
                return null;
        }
    }

    public function validateAddress($addressArray, $provider = null) {
        $address = null;
        if ($provider === null || $provider === AddressProvidersEnum::USPS) {
            $uspsService = new UspsServiceProvider();

            $address = $uspsService->validateAddress($addressArray);
        }

        if (is_null($address) || AddressProvidersEnum::GOOGLE) {
            if (!is_null(config('google.key', null))) {
                $googleService = new GoogleAddressServiceProvider();

                $address = $googleService->validateAddress($addressArray);
            }
        }

        return $address;
    }
}
