<?php
namespace Uab\Providers;

use Illuminate\Support\Collection;
use Uab\Http\Enums\UserTypesEnum;
use Uab\Http\Models\Followers;
use Uab\Http\Models\Users;

class FriendServiceProvider {
    /**
     * @param Users $user
     *
     * @return Users[]
     */
    public function getFriends($user) {
        $userService = new UsersServiceProvider();

        if ($userService->isAdmin($user)) {
            $friends = Users::all();
        } else {
            $friends = $userService->getUsersByUserTypeNames(
                [
                    UserTypesEnum::ADMIN
                ]
            );

            /** @var Followers[]|Collection $candidates */
            $candidates = Followers::query()
                ->where(
                    [
                        'following_id' => $user->id
                    ]
                )->get();

            foreach ($candidates as $candidate) {
                if ($this->isFriends($user->id, $candidate->user_id)) {
                    $friends->add($candidate->user());
                }
            }

            $userService = new UsersServiceProvider();
            $admin = $userService->getUsersByUserTypeNames(UserTypesEnum::ADMIN);

            $candidates->merge($admin);
        }

        $friends = $friends->sort(
            function(Users $a, Users $b) {
                return (strtolower($a->getDisplayNameAttribute()) < strtolower($b->getDisplayNameAttribute())) ? -1 : 1;
            }
        );

        $friends = $friends->values();

        return $friends;
    }

    public function isFriends($aId, $bId) {
        return $this->isFollowing($aId, $bId) && $this->isFollowing($bId, $aId);
    }

    public function isFollowing($aId, $bId) {
        $exists = false;

        if ($aId !== $bId) {
            $query = Followers::query()
                ->where(
                    [
                        'user_id' => $aId,
                        'following_id' => $bId,
                    ]
                );

            $exists = $query->exists();
        }

        return $exists;
    }

    public function isFollowedBy($aId, $bId) {
        return $this->isFollowing($bId, $aId);
    }
}
