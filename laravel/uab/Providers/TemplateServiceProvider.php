<?php
namespace Uab\Providers;

use Illuminate\Support\Str;

class TemplateServiceProvider {
    public function __construct() {

    }

    public function parse($text, ...$parameter) {
        $matches = [];

        // TODO: Allow custom prefix and suffix ie <% %>
        preg_match_all('/<%\s*[a-zA-Z]+@[a-zA-Z]+\s*%>/', $text, $matches);

        if (count($matches) > 0) {
            foreach ($matches as $match) {
                if (is_array($match) && count($match) === 1) {
                    $match = $match[0];
                    $parts = explode('@', trim(str_replace('<%', '', str_replace('%>', '', $match))));

                    if (count($parts) === 2) {
                        $serviceName = $parts[0];
                        $functionName = $parts[1];

                        // TODO: Check for namespace
                        // TODO: Get namespace prefix from settings

                        $servicePath = 'Uab\\Providers\\' . Str::studly($serviceName);

                        if (class_exists($servicePath, true)) {
                            $service = new $servicePath;

                            if (method_exists($service, $functionName) && is_callable(array($service, $functionName))) {
                                $value = call_user_func([$service, $functionName], ...$parameter);

                                $text = str_replace($match, $value, $text);
                            }
                        }
                    }
                }
            }

            return $text;
        }
    }
}
