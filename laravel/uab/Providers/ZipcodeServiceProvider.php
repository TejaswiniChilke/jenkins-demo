<?php
namespace Uab\Providers;

use Illuminate\Support\Arr;
use Uab\Http\Enums\QueueTypesEnum;
use Uab\Http\Models\Zipcodes;
use Uab\Jobs\UpdateZipcode;

class ZipcodeServiceProvider {
    public function __construct() {

    }

    public function updatePopulationData() {
        $path = storage_path('zipcode-populations.csv');

        $handle = fopen($path, 'r');

        $headerCount = 0;

        while (!feof($handle)) {
            while ($csvLine = fgetcsv($handle)) {
                if ($headerCount === 0) {
                    $headerCount++;
                } else {
                    $zipcode = Arr::get($csvLine, 0, null);
                    $population = Arr::get($csvLine, 1, null);

                    if (is_numeric($population)) {
                        if (is_string($population)) {
                            $population = intval($population);
                        }
                    } else {
                        $population = null;
                    }

                    $model = new Zipcodes();
                    $model->zipcode = $zipcode;
                    $model->population = $population;

                    UpdateZipcode::dispatch($model)->onQueue(QueueTypesEnum::LOW);
                }
            }

            fclose($handle);
        }
    }
}
