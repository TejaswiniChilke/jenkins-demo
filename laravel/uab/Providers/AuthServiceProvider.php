<?php

namespace Uab\Providers;

use Laravel\Passport\Passport;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider {
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot() {
        $this->registerPolicies();

        Passport::routes();

        if (config('auth.options.lifetime')) {
            Passport::personalAccessTokensExpireIn(now()->addCentury());
            Passport::refreshTokensExpireIn(now()->addCentury());
            Passport::tokensExpireIn(now()->addCentury());
        } else {
            $refreshTime = config('auth.options.refresh');

            Passport::personalAccessTokensExpireIn(now()->addMinutes($refreshTime));
            Passport::refreshTokensExpireIn(now()->addMinutes($refreshTime));
            Passport::tokensExpireIn(now()->addMinutes($refreshTime));
        }
    }
}
