<?php
namespace Uab\Providers;

use DB;

class SettingsServiceProvider {
    public function getSettings($userId, $userTypeId) {
        $modelService = new ModelServiceProvider();

        if (is_null($userId)) {
            $activeUser = Auth::user();

            if (!is_null($activeUser)) {
                $userId = $activeUser->id;
            }
        }

        $model = $modelService->getModel('settings');

        $query = DB::table($modelService->getAliasAs($model));

        $joins = [];

        if (!is_null($userId)) {
            $joins[] = 'settings_users.users';

            $usersSettingsTable = 'settings_users';
            $usersSettingsAlias = $modelService->getAlias($usersSettingsTable);

            $usersTable = 'users';
            $usersAlias = $modelService->getAlias($usersTable);

            $settingsTable = 'settings';
            $settingsAlias = $modelService->getAlias($settingsTable);

            $query->leftJoin(
                $usersSettingsTable . ' AS ' . $usersSettingsAlias,
                function ($join) use ($settingsAlias, $userId, $usersSettingsAlias) {
                    $join->on($settingsAlias . '.id', '=', $usersSettingsAlias . '.setting_id');
                    $join->on($usersSettingsAlias . '.user_id', '=', DB::raw($userId));
                }
            );

            $query->leftJoin(
                $usersTable . ' AS ' . $usersAlias,
                $usersSettingsAlias . '.user_id',
                '=',
                $usersAlias . '.id'
            );
        }

        if (is_null($userTypeId)) {
            $activeUser = Auth::user();

            if (!is_null($activeUser)) {
                $userTypeId = $activeUser->user_type_id;
            }
        }

        if (!is_null($userTypeId)) {
            $joins[] = 'settings_user_types.user_types';

            $userTypesSettingsTable = 'settings_user_types';
            $userTypesSettingsAlias = $modelService->getAlias($userTypesSettingsTable);

            $userTypeTable = 'user_types';
            $userTypeAlias = $modelService->getAlias($userTypeTable);

            $settingsTable = 'settings';
            $settingsAlias = $modelService->getAlias($settingsTable);

            $query->leftJoin(
                $userTypesSettingsTable . ' AS ' . $userTypesSettingsAlias,
                function ($join) use ($settingsAlias, $userTypeId, $userTypesSettingsAlias) {
                    $join->on($settingsAlias . '.id', '=', $userTypesSettingsAlias . '.setting_id');
                    $join->on($userTypesSettingsAlias . '.user_type_id', '=', DB::raw($userTypeId));
                }
            );

            $query->leftJoin(
                $userTypeTable . ' AS ' . $userTypeAlias,
                $userTypesSettingsAlias . '.user_type_id',
                '=',
                $userTypeAlias . '.id'
            );
        }

        $fields = $modelService->getFields($model, [], $joins);

        $query->select($fields)->distinct();

        $queryService = new QueryServiceProvider();

        $results = $queryService->collectionToModel($query, $model);
        $results = $queryService->slugIds($model, $results);

        $results = array_key_exists('settings', $results) ? $results['settings'] : [];

        $settings = [];
        foreach ($results as $setting) {
            $setting['setting_id'] = $setting['id'];
            unset($setting['id']);

            switch ($setting['setting_type']) {
                case 'user':
                    if (is_null($userId)) {
                        $setting = null;
                    } else {
                        if (!array_key_exists('settings_users', $settings) || count($setting['settings_users']) == 0) {
                            $setting['user_id'] = $userId;
                            $setting['value'] = $setting['default_value'];
                        } else {
                            $setting['id'] = $setting['settings_users']['id'];
                            $setting['user_id'] = $setting['settings_users']['user_id'];
                            $setting['value'] = $setting['settings_users']['value'];
                        }

                        $setting['user_id'] = $userId;
                    }
                    break;
                case 'user_type':
                    if (is_null($userTypeId)) {
                        $setting = null;
                    } else {
                        if (!array_key_exists('settings_user_types', $settings) || count($setting['settings_user_types']) == 0) {
                            $setting['user_type_id'] = $userTypeId;
                            $setting['value'] = $setting['default_value'];
                        } else {
                            $setting['id'] = $setting['settings_user_types']['id'];
                            $setting['user_type_id'] = $setting['settings_user_types']['user_type_id'];
                            $setting['value'] = $setting['settings_user_types']['value'];
                        }

                        $setting['user_type_id'] = $userTypeId;
                    }
                    break;
                default:
                    $setting['value'] = $setting['default_value'];
                    break;
            }

            if (is_null($setting)) {
                unset($setting);
                continue;
            }

            if ($setting['value_type'] === 'boolean') {
                $setting['value'] = $setting['value'] == 'true' || $setting['value'] == '1';
            }

            if (array_key_exists('settings_users', $setting)) {
                unset($setting['settings_users']);
            }

            if (array_key_exists('settings_user_types', $setting)) {
                unset($setting['settings_user_types']);
            }

            unset($setting['created']);
            unset($setting['modified']);

            $settings[] = $setting;
        }

        return $settings;
    }
}
