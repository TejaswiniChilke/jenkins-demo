<?php
namespace Uab\Providers;

use Uab\Http\Models\Base\BaseModel;
use Uab\Http\Models\PostAttachments;

class PostAttachmentServiceProvider {
    /**
     * @param PostAttachments $attachment
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|BaseModel|null
     */
    public function getReferenceObject($attachment) {
        $service = new ModelServiceProvider();

        $table = $attachment->getAttributeValue('reference_key');
        $id = $attachment->getAttributeValue('reference_value');

        return $service->getObject($table, $id);
    }
}
