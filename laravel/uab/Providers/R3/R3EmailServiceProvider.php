<?php
namespace Uab\Providers\R3;

use Uab\Http\Enums\QueueTypesEnum;
use Uab\Http\Enums\UserTypesEnum;
use Uab\Http\Models\Emails;
use Uab\Http\Models\Users;
use Uab\Jobs\Emails\SendEmail;
use Uab\Providers\EmailServiceProvider;

class R3EmailServiceProvider extends EmailServiceProvider {
    /**
     * @param Users $user
     */
    public function sendAcceptedEmail(Users $user) {
        $url = config('app.frontend') . '/#/set-password/' . $user->slug();

        $message = "<div style=\"font-size:16px\">Welcome to the SCIPE study!"
                    . "<br/><br/>"
                    . "Please click the link and follow the steps below to get access to the SCIPE website: " . $url
                    . "<br/><br/>"
                    . "The detailed steps are described below:<br/>"
                    . "Step 1: Create a <b>password</b>. Your user ID is preset as your email address.<br/>"
                    . "Step 2: <b>Log in</b> to the website.<br/>"
                    . "Step 3: Create a <b>User Profile</b>.<br/>"
                    . "Step 4: Complete <b>Function and Pain survey</b> (*This MUST be completed so that we can recommend an appropriate program for you!).<br/>"
                    . "Step 5: Set-up <b>Calendar Reminders</b><br/>"
                    . "Step 6: Now, you are all set to participate in the SCIPE program! Welcome to the SCIPE study!<br/><br/>";

        if ($user->userType()->name === UserTypesEnum::CONTROL) {
            $message .= 'You will receive a new article every week for the next 8 weeks. We recommend that you read the weekly article.';
        } else {
            $message .= 'You will receive a new exercise video playlist and article every week for the next 8 weeks. We recommend that you read the article and exercise with the videos 3 times per week. The intensity of exercise will gradually increase throughout the program. ';
        }

        $email = new Emails();
        $email->subject = 'Congratulations! We would like to invite you as a new SCIPE user';
        $email->message = $message;
        $email->to_user_id = $user->id;
        $email->to_email = $user->email;
        $email->save();

        SendEmail::dispatch($email)->onQueue(QueueTypesEnum::EMAIL);
    }
}
