<?php
namespace Uab\Providers;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Uab\Http\Models\AnalyticVideos;
use Uab\Http\Models\Playlists;
use Uab\Http\Models\Users;
use Uab\Http\Models\Videos;

class VideoServiceProvider {
    /**
     * @param integer $videoId
     *
     * @return Collection<Playlists>
     */
    public function getPlaylists($videoId) {
        /** @var Builder $query */
        $query = Playlists::query()
            ->select('playlists.*')
            ->rightJoin(
                'playlists_videos',
                'playlists.id',
                '=',
                'playlists_videos.playlist_id'
            )->where(
                [
                    'playlists_videos.video_id' => $videoId
                ]
            );

        return $query->get();
    }

    /**
     * @param string|int $userId
     * @param string|int $videoId
     * @param Carbon|string|null $start
     * @param Carbon|string|null $end
     *
     * @return bool
     */
    public function hasWatchedVideo($userId, $videoId, $start = null, $end = null):bool {
        $videoId = Videos::safeDecodeStatic($videoId);
        $userId = Users::safeDecodeStatic($userId);

        $query = AnalyticVideos::query()
            ->where(
                [
                    'analytic_videos.video_id' => $videoId,
                    'analytic_videos.user_id'  => $userId
                ]
            );

        if (!is_null($start)) {
            $query->where('watch_datetime', '>=', $start);
        }

        if (!is_null($end)) {
            $query->where('watch_datetime', '<=', $end);
        }

        // TODO
        // if (!is_null($start)) {
            // $query->where(
            //     'analytic_videos.watch_datetime',
            //     '>=',
            //     'CURDATE() - INTERVAL DAYOFWEEK(CURDATE())-'.$days.' DAY'
            // );
        // }

        return $query->exists();
    }
}
