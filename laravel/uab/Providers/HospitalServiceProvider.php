<?php
namespace Uab\Providers;

use Carbon\Carbon;
use Illuminate\Support\Arr;
use Uab\Http\Enums\QueueTypesEnum;
use Uab\Http\Models\Hospitals;
use Uab\Jobs\UpdateHospitalJob;

class HospitalServiceProvider {
    public function __construct() {

    }

    public function updateHospitalData() {
        $urlToDownload = 'https://opendata.arcgis.com/datasets/6ac5e325468c4cb9b905f1728d6fbf0f_0.csv';
//        $urlToDownload = storage_path('app/public/testing_center_test_data.csv');

        $filename = 'temp-hospital-data-' . uniqid() . '.csv';
        $tempFile = tempnam(storage_path(), $filename);
        copy($urlToDownload, $tempFile);

        $handle = fopen($tempFile, 'r');

        $headerCount = 0;

        while(!feof($handle)) {
            while ($csvLine = fgetcsv($handle)) {
                if ($headerCount === 0) {
                    $headerCount++;
                } else {
//                    $x = Arr::get($csvLine, 0, null);
//                    $y = Arr::get($csvLine, 1, null);
                    $external_object_id = Arr::get($csvLine, 2, null);
                    $external_id = Arr::get($csvLine, 3, null);
                    $name = Arr::get($csvLine, 4, null);
                    $address_line_one = Arr::get($csvLine, 5, null);
                    $city = Arr::get($csvLine, 6, null);
                    $state = Arr::get($csvLine, 7, null);
                    $zipcode = Arr::get($csvLine, 8, null);

                    $zipcode_four = Arr::get($csvLine, 9, null);
                    $zipcode_four = $zipcode_four === 'NOT AVAILABLE' ? null : $zipcode_four;

                    $telephone = Arr::get($csvLine, 10, null);
                    $telephone = $telephone === 'NOT AVAILABLE' ? null : $telephone;

                    $type = Arr::get($csvLine, 11, null);

                    $is_open = Arr::get($csvLine, 12, null);
                    $is_open = (is_null($is_open)) ? null : strtolower($is_open) === 'open';

                    $population = Arr::get($csvLine, 13, null);
                    $county = Arr::get($csvLine, 14, null);
                    $county_fips = Arr::get($csvLine, 15, null);
                    $country = Arr::get($csvLine, 16, null);
                    $latitude = Arr::get($csvLine, 17, null);
                    $longitude = Arr::get($csvLine, 18, null);
                    $naics_code = Arr::get($csvLine, 19, null);
                    $naics_description = Arr::get($csvLine, 20, null);
                    $source = Arr::get($csvLine, 21, null);

                    $source_date = Arr::get($csvLine, 22, null);
                    $source_date = is_null($source_date) ? null : Carbon::parse($source_date);

                    $validation_method = Arr::get($csvLine, 23, null);

                    $validation_date = Arr::get($csvLine, 24, null);
                    $validation_date = is_null($validation_date) ? null : Carbon::parse($validation_date);

                    $website = Arr::get($csvLine, 25, null); // TODO: Screenshot websites

//                    $state_id = Arr::get($csvLine, 26, null);

                    $alternative_name = Arr::get($csvLine, 27, null);
                    $alternative_name = $alternative_name === 'NOT AVAILABLE' ? null : $alternative_name;

//                    $state_fips = Arr::get($csvLine, 28, null);

                    $owner = Arr::get($csvLine, 29, null);

                    $total_staff = Arr::get($csvLine, 30, null);
                    $total_staff = is_null($total_staff) || intval($total_staff) <= 0 ? null : $total_staff;

                    $beds = Arr::get($csvLine, 31, null);

                    $trauma = Arr::get($csvLine, 32, null);
                    $trauma = $trauma === 'NOT AVAILABLE' ? null : $trauma;

                    $has_helipad = Arr::get($csvLine, 33, null);
                    $has_helipad = (is_null($has_helipad)) ? null : strtolower($has_helipad) === 'y';

                    if (!is_null($city) && !is_null($name) && !is_null($state)) {
                        $hospital = new Hospitals();
                        $hospital->external_object_id = $external_object_id;
                        $hospital->external_id = $external_id;
                        $hospital->name = $name;
                        $hospital->address_line_one = $address_line_one;
                        $hospital->city = $city;
                        $hospital->state = $state;
                        $hospital->zipcode = $zipcode;
                        $hospital->zipcode_four = $zipcode_four;
                        $hospital->telephone = $telephone;
                        $hospital->type = $type;
                        $hospital->is_open = $is_open;
                        $hospital->population = $population;
                        $hospital->county = $county;
                        $hospital->country = $country;
                        $hospital->county_fpis = $county_fips;
                        $hospital->latitude = $latitude;
                        $hospital->longitude = $longitude;
                        $hospital->naics_code = $naics_code;
                        $hospital->naics_description = $naics_description;
                        $hospital->source = $source;
                        $hospital->source_date = $source_date;
                        $hospital->validation_method = $validation_method;
                        $hospital->validation_date = $validation_date;
                        $hospital->website = $website;
                        $hospital->alternative_name = $alternative_name;
                        $hospital->owner = $owner;
                        $hospital->total_staff = $total_staff;
                        $hospital->beds = $beds;
                        $hospital->trauma = $trauma;
                        $hospital->has_helipad = $has_helipad;

                        UpdateHospitalJob::dispatch($hospital)->onQueue(QueueTypesEnum::NORMAL);
                    }
                }
            }
        }

        fclose($handle);
    }
}
