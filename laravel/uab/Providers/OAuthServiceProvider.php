<?php
namespace Uab\Providers;

use Carbon\Carbon;
use Illuminate\Support\Str;
use Laravel\Passport\Passport;
use Laravel\Passport\Token;
use League\OAuth2\Server\Exception\OAuthServerException;
use Uab\Exceptions\UserStatusApprovalException;
use Uab\Exceptions\UserTypeMissingException;
use Uab\Http\Enums\QueueTypesEnum;
use Uab\Http\Enums\UserStatusesEnum;
use Uab\Http\Models\Users;
use Uab\Jobs\Auth\LoginAttemptJob;

class OAuthServiceProvider {
    /**
     * @param Users $user
     */
    private function checkStatus($user) {
        $status = $user->getAttributeValue('status');

        $notAllowedStatuses = [
            UserStatusesEnum::APPLICATION_DENIED,
            UserStatusesEnum::APPLICATION_PENDING,
            UserStatusesEnum::APPLICATION_REJECTED_AGE,
            UserStatusesEnum::APPLICATION_REJECTED_SCI,
            UserStatusesEnum::DEACTIVATED
        ];

        if (in_array($status, $notAllowedStatuses)) {
            throw new UserStatusApprovalException($status);
        }
    }

    /**
     * @param Users $user
     */
    private function checkUserType($user) {
        $hasUserType = !is_null($user->user_type_id);

        if (!$hasUserType) {
            throw new UserTypeMissingException();
        }
    }

    /**
     * @param Users|null $user
     */
    public function logout($user = null) {
        if (is_null($user)) {
            $user = Auth::user();
        }

        if (!is_null($user)) {
            $user->logout_count++;
            $user->save();

            $user->token()->revoke();
        }
    }

    /**
     * @param $value
     * @param string $columns
     *
     * @return Users|null
     *
     * @throws OAuthServerException
     */
    public function findActiveUser($value, $columns = 'username') {
        if (!is_array($columns)) {
            $columns = [
                $columns
            ];
        }

        $success = false;
        $activeUser = null;

        try {
            if (is_null($value)) {
                throw new OAuthServerException(
                    Str::ucfirst(implode(',', $columns)).' is required to login.',
                    6,
                    'invalid_credentials',
                    401
                );
            }

            foreach ($columns as $column) {
                $query = Users::query()->where($column, '=', $value);

                /** @var Users $activeUser */
                $activeUser = $query->first();

                if (!is_null($activeUser)) {
                    break;
                }
            }

            if (is_null($activeUser)) {
                throw new OAuthServerException(
                    'Incorrect username or password.',
                    6,
                    'invalid_credentials',
                    401
                );
            }

            $this->checkStatus($activeUser);
            $this->checkUserType($activeUser);

            $success = true;

            $activeUser->login_count++;
            $activeUser->save();
        } finally {
            if (!is_null($value) && $success === false) {
                $this->recordLoginAttempt($value, false);
            }
        }

        return $activeUser;
    }

    public function login($value, $conditions) {
        $query = Users::query()
            ->where($conditions);

        $user = $query->first();

        $this->recordLoginAttempt($value, $query->exists());

        return $user;
    }

    /**
     * @param string  $username
     * @param boolean $successful
     */
    public function recordLoginAttempt($username, $successful) {
        LoginAttemptJob::dispatch($username, $successful)->onQueue(QueueTypesEnum::LOW);
    }

    public function findAndValidateUser($value, $findColumns, $expectedValues) {
        $user = $this->findActiveUser($value, $findColumns);

        $isValid = $this->validateUser($user, $expectedValues);

        return $isValid ? $user : null;
    }

    public function validateUser($user, $expectedValues) {
        $expectedValues['id'] = $user->id;

        $isValid = Users::query()
            ->where($expectedValues)
            ->exists();

        if ($isValid) {
            $token = new Token();
            $token->id = Str::random(60);
            $token->user_id = $user->id;
            $token->client_id = 3;
            $token->expires_at = Passport::$personalAccessTokensExpireAt;
            $token->scopes = '[]';
            $token->name = null;
            $token->created_at = Carbon::now();
            $token->updated_at = Carbon::now();

            $isValid = $token->save();
        }

        if ($isValid) {
            $user->withAccessToken($token);

            Auth::login($user);
        }

        return $isValid;
    }

    public function renewTokenExpirationTime() {
        $user = Auth::user();

        $minutesToAdd = config('AUTH_TOKEN_LIFETIME_REFRESH_TIME');

        $user->token()->forceFill(
            [
                'expires_at' => now()->addMinutes($minutesToAdd)
            ]
        )->save();
    }
}
