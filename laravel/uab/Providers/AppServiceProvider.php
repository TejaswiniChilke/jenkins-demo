<?php
namespace Uab\Providers;

use Illuminate\Database\Eloquent\Builder;
use Laravel\Dusk\DuskServiceProvider;
use Uab\Http\Enums\QueueTypesEnum;
use Uab\Http\Models\SqlLogs;
use Uab\Http\Response\ResponseMacroFactory;
use Uab\Jobs\Base\InsertObjectJob;
use DB;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {
    const PREFIXES_TO_LOG = [
        'INSERT',
        'DELETE',
        'REPLACE',
        'UPDATE'
    ];

    const TABLES_TO_IGNORE = [
        '`sql_logs`',
        '`jobs`',
        '`failed_jobs`',
        '`telescope_entries`',
        '`telescope_entries_tags`',
    ];

    /**
     * @param $query
     */
    private function _logQuery($query) {
        if ($this->_shouldLogQuery($query->sql)) {
            $log = new SqlLogs();

            $user = Auth::user();
            if (!is_null($user)) {
                $log->user_id = $user->id;
            }

            $log->query = $query->sql;
            $log->params = json_encode($query->bindings);
            $log->executed_at = $query->time;

            InsertObjectJob::dispatch($log)->onQueue(QueueTypesEnum::LOW);
        }
    }

    private function _shouldLogQuery($sql) {
        $hasPrefix = false;
        foreach (self::PREFIXES_TO_LOG as $prefix) {
            if (strpos(strtoupper(trim($sql)), strtoupper($prefix)) === 0) {
                $hasPrefix = true;
            }
        }

        if ($hasPrefix) {
            foreach (self::TABLES_TO_IGNORE as $table) {
                if (strpos(strtoupper(trim($sql)), strtoupper($table)) !== false) {
                    return false;
                }
            }
        }

        return $hasPrefix;
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        DB::listen(
            function($query) {
                $this->_logQuery($query);
            }
        );

        $responseMacroFactory = new ResponseMacroFactory();
        $responseMacroFactory->boot();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        if ($this->app->environment('dev', 'local', 'testing')) {
            $this->app->register(DuskServiceProvider::class);
        }

        $this->app->register(TelescopeServiceProvider::class);
    }
}
