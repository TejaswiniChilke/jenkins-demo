<?php
namespace Uab\Providers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Uab\Http\Enums\BadgeConditionTypesEnum;
use Uab\Http\Enums\QueueTypesEnum;
use Uab\Http\Models\Articles;
use Uab\Http\Models\BadgeConditions;
use Uab\Http\Models\BadgeConditionsUsers;
use Uab\Http\Models\BadgeConditionTypes;
use Uab\Http\Models\Badges;
use Uab\Http\Models\BadgesUsers;
use Uab\Http\Models\Playlists;
use Uab\Http\Models\Users;
use Illuminate\Database\Eloquent\Collection;
use Uab\Http\Models\Videos;
use Uab\Jobs\Base\InsertObjectJob;

class BadgeServiceProvider {
    /**
     * BadgeServiceProvider constructor.
     */
    public function __construct() {

    }

    /**
     * @param Badges $badge
     * @param Users $user
     *
     * @return Collection<BadgesUsers>|BadgesUsers[]
     */
    public function getCompletedBadges(Badges $badge, Users $user) {
        return BadgesUsers::query()
            ->where(
                [
                    'badge_id' => $badge->id,
                    'user_id'  => $user->id
                ]
            )->get();
    }

    /**
     * @param string|int $badgeId
     *
     * @return Collection<BadgeConditions>|BadgeConditions[]
     */
    public function getConditionsByBadgeId($badgeId) {
        $query = BadgeConditions::query()
            ->where(
                [
                    'badge_id' => $badgeId
                ]
            );

        return $query->get();
    }

    /**
     * @param string $type
     *
     * @return BadgeConditionTypes|null|object
     */
    public function getConditionTypeByName(string $type):?BadgeConditionTypes {
        return BadgeConditionTypes::query()
            ->where(
                [
                    'name' => $type
                ]
            )->first();
    }

    /**
     * @param BadgeConditions $badgeCondition
     * @param Users|null   $user
     *
     * @return Collection<BadgeConditionsUsers>|BadgeConditionsUsers[]
     */
    public function getCompletedConditions(BadgeConditions $badgeCondition, ?Users $user = null) {
        $params = [
            'badge_condition_id' => $badgeCondition->id
        ];

        if (!is_null($user)) {
            $params['user_id'] = $user->id;
        }

        $query = BadgeConditionsUsers::query()->where($params);

        return $query->get();
    }

    /**
     * @param string|int $userTypeId
     * @param BadgeConditionTypes|null $badgeConditionType
     *
     * @return Collection<Badges>|Badges[]
     */
    public function getUserTypeBadges($userTypeId, ?BadgeConditionTypes $badgeConditionType = null) {
        $query = Badges::query()
            ->select(
                'badges.*'
            )->where(
                [
                    'badges_user_types.user_type_id' => $userTypeId
                ]
            )->rightJoin(
                'badges_user_types',
                'badges_user_types.badge_id',
                '=',
                'badges.id'
            );

        if (!is_null($badgeConditionType)) {
            $query->rightJoin(
                'badge_conditions',
                'badge_conditions.badge_id',
                '=',
                'badges.id'
            )->where('badge_conditions.badge_condition_type_id', '=', $badgeConditionType->id);
        }

        $badges = $query->get();

        return $badges;
    }

    /**
     * @param Badges $badge
     * @param Users $user
     *
     * @return bool
     */
    public function checkAndRewardBadge(Badges $badge, Users $user):bool {
        $earnedBadges = $this->getCompletedBadges($badge, $user);
        $earnedBadgeCount = $earnedBadges->count();

        if ($earnedBadgeCount >= $badge->max_completions) {
            return false;
        }

        $conditions = $this->getConditionsByBadgeId($badge->id);
        foreach ($conditions as $condition) {
            $this->checkAndRewardCondition($badge, $condition, $user);
        }

        return true;
    }

    /**
     * @param Badges $badge
     * @param BadgeConditions $condition
     * @param Users $user
     *
     * @return void
     */
    public function checkAndRewardCondition(Badges $badge, BadgeConditions $condition, Users $user) {
        $isEligibleForCondition = $this->isEligibleForCondition($badge, $condition, $user);
        if ($isEligibleForCondition === false) {
            return;
        }

        $conditionType = $condition->badgeConditionType();
        if (is_null($conditionType)) {
            return;
        }

        $shouldRewardCondition = false;

        switch ($conditionType->name) {
            case BadgeConditionTypesEnum::ARTICLE_ID:
                $shouldRewardCondition = $this->shouldRewardArticleIdCondition($badge, $condition, $user);
                break;
            case BadgeConditionTypesEnum::LOGIN:
                $shouldRewardCondition = $this->shouldRewardLoginCondition($badge, $condition, $user);
                break;
            case BadgeConditionTypesEnum::PLAYLIST_ID:
                $shouldRewardCondition = $this->shouldRewardPlaylistIdCondition($badge, $condition, $user);
                break;
            case BadgeConditionTypesEnum::VIDEO_ID:
                $shouldRewardCondition = $this->shouldRewardVideoIdCondition($badge, $condition, $user);
                break;
            case BadgeConditionTypesEnum::CALORIES:
            case BadgeConditionTypesEnum::FLOORS:
            case BadgeConditionTypesEnum::STEPS:
            case BadgeConditionTypesEnum::CREATE_COMMENT:
            case BadgeConditionTypesEnum::CREATE_EVENT:
            case BadgeConditionTypesEnum::CREATE_POST:
            case BadgeConditionTypesEnum::LIKE_VIDEO:
            case BadgeConditionTypesEnum::FAVORITE_VIDEO:
            default:
                Log::error('Unable to find badge condition of type ' . $conditionType->name . ' (' . $conditionType->id . ').');
        }

        if ($shouldRewardCondition) {
            $userCondition = new BadgeConditionsUsers();
            $userCondition->badge_condition_id = $condition->id;
            $userCondition->user_id = $user->id;
            $userCondition->earned_at = Carbon::now();
            InsertObjectJob::dispatch($userCondition)->onQueue(QueueTypesEnum::HIGH);
        }
    }

    /**
     * @param Badges $badge
     * @param BadgeConditions $condition
     * @param Users $user
     *
     * @return bool
     */
    private function shouldRewardArticleIdCondition(Badges $badge, BadgeConditions $condition, Users $user):bool {
        $requiredArticleId = $this->decodeConditionValue($condition);

        $start = null;

        /** @var BadgesUsers|null $lastRelatedEarnedBadge */
        $lastRelatedEarnedBadge = $this->getCompletedBadges($badge, $user)->first();
        if (!is_null($lastRelatedEarnedBadge)) {
            $start = $lastRelatedEarnedBadge->earned_at;
        } else {
            $start = $this->getBadgeStartDatetime($badge, $user);
        }

        $end = $this->getBadgeEndDatetime($badge, $user);

        $articleService = new ArticleServiceProvider();

        $hasRead = $articleService->hasReadArticle($user->id, $requiredArticleId, $start, $end);
        if (!$hasRead) {
            return false;
        }

        return true;
    }

    /**
     * @param Badges $badge
     * @param Users $user
     *
     * @return Carbon
     */
    private function getBadgeStartDatetime(Badges $badge, Users $user):?Carbon {
        if (is_null($badge->start_week)) {
            return null;
        }

        return Carbon::parse($user->start_time)->addWeeks($badge->start_week - 1);
    }

    /**
     * @param Badges $badge
     * @param Users $user
     *
     * @return Carbon
     */
    private function getBadgeEndDatetime(Badges $badge, Users $user):?Carbon {
        if (is_null($badge->end_week)) {
            return null;
        }

        return Carbon::parse($user->start_time)->addWeeks($badge->end_week);
    }

    /**
     * @param Badges $badge
     * @param BadgeConditions $condition
     * @param Users $user
     *
     * @return bool
     */
    private function shouldRewardLoginCondition(Badges $badge, BadgeConditions $condition, Users $user):bool {
        $start = null;

        /** @var BadgesUsers|null $lastRelatedEarnedBadge */
        $lastRelatedEarnedBadge = $this->getCompletedBadges($badge, $user)->first();
        if (!is_null($lastRelatedEarnedBadge)) {
            $start = $lastRelatedEarnedBadge->earned_at;
        } else {
            $start = $this->getBadgeStartDatetime($badge, $user);
        }

        $end = $this->getBadgeEndDatetime($badge, $user);

        $loginService = new LoginServiceProvider();

        $loginCount = $loginService->countLogins($user->id, $start, $end);
        if ($loginCount === 0 || $loginCount < $condition->value) {
            return false;
        }

        return true;
    }

    /**
     * @param Badges $badge
     * @param BadgeConditions $condition
     * @param Users $user
     *
     * @return bool
     */
    private function shouldRewardVideoIdCondition(Badges $badge, BadgeConditions $condition, Users $user):bool {
        $requiredVideoId = $this->decodeConditionValue($condition);

        $start = null;

        /** @var BadgesUsers|null $lastRelatedEarnedBadge */
        $lastRelatedEarnedBadge = $this->getCompletedBadges($badge, $user)->first();
        if (!is_null($lastRelatedEarnedBadge)) {
            $start = $lastRelatedEarnedBadge->earned_at;
        } else {
            $start = $this->getBadgeStartDatetime($badge, $user);
        }

        $end = $this->getBadgeEndDatetime($badge, $user);

        $videoService = new VideoServiceProvider();
        $hasWatchedVideo = $videoService->hasWatchedVideo($user->id, $requiredVideoId, $start, $end);
        if ($hasWatchedVideo) {
            return false;
        }

        return true;
    }

    /**
     * @param Badges $badge
     * @param BadgeConditions $condition
     * @param Users $user
     *
     * @return bool
     */
    private function shouldRewardPlaylistIdCondition(Badges $badge, BadgeConditions $condition, Users $user):bool {
        $start = null;

        /** @var BadgesUsers|null $lastRelatedEarnedBadge */
        $lastRelatedEarnedBadge = $this->getCompletedBadges($badge, $user)->first();
        if (!is_null($lastRelatedEarnedBadge)) {
            $start = $lastRelatedEarnedBadge->earned_at;
        } else {
            $start = $this->getBadgeStartDatetime($badge, $user);
        }

        $end = $this->getBadgeEndDatetime($badge, $user);

        $playlistService = new PlaylistServiceProvider();
        $hasCompletedPlaylist = $playlistService->hasCompletedPlaylist($user->id, $condition->value, $start, $end);
        if (!$hasCompletedPlaylist) {
            return false;
        }

        return true;
    }

    /**
     * @param BadgeConditions $condition
     *
     * @return int|null
     */
    private function decodeConditionValue(BadgeConditions $condition):?int {
        switch ($condition->badgeConditionType()->name) {
            case BadgeConditionTypesEnum::ARTICLE_ID:
                $model = new Articles();
                break;
            case BadgeConditionTypesEnum::PLAYLIST_ID:
                $model = new Playlists();
                break;
            case BadgeConditionTypesEnum::VIDEO_ID:
                $model = new Videos();
                break;
            default:
                $model = null;
        }

        if (is_null($model)) {
            return null;
        }

        $model = new $model();
        $modelId = $model->safeDecode($condition->value);
        if (is_null($modelId)) {
            Log::error('Model ID not found (' . $condition->value . ') for badge condition (' . $condition->id . '.');
            return null;
        }

        if (is_numeric($modelId) && is_string($modelId)) {
            $modelId = intval($modelId);
        }

        return $modelId;
    }

    /**
     * @param Badges $badge
     * @param BadgeConditions $condition
     * @param Users $user
     *
     * @return bool
     */
    private function isEligibleForCondition(Badges $badge, BadgeConditions $condition, Users $user):bool {
        $completedConditions = BadgeConditionsUsers::query()
            ->where(
                [
                    'user_id' => $user->id,
                    'badge_condition_id' => $condition->id
                ]
            )->get();

        $completedConditionCount = $completedConditions->count();

        if ($completedConditionCount !== 0) {
            if ($completedConditionCount >= $badge->max_completions) {
                return false;
            }

            $earnedBadges = $this->getCompletedBadges($badge, $user);
            $earnedBadgeCount = $earnedBadges->count();

            if ($completedConditionCount > $earnedBadgeCount) {
                return false;
            }

            if ($completedConditionCount >= $badge->max_completions) {
                return false;
            }
        }

        return true;
    }
}
