<?php
namespace Uab\Providers;

use Hashids\Hashids;
use Illuminate\Support\Arr;

class HashServiceProvider {
    /**
     * Cached hashslug
     *
     * @var null|string
     */
    private $slugs = [];

    /**
     * Cached HashIds instance
     * @var null|Hashids
     */
    private static $hashIds = null;

    /**
     * Returns a chached Hashids instanse
     * or initialises it with salt
     *
     * @return Hashids
     */
    private static function getHashIds() {
        if (is_null(static::$hashIds)){
            $minSlugLength = config('hashslug.minSlugLength', 5);

            $modelSalt = get_called_class();

            $alphabet = config('hashslug.alphabet', 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890');

            $salt = config('hashslug.appsalt', config('app.key')) . $modelSalt;
            $salt = hash('sha256', $salt);

            static::$hashIds = new Hashids($salt, $minSlugLength, $alphabet);
        }

        return static::$hashIds;
    }

    /**
     * Hash calculated from key
     *
     * @param string $key
     *
     * @return string|null
     */
    public function slug($key) {
        if (!Arr::has($this->slugs, $key)) {
            $hashIds = $this->getHashids();

            $encoded = $hashIds->encode($key);

            Arr::set($this->slugs, $key, $encoded);
        }

        return Arr::get($this->slugs, $key, null);
    }

    /**
     * Decodes slug to key
     *
     * @param string|int $slug
     *
     * @return mixed
     */
    public function decodeSlug($slug) {
        $hashIds = static::getHashids();

        $decoded = $hashIds->decode($slug);
        $decoded = Arr::first($decoded, null, null);

        return $decoded;
    }
}
