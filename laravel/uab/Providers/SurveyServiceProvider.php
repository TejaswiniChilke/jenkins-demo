<?php
namespace Uab\Providers;

use stdClass;
use Cache;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;
use Log;
use Uab\Http\Models\AnswerInputTypes;
use Uab\Http\Models\AnswerOptions;
use Uab\Http\Models\Answers;
use Uab\Http\Models\Base\BaseModel;
use Uab\Http\Models\Questions;
use Uab\Http\Models\SurveyPages;
use Uab\Http\Models\Surveys;
use Uab\Http\Models\SurveysUsers;

class SurveyServiceProvider {
    public function getSurveyUser($surveyId, $user = null) {
        if (is_null($user)) {
            $user = Auth::user();
        }

        if (!is_null($user)) {
            $surveyUser = SurveysUsers::query()
                ->where(
                    [
                        'survey_id' => $surveyId,
                        'user_id'   => $user->id,
                        'is_locked' => 0
                    ]
                )->first();

            if (is_null($surveyUser)) {
                $surveyUser = new SurveysUsers();
                $surveyUser->is_locked = 0;
                $surveyUser->survey_id = $surveyId;
                $surveyUser->user_id = $user->id;

                $success = $surveyUser->save();
                if (!$success) {
                    $surveyUser = null;
                }
            }

            return $surveyUser;
        }

        return null;
    }

    public function assignUserSurveyTagLevel() {
        $surveyNamesArray = [
            'Self-Efficacy',
            'Outcome Expectation',
            'Exercise Goals',
            'Exercise Plans'
        ];

        foreach ($surveyNamesArray as $surveyName) {
            $survey = Surveys::query()->where('name', $surveyName)->first();

            if ($survey) {
                $surveyUser = $this->getSurveyUser($survey->id);

                $value = 0;

                if ($surveyUser) {

                    $query = Answers::query()
                        ->join('answer_options', 'answers.answer_option_id', '=', 'answer_options.id')
                        ->where('surveys_users_id', $surveyUser->id);

                    $answers = $query->get();

                    foreach ($answers as $answer) {
                        $value += $answer->value;
                    }
                }
            }
        }
    }

    public function getNextSurvey($number) {
//        $id = Auth::id(); TODO

        $service = new PhoneServiceProvider();
        $number = $service->formatNumber($number);

        if (is_null($number)) {
            return null;
        } else {
            $surveyId = null;
            if ($number === config('ivr.phone.goal')) {
                $surveyId = config('ivr.survey.goal');
            } else if ($number === config('ivr.phone.sct')) {
                $surveyId = config('ivr.survey.sct');
            } else if ($number === config('ivr.phone.tracking')) {
                $surveyId = config('ivr.survey.tracking');
            }

            return Surveys::query()->find($surveyId);
        }
    }

    /**
     * @param string $slug
     *
     * @return array|null
     */
    public function getSurveyPage(string $slug):?array {
        $model = new SurveyPages();
        $id = $model->safeDecode($slug);

        /** @var SurveyPages|null $page */
        $page = SurveyPages::query()->find($id);

        if (is_null($page)) {
            return null;
        }

        $page['questions'] = $this->getQuestions($page);

        return $page;
    }

    /**
     * @param string $slug
     *
     * @return array|null
     */
    public function getSurvey(string $slug):?array {
        $survey = null;

        /** @var Surveys $survey */
        $mm = new Surveys();
        $id = $mm->safeDecode($slug);

        $survey = Surveys::query()->find($id);

        if (is_null($survey)) {
            return null;
        }

        $survey = $survey->getAttributes();

        $survey['survey_pages'] = $this->getSurveyPages($survey);

        $queryService = new QueryServiceProvider();
        $survey = $queryService->slugIds('surveys', $survey);

        return $survey;
    }

    /**
     * @param stdClass $survey
     *
     * @return bool
     */
    public function saveSurvey(stdClass $survey):bool {
        $mm = new Surveys();
        $survey->id = $mm->safeDecode($survey->id);

        $success = true;

        foreach ($survey->survey_pages as $page) {
            $success &= $this->saveSurveyPage($page);
        }

        if (!$success) {
            Log::warning('Unable to save all of the survey pages.');

            return false;
        }

        Cache::flush();

        return true;
    }

    /**
     * @param stdClass $page
     *
     * @return bool
     */
    public function saveSurveyPage(stdClass $page):bool {
        $surveyUser = $this->getSurveyUser($page->survey_id);

        $answerIds = [];

        $mm = new SurveyPages();
        $page->id = $mm->safeDecode($page->id);

        foreach ($page->questions as $question) {
            $mm = new Questions();
            $question->id = $mm->safeDecode($question->id);

            $answers = $question->answer;
            if (!is_array($answers)) {
                $answers = [
                    $answers
                ];
            }

            $newAnswerIds = $this->saveAnswers($surveyUser, $question, $answers);

            $answerIds = array_merge($answerIds, $newAnswerIds);
        }

        $query = Answers::query()
            ->where(
                [
                    'surveys_users_id' => $surveyUser->id
                ]
            )
            ->join('questions', 'questions.id', '=', 'answers.question_id')
            ->join('survey_pages','survey_pages.id', '=', 'questions.survey_page_id')
            ->whereNotIn(
                'answers.id',
                $answerIds
            )->where(
                'survey_pages.id', '=', $page->id
            );

        $query->delete();

        return true;
    }

    /**
     * @param mixed[] $survey
     *
     * @return mixed[]
     */
    private function getSurveyPages(array $survey):array {
        $pages = SurveyPages::query()
            ->where(
                [
                    'survey_id' => $survey['id']
                ]
            )->orderBy('position', 'asc')
            ->get();

        $returnPages = [];

        foreach ($pages as $i => $page) {
            $returnPage = $page->toArray();

            if (!Arr::has($returnPage, 'questions')) {
                $returnPage['questions'] = [];
            }

            $returnPage['questions'] = $this->getQuestions($pages[$i]);

            $returnPages[] = $returnPage;
        }

        return $returnPages;
    }

    /**
     * @param stdClass  $question
     * @param number    $surveyId
     *
     * @return AnswerOptions[]|null
     */
    private function getAnswers(stdClass $question, $surveyId):?array {
        $answers = null;

        $surveyUser = $this->getSurveyUser($surveyId);

        if (!is_null($surveyUser)) {
            $answers = Answers::query()
                ->where(
                    [
                        'question_id'      => $question->id,
                        'surveys_users_id' => $surveyUser->id
                    ]
                )->get();
        }

        if ($question->answer_type !== 'CHECK') {
            if (count($answers) !== 0) {
                $answers = $answers[0];
            } else {
                $answers = null;
            }
        }

        return $answers;
    }

    /**
     * @param Questions $question
     *
     * @return string|null
     */
    private function getAnswerType(Questions $question):?string {
        $model = AnswerInputTypes::query()
            ->where(
                [
                    'id' => $question->answer_input_type_id
                ]
            )->first();

        if (is_null($model)) {
            return null;
        }

        return $model->type;
    }

    /**
     * @param Questions $question
     *
     * @return mixed[]
     */
    private function getAnswerOptions(Questions $question):array {
        /** @var AnswerOptions[] $options */
        $options = AnswerOptions::query()
            ->where(
                [
                    'parent_question_id' => $question->id
                ]
            )->orderBy('position', 'asc')
            ->get();

        $returnOptions = [];

        foreach ($options as $i => $option) {
            $returnOption = $option->toArray();

            $returnOption['next_question'] = $this->getNextQuestion($option);
            $returnOption['next_page'] = $this->getNextPage($option);

            $returnOptions[] = $returnOption;
        }

        return $returnOptions;
    }

    /**
     * @param AnswerOptions $option
     *
     * @return Builder|SurveyPages|null
     */
    public function getNextPage(AnswerOptions $option) {
        if (isset($option->next_survey_page_id)) {
            return SurveyPages::query()
                ->where(
                    [
                        'id' => $option->next_survey_page_id
                    ]
                )->first();
        }

        return null;
    }

    /**
     * @param Questions $question
     *
     * @return BaseModel|SurveyPages|null|Builder
     */
    public function getPreviousPage(Questions $question) {
        return SurveyPages::query()
            ->select(
                'survey_pages.*'
            )->leftJoin(
                'questions',
                'questions.survey_page_id',
                '=',
                'survey_pages.id'
            )->leftJoin(
                'answer_options',
                'answer_options.parent_question_id',
                '=',
                'questions.id'
            )->where(
                [
                    'answer_options.next_question_id' => $question->id
                ]
            )->first();
    }

    /**
     * @param AnswerOptions $option
     *
     * @return Builder|BaseModel|null
     */
    private function getNextQuestion(AnswerOptions $option) {
        if (isset($option->next_question_id) && is_numeric($option->next_question_id)) {
            return Questions::query()
                ->where(
                    [
                        'id' => $option->next_question_id
                    ]
                )->first();
        }

        return null;
    }

    /**
     * @param SurveyPages $page
     *
     * @return mixed[]
     */
    private function getQuestions(SurveyPages $page) {
        /** @var Questions[] $questions */
        $questions = Questions::query()
            ->where(
                [
                    'survey_page_id' => $page->id
                ]
            )->orderBy('questions.position', 'asc')
            ->get();

        $returnQuestions = [];

        foreach ($questions as $i => $question) {
            $returnQuestion = $question->toArray();

            $returnQuestion['answer_type'] = $this->getAnswerType($question);
            $returnQuestion['answer_options'] = $this->getAnswerOptions($question);
            $returnQuestion['answer'] = $this->getAnswers($question, $page->survey_id);
            $returnQuestion['previous_page'] = $this->getPreviousPage($question);

            $returnQuestions[] = $returnQuestion;
        }

        return $returnQuestions;
    }

    /**
     * @param SurveysUsers $surveyUser
     * @param Questions    $question
     * @param stdClass[]   $answers
     *
     * @return string[]|null
     */
    private function saveAnswers(SurveysUsers $surveyUser, Questions $question, array $answers):array {
        $answerIds = [];

        $previousAnswers = Answers::query()
            ->where(
                [
                    'surveys_users_id' => SurveysUsers::safeDecodeStatic($surveyUser->id),
                    'question_id' => Questions::safeDecodeStatic($question->id)
                ]
            )->get();

        foreach ($answers as $answer) {
            $mm = new AnswerOptions();
            if (isset($answer->answer_option_id)) {
                $answer->answer_option_id = $mm->safeDecode($answer->answer_option_id);
            } else {
                $answer->answer_option_id = null;
            }

            if (!is_null($answer->response) || !is_null($answer->answer_option_id)) {
                $answerChanged = false;
                $previouslyAnswered = false;

                foreach ($previousAnswers as $previousAnswer) {
                    if ($previousAnswer->question_id === $answer->question_id) {
                        $previouslyAnswered = true;

                        if ($previousAnswer->response === $answer->response && $previousAnswer->answer_option_id === $answer->answer_option_id) {
                            $answerIds[] = $answer->id;
                        } else {
                            $answerChanged = true;
                        }

                        break;
                    }
                }

                if (!$previouslyAnswered || $answerChanged) {
                    $newAnswer = new Answers();
                    $newAnswer->response = $answer->response;
                    $newAnswer->answer_option_id = AnswerOptions::safeDecodeStatic($answer->answer_option_id);
                    $newAnswer->surveys_users_id = SurveysUsers::safeDecodeStatic($surveyUser->id);
                    $newAnswer->question_id = Questions::safeDecodeStatic($question->id);

                    $success = $newAnswer->save();

                    if ($success) {
                        $answerIds[] = $newAnswer->id;
                    } else {
                        Log::error('Unable to save survey answer.');

                        return $answerIds;
                    }
                } else {
                    $answerIds[] = $answer->id;
                }
            }
        }

        return $answerIds;
    }
}
