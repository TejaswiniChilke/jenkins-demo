<?php
namespace Uab\Providers;

use Twilio\TwiML\Voice\Gather;

class UabGather extends Gather {
    public function __construct($attributes = array()) {
        parent::__construct($attributes = array());

        if (!array_key_exists('method', $attributes)) {
            $this->setMethod('get'); // TODO: Get from env
        }

        if (!array_key_exists('timeout', $attributes)) {
            $this->setTimeout(2); // TODO: Get from env
        }

        if (!array_key_exists('voice', $attributes)) {
            $user = Auth::user(); // TODO: Add user to Auth (from pincode)

            if (!is_null($user)) {
                $userService = new UsersServiceProvider();

                $attributes['voice']  = $userService->getVoiceGender($user);
            }
        }
    }

    public function say($message, $attributes = []) {
        return parent::say($message, $attributes);
    }
}
