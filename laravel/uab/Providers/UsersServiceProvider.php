<?php
namespace Uab\Providers;

use Illuminate\Support\Collection;
use Uab\Http\Enums\UserTypesEnum;
use Uab\Http\Enums\VoicesEnum;
use Uab\Http\Models\Users;
use Uab\Http\Models\UserTypes;

class UsersServiceProvider {
    /**
     * @param string|string[] $userTypes
     *
     * @return Users[]|Collection<Users>
     */
    public function getUserTypesByName($userTypes) {
        if (!is_array($userTypes)) {
            $userTypes = [
                $userTypes
            ];
        }

        /** @var UserTypes $userTypeIds */
        $userTypes = UserTypes::query()
            ->orWhereIn('name', $userTypes)
            ->get();

        return $userTypes;
    }

    /**
     * @param Collection<UserTypes> $userTypes
     *
     * @return Collection<Users>|Users[]
     */
    public function getUsersByUserTypes(Collection $userTypes):Collection {
        $users = new Collection();

        $userTypeIds = $userTypes->pluck('id')
            ->toArray();

        if (!is_null($userTypeIds)) {
            $users = Users::query()
                ->whereIn('user_type_id', $userTypeIds)
                ->get();
        }

        return $users;
    }
    /**
     * @param string|string[] $userTypeNames
     *
     * @return Users[]|Collection<Users>
     */
    public function getUsersByUserTypeNames($userTypeNames) {
        $userTypes = $this->getUserTypesByName($userTypeNames);

        return $this->getUsersByUserTypes($userTypes);
    }

    /**
     * @param string|string[] $userTypes
     *
     * @return Collection<Users>|Users[]
     */
    public function getUsersByNotUserTypeNames($userTypes):Collection {
        if (!is_array($userTypes)) {
            $userTypes = [
                $userTypes
            ];
        }

        $users = collect();

        /** @var UserTypes $userTypeIds */
        $userTypeIds = UserTypes::query()
            ->orWhereNotIn('name', $userTypes)
            ->get()
            ->pluck('id')
            ->toArray();

        if (!is_null($userTypeIds)) {
            $users = Users::query()
                ->whereIn('user_type_id', $userTypeIds)
                ->get();
        }

        return $users;
    }

    /**
     * @param Users $user
     *
     * @return string
     */
    public function getVoiceGender($user) {
        $voiceGender = VoicesEnum::MAN;
        if ($user->userType()->name === UserTypesEnum::SAME_GENDER) {
            $voiceGender = $user->gender ? VoicesEnum::WOMAN : VoicesEnum::MAN;
        } else if ($user->userType()->name === UserTypesEnum::OPPOSITE_GENDER) {
            $voiceGender = $user->gender ? VoicesEnum::MAN : VoicesEnum::WOMAN;
        } else if ($user->userType()->name === UserTypesEnum::RANDOM_GENDER) {
            if (rand(0, 100) > 50) {
                $voiceGender = VoicesEnum::WOMAN;
            } else {
                $voiceGender = VoicesEnum::MAN;
            }
        }

        return $voiceGender;
    }

    /**
     * @param Users $user
     *
     * @return bool
     */
    public function isAdmin($user = null):bool {
        if (is_null($user)) {
            $user = Auth::user();
        }

        if (is_null($user)) {
            return false;
        }

        return in_array(
            $user->getUserTypeNameAttribute(),
            [
                UserTypesEnum::ADMIN,
                UserTypesEnum::SUPER_ADMIN
            ]
        );
    }
}
