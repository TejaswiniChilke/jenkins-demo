<?php
namespace Uab\Providers;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use stdClass;
use Uab\Http\Models\Base\BaseModel;
use Uab\Providers\Enums\JoinTypes;

class QueryServiceProvider {
    private $OPERATORS = [
        'LE' => '<=', // LESS THAN OR EQUAL TO
        'GE' => '>=', // GREATER THAN EQUAL TO
        'IN' => '[]', // IN ARRAY
        'NE' => '!=', // NOT EQUAL TO
        'EQ' => '=',  // EQUAL TO
        'LT' => '<',  // LESS THAN
        'GT' => '>',  // GREAT THAN
        'LK' => '%',  // LIKE
        'NL' => '%',  // NOT LIKE
    ];

    /** @var ModelServiceProvider $modelService */
    public $modelService;

    /** @var RequestServiceProvider $requestService */
    public $requestService;

    public function __construct() {
        $this->modelService = new ModelServiceProvider();
        $this->requestService = new RequestServiceProvider();
    }

    /**
     * @param mixed[]       $options
     * @param Builder       $query
     * @param BaseModel     $parentModel
     *
     * @return Builder
     */
    public function addSelect($options, $query, $parentModel) {
        $joins = Arr::get($options, 'joins', []);

        $options = Arr::get($options, 'fields', []);
        $options = $this->modelService->getFields($parentModel, $options, $joins);

        return $query->select($options)->distinct();
    }

    /**
     * @param mixed[]       $fields
     * @param Builder       $query
     * @param BaseModel     $parentModel
     * @param string[]|null $joins
     *
     * @return Builder
     */
    private function _addJoins($fields, $query, $parentModel, $joins = null) {
        if (is_null($joins)) {
            $joins = $this->modelService->getJoins(
                Arr::get($fields, 'joins', [])
            );
        }

        if (is_array($joins)) {
            foreach ($joins as $joinType => $joinArray) {
                foreach ($joinArray as $join) {
                    $nestedJoins = explode('.', $join);

                    foreach ($nestedJoins as $i => $nestedJoin) {
                        $query = $this->_addJoin(
                            $query,
                            ($i === 0) ? $parentModel : $nestedJoins[$i - 1],
                            $nestedJoin,
                            $joinType
                        );
                    }
                }
            }
        }

        return $query;
    }

    /**
     * @param Builder      $query
     * @param BaseModel    $parentModel
     * @param string       $join
     * @param bool|string  $joinType
     *
     * @return mixed
     */
    private function _addJoin($query, $parentModel, $join, $joinType = 'join') {
        if ($parentModel instanceof BaseModel === false) {
            $parentModel = $this->modelService->getModel($parentModel);
        }

        $joinRelationshipType = $parentModel->getRelationshipType($join);
        $joinModel = $this->modelService->getModel($joinRelationshipType);

        if ($joinModel) {
            $parentTable = $parentModel->getTable();
            $joinTable = $joinModel->getTable();

            $parentKey = null;
            $joinKey = null;

            $parentRelationships = $parentModel->getRelationships();
            $joinRelationships = $joinModel->getRelationships();

            if (array_key_exists($join, $parentRelationships) && !is_null($parentRelationships[$join])) {
                $parentKey = $parentRelationships[$join];
                $joinKey = $joinModel->getKeyName();
            } else if (array_key_exists($parentTable, $joinRelationships)) {
                $parentKey = $parentModel->getKeyName();
                $joinKey = $joinRelationships[$parentTable];
            } else {
                $parentKey = $parentModel->getKeyName();
                $joinKey = $parentModel->getChildKey($joinTable);
            }

            $parentAlias = $this->modelService->getAlias($parentModel);
            $joinAlias = $this->modelService->getAlias($joinModel, $join);

            switch($joinType) {
                case JoinTypes::JOIN_TYPE_LEFT:
                    $joinType = 'leftJoin';
                    break;
                case JoinTypes::JOIN_TYPE_RIGHT:
                    $joinType = 'rightJoin';
                    break;
                case JoinTypes::JOIN_TYPE_OUTER:
                case JoinTypes::JOIN_TYPE_INNER:
                default:
                    $joinType = 'join';
                    break;
            }

            if (!is_null($parentKey) && !is_null($joinKey)) {
                $query->$joinType(
                    $joinRelationshipType . ' AS ' . $joinAlias,
                    /**
                     * @param Builder $join
                     */
                    function ($join) use ($joinModel, $joinAlias, $joinKey, $parentAlias, $parentKey) {
                        $join->on(
                            $parentAlias . '.' . $parentKey,
                            '=',
                            $joinAlias . '.' . $joinKey
                        );

                        if ($joinModel->hasColumn('deleted_at')) {
                            $join->whereNull($joinAlias.'.deleted_at');
                        }
                    }
                );
            } else {
                $parentKey = $parentModel->getKeyName();
                $joinKey = $joinModel->getKeyName();

                $manyToManyModel = $this->modelService->getManyToManyModel($parentModel, $joinModel);

                if ($manyToManyModel) {
                    $manyToManyAlias = $this->modelService->getAlias($manyToManyModel);

                    $manyToManyTable = $manyToManyModel->getTable();
                    $query = $this->_includeJoin($query, $parentModel, $joinType, $parentTable, $manyToManyTable, $manyToManyAlias, $manyToManyModel, $parentAlias, $parentKey);

                    $manyToManyJoinKey = $this->getForeignKey($joinTable);
                    $query = $this->_includeJoin($query, $joinModel, $joinType, $joinTable, $manyToManyJoinKey, $joinAlias, $joinModel, $joinTable, $joinKey);
                }
            }
        }

        return $query;
    }

    public function getForeignKey($table) {
        return Str::singular($table).'_id';
    }

    /**
     * @param mixed[] $fields
     *
     * @return float|int
     */
    private function getLimit($fields) {
        $limit = Arr::get($fields, 'limit', 20);

        if (!is_null($limit)) {
            $limit = intval($limit);
        }

        return $limit;
    }

    /**
     * @param mixed[] $fields
     *
     * @return float|int
     */
    private function getOffset($fields) {
        $offset = intval(Arr::get($fields, 'offset', 0));
        if ($offset === 0) {
            $offset = intval(Arr::get($fields, 'page', 1));

            $offset = $this->getLimit($fields) * ($offset - 1);
        }

        return $offset;
    }

    /**
     * @param mixed[] $fields
     * @param Builder $query
     *
     * @return Builder
     */
    private function _addLimit($fields, $query) {
        $limit = $this->getLimit($fields);

        $query->limit($limit);

        return $query;
    }

    /**
     * @param mixed[] $fields
     * @param Builder $query
     *
     * @return Builder
     */
    private function _addOffset($fields, $query) {
        $offset = $this->getOffset($fields);

        $query->offset($offset);

        return $query;
    }

    /**
     * @param mixed[]   $fields
     * @param Builder   $query
     * @param BaseModel $model
     *
     * @return Builder
     */
    private function _addOrder($fields, $query, $model) {
        $directions = [
            '+', // ASC
            '-'  // DESC
        ];

        $order = Arr::get($fields, 'order', false);

        if ($order) {
            $multipleOrderArray =  explode(',', $order);
            foreach ($multipleOrderArray as $key => $value) {
                $direction = substr($value, 0, 1);

                if ($direction === '-') {
                    $direction = 'DESC';

                    $value = substr($value, 1);
                } else {
                    $direction = 'ASC';

                    if ($direction === '+') {
                        $value = substr($value, 1);
                    }
                }

                $parts = explode('.', $value);


                if (count($parts) === 1) {
                    $column = $parts[0];

                    if ($model->hasColumn($column)) {
                        $column = $model->getTable() . '.' . $column;

                        $query->orderBy($column, $direction);
                    }
                } else if (count($parts) === 2) {
                    $orderTable = $parts[0];
                    if (in_array($direction, $directions)) {
                        $orderTable = substr($orderTable, 1);
                    }

                    $orderModel = $this->modelService->getModel($orderTable);

                    $column = $orderTable . '.' . $parts[1];

                    if (!is_null($orderModel) && $orderModel->hasColumn($column)) {
                        if (!strstr($order, '.')) {
                            $column = $model->getTable() . '.' . $column;
                        }

                        $query->orderBy($column, $direction);
                    }
                }
            }
        }

        return $query;
    }

    /**
     * @param mixed[]      $fields
     * @param BaseModel    $parentModel
     * @param Builder      $query
     * @param mixed[]|null $wheres
     *
     * @return Builder
     */
    public function addWheres($fields, $parentModel, $query, $wheres = null) {
        $wheres = $this->getWheres($fields, $wheres);

        foreach ($wheres as $i => $where) {
            $query = $this->_addWhere($query, $parentModel, $where);
        }

        return $query;
    }

    /**
     * @param mixed[]      $fields
     * @param mixed[]|null $wheres
     *
     * @return string[][]
     */
    public function getWheres($fields, $wheres = null) {
        $returnVar = [];

        if (is_null($wheres)) {
            $wheres = explode(',', Arr::get($fields, 'where', ''));
        }

        foreach ($wheres as $i => $where) {
            if ((is_array($where) && count($where) !== 0) || (is_string($where) && strlen($where) !== 0)) {
                $foundOperator = false;

                foreach ($this->OPERATORS as $operatorKey => $operator) {
                    if (strstr($where, $operator)) {
                        $foundOperator = true;

                        $where = explode($operator, $where);

                        $where[0] = $where[0] . $operatorKey;

                        $returnVar[] = $where;

                        break;
                    }
                }

                if ($foundOperator === false && is_string($i)) {
                    $returnVar[] = [$i . 'EQ', $where];
                }
            }
        }

        return $returnVar;
    }

    private function _getWhereOperatorKey($where) {
        return substr($where[0], -2);
    }

    /**
     * @param Builder   $query
     * @param BaseModel $parentModel
     * @param $where
     *
     * @return Builder
     */
    private function _addWhere($query, $parentModel, $where) {
        $operatorKey = $this->_getWhereOperatorKey($where);

        $columnName = $this->_getWhereColumnName($where);
        $columnTable = $this->_getWhereColumnTable($where, $parentModel);

        if (strtolower($columnName) === 'id') {
            $where[1] = $this->modelService->getIdFromSlug($where[1], $columnTable);
        } else if (strstr($columnName, '_id') !== false) {
            $childId = $where[1];

            $fkModel = $this->modelService->getTableByForeignKey($columnTable, $columnName);
            if ($fkModel) {
                $childId = $this->modelService->getIdFromSlug($childId, $fkModel);
            }

            $where[1] = $childId;
        }

        $or = strstr($columnName, '|') !== false;
        $isNot = $operatorKey === 'NE' || $operatorKey === 'NL';

        $columnExists = false;
        $columnModel = $this->modelService->getModel($columnTable);
        if ($columnModel) {
            if ($columnModel->isVirtualField($columnName)) {
                $columnExists = false;
            } else {
                $columnExists = $columnModel->hasColumn($columnName);
            }
        }

        if ($columnExists) {
            $isLike = $operatorKey === 'LK' || $operatorKey === 'NL';
            if ($isLike) {
                $where[1] = '%' . $where[1] . '%';
            }

            if ($operatorKey === 'IN') {
                if ($or && $isNot) {
                    $query->orWhereNotIn($columnTable.'.'.$columnName, explode(':', $where[1]));
                } else if ($or && !$isNot) {
                    $query->orWhereIn($columnTable.'.'.$columnName, explode(':', $where[1]));
                } else if (!$or && $isNot) {
                    $query->whereNotIn($columnTable.'.'.$columnName, explode(':', $where[1]));
                } else {
                    $query->whereIn($columnTable.'.'.$columnName, explode(':', $where[1]));
                }
            } else if (strtolower($where[1]) === 'null') {
                if ($isNot) {
                    $query->whereNotNull($columnTable.'.'.$columnName);
                } else {
                    $query->whereNull($columnTable.'.'.$columnName);
                }
            } else if ($isLike) {
                if ($isNot) {
                    $query->where($columnTable.'.'.$columnName, 'NOT LIKE', $where[1], $or ? 'or' : 'and');
                } else {
                    $query->where($columnTable.'.'.$columnName, 'LIKE', $where[1], $or ? 'or' : 'and');
                }
            } else {
                $query->where($columnTable.'.'.$columnName, $this->OPERATORS[$operatorKey], $where[1], $or ? 'or' : 'and');
            }
        }

        return $query;
    }

    private function _getWhereColumnName($where) {
        $columnName = substr(Arr::last(explode('.', $where[0])), 0, -2);
        $columnName = preg_replace("/[^._a-zA-Z]+/", "", $columnName);

        return $columnName;
    }

    private function _getWhereColumnTable($where, $parentModel) {
        $columnTable = explode('.', $where[0]);
        $chains = count($columnTable);

        if ($chains !== 0) {
            if ($chains === 1) {
                $columnTable = $parentModel->getTable();
            } else {
                $columnTable = $columnTable[$chains - 2];
            }
        }

        return $columnTable;
    }

    /**
     * @param string|BaseModel $parentTable
     * @param mixed[]          $fields
     * @param string|null      $id
     *
     * @return \Illuminate\Database\Eloquent\Builder|Builder
     */
    public function buildRequestQuery($parentTable, $fields, $id = null) {
        $id = $this->requestService->getId($fields, $id);

        $parentModel = $this->modelService->getModel($parentTable);

        $query = $parentModel::query()->select();

        if (!is_null($id)) {
            $id = $this->modelService->getIdFromSlug($id, $parentModel);

            $alias = $this->modelService->getAlias($parentModel);

            $query->where($alias.'.id', '=', $id);
        }

        $query = $this->addSelect($fields, $query, $parentModel);
        $query = $this->_addJoins($fields, $query, $parentModel);
        $query = $this->addWheres($fields, $parentModel, $query);
        $query = $this->_addOffset($fields, $query);
        $query = $this->_addLimit($fields, $query);
        $query = $this->_addOrder($fields, $query, $parentModel);

        return $query;
    }

    /**
     * @param Builder   $query
     * @param BaseModel $baseModel
     * @param string[]  $joins
     *
     * @return mixed[]
     */
    public function collectionToModel($query, $baseModel, $joins = []) {
        $collections = $query->get();

        $allModels = [];

        while ($collections->count() > 0) {
            $collectionModel = $collections->shift();

            if ($collectionModel instanceof stdClass) {
                $collectionModel = $this->modelService->stdToArray($collectionModel);
            } else {
                $collectionModel = $collectionModel->toArray();
            }

            $models = [];

            foreach ($collectionModel as $key => $value) {
                $key = explode('__', $key);

                if (count($key) === 2) {
                    $model = $key[0];
                    $field = $key[1];

                    if (!array_key_exists($model, $models)) {
                        $models[$model] = [];
                    }

                    $models[$model][$field] = $value;
                }
            }

            $object = $this->modelService->breakDown(collect($models), $joins, $baseModel);

            if ($object) {
                $added = false;
                foreach ($allModels as $i => $model) {
                    if (Arr::get($model, 'id') === Arr::get($object, 'id')) {
                        $allModels[$i] = $this->modelService->mergeObjects($object, $model);

                        $added = true;
                    }
                }

                if ($added === false) {
                    $allModels[] = $object;
                }
            }
        }

        return $allModels;
    }

    /**
     * @param string  $parentTable
     * @param mixed[] $fields
     *
     * @return int
     */
    public function count($parentTable, $fields) {
        $query = $this->buildRequestQuery($parentTable, $fields);

        return $query->count();
    }

    /**
     * @param string  $parentTable
     * @param mixed[] $fields
     *
     * @return boolean
     */
    public function exists($parentTable, $fields) {
        $query = $this->buildRequestQuery($parentTable, $fields);

        return $query->exists();
    }

    /**
     * @param mixed[]  $fields
     * @param string   $parentTable
     * @param int|null $id
     *
     * @return mixed[]
     */
    public function get($parentTable, $fields, $id = null) {
        $id = $this->requestService->getId($fields, $id);

        $query = $this->buildRequestQuery($parentTable, $fields, $id);

        $results = $this->parseGet($parentTable, $fields, $query);

        if (is_array($results) && array_key_exists($parentTable, $results)) {
            $results = $this->filterVirtualFields($parentTable, $fields, $results[$parentTable]);
        }

        return $results;
    }

    /**
     * @param string  $parentTable
     * @param mixed[] $fields
     * @param mixed[] $models
     *
     * @return mixed[]
     */
    public function filterVirtualFields($parentTable, $fields, $models) {
        $parentModel = $this->modelService->getModel($parentTable);

        $wheres = $this->getWheres($fields);

        foreach ($wheres as $where) {
            $columnName = $this->_getWhereColumnName($where);

            if ($parentModel->isVirtualField($columnName)) {
                foreach ($models as $i => $object) {
                    $matches = false;

                    $model = $parentModel->fill($object);

                    $value = $model->getAttribute($columnName);

                    $operatorKey = $this->_getWhereOperatorKey($where);
                    switch ($operatorKey) {
                        case 'LE': // LESS THAN OR EQUAL TO
                            $matches = $value <= $where[1];
                            break;
                        case 'GE': // GREATER THAN EQUAL TO
                            $matches = $value >= $where[1];
                            break;
                        case 'IN': // IN ARRAY
                            if (is_array($where[1])) {
                                $matches = Arr::has($where[1], $value);
                            }

                            break;
                        case 'NE': // NOT EQUAL TO
                            $matches = $value != $where[1];
                            break;
                        case 'LT': // LESS THAN
                            $matches = $value < $where[1];
                            break;
                        case 'GT': // GREAT THAN
                            $matches = $value > $where[1];
                            break;
                        case 'LK': // LIKE
                            $matches = strstr($value, $where[1]) !== false;
                            break;
                        case 'NL': // NOT LIKE
                            $matches = strstr($value, $where[1]) === false;
                            break;
                        case 'EQ': // EQUAL TO
                        default:
                            $matches = $value == $where[1];
                            break;
                    }

                    if (!$matches) {
                        unset($models[$i]);
                    }
                }
            }
        }

        return $models;
    }

    /**
     * @param integer        $id
     * @param boolean|string $model
     *
     * @return BaseModel|boolean
     */
    public function getById($id, $model = null) {
        $model = $this->modelService->getModel($model);

        $collection = $model->newQuery()->where(
            [
                $model->getKeyName() => $id
            ]
        )->get();

        return $collection->first();
    }

    /**
     * @param string  $parentTable
     * @param mixed[] $fields
     * @param Builder $query
     *
     * @return mixed[]
     */
    public function parseGet($parentTable, $fields, $query) {
        $joins = Arr::get($fields, 'joins', []);

        $results = $this->collectionToModel($query, $this->modelService->getModel($parentTable), $joins);

        return $this->filterVirtualFields($parentTable, $fields, $results);
    }

    /**
     * @param mixed[]          $object
     * @param string|BaseModel $parentModel
     *
     * @return mixed[]
     */
    public function parseModelProperties($object, $parentModel) {
        $model = $this->modelService->getModel($parentModel);

        /** @var BaseModel $model */
        if ($this->modelService->isAssociativeArray($object)) {
            $parentModel = $this->modelService->arrayToModel(
                $object,
                $parentModel
            );

            $object = $parentModel->toArray();

            if (!is_null($model)) {
                foreach ($object as $column => $value) {
                    if (is_string($column)) {
                        $childModel = $this->modelService->getTableByForeignKey($model, $column);
                    } else {
                        $childModel = $this->modelService->getTableByForeignKey($model, $model->getTable());
                    }

                    if ($childModel) {
                        if (is_array($value) && count($value) !== 0) {
                            if (!$this->modelService->isAssociativeArray($value)) {
                                $newValue = [];

                                foreach ($value as $child) {
                                    $newValue[] = $this->parseModelProperties($child, $childModel);
                                }
                            } else {
                                $childModel = $this->modelService->arrayToModel($value, $childModel);

                                $value = $childModel->toArray();

                                $newValue = [];

                                foreach ($value as $childKey => $childValue) {
                                    if ($this->modelService->isAssociativeArray($childValue)) {
                                        $newValue[] = $this->parseModelProperties($childValue, $childModel);
                                    } else {
                                        $newValue[$childKey] = $childValue;
                                    }
                                }
                            }

                            $object[$column] = $newValue;
                        } else {
                            $object[$column] = $value;
                        }
                    }
                }
            }
        } else if (is_array($object)) {
            foreach ($object as $i => $value) {
                $object[$i] = $this->parseModelProperties($value, $parentModel);
            }
        }

        return $object;
    }

    /**
     * @param string|BaseModel|BaseModel[] $parentModel
     * @param BaseModel|mixed[]                      $object
     *
     * @return mixed
     */
    public function slugIds($parentModel, $object = []) {
        $model = $this->modelService->getModel($parentModel);

        if ($object instanceof BaseModel) {
            $object = $object->toArray();
        }

        /** @var BaseModel $model */
        if ($this->modelService->isAssociativeArray($object) || is_null($object)) {
            if (!is_null($object)) {
                $parentModel = $this->modelService->arrayToModel(
                    $object,
                    $parentModel
                );
            }

            $object = $parentModel->toArray();
            $object['id'] = $parentModel->slug();

            if (!is_null($model)) {
                foreach ($object as $column => $value) {
                    if (is_string($column)) {
                        $childModel = $this->modelService->getTableByForeignKey($model, $column);
                    } else {
                        $childModel = $this->modelService->getTableByForeignKey($model, $model->getTable());
                    }

                    if ($childModel) {
                        if ($value instanceof BaseModel) {
                            $value = $value->toArray();
                        }

                        if (is_array($value) && count($value) !== 0) {
                            if (!$this->modelService->isAssociativeArray($value)) {
                                $newValue = [];

                                foreach ($value as $child) {
                                    $newValue[] = $this->slugIds($childModel, $child);
                                }
                            } else {
                                $childModel = $this->modelService->arrayToModel($value, $childModel);

                                $value = $childModel->toArray();
                                if (array_key_exists('id', $value)) {
                                    $value['id'] = $childModel->slug();
                                }

                                $newValue = [];

                                foreach ($value as $childKey => $childValue) {
                                    if ($this->modelService->isAssociativeArray($childValue)) {
                                        $newValue[] = $this->slugIds($childModel, $childValue);
                                    } else {
                                        if (strstr($childKey, '_id') && is_numeric($childValue)) {
                                            $childModel = $this->modelService->getTableByForeignKey($model, $childKey);

                                            if ($childModel) {
                                                /** @var BaseModel|null $childModel */
                                                $childModel = $childModel::query()->find($childValue);
                                                if (!is_null($childModel)) {
                                                    $childValue = $childModel->slug();
                                                }
                                            }
                                        }

                                        $newValue[$childKey] = $childValue;
                                    }
                                }
                            }

                            $object[$column] = $newValue;
                        } else {
                            if (strstr($column, '_id') && is_numeric($value)) {
                                $childModel = $this->modelService->getTableByForeignKey($model, $column);

                                if ($childModel) {
                                    /** @var BaseModel|null $childModel */
                                    $childModel = $childModel::query()->find($value);

                                    if (!is_null($childModel)) {
                                        $value = $childModel->slug();
                                    }
                                }
                            }

                            $object[$column] = $value;
                        }
                    }
                }
            }
        } else if (is_array($object)) {
            foreach ($object as $i => $value) {
                $object[$i] = $this->slugIds($model, $value);
            }
        }

        return $object;
    }

    /**
     * @param mixed[] $fields
     * @param string $parentTable
     * @param Builder $query
     * @param mixed[] $results
     *
     * @return array
     */
    public function getPaginationResponse($fields, $parentTable, $query, $results = []):array {
        $page = $this->requestService->getPage($fields);

        $count = (clone $query)->offset(0)->count($parentTable . '.id');

        $limit = $this->getLimit($fields);

        $pages = $count / $limit;
        if (fmod($pages, 1) != 0) {
            $pages = round($pages + 0.5);
        }

        $canLoadMore = true;
        if ($count < $limit || $page == $pages) {
            $canLoadMore = false;
        }

        $response = [];
        $response['count'] = $count;
        $response['pages'] = $pages;
        $response['can_load_more'] = $canLoadMore;
        $response[$parentTable] = $results;

        return $response;
    }

    /**
     * @param Builder   $query
     * @param BaseModel $parentModel
     * @param string    $joinType
     * @param string    $parentTable
     * @param string    $manyToManyTable
     * @param string    $manyToManyAlias
     * @param BaseModel $manyToManyModel
     * @param string    $parentAlias
     * @param string    $parentKey
     *
     * @return Builder
     */
    private function _includeJoin($query, $parentModel, $joinType, string $parentTable, string $manyToManyTable, string $manyToManyAlias, $manyToManyModel, string $parentAlias, string $parentKey):Builder {
        $manyToManyParentKey = $this->getForeignKey($parentTable);

        $query->$joinType(
            $manyToManyTable . ' AS ' . $manyToManyAlias,
            /**
             * @param Builder $join
             */
            function ($join) use ($parentModel, $manyToManyModel, $manyToManyAlias, $manyToManyParentKey, $parentAlias, $parentKey) {
                $join->on(
                    $parentAlias . '.' . $parentKey,
                    '=',
                    $manyToManyAlias . '.' . $manyToManyParentKey
                );

                if ($parentModel->hasColumn('deleted_at')) {
                    $join->whereNull($parentAlias . '.deleted_at');
                }
            }
        );

        return $query;
    }
}
