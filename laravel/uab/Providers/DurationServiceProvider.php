<?php
namespace Uab\Providers;

use Carbon\Carbon;

class DurationServiceProvider {
    private $modelService;

    public function __construct() {
        $this->modelService = new ModelServiceProvider();
    }

    /**
     * @param string|null $startTime
     * @param string|null $endTime
     *
     * @return int|null
     */
    public function getDay(?string $startTime, ?string $endTime = null):?int {
        if (is_null($startTime)) {
            return null;
        }

        if (is_null($endTime)) {
            $endTime = Carbon::now();
        }

        $startTime = Carbon::createFromFormat('Y-m-d H:s:i', $startTime)->startOfDay();
        $endTime = $endTime->startOfDay();

        return $startTime->diffInDays($endTime, false);
    }

    /**
     * @param string|null $startTime
     * @param string|null $endTime
     *
     * @return int|null
     */
    public function getWeek(?string $startTime, ?string $endTime = null):?int {
        $daysBetween = $this->getDay($startTime, $endTime);
        if (is_null($daysBetween)) {
            return null;
        }

        if ($daysBetween < 0) {
            return null;
        }

        $week = floor($daysBetween / 7) + 1;
        if ($week === false) {
            return null;
        }

        return $week;
    }
}
