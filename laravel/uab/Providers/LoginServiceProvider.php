<?php
namespace Uab\Providers;

use Carbon\Carbon;
use Uab\Http\Models\LoginAttempts;

class LoginServiceProvider {
    /**
     * @param string|int $userId
     * @param Carbon|string|null $start
     * @param Carbon|string|null $end
     *
     * @return bool
     */
    public function hasLoggedIn(string $userId, $start = null, $end = null):bool {
        return $this->countLogins($userId, $start, $end) !== 0;
    }

    /**
     * @param string|int $userId
     * @param Carbon|string|null $start
     * @param Carbon|string|null $end
     *
     * @return int
     */
    public function countLogins(string $userId, $start = null, $end = null):int {
        $query = LoginAttempts::query()
            ->where(
                [
                    'success' => true,
                    'user_id' => $userId
                ]
            );

        if (!is_null($start)) {
            $query->where('attempted_at', '>=', $start);
        }

        if (!is_null($end)) {
            $query->where('attempted_at', '<=', $end);
        }

        return $query->count();
    }
}
