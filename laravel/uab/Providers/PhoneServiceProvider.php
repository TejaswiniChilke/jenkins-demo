<?php
namespace Uab\Providers;

use EllisIO\Phone\Facades\Phone;
use Illuminate\Support\Arr;

class PhoneServiceProvider {
    private $numbers = [];

    /**
     * @param string $number
     *
     * @return string|null
     */
    public function formatNumber($number) {
        if (!is_string($number)) {
            return null;
        }

        $number = preg_replace('/\D/', '', $number);

        if (!is_numeric($number)) {
            return null;
        }

        if (strlen($number) === 10 && strpos($number, '+') === -1) {
            $number = '+'.$number;
        }

        if (strlen($number) === 11 && strpos($number, '+') === 0) {
            return $number;
        }

        if (!Arr::has($this->numbers, $number)) {
            $this->numbers[$number] =  Phone::getPhone($number);
        }

        $telephone = Arr::get($this->numbers, $number, null);
        if (!is_null($telephone)) {
            $number = $telephone->getFormattedNumber();
        }

        $number = preg_replace("/[^0-9]/", "", $number);
        if (strlen($number) === 10) {
            $number = '+1'.$number;
        } else if (strlen($number) === 11 && substr($number, 0, 1) === '1') {
            $number = '+'.$number;
        }

        return $number;
    }

    public function format10($number) {
        $number = $this->formatNumber($number);
        if (is_null($number)) {
            return null;
        }

        return str_replace('+1', '', $number);
    }

    /**
     * @param string $number
     *
     * @return bool
     */
    public function isValid($number) {
        if (strlen($number) === 11) {
            return false;
        }

        if (!is_numeric($number)) {
            return false;
        }

        if (strpos($number, '+1') !== 0) {
            return false;
        }

        $matches = [];
        $matches = preg_match_all("/[0-9]{3}/", $number, $matches);
        if (is_array($matches)) {
            foreach ($matches as $match) {
                $location = strstr($number, $match);
                if ($location === 2 || $location === 5) {
                    return false;
                }
            }
        }

        $matches = preg_match_all("/[0-9]{6}/", $number, $matches);
        if (is_array($matches) && count($matches) !== 0) {
            return false;
        }

        return true;
    }
}
