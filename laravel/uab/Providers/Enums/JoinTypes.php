<?php
namespace Uab\Providers\Enums;

/**
 * Describes log levels.
 */
class JoinTypes {
    const JOIN_TYPE_INNER = '∨';
    const JOIN_TYPE_LEFT  = '<';
    const JOIN_TYPE_OUTER = '^';
    const JOIN_TYPE_RIGHT = '>';

    public static function getJoinTypes() {
        return [
            self::JOIN_TYPE_INNER,
            self::JOIN_TYPE_LEFT,
            self::JOIN_TYPE_OUTER,
            self::JOIN_TYPE_RIGHT
        ];
    }
}
