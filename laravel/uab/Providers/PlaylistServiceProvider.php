<?php
namespace Uab\Providers;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Uab\Http\Models\Playlists;
use Uab\Http\Models\Videos;

class PlaylistServiceProvider {
    /**
     * @param string|int $playlistSlug
     *
     * @return Collection<Videos>|Videos[]
     */
    public function getVideos($playlistSlug) {
        $playlistSlug = Playlists::safeDecodeStatic($playlistSlug);

        $query = Videos::query()
            ->select('videos.*')
            ->rightJoin(
                'playlists_videos',
                'videos.id',
                '=',
                'playlists_videos.video_id'
            )->where(
                [
                    'playlists_videos.playlist_id' => $playlistSlug
                ]
            );

        return $query->get();
    }

    /**
     * @param string|int $userId
     * @param string|int $playlistSlug
     * @param Carbon|string|null $start
     * @param Carbon|string|null $end
     *
     * @return bool
     */
    public function hasCompletedPlaylist($userId, $playlistSlug, $start = null, $end = null):bool {
        $playlist = new Playlists();
        $playlistId = $playlist->safeDecode($playlistSlug);

        /** @var Playlists|null $playlist */
        $playlist = Playlists::query()->find($playlistId);
        if (is_null($playlist)) {
            return false;
        }

        $videos = $playlist->videos();

        $count = $videos->count();
        if ($count === 0) {
            return false;
        }

        $videoService = new VideoServiceProvider();

        foreach ($videos as $video) {
            $hasWatched = $videoService->hasWatchedVideo($userId, $video->id, $start, $end);

            if (!$hasWatched) {
                return false;
            }
        }

        return true;
    }
}
