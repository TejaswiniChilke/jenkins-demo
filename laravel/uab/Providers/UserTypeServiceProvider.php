<?php
namespace Uab\Providers;

use Illuminate\Support\Collection;
use Uab\Http\Enums\UserTypesEnum;
use Uab\Http\Models\Users;
use Uab\Http\Models\UserTypes;

class UserTypeServiceProvider {
    /**
     * @param Users|null $activeUser
     *
     * @return UserTypes[]|Collection
     */
    public function getPermissibleUserTypes($activeUser = null) {
        if (is_null($activeUser)) {
            $activeUser = Auth::user();

            if (is_null($activeUser)) {
                return [];
            }
        }

        $userService = new UsersServiceProvider();

        /** @var UserTypes[] $allUserTypes */
        $allUserTypes = UserTypes::query()->get();
        if ($userService->isAdmin($activeUser)) {
            $userTypes = $allUserTypes;
        } else {
            $userTypes = collect();

            foreach ($allUserTypes as $userType) {
                if (
                    $userType->name === $activeUser->getUserTypeNameAttribute()
                    || $userType->name === UserTypesEnum::ADMIN
                ) {
                    $userTypes->push($userType);
                }
            }
        }

        return $userTypes;
    }
}
