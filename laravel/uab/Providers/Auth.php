<?php
namespace Uab\Providers;

use \Auth as ExternalAuth;
use Uab\Http\Models\Users;

class Auth extends ExternalAuth {
    /**
     * @return Users|null
     */
    public static function user() {
        /** @var Users|null $user */
        $user = parent::user();

        return $user;
    }
}
