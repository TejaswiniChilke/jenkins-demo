<?php
namespace Uab\Providers;

use Carbon\Carbon;
use Config;
use Exception;
use Uab\Http\Models\EmailAttachments;
use Uab\Http\Models\Emails;
use Log;
use Uab\Http\Models\Users;
use Uab\Mail\Email;
use Mail;
use Uab\Providers\Enums\LogLevels;
use Validator;

class EmailServiceProvider {
    /**
     * @param Emails $email
     *
     * @return boolean
     */
    public function canSend(Emails $email):bool {
        if (is_null($email->to_email)) {
            if (is_null($email->to_user_id)) {
                return false;
            }

            /** @var Users|null $toUser */
            $toUser = Users::query()->where('id', '=', $email->to_user_id)->first();
            if (is_null($toUser)) {
                return false;
            }

            $domain = $this->getDomain($email->to_email);
            if (is_null($domain)) {
                return false;
            }

            if (
                in_array(
                    $domain,
                    [
                        'test.com',
                        'test.edu'
                    ]
                )
            )  {
                return false;
            }
        }

        return true;
    }

    /**
     * @param string $emailAddress
     *
     * @return string|null
     */
    public function getDomain(string $emailAddress):?string {
        $atPosition = strpos($emailAddress, '@');
        if ($atPosition === false) {
            Log::log(
                LogLevels::WARNING,
                'Domain not found in email address: ' . $emailAddress
            );

            return null;
        }

        $domain = trim(substr($emailAddress, $atPosition));
        if (strlen($domain) === 1) {
            Log::log(
                LogLevels::WARNING,
                'Email address does not contain domain: ' . $emailAddress
            );

            return null;
        }

        $domain = strtolower(trim(substr($domain, 1)));

        return $domain;
    }

    /**
     * @param string $email
     *
     * @return string
     */
    public function getUserName(string $email):string {
        $domain = $this->getDomain($email);

        return str_replace('@'.$domain, '', $email);
    }

    /**
     * @param string $email
     *
     * @return boolean
     */
    public function isValid(string $email):bool {
        $validator = Validator::make(
            [
                'email' => $email
            ],
            [
                'email' => 'required|email'
            ]
        );

        return $validator->passes() && filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    /**
     * @param Emails             $email
     * @param EmailAttachments[] $attachments
     *
     * @return Emails
     *
     * @throws Exception
     */
    public function send(Emails $email, $attachments = []):?Emails {
        $isValid = $this->isValid($email->to_email);
        if (!$isValid) {
            Log::warning('Attempting to send email to invalid address (' . $email->to_email . ').');

            return null;
        }

        $canSend = $this->canSend($email);
        if (!$canSend) {
            Log::warning('Attempting to send invalid (' . $email->slug() . ').');

            return null;
        }

        if (is_null($email->from_email)) {
            $email->from_email = config('mail.from.address');
        }

        $email->headers = 'From: ' . $email->from_email . "\r\n" .
            'Reply-To: ' . $email->from_email . "\r\n" .
            'X-Mailer: PHP/' . phpversion();

        $email->save();

        $mailable = new Email($email);

        foreach ($attachments as $attachmentData) {
            $attachmentData = json_decode(json_encode($attachmentData), true);

            $attachment = new EmailAttachments();
            $attachment->setRawAttributes($attachmentData);

            if (!is_null($attachment)) {
                $path = $attachment->path;

                $mailable->attach($path);
            }
        }

        $config = Config::get('mail');

        $config['auth_mode'] = false;
        $config['port'] = config('mail.port', 25);
        $config['driver'] = config('mail.driver', 'smtp');
        $config['host'] = config('mail.host');
        $config['username'] = config('mail.username');
        $config['password'] = config('mail.password');
        $config['from']['address'] = $email->from_email;
        $config['from']['name'] = $email->from_email;
        $config['reply_to']['address'] = $email->from_email;
        $config['reply_to']['name'] = $email->from_email;

        Config::set('mail', $config);

        Mail::send(
            $mailable,
            [],
            function() use ($email) {
                $email->sent = Carbon::now();
                $email->exists = true;

                $email->save();
            }
        );

        return $email;
    }
}
