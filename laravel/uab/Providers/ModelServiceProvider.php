<?php
namespace Uab\Providers;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Uab\Http\Models\Base\BaseModel;
use Uab\Providers\Enums\JoinTypes;
use Illuminate\Support\Collection;
use stdClass;

class ModelServiceProvider {
    /**
     * @param mixed[] $array
     * @param string|BaseModel $model
     *
     * @return BaseModel
     */
    public function arrayToModel(array $array, $model) {
        $model = $this->getModel($model);

        if ($model) {
            $model = $model->newInstance();

            $fillable = $model->getFillable();

            $model->setRawAttributes(
                $array
            );

            $model->syncOriginal();

            $model->exists = true;

            $model->fillable($fillable);
        }

        return $model;
    }

    /**
     * @param mixed[] $array
     *
     * @return stdClass
     */
    public function arrayToStd(array $array):stdClass {
        $std = new stdClass();

        foreach ($array as $key => $value) {
            if (is_array($value)) {
                if (count($value) !== 0 && !is_numeric(array_keys($value)[0])) {
                    $std->$key = $this->arrayToStd($value);
                } else {
                    $items = [];

                    foreach ($value as $i) {
                        if (is_array($i)) {
                            $items[] = $this->arrayToStd($i);
                        } else {
                            $items[] = $i;
                        }
                    }

                    $std->$key = $items;
                }
            } else {
                $std->$key = $value;
            }
        }

        return $std;
    }

    public function copy(string $table, string $id, array $fields) {
        $queryService = new QueryServiceProvider();

        $model = $this->getModel($table);

        $results = $queryService->get($table, $fields, $id);
        unset($results['id']);

        $object = Arr::first($results);

        $model->fill($object);
        if ($model->hasColumn('title')) {
            $model->title = $model->title . ' (Copy)';
        } else if ($model->hasColumn('name')) {
            $model->name = $model->name . ' (Copy)';
        }

        $model->save();

        $this->copyChildren($model, $object);

        return $model;
    }

    /**
     * @param BaseModel $parentModel
     * @param mixed[]   $object
     *
     * @return void
     */
    private function copyChildren(BaseModel $parentModel, array $object) {
        $modelService = new ModelServiceProvider();

        foreach ($object as $childTable => $children) {
            if (!is_array($children)) {
                $children = [
                    $children
                ];
            }

            foreach ($children as $child) {
                if (is_array($child) && $parentModel->hasRelationship($childTable)) {
                    $childModel = $modelService->getModel($childTable);

                    unset($child['id']);
                    $child['created'] = null;
                    $child['modified'] = null;

                    foreach ($child as $childProperty => $childValue) {
                        if (strstr($childProperty, '_id')) {
                            /** @var BaseModel $grandChildModel */
                            $grandChildModel = $childModel->getRelatedTable($childProperty);
                            $grandChildModel = $modelService->getModel($grandChildModel);

                            if (is_null($grandChildModel)) {
                                $grandChildModel = $childModel->getRelatedTable($childProperty);
                                $grandChildModel = $childModel->getRelationshipType($grandChildModel);
                                $grandChildModel = $modelService->getModel($grandChildModel);
                            }

                            if (is_string($childValue)) {
                                if ($grandChildModel && !is_null($grandChildModel)) {
                                    $childValue = $grandChildModel::decodeSlug($childValue);
                                }
                            }

                            $child[$childProperty] = $childValue;
                        }
                    }

                    $childModel->fill($child);

                    $relationshipField = $childModel->getRelationship($parentModel->getTable());

                    if (!is_null($relationshipField)) {
                        $childModel->setAttribute($relationshipField, $parentModel->getAttributeValue('id'));
                    } else {
                        $relationshipField = $parentModel->getRelationship($childTable);

                        if (!is_null($relationshipField)) {
                            $parentModel->setAttribute($relationshipField, $childModel->getAttributeValue('id'));
                        } else {
                            $relationshipField = $parentModel->getChildKey($childTable);

                            if (!is_null($relationshipField)) {
                                $childModel->setAttribute($relationshipField, $parentModel->getAttributeValue('id'));
                            }
                        }
                    }

                    $success = $childModel->save();

                    if ($success) {
                        $child['id'] = $childModel['id'];

                        $this->copyChildren($childModel, $child);
                    }
                }
            }
        }
    }

    /**
     * @param BaseModel|string $model
     *
     * @return string
     */
    public function getForeignKey($model):string {
        $table = $this->getModel($model)->getTable();
        $table = strtolower($table);
        $table = Str::singular($table);

        return $table . '_id';
    }

    /**
     * @param $slug
     * @param null $model
     *
     * @return int|string|null
     */
    public function getIdFromSlug($slug, $model = null) {
        $model = $this->getModel($model);

        return $model->safeDecode($slug);
    }

    /**
     * @param BaseModel|string      $table
     * @param BaseModel|string|null $parentTable
     *
     * @return bool|BaseModel
     */
    public function getModel($table, $parentTable = null) {
        $result = false;

        if (is_string($table)) {
            $modelClass = 'Uab\\Http\\Models\\' . Str::studly($table);

            if (class_exists($modelClass, true)) {
                $result = new $modelClass;
            }
        } else if ($table instanceof BaseModel) {
            $result = $table;
        }

        if ($result === false && !is_null($parentTable)) {
            $parentModel = $this->getModel($parentTable);

            $type = $parentModel->getRelationshipType($table);

            $result = $this->getModel($type);
        }

        return $result;
    }

    public function getObject($table, $slug) {
        $model = $this->getModel($table);

        if ($model !== false) {
            $id = $model::safeDecodeStatic($slug);

            return $model::query()->find($id);
        }

        return null;
    }

    /**
     * @param $model
     * @param bool $join
     *
     * @return string
     */
    public function getAlias($model, $join = false) {
        if (is_string($join)) {
            $alias = $join;
        } else {
            $model = $this->getModel($model);
            $alias = $model->getTable();
        }

        return Str::snake($alias);
    }

    public function getAliasAs($model) {
        $model = $this->getModel($model);

        return $model->getTable().' AS '.$this->getAlias($model);
    }

    private function _getAliasedFields($model, $join = false) {
        $model = $this->getModel($model);

        $fields = [];

        $attributes = $model->getColumns();
        foreach ($attributes as $attribute) {
            $alias = $this->getAlias($model, $join);

            $fields[] = $alias.'.'.$attribute.' AS '.$alias.'__'.$attribute;
        }

        return $fields;
    }

    /**
     * @param $model
     * @param array|string $fields
     * @param bool $joins
     *
     * @return array|string
     */
    public function getFields($model, $fields = [], $joins = false) {
        if (is_array($fields)) {
            $fields = [];

            $joins = $this->getJoins($joins);
            foreach ($joins as $joinType => $joinArray) {
                foreach ($joinArray as $join) {
                    $nestedJoins = explode('.', $join);

                    foreach ($nestedJoins as $nestedJoin) {
                        $childModel = $this->getModel($nestedJoin, $model);

                        if ($childModel) {
                            $fields = array_merge($fields, $this->_getAliasedFields($childModel, $nestedJoin));
                        }
                    }
                }
            }

            $fields = array_merge($fields, $this->_getAliasedFields($model));

            if (count($fields) === 0) {
                $fields = '*';
            }
        } else {
            $fields = explode(',', $fields);
        }

        return $fields;
    }

    public function getJoins($joins) {
        if (!is_array($joins)) {
            $joins = explode(',', $joins);
        }

        foreach ($joins as $i => $join) {
            $prefix = substr($join, 0,1);
            if (in_array($prefix, JoinTypes::getJoinTypes())) {
                $join = substr($join, 1);
            } else {
                $prefix = '∨';
            }

            unset($joins[$i]);
            if (!array_key_exists($prefix, $joins)) {
                $joins[$prefix] = [];
            }

            $join = Str::snake($join);
            $join = str_replace('._', '.', $join);

            $joins[$prefix][] = $join;
        }

        return $joins;
    }

    public function getManyToManyModel($a, $b) {
        $a = $this->getModel($a);
        $b = $this->getModel($b);

        if ($a && $b) {
            $a = $a->getTable();
            $b = $b->getTable();

            if (strcasecmp($a, $b) < 0) {
                $table = $a . '_' . $b;
            } else {
                $table = $b . '_' . $a;
            }

            return $this->getModel($table);
        }

        return false;
    }

    public function isAssociativeArray($arr) {
        if (!is_array($arr) || array() === $arr) {
            return false;
        }

        return array_keys($arr) !== range(0, count($arr) - 1);
    }

    public function mergeObjects($alpha, $beta) {
        foreach ($beta as $betaKey => $betaValue) {
            if (!array_key_exists($betaKey, $alpha)) {
                $alpha[$betaKey] = $betaValue;
            } else {
                if (array_key_exists('id', $alpha) && array_key_exists('id', $beta)) {
                    if (is_array($alpha[$betaKey]) && is_array($beta[$betaKey])) {
                        if (array_key_exists('id', $alpha[$betaKey]) && array_key_exists('id', $beta[$betaKey]) && $alpha[$betaKey]['id'] === $beta[$betaKey]['id']) {
                            $alpha[$betaKey] = $this->mergeObjects(
                                $alpha[$betaKey],
                                $beta[$betaKey]
                            );
                        } else {
                            if ($this->isAssociativeArray($alpha[$betaKey])) {
                                $alpha[$betaKey] = [
                                    $alpha[$betaKey]
                                ];
                            }
                        }
                    }
                }

                if (is_array($alpha[$betaKey])) {
                    if ($this->isAssociativeArray($beta[$betaKey])) {
                        $alpha[$betaKey][] = $beta[$betaKey];
                    } else {
                        $alpha[$betaKey] = array_merge(
                            $alpha[$betaKey],
                            $beta[$betaKey]
                        );
                    }
                }
            }
        }

        return $alpha;
    }

    /**
     * @param stdClass $std
     *
     * @return mixed[]
     */
    public function stdToArray($std) {
        return (array) $std;
    }

    /**
     * @param stdClass $std
     * @param BaseModel $model
     *
     * @return BaseModel
     */
    public function stdToModel(stdClass $std, $model) {
        $array = $this->stdToArray($std);

        return $this->arrayToModel($array, $model);
    }

    /**
     * @param Collection     $collection
     * @param bool|BaseModel $model
     * @param bool|string[]  $joins
     *
     * @return mixed
     */
    public function breakDown($collection, $joins, $model) {
        $model = $this->getModel($model);

        $parent = $collection->get($model->getTable());

        $joins = $this->getJoins($joins);
        foreach ($joins as $joinType) {
            foreach ($joinType as $join) {
                $nestedJoins = explode('.', $join);

                $childTable = $nestedJoins[0];

                $relationships = $model->getRelationships();
                if (Arr::has($relationships, $childTable)) {
                    if (is_null($relationships[$childTable])) {
                        $parent[$childTable] = [];
                    } else {
                        $parent[$childTable] = null;
                    }
                } else {
                    $parent[$childTable] = [];
                }

                $childObject = $collection->get($childTable);

                if (!is_null($childObject)) {
                    $childId = 0;
                    if (array_key_exists('id', $childObject)) {
                        $childId = $childObject['id'];
                    }

                    if (is_numeric($childId) && $childId > 0) {
                        $nestedJoins = array_splice($nestedJoins, 1);

                        if (count($nestedJoins) > 0) {
                            $result = $this->breakDown(
                                $collection,
                                [
                                    implode('.', $nestedJoins)
                                ],
                                $this->getModel($childTable)
                            );
                        } else {
                            $result = $childObject;
                        }

                        if (is_null($parent[$childTable])) {
                            $parent[$childTable] = $result;
                        } else {
                            $parent[$childTable][] = $result;
                        }
                    } else if (!Arr::has($relationships, $childTable)) {
                        $parent[$childTable] = [];
                    }
                }
            }
        }

        return $parent;
    }

    public function getTable($model) {
        return $this->getModel($model)->getTable();
    }

    /**
     * @param BaseModel|string $model
     * @param string           $column
     *
     * @return bool|BaseModel
     */
    public function getTableByForeignKey($model, $column) {
        $table = str_replace('_id', '', $column);
        $table = Str::plural($table);

        $model = $this->getModel($model);
        if (!is_null($model)) {
            $relationship = $model->getRelationshipType($table);
            $model = $this->getModel($relationship);
        }

        return $model;
    }
}
