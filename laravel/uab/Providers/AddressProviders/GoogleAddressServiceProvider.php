<?php
namespace Uab\Providers\AddressProviders;

use Uab\Http\Models\GoogleAddresses;
use Illuminate\Support\Arr;

class GoogleAddressServiceProvider implements AddressProviderInterface {
    /**
     * @param mixed[] $addressArray
     *
     * @return null|GoogleAddresses
     */
    public function validateAddress($addressArray) {
        $googleAddress = null;

        $full_address = '';

        $location = '';
        if (Arr::has($addressArray, 'location')) {
            $location = Arr::get($addressArray, 'location', '');

            $location = substr($location, 0, 50);

            $full_address .= $location;
        }

        $city = '';
        if (Arr::has($addressArray, 'city')) {
            $city = Arr::get($addressArray, 'city', '');

            $city = substr($city, 0, 40);

            $full_address .= ',' . $city;
        }

        $state = '';
        if (Arr::has($addressArray, 'state')) {
            $state = Arr::get($addressArray, 'state', '');

            $state = substr($state, 0, 20);

            $full_address .= ',' . $state;
        }

        $addressLineOne = Arr::get($addressArray, 'address_line_one', null);
        $zipcode = Arr::get($addressArray, 'zipcode', null);

        if (strlen($full_address) !== 0) {
            $alreadyExistsQuery = GoogleAddresses::query()
                ->where(
                    [
                        'requested_address_line_one' => $addressLineOne,
                        'requested_city'             => $city,
                        'requested_location'         => $location,
                        'requested_state'            => $state,
                        'requested_zipcode'          => $zipcode
                    ]
                );

            $alreadyExists = $alreadyExistsQuery->exists();

            if ($alreadyExists) {
                $googleAddress = $alreadyExistsQuery->first();
            } else {
                $url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($full_address) . '&key='.config('google.key');

                // $url = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address=5300%20crowne%20chase%20pkway%20hoover&key=AIzaSyDhITOJJPXdscLxnmQToloqkKLB7XlW9wA'));
                $response = json_decode(file_get_contents($url));

                if ($response->status === 'OK') {
                    // TODO: Save geometry from request
                    $googleAddress = new GoogleAddresses();

                    $googleAddress->requested_city = $city;
                    $googleAddress->requested_location = $location;
                    $googleAddress->requested_state = $state;

                    foreach ($response->results[0]->address_components as $address_component) {
                        if ($address_component->types[0] === 'street_number') {
                            $googleAddress->returned_street_number = $address_component->long_name;
                        }

                        if ($address_component->types[0] === 'route') {
                            $googleAddress->returned_route = $address_component->long_name;
                        }

                        if ($address_component->types[0] === 'locality') {
                            $googleAddress->returned_locality = $address_component->long_name;
                        }

                        if ($address_component->types[0] === 'administrative_area_level_2') {
                            $googleAddress->returned_county = $address_component->long_name;
                        }

                        if ($address_component->types[0] === 'colloquial_area') {
                            $googleAddress->returned_county = $address_component->long_name;
                        }

                        if ($address_component->types[0] === 'administrative_area_level_1') {
                            $googleAddress->returned_state_long_name = $address_component->long_name;
                            $googleAddress->returned_state_short_name = $address_component->short_name;
                        }

                        if ($address_component->types[0] === 'postal_code') {
                            $googleAddress->returned_zipcode = $address_component->short_name;
                        }
                    }

                    if (isset($response->results)) {
                        $results = Arr::get($response->results, 0, null);

                        if (isset($results->place_id)) {
                            $googleAddress->returned_place_id = $results->place_id;
                        }

                        if (isset($results->formatted_address)) {
                            $googleAddress->returned_address = $results->formatted_address;
                        }

                        if (isset($results->geometry)) {
                            $geometry = $results->geometry;

                            if (isset($geometry->location)) {
                                $location = $geometry->location;

                                $googleAddress->returned_lat = $location->lat;
                                $googleAddress->returned_long = $location->lng;
                            }
                        }
                    }

                    $googleAddress->save();

                    $addressService = new GoogleAddressServiceProvider();
                    $addressService->validateAddress(
                        [
                            'requested_location' => $location,
                            'requested_city' => $city,
                            'requested_state' => $state
                        ]
                    );
                }
            }
        }

        return $googleAddress;
    }
}
