<?php
namespace Uab\Providers\AddressProviders;

use Uab\Http\Models\UspsAddresses;
use Illuminate\Support\Arr;
use Usps;

class UspsServiceProvider implements AddressProviderInterface {
    function validateAddress($addressArray) {
        $line1 = Arr::get($addressArray, 'line1', null);
        $line2 = Arr::get($addressArray, 'line2', null);
        $city = Arr::get($addressArray, 'city', null);
        $state = Arr::get($addressArray, 'state', null);
        $zipcode = Arr::get($addressArray, 'zipcode', null);

        $address = UspsAddresses::query()
            ->where(
                [
                    'og_address_line_1' => $line1,
                    'og_address_line_2' => $line2,
                    'og_city'           => $city,
                    'og_state'          => $state,
                    'og_zipcode'        => $zipcode,
                ]
            )->first();

        if (is_null($address)) {
            $uspsAddress = Usps::validate(
                $line1,
                $city,
                $state,
                $zipcode
            );

            if (is_array($uspsAddress) && array_key_exists('address', $uspsAddress)) {
                $uspsAddress = $uspsAddress['address'];

                $address = new UspsAddresses();
                $address->og_address_line_1 = $line1;
                $address->og_address_line_2 = $line2;
                $address->og_city = $city;
                $address->og_state = $state;
                $address->og_zipcode = $zipcode;

                $address = $this->addUspsAddress($address, $uspsAddress);

                $address->save();
            }
        }

        return $address;
    }

    private function addUspsAddress($address, $uspsAddress) {
        if (array_key_exists('Address1', $uspsAddress)) {
            $address->new_address_line_1 = $uspsAddress['Address2'];
        }

        if (array_key_exists('Address2', $uspsAddress)) {
            $address->new_address_line_2 = $uspsAddress['Address2'];
        }

        if (array_key_exists('city', $uspsAddress)) {
            $address->new_city = $uspsAddress['City'];
        }

        if (array_key_exists('State', $uspsAddress)) {
            $address->new_state = $uspsAddress['State'];
        }

        if (array_key_exists('Zip5', $uspsAddress)) {
            $address->new_zipcode = $uspsAddress['Zip5'];
        }

        if (array_key_exists('Zip4', $uspsAddress)) {
            $address->new_zipcode_suffix = $uspsAddress['Zip4'];
        }

        if (array_key_exists('ReturnText', $uspsAddress)) {
            $address->return_text = $uspsAddress['ReturnText'];
        }

        if (array_key_exists('@attributes', $uspsAddress)) {
            $address->attributes = json_encode($uspsAddress['@attributes']);
        }

        return $address;
    }
}
