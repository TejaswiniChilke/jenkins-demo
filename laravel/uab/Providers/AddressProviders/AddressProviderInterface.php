<?php
namespace Uab\Providers\AddressProviders;

use Uab\Http\Models\ZipcodeGeometries;

interface AddressProviderInterface {
    /**
     * @param mixed[]
     *
     * @return ZipcodeGeometries[]
     */
    public function validateAddress($addressArray);
}
