<?php
namespace Uab\Providers;

use Carbon\Carbon;
use Twilio\Rest\Api\V2010\Account\CallInstance;
use Uab\Http\Enums\PhoneDirectionsEnum;
use Uab\Http\Models\AnalyticTrackCalls;
use Uab\Http\Models\AnalyticVideos;
use Uab\Http\Models\Users;
use Uab\Http\Models\Videos;

class AnalyticServiceProvider {
    private $modelService;

    public function __construct() {
        $this->modelService = new ModelServiceProvider();
    }

    /**
     * @param Users       $user
     * @param integer     $week
     * @param Videos|null $video
     *
     * @return AnalyticVideos[]
     */
    public function getWatches($user, $week = 1, $video = null) {
        $params = [
            'user_id' => $user->id
        ];

        if (!is_null($video)) {
            $params['video_id'] = $video->id;
        }

        $currentWeek = $user->getAttributeValue('week');

        if ($week <= $currentWeek) {
            $daysBefore = $currentWeek - $week;
            $daysAfter = $daysBefore * 6;

            return AnalyticVideos::where($params)
                ->where(
                    'watch_datetime',
                    '>=',
                    'CURDATE() - INTERVAL DAYOFWEEK(CURDATE())+'.$daysAfter.' DAY'
                )->where(
                    'watch_datetime',
                    '<',
                    'CURDATE() - INTERVAL DAYOFWEEK(CURDATE())-'.$daysBefore.' DAY'
                )->get()->toArray();
        }

        return [];
    }

    /**
     * @param CallInstance $call
     *
     * @return AnalyticTrackCalls|null
     */
    public function trackCall($call) {
        $recordOutgoingCall = new AnalyticTrackCalls();
        $recordOutgoingCall->call_start_time = Carbon::createFromTimestamp($call->startTime->getTimestamp());
        $recordOutgoingCall->to_number = $call->to;
        $recordOutgoingCall->from_number = $call->from;
        $recordOutgoingCall->direction = PhoneDirectionsEnum::OUTGOING;
        $recordOutgoingCall->status = $call->status;

        $toUser = Users::query()->where(
            [
                'mobile_number' => $call->to
            ]
        )->first();

        if (!is_null($toUser)) {
            $recordOutgoingCall->to_user_id = $toUser->id;
        }

        $fromUser = Users::query()->where(
            [
                'mobile_number' => $call->from
            ]
        )->first();

        if (!is_null($fromUser)) {
            $recordOutgoingCall->from_user_id = $fromUser->id;
        }

        $success = $recordOutgoingCall->save();

        return $success ? $recordOutgoingCall : null;
    }
}
