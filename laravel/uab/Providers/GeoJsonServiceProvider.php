<?php
namespace Uab\Providers;

use stdClass;
use Uab\Http\Models\ZipcodeGeometries;

class GeoJsonServiceProvider {
    public function __construct() {

    }

    /**
     * @param ZipcodeGeometries[] $zipcodeGeometries
     * @param mixed[]|callable    $properties
     *
     * @return stdClass
     */
    public function getGeoJson($zipcodeGeometries, $properties = []) {
        $json = new stdClass();
        $json->type = 'FeatureCollection';
        $json->features = [];

        foreach ($zipcodeGeometries as $zipcodeGeometry) {
            $feature = new stdClass();
            $feature->type = 'Feature';

            $feature->geometry = new stdClass();
            $feature->geometry->type = $zipcodeGeometry->type;
            $feature->geometry->coordinates = json_decode($zipcodeGeometry->coordinates);

            $feature->properties = [];
            if (is_array($properties)) {
                $feature->properties = $properties;
            } else if (is_callable($properties)) {
                $feature->properties = call_user_func($properties, $zipcodeGeometry);
            }

            $json->features[] = $feature;
        }

        return $json;
    }
}
