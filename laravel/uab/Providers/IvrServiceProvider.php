<?php
namespace Uab\Providers;

use Carbon\Carbon;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use League\OAuth2\Server\Exception\OAuthServerException;
use Twilio\Exceptions\ConfigurationException;
use Twilio\Exceptions\TwilioException;
use Twilio\Rest\Client;
use Uab\Exceptions\PhoneNumberMissing;
use Uab\Exceptions\PincodeMissing;
use Uab\Exceptions\WrongPincode;
use Uab\Http\Enums\AnswerInputTypesEnum;
use Uab\Http\Enums\QueueTypesEnum;
use Uab\Http\Enums\UserTypesEnum;
use Uab\Http\Models\AnalyticTrackCalls;
use Uab\Http\Models\AnswerInputTypes;
use Uab\Http\Models\AnswerOptions;
use Uab\Http\Models\Answers;
use Uab\Http\Models\CallWelcomeMessages;
use Uab\Http\Models\Questions;
use Uab\Http\Models\SurveyPages;
use Uab\Http\Models\Surveys;
use Uab\Http\Models\SurveysUsers;
use Uab\Http\Models\Users;
use Uab\Http\Models\UserTypes;
use Uab\Jobs\Notifications\NotifyUserType;

class IvrServiceProvider {
    private $surveyService;

    public function __construct() {
        $this->surveyService = new SurveyServiceProvider();
    }

    /**
     * @param UabVoiceResponse $voiceResponse
     * @param Answers          $answer
     *
     * @return UabVoiceResponse
     */
    public function addAnswerOptionResponse($voiceResponse, $answer) {
        if (!is_null($answer->answer_option_id)) {
            $answerOption = AnswerOptions::query()->find($answer->answer_option_id);

            if (!is_null($answerOption) && !is_null($answerOption->response)) {
                $voiceResponse->say($answerOption->response);
            }
        }

        return $voiceResponse;
    }

    public function isScheduleSet($user) {
        return Users::query()->where(
            [
                'preferred_session_times'   =>   $user->preferred_session_times
            ]
        )->exists();
    }

    /**
     * @param Users $user
     *
     * @return AnalyticTrackCalls
     *
     * @throws TwilioException
     * @throws ConfigurationException
     */
    public function makeCall($user) {
        $client = new Client(
            config('twilio.account.sid'),
            config('twilio.auth.token')
        );

        $call = $client->calls->create(
            $user->mobile_number,
            config('twilio.phone_number'),
            [
                'record' => true,
                'url'    => config('app.backend') . 'voice/auth/0/'.$user->mobile_number
            ]
        );

        $analyticService = new AnalyticServiceProvider();

        return $analyticService->trackCall($call);
    }

    /**
     * @param string      $surveyId
     * @param string      $questionId
     * @param string      $response
     * @param string|null $answerOptionId
     *
     * @return Answers|null
     */
    public function savePreviousResponse($surveyId, $questionId, $response, $answerOptionId = null) {
        $user = Auth::user();
        if (!is_null($user)) {
            $surveyUser = SurveysUsers::query()
                ->where([
                    'user_id' => $user->id,
                    'survey_id' => $surveyId,
                    'is_locked' => 0
                ])
                ->first();

            if (!is_null($response) && $response !== '*' && !is_null($questionId)) {
                $answer = new Answers();
                $answer->response = $response;
                $answer->question_id = $questionId;
                $answer->answer_option_id = $answerOptionId;
                $answer->surveys_users_id = $surveyUser->id;

                if ($answer->save()) {
                    return $answer;
                }
            }
        }

        return null;
    }

    public function notifyAdmin($userId) {
        $user = User::query()->find($userId);

        NotifyUserType::dispatch(
            UserTypes::ADMIN,
            $user->mobile_number,
            'Participant notified about pain during exercise.'
        )->onQueue(QueueTypesEnum::HIGH);
    }

    /**
     * @param bool $incoming
     *
     * @return CallWelcomeMessages
     */
    public function getWelcomeMessage($incoming = true) {
        return CallWelcomeMessages::query()->where(
            [
                'call_status' => 0,
//                'incoming'    => $incoming TODO
            ]
        )->get()->random(1)->first();
    }

    public function getAnswerOption($questionId, $position) {
        $answerOption = AnswerOptions::query()->where(
            [
                'parent_question_id'      =>  $questionId,
                'position'                =>  $position
            ]
        )->first();
        return $answerOption;
    }
    public function getAnswerInputType($questionId) {
        /** @var Questions $question */
        $question = Questions::query()->find($questionId);
        if ($question) {
            return $question->answerInputType()->type;
        }
        return null;
    }

    public function participantSchedule($userId, UabVoiceResponse $voiceResponse) {
        $action = '';

        $gather = $voiceResponse->gather(
            [
                'action'     =>  $action,
                'numDigits'  =>  10,
                'method'     =>  'GET',
                'timeout'    =>  7
            ]
        );

        $gather->say('Enter participant phone number that you want to work with:');

        return $voiceResponse;
    }

    /**
     * @return Users|null
     *
     * @throws OAuthServerException
     * @throws PhoneNumberMissing
     */
    public function findUser($phone = null) {
        if (!is_null($phone)) {
            $phoneService = new PhoneServiceProvider();
            $phone = $phoneService->formatNumber($phone);
        }

        if (is_null($phone)) {
            throw new PhoneNumberMissing();
        }

        $oauthService = new OAuthServiceProvider();

        $phoneService = new PhoneServiceProvider();
        $phone = $phoneService->formatNumber($phone);

        return $oauthService->findActiveUser($phone, 'mobile_number');
    }

    /**
     * @param Request $request
     * @param Users $user
     *
     * @return bool
     */
    public function validatePinCode($request, $user) {
        $pinCode = $request->input('Digits', null);

        if (is_null($pinCode) || strlen($pinCode) !== 4) {
            throw new PincodeMissing();
        }

        $oauthService = new OAuthServiceProvider();

        $doesPinCodeMatch = $oauthService->validateUser(
            $user,
            [
                'pinCode' => $pinCode
            ]
        );

        if ($doesPinCodeMatch) {
            return true;
        } else {
            throw new WrongPincode();
        }
    }


    public function getUrl($path) {
        $backendUrl = config('app.backend');

        $group = 'voice';
        if ($path[0] !== '/') {
            $group = $group . '/';
        }

        if ($backendUrl[-1] !== '/') {
            $group = '/' . $group;
        }

        $url = $backendUrl . $group . $path;

        $user = Auth::user();
        if (!is_null($user)) {
            $token = $user->token();

            if (!is_null($token)) {
                if (strstr($url, '?') === false) {
                    $url .= '?';
                } else {
                    $url .= '&';
                }

                $url .= 'token=' . $token->id;
            }
        }

        return $url;
    }

    public function getQuestion($answerOptionId) {
        if (!is_null($answerOptionId)) {
            $answerOption = AnswerOptions::query()
                ->where(
                    [
                        'id'   =>   $answerOptionId
                    ]
                )->first();
            if (!is_null($answerOption)) {
                $question = Questions::query()
                    ->where(
                        [
                            'id'    =>   $answerOption->parent_question_id
                        ]
                    )->first();

                return $question;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * @param string|number $questionId
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null|object
     */
    public function getSurvey($questionId) {
        if (!is_null($questionId)) {
            $question = Questions::query()
                ->where(
                    [
                        'id'    =>   $questionId
                    ]
                )->first();
            if (!is_null($question)) {
                $surveyPage = SurveyPages::query()
                    ->where(
                        [
                            'id'  =>    $question->survey_page_id
                        ]
                    )->first();
                if (!is_null($surveyPage)) {
                    return Surveys::query()
                        ->where(
                            [
                                'id'   =>   $surveyPage->survey_id
                            ]
                        )->first();
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * @param string|number $surveyId
     * @param string|number $userId
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null|object
     */
    public function getSurveyUser($surveyId, $userId = null) {
        if (is_null($userId))  {
            $userId = Auth::id();
        }

        return SurveysUsers::query()
            ->where(
                [
                    'survey_id' => $surveyId,
                    'user_id'   => $userId
                ]
            )->orderBy('created', 'desc')->first();
    }

    public function makeParticipantSchedule(UabVoiceResponse $voiceResponse, $message, $userId) {
        $gather = $voiceResponse->gather(
            [
                'numDigits'  =>  4,
                'method'     =>  'GET',
                'timeout'    =>  8
            ]
        );

        $gather->setAction(config('app.backend') . 'voice/scheduleSet/1/'. $userId);
        $gather->say($message);
        return $voiceResponse;
    }

    public function getUserIncomingCallCount($userId) {
        return AnalyticTrackCalls::query()
            ->where(
                [
                    'from_user_id'  => $userId
                ]
            )->get()->count();
    }

    public function getUserOutgoingCallCount($userId) {
        return AnalyticTrackCalls::query()
            ->where(
                [
                    'to_user_id'  => $userId
                ]
            )->get()->count();
    }

    public function getUserCallCount($userId) {
        $outgoing = AnalyticTrackCalls::query()
            ->where(
                [
                    'to_user_id'  => $userId
                ]
            )->get()->count();
        $incoming = AnalyticTrackCalls::query()
            ->where(
                [
                    'from_user_id'  => $userId
                ]
            )->get()->count();
        return $outgoing + $incoming;
    }

    private function getWeeklyTip() {
    }

    public function getUserInterventionDayCount($userStartDate) {
        $startDate = Carbon::parse($userStartDate);
        $now = Carbon::now();

        return $startDate->diffInDays($now);
    }

    public function getUserCompletedCallsCount($userId) {
        $incomingCompletedCallCount = AnalyticTrackCalls::query()
            ->where(
                [
                    'from_user_id'   => $userId,
                    'status'         => 'completed'
                ]
            )->get()->count();
        $outgoingCompletedCallCount = AnalyticTrackCalls::query()
            ->where(
                [
                    'to_user_id'   => $userId,
                    'status'         => 'completed'
                ]
            )->get()->count();
        return ($incomingCompletedCallCount + $outgoingCompletedCallCount);
    }

    public function getUserIncentivesCount($userId) {
        $user = Users::query()
            ->where(['id'    => $userId])->first();
        if (!is_null($user->ivr_voice_gender)) {
            $user->ivr_voice_gender = $user->ivr_voice_gender + 25;
        } else {
            $user->ivr_voice_gender = 25;
        }
        $addedIncentive = $user->update(['ivr_voice_gender'  => $user->ivr_voice_gender]);
        $user = Users::query()
            ->where(['id'    => $userId])->first();
        return $user->ivr_voice_gender;
    }

    public function getUserMissedCallCount($userId) {
        $incomingMissedCallCount = AnalyticTrackCalls::query()
            ->where(
                [
                    'from_user_id'   => $userId,
                    'status'         => 'missed'
                ]
            )->get()->count();
        $outgoingMissedCallCount = AnalyticTrackCalls::query()
            ->where(
                [
                    'to_user_id'   => $userId,
                    'status'         => 'missed'
                ]
            )->get()->count();
        return ($incomingMissedCallCount + $outgoingMissedCallCount);
    }

    public function getAnswerResponseFromLastCompletedSurvey($type, $userId = null) {
        if (is_null($userId))  {
            $userId = Auth::id();
        }

        /** @var Collection<SurveysUsers> $surveyUsers */
        $surveyUsers = SurveysUsers::query()->distinct()->select(['survey_id', 'id'])
            ->where(
                [
                    'user_id' => $userId,
                    'is_locked' => true
                ]
            )->groupBy(['id'])->orderBy('response_order', 'desc')
            ->get();

        foreach ($surveyUsers as $surveyUser) {
                /** @var Surveys $survey */
                $survey = Surveys::query()->find($surveyUser->id);

                if (!is_null($survey)) {
                    /** @var Collection<SurveyPages> $pages */
                    $pages = $survey->surveyPages();
                    foreach ($pages as $page) {
                        /** @var Questions[] $questions */
                        $questions = $page->questions();

                        foreach ($questions as $question) {
                            if ($question->answerInputType()->type === $type) {
                                return Answers::query()
                                    ->where(
                                        [
                                            'question_id'      => $question->id,
                                            'surveys_users_id' => $surveyUser->id
                                        ]
                                    )->orderBy('created', 'desc')->first();
                            }
                        }
                    }
                }
            }
    }

    public function getAnswerResponseFromLastQuestion($type, $userId = null) {
        if (is_null($userId))  {
            $userId = Auth::id();
        }

        $surveyUsers = SurveysUsers::query()
            ->where('user_id', '=', $userId)
            ->get();

        $surveyUserIds = $surveyUsers->pluck('id');

        $type = AnswerInputTypes::query()
            ->where('type', '=', $type)
            ->first();

        if (!is_null($type)) {
            $questionIds = Questions::query()
                ->where('answer_input_type_id', '=', $type->id)
                ->get()
                ->pluck('id');

            return Answers::query()
                ->whereIn('answers.surveys_users_id', $surveyUserIds)
                ->whereIn('answers.question_id', $questionIds)
                ->orderBy('answers.created', 'desc')
                ->first();
        }

        return null;
    }

    public function isUserCompletedSurvey($surveyUser) {
        $surveyId = $surveyUser->survey_id;
        $surveyPage = SurveyPages::query()->where(
            [
                'survey_id'  => $surveyId
            ]
        )->first();

        if (!is_null($surveyPage)) {
            $questions = \Uab\Http\Models\Questions::query()->where(
                [
                    'survey_page_id' => $surveyPage->id,
                    'required'       => 1
                ]
            )->get();
            if (!is_null($questions)) {
                foreach ($questions as $question) {
                    $answer = Answers::query()->where(
                        [
                            'surveys_users_id' => $surveyUser->id,
                            'question_id'      => $question->id
                        ]
                    )->first();
                    if (is_null($answer)) {
                        return false;
                    }
                }
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    // <% IvrServiceProvider@getSteps %>
    public function getSteps() {
        $answerType = $this->getAnswerResponseFromLastCompletedSurvey(AnswerInputTypesEnum::STEPS);

        if (!is_null($answerType)) {
            return $answerType->response;
        }

        return null;
    }

    // <% IvrServiceProvider@getCurrentSteps %>
    public function getCurrentSteps() {
        $answerType = $this->getAnswerResponseFromLastQuestion(AnswerInputTypesEnum::STEPS);

        if (!is_null($answerType)) {
            return $answerType->response;
        }

        return null;
    }

    // <% IvrServiceProvider@getActiveMinutes %>
    public function getActiveMinutes() {
        $answerType = $this->getAnswerResponseFromLastCompletedSurvey(AnswerInputTypesEnum::MINUTES);

        if (!is_null($answerType)) {
            return $answerType->response;
        }

        return null;
    }

    // <% IvrServiceProvider@getCurrentActiveMinutes %>
    public function getCurrentActiveMinutes() {
        $answerType = $this->getAnswerResponseFromLastQuestion(AnswerInputTypesEnum::MINUTES);

        if (!is_null($answerType)) {
            return $answerType->response;
        }

        return null;
    }

    // <% IvrServiceProvider@getCurrentStepGoal %>
    public function getCurrentStepGoal() {
        $answerType = $this->getAnswerResponseFromLastQuestion(AnswerInputTypesEnum::GOAL_STEPS);

        if (!is_null($answerType)) {
            return $answerType->response;
        }

        return null;
    }

    public function getStepGoal() {
        $answerType = $this->getAnswerResponseFromLastCompletedSurvey(AnswerInputTypesEnum::GOAL_STEPS);

        if (!is_null($answerType)) {
            return $answerType->response;
        }

        return null;
    }

    public function getQuestionAnswer($questionId) {
        $survey = $this->getSurvey($questionId);

        if (!is_null($survey)) {
            $surveyUser = $this->getSurveyUser($survey->id);

            if (!is_null($surveyUser)) {
                $surveyUserId = $surveyUser->id;

                if (!is_null($surveyUserId)) {
                    $query = Answers::query()
                        ->where(
                            [
                                'question_id'      => $questionId,
                                'surveys_users_id' => $surveyUserId
                            ]
                        )->orderBy('created', 'desc');

                    $answer = $query->first();

                    return $answer->response;
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    // TODO: Move to custom service
    // TODO: Update questionIds after seeder run
    public function handleTrackingStepsSkipLogic($question) {
        $steps = $this->getQuestionAnswer($question->id);
        if (!is_null($steps)) {
            if ($steps < 3000 || $steps > 10000) {
                return 986;
            } else {
                return 987;
            }
        } else {
            return null;
        }
    }

    // TODO: Move to custom service
    // TODO: Update questionIds after seeder run
    public function handleTrackingMinutesSkipLogic($question) {
        $minutes = $this->getQuestionAnswer($question->id);
        if (!is_null($minutes)) {
            if ($minutes > 150) {
                return 990;
            } else {
                return 997;
            }
        } else {
            return null;
        }
    }

    // TODO: Move to custom service
    // TODO: Update questionIds after seeder run
    public function handlePhysicalActivitySkipLogic($question) {
        $minutesCheckResponse = $this->getQuestionAnswer($question->id);
        if (!is_null($minutesCheckResponse)) {
            if ($minutesCheckResponse === '1') {
                return 899;
            } else {
                return 890;
            }
        } else {
            return null;
        }
    }

    // TODO: Move to custom service
    // TODO: Update questionIds after seeder run
    public function handleSCTStepsSkipLogic($question) {
        $steps = $this->getQuestionAnswer($question->id);
        if (!is_null($steps)) {
            if ($steps < 3000 || $steps > 10000) {
                return 1022;
            } else {
                return 1023;
            }
        } else {
            return null;
        }
    }

    // TODO: Move to custom service
    // TODO: Update questionIds after seeder run
    public function handleSCTMinutesSkipLogic($question) {
        $minutes = $this->getQuestionAnswer($question->id);
        if (!is_null($minutes)) {
            if ($minutes > 150) {
                return 1026;
            } else {
                return 1027;
            }
        } else {
            return null;
        }
    }

    // TODO: Move to custom service
    // TODO: Update questionIds after seeder run
    public function handleGSStepsSkipLogic($question) {
        $steps = $this->getQuestionAnswer($question->id);
        if (!is_null($steps)) {
            if ($steps < 3000 || $steps > 10000) {
                return 1001;
            } else {
                return 1002;
            }
        } else {
            return null;
        }
    }

    // TODO: Move to custom service
    // TODO: Update questionIds after seeder run
    public function handleGSMinutesSkipLogic($question) {
        $minutes = $this->getQuestionAnswer($question->id);
        if (!is_null($minutes)) {
            if ($minutes > 150) {
                return 1010;
            } else {
                return 1011;
            }
        } else {
            return null;
        }
    }

    // TODO: Move to custom service
    // TODO: Update questionIds after seeder run
    public function handleSetStepGoalSkipLogic() {
        // check step goal last GS call
        $priorStepGoal = $this->getPreviousStepGoal();
        if (!is_null($priorStepGoal)) {
            return 1003;
        } else {
            return 1005;
        }
    }

    // TODO: Move to custom service
    // TODO: Update questionIds after seeder run
    public function handleSetNewGSStepGoalSkipLogic($question) {
        $newStepGoal = $this->getQuestionAnswer($question->id);
        if (!is_null($newStepGoal)) {
            if ($newStepGoal < 3000 || $newStepGoal > 10000) {
                return 1006;
            } else {
                return 1007;
            }
        } else {
            return null;
        }
    }

    public function getNewStepGoalResponse() {
        $previousStepGoal = $this->getPreviousStepGoal();
        if (!is_null($previousStepGoal)) {
            $newStepGoal = $previousStepGoal + 500;
            if (intval($newStepGoal) <= 10000) {
                return 'Great! Your new step goal for this week is '.intval($newStepGoal).'. We will check in on your progress in 7 days.';
            } else {
                return 'Your new step goal is over 10,000 steps per day, which is the amount recommended for health benefits.  While more is often better, listen to your body and be careful not to overdo it.';
            }
        } else {
            return null;
        }
    }

    public function getPreviousStepGoal($userId = null) {
        if (is_null($userId))  {
            $userId = Auth::id();
        }

        /** @var Collection<SurveysUsers> $surveyUsers */
        $surveyUsers = SurveysUsers::query()->distinct()->select(['survey_id', 'id'])
            ->where(
                [
                    'user_id' => $userId,
                    'survey_id' => 75,
                    'is_locked' => true
                ]
            )->groupBy(['id'])->orderBy('response_order', 'desc')
            ->get();
        $stepGoalRadioCount = 0;
        if (!is_null($surveyUsers)) {
            foreach ($surveyUsers as $surveyUser) {
                $radioAnswer = Answers::query()
                    ->where([
                        'surveys_users_id'  => $surveyUser->id,
                        'question_id' => 1004,
                        'response' => 1
                    ])->orderBy('created', 'desc')
                    ->first();

                if (!is_null($radioAnswer)) {
                    $stepGoalRadioCount++;
                }

                $stepAnswer = Answers::query()
                    ->where(
                        [
                            'surveys_users_id'  => $surveyUser->id,
                            'question_id' => 1005
                        ]
                    )->orderBy('created', 'desc')->first();
                if (!is_null($stepAnswer)) {
                    if ($stepGoalRadioCount === 0) {
                        return intval($stepAnswer->response);
                    } else {
                        return (intval($stepAnswer->response) + $stepGoalRadioCount * 500);
                    }
                }
            }
        }
        return null;
    }

    public function getLatestStepsWithQuestion($question, $userId = null) {
        if (is_null($userId))  {
            $userId = Auth::id();
        }
        $latestSurveyUser = SurveysUsers::query()
            ->where(
                [
                    'user_id' => $userId
                ]
            )->orderBy('created', 'desc')
            ->first();
        if (!is_null($latestSurveyUser)) {
            if (!is_null($question)) {
                $steps_answer = Answers::query()->where(
                    [
                        'question_id'       => $question->id,
                        'surveys_users_id'  => $latestSurveyUser->id
                    ]
                )->first();
                if (!is_null($steps_answer)) {
                    $steps = $steps_answer->response;
                    if (!is_null($steps)) {
                        if ($steps < 3000 && $steps > 10000) {
                            return 666;
                        } else {
                            return 667;
                        }
                    } else {
                        return null;
                    }
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
        return null;
    }

    public function getTrackingStepsResponsePhrasing() {
    $lastSurveySteps = $this->getSteps();
    $currentSurveySteps = $this->getCurrentSteps();

    if (!is_null($lastSurveySteps)) {
        if ($currentSurveySteps >= 10000) {
            return 'Congratulations on meeting the DIAL Study step goal!  Let’s keep trying to meet this step goal.  Have fun, but always keep your health in mind. Be careful not to overdo it.';
        } elseif ($currentSurveySteps < $lastSurveySteps) {
            return 'That’s fewer steps than you reported last time. Pick up the pace tomorrow. You can do it.';
        } elseif ($currentSurveySteps > $lastSurveySteps) {
            return 'Way to go! That’s more steps than you reported last time!  You are on the right path. Keep adding steps.';
        } else {
            return 'That’s the same number of steps you reported last time. Beat your record by taking a few more steps tomorrow.';
        }
    } else {
        if ($currentSurveySteps >= 10000) {
            return 'Congratulations on meeting the DIAL Study step goal!  Let’s keep trying to meet this step goal.  Have fun, but always keep your health in mind. Be careful not to overdo it.';
        } else {
            return 'Thanks for reporting your steps.  You did not meet the DIAL Study step goal of 10,000 steps per day yet but you are on your way.  Keep making small increases until you get there.';
        }
    }
}

    public function getSCTStepsResponsePhrasing() {
        $lastSurveySteps = $this->getSteps();
        $currentSurveySteps = $this->getCurrentSteps();

        if (!is_null($lastSurveySteps)) {
            if ($currentSurveySteps >= 10000) {
                return 'Congratulations on meeting the DIAL Study step goal!  Keep up the good work.';
            } elseif ($currentSurveySteps < $lastSurveySteps) {
                return 'That’s fewer steps than you reported last time. Pick up the pace tomorrow. You can do it.';
            } elseif ($currentSurveySteps > $lastSurveySteps) {
                return 'Way to go! That’s more steps than you reported last time!  You are on the right path. Keep adding steps.';
            } else {
                return 'That’s the same number of steps you reported last time. Beat your record by taking a few more steps tomorrow.';
            }
        } else {
            if ($currentSurveySteps >= 10000) {
                return 'Congratulations on meeting the DIAL Study step goal!  Keep up the good work.';
            } else {
                return 'Thanks for reporting your steps.  You did not meet the DIAL Study step goal of 10,000 steps per day yet but you are on your way.';
            }
        }
    }

    public function getGSStepGoalResponsePhrasing() {
        $currentSurveyStepGoal = $this->getCurrentStepGoal();

        if (!is_null($currentSurveyStepGoal)) {
            if ($currentSurveyStepGoal > 10000) {
                return 'You entered over 10,000 steps per day which is the amount recommended for health benefits.  While more is often better, listen to your body and be careful not to overdo it.';
            } elseif ($currentSurveyStepGoal < 3000) {
                return '10,000 steps per day are recommended for good health, so this goal is a bit low.  Keep increasing those steps each week!';
            } else {
                return 'Your goal has been noted. We will check in on your progress in 7 days.';
            }
        } else {
            return null;
        }
    }

    // TODO: Jeremy -- Pass question from template service
    public function checkStepResponseForQuestion($question) {
        // TODO: Make dynamic
        $steps = $this->getSteps();

        if ($steps < 3000 || $steps > 10000) {
            if ($question->id === 43) { //tracking
                return $question->id === 44;
            }

            if ($question->id === 67) { //sct
                return $question->id === 68;
            }
        }

        if ($question->id === 43) { //tracking
            return $question->id === 45;
        }

        if ($question->id === 67) { //sct
            return $question->id === 69;
        }
    }

    public function getLastRelevantSurveyQuestionsByKey($key, $userId = null) {
        if (is_null($userId))  {
            //TODO: userId comes from Auth user
            //     $userId = Auth::id();
            $userId = 3437;
        }

        /** @var Collection<SurveysUsers> $surveyUsers */
        $surveyUsers = SurveysUsers::query()
            ->where(
                [
                    'user_id' => $userId
                ]
            )->orderBy('created', 'desc')
            ->get();

        foreach ($surveyUsers as $surveyUser) {
            if ($this->isUserCompletedSurvey($surveyUser)) { // Survey is complete
                /** @var Surveys $survey */
                $survey = $surveyUser->survey();

                /** @var Collection<SurveyPages> $pages */
                $pages = $survey->surveyPages();
                foreach ($pages as $page) {
                    /** @var Collection<Questions> $questions */
                    $questions = $page->questions();
                    $filteredQuestions = $questions->filter((function($question) use ($key) {
                        return $question->placeholder === $key;
                    }));
                    return $filteredQuestions->all();

                }
            }
        }
        return null;
    }

    public function getCurrentSurveyQuestionsByKey($key, $userId = null) {
        if (is_null($userId))  {
            $userId = Auth::id();
        }

        return Questions::query()->where(
            [
                'placeholder'   =>  $key
            ]
        )->get();
    }

    public function getKeyAnswerValues($questions) {
        $answerValues = [];
        foreach ($questions as $question) {
            $questionId = $question->id;
            $survey = $this->getSurvey($questionId);

            if (!is_null($survey)) {
                $surveyUser = $this->getSurveyUser($survey->id);

                if (!is_null($surveyUser)) {
                    $surveyUserId = $surveyUser->id;

                    if (!is_null($surveyUserId)) {
                        $query = Answers::query()
                            ->where(
                                [
                                    'question_id'      => $questionId,
                                    'surveys_users_id' => $surveyUserId
                                ]
                            )->orderBy('created', 'desc');

                        $answer = $query->first();

                        $answerValue = intval($answer->response);
                        array_push($answerValues, $answerValue);
                    } else {
                        return null;
                    }
                } else {
                    return null;
                }
            } else {
                return null;
            }
        }
        return collect($answerValues);
    }

    public function getKeyByAnswerOptionId($answerOptionId) {
        $answerOption = AnswerOptions::query()->where(
            [
                'id' => $answerOptionId
            ]
        )->first();

        if (!is_null($answerOptionId)) {
            $question = Questions::query()->where(
                [
                    'id'   => $answerOption->parent_question_id
                ]
            )->first();

            if (!is_null($question)) {
                return $question->placeholder;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public function getSocialSupportResponse() {
        $userId = Auth::id();

        // TODO: change to response_order
        /** @var SurveysUsers|null $currentSurveyUser */
        $currentSurveyUser = SurveysUsers::query()
            ->where('user_id', '=', $userId)
            ->orderBy('created', 'desc')
            ->first();

        if (is_null($currentSurveyUser)) {
            return 'No survey user.'; // TODO
        }

        $questionIds = Questions::query()
            ->where('survey_page_id', '=', 67)
            ->whereIn('position', [32,33,34]) // TODO: Correct positions
            ->get()
            ->pluck('id');

        $currentSurveyAnswersTotal = Answers::query()
            ->where('surveys_users_id', '=', $currentSurveyUser->id)
            ->whereIn('question_id', $questionIds)
            ->sum('response');

        // TODO: change to response_order
        /** @var SurveysUsers|null $previousSurveyUser */
        $previousSurveyUser = SurveysUsers::query()
            ->where('user_id', '=', $userId)
            ->where('id', '!=', $currentSurveyUser->id)
            ->where('survey_id', '=', $currentSurveyUser->survey_id)
            ->orderBy('created', 'desc')
            ->first();

        if (is_null($previousSurveyUser)) {
            if ($currentSurveyAnswersTotal != 0) {
                // TODO: update for all months
                if ($currentSurveyAnswersTotal >= 6) {
                    return 'You may not have many people supporting your efforts to be physically active right now. As you make exercise a habit, think of ways your family and friends can support you and then talk to them about it.';
                } else {
                    return 'You have people in your life who support your efforts to be physically active. Congratulations! Ask them to remind you to walk. A phone call or text can help you keep on track!';
                }
            } else {
                return 'No data.';
            }
        }

        $previousSurveyAnswersTotal = Answers::query()
            ->where('surveys_users_id', '=', $previousSurveyUser->id)
            ->whereIn('question_id', $questionIds)
            ->sum('response');

        if ($previousSurveyAnswersTotal != 0) {
            //TODO: Based on Month (For now only updating for month 2,6, 10)
            if ($currentSurveyAnswersTotal > $previousSurveyAnswersTotal) {
                if ($currentSurveyAnswersTotal >= 6) {
                    return 'You may not have many people supporting your efforts to be physically active right now, but it’s better than last time we spoke. Consider trying a group exercise class for support.';
                } else {
                    return 'You have people in your life who support your efforts to be physically active, perhaps even more so than last time we spoke. We are so glad. Turn to them when exercise gets challenging and ask them to cheer you on.';
                }
            } elseif ($currentSurveyAnswersTotal === $previousSurveyAnswersTotal) {
                if ($currentSurveyAnswersTotal >= 6) {
                    return 'You do not have many people supporting your efforts to be physically active right now, same as last time we spoke. Consider trying a group exercise class for support.';
                } else {
                    return 'You have people in your life who support your efforts to be physically active, same as last time we spoke. We are so glad. Turn to them when exercise gets challenging and ask them to cheer you on.';
                }
            } else {
                if ($currentSurveyAnswersTotal >= 6) {
                    return 'You may not have many people supporting your efforts to be physically active right now, perhaps even fewer than last time we spoke. Consider trying a group exercise class for support.';
                } else {
                    return 'You have people in your life who support your efforts to be physically active, perhaps less so than last time we spoke though. Turn to your friends and family when exercise gets challenging and ask them to cheer you on.';
                }
            }
        } else {
            return 'Data Missing.'; // TODO
        }
    }

    public function getOutcomeExpectationsResponse() {
        $userId = Auth::id();

        // TODO: change to response_order
        /** @var SurveysUsers|null $currentSurveyUser */
        $currentSurveyUser = SurveysUsers::query()
            ->where('user_id', '=', $userId)
            ->orderBy('created', 'desc')
            ->first();

        if (is_null($currentSurveyUser)) {
            return 'No survey user.'; // TODO
        }

        $questionIds = Questions::query()
            ->where('survey_page_id', '=', 67)
            ->whereIn('position', [21,22,23,24,25,26,27,28,29]) // TODO: Correct positions
            ->get()
            ->pluck('id');

        $currentSurveyAnswersTotal = Answers::query()
            ->where('surveys_users_id', '=', $currentSurveyUser->id)
            ->whereIn('question_id', $questionIds)
            ->sum('response');

        // TODO: change to response_order
        /** @var SurveysUsers|null $previousSurveyUser */
        $previousSurveyUser = SurveysUsers::query()
            ->where('user_id', '=', $userId)
            ->where('id', '!=', $currentSurveyUser->id)
            ->where('survey_id', '=', $currentSurveyUser->survey_id)
            ->orderBy('created', 'desc')
            ->first();

        if (is_null($previousSurveyUser)) {
            if ($currentSurveyAnswersTotal != 0) {
                // TODO: update for all months
                if ($currentSurveyAnswersTotal >= 12) {
                    return 'You don’t expect much good to come from exercising right now but give it a try. Physical activity can improve your mood and leave you feeling refreshed. Keep this in mind as you work on making physical activity a habit.';
                } else {
                    return 'You expect good things to come from being physically active. This is great! Remind yourself that physical activity can improve your fitness and overall health. That will help keep you going when you don’t feel like exercising';
                }
            } else {
                return 'No data.';
            }
        }

        $previousSurveyAnswersTotal = Answers::query()
            ->where('surveys_users_id', '=', $previousSurveyUser->id)
            ->whereIn('question_id', $questionIds)
            ->sum('response');

        if ($previousSurveyAnswersTotal != 0) {
            //TODO: Based on Month (For now only updating for month 2,6, 10)
            if ($currentSurveyAnswersTotal > $previousSurveyAnswersTotal) {
                if ($currentSurveyAnswersTotal >= 12) {
                    return 'You expect more good things to come from exercising today than last time we spoke. Congratulations! However you still don’t expect much good to come from exercising. Talk to friends and family who exercise and find out what kind of benefits they get from it.';
                } else {
                    return 'You expect good things to come from being physically active, even more so than last time we spoke. Wonderful. On rough days- remember that physical activity is a great way to lower stress and beat the blues. This will help you keep moving.';
                }
            } elseif ($currentSurveyAnswersTotal === $previousSurveyAnswersTotal) {
                if ($currentSurveyAnswersTotal >= 12) {
                    return 'You don’t expect much good to come from exercising, same as last time we talked. Talk to friends and family who exercise and find out what kind of benefits they get from it.';
                } else {
                    return 'You expect good things to come from being physically active, same as last time we spoke. Wonderful. On rough days- remember that physical activity is a great way to lower stress and beat the blues. This will help you keep moving.';
                }
            } else {
                if ($currentSurveyAnswersTotal >= 12) {
                    return 'You expect even less good to come from exercising than last time we spoke. And you did not expect much good to come from exercising then. Talk to friends and family who exercise and find out what kind of benefits they get from it.';
                } else {
                    return 'You expect good things to come from being physically active, but perhaps less so than last time we spoke. On rough days- remember that physical activity is a great way to lower stress and beat the blues. This will help you keep moving.';
                }
            }
        } else {
            return 'Data Missing.'; // TODO
        }
    }

    public function getEnjoymentResponse() {
        $userId = Auth::id();

        // TODO: change to response_order
        /** @var SurveysUsers|null $currentSurveyUser */
        $currentSurveyUser = SurveysUsers::query()
            ->where('user_id', '=', $userId)
            ->orderBy('created', 'desc')
            ->first();

        if (is_null($currentSurveyUser)) {
            return 'No survey user.'; // TODO
        }

        $questionIds = Questions::query()
            ->where('survey_page_id', '=', 67)
            ->whereIn('position', [14,15,16,17,18]) // TODO: Correct positions
            ->get()
            ->pluck('id');

        $currentSurveyAnswersAvg = $currentAnswers = Answers::query()
            ->where('surveys_users_id', '=', $currentSurveyUser->id)
            ->whereIn('question_id', $questionIds)
            ->average('response');

        // TODO: change to response_order
        /** @var SurveysUsers|null $previousSurveyUser */
        $previousSurveyUser = SurveysUsers::query()
            ->where('user_id', '=', $userId)
            ->where('id', '!=', $currentSurveyUser->id)
            ->where('survey_id', '=', $currentSurveyUser->survey_id)
            ->orderBy('created', 'desc')
            ->first();

        if (is_null($previousSurveyUser)) {
            if (!is_null($currentSurveyAnswersAvg)) {
                // TODO: update for all months
                if ($currentSurveyAnswersAvg >= 3) {
                    return 'You do not enjoy physical activity that much. Think of ways to make physical activity more fun. Try listening to music while you exercise. The time will fly by.';
                } else {
                    return 'You enjoy physical activity. That’s great! Enjoying physical activity will make it easier to stick with it.';
                }
            } else {
                return 'No data.';
            }
        }

        $previousSurveyAnswersAvg = Answers::query()
            ->where('surveys_users_id', '=', $previousSurveyUser->id)
            ->whereIn('question_id', $questionIds)
            ->average('response');

        if (!is_null($previousSurveyAnswersAvg)) {
            //TODO: Based on Month (For now only updating for month 2,6, 10)
            if ($currentSurveyAnswersAvg > $previousSurveyAnswersAvg) {
                if ($currentSurveyAnswersAvg >= 3) {
                    return 'You enjoy physical activity more today than last time we spoke. Congratulations! But it seems like you still do not enjoy physical activity that much. Take some small steps to make it more pleasant. Turn on the fan. Wear comfortable clothes. Bring extra water.';
                } else {
                    return 'You enjoy physical activity, more so today than the last time we spoke. Wonderful! Try exploring new walking paths. Keep mixing it up so it stays fun!';
                }
            } elseif ($currentSurveyAnswersAvg === $previousSurveyAnswersAvg) {
                if ($currentSurveyAnswersAvg >= 3) {
                    return 'You do not enjoy physical activity that much, same as last time we spoke. Take some small steps to make it more pleasant. Turn on the fan. Wear comfortable clothes. Bring extra water.';
                } else {
                    return 'You enjoy physical activity, same as last time we spoke. Wonderful! Try exploring new walking paths. Keep mixing it up so it stays fun!';
                }
            } else {
                if ($currentSurveyAnswersAvg >= 3) {
                    return 'You do not enjoy physical activity that much and you enjoy it less today than last time we spoke. Take some small steps to make it more pleasant. Turn on the fan. Wear comfortable clothes. Bring extra water.';
                } else {
                    return 'You enjoy physical activity but perhaps a bit less than the last time we spoke. Try exploring new walking paths. Keep mixing it up so it stays fun!';
                }
            }
        } else {
            return 'Data Missing.'; // TODO
        }
    }

    public function getWalkSelfEfficacyResponse() {
        $userId = Auth::id();

        // TODO: change to response_order
        /** @var SurveysUsers|null $currentSurveyUser */
        $currentSurveyUser = SurveysUsers::query()
            ->where('user_id', '=', $userId)
            ->orderBy('created', 'desc')
            ->first();

        if (is_null($currentSurveyUser)) {
            return 'No survey user.'; // TODO
        }

        $questionIds = Questions::query()
            ->where('survey_page_id', '=', 67)
            ->whereIn('position', [9,10,11]) // TODO: Correct positions
            ->get()
            ->pluck('id');

        $currentSurveyAnswersAvg = Answers::query()
            ->where('surveys_users_id', '=', $currentSurveyUser->id)
            ->whereIn('question_id', $questionIds)
            ->average('response');

        // TODO: change to response_order
        /** @var SurveysUsers|null $previousSurveyUser */
        $previousSurveyUser = SurveysUsers::query()
            ->where('user_id', '=', $userId)
            ->where('id', '!=', $currentSurveyUser->id)
            ->where('survey_id', '=', $currentSurveyUser->survey_id)
            ->orderBy('created', 'desc')
            ->first();

        if (is_null($previousSurveyUser)) {
            if (!is_null($currentSurveyAnswersAvg)) {
                // TODO: update for all months
                if ($currentSurveyAnswersAvg >= 75) {
                    return 'You feel sure about your ability to exercise. Fabulous. Pat yourself on the back for joining the Dial Study. That was a big step towards prioritizing your health and becoming more active.';
                } else {
                    return 'You sound unsure about your ability to exercise. We are glad you joined the Dial Study. Keep wearing your pedometer and tracking those steps. Watch the numbers grow along with your confidence. You got this.';
                }
            } else {
                return 'No data.';
            }
        }

        $previousSurveyAnswersAvg = Answers::query()
            ->where('surveys_users_id', '=', $previousSurveyUser->id)
            ->whereIn('question_id', $questionIds)
            ->average('response');

        if (!is_null($previousSurveyAnswersAvg)) {
            //TODO: Based on Month (For now only updating for month 2, 6, 10)
            if ($currentSurveyAnswersAvg > $previousSurveyAnswersAvg) {
                if ($currentSurveyAnswersAvg >= 75) {
                    return 'You feel sure about your ability to exercise, even more so than last time we spoke. This is great! To stay sure about your ability to exercise, remind yourself how far you have already come. You can do this.';
                } else {
                    return 'You do not sound very sure about your ability to exercise, but it’s better than last time we spoke. Try squeezing in a 10 minute walk this week. Meeting this small goal will help you feel more sure that you can fit physical activity into your life.';
                }
            } elseif ($currentSurveyAnswersAvg === $previousSurveyAnswersAvg) {
                if ($currentSurveyAnswersAvg >= 75) {
                    return 'You feel sure about your ability to exercise, just like last time we spoke. This is great! To stay sure about your ability to exercise, remind yourself how far you have already come. You can do this.';
                } else {
                    return 'You sound unsure about your ability to exercise, same as last time we spoke. Try squeezing in a 10 minute walk this week. Meeting this small goal will help you feel more sure that you can fit physical activity into your life.';
                }
            } else {
                if ($currentSurveyAnswersAvg >= 75) {
                    return 'You still feel pretty sure about your ability to exercise, but perhaps less so than last time we spoke. To stay sure about your ability to exercise, remind yourself how far you have already come. You can do this.';
                } else {
                    return 'You sound unsure about your ability to exercise, even more so than last time we spoke. Try squeezing in a 10 minute walk this week. Meeting this small goal will help you feel more sure that you can fit physical activity into your life.';
                }
            }
        } else {
            return 'Data Missing.'; // TODO
        }
    }
}
