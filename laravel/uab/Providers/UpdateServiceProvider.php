<?php
namespace Uab\Providers;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Uab\Http\Models\Base\BaseModel;

class UpdateServiceProvider {
    public $modelService;
    public $queryService;
    public $requestService;

    public function __construct() {
        $this->modelService = new ModelServiceProvider();
        $this->queryService = new QueryServiceProvider();
        $this->requestService = new RequestServiceProvider();
    }

    /**
     * @param BaseModel $parentModel
     * @param string    $column
     * @param mixed     $value
     * @param mixed[]   $array
     *
     * @return int|string|null
     */
    private function convertValue($parentModel, $column, $value, $array = []) {
        if (is_string($value) && preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}$/i', $value)) {
            $value .= ':00';
        } else if (strstr($column, '_id') && !is_numeric($value)) {
            if (array_key_exists('object_table', $array)) {
                $childTable = $array['object_table'];
            } else {
                $fk = explode('_id', $column)[0];

                $childTable = $this->modelService->getTableByForeignKey(
                    $parentModel,
                    $fk
                );
            }

            if ($childTable !== false) {
                $childModel = $this->modelService->getModel($childTable);

                if ($childModel === false && $parentModel->hasRelationship($column)) {
                    $childModel = $parentModel;
                }

                if ($childModel) {
                    $id = $childModel->safeDecode($value);

                    if (!is_null($value)) {
                        $value = $id;
                    }
                }
            }
        }

        return $value;
    }

    /**
     * @param string  $parentTable
     * @param mixed[] $fields
     * @param string  $grandparentTable
     *
     * @return null|mixed[]
     */
    public function create($parentTable, $fields, $grandparentTable = null) {
        $parentModel = $this->modelService->getModel($parentTable, $grandparentTable);

        foreach ($fields as $attribute => $value) {
            if ($parentModel->hasColumn($attribute)) {
                $value = $this->convertValue($parentModel, $attribute, $value);

                $parentModel->setAttribute($attribute, $value);
            }
        }

        $success = $parentModel->save();

        if ($success) {
            $this->updateJoins($parentTable, $parentModel->toArray(), $fields);

            return $this->queryService->parseModelProperties($parentModel->toArray(), $parentTable);
        } else {
            return null;
        }
    }

    public function delete($table, $id) {
        $model = $this->modelService->getModel($table);

        $ids = explode(',', $id);
        foreach ($ids as $i => $id) {
            $ids[$i] = $model->safeDecode($id);
        }

        return $model::query()
            ->whereIn(
                'id', $ids
            )->delete();
    }

    /**
     * @param string      $parentTable
     * @param mixed[]     $fields
     * @param string|null $id
     *
     * @return BaseModel[]|null
     */
    public function edit(string $parentTable, array $fields, ?string $id = null) {
        $parentModel = $this->modelService->getModel($parentTable);

        if (Arr::has($fields, $parentTable)) {
            $requestObjects = $fields[$parentTable];
        } else {
            if (!is_null($id)) {
                $fields['id'] = $id;
            }

            $requestObjects = [$fields];
        }

        $modelObjects = [];

        foreach ($requestObjects as $i => $object) {
            if (is_array($object) && array_key_exists('id', $object)) {
                $id = $parentModel::safeDecodeStatic($object['id']);

                $modelObjects[] = $parentModel::query()->find($id);
            }
        }

        $returnVar = [];
        foreach ($modelObjects as $i => $object) {
            $fillables = $this->getFillables($parentTable, $requestObjects[$i]);
            $object->fill($fillables);
            $success = $object->save();

            if ($success) {
                $this->updateJoins($parentModel, $object, $fields);

                $array = $object->toArray();
                $array['id'] = $object->slug();

                $returnVar[] = $array;
            }
        }

        return $returnVar;
    }

    /**
     * @param  string  $parentTable
     * @param  mixed[] $fields
     *
     * @return mixed|boolean
     */
    public function findOrCreate($parentTable, $fields) {
        $parentModel = $this->modelService->getModel($parentTable);

        $results = false;

        $query = $this->queryService->buildRequestQuery($parentTable, $fields);

        $fillables = $this->getFillables($parentTable, $fields);

        $query = $this->queryService->addWheres($fields, $parentModel, $query, $fillables);

        $exists = $query->exists();

        if ($exists) {
            $results = $this->queryService->parseGet($parentTable, $fields, $query);
            $results['exists'] = true;
        } else {
            $newObject = $this->create($parentTable, $fields);
            $newObject = $this->queryService->slugIds($parentModel, $newObject);

            $results = [
                'exists' => false,
                $parentTable => $newObject
            ];
        }

        return $results;
    }

    /**
     * @param string  $parentTable
     * @param mixed[] $fields
     * @param bool    $array
     *
     * @return array
     */
    public function getFillables($parentTable, $fields) {
        $parentModel = $this->modelService->getModel($parentTable);

        if (Arr::has($fields, 'id')) {
            $id = $parentModel->safeDecode($fields['id']);

            $parentModel = $parentModel::query()->find($id);
        }

        $fillables = $parentModel->getFillable();
        foreach ($fillables as $fillable) {
            if (Arr::has($fields, $fillable)) {
                $value = $fields[$fillable];

                if (!is_array($value)) {
                    $value = $this->convertValue($parentModel, $fillable, $value, $fields);

                    $parentModel->setAttribute($fillable, $value);
                }
            }
        }

        $relationships = $parentModel->getRelationships();
        foreach ($relationships as $childTable => $column) {
            if (Arr::has($fields, $childTable) && !Arr::has($fields, $column)) {
                $object = $fields[$childTable];

                if (Arr::has($object, 'id')) {
                    $id = $object['id'];

                    if ($id !== 0) {
                        $parentModel->setAttribute($column, $id);
                    }
                }
            }
        }

        return $parentModel->getDirty();
    }

    /**
     * @param BaseModel $parentModel
     * @param BaseModel $parentObject
     * @param BaseModel $childModel
     * @param BaseModel $childObject
     *
     * @return array|bool
     */
    private function joinManyToMany($parentModel, $parentObject, $childModel, $childObject) {
        $joinModel = $this->modelService->getManyToManyModel($parentModel, $childModel);

        if ($joinModel) {
            $parentForeignKey = Str::singular($parentModel->getTable()) . '_id';
            $childForeignKey = Str::singular($childModel->getTable()) . '_id';

            $joinModel->$parentForeignKey = $parentObject->id;
            $joinModel->$childForeignKey = $childObject->id;

            $joinObjectId = $joinModel::query()
                ->insertGetId($joinModel->getDirty());

            if ($joinObjectId) {
                $joinObject = $this->queryService->getById($joinObjectId, $joinModel);

                return [
                    $parentModel->getTable() => $parentObject,
                    $childModel->getTable() => $childObject,
                    $joinModel->getTable() => $joinObject
                ];
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * @param BaseModel $oneModel
     * @param BaseModel $oneObject
     * @param BaseModel $manyModel
     * @param BaseModel $manyObject
     *
     * @return array|bool
     */
    private function joinOneToMany($oneModel, $oneObject, $manyModel, $manyObject) {
        $foreignKey = $this->modelService->getForeignKey($oneModel);

        $manyModel->$foreignKey = $oneObject->id;

        if ($manyObject->exists) {
            $manyObject->update();

            $joinObject = $manyObject;
        } else {
            $joinObjectId = $manyModel::query()
                ->insertGetId($manyObject->getDirty());

            $joinObject = $this->queryService->getById($joinObjectId, $manyModel);
        }

        if ($joinObject) {
            return [
                $manyModel->getTable() => $joinObject
            ];
        } else {
            return false;
        }
    }

    /**
     * @param BaseModel $parentModel
     * @param integer   $parentId
     * @param BaseModel $childModel
     * @param string    $childId
     * @param mixed[]   $parentFields
     *
     * @return mixed[]
     */
    public function joinRelationship($parentModel, $parentId, $childModel, $childId, $parentFields = []) {
        $returnVar = [];

        $parentObject = $this->queryService->getById($parentId, $parentModel);

        $childTable = $childModel->getTable();

        $childObjects = [];

        if ($parentObject) {
            if (strtolower($childId) === 'bulk') {
                $childObjects = Arr::get($parentFields, $childTable, []);
            } else if (!is_null($childId)) {
                $childId = $childModel->safeDecode($childId);

                $childObject = $this->queryService->getById($childId, $childModel);

                if (!is_null($childObject)) {
                    $childObjects = [
                        $childObject
                    ];
                }
            }

            foreach ($childObjects as $childObject) {
                $fillables = $this->getFillables($childTable, $childObject);
                $childObject = $childObject->fill($fillables);
                $childObject->save();

                if ($childObject->id) {
                    $childKey = $this->modelService->getForeignKey($parentModel);
                    $parentKey = $this->modelService->getForeignKey($childModel);

                    if ($parentModel->hasColumn($parentKey)) {
                        $returnVar[] = $this->joinOneToMany($childModel, $childObject, $parentModel, $parentObject);
                    } else if ($childModel->hasColumn($childKey)) {
                        $returnVar[] = $this->joinOneToMany($parentModel, $parentObject, $childModel, $childObject);
                    } else {
                        $returnVar[] = $this->joinManyToMany($parentModel, $parentObject, $childModel, $childObject);
                    }
                }
            }
        }

        return $returnVar;
    }

    /**
     * @param string  $parentTable
     * @param mixed[] $parentObject
     * @param mixed[] $fields
     *
     * @return void
     */
    public function updateJoins($parentTable, $parentObject, $fields = []) {
        $parentModel = $this->modelService->getModel($parentTable);

        foreach ($fields as $childTable => $childObject) {
            if (is_array($childObject) && is_string($childTable)) {
                $childModel = $this->modelService->getTableByForeignKey($parentModel, $childTable);

                if (!$childModel) {
                    continue;
                }

                $parentId = Arr::get($parentObject, 'id');
                $parentId = $childModel::safeDecodeStatic($parentId);

                $isOneToMany = array_key_exists('id', $childObject);

                if ($isOneToMany) {
                    $childObject['id'] = $childModel::safeDecodeStatic($childObject['id']);

                    $this->joinRelationship($parentModel, $parentId, $childModel, $childObject['id']);
                } else {
                    $childKey = $this->modelService->getForeignKey($parentModel);

                    if ($childModel->hasColumn($childKey)) {
                        $joinModel = $childModel;
                    } else {
                        $joinModel = $this->modelService->getManyToManyModel($parentModel, $childModel);
                    }

                    if ($joinModel) {
                        $childIds = [];

                        foreach ($childObject as $aChildObject) {
                            if (is_array($aChildObject) && array_key_exists('id', $aChildObject)) {
                                $childIds[] = $childModel->safeDecode($aChildObject['id']);
                            }
                        }

                        $joinModel::query()
                            ->where(
                                [
                                    $childKey => $parentId
                                ]
                            )->whereNotIn('id', $childIds)
                            ->delete();
                    }

                    foreach ($childObject as $aChildObject) {
                        if (is_array($aChildObject)) {
                            if (array_key_exists('id', $aChildObject)) {
                                $childId = $childModel->safeDecode($aChildObject['id']);
                                $childModel = $childModel::query()->find($childId);

                                $fillables = $this->getFillables($childModel, $aChildObject);
                                $childModel->fill($fillables);
                                $childModel->save();

//                            $this->joinRelationship($parentModel, $parentId, $childModel, $childModel->id);
                            } else {
                                $aChildObject = $this->create($childTable, $aChildObject, $parentTable);

                                if ($aChildObject) {
                                    $this->joinRelationship($parentModel, $parentId, $childModel, $aChildObject['id'], $fields);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
