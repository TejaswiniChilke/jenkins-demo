<?php
namespace Uab\Providers;

use GuzzleHttp\Client;

class LoremIpsumServiceProvider {
    private $client;
    private $sentences;

    public function __construct() {
        $this->client = new Client();
        $this->sentences = [];
    }

    public function get($sentenceNum = 5) {
        if (!array_key_exists($sentenceNum, $this->sentences)) {
            $res = $this->client->get(
                'https://baconipsum.com/api/?type=all-meat&sentences='.$sentenceNum.'&start-with-lorem=1'
            );

            /** @var string[] $sentences */
            $sentences = json_decode($res->getBody()->getContents());

            $this->sentences[$sentenceNum] = implode(' ', $sentences);
        }

        return $this->sentences[$sentenceNum];
    }
}
