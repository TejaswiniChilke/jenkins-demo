<?php
namespace Uab\Providers;

use Carbon\Carbon;
use Exception;
use Twilio\Rest\Api\V2010\Account\MessageInstance;
use Twilio\Rest\Client;
use Uab\Http\Enums\PhoneDirectionsEnum;
use Uab\Http\Models\TextMessages;
use Uab\Http\Models\TextMessageStops;
use Uab\Http\Models\Users;

class TextMessageServiceProvider {
    /**
     * @param string $number
     *
     * @return bool
     */
    public function hasStopped(string $number):bool {
        $query = TextMessageStops::query()
            ->where(
                [
                    'from_number' => $number
                ]
            );

        return $query->exists();
    }
    /**
     * @param TextMessages|null    $textMessage
     * @param MessageInstance|null $message
     *
     * @return TextMessages
     */
    private function convertToTextMessages(?TextMessages $textMessage = null, ?MessageInstance $message = null):TextMessages {
        if (is_null($textMessage)) {
            $textMessage = new TextMessages();
        }

        if (!is_null($message)) {
            $textMessage->date_created = $message->dateCreated;
            $textMessage->date_sent = $message->dateSent;
            $textMessage->date_updated = $message->dateUpdated;
            $textMessage->direction = $message->direction;
            $textMessage->error_code = $message->errorCode;
            $textMessage->error_message = $message->errorMessage;
            $textMessage->from_number = $message->from;
            $textMessage->message = $message->body;
            $textMessage->num_media = $message->numMedia;
            $textMessage->num_segments = $message->numSegments;
            $textMessage->price = $message->price;
            $textMessage->price_unit = $message->priceUnit;
            $textMessage->sid = $message->sid;
            $textMessage->to_number = $message->to;
            $textMessage->uri = $message->uri;
        }

        return $textMessage;
    }

    /**
     * @param TextMessages $textMessage
     *
     * @return TextMessages|boolean
     */
    public function receiveSms(TextMessages $textMessage):TextMessages {
        return $textMessage;
    }

    /**
     * @param TextMessages $textMessage
     *
     * @return TextMessages|null
     */
    public function sendSms(TextMessages $textMessage):?TextMessages {
        $phoneService = new PhoneServiceProvider();

        $textMessage->direction = PhoneDirectionsEnum::OUTBOUND_API;

        $toNumber = null;
        if (isset($textMessage->to_number)) {
            $toNumber = $textMessage->to_number;
        } else if (isset($textMessage->to_user_id)) {
            /** @var Users $toUser */
            $toUser = Users::query()->find($textMessage->to_user_id);

            if (!is_null($toUser)) {
                $toNumber = $toUser->mobile_number;
            }
        }

        $toNumber = $phoneService->formatNumber($toNumber);

        if (is_null($toNumber)) {
            return null;
        } else if (!$phoneService->isValid($toNumber)) {
            return null;
        } else {
            $hasStopped = TextMessageStops::query()
                ->where('from_number', '=', $toNumber)
                ->exists();

            if ($hasStopped) {
                return null;
            }
        }

        $textMessage->to_number = $toNumber;

        if (is_null($textMessage->from_number)) {
            $textMessage->from_number = config('twilio.phone_number');
        }

        $message = null;
        try {
            $textMessage->save();

            $client = new Client(
                config('twilio.account.sid'),
                config('twilio.auth.token')
            );

            $message = $client->messages->create(
                $textMessage->to_number,
                [
                    'from'           => $textMessage->from_number,
                    'body'           => $textMessage->message,
                    'statusCallback' => config('app.backend') . '/open/text_messages/status/'.$textMessage->slug()
                ]
            );
        } catch (Exception $e) {
            $textMessage->error_code = $e->getCode();
            $textMessage->error_message = $e->getMessage();
        }

        $textMessage = $this->convertToTextMessages($textMessage, $message);
        $success = $textMessage->save();

        return $success ? $textMessage : null;
    }

    /**
     * @param string $word
     *
     * @return bool
     */
    public function isStart(string $word):bool {
        $word = strtolower($word);

        $startWords = ['start','unstop'];

        foreach ($startWords as $startWord) {
            if (strpos($word, $startWord) !== false) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param string $word
     *
     * @return bool
     */
    public function isStop(string $word):bool {
        $word = strtolower($word);

        $stopWords = ['stop','cancel','unsubscribe'];

        foreach ($stopWords as $stopWord) {
            if (strpos($word, $stopWord) !== false) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param Users $user
     * @param Carbon|null $since
     *
     * @return boolean
     */
    public function hasTexted(Users $user, $since = null):bool {
        if (is_null($since)) {
            $since = Carbon::now()->startOfDay();
        }

        return TextMessages::query()
            ->where('created', '>', $since)
            ->where(
                function($query) use ($user) {
                    $query->where('to_number', '=', $user->mobile_number)
                        ->orWhere('to_user_id', '=', $user->id);
                }
            )->exists();
    }
}
