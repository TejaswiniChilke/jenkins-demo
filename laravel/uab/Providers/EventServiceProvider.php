<?php
namespace Uab\Providers;

use Uab\Events\AfterCreate\AfterCreate;
use Uab\Events\AfterCreate\AnalyticArticlesAfterCreate;
use Uab\Events\AfterCreate\AnalyticVideosAfterCreate;
use Uab\Events\AfterCreate\ArticlesUsersAfterCreate;
use Uab\Events\AfterCreate\BadgeConditionsUsersAfterCreate;
use Uab\Events\AfterCreate\BadgesUsersAfterCreate;
use Uab\Events\AfterCreate\BugReportsAfterCreate;
use Uab\Events\AfterCreate\CheckInsAfterCreate;
use Uab\Events\AfterCreate\CommentsAfterCreate;
use Uab\Events\AfterCreate\ContactStaffMessagesAfterCreate;
use Uab\Events\AfterCreate\EventsAfterCreate;
use Uab\Events\AfterCreate\EventsUsersAfterCreate;
use Uab\Events\AfterCreate\FavoriteVideoAfterCreate;
use Uab\Events\AfterCreate\FollowersAfterCreate;
use Uab\Events\AfterCreate\LoginAttemptsAfterCreate;
use Uab\Events\AfterCreate\NotificationsAfterCreate;
use Uab\Events\AfterCreate\PostLikesAfterCreate;
use Uab\Events\AfterCreate\PostsAfterCreate;
use Uab\Events\AfterCreate\ReportedCommentsAfterCreate;
use Uab\Events\AfterCreate\ReportedPostsAfterCreate;
use Uab\Events\AfterCreate\UsersAfterCreate;
use Uab\Events\AfterCreate\VideoLikesAfterCreate;
use Uab\Events\AfterEdit\AfterEdit;
use Uab\Events\AfterEdit\NotificationsAfterEdit;
use Uab\Events\AfterEdit\UsersAfterEdit;
use Uab\Events\Excel\ExportQuery;
use Uab\Listeners\After\AfterCreateListener;
use Uab\Listeners\After\AfterEditListener;
use Uab\Listeners\BadgeCheck\ArticleRead;
use Uab\Listeners\BadgeCheck\AwardBadges;
use Uab\Listeners\BadgeCheck\CommentCreated;
use Uab\Listeners\BadgeCheck\EventCreated;
use Uab\Listeners\BadgeCheck\FavoriteVideoCreated;
use Uab\Listeners\BadgeCheck\LoginAttempted;
use Uab\Listeners\BadgeCheck\PlaylistWatched;
use Uab\Listeners\BadgeCheck\PostCreated;
use Uab\Listeners\BadgeCheck\PostLiked;
use Uab\Listeners\BadgeCheck\VideoWatched;
use Uab\Listeners\Email\ApplicationAcceptedEmail;
use Uab\Listeners\Email\ApplicationDeniedEmail;
use Uab\Listeners\Excel\ExportQueryListener;
use Uab\Listeners\Notifications\CheckCommentForBlacklisted;
use Uab\Listeners\Notifications\CheckPostForBlacklisted;
use Uab\Listeners\Notifications\NotificationAfterEditCount;
use Uab\Listeners\Users\UserStatusChanged;
use Uab\Listeners\Events\EventsSendListener;
use Uab\Listeners\Notifications\NotificationAfterCreateCount;
use Uab\Listeners\Notifications\ArticlesUsersNotification;
use Uab\Listeners\Notifications\BadgesUsersEarnedBadge;
use Uab\Listeners\Notifications\BadgesUsersNotification;
use Uab\Listeners\Notifications\BugReportsNotification;
use Uab\Listeners\Notifications\CommentsNotification;
use Uab\Listeners\Notifications\ContactStaffMessagesNotification;
use Uab\Listeners\Notifications\EventsUsersNotification;
use Uab\Listeners\Notifications\FollowersNotification;
use Uab\Listeners\Notifications\NotificationSendListener;
use Uab\Listeners\Notifications\PostLikesNotification;
use Uab\Listeners\Notifications\PostsNotification;
use Uab\Listeners\Notifications\ReportedCommentsNotification;
use Uab\Listeners\Notifications\ReportedPostsNotification;
use Uab\Listeners\Notifications\UsersNotification;
use Uab\Listeners\Posts\CreateCheckInPost;
use Uab\Listeners\Posts\CreateVideoLikePost;
use Uab\Listeners\Slack\BugReportsSlack;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider {
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        AnalyticArticlesAfterCreate::class => [
            ArticleRead::class
        ],
        AnalyticVideosAfterCreate::class => [
            VideoWatched::class,
            PlaylistWatched::class,
        ],
        AfterCreate::class => [
            AfterCreateListener::class,
        ],
        AfterEdit::class => [
            AfterEditListener::class,
        ],
        ArticlesUsersAfterCreate::class => [
            ArticlesUsersNotification::class,
        ],
        BadgeConditionsUsersAfterCreate::class => [
            AwardBadges::class,
        ],
        BadgesUsersAfterCreate::class => [
            BadgesUsersNotification::class,
            BadgesUsersEarnedBadge::class,
        ],
        BugReportsAfterCreate::class => [
            BugReportsNotification::class,
            BugReportsSlack::class,
        ],
        CheckInsAfterCreate::class => [
            CreateCheckInPost::class,
        ],
        CommentsAfterCreate::class => [
            CheckCommentForBlacklisted::class,
            CommentCreated::class,
            CommentsNotification::class,
        ],
        ContactStaffMessagesAfterCreate::class => [
            ContactStaffMessagesNotification::class,
        ],
        EventsAfterCreate::class => [
            EventCreated::class,
            EventsSendListener::class,
        ],
        EventsUsersAfterCreate::class => [
            EventsUsersNotification::class,
        ],
        ExportQuery::class => [
            ExportQueryListener::class,
        ],
        FavoriteVideoAfterCreate::class => [
            FavoriteVideoCreated::class
        ],
        FollowersAfterCreate::class => [
            FollowersNotification::class,
        ],
        LoginAttemptsAfterCreate::class => [
            LoginAttempted::class
        ],
        NotificationsAfterCreate::class => [
            NotificationAfterCreateCount::class,
            NotificationSendListener::class,
        ],
        NotificationsAfterEdit::class => [
            NotificationAfterEditCount::class,
        ],
        PostLikesAfterCreate::class => [
            PostLiked::class,
            PostLikesNotification::class,
        ],
        PostsAfterCreate::class => [
            CheckPostForBlacklisted::class,
            PostCreated::class,
            PostsNotification::class,
        ],
        ReportedCommentsAfterCreate::class => [
            ReportedCommentsNotification::class,
        ],
        ReportedPostsAfterCreate::class => [
            ReportedPostsNotification::class,
        ],
        UsersAfterCreate::class => [
            UsersNotification::class,
            UserStatusChanged::class
        ],
        UsersAfterEdit::class => [
            ApplicationAcceptedEmail::class,
            ApplicationDeniedEmail::class,
            UserStatusChanged::class
        ],
        VideoLikesAfterCreate::class => [
            CreateVideoLikePost::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot() {
        parent::boot();
    }
}
