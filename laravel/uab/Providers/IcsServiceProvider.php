<?php
namespace Uab\Providers;

use DateTime;
use Exception;
use Illuminate\Support\Facades\File;
use Uab\Http\Models\Events;
use Calendar\Element\Calendar;

class IcsServiceProvider {
    /**
     * @param Events[] $events
     *
     * @return string|null
     *
     * @throws Exception
     */
    public function getIcs($events) {
        $calender = (new Calendar())
            ->setName('Exercise Plan')
            ->setVersion('2.0');

        foreach ($events as $event) {
            $newEvent = $calender->newEvent();

            $newEvent->setDtStart(new DateTime($event->start))
                ->setDtEnd(new DateTime($event->end))
                ->setSummary($event->title)
                ->setUid($event->slug());

            if (!is_null($event->description)) {
                $newEvent->setDescription($event->description);
            }

            if ($event->allDay) {
                $newEvent->setWholeDay();
            }
        }

        $ics = $calender->serve();

        $path = storage_path('app/public/ics/'.rand().'.ics');

        File::put($path, $ics);

        if (File::exists($path)) {
            return $path;
        }

        return null;
    }
}
