<?php

namespace Uab\Providers;

use Illuminate\Support\ServiceProvider;
use Form;

class HtmlFormServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // for custom html or form elements
        Form::component('dateTimePicker', 'components.form.dateTimePicker',
            ['params']);
        Form::component('autoComplete', 'components.form.autoComplete',
            ['params']);
        Form::component('toggleButton', 'components.form.toggleButton',
            ['params']);
        Form::component('datePicker', 'components.form.datePicker',
            ['params']);
        Form::component('timePicker', 'components.form.timePicker',
            ['params']);
        Form::component('formatPhoneNumber', 'components.form.formatPhoneNumber',
            ['params']);
        Form::component('radioGroupHorizontal', 'components.form.radioGroupHorizontal',
            ['groupDisplayLabel', 'name', 'items', 'attributes']);
        Form::component('checkboxGroupHorizontal', 'components.form.checkboxGroupHorizontal',
            ['groupDisplayLabel', 'name', 'items', 'attributes']);
        Form::component('textWithError', 'components.form.textWithError',
            ['labeltext', 'name', 'value', 'attributes']);
        Form::component('textAreaWithError', 'components.form.textAreaWithError',
            ['labeltext', 'name', 'value', 'attributes']);
    }
}
