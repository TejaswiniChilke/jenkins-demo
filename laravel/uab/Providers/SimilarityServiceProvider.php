<?php
namespace Uab\Providers;

class SimilarityServiceProvider {
    /** @var ModelServiceProvider $modelService */
    public $modelService;

    public function __construct() {
        $this->modelService = new ModelServiceProvider();
    }

    /**
     * @param string $table
     * @param number $notId
     * @param string $column
     * @param mixed  $value
     *
     * @return array
     */
    private function getModelsWithSameColumnValue($table, $notId, $column, $value) {
        $model = $this->modelService->getModel($table);

        $results = [];

        if (!is_null($value) && strlen($value) > 0) {
            if ($model->hasColumn($column)) {
                $results = $model::query()
                    ->select(
                        [
                            'id',
                            'email',
                            $column
                        ]
                    )->where([
                        [
                            'id', '!=', $model->safeDecode($notId)
                        ],
                        [
                            $column, '=', $value
                        ]
                    ])->get()
                    ->toArray();
            }
        }

        return $results;
    }

    /**
     * @param string  $table
     * @param mixed[] $objectArray
     * @param int     $minimum
     *
     * @return mixed[]
     */
    public function getSimilar($table, $objectArray, $minimum = 0) {
        $model = $this->modelService->getModel($table);

        $similars = [];

        $similarWeights = $model->getSimilarWeights();

        if (array_key_exists('id', $objectArray)) {
            foreach ($similarWeights as $column => $weight) {
                if (array_key_exists($column, $objectArray)) {
                    $value = $objectArray[$column];

                    $matchingModels = $this->getModelsWithSameColumnValue($table, $objectArray['id'], $column, $value);

                    foreach ($matchingModels as $dupe) {
                        $slug = $dupe->id;

                        if (!array_key_exists($slug, $similars)) {
                            $similars[$slug] = [
                                'id'       => $slug,
                                'email'    => $dupe->email,
                                'matching' => [],
                                'score'    => 0
                            ];
                        }

                        $similars[$slug]['matching'][$column] = $value;

                        $similars[$slug]['score'] += $weight * 100;
                    }
                }
            }
        }

        $results = [];

        foreach ($similars as $slug => $similar) {
            if ($similar['score'] < $minimum) {
                unset($similars[$slug]);
            } else {
                $results[] = $similars[$slug];
            }
        }

        usort(
            $results,
            function($a, $b) {
                if ($a['score'] > $b['score']) {
                    return -1;
                } else if ($a['score'] < $b['score']) {
                    return 1;
                }

                return 0;
            }
        );

        return $results;
    }
}
