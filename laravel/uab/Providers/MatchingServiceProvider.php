<?php
namespace Uab\Providers;

use Cache;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Uab\Exceptions\UserNotFound;
use Uab\Http\Enums\HttpResponseCodesEnum;
use Uab\Http\Models\Tags;
use Uab\Http\Models\Users;

class MatchingServiceProvider {
    /** @var ModelServiceProvider $modelService */
    private $modelService;

    /** @var QueryServiceProvider $queryService */
    private $queryService;

    public function __construct() {
        $this->modelService = new ModelServiceProvider();
        $this->queryService = new QueryServiceProvider();
    }

    /**
     * @param string  $table
     * @param mixed[] $fields
     * @param string  $id
     *
     * @return mixed[]
     */
    public function getMatching($table, $fields, $id) {
        $query = $this->getMatchingQuery($table, $fields, $id);

        $results = $this->queryService->parseGet($table, $fields, $query);
        $results = $this->queryService->slugIds($table, $results);
        $results = $this->queryService->filterVirtualFields($table, $fields, $results);

        return $results;
    }

    /**
     * @param string  $alphaTable
     * @param mixed[] $fields
     * @param string  $id
     *
     * @return Builder
     */
    public function getMatchingQuery($alphaTable, $fields, $id) {
        $manyModel = $this->modelService->getManyToManyModel($alphaTable, 'tags');

        $tags = $this->getMatchingTags($alphaTable, $id);

        $ids = $tags->pluck('id')->toArray();

        $manyAlias = $this->modelService->getAlias($manyModel);
        $manyTable = $manyModel->getTable();

//        $inclusiveIds = $tags->where('inclusive', '=', 1)->pluck('id')->toArray();

        $column = Str::singular($alphaTable).'_id';

        $query = $this->queryService->buildRequestQuery($alphaTable, $fields);

        $query->select(
            $this->modelService->getFields(
                $this->modelService->getModel($alphaTable)
            )
        );

        $query->join(
            $manyTable.' AS '.$manyAlias,
            $manyAlias.'.'.$column,
            '=',
            $this->modelService->getAlias($alphaTable).'.id'
        );

        $query->whereIn('tag_id', $ids);

//        $query->where('exclusive', '=', 1);

//        $exclusiveIds = $tags->where('inclusive', '=', 0)->pluck('id')->toArray();

//        $query->where('exclusive', '!=', 0);

//        $query->whereIn('tag_id', $ids);

        return $query;
    }

    public function getMatchingTags($alphaTable, $id = null) {
        $tags = collect();

        $alphaModel = $this->modelService->getModel($alphaTable);

        $manyModel = $this->modelService->getManyToManyModel($alphaModel, 'tags');

        if ($manyModel) {
            $manyTable = $manyModel->getTable();

            if (is_null($id)) {
                $activeUser = Auth::user();

                if (!is_null($activeUser)) {
                    $id = $activeUser->id;
                }
            }

            if (!is_null($id)) {
                $tags = Cache::remember(
                    'matching-service.'.$alphaModel.'.'.$manyTable.'.'.$id,
                    0,
                    function() use ($id) {
                        return $this->getUserTags($id);
                    }
                );
            }
        }

        return $tags;
    }

    /**
     * @param $userId
     *
     * @return Collection
     */
    public function getUserTags($userId) {
        $userId = Users::safeDecodeStatic($userId);

        $tags = collect();

        $userTagsModel = $this->modelService->getManyToManyModel('users', 'tags');

        if ($userTagsModel) {
            $userTags = Tags::query()
            ->select(
                'tags.*'
            )->rightJoin(
                $userTagsModel->getTable(),
                'tags_users.tag_id',
                '=',
                'tags.id'
            )->where(
                [
                    'tags_users.user_id' => $userId
                ]
            )->get();

            $tags = $tags->merge($userTags);
        }

        $userTypeTagsModel = $this->modelService->getManyToManyModel('user_types', 'tags');

        if ($userTypeTagsModel) {
            /** @var Users|null $user */
            $user = Users::query()->find($userId);

            if (is_null($user)) {
                throw new UserNotFound('Could not find user by id ('.$userId.').',
                    HttpResponseCodesEnum::CLIENT_ERROR_BAD_REQUEST
                );
            } else {
                $query = Tags::query()
                    ->select(
                        'tags.*'
                    )->rightJoin(
                        $userTypeTagsModel->getTable(),
                        'tags_user_types.tag_id',
                        '=',
                        'tags.id'
                    )->where(
                        [
                            'tags_user_types.user_type_id' => $user->user_type_id
                        ]
                    );

                $userTypeTags = $query->get();

                $tags = $tags->merge($userTypeTags);
            }
        }

        $answerOptionsTagsModel = $this->modelService->getManyToManyModel('answer_options', 'tags');
        if ($answerOptionsTagsModel) {
            $surveyTagQuery = Tags::query()
                ->select(
                    'tags.*'
                )->rightJoin(
                    $answerOptionsTagsModel->getTable(),
                    'answer_options_tags.tag_id',
                    '=',
                    'tags.id'
                )->rightJoin(
                    'answer_options',
                    'answer_options_tags.answer_option_id',
                    '=',
                    'answer_options.id'
                )->rightJoin(
                    'answers',
                    'answers.answer_option_id',
                    '=',
                    'answer_options.id'
                )->rightJoin(
                    'surveys_users',
                    'surveys_users.id',
                    '=',
                    'answers.surveys_users_id'
                )->where(
                    [
                        'surveys_users.user_id' => $userId
                    ]
                )->whereNull(
                    [
                        'answers.deleted_at'
                    ]
                )->whereNotNull(
                    'answers.id'
                )->whereNotNull(
                    'tags.id'
                )->whereNotNull(
                    'answers.answer_option_id'
                );

            $surveyTags = $surveyTagQuery->get();

            $tags = $tags->merge($surveyTags);
        }

        return $tags;
    }
}
