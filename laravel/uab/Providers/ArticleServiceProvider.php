<?php
namespace Uab\Providers;

use Carbon\Carbon;
use Uab\Http\Models\AnalyticArticles;
use Uab\Http\Models\Articles;
use Uab\Http\Models\Users;

class ArticleServiceProvider {
    /**
     * @param string|int $userId
     * @param string|int $articleId
     * @param Carbon|string|null $start
     * @param Carbon|string|null $end
     *
     * @return bool
     */
    public function hasReadArticle($userId, $articleId, $start = null, $end = null):bool {
        $articleId = Articles::safeDecodeStatic($articleId);
        $userId = Users::safeDecodeStatic($userId);

        $query = AnalyticArticles::query()
            ->where(
                [
                    'analytic_articles.article_id' => $articleId,
                    'analytic_articles.user_id'  => $userId
                ]
            );

        if (!is_null($start)) {
            $query->where('read_on', '>=', $start);
        }

        if (!is_null($end)) {
            $query->where('read_on', '<=', $end);
        }

        return $query->exists();
    }
}
