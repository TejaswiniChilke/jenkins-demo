<?php
namespace Uab\Providers;

use Illuminate\Support\Arr;

class RequestServiceProvider {
    public $modelService;

    public function __construct() {
        $this->modelService = new ModelServiceProvider();
    }

    /**
     * @param mixed[]     $fields
     * @param string|null $id
     *
     * @return int|string|null
     */
    public function getId($fields, $id = null) {
        if (is_null($id)) {
            $slug = Arr::get($fields, 'id', null);

            if (!is_null($slug)) {
                $id = $this->modelService->getIdFromSlug($slug);
            }
        }

        return $id;
    }

    /**
     * @param mixed[]      $fields
     * @param integer|null $backup
     *
     * @return null|int
     */
    public function getPage($fields, $backup = null) {
        $page = Arr::get($fields, 'page', $backup);

        if (!is_null($page)) {
            $page = intval($page);
        }

        return $page;
    }
}
