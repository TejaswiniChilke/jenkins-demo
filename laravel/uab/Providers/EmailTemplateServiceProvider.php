<?php
namespace Uab\Providers;

use Uab\Http\Models\EmailTemplateLogs;
use Uab\Http\Models\EmailTemplates;
use Uab\Http\Models\Users;

class EmailTemplateServiceProvider {
    /**
     * @param EmailTemplates $template
     * @param Users          $user
     *
     * @return  boolean
     */
    public function hasSentTemplate($template, $user) {
        return EmailTemplateLogs::query()->where(
            [
                'template_id' => $template->id,
                'user_id'     => $user->id
            ]
        )->exists();
    }
}
