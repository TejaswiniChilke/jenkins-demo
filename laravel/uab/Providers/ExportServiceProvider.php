<?php
namespace Uab\Providers;

use DB;
use Exception;
use Illuminate\Support\Collection;
use Uab\Http\Enums\ExportFormatsEnum;
use Uab\Http\Enums\QueueTypesEnum;
use Uab\Http\Models\EmailAttachments;
use Uab\Http\Models\Emails;
use Uab\Http\Models\EmailsEmailAttachments;
use Uab\Jobs\Emails\SendEmail;
use Uab\Listeners\Excel\CollectionExport;

class ExportServiceProvider {
    /**
     * @param Collection $collection
     * @param string $format
     *
     * @return string|null
     */
    public function toExcel(Collection $collection, $format = ExportFormatsEnum::XLS) {
        $export = new CollectionExport($collection);

        $path = 'excel/'.uniqid().'.'.$format;

        $success = $export->store($path);
        if ($success) {
            return $path;
        }

        return null;
    }

    /**
     * @param string $emailAddress
     * @param string $path
     */
    public function emailExport(string $emailAddress, string $path) {
        DB::transaction(
            function() use ($emailAddress, $path) {
                $email = new Emails();
                $email->to_email = $emailAddress;
                $email->message = 'Attached is the export you requested.';
                $email->subject = 'Requested Export';

                $success = $email->save();

                if ($success) {
                    $attachment = new EmailAttachments();
                    $attachment->path = $path;
                    $success = $attachment->save();

                    if ($success) {
                        $manyToMany = new EmailsEmailAttachments();
                        $manyToMany->email_attachment_id = $attachment->id;
                        $manyToMany->email_id = $email->id;
                        $success = $manyToMany->save();

                        if ($success) {
                            SendEmail::dispatch($email, $attachment)->onQueue(QueueTypesEnum::EMAIL);
                        } else {
                            $attachment->delete();
                            $email->delete();

                            throw new Exception(
                                'Unable to create the email\'s email attachment for the export'
                            );
                        }
                    } else {
                        $email->delete();

                        throw new Exception(
                            'Unable to create the email attachment for the export'
                        );
                    }
                } else {
                    throw new Exception(
                        'Unable to create the email for the export'
                    );
                }
            }
        );
    }
}
