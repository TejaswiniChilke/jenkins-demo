<?php
namespace Uab\Providers;

use Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider {
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'Uab\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot() {
        Route::pattern('id', '.*');
        Route::pattern('model', '^([a-zA-Z_])*s$');

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *ionic-app-scripts.js
     * @return void
     */
    public function map() {
        $this->mapApiRoutes();
        $this->mapMessagingRoutes();
        $this->mapOAuthRoutes();
        $this->mapOnboardingRoutes();
        $this->mapOpenRoutes();
        $this->mapSocketRoutes();
        // $this->mapTestRoutes();
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes() {
        Route::prefix('api')
            //->middleware('auth:api')
            ->namespace($this->namespace)
            ->group(base_path('routes/api.php'));
    }

    protected function mapMessagingRoutes() {
        Route::prefix('messaging')
            ->middleware('auth:api')
            ->namespace($this->namespace)
            ->group(base_path('routes/messaging.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapOAuthRoutes() {
        Route::prefix('oauth')
            ->namespace($this->namespace)
            ->group(base_path('routes/oauth.php'));
    }

    protected function mapOnboardingRoutes() {
        Route::prefix('onboarding')
            ->namespace($this->namespace)
            ->group(base_path('routes/onboarding.php'));
    }

    protected function mapOpenRoutes() {
        Route::prefix('open')
            ->namespace($this->namespace)
            ->group(base_path('routes/open.php'));
    }

    /**
     * Define the "socket" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapSocketRoutes() {
        Route::prefix('socket')
            ->namespace($this->namespace)
            ->group(base_path('routes/socket.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapTestRoutes() {
        Route::prefix('test')
            ->namespace($this->namespace)
            ->group(base_path('routes/test.php'));
    }
}
