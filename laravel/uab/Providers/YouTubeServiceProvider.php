<?php
namespace Uab\Providers;

use DateInterval;
use DateTime;
use \Exception;
use Uab\Http\Enums\QueueTypesEnum;
use Uab\Http\Models\Videos;
use Uab\Http\Models\YoutubeVideoInfos;
use Uab\Jobs\Base\InsertObjectJob;
use Uab\Jobs\Base\UpdateObjectJob;
use YouTube;

class YouTubeServiceProvider {
    private function convertToModel($stdVideo) {
        $model = new YoutubeVideoInfos();

        $model->youtube_id = $stdVideo->id;
        $model->kind = $stdVideo->kind;
        $model->etag = $stdVideo->etag;

        if (isset($stdVideo->snippet)) {
            $model->published_at = $stdVideo->snippet->publishedAt;
            $model->channel_id = $stdVideo->snippet->channelId;
            $model->title = $stdVideo->snippet->title;
            $model->description = $stdVideo->snippet->description;
            $model->channel_title = $stdVideo->snippet->channelTitle;

            if (isset($stdVideo->snippet->tags) && is_array($stdVideo->snippet->tags)) {
                $model->tags = implode(',', $stdVideo->snippet->tags);
            }

            $model->youtube_category_id = $stdVideo->snippet->categoryId;
            $model->live_broadcast_content  = $stdVideo->snippet->liveBroadcastContent;

            if (isset($stdVideo->snippet->thumbnails)) {
                if (isset($stdVideo->snippet->thumbnails->default)) {
                    $model->thumbnail_default = $stdVideo->snippet->thumbnails->default->url;
                }

                if (isset($stdVideo->snippet->thumbnails->medium)) {
                    $model->thumbnail_medium = $stdVideo->snippet->thumbnails->medium->url;
                }

                if (isset($stdVideo->snippet->thumbnails->high)) {
                    $model->thumbnail_high = $stdVideo->snippet->thumbnails->high->url;
                }

                if (isset($stdVideo->snippet->thumbnails->standard)) {
                    $model->thumbnail_standard = $stdVideo->snippet->thumbnails->standard->url;
                }

                if (isset($stdVideo->snippet->thumbnails->maxres)) {
                    $model->thumbnail_max = $stdVideo->snippet->thumbnails->maxres->url;
                }
            }

            if (isset($stdVideo->snippet->localized)) {
                $model->localized_title = $stdVideo->snippet->localized->title;
                $model->localized_description = $stdVideo->snippet->localized->description;
            }
        }

        if (isset($stdVideo->contentDetails)) {
            $model->duration = $stdVideo->contentDetails->duration;
            $model->dimension = $stdVideo->contentDetails->dimension;
            $model->definition = $stdVideo->contentDetails->definition;
            $model->caption = $stdVideo->contentDetails->caption;
            $model->licensed_content = $stdVideo->contentDetails->licensedContent;
            $model->projection = $stdVideo->contentDetails->projection;
        }

        if (isset($stdVideo->status)) {
            $model->upload_status = $stdVideo->status->uploadStatus;
            $model->privacy_status = $stdVideo->status->privacyStatus;
            $model->license = $stdVideo->status->license;
            $model->embeddable = $stdVideo->status->embeddable == true;
            $model->public_stats_viewable = $stdVideo->status->publicStatsViewable == true;
        }

        if (isset($stdVideo->statistics)) {
            $model->view_count = $stdVideo->statistics->viewCount;
            $model->like_count = $stdVideo->statistics->likeCount;
            $model->dislike_count = $stdVideo->statistics->dislikeCount;
            $model->favorite_count = $stdVideo->statistics->favoriteCount;
            $model->comment_count = $stdVideo->statistics->commentCount;
        }

        if (isset($stdVideo->player)) {
            $model->embed_html = $stdVideo->player->embedHtml;
        }

        $video = Videos::query()
            ->where('file_path', 'LIKE', '%'.$model->youtube_id.'%')
            ->first();

        if (!is_null($video)) {
            $model->video_id = $video->id;
        }

        return $model;
    }

    public function getVideoInfo($youtubeId) {
        $video = YoutubeVideoInfos::where('youtube_id', $youtubeId)->first();

        if (is_null($video)) {
            $video = Youtube::getVideoInfo($youtubeId);

            if ($video) {
                $video = $this->convertToModel($video);

                InsertObjectJob::dispatch($video)->onQueue(QueueTypesEnum::LOW);
            } else {
                return false;
            }
        }

        return $video;
    }

    public function getYouTubeId($filePath) {
        if (strstr($filePath, 'youtube.com')) {
            $youtubeId = substr($filePath, strpos($filePath, 'v=') + 2);
        } else if (strstr($filePath, 'youtu.be/')) {
            $youtubeId = substr($filePath, strpos($filePath, 'youtu.be/') + 9);
        }

        if (strpos($youtubeId, '&') === -1) {
            $youtubeId = substr($youtubeId, 0, strpos($youtubeId, '&'));
        }

        return $youtubeId;
    }

    /**
     * @param Videos $video
     * @param YoutubeVideoInfos $info
     *
     * @throws Exception
     */
    public function patchVideo($video, $info) {
        if (!$this->stringIsSet($video->description)) {
            if ($this->stringIsSet($info->description)) {
                $video->description = $info->description;
            }
        }

        if (!$this->stringIsSet($video->short_description)) {
            if ($this->stringIsSet($info->description)) {
                $video->short_description = $info->description;
            }
        }

        if (!$this->numberIsSet($video->seconds)) {
            if ($this->stringIsSet($info->duration)) {
                $seconds = 0;

                $start = new DateTime('@0');
                try {
                    $start->add(new DateInterval($info->duration));
                    $start = $start->format('H:i:s');
                    $start = time() - strtotime($start);

                    if ($start !== 0) {
                        $seconds = ($start) / 1000;
                    }
                } catch (Exception $e) {

                }

                $video->seconds = $seconds;
            }
        }

        if ($video->isDirty()) {
            UpdateObjectJob::dispatch($video)->onQueue(QueueTypesEnum::LOW);
        }
    }

    private function numberIsSet($string) {
        return !is_null($string) && $string !== 0;
    }

    private function stringIsSet($string) {
        return !is_null($string) && strlen($string) !== 0;
    }
}
