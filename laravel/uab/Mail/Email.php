<?php
namespace Uab\Mail;

use Uab\Http\Models\Emails;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use \Exception;
use Uab\Http\Models\Users;

class Email extends Mailable {
    use SerializesModels;

    protected $email;
    protected $fromEmail = 'notifications@nchpad.org';

    /**
     * Create a new message instance.
     *
     * @param Emails $email
     *
     * @throws Exception
     */
    public function __construct(Emails $email) {
        $message = $email->getAttributeValue('message');
        $subject = $email->getAttributeValue('subject');
        $toEmail = $email->getAttributeValue('to_email');

        if (is_null($toEmail) && !is_null($email->to_user_id)) {
            $toUserId = Users::safeDecodeStatic($email->to_user_id);

            /** @var Users $user */
            $user = Users::query()->find($toUserId);
            if (is_null($user)) {
                /** @var Users $user */
                $user = Users::findBySlug($toUserId);
            }

            if (!is_null($user)) {
                if (isset($user->email)) {
                    if (!is_null($user->email)) {
                        $toEmail = $user->email;
                    }
                }
            }
        }

        if (!filter_var($toEmail, FILTER_VALIDATE_EMAIL)) {
            $toEmail = null;
        }

        $signature = '<br/><br/><b>Contact Information</b><br/>If you would like to find out more about the study, please reply to this email (scipe@uab.edu) or contact the SCIPE team at (205) 209-2245.<br/><br/>Sincerely,<br/>SCIPE Team<br/>';
        $checkIfRead = '<img style="height: auto; width: 100%; max-width: 500px; margin: 0; border: 0; padding: 0; display: block;" src="'.config('app.backend').'/open/emails/check/'.$email->slug().'"/>';
        $message .= $signature . $checkIfRead;
        $message = '<div style="font-size:16px">' . $message . '</div>';

        if (!is_null($message) && !is_null($subject) && !is_null($toEmail)) {
            $this->html($message);
            $this->subject($subject);
            $this->to($toEmail);

            $fromEmail = $email->getAttributeValue('from_email');
            if (is_null($fromEmail)) {
                $this->fromEmail = config('mail.from.address', $this->fromEmail);
            } else {
                $this->fromEmail = $fromEmail;
            }
        } else {
            throw new Exception(
                'To email address, subject, and message are all required to send an email.'
            );
        }
    }

    public function build() {
        return $this->from($this->fromEmail);
    }
}
