<?php
namespace Uab\Exceptions;

use RuntimeException;
use Throwable;

class PincodeMissing extends RuntimeException {
    public function __construct($message = "", $code = 0, Throwable $previous = null) {
        if (strlen($message) === 0) {
            $message = 'Pincode missing.';
        }

        parent::__construct($message, $code, $previous);
    }
}
