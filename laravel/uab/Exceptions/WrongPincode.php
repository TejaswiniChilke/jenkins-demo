<?php
namespace Uab\Exceptions;

use RuntimeException;
use Throwable;

class WrongPincode extends RuntimeException {
    public function __construct($message = "", $code = 0, Throwable $previous = null) {
        if (strlen($message) === 0) {
            $message = 'Wrong pincode.';
        }

        parent::__construct($message, $code, $previous);
    }
}
