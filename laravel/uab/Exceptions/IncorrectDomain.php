<?php
namespace Uab\Exceptions;

use RuntimeException;
use Throwable;

class IncorrectDomain extends RuntimeException {
    public function __construct($message = '', $code = 0, Throwable $previous = null) {
        if (strlen($message) !== 0) {
            $message = ' ('.$message.')';
        }

        $message = 'Incorrect domain.' . $message;

        parent::__construct($message, $code, $previous);
    }
}
