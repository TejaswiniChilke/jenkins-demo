<?php
namespace Uab\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redis;
use League\OAuth2\Server\Exception\OAuthServerException;
use Log;
use Uab\Http\Enums\HttpResponseCodesEnum;

class Handler extends ExceptionHandler {
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        OAuthServerException::class
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * @return bool
     */
    private function isDebug():bool {
        return config('app.debug', true) == true;
    }

    /**
     * @return bool
     */
    private function isLocal():bool {
        return config('app.environment', 'prod') === 'local';
    }

    /**
     * @return bool
     */
    private function isTest():bool {
        return config('app.environment', 'prod') === 'test';
    }

    /**
     * Report or log an exception.
     *
     * @param  Exception $exception
     *
     * @return void
     * @throws Exception
     */
    public function report(Exception $exception) {
        if (app()->bound('sentry') && $this->shouldReport($exception)) {
            app('sentry')->captureException($exception);
        }

        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  Request   $request
     * @param  Exception $exception
     *
     * @return Response|void
     */
    public function render($request, Exception $exception) {
        if ($this->isDebug() || $this->isLocal() || $this->isTest()) {
            return parent::render($request, $exception);
        } else {
            Log::error($exception->getFile() . ':' . $exception->getLine());
            Log::error($exception->getTraceAsString());
            abort(500);
        }
    }

    /**
     * @param Request $request
     * @param AuthenticationException $exception
     * @return Response
     */
    protected function unauthenticated($request, AuthenticationException $exception):Response {
        if ($request->expectsJson()) {
            return response(['error' => 'Unauthenticated.'], 401);
        }

        return response('Unauthenticated.', 401);
    }

    /**
     * @param Exception $e
     *
     * @return bool
     */
    public function shouldReport(Exception $e):bool {
        $shouldReport = parent::shouldReport($e);
        if ($shouldReport === false) {
            return false;
        }

        $environment = config('sentry.environment', 'local');
        if (trim(strtolower($environment)) === 'local') {
            return false;
        }

        $errorKey = $e->getFile() . '@' . $e->getLine();
        $errorCount = Redis::get($errorKey);
        if (!is_numeric($errorCount)) {
            $errorCount = 0;
        }

        Redis::set($errorKey, $errorCount);
        Redis::expire($errorKey, 120);

        if ($errorCount > 20) {
            return false;
        }

        $shouldReport = !in_array(
            $e->getCode(),
            [
                HttpResponseCodesEnum::CLIENT_ERROR_UNAUTHORIZED
            ]
        );


        return $shouldReport;
    }
}
