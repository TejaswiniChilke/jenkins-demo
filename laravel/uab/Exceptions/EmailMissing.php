<?php
namespace Uab\Exceptions;

use RuntimeException;
use Throwable;

class EmailMissing extends RuntimeException {
    public function __construct($message = "", $code = 0, Throwable $previous = null) {
        if (strlen($message) === 0) {
            $message = 'Email missing.';
        }

        parent::__construct($message, $code, $previous);
    }
}
