<?php
namespace Uab\Exceptions;

use RuntimeException;
use Throwable;

class TokenMissing extends RuntimeException {
    public function __construct($message = "", $code = 0, Throwable $previous = null) {
        if (strlen($message) === 0) {
            $message = 'Token missing.';
        }

        parent::__construct($message, $code, $previous);
    }
}
