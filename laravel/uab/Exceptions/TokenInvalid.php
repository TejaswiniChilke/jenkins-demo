<?php
namespace Uab\Exceptions;

use RuntimeException;
use Throwable;
use Uab\Http\Enums\HttpResponseCodesEnum;

class TokenInvalid extends RuntimeException {
    public function __construct($message = "", $code = null, Throwable $previous = null) {
        if (strlen($message) === 0) {
            $message = 'Invalid token.';
        }

        if (is_null($code)) {
            $code = HttpResponseCodesEnum::CLIENT_ERROR_UNAUTHORIZED;
        }

        parent::__construct($message, $code, $previous);
    }
}
