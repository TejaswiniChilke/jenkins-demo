<?php
namespace Uab\Exceptions;

use RuntimeException;

class UserNotFound extends RuntimeException {

}
