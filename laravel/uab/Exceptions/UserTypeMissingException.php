<?php
namespace Uab\Exceptions;

use RuntimeException;
use Throwable;

class UserTypeMissingException extends RuntimeException {
    public function __construct($message = "", $code = 0, Throwable $previous = null) {
        if (strlen($message) === 0) {
            $message = 'The user does not have a user type set.';
        }

        parent::__construct($message, $code, $previous);
    }
}
