<?php
namespace Uab\Exceptions;

use RuntimeException;

class UserRequiresApprovalException extends RuntimeException {

}
