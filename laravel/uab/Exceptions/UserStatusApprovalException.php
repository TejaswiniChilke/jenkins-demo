<?php
namespace Uab\Exceptions;

use RuntimeException;
use Throwable;

class UserStatusApprovalException extends RuntimeException {
    public function __construct($userStatus, $message = "", $code = 0, Throwable $previous = null) {
        if (strlen($message) === 0) {
            $message = 'User not allowed to login. Please contact staff if this is a mistake.';
        }

        parent::__construct($message, $code, $previous);
    }
}
