<?php
namespace Uab\Exceptions;

use RuntimeException;
use Throwable;
use Uab\Http\Enums\HttpResponseCodesEnum;

class TokenRevoked extends RuntimeException {
    public function __construct($message = "", $code = null, Throwable $previous = null) {
        if (strlen($message) === 0) {
            $message = 'Token already used.';
        }

        if (is_null($code)) {
            $code = HttpResponseCodesEnum::CLIENT_ERROR_UNAUTHORIZED;
        }

        parent::__construct($message, $code, $previous);
    }
}
