<?php
namespace Uab\Controllers;

use Artisan;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redis;
use Uab\Http\Models\Base\BaseModel;
use Uab\Providers\ModelServiceProvider;

class TestController {
    /**
     * @param Request $request
     * @param $parentTable
     * @param null $slug
     *
     * @return Response
     */
    public function id(Request $request, $parentTable, $slug = null):Response {
        $service = new ModelServiceProvider();

        /** @var BaseModel $model */
        $parentModel = $service->getModel($parentTable);

        $slug = $request->input('slug', $slug);
        if (is_null($slug)) {
            return response([
                'error' => 'ID is required to get slug.'
            ]);
        }

        return response([
            'id' => $parentModel->safeDecode($slug)
        ]);
    }

    /**
     * @param Request $request
     * @param $parentTable
     * @param null $id
     *
     * @return Response
     */
    public function slug(Request $request, $parentTable, $id = null):Response {
        $service = new ModelServiceProvider();

        /** @var BaseModel $model */
        $parentModel = $service->getModel($parentTable);

        $id = $request->input('id', $id);
        if (is_null($id)) {
            return response([
                'error' => 'ID is required to get slug.'
            ]);
        }

        $id = $parentModel->safeDecode($id);

        $model = $parentModel->newQuery()->find($id);
        if (is_null($model)) {
            return response([
                'error' => 'Unable to find model by ID ('.$id.')'
            ]);
        }

        return response([
            'slug' => $model->slug()
        ]);
    }

    /**
     * @param Request $request
     * @param $command
     */
    public function make(Request $request, $command) {
        $params = $request->input('params', []);

        Artisan::call(
            'make:'.$command,
            [
                'name' => $params
            ]
        );
    }

    /**
     * @param Request $request
     * @param $key
     *
     * @return mixed
     */
    public function redisGet(Request $request, $key) {
        return Redis::get($key);
    }

    /**
     * @param Request $request
     * @param $key
     * @param $value
     *
     * @return mixed
     */
    public function redisSet(Request $request, $key, $value) {
        Redis::set($key, $value);

        return Redis::get($key);
    }
}
