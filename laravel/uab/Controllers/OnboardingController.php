<?php
namespace Uab\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Uab\Controllers\Base\ApiController;
use Uab\Http\Enums\NotificationTypesEnum;
use Uab\Http\Enums\QueueTypesEnum;
use Uab\Http\Enums\UserStatusesEnum;
use Uab\Http\Enums\UserTypesEnum;
use Uab\Http\Models\BrowserChecks;
use Uab\Http\Models\Emails;
use Uab\Http\Models\Users;
use Uab\Http\Enums\HttpResponseCodesEnum;
use Uab\Jobs\Base\InsertObjectJob;
use Uab\Jobs\Emails\EmailUserType;
use Uab\Jobs\Emails\SendEmail;
use Uab\Jobs\Notifications\NotifyUserType;
use Uab\Jobs\Emails\SendAndCreateEmail;
use Uab\Providers\Auth;

class OnboardingController extends ApiController {
    public $model = 'users';

    function apply(Request $request):Response {
        $email = $request->input('email', null);

        if (is_null($email)) {
            return response([
                'Email address is required to apply.'
            ]);
        }
        /** @var Users $user */
        $user = Users::query()
            ->where('email', '=', $email)
            ->first();

        if (is_null($user)) {
            $user = new Users();
            $user->email = $email;
            $user->approved_on = null;
            $user->status = UserStatusesEnum::AWAITING_EMAIL_CONFIRMATION;

            $success = $user->save();

            $this->sendConfirmationEmail($user);

            if (!$success) {
                return response([
                    'error' => 'Unable to create user for applicant.'
                ]);
            }
        }

        $array = $user->toArray();
        $array['states'] = $user->state();
        $array = $this->queryService->slugIds($user, $array);

        return response(
            [
                'users' => $array
            ]
        );
    }

    function blockBrowser(Request $request, $browser = null):Response {
        $browser = $request->input('browser', $browser);
        if (is_null($browser)) {
            return response(['error' => 'Browser is required.']);
        }

        $id = Auth::id();

        $check = new BrowserChecks();
        $check->user_id = $id;
        $check->browser = $browser;
        $check->checked_at = Carbon::now();
        $check->ip_address = $request->ip();

        InsertObjectJob::dispatch($check)->onQueue(QueueTypesEnum::LOW);

        return response(
            [
                'done'
            ]
        );
    }

    function checkBrowser(Request $request, $id):Response {
        $success = false;

        $browser = $request->input('browser_check');
        if (is_null($browser)) {
            return response(['error' => 'Browser check is required.']);
        }

        $id = Users::safeDecodeStatic($id);

        /** @var Users|null $user */
        $user = Users::query()->find($id);

        if (!is_null($user)) {
            $user->browser_check = $browser;
            $user->status = UserStatusesEnum::BROWSER_CONFIRMED;

            $success = $user->save();
        }

        $check = new BrowserChecks();
        $check->user_id = $id;
        $check->browser = $browser;
        $check->checked_at = Carbon::now();
        $check->ip_address = $request->ip();

        InsertObjectJob::dispatch($check)->onQueue(QueueTypesEnum::LOW);

        return response(
            [
                'success' => $success
            ]
        );
    }

    function speedCheck(Request $request, $id):Response {
        $success = false;

        $id = Users::safeDecodeStatic($id);

        $user = Users::query()->find($id);

        if (!is_null($user)) {
            $user->speed_check = $request->input('speed_check');
            $user->status = UserStatusesEnum::SPEED_CONFIRMED;

            $success = $user->save();
        }

        return response(
            [
                'success' => $success
            ]
        );
    }

    function finalize($id) {
        $model = new Users();

        $id = $model->safeDecode($id);

        /** @var Users|null $user */
        $user = Users::query()->find($id);

        if (!is_null($user)) {
            $user->approved_on = null;
            $user->declined_on = null;
            $user->status = UserStatusesEnum::APPLICATION_PENDING;
            $success = $user->save();

            if ($success) {
                EmailUserType::dispatch(
                    UserTypesEnum::ADMIN,
                    'A user applied to the study.<br/><br/>User Edit Link: ' . config('app.frontend') . '/#/user-edit/' . $user->slug(),
                    'Application Received'
                )->onQueue(QueueTypesEnum::NORMAL);

                NotifyUserType::dispatch(
                    UserTypesEnum::ADMIN,
                    'A user applied to the study.',
                    'Application Received',
                    NotificationTypesEnum::NEW_APPLICATION
                )->onQueue(QueueTypesEnum::NORMAL);

                $email = new Emails();
                $email->to_user_id = $user->id;
                $email->to_email = $user->email;
                $email->subject = 'Your SCIPE application status is pending';

                $message = "Your application has been successfully submitted to the SCIPE team!<br/><br/>"
                            . "We will review your application within 24 hours and follow up with a link to the Participant Screening Form in the SCIPE study. This form will help us determine your eligibility for participation and whether you will need a physician’s approval before participating in the study. Please complete the Participant Screening Form at your earliest convenience.<br/><br/>"
                            . "Thank you again for your interest in our study.";

                $email->message = $message;
                $email->save();

                SendEmail::dispatch($email)->onQueue(QueueTypesEnum::EMAIL);

                $array = $user->toArray();
                $array['id'] = $user->slug();

                return response(
                    [
                        'user' => $array
                    ]
                );
            } else {
                return response(
                    ['error' => 'Unable to update applicant.' ],
                    HttpResponseCodesEnum::SERVER_ERROR_INTERNAL_SERVER_ERROR
                );
            }
        }

        return response(
            ['error' => 'Unable to finalize the applicant.' ],
            HttpResponseCodesEnum::CLIENT_ERROR_BAD_REQUEST
        );
    }

    public function resend(Request $request, $userId) {
        $user = new Users();
        $userId = $user->safeDecode($userId);

        $user = Users::query()->find($userId);

        if (is_null($user)) {
            return response(
                ['error' => 'Could not find user ('.$userId.').' ],
                HttpResponseCodesEnum::CLIENT_ERROR_BAD_REQUEST
            );
        }

        $this->sendConfirmationEmail($user);

        return response()->json(
            [
                'success' => true
            ]
        );
    }

    public function setPassword(Request $request, $id, $password) {
        $id = Users::decodeSlug($id);

        /** @var Users $user */
        $user = Users::find($id);

        if (is_null($user)) {
            return response()->error(
                'Cannot find user with id ('.$id.').',
                HttpResponseCodesEnum::CLIENT_ERROR_BAD_REQUEST
            );
        }

        $password = $request->input('password', $password);

        if (is_null($password)) {
            return response()->error(
                'Password is required.',
                HttpResponseCodesEnum::CLIENT_ERROR_BAD_REQUEST
            );
        }

        $securityQuestion = $request->input('security_question', null);
        $securityAnswer = $request->input('security_answer', null);

        $user->status = UserStatusesEnum::NEED_PROFILE_COMPLETE;
        $user->password = $password;
        $user->security_question = $securityQuestion;
        $user->security_answer = $securityAnswer;
        $success = $user->save();

        return response()->json(
            [
                'done' => $success
            ]
        );
    }

    /**
     * @param Users $user
     */
    private function sendConfirmationEmail(Users $user) {
        $email = new Emails();
        $email->to_user_id = $user->id;
        $email->to_email = $user->email;
        $email->subject = 'Email confirmation from SCIPE Website';

        $message = "Thank you for your interest in our SCIPE study!<br/><br/>Please click the link below to continue with the application process on the SCIPE study website. "
                    . "Please open the link with either Google Chrome, Firefox, or Safari. The Internet Explorer browser does not support the SCIPE website. " . "https://screen.scipe.org/#/confirm-email/".$user->slug() . "<br/><br/>"
                    . "While directing you to the study website, we will automatically check your internet speed and the current version of the internet browser. This test will confirm whether your internet speed and browser update are sufficient for viewing exercise videos with appropriate quality. If you need technical support, do not hesitate to contact the SCIPE team.";

        $email->message = $message;
        $email->save();

        SendEmail::dispatch($email)->onQueue(QueueTypesEnum::EMAIL);
    }
}
