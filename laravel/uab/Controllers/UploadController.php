<?php
namespace Uab\Controllers;

use File;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Uab\Http\Models\Images;
use Uab\Http\Enums\HttpResponseCodesEnum;
use Uab\Providers\QueryServiceProvider;
use Illuminate\Contracts\Filesystem\FileNotFoundException;

class UploadController {
    public $model = 'images';

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function upload(Request $request):Response {
        return $this->uploadFiles($request, 'image');
    }

    /**
     * @param Request $request
     * @param string $key
     * @param bool $isWysiwygFile
     *
     * @return Response
     */
    public function uploadFiles(Request $request, string $key, $isWysiwygFile = false):Response {
        if (is_null($request->file($key))) {
            return response([
                'error' => 'File not available from request.'
            ]);
        }

        $uploadedImage = $request->file($key);

        if (is_null($uploadedImage)) {
            return response(
                [
                    'error' => 'Could not get uploaded file.',
                ],
                HttpResponseCodesEnum::CLIENT_ERROR_BAD_REQUEST
            );
        } else if (!$uploadedImage->isValid()) {
            return response(
                [
                    'error' => 'The uploaded file is not valid.'
                ]
            );
        }

        $extension = $uploadedImage->extension();
        if (!in_array($extension, ['png', 'jpg', 'jpeg', 'gif', 'pdf'])) {
            return response(
                [
                    'error' => 'Extension ' . $extension . ' not allowed.'
                ]
            );
        }

        $middlePath = storage_path('app/public') . '/' . config('upload.image.directory') . '/';

        $file = $uploadedImage->move($middlePath, uniqid() . '.' . $extension);

        if ($uploadedImage->getError()) {
            return response(
                [
                    'error' => $uploadedImage->getErrorMessage()
                ]
            );
        }

        $image = new Images();

        $image->file_path = config('upload.image.directory') . '/' . $file->getFilename();
        $image->title = $file->getFilename();

        if ($image->save()) {
            if ($isWysiwygFile) {
                $image_url = config('app.frontend') . '/#/open/' . $image->file_path;

                $returnThread = collect();
                $returnThread->put('url', $image_url);

                return response(
                    $returnThread,
                    HttpResponseCodesEnum::SUCCESS_OK
                );
            }

            $queryService = new QueryServiceProvider();
            $returnImage = $queryService->slugIds('images', $image);

            return response($returnImage);
        }

        return response(
            [
                'error' => 'Could not save image.'
            ]
        );
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function wysiwygUpload(Request $request):Response {
        return $this->uploadFiles( $request, 'file',true );
    }

    /**
     * @param string $filename
     *
     * @return Response
     *
     * @throws FileNotFoundException
     */
    public function getFile(string $filename):Response {
        $path = storage_path('app/public') . '/' . config('upload.image.directory') . '/' . $filename;

        if (!File::exists($path)) {
            return response(
                [
                    'error' => 'File not found.'
                ],
                HttpResponseCodesEnum::CLIENT_ERROR_FOUND
            );
        }

        $file = File::get($path);
        $type = File::mimeType($path);

        $response = Response::create($file, 200);
        $response->header("Content-Type", $type);

        return $response;
    }

    /**
     * @param string $filename
     *
     * @return Response
     *
     * @throws FileNotFoundException
     */
    public function getVideo(string $filename):Response {
        $path = storage_path('videos') . '/' . $filename;

        if (!File::exists($path)) {
            return response(
                [
                    'error' => 'Video not found.'
                ],
                HttpResponseCodesEnum::CLIENT_ERROR_FOUND
            );
        }

        $file = File::get($path);
        $type = File::mimeType($path);

        $response = Response::create($file, 200);
        $response->header("Content-Type", $type);

        return $response;
    }
}
