<?php
namespace Uab\Controllers;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use Uab\Http\Models\Threads;
use Uab\Http\Models\Users as User;
use Uab\Http\Enums\HttpResponseCodesEnum;
use Carbon\Carbon;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Uab\Providers\Auth;
use Exception;

class MessagingController {
    /**
     * Show all of the message threads to the user.
     *
     * @param string|int|null $id
     *
     * @return JsonResponse
     */
    public function index($id = null):JsonResponse {
        if (!is_null($id)) {
            if (is_string($id) && !is_numeric($id)) {
                $id = Threads::decodeSlug($id);
            }

            /** @var Threads $thread */
            $thread = Threads::query()->find($id);
            if (is_null($thread)) {
                return response()->json(
                    [
                        'error' => 'Unable to find the thread.'
                    ],
                    HttpResponseCodesEnum::CLIENT_ERROR_FOUND
                );
            }
            $thread->markAsRead(Auth::id());

            $threads = [ $thread ];
        } else {
            $operator = '!=';

            $threads = $this->loadThreads($operator);
        }

        return $this->returnThreads($threads);
    }

    /**
     * @return JsonResponse
     */
    public function sentMessages():JsonResponse {
        $operator = '=';

        $threads = $this->loadThreads($operator);

        return $this->returnThreads($threads);
    }

    /**
     * @param $operator
     *
     * @return Collection<Threads>|Threads[]
     */
    public function loadThreads($operator) {
        $uId = Auth::id();

        /** @var Builder $query */
        $query = Threads::forUser($uId)
            ->distinct()
            ->join('messages', 'messages.thread_id', '=', 'threads.id')
            ->where('messages.user_id', $operator, $uId)
            ->orderBy('messages.created_at', 'desc');

        return $query->get();
    }

    /**
     * @param Collection<Threads>|Threads[] $threads
     *
     * @return JsonResponse
     */
    public function returnThreads($threads):JsonResponse {
        $returnVar = collect();

        foreach ($threads as $thread) {
            $users = $thread->users();

            $participants = collect();

            $hasMany = $thread->participants()->get();
            foreach ($hasMany as $i => $participant) {
                $user = $participant->user;

                $participants->push($user);
            }

            $messages = $thread->messages()->get();
            foreach ($messages as $i => $message) {
                $messages[$i]->user = $message->user();
            }

            $threadArray = $thread->toArray();
            $threadArray['id'] = $thread->slug();
            $threadArray['has_read'] = !$thread->isUnread(Auth::id());
            $threadArray['messages'] = $messages;
            $threadArray['participants'] = $participants;
            $threadArray['users'] = $users;

            $returnVar->push($threadArray);
        }

        return response()->json(
            [
                'conversations' => $returnVar
            ],
            HttpResponseCodesEnum::SUCCESS_OK
        );
    }

    /**
     * Stores a new message thread.
     *
     * @return Response
     *
     * @throws Exception
     */
    public function store():Response {
        $input = Input::all();

        /** @var Threads $thread */
        $thread = Threads::query()
            ->create(
                [
                    'subject' => $input['subject'],
                ]
            );

        Message::query()
            ->create(
                [
                    'thread_id' => $thread->id,
                    'user_id' => Auth::id(),
                    'body' => $input['message'],
                ]
            );

        Participant::query()
            ->create(
                [
                    'thread_id' => $thread->id,
                    'user_id' => Auth::id(),
                    'last_read' => new Carbon,
                ]
            );

        if (Input::has('recipients')) {
            $recipients = $input['recipients'];
            $recipients = collect($recipients)->pluck('id')->all();
            $recipients = array_values($recipients);

            foreach ($recipients as $key => $value) {
                $userId = User::safeDecodeStatic($value);

                $recipient = User::query()->find($userId);

                $recipients[$key] = $recipient->id;
            }

            $thread->addParticipant($recipients);

            return response(
                [
                    'message' => 'Message sent.'
                ],
                HttpResponseCodesEnum::SUCCESS_OK
            );
        }

        return response(
            [
                'error' => 'Unable to send message.'
            ]
        );
    }

    /**
     * Adds a new message to a current thread.
     *
     * @param $id
     *
     * @return Response
     */
    public function update($id):Response {
        try {
            $id = Threads::decodeSlug($id);

            $thread = Threads::query()->findOrFail($id);

            $thread->activateAllParticipants();

            $message = Message::query()
                ->create(
                    [
                        'thread_id' => $thread->id,
                        'user_id' => Auth::id(),
                        'body' => Input::get('message'),
                    ]
                );

            if ($message) {
                $message->user;
            }

            $createdMessage = collect();
            $createdMessage->put('message', $message );

            return response(
                $createdMessage,
                HttpResponseCodesEnum::SUCCESS_OK
            );
        } catch (ModelNotFoundException $e) {
            return response(
                [
                    'error' => 'The thread with ID: ' . $id . ' was not found.'
                ]
            );
        }
    }
}
