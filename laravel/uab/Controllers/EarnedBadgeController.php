<?php
namespace Uab\Controllers;

use Uab\Http\Models\Users;
use Uab\Jobs\Badges\EarnedBadgeNotification;

class EarnedBadgeController  {
    public function join($activeUserId) {
        $model = new Users();
        $activeUserId = $model->safeDecode($activeUserId);

        EarnedBadgeNotification::dispatch($activeUserId);

        return response(
            [
                'success' => true
            ]
        );
    }
}
