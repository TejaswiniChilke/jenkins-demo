<?php
namespace Uab\Controllers\Communication;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Uab\Controllers\TextMessagesController as GeneratedController;
use Uab\Http\Enums\QueueTypesEnum;
use Uab\Http\Enums\PhoneDirectionsEnum;
use Uab\Http\Enums\TwilioTextEnums;
use Uab\Http\Models\TextMessages;
use Uab\Http\Models\Users;
use Uab\Http\Enums\HttpResponseCodesEnum;
use Uab\Jobs\Texts\SaveText;
use Uab\Jobs\Base\UpdateObjectJob;
use Uab\Jobs\Texts\SendAndCreateText;
use Uab\Providers\Auth;
use Uab\Providers\TextMessageServiceProvider;

class TextsController extends GeneratedController {
    /**
     * @param Request $request
     *
     * @return TextMessages
     */
    private function buildTextMessage($request) {
        $textMessage = new TextMessages();
        $textMessage->direction = PhoneDirectionsEnum::INCOMING;
        $textMessage->from_number = $request->input(TwilioTextEnums::FROM);
        $textMessage->message = $request->input(TwilioTextEnums::BODY);
        $textMessage->num_media = $request->input(TwilioTextEnums::NUM_MEDIA);
        $textMessage->num_segments = $request->input(TwilioTextEnums::NUM_SEGMENTS);
        $textMessage->sid = $request->input(TwilioTextEnums::MESSAGE_SID);
        $textMessage->to_number = $request->input(TwilioTextEnums::TO);

        return $textMessage;
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function sendSms(Request $request) {
        $textMessage = new TextMessages();

        $fromUserId = $request->input('from_user_id', null);
        if (is_null($fromUserId)) {
            /** @var Users|null $activeUser */
            $fromUserId = Auth::id();
        }

        if (is_null($fromUserId)) {
            return response()->error(
                'You must include a from_user_id or be logged in to send a text message.',
                HttpResponseCodesEnum::CLIENT_ERROR_BAD_REQUEST
            );
        }

        /** @var Users $fromUser */
        $fromUser = Users::query()->find($fromUserId);

        if (is_null($fromUser)) {
            return response()->error(
                "Unable to find user with id ({$fromUserId}).",
                HttpResponseCodesEnum::CLIENT_ERROR_BAD_REQUEST
            );
        }

        $textMessage->from_user_id = $fromUser->id;

        $toUserId = $request->input('from_user_id', null);
        if (is_null($toUserId)) {
            /** @var Users|null $activeUser */
            $toUserId = Auth::id();
        }

        if (is_null($toUserId)) {
            return response()->error(
                "Unable to find user with id ({$toUserId}).",
                HttpResponseCodesEnum::CLIENT_ERROR_BAD_REQUEST
            );
        }

        /** @var Users $toUser */
        $toUser = Users::query()->find($toUserId);

        if (is_null($toUser)) {
            return response()->error(
                "Unable to find user with id ({$toUserId}).",
                HttpResponseCodesEnum::CLIENT_ERROR_BAD_REQUEST
            );
        }

        $textMessage->to_user_id = $toUser->id;

        $message = $request->input('message', '');
        if (strlen($message) === 0) {
            return response()->error(
                "Message is required.",
                HttpResponseCodesEnum::CLIENT_ERROR_BAD_REQUEST
            );
        }

        $textMessage->message = $message;

        SendAndCreateText::dispatch($textMessage)->onQueue(QueueTypesEnum::TEXT);

        return response(
            [
                'success' => true
            ]
        );
    }

    public function receiveError(Request $request) {
        // TODO: Save errors to DB
        return response(
            [
                'success' => true
            ]
        );
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function receiveSms(Request $request) {
        $accountSid = $request->input(TwilioTextEnums::ACCOUNT_SID);
        if ($accountSid === config('twilio.account.sid')) {
            $textMessage = $this->buildTextMessage($request);

            $textMessageService = new TextMessageServiceProvider();
            $textMessage = $textMessageService->receiveSms($textMessage);

            if ($textMessage) {
                SaveText::dispatch($textMessage)->onQueue(QueueTypesEnum::NORMAL);

                return response(
                    [
                        'text_messages' => $textMessage
                    ]
                );
            } else {
                return response()->error('Unable to convert text message.');
            }
        } else {
            return response()->error('Incorrect AccountSid.');
        }
    }

    public function receiveStatus(Request $request, $slug) {
        $id = TextMessages::safeDecodeStatic($slug);

        /** @var TextMessages|null $text */
        $text = TextMessages::query()->find($id);
        if (!is_null($text)) {
            $text->sms_id = $request->input('SmsSid', null);
            $text->status = $request->input('SmsStatus', null);
            $text->message_sid = $request->input('MessageSid', null);
            $text->account_sid = $request->input('AccountSid', null);

            UpdateObjectJob::dispatch($text)->onQueue(QueueTypesEnum::HIGH);
        }

        return response(
            [
                'success' => true
            ]
        );
    }
}
