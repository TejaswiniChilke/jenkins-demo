<?php
namespace Uab\Controllers\Communication;

use \Exception;
use File;
use Illuminate\Http\Request;
use Log;
use Response;
use Uab\Controllers\Generated\EmailsController as GeneratedController;
use Uab\Http\Enums\HttpResponseCodesEnum;
use Uab\Http\Enums\QueueTypesEnum;
use Uab\Http\Models\Emails;
use Uab\Http\Models\Users;
use Uab\Jobs\Emails\MarkEmailRead;
use Uab\Jobs\Emails\SendEmail;
use Uab\Providers\Auth;
use Uab\Providers\UsersServiceProvider;

class EmailsController extends GeneratedController {
    public function __construct() {
        parent::__construct();
    }

    public function checkEmail(Request $request, $id) {
        MarkEmailRead::dispatch($id, now())->onQueue(QueueTypesEnum::LOW);

        $path = config('app.logo', '');

        if (strlen($path) !== 0 && File::exists($path)) {
            try {
                $file = File::get($path);
                $type = File::mimeType($path);

                return Response::make($file)->header("Content-Type", $type);
            } catch (Exception $e) {
                Log::error($e->getMessage());
            }
        }

        return response('');
    }

    public function sendEmail(Request $request) {
        $email = new Emails();

        if ($request->has('from_user_id')) {
            $email->from_user_id = $request->input('from_user_id', null);
        } else {
            $activeUser = Auth::user();
            if (!is_null($activeUser)) {
                $userService = new UsersServiceProvider();

                if (!$userService->isAdmin($activeUser)) {
                    $email->from_user_id = $activeUser->id;
                }
            } else {
                return response([
                    'You must be logged into to assume from_user_id when sending an email.',
                    HttpResponseCodesEnum::CLIENT_ERROR_BAD_REQUEST
                ]);
            }
        }

        if ($request->has('from_email')) {
            $email->from_email = $request->input('from_email', null);
        } else {
            /** @var Users $fromUser */
            $fromUser = $this->queryService->getById($email->from_user_id, 'users');
            if (!is_null($fromUser)) {
                $email->from_email = $fromUser->email;
            }
        }

        if ($request->has('to_user_id')) {
            $users = new Users();
            $email->to_user_id = $users->safeDecode($request->input('to_user_id', null));
        } else {
            return response([
                'To user ID is required to send.',
                HttpResponseCodesEnum::CLIENT_ERROR_BAD_REQUEST
            ]);
        }

        if ($request->has('to_email')) {
            $email->to_email = $request->input('to_email', null);
        } else {
            /** @var Users $toUser */
            $toUser = Users::query()->find($email->to_user_id);

            if (!is_null($toUser)) {
                $email->to_email = $toUser->email;
            } else {
                return response([
                    "Unable to find user with id ({$email->to_user_id}).",
                    HttpResponseCodesEnum::CLIENT_ERROR_BAD_REQUEST
                ]);
            }
        }

        $message = '';
        if ($request->has('message')) {
            $message = $request->input('message', null);
        }

        if (strlen($message) === 0) {
            return response([
                'Message is required.',
                HttpResponseCodesEnum::CLIENT_ERROR_BAD_REQUEST
            ]);
        } else {
            $email->message = $message;
        }

        $subject = '';
        if ($request->has('subject')) {
            $subject = $request->input('subject', null);
        }

        if (strlen($subject) === 0) {
            return response([
                'Message is required.',
                HttpResponseCodesEnum::CLIENT_ERROR_BAD_REQUEST
            ]);
        } else {
            $email->subject = $subject;
        }

        $email->save();

        try {
            SendEmail::dispatch($email)->onQueue(QueueTypesEnum::EMAIL);
        } catch (Exception $e) {
            return response([
                'Unable to send the email. Try again later.',
                HttpResponseCodesEnum::SERVER_ERROR_INTERNAL_SERVER_ERROR
            ]);
        }

        return response(
            [
                'emails' => $email
            ]
        );
    }
}
