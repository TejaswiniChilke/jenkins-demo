<?php
namespace Uab\Controllers;

use Illuminate\Http\Request;
use Uab\Controllers\Generated\UserTypesController as GeneratedController;
use Uab\Http\Models\Badges;
use Uab\Http\Models\BadgesUserTypes;
use Uab\Http\Models\UserTypes;

class UserTypesController extends GeneratedController {
    public function doesUserTypeHaveBadge($userTypeId, $badgeId) {
        return BadgesUserTypes::where(
            [
                'badge_id' => $badgeId,
                'user_type_id' => $userTypeId
            ]
        )->exists();
    }

    public function addBadges(Request $request, $id) {
        $id =  UserTypes::safeDecodeStatic($id);

        $badges = $request->input('badges', []);

        $toBeCreated = [];
        $toBeDeleted = [];

        foreach ($badges as $badge) {
            if (array_key_exists('id', $badge) && array_key_exists('isSelected',  $badge)) {
                $badgeId = Badges::decodeSlug($badge['id']);

                if ($badge['isSelected']) {
                    $toBeCreated[] = $badgeId;
                } else {
                    $toBeDeleted[] = $badgeId;
                }
            }
        }

        foreach ($toBeCreated as $badgeId) {
            $exists = $this->doesUserTypeHaveBadge($id, $badgeId);

            if (!$exists) {
                $badgeUserType = new BadgesUserTypes();
                $badgeUserType->badge_id = $badgeId;
                $badgeUserType->user_type_id = $id;
                $badgeUserType->save();
            }
        }

        foreach ($toBeDeleted as $badgeId) {
            $exists = $this->doesUserTypeHaveBadge($id, $badgeId);

            if ($exists) {
                BadgesUserTypes::where(
                    [
                        'badge_id'     => $badgeId,
                        'user_type_id' => $id
                    ]
                )->delete();
            }
        }

        return response(
            [
                'success' => true
            ]
        );
    }
}
