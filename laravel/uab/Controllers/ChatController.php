<?php
namespace Uab\Controllers;

use Uab\Providers\Auth;
use Illuminate\Http\Request;
use Uab\Events\Chat\UserJoinedChat;
use Uab\Events\Chat\UserLeftChat;
use Uab\Events\Chat\UserSentChat;

class ChatController  {
    function socket(Request $request, $action) {
        $user = Auth::user();

        if ($user) {
            $event = false;

            switch ($action) {
                case 'join':
                    $event = new UserJoinedChat($user);

                    break;
                case 'leave':
                    $event = new UserLeftChat($user);

                    break;
                case 'send':
                    $message = $request->input('message');

                    if (!is_null($message)) {
                        $event = new UserSentChat($user, $message);
                    }

                    break;
                default:
                    return response()->error(
                        'Unknown action ('.$action.').'
                    );

                    break;
            }

            if ($event) {
                event($event);

                return response()->json(
                    [
                        'success' => true
                    ]
                );
            }
        } else {
            return response()->error(
                'User could not be found.'
            );
        }

        return response(
            [
                'success' => true
            ]
        );
    }
}
