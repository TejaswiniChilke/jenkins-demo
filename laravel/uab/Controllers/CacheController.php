<?php
namespace Uab\Controllers;

use Response;
use Uab\Controllers\Generated\ColorTemplatesController as GeneratedController;
use Cache;
use Illuminate\Http\Request;
use Uab\Http\Enums\HttpResponseCodesEnum;
use Uab\Http\Enums\UserTypesEnum;
use Uab\Providers\Auth;

class CacheController extends GeneratedController {
    /**
     * @param Request     $request
     * @param string|null $candidateSecret
     *
     * @return Response
     */
    public function flush(Request $request, $candidateSecret = null) {
        $user = Auth::user();

        if (is_null($user)) {
            $correctSecret = config('cache.secret');
            if (is_null($correctSecret)) {
                return response()->error(
                    'Secret is not set.',
                    HttpResponseCodesEnum::SERVER_ERROR_INTERNAL_SERVER_ERROR
                );
            }

            $candidateSecret = $request->input('secret', $candidateSecret);

            if (is_null($candidateSecret)) {
                return response()->error(
                    'Secret is required.',
                    HttpResponseCodesEnum::CLIENT_ERROR_BAD_REQUEST
                );
            } else if ($candidateSecret === $correctSecret) {
                return response()->json(
                    [
                        'success' => Cache::flush()
                    ]
                );
            } else {
                return response()->error(
                    'Secret does not match.',
                    HttpResponseCodesEnum::CLIENT_ERROR_UNAUTHORIZED
                );
            }
        } else {
            if ($user->userType()->name === UserTypesEnum::SUPER_ADMIN) {
                return response()->json(
                    [
                        'success' => Cache::flush()
                    ]
                );
            } else {
                return response()->error(
                    'User does not have permissions to flush cache.',
                    HttpResponseCodesEnum::CLIENT_ERROR_UNAUTHORIZED
                );
            }
        }
    }
}
