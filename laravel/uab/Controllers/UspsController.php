<?php
namespace Uab\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Uab\Http\Models\Users;
use Uab\Http\Models\UspsAddresses;
use Usps;

class UspsController {
    public function validateAddress(Request $request) {
        $line1 = $request->input('line1', null);
        if (is_null($line1)) {
            return response()->error('Line1 is required to validate address.');
        }

        $line2 = $request->input('line2', null);

        $city = $request->input('city', null);
        if (is_null($city)) {
            return response()->error('City is required to validate address.');
        }

        $state = $request->input('state', null);
        if (is_null($state)) {
            return response()->error('State is required to validate address.');
        }

        $zipcode = $request->input('zipcode', null);
        if (is_null($zipcode)) {
            return response()->error('Zipcode is required to validate address.');
        }

        $address = UspsAddresses::where(
            [
                'og_address_line_1' => $line1,
                'og_address_line_2' => $line2,
                'og_city'           => $city,
                'og_state'          => $state,
                'og_zipcode'        => $zipcode,
            ]
        )->first();

        if (is_null($address)) {
            $uspsAddress = Usps::validate(
                $line1,
                $city,
                $state,
                $zipcode
            );

            if (is_array($uspsAddress) && array_key_exists('address', $uspsAddress)) {
                $uspsAddress = $uspsAddress['address'];

                $address = new UspsAddresses();
                $address->og_address_line_1 = $line1;
                $address->og_address_line_2 = $line2;
                $address->og_city = $city;
                $address->og_state = $state;
                $address->og_zipcode = $zipcode;

                $address = $this->addUspsAddress($address, $uspsAddress);

                $address->save();
            }
        }

        return response()->json(
            [
                'address' => $address
            ]
        );
    }

    public function validateUserAddress(Request $request) {
        $fillables = $request->input('users', null);
        if (is_null($fillables)) {
            return response()->error('User required.');
        }

        $user = new Users();
        $user->setRawAttributes($fillables);

        $line1 = $user->address_line_one;
        $line2 = $user->address_line_two;
        $city = $user->city;

        $state = $user->state();
        if (!is_null($state)) {
            $state = $state->name;
        } else {
            $state = '';
        }

        $zipcode = $user->zipcode;

        $address = UspsAddresses::query()
            ->where(
                [
                    'og_address_line_1' => $line1,
                    'og_address_line_2' => $line2,
                    'og_city'           => $city,
                    'og_state'          => $state,
                    'og_zipcode'        => $zipcode,
                ]
            )->first();

        if (is_null($address) || Carbon::parse($address->created)->isAfter(Carbon::parse($address->created)->subSeconds(10))) {
            $uspsAddress = Usps::validate(
                trim($line1.' '.$line2),
                $city,
                $state,
                $zipcode
            );

            if (is_array($uspsAddress) && array_key_exists('address', $uspsAddress)) {
                $uspsAddress = $uspsAddress['address'];

                $address = new UspsAddresses();
                $address->og_address_line_1 = $line1;
                $address->og_address_line_2 = $line2;
                $address->og_city = $city;
                $address->og_state = $state;
                $address->og_zipcode = $zipcode;

                $address = $this->addUspsAddress($address, $uspsAddress);

                $address->save();
            }
        }

        return response()->json(
            [
                'address' => $address
            ]
        );
    }

    private function addUspsAddress($address, $uspsAddress) {
        if (array_key_exists('Address1', $uspsAddress)) {
            $address->new_address_line_1 = $uspsAddress['Address2'];
        }

        if (array_key_exists('Address2', $uspsAddress)) {
            $address->new_address_line_2 = $uspsAddress['Address2'];
        }

        if (array_key_exists('city', $uspsAddress)) {
            $address->new_city = $uspsAddress['City'];
        }

        if (array_key_exists('State', $uspsAddress)) {
            $address->new_state = $uspsAddress['State'];
        }

        if (array_key_exists('Zip5', $uspsAddress)) {
            $address->new_zipcode = $uspsAddress['Zip5'];
        }

        if (array_key_exists('Zip4', $uspsAddress)) {
            $address->new_zipcode_suffix = $uspsAddress['Zip4'];
        }

        if (array_key_exists('ReturnText', $uspsAddress)) {
            $address->return_text = $uspsAddress['ReturnText'];
        }

        if (array_key_exists('@attributes', $uspsAddress)) {
            $address->attributes = json_encode($uspsAddress['@attributes']);
        }

        return $address;
    }
}
