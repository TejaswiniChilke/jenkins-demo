<?php
namespace Uab\Controllers;

use Cache;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Uab\Http\Enums\UserStatusesEnum;
use Uab\Http\Enums\UserTypesEnum;
use Uab\Http\Models\AnalyticTrackCalls;
use Uab\Http\Models\LoginAttempts;
use Uab\Http\Models\Users;
use Uab\Providers\AnalyticServiceProvider;
use Uab\Providers\MatchingServiceProvider;
use Uab\Providers\PlaylistServiceProvider;
use Uab\Providers\QueryServiceProvider;
use Uab\Providers\UsersServiceProvider;

class ReportController {
    public function index() {
        return view('charts.create');
    }

    public function getReport(Request $request, $name) {
        return Cache::remember(
            $name,
            0,
            function() use ($request, $name) {
                switch (strtolower($name)) {
                    case 'call_count':
                        return $this->handleCallCount($request);
                    case 'login_attempts':
                        return $this->handleLoginReport($request);
                    case 'login_adherence':
                        return $this->handleLoginAdherence($request);
                    case 'playlist_adherence':
                        return $this->handlePlaylistAdherence($request);
                    case 'status_count':
                        return $this->handleStatusCount($request);
                    case 'user_stages':
                        return $this->handleUserStages($request);
                    default:
                        return response()->error(
                            'Unknown report name (' . $name . ').'
                        );
                }
            }
        );
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function handleLoginReport($request) {
        $response = [];

        $days = $request->input('days', 7);

        for ($i = $days - 1; $i >= 0; $i--) {
            $date = Carbon::now()->subDays($i);
            $formattedDate = $date->format('Y-m-d');

            $series = [];

            /** @var Builder $query */
            $query = LoginAttempts::query()
                ->where('attempted_at', 'LIKE', $formattedDate . '%');

            $totalCount = $query->count();
            $successCount = $query->where('success', '=', true)->count();

            $series[] = [
                'name' => 'Successes',
                'value' => $successCount
            ];

            $series[] = [
                'name' => 'Failures',
                'value' => $totalCount - $successCount
            ];

            $response[] = [
                'name'   => $formattedDate,
                'series' => $series
            ];
        }

        return response()->json($response);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function handleLoginAdherence($request) {
        $returnData = [];

        $userService = new UsersServiceProvider();
        $users = $userService->getUsersByUserTypeNames(
            [
                UserTypesEnum::CONTROL,
                UserTypesEnum::M2M,
                UserTypesEnum::SET
            ]
        );

        $users = $users->where('status', '=', UserStatusesEnum::PROFILE_COMPLETE)
            ->sortByDesc('start_time');

        foreach ($users as $user) {
            /** @var Users $user */
            $data = [
                'user' => $user,
                'logins_per_week' => []
            ];

            $weeks = config('app.weeks');
            $i = 0;

            $startTime = Carbon::parse($user->start_time)->startOfDay();
            $endTime = $startTime->clone()->addDays(7);

            do {
                $i++;

                $query = LoginAttempts::query()
                    ->where(
                        [
                            'success' => true,
                            'user_id' => $user->id
                        ]
                    )->whereBetween(
                        'attempted_at',
                        [
                            $startTime,
                            $endTime
                        ]
                    );

                $count = $query->count();

                $data['logins_per_week'][] = $count;

                $startTime = $startTime->addDays(7);
                $endTime = $endTime->addDays(7);
            } while (Carbon::today()->isAfter($startTime) && $i < $weeks);

            $returnData[] = $data;
        }

        return response()->json(array_values($returnData));
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function handlePlaylistAdherence($request) {
        $analyticService = new AnalyticServiceProvider();
        $matchingService = new MatchingServiceProvider();
        $queryService = new QueryServiceProvider();
        $playlistService = new PlaylistServiceProvider();

        $compliances = [];

        $analytics = [];

        /** @var Users[] $users */
        $userService = new UsersServiceProvider();
        $users = $userService->getUsersByUserTypeNames(
            [
                UserTypesEnum::M2M,
                UserTypesEnum::SET
            ]
        );

        foreach ($users as $user) {
            $compliances[$user->id] = [
                'user' => $user
            ];

            $currentWeek = $user->getWeek();

            $analytics[$user->id] = [];

            for ($week = 1; $week <= 16; $week++) {
                $analytics[$user->id][$week] = [];

                $complianceWatchTime = 0;
                $complianceTotalTime = 0;

                if (!is_null($currentWeek) && $week <= $currentWeek) {
                    /** @var Builder $query */
                    $query = $matchingService->getMatchingQuery(
                        'playlists',
                        [],
                        $user->id
                    );

                    $query = $query->where('end_week', '>=', $week);
                    $query = $query->where('week', '<=', $week);

                    $playlists = $queryService->parseGet('playlists', $request->toArray(), $query);

                    $analytics[$user->id][$week]['videos'] = [];

                    foreach ($playlists as $playlist) {
                        $videos = $playlistService->getVideos($playlist['id']);

                        $watchTimes = 0;
                        $totalTimes = 0;

                        foreach ($videos as $video) {
                            $analytics = $analyticService->getWatches($user, $week, $video);

                            $playTimes = 0;
                            $stopTimes = 0;

                            foreach ($analytics as $analytic) {
                                $playTimes += $analytic['play_time'];
                                $stopTimes += $analytic['stop_time'];
                            }

                            $analytic['watch_time'] = $stopTimes - $playTimes;
                            $watchTimes += $analytic['watch_time'];

                            $analytics[$user->id][$week]['videos'][] = [
                                'analytics' => $analytic,
                                'video' => $video
                            ];

                            $totalTimes += $video->seconds;
                        }

                        $playlist['watch_time'] = $watchTimes;
                        $playlist['total_time'] = $totalTimes;

                        $complianceWatchTime += $playlist['watch_time'];
                        $complianceTotalTime += $playlist['total_time'];

                        $analytics[$user->id][$week]['playlists'][] = $playlist;
                    }

                    $compliances[$user->id]['week_' . $week] = [
                        'adherence_percent' => null,
                        'total_time' => 0,
                        'watch_time' => 0
                    ];

                    $compliances[$user->id]['week_' . $week]['watch_time'] = $complianceWatchTime;
                    $compliances[$user->id]['week_' . $week]['total_time'] = $complianceTotalTime;

                    if ($complianceTotalTime === 0) {
                        $compliances[$user->id]['week_' . $week]['adherence_percent'] = 0;
                    } else {
                        $compliances[$user->id]['week_' . $week]['adherence_percent'] = $complianceWatchTime / $complianceTotalTime * 100;
                    }
                }
            }
        }

        $returnData =  array_values($compliances);

        return response()->json($returnData);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function handleStatusCount($request) {
        /** @var Builder $query */
        $query = Users::query()->groupBy('status');

        $status = $request->input('status', null);
        if (!is_null($status)) {
            if (!is_array($status)) {
                $status = explode(',', $status);
            }

            $query->whereIn('status', $status);
        }

        $groups = $query->get();

        $results = [];

        foreach ($groups as $group) {
            $results[] =
                [
                    'status' => $group->getAttributeValue('status'),
                    'count' => $group->count()
                ];
        }

        return response()->json($results);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function handleUserStages($request) {
        $results = DB::table('users as u')->select(
            [
                'u.status as name',
                DB::raw('COUNT(u.id) as value')
            ]
        )->join(
            'user_types as t',
            't.id',
            '=',
            'u.user_type_id'
        )->where(
            't.name',
            '!=',
            UserTypesEnum::ADMIN
        )->where(
            't.name',
            '!=',
            UserTypesEnum::SUPER_ADMIN
        )->groupBy(
            [
                'u.status'
            ]
        )->get();

        return response()->json(
            [
                'stages' => $results
            ]
        );
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function handleCallCount($request) {
        $pastWeekCallData = [];
        $today = Carbon::today();
        for ($days = 0; $days <= 7; $days++) {
            $referenceDate = $today->sub(strval($days). ' days');
            $furtherReferenceDate = $referenceDate->add('1 day');
            $formattedFurtherReferenceDate = $furtherReferenceDate->toDateString();
            $formattedReferenceDate = $referenceDate->toDateString();
            $completedCalls = AnalyticTrackCalls::query()
                ->where(
                    [
                        'status'   => 'completed'
                    ]
                )->where('created', '>', $formattedReferenceDate)
                ->where('created', '<', $formattedFurtherReferenceDate)->get();

            $completedCallCount =  $completedCalls->count();

            $missedCalls = AnalyticTrackCalls::query()
                ->where(
                    [
                        'status'   => 'missed'
                    ]
                )->where('created', '>', $formattedReferenceDate)
                ->where('created', '<', $formattedFurtherReferenceDate)->get();

            $missedCallCount =  $missedCalls->count();

            $errorCalls = AnalyticTrackCalls::query()
                ->where(
                    [
                        'status'   => 'error'
                    ]
                )->where('created', '>', $formattedReferenceDate)
                ->where('created', '<', $formattedFurtherReferenceDate)->get();

            $errorCallCount =  $errorCalls->count();

            $disconnectedCalls = AnalyticTrackCalls::query()
                ->where(
                    [
                        'status'   => 'disconnected'
                    ]
                )->where('created', '>', $formattedReferenceDate)
                ->where('created', '<', $formattedFurtherReferenceDate)
                ->get();

            $disconnectedCallCount =  $disconnectedCalls->count();
            $inCompleteCallCount = $missedCallCount + $errorCallCount + $disconnectedCallCount;

            $pastWeekCallData[] = [
                'date'                   =>   $referenceDate,
                'complete_call_count'    =>   $completedCallCount,
                'incomplete_call_count'  =>   $inCompleteCallCount
            ];
        }

        return response()->json($pastWeekCallData);
    }
}
