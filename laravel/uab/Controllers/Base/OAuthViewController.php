<?php
namespace Uab\Controllers\Base;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use League\OAuth2\Server\Exception\OAuthServerException;
use Psr\Http\Message\ServerRequestInterface;
use Uab\Exceptions\UserNotApprovedException;
use Uab\Exceptions\UserRequiresApprovalException;
use Uab\Exceptions\UserTypeMissingException;
use \Exception;
use Uab\Providers\Auth;
use Uab\Providers\OAuthServiceProvider;

class OAuthViewController extends OAuthController {
    private $errors = [];

    public function loginView() {
        $user = Auth::user();

        if (is_null($user)) {
            return view('auth.login')->withErrors($this->errors);
        } else {
            return view('home');
        }
    }

    public function login(ServerRequestInterface $request) {
        $body = $request->getParsedBody();

        $email = Arr::get($body, 'email', null);
        if (is_null($email)) {
            $this->errors = [
                'email' => 'Email address is required.'
            ];

            return $this->loginView();
        }

        $password = Arr::get($body, 'password', null);
        if (is_null($password)) {
            $this->errors = [
                'password' => 'Password is required.'
            ];

            return $this->loginView();
        }

        try {
            $service = new OAuthServiceProvider();

            $activeUser = $service->findActiveUser(
                $email,
                [
                    'email'
                ]
            );

            $token = $this->getToken($request, $email);

            $activeUser = $this->buildActiveUser($activeUser);

            return response(
                [
                    'active_user' => $activeUser,
                    'token'       => $token
                ]
            );
        } catch (ModelNotFoundException $e) {
            return view('home')->withErrors(
                [
                    'User not found.',
                ]
            );
        } catch (OAuthServerException $e) {
            return view('home')->withErrors(
                [
                    'OAuthServerException: '.$e->getMessage(),
                ]
            );
        } catch (UserNotApprovedException $e) {
            return view('home')->withErrors(
                [
                    'This user is not approved to login.',
                ]
            );
        } catch (UserRequiresApprovalException $e) {
            return view('home')->withErrors(
                [
                    'This user requires approval before logging in.',
                ]
            );
        } catch (UserTypeMissingException $e) {
            return view('home')->withErrors(
                [
                    'This user must have a user type in order to login.',
                ]
            );
        } catch (Exception $e) {
            view('home')->withErrors(
                [
                    'Unhandled exception: '.$e->getMessage(),
                ]
            );
        }

        return view('home');
    }

    public function logout(Request $request) {
        $oauthService = new OAuthServiceProvider();
        $oauthService->logout();

        return view('auth.login')->withErrors($this->errors);
    }
}
