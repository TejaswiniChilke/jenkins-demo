<?php
namespace Uab\Controllers\Base;

use Illuminate\Http\Request;
use Uab\Http\Models\Users;
use Uab\Providers\Auth;
use Uab\Providers\ModelServiceProvider;
use Uab\Providers\QueryServiceProvider;
use View;
use \Exception;
use Yajra\Datatables\Datatables;

class ViewController {
    public $errors;
    public $modelService;

    public function __construct() {
        $this->modelService = new ModelServiceProvider();
    }

    public function datatable(Request $request, $model) {
        $model = $this->modelService->getModel($model);

        $query = $model::query();

        try {
            $table = DataTables::of($query);

            return $table->make(true);
        } catch (Exception $e) {
            return response()->error('Something went wrong.');
        }
    }

    public function route(Request $request, $model, $action = false, $id = false) {
        $model = strtolower($model);

        $activeUser = Auth::user();
        if (is_null($activeUser) || $model === 'login') {
            return view('auth.login')->withErrors($this->errors);
        }

        if ($action === false) {
            if ($action === 'export') {
                return $this->exportPage($request);
            }

            return view($model)->withErrors($this->errors);
        }

        $action = $action.'AdminView';

        if ($id) {
            return $this->$action($id, $model);
        } else {
            return $this->$action($model);
        }
    }

    public function postRoute(Request $request, $model, $action = false, $id = false) {
        if ($request->input('gender', 'female') === 'male') {
            $gender = 1;
        } else {
            $gender = 0;
        }

        $user = new Users();
        $user->type_diabetes = $request->input('type_diabetes', null);
        $user->mobile_number = $request->input('mobile_number', null);
        $user->pincode = sprintf("%04d", rand(0, 9999));
        $user->gender = $gender;
        $user->save();

        return $this->route($request, 'users', 'index');
    }

    public function indexAdminView($model) {
        $model = $this->modelService->getModel($model);

        $all = $model::query()->select()->get();

        return $this->_createView('index', $model)->with('objects', $all);
    }

    public function createAdminView($model) {
        return $this->_createView('create', $model);
    }

    public function showAdminView($model, $id) {
        $queryService = new QueryServiceProvider();

        $object = $queryService->getById($id, $model);

        return $this->_createView('show', $model)->with('object', $object);
    }

    public function editAdminView($id, $model) {
        $queryService = new QueryServiceProvider();

        $object = $queryService->getById($id, $model);

        return $this->_createView('edit', $model)->with('object', $object);
    }

    private function _createView($view, $model) {
        $model = $this->modelService->getModel($model);

        $table = $model->getTable();

        return View::make($table.'.'.$view)->with('errors', collect());
    }
}
