<?php
namespace Uab\Controllers\Base;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Psr\Http\Message\ServerRequestInterface;
use Uab\Exceptions\UserNotApprovedException;
use Uab\Exceptions\UserRequiresApprovalException;
use Uab\Exceptions\UserTypeMissingException;
use Uab\Http\Enums\HttpResponseCodesEnum;
use Uab\Http\Models\Emails;
use Uab\Http\Models\Users;
use Uab\Providers\Auth;
use Uab\Providers\OAuthServiceProvider;
use League\OAuth2\Server\Exception\OAuthServerException;
use \Laravel\Passport\Http\Controllers\AccessTokenController;
use \Exception;
use URL;

class OAuthController extends AccessTokenController {
    /**
     * @param ServerRequestInterface $request
     *
     * @return ServerRequestInterface
     */
    private function decodePassword(ServerRequestInterface $request):ServerRequestInterface {
        $body = $request->getParsedBody();
        $body['password'] = base64_decode($body['password']);

        return $request->withParsedBody($body);
    }

    public function checkRegistry(Request $request, $username = null) {
        if (is_null($username)) {
            $username = $request->input('username', null);
            if (is_null($username)) {
                $username = $request->input('email', null);
            }
        }

        try {
            $oauthService = new OAuthServiceProvider();

            $user = $oauthService->findActiveUser(
                $username,
                [
                    'email'
                ]
            );

            return response(
                [
                    'url' => config('app.frontend') . '/#/login;username=' . $user->email
                ]
            );
        } catch (Exception $e) {
            return response(
                'Not allowed.',
                HttpResponseCodesEnum::CLIENT_ERROR_UNAUTHORIZED
            );
        }
    }

    /**
     * @param ServerRequestInterface $request
     * @param string $username
     *
     * @return array
     *
     * @throws OAuthServerException
     */
    protected function getToken(ServerRequestInterface $request, string $username):array {
        $request = $this->decodePassword($request);

        $tokenResponse = parent::issueToken($request);

        $token = $tokenResponse->getContent();
        $token = json_decode($token, true);

        $service = new OAuthServiceProvider();

        if (is_array($token) && array_key_exists('error', $token)) {
            $service->recordLoginAttempt($username, false);

            throw new OAuthServerException(
                'Invalid credentials.',
                6,
                'invalid_credentials',
                401
            );
        }

        $service->recordLoginAttempt($username, true);

        return $token;
    }

    /**
     * @param ServerRequestInterface $request
     *
     * @return Response
     */
    public function login(ServerRequestInterface $request):Response {
        $body = $request->getParsedBody();

        $username = null;
        if (array_key_exists('username', $body)) {
            $username = $body['username'];
        } else if (array_key_exists('email', $body)) {
            $username = $body['email'];
        }

        try {
            $service = new OAuthServiceProvider();

            $activeUser = $service->findActiveUser(
                $username,
                [
                    'username',
                    'email'
                ]
            );

            $token = $this->getToken($request, $username);

            $activeUser = $this->buildActiveUser($activeUser);

            return response(
                [
                    'active_user' => $activeUser,
                    'token'       => $token
                ]
            );
        } catch (ModelNotFoundException $e) {
            return response(
                [
                    'error' => 'User not found.',
                ],
                HttpResponseCodesEnum::CLIENT_ERROR_FORBIDDEN
            );
        } catch (OAuthServerException $e) {
            return response(
                [
                    'error' => 'Wrong username or password. Try again or click FORGOT PASSWORD to reset it.',
                ],
                HttpResponseCodesEnum::CLIENT_ERROR_FORBIDDEN
            );
        } catch (UserNotApprovedException $e) {
            return response(
                [
                    'error' => 'This user is not approved to login.'
                ],
                HttpResponseCodesEnum::CLIENT_ERROR_FORBIDDEN
            );
        } catch (UserRequiresApprovalException $e) {
            return response(

                [
                    'error' => 'This user requires approval before logging in.'
                ],
                HttpResponseCodesEnum::CLIENT_ERROR_FORBIDDEN
            );
        } catch (UserTypeMissingException $e) {
            return response(
                [
                    'This user must have a user type in order to login.',
                ],
                HttpResponseCodesEnum::CLIENT_ERROR_FORBIDDEN
            );
        } catch (Exception $e) {
            return response()->error(
                'Unhandled exception: '.$e->getMessage(),
                HttpResponseCodesEnum::SERVER_ERROR_INTERNAL_SERVER_ERROR
            );
        }
    }

    /**
     * @return Response
     */
    public function logout():Response {
        $user = Auth::user();

        if (is_null($user)) {
            return response('Already logged out.', HttpResponseCodesEnum::CLIENT_ERROR_UNAUTHORIZED);
        } else {
            $service = new OAuthServiceProvider();
            $service->logout($user);

            return response('Logged out.', HttpResponseCodesEnum::SUCCESS_OK);
        }
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function getSecurityQuestion(Request $request):Response {
        $username = $request->input('username', null);
        if (is_null($username)) {
            $username = $request->input('email', null);
            if (is_null($username)) {
                return response(
                    'Username or email is required.',
                    HttpResponseCodesEnum::CLIENT_ERROR_BAD_REQUEST
                );
            }
        }

        /** @var Users $user */
        $user = Users::query()->where('username', '=', $username)->first();
        if (is_null($user)) {
            $user = Users::query()->where('email', '=', $username)->first();
        }

        if (!is_null($user)) {
            $question = $user->security_question;

            if (!is_null($question) && strlen($question) !== 0) {
                return response(
                    [
                        'security_questions' => $question
                    ]
                );
            } else {
                return response(
                    'User does not have a security question set.',
                    HttpResponseCodesEnum::CLIENT_ERROR_GONE
                );
            }
        } else {
            return response(
                'User not found.',
                HttpResponseCodesEnum::CLIENT_ERROR_BAD_REQUEST
            );
        }
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function updatePassword(Request $request):Response {
        try {
            $username = $request->input('username');
            $securityAnswer = $request->input('security_question_answer');
            $updatedPassword = $request->input('password');

            if (!empty($securityAnswer)) {
                /** @var Users $user */
                $user = Users::query()->where('username', '=', $username)->first();
                if (is_null($user)) {
                    $user = Users::query()->where('email', '=', $username)->first();
                }

                if (is_null($user)) {
                    return response('Could not verify username.', HttpResponseCodesEnum::CLIENT_ERROR_UNAUTHORIZED);
                }

                if (strcasecmp($user->security_answer, $securityAnswer) === 0) {
                    $user->setAttribute('password', $updatedPassword);
                    if ($user->save()) {
                        return response(['message' => 'Password Updated'], 200);
                    } else {
                        return response(['message' => 'Password Update Failed'], 200);
                    }

                } else {
                    return response(['message' => 'Security answer does not match'], 401);
                }
            } else {
                return response(['message' => 'Security answer cannot be empty.'], 500);
            }
        } catch (ModelNotFoundException $e) { // email notfound
            return response(['message' => 'User not found'], 500);
        }
    }

    /**
     * @param ServerRequestInterface $request
     * @param string|null $email
     *
     * @return Response
     */
    public function emailExists(ServerRequestInterface $request, ?string $email = null):Response {
        if (is_null($email)) {
            $email = $request->getParsedBody()['email'];
        }

        $exists = Users::query()
            ->where(
                [
                    'email' => $email
                ]
            )->exists();

        return response(
            [
                'exists' => $exists
            ],
            HttpResponseCodesEnum::SUCCESS_OK
        );
    }

    /**
     * @return Response
     */
    public function loadActiveUser():Response {
        $activeUser = Auth::user();

        if (is_null($activeUser)) {
            return response('Cannot find the active user.');
        } else {
            $activeUser = $this->buildActiveUser($activeUser);
        }

        return response(
            [
                'active_user' => $activeUser
            ]
        );
    }

    /**
     * @param Users $activeUser
     *
     * @return mixed[]
     */
    protected function buildActiveUser(Users  $activeUser):array {
        $returnVar = $activeUser->toArray();
        $returnVar['id'] = $activeUser->slug();

        $image = $activeUser->image();
        if (!is_null($image)) {
            $slug = $image->slug();

            $image = $image->toArray();
            $image['id'] = $slug;
        }

        $returnVar['images'] = $image;

        $state = $activeUser->state();
        if (!is_null($state)) {
            $slug = $state->slug();

            $state = $state->toArray();
            $state['id'] = $slug;
        }

        $returnVar['states'] = $state;

        $userType = $activeUser->userType();
        if (!is_null($userType)) {
            $slug = $userType->slug();

            $userType = $userType->toArray();
            $userType['id'] = $slug;
        }

        $returnVar['user_types'] = $userType;

        return $returnVar;
    }
}
