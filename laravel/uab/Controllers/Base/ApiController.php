<?php
namespace Uab\Controllers\Base;

use Uab\Controllers\Base\Traits\CommentTrait;
use Uab\Controllers\Base\Traits\CopyTrait;
use Uab\Controllers\Base\Traits\CreateTrait;
use Uab\Controllers\Base\Traits\DatatableTrait;
use Uab\Controllers\Base\Traits\DeleteTrait;
use Uab\Controllers\Base\Traits\DistanceTrait;
use Uab\Controllers\Base\Traits\EditTrait;
use Uab\Controllers\Base\Traits\ExportTrait;
use Uab\Controllers\Base\Traits\GetTrait;
use Uab\Controllers\Base\Traits\LikeTrait;
use Uab\Controllers\Base\Traits\MatchingTrait;
use Uab\Controllers\Base\Traits\NotifyTrait;
use Uab\Controllers\Base\Traits\QueueCrudTrait;
use Uab\Controllers\Base\Traits\SimilarityTrait;
use Uab\Controllers\Base\Traits\TagTrait;
use Uab\Providers\ModelServiceProvider;
use Illuminate\Routing\Controller;
use Uab\Providers\QueryServiceProvider;
use Uab\Providers\RequestServiceProvider;
use Uab\Providers\UpdateServiceProvider;

class ApiController extends Controller {
    use CommentTrait;
    use CopyTrait;
    use CreateTrait;
    use DatatableTrait;
    use DeleteTrait;
    use DistanceTrait;
    use EditTrait;
    use ExportTrait;
    use GetTrait;
    use LikeTrait;
    use MatchingTrait;
    use NotifyTrait;
    use QueueCrudTrait;
    use SimilarityTrait;
    use TagTrait;

    /** @var ModelServiceProvider $modelService */
    protected $modelService;

    /** @var QueryServiceProvider $queryService*/
    protected $queryService;

    /** @var RequestServiceProvider $requestService */
    protected $requestService;

    /** @var UpdateServiceProvider $updateService */
    protected $updateService;

    public function __construct() {
        $this->modelService = new ModelServiceProvider();
        $this->queryService = new QueryServiceProvider();
        $this->requestService = new RequestServiceProvider();
        $this->updateService = new UpdateServiceProvider();
    }
}
