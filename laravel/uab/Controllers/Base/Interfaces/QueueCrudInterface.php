<?php
namespace Uab\Controllers\Base\Interfaces;

interface QueueCrudInterface {
    public function shouldQueueCreate();
}
