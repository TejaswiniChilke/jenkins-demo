<?php
namespace Uab\Controllers\Base\Traits;

use Cache;
use Uab\Providers\ModelServiceProvider;

trait DatatableTrait {
    public function columns($table) {
        $modelService = new ModelServiceProvider();

        $columns = Cache::remember(
            $table.'-data-table-columns',
            60 * 24,
            function() use ($table, $modelService) {
                /** @var \Uab\Http\Models\Traits\DataTableTrait $model */
                $model = $modelService->getModel($table);

                return $model->getDataTableColumns();
            }
        );

        return response(
            [
                $table          => $columns,
                'count'         => count($columns),
                'can_load_more' => false
            ]
        );
    }
}
