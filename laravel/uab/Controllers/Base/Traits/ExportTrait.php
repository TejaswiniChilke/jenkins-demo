<?php
namespace Uab\Controllers\Base\Traits;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Uab\Http\Enums\QueueTypesEnum;
use Uab\Listeners\Excel\EmailCollectionExport;
use Uab\Providers\Auth;
use Uab\Providers\QueryServiceProvider;

trait ExportTrait {
    /**
     * @param Request     $request
     * @param string      $table
     * @param null|string $id
     *
     * @return JsonResponse
     */
    public function export(Request $request, $table, $id = null) {
        $queryService = new QueryServiceProvider();
        $query = $queryService->buildRequestQuery($table, $request->toArray(), $id);

        if (!$request->has('order')) {
            $query->orderBy('id');
        }

        $format = $request->input('format', 'xlsx');

        $user = Auth::user();

        if (!is_null($user)) {
            EmailCollectionExport::dispatch($query->get(), $user->email, $format)
                ->onQueue(QueueTypesEnum::LOW);
        } else {
            return response()->json(
                [
                    'error' => 'You must be logged in to request a report.'
                ]
            );
        }

        return response()->json(
            [
                'success' => true
            ]
        );
    }
}
