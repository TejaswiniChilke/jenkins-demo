<?php
namespace Uab\Controllers\Base\Traits;

use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Uab\Http\Enums\HttpResponseCodesEnum;
use Uab\Providers\QueryServiceProvider;
use Uab\Providers\RequestServiceProvider;

trait GetTrait {
    /**
     * @param Request $request
     * @param string  $table
     *
     * @return ResponseFactory
     */
    public function count(Request $request, $table) {
        $queryService = new QueryServiceProvider();

        $count = $queryService->count($table, $request->toArray());

        return response(
            [
                'count' => $count
            ]
        );
    }

    /**
     * @param Request $request
     * @param string  $table
     *
     * @return ResponseFactory
     */
    public function exists(Request $request, $table) {
        $queryService = new QueryServiceProvider();

        $exists = $queryService->exists($table, $request->toArray());

        return response(
            [
                'exists' => $exists
            ]
        );
    }

    /**
     * @param Request     $request
     * @param string      $table
     * @param string|null $id
     *
     * @return ResponseFactory
     */
    public function get(Request $request, $table, $id = null) {
        $requestService = new RequestServiceProvider();
        $queryService = new QueryServiceProvider();

        $fields = $request->toArray();

        $query = $queryService->buildRequestQuery($table, $fields, $id);

        $results = $queryService->parseGet($table, $fields, $query);
        $results = $queryService->slugIds($table, $results);

        $page = $requestService->getPage($request->toArray());
        if ($page) {
            $response = $queryService->getPaginationResponse($fields, $table, $query, $results);
        } else {
            $response = [
                $table => $results
            ];
        }

        if (Arr::has($fields, 'showQuery')) {
            $response['query'] = $query->toSql();
        }

        $statusCode = HttpResponseCodesEnum::SUCCESS_OK;
        if (!is_null($id)) {
            if (count($response[$table]) === 0) {
                $statusCode = HttpResponseCodesEnum::CLIENT_ERROR_FOUND;

                $response['error'] = Str::title($table) . ' not found.';
            }
        }

        return response($response, $statusCode);
    }
}
