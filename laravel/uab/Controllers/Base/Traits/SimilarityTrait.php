<?php
namespace Uab\Controllers\Base\Traits;

use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Uab\Providers\SimilarityServiceProvider;

trait SimilarityTrait {
    /**
     * @param Request  $request
     * @param string   $table
     * @param mixed[]  $object
     *
     * @return boolean
     */
    public function hasSimilar($request, $table, $object) {
        $similarityProvider = new SimilarityServiceProvider();

        $minimum = $request->input('minimum', 0);

        $results = $similarityProvider->getSimilar($table, $object, $minimum);

        return count($results) !== 0;
    }

    /**
     * @param Request     $request
     * @param string      $table
     * @param mixed[]     $results
     *
     * @return mixed[]
     */
    public function includeSimilarRecords($request, $table, $results) {
        $similarityProvider = new SimilarityServiceProvider();

        $minimum = $request->input('minimum', 0);

        foreach ($results[$table] as $id => $result) {
            $results[$table][$id]['similar'] = $similarityProvider->getSimilar(
                $table,
                $result,
                $minimum
            );
        }

        return $results;
    }

    //  TODO: Extract to similar service
    /**
     * @param Request     $request
     * @param string      $table
     * @param number|null $id
     *
     * @return ResponseFactory
     */
    public function similar(Request $request, $table, $id = null) {
        $query = $this->queryService->buildRequestQuery($table, $request->toArray(), $id);

        $results = $this->queryService->parseGet($table, $request->toArray(), $query);

        $results = $this->includeSimilarRecords($request, $table, $results);

        return response($results);
    }

    //  TODO: Extract to similar service
    /**
     * @param Request     $request
     * @param string      $table
     * @param number|null $id
     *
     * @return ResponseFactory
     */
    public function similarExists(Request $request, $table, $id) {
        $query = $this->queryService->buildRequestQuery($table, $request->toArray(), $id);
        $query = $query->limit(1);

        $results = $this->queryService->parseGet($table, $request->toArray(), $query);

        $hasSimilar = false;

        if (array_key_exists($table, $results)) {
            $objects = $results[$table];

            if (count($objects) !== 0) {
                $object = $objects[0];

                $hasSimilar = $this->hasSimilar($request, $table, $object);
            }
        }

        return response(
            [
                'has_similar' => $hasSimilar
            ]
        );
    }
}
