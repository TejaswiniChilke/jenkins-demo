<?php
namespace Uab\Controllers\Base\Traits;

use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Uab\Http\Enums\HttpResponseCodesEnum;
use Uab\Providers\UpdateServiceProvider;
use Exception;

trait DeleteTrait {
    /**
     * @param Request $request
     * @param string $table
     * @param string|null $id
     *
     * @return Response
     *
     * @throws Exception
     */
    public function delete(Request $request, string $table, ?string $id = null):Response {
        $updateService = new UpdateServiceProvider();

        $id = $request->input('id', $id);

        if (!is_null($id)) {
            try {
                $deletedCount = $updateService->delete($table, $id);
            } catch (Exception $e) {
                $message = $e->getMessage();

                if (Str::contains($message, 'a foreign key constraint fails')) {
                    $pre = '`.`';
                    $post = '`, CONSTRAINT `';

                    $preIndex = strpos($message, $pre) + Str::length($pre);
                    $length = strpos($message, $post) - $preIndex;

                    $foreignTable = Str::title(Str::substr($message, $preIndex, $length));

                    return response(
                        [
                            'error' => 'Cannot delete ' . $table . '. Must first delete related "' . $foreignTable . '".',
                            'pre' => strpos($message, $pre) + Str::length($pre),
                            'length' => $length
                        ],
                        HttpResponseCodesEnum::CLIENT_ERROR_CONFLICT
                    );
                } else {
                    return response(
                        [
                            'error' => 'Error while deleting ' . $table . '.'
                        ],
                        HttpResponseCodesEnum::SERVER_ERROR_INTERNAL_SERVER_ERROR
                    );
                }
            }

            return response(
                [
                    'deleted' => $deletedCount
                ],
                HttpResponseCodesEnum::SUCCESS_NO_CONTENT
            );

        } else {
            return response(
                [
                    'At least one ID is required for action (delete).'
                ],
                HttpResponseCodesEnum::CLIENT_ERROR_BAD_REQUEST
            );
        }
    }
}
