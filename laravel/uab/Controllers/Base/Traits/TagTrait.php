<?php
namespace Uab\Controllers\Base\Traits;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Uab\Http\Models\Tags;
use Uab\Providers\ModelServiceProvider;
use Uab\Providers\QueryServiceProvider;

trait TagTrait {
    /**
     * @param Request     $request
     * @param string      $table
     * @param string|null $id
     *
     * @return JsonResponse
     */
    public function getByTag(Request $request, $table, $id = null) {
        $modelService = new ModelServiceProvider();
        $queryService = new QueryServiceProvider();

        $playlistTags = [];

        $fields = $request->toArray();

        $query = $queryService->buildRequestQuery($table, $fields, $id);
        $candidates = $queryService->parseGet($table, $fields, $query);

        foreach ($candidates as $candidate) {
            $manyToManyModel = $modelService->getManyToManyModel($table, 'tags');

            $manyToManyModels = $manyToManyModel::query()
                ->where(
                    [
                        $modelService->getForeignKey($table) => $candidate['id']
                    ]
                )->get();

            $tagIds = $manyToManyModels->pluck('tag_id');

            $tags = Tags::query()
                ->whereIn('id', $tagIds)
                ->get();

            foreach ($tags as $tag) {
                $tagArray = $tag->toArray();
                $queryService->slugIds('tags', $tagArray);
                if (!Arr::has($playlistTags, $tagArray['id'])) {
                    $playlistTags[$tagArray['id']] = [
                        'tag' => $tagArray,
                        'videos' => []
                    ];
                }

                array_push(
                    $playlistTags[$tagArray['id']]['videos'],
                    $queryService->slugIds($table, $candidate)
                );
            }
        }

        $playlistTags = array_values($playlistTags);

        return response()->json($playlistTags);
    }
}
