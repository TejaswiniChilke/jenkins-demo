<?php
namespace Uab\Controllers\Base\Traits;

use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Uab\Providers\ModelServiceProvider;
use Uab\Providers\QueryServiceProvider;

trait DistanceTrait {
    /**
     * @param Request $request
     * @param string  $table
     * @param string  $unit
     * @param integer $distance
     *
     * @return ResponseFactory
     */
    public function within(Request $request, $table, $unit, $distance) {
        $modelService = new ModelServiceProvider();
        $queryService = new QueryServiceProvider();

        $latitude = $request->input('latitude', false);
        $longitude = $request->input('longitude', false);

        // TODO
//        if (!$latitude || !$longitude) {
//            $city = $request->input('city', false);
//            if (!$city) {
//
//            }
//
//            $state = $request->input('state', false);
//            if (!$state) {
//
//            }
//
//            $zip = $request->input('zip', false);
//            if (!$zip) {
//
//            }
//        }

        $query = $queryService->buildRequestQuery($table, $request->toArray());

        if ($latitude && $longitude) {
            $query->selectRaw(
                '( ? * acos(cos(radians(?))
                            * cos(radians(?))
                            * cos(radians('.$modelService->getAlias($table).'.longitude) - radians(?))
                            + sin(radians(?))
                            * sin(radians('.$modelService->getAlias($table).'.latitude)))) AS distance',
                [
                    $unit == 'km' ? 6371 : 3959,
                    $latitude,
                    $latitude,
                    $longitude,
                    $latitude
                ]
            );
            $query->whereRaw(
                $modelService->getAlias($table).'.latitude BETWEEN ? - 3 AND ? + 3
                  AND '.$modelService->getAlias($table).'.longitude BETWEEN ? - 3 AND ? + 3',
                [
                    $latitude,
                    $latitude,
                    $longitude,
                    $longitude
                ]
            );
            $query->having('distance', '<', $distance);
            $query->orderBy('distance');
        }

        $results = $queryService->parseGet($table, $request->toArray(), $query);

        return response($results);
    }
}
