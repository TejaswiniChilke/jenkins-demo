<?php
namespace Uab\Controllers\Base\Traits;

use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Uab\Providers\UpdateServiceProvider;

trait EditTrait {
    /**
     * @param Request $request
     * @param string  $table
     * @param bool    $id
     *
     * @return ResponseFactory|JsonResponse
     */
    public function edit(Request $request, $table, $id = null) {
        $updateService = new UpdateServiceProvider();

        if (!is_null($id)) {
            $result = $updateService->edit($table, $request->toArray(), $id);

            return response()->json(
                [
                    $table => $result
                ]
            );
        } else {
            return response()->error('ID required for edit.');
        }
    }
}
