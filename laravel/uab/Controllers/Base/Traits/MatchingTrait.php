<?php
namespace Uab\Controllers\Base\Traits;

use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Uab\Http\Models\Base\BaseModel;
use Uab\Http\Models\Playlists;
use Uab\Providers\Auth;
use Uab\Providers\MatchingServiceProvider;
use Uab\Providers\ModelServiceProvider;
use Uab\Providers\QueryServiceProvider;

trait MatchingTrait {
    /**
     * @param Request     $request
     * @param string      $table
     * @param string|null $id
     *
     * @return JsonResponse|BaseModel
     */
    public function exactMatching(Request $request, string $table, $id = null) {
        $returnVar = $this->getExactModels($table, $request->toArray(), $id);

        return response()->json(
            [
                'can_load_more' => false,
                'count'         => count($returnVar),
                $table          => $returnVar
            ]
        );
    }

    public function getUserTags(Request $request, $id = null) {
        $id = $request->input('id', $id);
        if (is_null($id)) {
            $activeUser = Auth::user();
            if (is_null($activeUser)) {
                return response()->error(
                    'A user ID is required to get tags.'
                );
            } else {
                $id = $activeUser->id;
            }
        }

        $matchingService = new MatchingServiceProvider();

        $userTags = $matchingService->getUserTags($id);

        return response()->json(
            [
                'tags'  => $userTags,
                'count' => count($userTags)
            ]
        );
    }

    public function getExactModels($table, $fields, $id = null) {
        $modelService = new ModelServiceProvider();
        $queryService = new QueryServiceProvider();

        $returnVar = [];

        $alphaModel = $modelService->getModel($table);

        $matchingService = new MatchingServiceProvider();

        $query = $matchingService->getMatchingQuery($table, $fields, $id);

        $matching = $queryService->parseGet($table, $fields, $query);

        $userTags = $matchingService->getMatchingTags($table, $id);
        $userTagCount = count($userTags);

        /** @var BaseModel $manyModel */
        $manyModel = $modelService->getManyToManyModel($table, 'tags');

        foreach ($matching as $tableI => $tableArray) {
            /** @var Playlists $alphaObject */
            $tableArray['id'] = $alphaModel::safeDecodeStatic($tableArray['id']);

            $alphaObject = $alphaModel::query()->find($tableArray['id']);
            $alphaForeignKey = $modelService->getForeignKey($alphaObject);

            /** @var Builder $matchingTagQuery */
            $matchingTagQuery = $manyModel::query()->
                where(
                    [
                        'exclusive'      => false,
                        $alphaForeignKey => $alphaObject->id
                    ]
                );

            $matchingTagCount = $matchingTagQuery->count();

            if ($matchingTagCount === $userTagCount) {
                $matchingTags = $matchingTagQuery->get();

                $exactMatch = true;

                foreach ($userTags as $userTag) {
                    $hasPlaylistTag = false;

                    foreach ($matchingTags as $matchingTag) {
                        if (isset($userTag->id) && isset($matchingTag->tag_id) && $userTag->id === $matchingTag->tag_id) {
                            $hasPlaylistTag = true;
                            break;
                        }
                    }

                    if ($hasPlaylistTag === false) {
                        $exactMatch = false;
                        break;
                    }
                }

                if ($exactMatch) {
                    $object = $alphaObject->toArray();
                    $object['images'] = $alphaObject->image();
                    $object[$manyModel->getTable()] = $matchingTags;

                    $object = $queryService->slugIds($alphaObject, $object);

                    $returnVar[] = $object;
                }
            }
        }

        return $returnVar;
    }

    /**
     * @param Request $request
     * @param string  $table
     * @param mixed   $id
     *
     * @return ResponseFactory|\Illuminate\Http\Response
     */
    public function matching(Request $request, $table, $id = null) {
        $matchingService = new MatchingServiceProvider();

        $results = $matchingService->getMatching($table, $request->toArray(), $id);

        return response(
            [

                'can_load_more' => false,
                'count'         => count($results),
                $table          => $results
            ]
        );
    }
}
