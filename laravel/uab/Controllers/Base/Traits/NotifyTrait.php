<?php
namespace Uab\Controllers\Base\Traits;

use Illuminate\Http\Request;
use Uab\Http\Enums\HttpResponseCodesEnum;
use Uab\Http\Enums\QueueTypesEnum;
use Uab\Http\Models\Notifications;
use Uab\Jobs\Base\InsertObjectJob;
use Uab\Providers\UsersServiceProvider;

trait NotifyTrait {
    //    TODO: Jeremy - Move to different controller
    public function notify(Request $request, $userType) {
        if (!$request->has('message')) {
            return response()->error(
                'Field \'message\' is required to notify user_types.',
                HttpResponseCodesEnum::CLIENT_ERROR_BAD_REQUEST
            );
        }

        if (!$request->has('type')) {
            return response()->error(
                'Field \'type\' is required to notify user_types.',
                HttpResponseCodesEnum::CLIENT_ERROR_BAD_REQUEST
            );
        }

        $service = new UsersServiceProvider();

        $users = $service->getUsersByUserTypeNames($userType);

        foreach ($users as $user) {
            $notification = new Notifications();

            $notification->user_id = $user->id;

            $notification->message = $request->input('message');
            $notification->type = $request->input('type');

            if (!$request->has('reference_name')) {
                $notification->reference_name = $request->input('reference_name');
            }

            if (!$request->has('reference_value')) {
                $notification->reference_value = $request->input('reference_value');
            }

            InsertObjectJob::dispatch($notification)->onQueue(QueueTypesEnum::NORMAL);
        }

        return response(
            [
                'notifications_sent' => count($users)
            ]
        );
    }
}
