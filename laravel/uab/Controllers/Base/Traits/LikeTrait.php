<?php
namespace Uab\Controllers\Base\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Uab\Providers\Auth;
use Uab\Providers\ModelServiceProvider;
use Uab\Providers\QueryServiceProvider;

trait LikeTrait {
    // TODO: Extract to update service
    public function like(Request $request, $parentTable, $parentSlug) {
        $modelService = new ModelServiceProvider();
        $queryService = new QueryServiceProvider();

        $user = Auth::user();
        if (is_null($user)) {
            return response()->error('Must be logged in to like '.$parentTable.'.');
        }

        $table = Str::singular($parentTable).'_likes';

        $model = $modelService->getModel($table);

        if (!is_null($model)) {
            $parentModel = $modelService->getModel($parentTable);

            if (!is_null($parentModel)) {
                $parentColumn = Str::singular($parentTable) . '_id';
                $parentId = $parentModel->safeDecode($parentSlug);

                $query = $model::query()
                    ->where(
                        $parentColumn,
                        '=',
                        $parentId
                    )->where(
                        'user_id',
                        '=',
                        $user->id
                    );

                $exists = $query->exists();

                if ($exists) {
                    $model = $query->first();
                } else {
                    $model->setAttribute($parentColumn, $parentId);
                    $model->setAttribute('user_id', $user->id);
                    $model->save();
                }

                $object = $model->toArray();
                $object = $queryService->slugIds($model, $object);

                return response()->json($object);
            }
        }

        return response()->error('There was an error liking the '.$parentTable.'.');
    }

    // TODO: Extract to update service
    public function unlike(Request $request, $parentTable, $parentSlug) {
        $modelService = new ModelServiceProvider();

        $user = Auth::user();
        if (is_null($user)) {
            return response()->error('Must be logged in to unlike '.$parentTable.'.');
        }

        $table = Str::singular($parentTable).'_likes';

        $model = $modelService->getModel($table);

        if (!is_null($model)) {
            $parentModel = $modelService->getModel($parentTable);

            if (!is_null($parentModel)) {
                $parentColumn = Str::singular($parentTable) . '_id';
                $parentId = $parentModel->safeDecode($parentSlug);

                $query = $model::query();

                $query->where(
                    $parentColumn,
                    '=',
                    $parentId
                )->where(
                    'user_id',
                    '=',
                    $user->id
                );

                $exists = $query->exists();

                $success = false;
                if ($exists) {
                    $success = $query->delete();
                }

                return response()->json(
                    [
                        'deleted' => $success
                    ]
                );
            }
        }

        return response()->error('There was an error liking the '.$parentTable.'.');
    }
}
