<?php
namespace Uab\Controllers\Base\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Uab\Http\Enums\UserTypesEnum;
use Uab\Http\Models\Comments;
use Uab\Http\Models\Users;
use Uab\Providers\Auth;
use Uab\Providers\ModelServiceProvider;
use Uab\Providers\QueryServiceProvider;
use Uab\Providers\UpdateServiceProvider;
use Illuminate\Http\Response;

trait CommentTrait {
    /**
     * @param Request $request
     * @param string $parentTable
     * @param $parentId
     *
     * @return Response
     */
    public function createComment(Request $request, string $parentTable, $parentId) {
        $modelService = new ModelServiceProvider();
        $queryService = new QueryServiceProvider();
        $updateService = new UpdateServiceProvider();

        $parentModel = $modelService->getModel($parentTable);

        $commentTable = 'comments';

        $fields = $updateService->getFillables($commentTable, $request->toArray());

        $comment = $updateService->create($commentTable, $fields);

        if (!is_null($comment)) {
            $joinModel = $modelService->getManyToManyModel($commentTable, $parentTable);

            if ($joinModel) {
                $commentId = $comment['id'];

                $parentColumn = Str::singular($parentTable).'_id';
                $parentId = $parentModel->safeDecode($parentId);

                $joinModel->setAttribute('comment_id', $commentId);
                $joinModel->setAttribute($parentColumn, $parentId);
                $joinModel->save();

                if (Arr::has($comment, 'users'))  {
                    $comment['users'] = Users::query()->find($comment['user_id']);
                }

                $comment = $queryService->slugIds('comments', $comment);

                return response($comment);
            }
        }

        return response(['error' => 'There was an error creating the comment.']);
    }

    /**
     * @param Request $request
     * @param string $parentTable
     * @param $parentId
     *
     * @return Response
     */
    public function getComments(Request $request, string $parentTable, $parentId):Response {
        $activeUser = Auth::user();
        if (is_null($activeUser)) {
            return response(['error' => 'Must be logged in.']);
        }

        $modelService = new ModelServiceProvider();
        $queryService = new QueryServiceProvider();

        $parentModel = $modelService->getModel($parentTable);
        $foreignKey = $modelService->getForeignKey($parentModel);
        $parentId = $parentModel::safeDecodeStatic($parentId);
        if (is_null($parentId)) {
            return response(['error' => 'Parent ID is required.']);
        }

        $joinModel = $modelService->getManyToManyModel('comments', $parentTable);
        $joinTable = $joinModel->getTable();

        $query = Comments::query()
            ->distinct()
            ->select('comments.*')
            ->leftJoin($joinTable, $joinTable . '.comment_id', '=', 'comments.id')
            ->leftJoin('users', 'users.id', '=', 'comments.user_id')
            ->leftJoin('user_types', 'user_types.id', '=', 'users.user_type_id')
            ->where($joinTable . '.' . $foreignKey, '=', $parentId)
            ->orderBy('comments.created', 'asc');

        $activeUserType = $activeUser->userType();
        if ($activeUserType->name !== UserTypesEnum::ADMIN && $activeUserType->name !== UserTypesEnum::SUPER_ADMIN) {
            $query->whereIn('user_types.name', [UserTypesEnum::ADMIN, $activeUserType->name]);
        }

        $comments = $query->get();

        $comments = $comments->toArray();

        foreach ($comments as $i =>  $comment) {
            $user = Users::query()->find($comment['user_id']);

            $comments[$i]['users'] = $user->toArray();
        }

        $comments = $queryService->slugIds('comments', $comments);

        return response($comments);
    }
}
