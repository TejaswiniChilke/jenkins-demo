<?php
namespace Uab\Controllers\Base\Traits;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Uab\Http\Enums\HttpResponseCodesEnum;
use Uab\Http\Enums\QueueTypesEnum;
use Uab\Jobs\Base\InsertFieldsJob;
use Uab\Providers\ModelServiceProvider;
use Uab\Providers\QueryServiceProvider;
use Uab\Providers\UpdateServiceProvider;
use \Exception;

trait CreateTrait {
    /**
     * @param Request $request
     * @param string  $table
     *
     * @return Response
     */
    public function create(Request $request, string $table):Response {
        $modelService = new ModelServiceProvider();
        $queryService = new QueryServiceProvider();
        $updateService = new UpdateServiceProvider();

        $model = $modelService->getModel($table);

        if ($model->shouldQueue()) {
            $fields = $updateService->getFillables($table, $request->toArray());

            InsertFieldsJob::dispatch($table, $fields)->onQueue(QueueTypesEnum::NORMAL);

            return response(
                [
                    'queued' => true
                ]
            );
        } else {
            $newObject = $updateService->create($table, $request->toArray());
            $newObject = $queryService->slugIds($table, $newObject);

            if ($newObject === false) {
                return response(
                    [
                        'error' => 'Unable to create \'' . $table . '\''
                    ]
                );
            } else {
                return response(
                    [
                        $table => $newObject
                    ],
                    HttpResponseCodesEnum::SUCCESS_CREATED
                );
            }
        }
    }

    /**
     * @param Request $request
     * @param string  $table
     *
     * @return Response
     */
    public function findOrCreate(Request $request, string $table):Response {
        $updateService = new UpdateServiceProvider();

        $results = $updateService->findOrCreate($table, $request->toArray());
        $results = $this->queryService->slugIds($table, $results);
        $results = $this->queryService->filterVirtualFields($table, [], $results);

        if ($results) {
            return response($results);
        } else {
            return response(
                [
                    'error' => 'Unable to find or create \''.$table.'\'.'
                ]
            );
        }
    }

    /**
     * @param Request $request
     * @param $action
     * @param string $parentModel
     * @param string $parentId
     * @param string $childModel
     * @param string|null $childId
     *
     * @return Response
     */
    public function joint(Request $request, $action, string $parentModel, string $parentId, string $childModel, ?string $childId = null):Response {
        try {
            switch ($action) {
                case 'add':
                    $response = $this->updateService->joinRelationship($parentModel, $parentId, $childModel, $childId, $request->toArray());
                    break;
                default:
                    if (method_exists($this, $action)) {
                        $response = $this->$action();
                    } else {
                        return response(
                            [
                                'error' => 'Join action is not supported (' . $action . ').',
                            ],
                            HttpResponseCodesEnum::CLIENT_ERROR_BAD_REQUEST
                        );
                    }
                    break;
            }
        } catch (Exception $e) {
            return response('Unknown error.', HttpResponseCodesEnum::SERVER_ERROR_INTERNAL_SERVER_ERROR);
        }

        return response($response);
    }
}
