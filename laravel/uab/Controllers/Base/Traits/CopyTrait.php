<?php
namespace Uab\Controllers\Base\Traits;

use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Uab\Providers\ModelServiceProvider;
use Uab\Providers\QueryServiceProvider;

trait CopyTrait {
    /**
     * @param Request $request
     * @param $table
     * @param null $id
     *
     * @return Response
     */
    public function copy(Request $request, $table, $id = null):Response {
        $modelService = new ModelServiceProvider();

        if (is_null($id)) {
            return response([ 'error' => 'ID is required to copy.']);
        }

        $model = $modelService->copy($table, $id, $request->toArray());

        $queryService = new QueryServiceProvider();
        $returnVar = $queryService->slugIds($model->getTable(), $model);

        return response(
            [
                $table => $returnVar
            ]
        );
    }
}
