<?php
namespace Uab\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Log;
use Uab\Controllers\Generated\PlaylistsController as GeneratedController;
use Uab\Http\Models\Playlists;
use Uab\Http\Models\Tags;
use Uab\Providers\MatchingServiceProvider;

class PlaylistsController extends GeneratedController {
    const DESIRED_TAGS = [
        'range of motion',
        'strength',
        'cardio',
        'functional strength and balance',
        'cool down'
    ];

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getAdherenceRate(Request $request) {
        $matchingService = new MatchingServiceProvider();

        $query = $matchingService->getMatchingQuery('videos', $request->toArray(), 2148);

        return response()->json($query->get());
    }

    /**
     * @param Tags[]|Collection $tags
     *
     * @return bool
     */
    private function hasExcludedTags($tags) {
        $excludedTags = [
            'instructor'
        ];

        $hasExcluded = false;

        foreach ($tags as $tag) {
            if (in_array(strtolower($tag->name), $excludedTags)) {
                $hasExcluded = true;

                break;
            }
        }

        return $hasExcluded;
    }

    /**
     * @param Tags[]|Collection $tags
     *
     * @return Tags|null
     */
    private function hasDesiredTags($tags) {
        $desiredTag = null;

        foreach ($tags as $tag) {
            if (in_array(strtolower($tag->name), self::DESIRED_TAGS)) {
                $desiredTag = $tag;

                break;
            }
        }

        return $desiredTag;
    }

    /**
     * @param Request     $request
     * @param string|null $id
     *
     * @return JsonResponse
     */
    public function getByPlaylistTags(Request $request) {
        $playlistTags = [];
        $alreadyAdded = [];

        $fields = $request->toArray();
        $fields['limit'] = 1000;

        /** @var Playlists[] $playlists */
        $playlists = $this->getExactModels('playlists', $fields);

        foreach ($playlists as $playlistArray) {
            $id = Playlists::decodeSlug($playlistArray['id']);
            if (is_null($id)) {
                Log::error('Playlist ID not found (' . $playlistArray['id'] . ').');
                continue;
            }

            /** @var Playlists $playlist */
            $playlist = Playlists::query()->find($id);
            if (is_null($playlist)) {
                Log::error('Playlist not found by ID (' . $id . ') (.' . $playlistArray['id'] . ')');
                continue;
            }

            $videos = $playlist->videos();

            foreach ($videos as $video) {
                if (Arr::has($alreadyAdded, $video->id)) {
                    continue;
                }

                $tags = $video->tags();

                $hasExcluded = $this->hasExcludedTags($tags);
                if ($hasExcluded) {
                    continue;
                }

                $desiredTag = $this->hasDesiredTags($tags);
                if (is_null($desiredTag)) {
                    continue;
                }

                $tagArray = $desiredTag->toArray();
                $tagArray = $this->queryService->slugIds('tags', $tagArray);

                if (!Arr::has($playlistTags, $tagArray['id'])) {
                    $playlistTags[$tagArray['id']] = [
                        'tag' => $tagArray,
                        'videos' => []
                    ];
                }

                $alreadyAdded[$video->id] = true;

                $slugged = $this->queryService->slugIds('videos', $video);

                $image = $video->image();
                if (!is_null($image)) {
                    $image = $this->queryService->slugIds('images', $image);
                }
                $slugged['images'] = $image;

                array_push(
                    $playlistTags[$tagArray['id']]['videos'],
                    $slugged
                );

                usort(
                    $playlistTags[$tagArray['id']]['videos'],
                    function($a, $b) {
                        return strcmp($a['title'], $b['title']);
                    }
                );
            }
        }

        usort(
            $playlistTags,
            function($a, $b) {
                $tagA = strtolower($a['tag']['name']);
                $tagB = strtolower($b['tag']['name']);

                $aPosition = array_search($tagA, self::DESIRED_TAGS);
                $bPosition = array_search($tagB, self::DESIRED_TAGS);

                return $aPosition > $bPosition ? 1 : -1;
            }
        );

        $playlistTags = array_values($playlistTags);

        return response()->json($playlistTags);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getByPlaylistWIthInstructorTag(Request $request) {
        $returnVar = [];

        $alreadyAdded = [];

        $fields = $request->toArray();
        $fields['limit'] = 1000;

        /** @var Playlists[] $playlists */
        $playlists = $this->getExactModels('playlists', $fields);

        foreach ($playlists as $playlistArray) {
            $id = Playlists::decodeSlug($playlistArray['id']);
            if (is_null($id)) {
                Log::error('Playlist ID not found (' . $playlistArray['id'] . ').');
                continue;
            }

            /** @var Playlists $playlist */
            $playlist = Playlists::query()->find($id);
            if (is_null($playlist)) {
                Log::error('Playlist not found by ID (' . $id . ') (.' . $playlistArray['id'] . ')');
                continue;
            }

            $videos = $playlist->videos();

            foreach ($videos as $video) {
                if (Arr::has($alreadyAdded, $video->id)) {
                    continue;
                }

                $tags = $video->tags();

                foreach ($tags as $tag) {
                    $desiredTags = [
                        'instructor'
                    ];

                    if (in_array(strtolower($tag->name), $desiredTags)) {
                        $alreadyAdded[$video->id] = true;

                        $slugged = $this->queryService->slugIds('videos', $video);

                        $image = $video->image();
                        if (!is_null($image)) {
                            $image = $this->queryService->slugIds('images', $image);
                        }
                        $slugged['images'] = $image;

                        array_push($returnVar, $slugged);

                        break;
                    }
                }
            }
        }

        usort(
            $returnVar,
            function($a, $b) {
                return strcmp($a['title'], $b['title']);
            }
        );

        return response()->json([
            'can_load_more' => false,
            'count'         => count($returnVar),
            'pages'         => 1,
            'videos'        => $returnVar
        ]);
    }
}
