<?php
namespace Uab\Controllers;

use Illuminate\Http\Response;
use Uab\Http\Enums\UserTypesEnum;
use Uab\Http\Models\Users;
use Uab\Providers\Auth;
use Uab\Controllers\Surveys\SurveyController;
use Illuminate\Http\Request;
use Uab\Http\Enums\HttpResponseCodesEnum;

class R3SurveyController extends SurveyController {
    public function getCompleteProfileSurvey(Request $request, $userId = null):Response {
        if (is_null($userId)) {
            $activeUser = Auth::user();
        } else {
            $user = new Users();
            $userId = $user->safeDecode($userId);

            $activeUser = Users::query()->find($userId);
        }

        if (is_null($activeUser)) {
            return response(
                [
                    'error' => 'User ID is required.',
                ],
                HttpResponseCodesEnum::CLIENT_ERROR_BAD_REQUEST
            );
        } else {
            $userType = $activeUser->getUserTypeNameAttribute();

            if ($userType === UserTypesEnum::M2M) {
                return $this->getSurvey($request, 7);
            } else {
                return $this->getSurvey($request, 10);
            }
        }
    }
}
