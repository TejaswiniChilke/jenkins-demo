<?php
namespace Uab\Controllers;

use Uab\Providers\YouTubeServiceProvider;

class YouTubeController {
    public function getVideoInfo($videoId) {
        $service = new YouTubeServiceProvider();

        $video = $service->getVideoInfo($videoId);

        return response()->json($video);
    }
}
