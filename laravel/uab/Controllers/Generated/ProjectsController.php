<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class ProjectsController extends ApiController {
    public $model = 'projects';
}
