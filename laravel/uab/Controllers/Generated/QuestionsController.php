<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class QuestionsController extends ApiController {
    public $model = 'questions';
}
