<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class AnalyticArticlesController extends ApiController {
    public $model = 'analytic_articles';
}
