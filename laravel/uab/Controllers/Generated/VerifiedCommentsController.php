<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class VerifiedCommentsController extends ApiController {
    public $model = 'verified_comments';
}
