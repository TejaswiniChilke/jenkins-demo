<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class EmailsController extends ApiController {
    public $model = 'emails';
}
