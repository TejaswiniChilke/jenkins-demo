<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class BlacklistedWordsController extends ApiController {
    public $model = 'blacklisted_words';
}
