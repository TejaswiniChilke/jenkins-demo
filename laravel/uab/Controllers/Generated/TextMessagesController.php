<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class TextMessagesController extends ApiController {
    public $model = 'text_messages';
}
