<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class BadgesCommentsController extends ApiController {
    public $model = 'badges_comments';
}
