<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class TagsUserTypesController extends ApiController {
    public $model = 'tags_user_types';
}
