<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class SettingsUsersController extends ApiController {
    public $model = 'settings_users';
}
