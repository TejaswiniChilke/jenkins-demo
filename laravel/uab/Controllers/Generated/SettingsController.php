<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class SettingsController extends ApiController {
    public $model = 'settings';
}
