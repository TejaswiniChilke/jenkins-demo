<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class BadgeConditionsUsersController extends ApiController {
    public $model = 'badge_conditions_users';
}
