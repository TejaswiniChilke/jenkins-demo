<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class FeaturesController extends ApiController {
    public $model = 'features';
}
