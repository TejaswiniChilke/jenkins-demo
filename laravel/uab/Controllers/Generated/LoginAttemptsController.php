<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class LoginAttemptsController extends ApiController {
    public $model = 'login_attempts';
}
