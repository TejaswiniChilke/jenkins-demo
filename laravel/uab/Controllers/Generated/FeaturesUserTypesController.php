<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class FeaturesUserTypesController extends ApiController {
    public $model = 'features_user_types';
}
