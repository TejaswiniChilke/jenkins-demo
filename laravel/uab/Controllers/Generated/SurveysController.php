<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class SurveysController extends ApiController {
    public $model = 'surveys';
}
