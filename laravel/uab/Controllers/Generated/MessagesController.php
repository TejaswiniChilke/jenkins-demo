<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class MessagesController extends ApiController {
    public $model = 'messages';
}
