<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class SurveysUsersController extends ApiController {
    public $model = 'surveys_users';
}
