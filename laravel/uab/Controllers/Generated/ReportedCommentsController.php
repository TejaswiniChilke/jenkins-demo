<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class ReportedCommentsController extends ApiController {
    public $model = 'reported_comments';
}
