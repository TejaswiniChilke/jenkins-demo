<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class ContactStaffMessagesController extends ApiController {
    public $model = 'contact_staff_messages';
}
