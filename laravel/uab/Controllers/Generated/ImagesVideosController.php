<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class ImagesVideosController extends ApiController {
    public $model = 'images_videos';
}
