<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class TermsAndConditionsUsersController extends ApiController {
    public $model = 'terms_and_conditions_users';
}
