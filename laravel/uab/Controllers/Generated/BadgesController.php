<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class BadgesController extends ApiController {
    public $model = 'badges';
}
