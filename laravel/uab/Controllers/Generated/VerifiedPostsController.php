<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class VerifiedPostsController extends ApiController {
    public $model = 'verified_posts';
}
