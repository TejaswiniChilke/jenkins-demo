<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class StatesController extends ApiController {
    public $model = 'states';
}
