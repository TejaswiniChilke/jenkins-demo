<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class PlaylistsVideosController extends ApiController {
    public $model = 'playlists_videos';
}
