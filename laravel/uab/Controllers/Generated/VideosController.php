<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class VideosController extends ApiController {
    public $model = 'videos';
}
