<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class FollowersController extends ApiController {
    public $model = 'followers';
}
