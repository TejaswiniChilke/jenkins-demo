<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class EventsUsersController extends ApiController {
    public $model = 'events_users';
}
