<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class OauthPersonalAccessClientsController extends ApiController {
    public $model = 'oauth_personal_access_clients';
}
