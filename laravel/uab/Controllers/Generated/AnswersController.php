<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class AnswersController extends ApiController {
    public $model = 'answers';
}
