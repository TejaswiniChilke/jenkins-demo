<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class PostsImagesController extends ApiController {
    public $model = 'posts_images';
}
