<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class BlockedAccessAttemptsController extends ApiController {
    public $model = 'blocked_access_attempts';
}
