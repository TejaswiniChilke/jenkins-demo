<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class YoutubeVideoInfosController extends ApiController {
    public $model = 'youtube_video_infos';
}
