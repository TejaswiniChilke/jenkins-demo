<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class CommentsPostsController extends ApiController {
    public $model = 'comments_posts';
}
