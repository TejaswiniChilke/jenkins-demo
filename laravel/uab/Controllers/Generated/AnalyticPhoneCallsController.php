<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class AnalyticPhoneCallsController extends ApiController {
    public $model = 'analytic_phone_calls';
}
