<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class BadgeConditionsController extends ApiController {
    public $model = 'badge_conditions';
}
