<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class CommentsController extends ApiController {
    public $model = 'comments';
}
