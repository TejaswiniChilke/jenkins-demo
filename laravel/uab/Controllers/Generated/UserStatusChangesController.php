<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class UserStatusChangesController extends ApiController {
    public $model = 'user_status_changes';
}
