<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class UspsAddressesController extends ApiController {
    public $model = 'usps_addresses';
}
