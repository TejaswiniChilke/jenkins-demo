<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class ThreadsController extends ApiController {
    public $model = 'threads';
}
