<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class OauthAuthCodesController extends ApiController {
    public $model = 'oauth_auth_codes';
}
