<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class SecurityQuestionsController extends ApiController {
    public $model = 'security_questions';
}
