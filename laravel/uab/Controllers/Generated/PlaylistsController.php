<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class PlaylistsController extends ApiController {
    public $model = 'playlists';
}
