<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class UserTypesController extends ApiController {
    public $model = 'user_types';
}
