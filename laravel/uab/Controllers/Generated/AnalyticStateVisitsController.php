<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class AnalyticStateVisitsController extends ApiController {
    public $model = 'analytic_state_visits';
}
