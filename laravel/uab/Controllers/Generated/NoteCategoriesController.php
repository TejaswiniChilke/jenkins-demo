<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class NoteCategoriesController extends ApiController {
    public $model = 'note_categories';
}
