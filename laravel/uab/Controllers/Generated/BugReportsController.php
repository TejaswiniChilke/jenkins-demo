<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class BugReportsController extends ApiController {
    public $model = 'bug_reports';
}
