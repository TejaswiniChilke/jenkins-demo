<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class ReportedPostsController extends ApiController {
    public $model = 'reported_posts';
}
