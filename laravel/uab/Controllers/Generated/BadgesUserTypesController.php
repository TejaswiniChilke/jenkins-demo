<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class BadgesUserTypesController extends ApiController {
    public $model = 'badges_user_types';
}
