<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class AnalyticVideosController extends ApiController {
    public $model = 'analytic_videos';
}
