<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class PlaylistsTagsController extends ApiController {
    public $model = 'playlists_tags';
}
