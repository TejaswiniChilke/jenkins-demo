<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class ArticlesController extends ApiController {
    public $model = 'articles';
}
