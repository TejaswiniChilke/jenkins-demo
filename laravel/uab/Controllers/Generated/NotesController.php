<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class NotesController extends ApiController {
    public $model = 'notes';
}
