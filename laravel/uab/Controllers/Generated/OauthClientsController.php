<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class OauthClientsController extends ApiController {
    public $model = 'oauth_clients';
}
