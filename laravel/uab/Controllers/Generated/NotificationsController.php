<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class NotificationsController extends ApiController {
    public $model = 'notifications';
}
