<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class EventsController extends ApiController {
    public $model = 'events';
}
