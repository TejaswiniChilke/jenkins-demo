<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class UsersController extends ApiController {
    public $model = 'users';
}
