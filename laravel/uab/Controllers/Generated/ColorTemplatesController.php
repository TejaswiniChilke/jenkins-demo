<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class ColorTemplatesController extends ApiController {
    public $model = 'color_templates';
}
