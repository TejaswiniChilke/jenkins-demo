<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class OauthRefreshTokensController extends ApiController {
    public $model = 'oauth_refresh_tokens';
}
