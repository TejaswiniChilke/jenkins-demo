<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class SettingsUserTypesController extends ApiController {
    public $model = 'settings_user_types';
}
