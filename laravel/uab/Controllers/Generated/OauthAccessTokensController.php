<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class OauthAccessTokensController extends ApiController {
    public $model = 'oauth_access_tokens';
}
