<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class SurveyPagesController extends ApiController {
    public $model = 'survey_pages';
}
