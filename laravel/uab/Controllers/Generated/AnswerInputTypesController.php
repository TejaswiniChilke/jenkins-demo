<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class AnswerInputTypesController extends ApiController {
    public $model = 'answer_input_types';
}
