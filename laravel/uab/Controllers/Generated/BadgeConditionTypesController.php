<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class BadgeConditionTypesController extends ApiController {
    public $model = 'badge_condition_types';
}
