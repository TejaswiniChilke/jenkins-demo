<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class ImagesController extends ApiController {
    public $model = 'images';
}
