<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class ArticlesCommentsController extends ApiController {
    public $model = 'articles_comments';
}
