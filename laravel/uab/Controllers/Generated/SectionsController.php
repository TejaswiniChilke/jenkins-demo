<?php
namespace Uab\Controllers\Generated;

use Uab\Controllers\Base\ApiController;

class SectionsController extends ApiController {
    public $model = 'sections';
}
