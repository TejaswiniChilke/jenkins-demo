<?php
namespace Uab\Controllers;

use Response;
use Uab\Controllers\Generated\ColorTemplatesController as GeneratedController;
use Cache;
use Illuminate\Http\Request;

class ColorTemplatesController extends GeneratedController {
    /**
     * @param Request $request
     * @param null    $id
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function getRequest(Request $request, $id = null) {
        $results = Cache::remember(
            'color_templates',
            60,
            function() use ($request, $id) {
                $query = $this->queryService->buildRequestQuery('color_templates', $request->toArray(), $id);

                return $this->queryService->parseGet('color_templates', $request->toArray(), $query);
            }
        );

        return response($results);
    }
}
