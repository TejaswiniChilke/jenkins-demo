<?php
namespace Uab\Controllers;

use Illuminate\Http\JsonResponse;
use Uab\Controllers\Generated\PostsController as GeneratedController;
use Uab\Http\Models\Posts;
use Uab\Providers\Auth;
use Cache;
use Illuminate\Http\Request;
use Uab\Providers\UserTypeServiceProvider;

class PostsController extends GeneratedController {
    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getNewsfeed(Request $request) {
        $userTypes = collect();

        $activeUser = Auth::user();
        if (is_null($activeUser)) {
            $userId = 'none';
        } else {
            $userId = $activeUser->id;

            $userTypeService = new UserTypeServiceProvider();
            $userTypes = $userTypeService->getPermissibleUserTypes($activeUser);
        }

        $userTypeIds = $userTypes->pluck('id');

        $page = $request->input('page', 1);
        $limit = $request->input('limit', 19);

        $response = Cache::remember(
            'posts-get-'.$userId.'page-'.$page,
            0,
            function() use ($request, $userTypeIds, $page, $limit) {
                $options = ['joins' => ['from_users','to_users']];

                $query = Posts::query()
                    ->join('users as from_users', 'from_users.id', '=', 'posts.from_user_id')
                    ->leftJoin('users as to_users', 'to_users.id', '=', 'posts.to_user_id')
                    ->whereIn('from_users.user_type_id', $userTypeIds)
                    ->where(
                        function($query) use ($userTypeIds) {
                            /** Builder $query */
                            return $query->whereIn('to_users.user_type_id', $userTypeIds)
                                ->orWhereNull('posts.to_user_id');
                        }
                    )->offset(($page - 1) * $limit)
                    ->orderBy('posts.created', 'DESC')
                    ->limit($limit);

                $query = $this->queryService->addSelect($options, $query, new Posts());

                $results = $this->queryService->parseGet('posts', $options, $query);
                $results = $this->queryService->slugIds('posts', $results);

                $page = $this->requestService->getPage($request->toArray());
                if ($page) {
                    $response = $this->queryService->getPaginationResponse($options, 'posts', $query, $results);
                } else {
                    $response = [
                        'posts' => $results
                    ];
                }

                return $response;
            }
        );

        return response()->json($response);
    }


}
