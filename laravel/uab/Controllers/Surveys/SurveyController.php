<?php
namespace Uab\Controllers\Surveys;

use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Uab\Http\Enums\HttpResponseCodesEnum;
use Uab\Http\Models\AnswerOptions;
use Uab\Providers\ModelServiceProvider;
use Uab\Providers\SurveyServiceProvider;

class SurveyController {
    public $surveyService;

    public function __construct() {
        $this->surveyService = new SurveyServiceProvider();
    }

    /**
     * @param Request $request
     * @param string|null $slug
     *
     * @return Response
     */
    public function getSurvey(Request $request, string $slug = null):Response {
        $slug = $request->input('id', $slug);
        if (is_null($slug)) {
            return response(
                [
                    'error' => 'ID is required to get survey.'
                ],
                HttpResponseCodesEnum::CLIENT_ERROR_BAD_REQUEST
            );
        }

        $survey = $this->surveyService->getSurvey($slug);
        if (is_null($survey)) {
            return response(
                [
                    'error' => 'Could not find the survey by id ('.$slug.').'
                ],
                HttpResponseCodesEnum::CLIENT_ERROR_GONE
            );
        }

        return response(
            [
                'survey' => $survey
            ],
            HttpResponseCodesEnum::SUCCESS_OK
        );
    }

    /**
     * @param Request $request
     * @param string|null $slug
     *
     * @return Response
     */
    public function getSurveyPage(Request $request, ?string $slug = null):Response {
        $slug = $request->input('id', $slug);
        if (is_null($slug)) {
            return response(
                [
                    'error' => 'ID is required to get survey page.'
                ],
                HttpResponseCodesEnum::CLIENT_ERROR_BAD_REQUEST
            );
        }

        $page = $this->surveyService->getSurveyPage($slug);

        return response(
            [
                'page' => $page
            ],
            HttpResponseCodesEnum::SUCCESS_OK
        );
    }

    /**
     * @param Request $request
     * @param string|null $slug
     *
     * @return Response
     */
    public function getNextPage(Request $request, ?string $slug = null):Response {
        $modelService = new ModelServiceProvider();

        $surveyPage = $modelService->arrayToStd(
            $request->toArray()
        );

        if (!is_null($slug)) {
            $surveyPage->id = $slug;
        }

        if (!$surveyPage && !isset($surveyPage->id) && is_null($surveyPage->id)) {
            return response(
                [
                    'error' => 'ID is required to get the next survey page.'
                ],
                HttpResponseCodesEnum::CLIENT_ERROR_BAD_REQUEST
            );
        }

        $success = $this->surveyService->saveSurveyPage($surveyPage);
        if (!$success) {
            return response(
                [
                    'error' => 'Unable to save survey page and get the next page.'
                ],
                HttpResponseCodesEnum::CLIENT_ERROR_BAD_REQUEST
            );
        }

        $option = null;

        for ($i = count($surveyPage->questions); $i >= 0; $i--) {
            $question = $surveyPage->questions[$i];

            if (is_null($question->answer)) {
                continue;
            }

            $answer = Arr::last($question->answer);
            if (is_null($answer)) {
                continue;
            }

            $mm = new AnswerOptions();
            $id = $mm->safeDecode($answer->answer_option_id);
            if (is_null($id)) {
                continue;
            }

            /** @var AnswerOptions|null $option */
            $option = AnswerOptions::query()->find($id);
            if (!is_null($option)) {
                break;
            }
        }

        if (is_null($option)) {
            return response(
                [
                    'error' => 'Cannot find the next page.'
                ],
                HttpResponseCodesEnum::CLIENT_ERROR_BAD_REQUEST
            );
        }

        $nextPage = $this->surveyService->getNextPage($option);

        return response(
            [
                'page' => $nextPage
            ]
        );
    }

    /**
     * @param Request $request
     * @param string|null $slug
     *
     * @return Response
     */
    public function getPreviousPage(Request $request, ?string $slug = null):Response {
        $modelService = new ModelServiceProvider();

        $surveyPage = $modelService->arrayToStd(
            $request->toArray()
        );

        if (!is_null($slug)) {
            $surveyPage->id = $slug;
        }

        if (!$surveyPage && !isset($surveyPage->id) && is_null($surveyPage->id)) {
            return response(
                [
                    'error' => 'ID is required to get the previous survey page.'
                ],
                HttpResponseCodesEnum::CLIENT_ERROR_BAD_REQUEST
            );
        }

        $success = $this->surveyService->saveSurveyPage($surveyPage);
        if (!$success) {
            return response(
                [
                    'error' => 'Unable to save survey page to get the previous page.'
                ],
                HttpResponseCodesEnum::CLIENT_ERROR_BAD_REQUEST
            );
        }

        $prevPage = $this->surveyService->getPreviousPage(Arr::first($surveyPage->questions));

        return response(
            [
                'page' => $prevPage
            ]
        );
    }

    /**
     * @param Request $request
     * @param string|null $slug
     *
     * @return Response
     */
    public function saveSurvey(Request $request, ?string $slug = null):Response {
        $modelService = new ModelServiceProvider();

        $survey = $modelService->arrayToStd(
            $request->toArray()
        );

        if (!is_null($slug)) {
            $survey['id'] = $slug;
        }

        if (!$survey && !isset($survey->id) && is_null($survey->id)) {
            return response(
                [
                    'error' => 'ID is required to save survey.'
                ],
                HttpResponseCodesEnum::CLIENT_ERROR_BAD_REQUEST
            );
        }

        $success = $this->surveyService->saveSurvey($survey);
        if (!$success) {
            return response(
                [
                    'error' => 'Unable to save survey.'
                ],
                HttpResponseCodesEnum::CLIENT_ERROR_BAD_REQUEST
            );
        }

        return $this->getSurvey($request, $slug);
    }
}
