<?php
namespace Uab\Controllers;

use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Uab\Controllers\Generated\UsersController as GeneratedController;
use Uab\Http\Enums\UserStatusesEnum;
use Uab\Http\Models\Users;
use Uab\Providers\UserTypeServiceProvider;

class UsersController extends GeneratedController {
    /**
     * @param Request $request
     * @param $id
     *
     * @return JsonResponse
     */
    public function confirmEmail(Request $request, $id) {
        $user = new Users();
        $id = $user->safeDecode($id);

        /** @var Users|null $user */
        $user = Users::query()->find($id);

        if (!is_null($user)) {
            $user->email_confirmed_on = Carbon::now();
            $user->status = UserStatusesEnum::EMAIL_CONFIRMED;
            $success = $user->save();

            if ($success) {
                return response()->json(
                    [
                        'users' => $user
                    ]
                );
            } else {
                return response()->error('Unable to save user confirming email.');
            }
        } else {
            return response()->error('Cannot find user by ID ('.$id.')');
        }
    }

    public function getApplicants() {
        $query = Users::query()
            ->where(
                [
                    'requires_approval' => 1,
                    'status'            => UserStatusesEnum::APPLICATION_PENDING
                ]
            );

        $users = $query->get()->toArray();
        $users = $this->queryService->slugIds('users', $users);

        return response(
            [
                'applicants' => $users
            ]
        );
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function orderByActivity(Request $request) {
        $userTypeService = new UserTypeServiceProvider();
        $userTypes = $userTypeService->getPermissibleUserTypes();

        $userTypeIds = $userTypes->pluck('id');

        $limit = $request->input('limit', 25);

        $page = $request->input('page', 1);
        $offset = ($page - 1) * $limit;

        $query = Users::query()
            ->select('users.*')
            ->whereIn('users.user_type_id', $userTypeIds)
            ->join(
                'analytic_state_visits as s',
                'users.id',
                '=',
                's.user_id'
            )->orderBy(
                's.transition_datetime',
                'desc'
            )->distinct()
            ->offset($offset)
            ->limit($limit);

        $users = $query->get();
        $users = $users->toArray();

        $users = $this->queryService->slugIds(new Users(), $users);

        return response(
            [
                'users' => $users
            ]
        );
    }
}
