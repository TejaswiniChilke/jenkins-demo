<?php
namespace Uab\Controllers;

use Illuminate\Http\Response;
use \ReflectionException;
use Illuminate\Http\Request;
use Uab\Controllers\Generated\UsersController as GeneratedController;
use Uab\Http\Enums\UserStatusesEnum;
use \Illuminate\Contracts\Routing\ResponseFactory;
use \Illuminate\Http\JsonResponse;

class UserStatusesController extends GeneratedController {
    /**
     * @param Request $request
     * @param string $table
     * @param null $id
     *
     * @return ResponseFactory|JsonResponse
     *
     * @throws ReflectionException
     */
    public function get(Request $request, $table, $id = null) {
        $statuses = UserStatusesEnum::getAll();

        return response()->json(
            [
                'user_statuses' => $statuses
            ]
        );
    }

    /**
     * @param Request $request
     * @param string $table
     * @param null $id
     *
     * @return ResponseFactory|Response
     */
    public function create(Request $request, $table, $id = null) {
        return response()->error('Cannot create user status.');
    }

    /**
     * @param Request $request
     * @param string $table
     * @param null $id
     *
     * @return ResponseFactory|Response
     */
    public function edit(Request $request, $table, $id = null) {
        return response()->error('Cannot edit user status.');
    }

    /**
     * @param Request $request
     * @param string $table
     * @param null $id
     *
     * @return ResponseFactory|Response
     */
    public function delete(Request $request, $table, $id = null) {
        return response()->error('Cannot delete user status.');
    }
}
