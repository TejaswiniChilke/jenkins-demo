<?php
namespace Uab\Controllers;

use Cache;
use Illuminate\Http\Request;
use Uab\Controllers\Generated\StatesController as GeneratedController;

class StatesController extends GeneratedController {
    public function getRequest(Request $request, $id = null) {
        if (is_null($id)) {
            $results = Cache::remember(
                'states-get',
                60 * 24 * 30,
                function () use ($request, $id) {
                    $id = $this->requestService->getId($request, $id);

                    $query = $this->queryService->buildRequestQuery('states', $request->toArray(), $id);

                    return $this->queryService->parseGet('states', $request->toArray(), $query);
                }
            );

            return response($results);
        }

        return parent::get($request, $id);
    }
}
