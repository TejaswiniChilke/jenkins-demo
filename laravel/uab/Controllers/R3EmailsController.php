<?php
namespace Uab\Controllers;

use DateInterval;
use DatePeriod;
use DateTime;
use Exception;
use Illuminate\Http\Request;
use Response;
use Uab\Controllers\Generated\EmailsController;
use Uab\Http\Enums\HttpResponseCodesEnum;
use Uab\Http\Enums\QueueTypesEnum;
use Uab\Http\Models\Emails;
use Uab\Http\Models\Events;
use Uab\Http\Models\EventsUsers;
use Uab\Http\Models\Users;
use Uab\Jobs\Emails\SendEmail;
use Uab\Providers\Auth;
use Uab\Providers\R3\R3EmailServiceProvider;
use Uab\Providers\UsersServiceProvider;

class R3EmailsController extends EmailsController {
    public function __construct() {
        parent::__construct();
    }

    /**
     * @param $startDate
     * @param $endDate
     *
     * @return array
     *
     * @throws Exception
     */
    public function generate($startDate, $endDate) {
        $events = [];

        $user = Auth::user();

        if (!is_null($user)) {
            $begin = new DateTime($startDate);
            $end = new DateTime($endDate);

            $interval = DateInterval::createFromDateString('1 day');
            $period = new DatePeriod($begin, $interval, $end);

            foreach ($period as $dt) {
                $event = new Events();

                $event->start = $dt->format('Y-m-d H:i:s');
                $event->end = $dt->format('Y-m-d H:i:s');
                $event->title = 'Exercise';
                $event->description = '';
                $event->creator_id = null;
                $event->allDay =  false;
                $event->is_cancelled = false;
                $event->is_complete = false;

                $event->save();

                $eventUser = new EventsUsers();
                $eventUser->event_id = $event->id;
                $eventUser->user_id = $user->id;
                $eventUser->save();

                $events[] = $event;
            }
        }

        return $events;
    }

    /**
     * @param Request $request
     * @param string  $id
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|Response
     */
    public function sendAcceptEmail(Request $request, $id) {
        $service = new R3EmailServiceProvider();

        /** @var Users $user */
        $user = new Users();
        $id = $user->safeDecode($id);

        $user = Users::query()->find($id);

        if (is_null($user)) {
            return response(['error' => 'Unable to send accepted email.' ]);
        } else {
            $service->sendAcceptedEmail($user);

            return response('Done');
        }
    }

    public function sendEmail(Request $request) {
        $email = new Emails();

        if ($request->has('from_user_id')) {
            $email->from_user_id = $request->input('from_user_id', null);
        } else {
            $activeUser = Auth::user();
            if (!is_null($activeUser)) {
                $userService = new UsersServiceProvider();

                if (!$userService->isAdmin($activeUser)) {
                    $email->from_user_id = $activeUser->id;
                }
            } else {
                return response()->error(
                    'You must be logged into to assume from_user_id when sending an email.',
                    HttpResponseCodesEnum::CLIENT_ERROR_BAD_REQUEST
                );
            }
        }

        if ($request->has('from_email')) {
            $email->from_email = $request->input('from_email', null);
        } else {
            /** @var Users $fromUser */
            $fromUser = $this->queryService->getById($email->from_user_id, 'users');
            if (!is_null($fromUser)) {
                $email->from_email = $fromUser->email;
            }
        }

        if ($request->has('to_user_id')) {
            $users = new Users();
            $email->to_user_id = $users->safeDecode($request->input('to_user_id', null));
        } else {
            return response()->error(
                'To user ID is required to send.',
                HttpResponseCodesEnum::CLIENT_ERROR_BAD_REQUEST
            );
        }

        if ($request->has('to_email')) {
            $email->to_email = $request->input('to_email', null);
        } else {
            /** @var Users $toUser */
            $toUser = Users::query()->find($email->to_user_id);

            if (!is_null($toUser)) {
                $email->to_email = $toUser->email;
            } else {
                return response()->error(
                    "Unable to find user with id ({$email->to_user_id}).",
                    HttpResponseCodesEnum::CLIENT_ERROR_BAD_REQUEST
                );
            }
        }

        $message = '';
        if ($request->has('message')) {
            $message = $request->input('message', null);
        }

        if (strlen($message) === 0) {
            return response()->error(
                'Message is required.',
                HttpResponseCodesEnum::CLIENT_ERROR_BAD_REQUEST
            );
        } else {
            $email->message = $message;
        }

        $subject = '';
        if ($request->has('subject')) {
            $subject = $request->input('subject', null);
        }

        if (strlen($subject) === 0) {
            return response()->error(
                'Message is required.',
                HttpResponseCodesEnum::CLIENT_ERROR_BAD_REQUEST
            );
        } else {
            $email->subject = $subject;
        }

        $email->save();

        try {
            SendEmail::dispatch($email)->onQueue(QueueTypesEnum::EMAIL);
        } catch (Exception $e) {
            return response()->error(
                'Unable to send the email. Try again later.',
                HttpResponseCodesEnum::SERVER_ERROR_INTERNAL_SERVER_ERROR
            );
        }

        return response(
            [
                'emails' => $email
            ]
        );
    }
}
