<?php
namespace Uab\Controllers;

use Illuminate\Http\Response;
use Uab\Http\Models\Users;
use Uab\Http\Models\Followers;
use Uab\Providers\FriendServiceProvider;
use Illuminate\Http\Request;
use Uab\Http\Enums\HttpResponseCodesEnum;
use Uab\Providers\Auth;
use Uab\Providers\QueryServiceProvider;

class FriendsController {
    private $friendService;

    public function __construct() {
        $this->friendService = new FriendServiceProvider();
    }

    /**
     * @param Request $request
     * @param string|null $id
     *
     * @return Response
     */
    public function addFriend(Request $request, ?string $id = null):Response {
        $success = false;

        $user = Auth::user();
        if (!is_null($user)) {
            $followingId = $request->input('id', $id);

            $follower = new Followers();

            $follower->user_id = $user->id;
            $follower->following_id = $followingId;
            $success = $follower->save();
        }

        return response(
            [
                'success' => $success
            ],
            $success ?
                HttpResponseCodesEnum::SUCCESS_OK : HttpResponseCodesEnum::CLIENT_ERROR_BAD_REQUEST
        );
    }

    /**
     * @param Request $request
     * @param string|null $id
     *
     * @return Response
     */
    public function cancelFriendRequest(Request $request, ?string $id = null):Response {
        $id = $request->input('id', $id);

        $user = new Users();
        $id = $user->safeDecode($id);

        $user = Auth::user();

        if (!is_null($user)) {
            $query = Followers::query()
                ->where(
                    [
                        'following_id' => $id,
                        'user_id' => $id,

                    ]
                );

            $success = $query->delete();

            if ($success) {
                return response(
                    [
                        'success' => $success
                    ]
                );
            } else {
                return response()->error('Unable to delete friend request.');
            }
        }

        return response()->error('Unable to cancel friend request.');
    }

    /**
     * @param Request $request
     * @param string|null $id
     *
     * @return Response
     */
    public function confirmFriend(Request $request, ?string $id = null):Response {
        return $this->addFriend($request, $id);
    }

    /**
     * @param Request $request
     * @param string|null $id
     *
     * @return Response
     */
    public function declineFriendRequest(Request $request, ?string $id = null):Response {
        return $this->remove($request, $id);
    }

    /**
     * @param Request $request
     * @param string|null $id
     *
     * @return Response
     */
    public function getFriends(Request $request, ?string $id = null):Response {
        $user = $this->getUser($request, $id);

        if (!is_null($user)) {
            $friendService = new FriendServiceProvider();
            $queryService = new QueryServiceProvider();

            $friends = $friendService->getFriends($user);
            $friends = $queryService->slugIds(new Users(), $friends);

            $response = [
                'can_load_more' => false,
                'count'         => count($friends),
                'friends'       => $friends
            ];

            return response(
                $response,
                HttpResponseCodesEnum::SUCCESS_OK
            );
        }

        return response(
            [
                'error' => 'Requested user not found',
            ],
            HttpResponseCodesEnum::CLIENT_ERROR_FOUND
        );
    }

    /**
     * @param Request $request
     * @param string|null $id
     *
     * @return Response
     */
    public function getFriendRequests(Request $request, ?string $id = null):Response {
        $requests = [];

        $requestedUser = $this->getUser($request, $id);

        if (!is_null($requestedUser)) {
            $users = Users::all();

            foreach ($users as $user) {
                if (!$this->friendService->isFriends($user->id, $requestedUser->id)) {
                    if ($this->friendService->isFollowing($user->id,$requestedUser->id)) {
                        $requests[] = $user;
                    }
                }
            }

            $response = [
                'can_load_more'   => false,
                'count'           => count($requests),
                'friend_requests' => $requests
            ];

            return response(
                $response,
                HttpResponseCodesEnum::SUCCESS_OK
            );
        }

        return response(
            [
                'error' => 'Requested user not found',
            ],
            HttpResponseCodesEnum::CLIENT_ERROR_FOUND
        );
    }

    /**
     * @param Request $request
     * @param string|null $id
     *
     * @return Users
     */
    private function getUser(Request $request, ?string $id = null):Users {
        $id = $request->input('id', $id);

        $user = null;
        if (!is_null($id)) {
            $user = new Users();
            $id = $user->safeDecode($id);

            $user = Users::query()->find($id);
        }

        if (is_null($user)) {
            $user = Auth::user();
        }

        return $user;
    }

    /**
     * @param Request $request
     * @param string|null $id
     *
     * @return Response
     */
    public function remove(Request $request, ?string $id = null):Response {
        $id = $request->input('id', $id);

        $user = new Users();
        $id = $user->safeDecode($id);

        $user = Auth::user();

        if (!is_null($user)) {
            $query = Followers::query()
                ->where(
                    [
                        'following_id' => $user->id,
                        'user_id'      => $id
                    ]
                );

            $success = $query->delete();

            $query = Followers::query()
                ->where(
                    'user_id',
                    '=',
                    $user->id
                )->where(
                    'following_id',
                    '=',
                    $id
                );

            $success = $query->delete() && $success;

            if ($success) {
                return response(
                    [
                        'success' => true
                    ]
                );
            } else {
                return response()->error('Unable to clear followers.');
            }
        }

        return response()->error('Unable to unfollow.');
    }

    /**
     * @param Request $request
     * @param string|null $id
     *
     * @return Response
     */
    public function unfollow(Request $request, ?string $id = null):Response {
        $id = $request->input('id', $id);

        $user = new Users();
        $id = $user->safeDecode($id);

        $user = Auth::user();

        if (!is_null($user)) {
            $query = Followers::query()
                ->where(
                    [
                        'user_id'      => $user->id,
                        'following_id' => $id,
                    ]
                );

            $success = $query->delete();

            if ($success) {
                return response(
                    [
                        'success' => $success
                    ]
                );
            } else {
                return response()->error('Unable to clear followers.');
            }
        }

        return response()->error('Unable to unfollow.');
    }
}
