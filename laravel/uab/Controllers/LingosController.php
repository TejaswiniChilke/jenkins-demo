<?php
namespace Uab\Controllers;

use Illuminate\Contracts\Routing\ResponseFactory;
use Response;
use Uab\Controllers\Generated\LingosController as GeneratedController;
use Cache;
use Illuminate\Http\Request;

class LingosController extends GeneratedController {
    /**
     * @param Request $request
     * @param null $id
     *
     * @return ResponseFactory|Response
     */
    public function getRequest(Request $request, $id = null) {
        return Cache::remember(
            'all-lingo',
            60,
            function() use ($request, $id) {
                return $this->get($request, 'lingos', $id);
            }
        );
    }
}
