<?php

namespace Uab\Controllers;

use Storage;
use Uab\Http\Models\EventsUsers;
use Uab\Providers\Auth;
use Uab\Providers\IcsServiceProvider;

class IcsController {
    public function get($id = null) {
        if (is_null($id)) {
            $id = Auth::id();
        }

        $events = EventsUsers::query()
            ->where('user_id', $id)
            ->join(
                'events',
                'events.id',
                '=',
                'events_users.event_id'
            )->get();

        $service = new IcsServiceProvider();
        $ics = $service->getIcs($events);

        if (is_null($ics)) {
            return response()->error('Unable to create ICS file.');
        }

        return response()->download($ics)->deleteFileAfterSend(true);
    }
}
