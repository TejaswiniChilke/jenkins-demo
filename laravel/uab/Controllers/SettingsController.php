<?php
namespace Uab\Controllers;

use Uab\Controllers\Generated\SettingsController as GeneratedController;
use Illuminate\Http\Request;
use Uab\Http\Models\Base\BaseModel;
use Uab\Http\Models\Settings;
use Uab\Providers\SettingsServiceProvider;

class SettingsController extends GeneratedController {
    const SETTING_TYPES = [
        'user'      => 'SettingsUsers',
        'user_type' => 'SettingsUserTypes'
    ];

    const VALUE_TYPE_BOOLEAN = 'boolean';

    public function getSettings(Request $request) {
        $settingService = new SettingsServiceProvider();

        $userId = $request->input('user_id');

        $userTypeId = $userTypeId = $request->input('user_type_id');

        $settings = $settingService->getSettings($userId, $userTypeId);

        return response(
            [
                'settings' => $settings
            ]
        );
    }

    public function updateSettings(Request $request) {
        $models = [];

        if ($request->has('settings')) {
            $settings = $request->input('settings');
            if (count($settings) == 1) {
                $settings = $settings[0];
            }


            foreach ($settings as $setting) {
                $alias = (is_array($setting) && array_key_exists('setting_type', $setting)) ?
                    self::SETTING_TYPES[$setting['setting_type']] : null;

                if (is_null($alias)) {
                    return response()->error(
                        'Could not find setting type: '.$setting['setting_type']
                    );
                } else {
                    $object = null;
                    $model = $this->modelService->getModel($alias);

                    if (array_key_exists('id', $setting)) {
                        $settingId = $setting['id'];

                        /** @var BaseModel $object */
                        $object = $this->queryService->getById($settingId, $model);
                    }

                    if (!is_null($object)) {
                        $object->fill($setting);
                    } else {
                        $object = $model->fill($setting);

                        $object->value = array_key_exists('value', $setting) ?
                            $setting['value'] : $setting['default_value'];
                    }

                    if (!array_key_exists($alias, $models)) {
                        $models[$alias] = [];
                    }
                    $models[$alias][] = $object;
                }
            }

            foreach ($models as $alias => $objects) {
                $errors = [];
                $saved = [];

                foreach ($objects as $object) {
                    /** @var Settings $setting */
                    $setting = $this->queryService->getById($object->setting_id, 'Settings');
                    if ($setting->value_type === 'boolean') {
                        $object->value = $object->value === true ||$object->value === 'true' || $object->value === 1 || $object->value === '1';
                    }

                    $success = $object->save();

                    if ($success) {
                        $saved[] = $object;
                    } else {
                        $errors[] = 'Could not edit ' . $alias . '.';
                    }
                }

                return response(
                    [
                        $alias => $saved
                    ]
                )->error($errors);
            }
        }

        return parent::get($request,'settings');
    }
}
