<?php
namespace Uab\Controllers;

use Uab\Controllers\Generated\FeaturesUserTypesController as GeneratedController;
use Uab\Providers\Auth;
use Cache;
use Illuminate\Http\Request;

class FeaturesUserTypesController extends GeneratedController {
    public function getRequest(Request $request, $id = null) {
        $activeUser = Auth::user();
        if (is_null($activeUser)) {
            $userId = 'none';
        } else {
            $userId = $activeUser->id;
        }

        $results = Cache::remember(
            'features-user-types-get-'.$userId,
            60,
            function() use ($request, $id) {
                $id = $this->requestService->getId($request, $id);

                $query = $this->queryService->buildRequestQuery('features_user_types', $request->toArray(), $id);

                return $this->queryService->parseGet('features_user_types', $request->toArray(), $query);
            }
        );

        return response($results);
    }
}
