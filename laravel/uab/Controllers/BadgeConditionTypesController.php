<?php
namespace Uab\Controllers;

use Cache;
use Illuminate\Http\Request;
use Uab\Controllers\Generated\BadgeConditionTypesController as GeneratedController;

class BadgeConditionTypesController extends GeneratedController {
    public function getRequest(Request $request, $id = null) {
        if (is_null($id)) {
            $results = Cache::remember(
                'badge-condition-types-get',
                60 * 24 * 7,
                function () use ($request, $id) {
                    $id = $this->requestService->getId($request->toArray(), $id);

                    $query = $this->queryService->buildRequestQuery('badge_condition_types', $request->toArray(), $id);

                    return $this->queryService->parseGet('badge_condition_types', $request->toArray(), $query);
                }
            );

            return response($results);
        }

        return parent::get($request, $id);
    }
}
