<?php
namespace Uab\Controllers;

use Uab\Http\Enums\QueueTypesEnum;
use Uab\Jobs\Notifications\NotificationCount;

class NotificationCountController  {
    public function join($activeUserId) {
        NotificationCount::dispatch($activeUserId)->onQueue(QueueTypesEnum::NORMAL);

        return response(
            [
                'success' => true
            ]
        );
    }
}
