<?php
namespace Uab\Controllers;

use Illuminate\Http\Request;
use Uab\Controllers\Generated\BadgesController as GeneratedController;
use Uab\Http\Enums\HttpResponseCodesEnum;
use Uab\Http\Enums\QueueTypesEnum;
use Uab\Jobs\Badges\MarkBadgesRead;
use Uab\Providers\Auth;

class BadgesController extends GeneratedController {
    public function markRead(Request $request) {
        $user = Auth::user();

        if (!is_null($user)) {
            $ids = $request->input('ids', []);

            MarkBadgesRead::dispatch($ids, $user->id)->onQueue(QueueTypesEnum::HIGH);

            return response()->json(
                [
                    'done' => true
                ]
            );
        }

        return response([ 'error' => 'Must be logged in to mark badges.', HttpResponseCodesEnum::CLIENT_ERROR_FORBIDDEN ]);
    }
}
