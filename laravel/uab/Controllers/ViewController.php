<?php
namespace Uab\Controllers;

use Illuminate\Http\Request;
use Uab\Http\Models\Users;
use Uab\Providers\ModelServiceProvider;
use Uab\Providers\QueryServiceProvider;
use View;

class ViewController {
    public $modelService;

    public $model;
    public $request;

    public function __construct() {
        $this->modelService = new ModelServiceProvider();
    }

    public function route(Request $request, $model, $action = false, $id = false) {
        $this->model = $model;

        if ($this->model === 'login') {
            return view('login.create');
        }
        $this->request = $request;

        if ($action === false) {
            $action = 'index';
        }

        $action = $action.'AdminView';

        if ($id) {
            return $this->$action($id);
        } else {
            return $this->$action();
        }
    }

    public function postRoute(Request $request, $model, $action = false, $id = false) {
/*        $model =  $this->modelService->getModel($model);
        $inpItems = $request->all();*/

        if ($request->gender === 'male') {
            $gender = 1;
        } else {
            $gender = 0;
        }
        $user = new Users();

        $userPin = rand (1000, 9999);

        $user->type_diabetes = $request->type_diabetes;

        $user->mobile_number = $request->mobile_number;
        $user->pincode = $userPin;
        $user->gender = $gender;

        $user->save();
        $action = $action.'AdminView';

        //     $model->setRawAttributes($inpItems);
        if (true) {
            return $this->route($request, 'users', 'index');
        }
    }

    public function indexAdminView() {
        $model = $this->modelService->getModel($this->model);

        $all = $model::query()->select()->get();

        return $this->_createView('index')->with('objects', $all);
    }

    public function createAdminView() {
        return $this->_createView('create');
    }

    public function showAdminView($id) {
        $queryService = new QueryServiceProvider();

        $object = $queryService->getById($id, $this->model);

        return $this->_createView('show')->with('object', $object);
    }

    public function editAdminView($id) {
        $queryService = new QueryServiceProvider();

        $object = $queryService->getById($id, $this->model);

        return $this->_createView('edit')->with('object', $object);
    }

    private function _createView($view) {
        $model = $this->modelService->getModel($this->model);

        $table = $model->getTable();

        return View::make($table.'.'.$view);
    }
}
