<?php
namespace Uab\Controllers;

use Uab\Http\Enums\QueueTypesEnum;
use Uab\Jobs\Events\GenerateExerciseSessions;
use Uab\Providers\Auth;
use Illuminate\Http\Request;

class SessionsController {
    public function create(Request $request) {
        $sessions = $request->input('sessions', []);

        $user = Auth::user();
        if (is_null($user)) {
            $user = $request->input('user', null);
        }

        if (!is_null($user)) {
            GenerateExerciseSessions::dispatch($user, $sessions)->onQueue(QueueTypesEnum::LOW);

            return response()->json(
                [
                    'success' => true
                ]
            );
        } else {
            return response()->error('Must be logged in to generate sessions.');
        }
    }
}
