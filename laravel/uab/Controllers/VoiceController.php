<?php
namespace Uab\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use League\OAuth2\Server\Exception\OAuthServerException;
use Twilio\Exceptions\ConfigurationException;
use Twilio\Exceptions\TwilioException;
use Uab\Exceptions\PhoneNumberMissing;
use Uab\Exceptions\PincodeMissing;
use Uab\Exceptions\WrongPincode;
use Uab\Http\Enums\AnswerInputTypesEnum;
use Uab\Http\Models\AnswerOptions;
use Uab\Http\Models\Answers;
use Uab\Http\Models\Questions;
use Uab\Http\Models\SurveyPages;
use Uab\Http\Models\Surveys;
use Uab\Http\Models\SurveysUsers;
use Uab\Http\Models\UserTypes;
use Uab\Providers\Auth;
use Uab\Providers\IvrServiceProvider;
use Uab\Providers\PhoneServiceProvider;
use Uab\Providers\SurveyServiceProvider;
use Uab\Providers\UabVoiceResponse;
use Uab\Controllers\Base\ApiController;
use Uab\Http\Models\Users;
use Uab\Providers\UsersServiceProvider;

class VoiceController extends ApiController {
    private $ivrService;

    public function __construct() {
        parent::__construct();

        $this->ivrService = new IvrServiceProvider();
    }

    /**
     * @param Request $request
     * @param string  $id
     *
     * @return Response
     */
    public function makeCall(Request $request, $id = null) {
        $phone = $request->input('phone', null);

        $user = null;
        if (!is_null($phone)) {
            $phoneService = new PhoneServiceProvider();
            $phone = $phoneService->formatNumber($phone);

            $user = Users::query()->where(
                [
                    'mobile_number' => $phone
                ]
            )->first();
        } else {
            if (is_null($id)) {
                return response()->error('ID or phone number is required to make a call.');
            }

            $id = $request->input('id', $id);
            $id = Users::safeDecodeStatic($id);

            if (is_null($id)) {
                return response()->error('ID or phone number is required to make a call.');
            }

            /** @var Users $user */
            $user = Users::query()->find($id);
        }

        if (is_null($user)) {
            return response()->error('Could not find user for phone call.');
        }

        if (is_null($user->mobile_number)) {
            return response()->error('User must have a mobile phone number to make a call.');
        }

        $call = null;

        try {
            $call = $this->ivrService->makeCall($user);
        } catch (ConfigurationException $e) {
            return response()->error('Configuration not configured for making calls.');
        } catch (TwilioException $e) {
            return response()->error('Twilio was unable to make the call.');
        }

        if (is_null($call)) {
            response()->error('Something went wrong making the call.');
        }

        return response(
            [
                'analytic_track_calls' => $call
            ]
        );
    }

    public function handleAdminPinCheck(Request $request, $isParticipantPhonenumber) {
        $response = new UabVoiceResponse();

        if ($isParticipantPhonenumber) {
            $phoneService = new PhoneServiceProvider();
            $phone = $phoneService->formatNumber($request->input('Digits', null));

            /** @var Users $user */
            $user = Users::query()->where(
                [
                    'mobile_number' => $phone
                ]
            )->first();

            if (!is_null($user)) {
                $userId = $user->id;

                $action = $this->ivrService->getUrl('admin/participant/0?userId=' . $userId);
                $gather = $response->gather(
                    [
                        'action' => $action,
                        'numDigits'  => 1,
                        'method' => 'GET'
                    ]
                );

                $gather->say('Participant found.');

                if (!is_null($user->user_type_id)) {
                    $userService = new UsersServiceProvider();

                    $gender = $userService->getVoiceGender($user);

                    $gather->say('This user\'s voice gender is currently set to ' . $gender);
                }

                $gather->say('To change the user\'s voice gender press 1 for same gender, press 2 for opposite gender, or press 3 for a random gender.');
            } else {
                $response->say('Unable to find participant.');
                $response->redirect($this->ivrService->getUrl('admin/'));
            }
        } else {
            $userTypeEnteredByAdmin = $request->input('Digits', null);
            $participantId = $request->input('userId', null);
            $user = Users::query()->find($participantId);
            $isScheduleSet = $user->preferred_contact_time;
            if (!is_null($participantId)) {
                $sameGenderUserType = UserTypes::query()->where(
                    'name', 'same-gender'
                )->first();

                $oppositeGenderUserType = UserTypes::query()->where(
                    'name', 'opposite-gender'
                )->first();

                $randomGenderUserType = UserTypes::query()->where(
                    'name', 'random-gender'
                )->first();

                if ($userTypeEnteredByAdmin === '1') {
                    $user = Users::query()->find(intval($participantId));
                    $user->user_type_id = $sameGenderUserType->id;
                    $user->save();
                }
                if ($userTypeEnteredByAdmin === '2') {
                    $user = Users::query()->find(intval($participantId));
                    $user->user_type_id = $oppositeGenderUserType->id;
                    $user->save();
                }
                if ($userTypeEnteredByAdmin === '3') {
                    $user = Users::query()->find(intval($participantId));
                    $user->user_type_id = $randomGenderUserType->id;
                    $user->save();
                }
                if (!is_null($isScheduleSet)) {
                    $response->say('Gender Voice Changed Successfully. Thanks for calling dial study. We hope you have a great day!  Goodbye.');
                    $response->hangup();
                } else {
                    $response->say('Gender Voice Assigned Successfully.');
                    $response->redirect($this->ivrService->getUrl('scheduleSet/1/'. intval($participantId)));
                }
            }
        }
        return response($response)->header('Content-Type', 'text/xml');
    }

    public function handleAdminCall(Request $request) {
        $userService = new UsersServiceProvider();

        if (!$userService->isAdmin(Auth::user())) {
            $response = new UabVoiceResponse();
            $response->say('Admins only.');
            $response->hangup();
        } else {
            $response = new UabVoiceResponse();

            $action = $this->ivrService->getUrl('admin/participant/');

            $response->gather(
                [
                    'action' => $action,
                    'numDigits' => 10,
                    'method' => 'GET',
                    'timeout' => 7
                ]
            );

            $response->say('Enter participant phone number that you want to work with:');
        }

        $this->respond($request, $response);
    }

    public function handleAuth(Request $request, $attempts = 0, $activeNumber = null) {
        if (is_string($attempts)) {
            $attempts = intval($attempts);
        }

        $response = new UabVoiceResponse();

        if ($attempts === 0 && is_null($activeNumber)) {
            $welcomeMessage = $this->ivrService->getWelcomeMessage(); // TODO: Change welcome message if incoming vs outgoing

            $response->say($welcomeMessage->welcome_message);
        }

        $phoneService = new PhoneServiceProvider();

        $phone = $request->input('Digits', null);
        $phone = $phoneService->formatNumber($phone);

        if (is_null($phone)) {
            $phone = $phoneService->formatNumber($activeNumber);

            if (is_null($phone)) {
                $phone = $request->input('From', null);
                $phone = $phoneService->formatNumber($phone);

                if (is_null($phone)) {
                    $phone = $request->input('Called', $request->input('To', null));
                    $phone = $phoneService->formatNumber($phone);
                }
            }
        }

        try {
            $user = $this->ivrService->findUser($phone);

            if (is_null($user)) {
                $response->say('Something went wrong. Please call back.');
                $response->hangup();
            } else {
                $isValid = $this->ivrService->validatePinCode($request, $user);

                if ($isValid) {
                    $userService = new UsersServiceProvider();

                    if ($userService->isAdmin($user)) {
                        $response->redirect($this->ivrService->getUrl('admin'));
                    } else {
                        $response->redirect($this->ivrService->getUrl('survey'));
                    }
                }
            }
        } catch (OAuthServerException|PhoneNumberMissing $exception) {
            $response->gather()
                ->setNumDigits(10)
                ->setTimeout(15)
                ->say('The number you are calling from is not registered with an account. Please enter the 10 digit phone number associated for your account.');

            $response->gather()->setAction($this->ivrService->getUrl('auth/'.$attempts.'/'.$phone));
        } catch (PincodeMissing $exception) {
            if ($attempts === 0) {
                $gather = $response->gather()
                    ->setMethod('GET')
                    ->setNumDigits(4)
                    ->setTimeout(10)
                    ->setAction($this->ivrService->getUrl('auth/'.$attempts.'/'.$phone))
                    ->setActionOnEmptyResult(true);

                    $gather->say('Please enter your four digit pin code.');
            } else {
                $response->gather()->say('No pin code. Please call back at your convenience and try again.');
                $response->hangup();
            }
        } catch (WrongPincode $exception) {
            $attempts++;

            if ($attempts === 3) { // TODO: Add max attempts to env
                $response->say('You entered the wrong pin. Talk to your coordinator to get back your pin number.');
                $response->hangup();
            } else {
                $response->gather()
                    ->setNumDigits(4)
                    ->setAction($this->ivrService->getUrl('auth/'.$attempts.'/'.$phone))
                    ->say('You entered the wrong pin. Try again.');
            }
        }

        return $this->respond($request, $response);
    }

    /**
     * @param Request $request
     * @param UabVoiceResponse $voiceResponse
     * @param Surveys $survey
     * @param SurveyPages $surveyPage
     * @param Questions $question
     * @param string|int|null $response
     * @param string|int|null $answerOptionId
     * @param string|int|null $nextQuestionId
     * @return Response
     */
    public function saveAndRespondToQuestion($request, $voiceResponse, $survey, $surveyPage, $question, $response = null, $answerOptionId = null, $nextQuestionId = null) {
        $this->ivrService->savePreviousResponse($survey->id, $question->id, $response, $answerOptionId);
        $this->getNextQuestion($request, $voiceResponse, $survey, $surveyPage, $question, $nextQuestionId);
        return $this->respond($request, $voiceResponse);
    }

    /**
     * @param Request $request
     * @param UabVoiceResponse $voiceResponse
     * @param Surveys $survey
     * @param SurveyPages $surveyPage
     * @param Questions $question
     * @param string|int|null $nextQuestionId
     * @return Response
     */
    public function getNextQuestion($request, $voiceResponse, $survey, $surveyPage, $question, $nextQuestionId = null) {
        if (is_null($nextQuestionId)) {
            if (is_null($question->next_question_function)) {
                $nextQuestion = Questions::query()
                    ->where('position', '>', $question->position)
                    ->where('survey_page_id', '=', $surveyPage->id)
                    ->orderBy('position', 'asc')
                    ->first();
            } else {
                $nextQuestionId = $question->next_question_function;
                $nextQuestion = Questions::query()->find($nextQuestionId);
            }

            if (is_null($nextQuestion)) {
                $userId = Auth::id();
                if (!is_null($userId)) {
                    SurveysUsers::query()
                        ->where(
                            [
                                'user_id' => $userId,
                                'survey_id' => $survey->id,
                                'is_locked' => 0
                            ]
                        )->update(['is_locked' => 1]);

                    //TODO: Weekly tip (May be need a new function, confirm with Jeremy)

                    //TODO: Daily Total (May be need a new function, confirm with Jeremy)
                    $user = Auth::user();

                    $studyDaysNumber = Carbon::parse(Carbon::now())->diffInDays($user->created);
                    $voiceResponse->say('This is day number '.$studyDaysNumber. 'of the DIAL Study.');

                    $completedCallSurveyUsers = SurveysUsers::query()
                        ->where(
                            [
                                'user_id'    => $user->id,
                                'is_locked'  => 1
                            ]
                        )
                        ->get();
                    $completedCallCount = $completedCallSurveyUsers->count();
                    $voiceResponse->say('The total number of calls that you have completed is '.$completedCallCount.'.');

                    $incentiveTotal = $completedCallCount * 0.25;
                    $formattedIncentivesCount = $incentiveTotal * 100;
                    if ($formattedIncentivesCount < 100) {
                        $voiceResponse->say('Your call incentive total as of today is: ' .$formattedIncentivesCount . 'cents.');
                    } else {
                        $dollarAmount = $formattedIncentivesCount / 100;
                        $centsAmount = $formattedIncentivesCount % 100;
                        if ($centsAmount !== 0) {
                            $voiceResponse->say('Your call incentive total as of today is '.intval($dollarAmount). ' dollars and ' .$centsAmount . 'cents.');
                        } else {
                            $voiceResponse->say('Your call incentive total as of today is: ' .intval($dollarAmount). 'dollars.');
                        }
                    }

                    $voiceResponse->say('Thank you. Goodbye.');
                    $voiceResponse->hangup();

                    return $this->respond($request, $voiceResponse);
                } else {
                    $voiceResponse->say('Could not find authenticated user. Goodbye.');
                    $voiceResponse->hangup();

                    return $this->respond($request, $voiceResponse);                }
            } else {
                $nextQuestionId = $nextQuestion->id;
            }
        }

        $voiceResponse->redirect(
            $this->ivrService->getUrl(
                '/survey/'.$survey->id.'/'.$surveyPage->id.'/'.$nextQuestionId
            )
        );
        return $this->respond($request, $voiceResponse);
    }

    public function handleSurvey(Request $request, $surveyId = null, $surveyPageId = null, $questionId = null) {
        // TODO: Split controller into functions
        $voiceResponse = new UabVoiceResponse();

        $user = Auth::user();
        if (is_null($user)) {
            $voiceResponse->say('Unable to validate user.');
            $voiceResponse->hangup();

            return $this->respond($request, $voiceResponse);
        }

        // // // // check if response is given
        $survey = null;
        if (!is_null($surveyId)) {
            $surveyId = Surveys::safeDecodeStatic($surveyId);
            /** @var Surveys|null $survey */
            $survey = Surveys::query()->find($surveyId);
        }

        $surveyPage = null;
        if (!is_null($surveyPageId)) {
            $surveyPageId = SurveyPages::safeDecodeStatic($surveyPageId);
            /** @var SurveyPages|null $surveyPage */
            $surveyPage = SurveyPages::query()->find($surveyPageId);
        }

        $question = null;
        if (!is_null($questionId)) {
            /** @var Questions|null $question */
            $question = Questions::query()->find($questionId);
        }

        $response = $request->input('Digits', null);
        if (!is_null($response) && $response !== '' && !is_null($survey) && !is_null($surveyPage) && !is_null($question)) {
            if ($response === '*') {
                $latestSurveyUser = SurveysUsers::query()
                    ->where('user_id', '=', $user->id)
                    ->where('survey_id', '=', $surveyId)
                    ->where('is_locked', '=', 0)
                    ->orderBy('id', 'DESC')
                    ->first();
                $lastQuestionAnswer = Answers::query()
                    ->where(
                        [
                            'surveys_users_id' => $latestSurveyUser->id
                        ]
                    )->orderBy('id', 'DESC')
                    ->first();

                $previousQuestionId = $lastQuestionAnswer->question_id;
                $previousQuestionId = Questions::safeDecodeStatic($previousQuestionId);

                $previousQuestion = Questions::query()->find($previousQuestionId);
                if (is_null($previousQuestion)) {
                    $voiceResponse->say('Unable to go back.');

                    return $this->respond($request, $voiceResponse);
                } else {
                    $voiceResponse->redirect(
                        $this->ivrService->getUrl(
                            '/survey/'.$survey->id.'/'.$surveyPage->id.'/'.$previousQuestionId
                        )
                    );
                    return $this->respond($request, $voiceResponse);
                }
            } else {
                $surveyUser = SurveysUsers::query()
                    ->where(
                        [
                            'survey_id' => $survey->id,
                            'user_id' => $user->id,
                            'is_locked' => 0
                        ]
                    )->orderBy('response_order', 'asc')
                    ->first();

                if (is_null($surveyUser)) {
                    $responseOrderCount = SurveysUsers::query()
                        ->where(
                            [
                                'survey_id' => $survey->id,
                                'user_id' => $user->id
                            ]
                        )
                        ->count();
                    // TODO: Check and create only if needed
                    $surveyUser = new SurveysUsers();
                    $surveyUser->survey_id = $survey->id;
                    $surveyUser->user_id = $user->id;
                    $surveyUser->response_order = $responseOrderCount + 1;
                    $surveyUser->save();

                    if (is_null($surveyUser)) {
                        $voiceResponse->say('Unable to create survey for user.');
                        $voiceResponse->hangup();

                        return $this->respond($request, $voiceResponse);
                    }
                }

                // TODO: Check response before loading question. If question was null then the user did not answer and the digits are therefore junk
                $answerInputType = $this->ivrService->getAnswerInputType($question->id);

                switch ($answerInputType) {
                    case AnswerInputTypesEnum::CHECK:
                    case AnswerInputTypesEnum::DROP:
                    case AnswerInputTypesEnum::RADIO:
                        /** @var AnswerOptions|null $answerOption */
                        $answerOption = AnswerOptions::query()->where(
                            [
                                'parent_question_id' => $question->id,
                                'answer' => $response
                            ]
                        )->first();

                        if (!is_null($answerOption)) {
                            if (!is_null($answerOption->response) && strlen($answerOption->response) !== '') {
                                $voiceResponse->say($answerOption->response);
                            }

                            return $this->saveAndRespondToQuestion($request, $voiceResponse, $survey, $surveyPage, $question, $response, $answerOption->id, $answerOption->next_question_id);
                        } else {
                            $voiceResponse->say('Invalid option.');

                            $voiceResponse->redirect(
                                $this->ivrService->getUrl(
                                    '/survey/'.$survey->id.'/'.$surveyPage->id.'/'.$question->id
                                )
                            );

                            return $this->respond($request, $voiceResponse);
                        }
                    case AnswerInputTypesEnum::GOAL_STEPS:
                    case AnswerInputTypesEnum::MINUTES:
                    case AnswerInputTypesEnum::STEPS:
                    case AnswerInputTypesEnum::INTEGER:
                        if (is_numeric($response)) {
                            $response = intval($response);

                            return $this->saveAndRespondToQuestion($request, $voiceResponse, $survey, $surveyPage, $question, $response);
                        } else {
                            $voiceResponse->say('Invalid number response.');

                            $voiceResponse->redirect(
                                $this->ivrService->getUrl(
                                    '/survey/'.$survey->id.'/'.$surveyPage->id.'/'.$question->id
                                )
                            );

                            return $this->respond($request, $voiceResponse);
                        }
                    case AnswerInputTypesEnum::DATE:
                    case AnswerInputTypesEnum::DATETIME:
                    case AnswerInputTypesEnum::SHORT_TEXT:
                    case AnswerInputTypesEnum::TEXT:
                    case AnswerInputTypesEnum::TIME:
                    default:
                        return $this->saveAndRespondToQuestion($request, $voiceResponse, $survey, $surveyPage, $question, $response);
                        break;
                }
            }
        }
        else {
            if (is_null($survey)) {
                // TODO: Delete
                $callDirection = $request->input('Direction', null);

                $twilioPhoneNumber = null;
                if ($callDirection === 'outbound-api') {
                    $twilioPhoneNumber = $request->input('From', null);
                } else if ($callDirection === 'inbound') {
                    $twilioPhoneNumber = $request->input('To', null);
                }

                if (is_null($twilioPhoneNumber)) {
                    $voiceResponse->say('Twilio phone number not found.');
                    $voiceResponse->hangup();

                    return $this->respond($request, $voiceResponse);
                }

                $userId = Auth::id();
                SurveysUsers::query()
                    ->where('user_id', '=', $userId)
                    ->where('is_locked', '=', 0)
                    ->where('created', '<', Carbon::now()->subHours(2)->toDateTimeString())
                    ->delete();

                $surveyService = new SurveyServiceProvider();
                $survey = $surveyService->getNextSurvey($twilioPhoneNumber);

                if (is_null($survey)) {
                    $voiceResponse->say('Unable to find survey.');
                    $voiceResponse->hangup();

                    return $this->respond($request, $voiceResponse);
                }
            }

            if (is_null($surveyPage)) {
                $surveyPage = SurveyPages::query()
                    ->where(
                        [
                            'survey_id' => $survey->id
                        ]
                    )->orderBy('position', 'desc')
                    ->first();

                if (is_null($surveyPage)) {
                    $voiceResponse->say('Survey page is missing.');
                    $voiceResponse->hangup();

                    return $this->respond($request, $voiceResponse);
                }
            }

            if (is_null($question)) {
                $surveyUser = SurveysUsers::query()
                    ->where(
                        [
                            'survey_id' => $survey->id,
                            'user_id' => $user->id,
                            'is_locked' => 0
                        ]
                    )->orderBy('response_order', 'asc')
                    ->first();
                if (!is_null($surveyUser)) {
                    $lastSurveyUserAnswer = Answers::query()
                                            ->where(
                                                [
                                                    'surveys_users_id' => $surveyUser->id
                                                ]
                                            )->orderBy('id', 'desc')
                                            ->first();

                    //TODO: Make function
                    if (!is_null($lastSurveyUserAnswer)) {
                        $question = Questions::query()
                            ->where(
                                [
                                    'id' => $lastSurveyUserAnswer->question_id
                                ]
                            )
                            ->first();
                    } else {
                        $question = Questions::query()
                            ->where(
                                [
                                    'survey_page_id' => $surveyPage->id
                                ]
                            )->orderBy('position', 'asc')
                            ->first();
                    }
                } else {
                    $question = Questions::query()
                        ->where(
                            [
                                'survey_page_id' => $surveyPage->id
                            ]
                        )->orderBy('position', 'asc')
                        ->first();
                }

                if (is_null($question)) {
                    $voiceResponse->say('Question is missing.');
                    $voiceResponse->hangup();

                    return $this->respond($request, $voiceResponse);
                }
            }

            $answerInputType = $this->ivrService->getAnswerInputType($question->id);
            if (is_null($answerInputType)) {
                $voiceResponse->say('Could not find answer input type.');
                $voiceResponse->hangup();

                return $this->respond($request, $voiceResponse);
            }

            switch ($answerInputType) {
                case AnswerInputTypesEnum::DATETIME:
                    $numDigits = 8;
                    $timeout = 12;
                    break;
                case AnswerInputTypesEnum::GOAL_STEPS:
                case AnswerInputTypesEnum::INTEGER:
                case AnswerInputTypesEnum::STEPS:
                    $numDigits = 5;
                    $timeout = 11;
                    break;
                case AnswerInputTypesEnum::DATE:
                case AnswerInputTypesEnum::TIME:
                    $numDigits = 4;
                    $timeout = 12;
                break;
                case AnswerInputTypesEnum::MINUTES:
                    $numDigits = 3;
                    $timeout = 11;
                    break;
                case AnswerInputTypesEnum::DESCRIPTION:
                case AnswerInputTypesEnum::SHORT_TEXT:
                case AnswerInputTypesEnum::TEXT:
                case AnswerInputTypesEnum::TITLE:
                    $numDigits = 0;
                    $timeout = 1;
                break;
                case AnswerInputTypesEnum::CHECK:
                case AnswerInputTypesEnum::DROP:
                case AnswerInputTypesEnum::RADIO:
                default:
                    $numDigits = 1;
                    $timeout = 8;
                break;
            }

            if ($numDigits > 0) {
                $gather = $voiceResponse->gather()
                    ->setMethod('GET')
                    ->setNumDigits($numDigits)
                    ->setTimeout($timeout)
                    ->setFinishOnKey('#')
                    ->setAction($this->ivrService->getUrl(
                        '/survey/'.$survey->id.'/'.$surveyPage->id.'/'.$question->id
                    ))
                    ->setActionOnEmptyResult(true);
/*                $gather->setTimeout($timeout);
                $gather->setNumDigits($numDigits);
                $gather->setAction($this->ivrService->getUrl(
                    '/survey/'.$survey->id.'/'.$surveyPage->id.'/'.$question->id
                ));*/
                $gather->say($question->phrasing);
            } else {
                $voiceResponse->say($question->phrasing);
                $this->getNextQuestion($request, $voiceResponse, $survey, $surveyPage, $question);
            }
        }

//        if (!is_null($answer)) {
//            $this->ivrService->addAnswerOptionResponse($voiceResponse, $answer);
//
//            if (is_null($questionId)) {
//                //TODO: check for missed calls
//                $missedCallCount = $this->ivrService->getUserMissedCallCount($userId);
//                if ($missedCallCount >= 1) {
//                    if ($callDirection === 'incoming') {
//                        $missedCall = AnalyticTrackCalls::query()
//                            ->where(
//                                [
//                                    'from_user_id' =>  $userId,
//                                    'status'       =>  'missed'
//                                ]
//                            )->first();
//                    } else {
//                        $missedCall = AnalyticTrackCalls::query()
//                            ->where(
//                                [
//                                    'to_user_id'    =>  $userId,
//                                    'status'          =>  'missed'
//                                ]
//                            )->first();
//                    }
//                    $missedCall->update(['status'  => 'completed']);
//                    $this->ivrService->getUserIncentivesCount($userId);
//                    $voiceResponse->redirect($this->ivrService->getUrl('survey/44/35?direction=' .$callDirection. '&userId=' .$userId));
//                } else {
//                    //TODO: Weekly tip
//                    if (is_null($weeklyTip)) {
//                        $voiceResponse->redirect($this->ivrService->getUrl('survey/43/34?direction=' .$callDirection. '&userId=' .$userId. '&weeklyTip=tip'));
//                    }
//                    //TODO: Weekly tip
//                    $interventionUser = Users::query()
//                        ->where(
//                            [
//                                'id'  =>$userId
//                            ]
//                        )->first();
//
//                    //TODO: Handle incentives (change ivr_voice_gender to incentives field in database)
//                    $userInterventionDayCount = $this->ivrService->getUserInterventionDayCount($interventionUser->approved_on);
//                    $voiceResponse->say('This is day number ' .$userInterventionDayCount. ' of the DIAL Study');
//                    $userCompletedCallCount = $this->ivrService->getUserCompletedCallsCount($userId);
////                        $voiceResponse->say('The total number of calls that you have completed is ' .$userCompletedCallCount);
//                    $incentivesCount = $this->ivrService->getUserIncentivesCount($userId);
//                    if ($incentivesCount < 100) {
//                        $voiceResponse->say('Your call incentive total as of today is: ' .$incentivesCount . 'cents');
//                    } else {
//                        $dollarAmount = $incentivesCount / 100;
//                        $centsAmount = $incentivesCount % 100;
//                        if ($centsAmount !== 0) {
//                            $voiceResponse->say('Your call incentive total as of today is '.intval($dollarAmount). ' dollars and ' .$centsAmount . 'cents');
//                        } else {
//                            $voiceResponse->say('Your call incentive total as of today is: ' .intval($dollarAmount). 'dollars');
//                        }
//                    }
//                }
//                if ($callDirection === 'incoming') {
//                    $action = $this->ivrService->getUrl('scheduleSet/0/'. $userId);
//                    $gather = $voiceResponse->gather(
//                        [
//                            'action' => $action,
//                            'numDigits'  => 1,
//                            'method' => 'GET'
//                        ]
//                    );
//                    $gather->say('Do you want to change your schedule? Press 1 for Yes. Press 2 for No');
//                }
//
//                $voiceResponse = $this->ivrService->getNextQuestion($survey->id, $surveyPage->id, $questionId, $response, $voiceResponse, $previousResponse, $emptyResponseCount, $callDirection, $userId, $weeklyTip);
//            }
//        } else {
//            $voiceResponse = $this->ivrService->getNextQuestion($survey->id, $surveyPage->id, $questionId, $response, $voiceResponse, $previousResponse, $emptyResponseCount, $callDirection, $userId, $weeklyTip);
//        }

        return response($voiceResponse)->header('Content-Type', 'text/xml');
    }

    public function setSchedule(Request $request, $scheduleStatus, $userId) {
        $voiceResponse = new UabVoiceResponse();
        $message = 'Enter preferred time to answer survey every day:';
        $response = $request->input('Digits', null);
        if (intval($scheduleStatus)) {
            if (is_null($response)) {
                $this->ivrService->makeParticipantSchedule($voiceResponse, $message, $userId);
            } else {
                $hours = substr($response, 0, 2);
                $minutes = substr($response, 2, 2);
                if (intval($hours) > 24 || intval($minutes) > 60) {
                    $voiceResponse->say('Enter Valid Time');
                    $this->ivrService->makeParticipantSchedule($voiceResponse, $message, $userId);
                } else {
                    $meeting = Carbon::createFromTime($hours, $minutes);
                    $formattedTime = $meeting->format('g:i A');
                    Users::query()
                        ->where('id', $userId)
                        ->update([
                            'preferred_contact_time'    =>    $formattedTime
                        ]);
                    $voiceResponse->say('Scheduled time added successfully. Thanks for reaching out to dial study. We hope you have a great day!  Goodbye.');
                }
            }
        } else {
            if ($response === '1') {
                $voiceResponse->redirect($this->ivrService->getUrl('scheduleSet/1/'. $userId));
            } else {
                $voiceResponse->say('Thank you for completing your DIAL study call. We hope you have a great day!  Goodbye.');
                $voiceResponse->hangup();
            }
        }
        return response($voiceResponse)->header('Content-Type', 'text/xml');
    }

    /**
     * @param Request $request
     * @param UabVoiceResponse $response
     *
     * @return Response
     */
    private function respond($request, $response) {
        $type = $request->input('type', 'text/xml');

        return response($response)->header('Content-Type', $type);
    }

    public function getMinutesAnswer($answerOptionId) {
        $question = $this->ivrService->getQuestion($answerOptionId);

        if (is_null($question)) {
            return null;
        }

        return $this->getMinutesCurrentQuestion($question->id);
    }

    public function getMinutesCurrentQuestion($questionId) {
        $survey = $this->ivrService->getSurvey($questionId);

        if (!is_null($survey)) {
            $surveyUser = $this->ivrService->getSurveyUser($survey->id);

            if (!is_null($surveyUser)) {
                $surveyUserId = $surveyUser->id;

                if (!is_null($surveyUserId)) {
                    $query = Answers::query()
                        ->where(
                            [
                                'question_id'      => $questionId,
                                'surveys_users_id' => $surveyUserId
                            ]
                        )->orderBy('created', 'desc');

                    $answer = $query->first();

                    return $answer->response;
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    // TODO: delete after confirm
    public function getStepsPastQuestion() {
        $answer = $this->ivrService->getAnswerResponseFromLastCompletedSurvey(AnswerInputTypesEnum::STEPS);

        return $answer->response;
    }

    public function getStepsAnswer($answerOptionId) {
        $question = $this->ivrService->getQuestion($answerOptionId);

        if (is_null($question)) {
            return null;
        }

        return $this->getMinutesCurrentQuestion($question->id);
    }

    public function getStepsCurrentQuestion($questionId) {
        $survey = $this->ivrService->getSurvey($questionId);

        if (!is_null($survey)) {
            $surveyUser = $this->ivrService->getSurveyUser($survey->id);

            if (!is_null($surveyUser)) {
                $surveyUserId = $surveyUser->id;

                if (!is_null($surveyUserId)) {
                    $query = Answers::query()
                        ->where(
                            [
                                'question_id'      => $questionId,
                                'surveys_users_id' => $surveyUserId
                            ]
                        )->orderBy('created', 'desc');

                    $answer = $query->first();

                    return $answer->response;
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public function getGoalStepsPastQuestion() {
        $answer = $this->ivrService->getAnswerResponseFromLastCompletedSurvey(AnswerInputTypesEnum::GOAL_STEPS);

        return $answer->response;
    }

    public function getGoalStepsAnswer($answerOptionId) {
        $question = $this->ivrService->getQuestion($answerOptionId);

        if (is_null($question)) {
            return null;
        }

        return $this->getMinutesCurrentQuestion($question->id);
    }

    public function getGoalStepsCurrentQuestion($questionId) {
        $survey = $this->ivrService->getSurvey($questionId);

        if (!is_null($survey)) {
            $surveyUser = $this->ivrService->getSurveyUser($survey->id);

            if (!is_null($surveyUser)) {
                $surveyUserId = $surveyUser->id;

                if (!is_null($surveyUserId)) {
                    $query = Answers::query()
                        ->where(
                            [
                                'question_id'      => $questionId,
                                'surveys_users_id' => $surveyUserId
                            ]
                        )->orderBy('created', 'desc');

                    $answer = $query->first();

                    return $answer->response;
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
}

