<?php
namespace Uab\Events\Traits;

trait UserBroadcastTrait {
    public $user;

    /**
     * @return array
     */
    public function broadcastWithUser() {
        return [
            'user' => [
                'id' => $this->user->id,
                'display_name' => $this->user->display_name,
                'username' => $this->user->username
            ]
        ];
    }
}
