<?php
namespace Uab\Events\Traits;

use Log;
use Psr\Log\LogLevel;

trait LogTrait {
    public $user;

    /**
     * @param $msg
     * @param string $level
     * @param array $context
     *
     * @return void
     */
    public function log($msg, $level = LogLevel::ERROR, $context = []) {
        Log::write($level, $msg, $context);
    }
}
