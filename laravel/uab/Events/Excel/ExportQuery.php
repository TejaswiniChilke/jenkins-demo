<?php
namespace Uab\Events\Excel;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Queue\SerializesModels;

class ExportQuery {
    use SerializesModels;

    const ALLOW_FORMATS = [
        'csv',
        'html',
        'tsv',
        'ods',
        'xls',
        'xlsx'
    ];

    /** @var string $format */
    public $format;

    /** @var Builder $query */
    public $query;

    /** @var null|string  */
    public $email;

    /**
     * AfterCreate constructor.
     *
     * @param Builder        $query
     * @param string         $format
     * @param string|null $email
     *
     * @return void
     */
    public function __construct($query, $format = 'xlsx', $email = null) {
        $this->setFormat($format);

        $this->query = $query;
        $this->email = $email;
    }

    public function setFormat($format) {
        if (!in_array($format, self::ALLOW_FORMATS)) {
            $format = 'xlsx';
        }

        $this->format = $format;
    }
}
