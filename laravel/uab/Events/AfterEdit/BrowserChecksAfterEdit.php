<?php
namespace Uab\Events\AfterEdit;

use Illuminate\Queue\SerializesModels;

class BrowserChecksAfterEdit {
    use SerializesModels;

    public $table = 'browser_checks';

    public $fields;

    /**
     * AfterEdit constructor.
     *
     * @param mixed[] $fields
     */
    public function __construct($fields) {
        $this->fields = $fields;
    }

    /**
     * @return mixed[]
     */
    public function getFields() {
        return $this->fields;
    }
}
