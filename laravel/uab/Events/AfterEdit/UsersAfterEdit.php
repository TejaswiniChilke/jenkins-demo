<?php
namespace Uab\Events\AfterEdit;

use Illuminate\Queue\SerializesModels;

class UsersAfterEdit {
    use SerializesModels;

    public $table = 'users';

    public $fields;

    /**
     * AfterEdit constructor.
     *
     * @param mixed[] $fields
     */
    public function __construct($fields) {
        $this->fields = $fields;
    }

    /**
     * @return mixed[]
     */
    public function getFields() {
        return $this->fields;
    }
}
