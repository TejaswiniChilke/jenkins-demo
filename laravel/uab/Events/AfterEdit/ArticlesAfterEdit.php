<?php
namespace Uab\Events\AfterEdit;

use Illuminate\Queue\SerializesModels;

class ArticlesAfterEdit {
    use SerializesModels;

    public $table = 'articles';

    public $fields;

    /**
     * AfterEdit constructor.
     *
     * @param mixed[] $fields
     */
    public function __construct($fields) {
        $this->fields = $fields;
    }

    /**
     * @return mixed[]
     */
    public function getFields() {
        return $this->fields;
    }
}
