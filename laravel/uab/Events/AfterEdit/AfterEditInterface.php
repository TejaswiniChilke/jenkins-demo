<?php
namespace Uab\Events\AfterEdit;

use Uab\Http\Models\Base\BaseModel;

interface AfterEditInterface {
    /**
     * AfterCreate constructor.
     *
     * @param BaseModel $model
     * @param mixed[]   $dirty
     */
    function __construct($model, $dirty);

    function getDirty();
    function getId();
    function getTable();
}
