<?php
namespace Uab\Events\AfterEdit;

use Illuminate\Queue\SerializesModels;

class ReportedCommentsAfterEdit {
    use SerializesModels;

    public $table = 'reported_comments';

    public $fields;

    /**
     * AfterEdit constructor.
     *
     * @param mixed[] $fields
     */
    public function __construct($fields) {
        $this->fields = $fields;
    }

    /**
     * @return mixed[]
     */
    public function getFields() {
        return $this->fields;
    }
}
