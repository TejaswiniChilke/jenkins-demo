<?php
namespace Uab\Events\AfterEdit;

use Illuminate\Queue\SerializesModels;

class NotificationsAfterEdit {
    use SerializesModels;

    public $table = 'notifications';

    public $fields;

    /**
     * AfterEdit constructor.
     *
     * @param mixed[] $fields
     */
    public function __construct($fields) {
        $this->fields = $fields;
    }

    /**
     * @return mixed[]
     */
    public function getFields() {
        return $this->fields;
    }
}
