<?php
namespace Uab\Events\AfterEdit;

use Uab\Http\Models\Base\BaseModel;

class AfterEdit implements AfterEditInterface {
    public $dirty;
    public $id;
    public $table;

    /**
     * AfterEdit constructor.
     *
     * @param BaseModel $model
     * @param mixed[]   $dirty
     */
    public function __construct($model, $dirty) {
        $this->id = $model->safeDecode($model->id);
        $this->table = $model->getTable();

        $this->dirty = $dirty;
    }

    /**
     * @return mixed[]
     */
    function getDirty() {
        return $this->dirty;
    }

    /**
     * @return string
     */
    function getId() {
        return $this->id;
    }

    /**
     * @return string
     */
    function getTable() {
        return $this->table;
    }
}
