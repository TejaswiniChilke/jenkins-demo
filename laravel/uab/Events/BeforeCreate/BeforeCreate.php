<?php
namespace Uab\Events\BeforeCreate;

use Uab\Events\AfterCreate\AfterCreateInterface;
use Uab\Http\Models\Base\BaseModel;

class BeforeCreate implements AfterCreateInterface {
    public $model;
    public $object;

    /**
     * AfterCreate constructor.
     *
     * @param BaseModel $model
     */
    public function __construct($model) {
        $this->model = $model->getTable();
        $this->object = $model;
    }

    /**
     * @return BaseModel
     */
    function getModel() {
        return $this->object;
    }

    /**
     * @return string
     */
    function getTable() {
        return $this->model;
    }
}
