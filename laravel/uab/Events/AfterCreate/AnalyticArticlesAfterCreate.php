<?php
namespace Uab\Events\AfterCreate;

use Illuminate\Queue\SerializesModels;

class AnalyticArticlesAfterCreate extends AfterCreate {
    use SerializesModels;
}
