<?php
namespace Uab\Events\AfterCreate;

use Illuminate\Queue\SerializesModels;

class FollowersAfterCreate extends AfterCreate {
    use SerializesModels;
}
