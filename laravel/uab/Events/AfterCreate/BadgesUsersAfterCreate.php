<?php
namespace Uab\Events\AfterCreate;

use Illuminate\Queue\SerializesModels;

class BadgesUsersAfterCreate extends AfterCreate {
    use SerializesModels;
}
