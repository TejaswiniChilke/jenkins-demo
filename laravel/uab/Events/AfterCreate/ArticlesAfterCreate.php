<?php
namespace Uab\Events\AfterCreate;

use Illuminate\Queue\SerializesModels;

class ArticlesAfterCreate extends AfterCreate {
    use SerializesModels;
}
