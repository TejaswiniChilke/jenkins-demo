<?php
namespace Uab\Events\AfterCreate;

use Illuminate\Queue\SerializesModels;

class LoginAttemptsAfterCreate extends AfterCreate {
    use SerializesModels;
}
