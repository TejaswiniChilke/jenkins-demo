<?php
namespace Uab\Events\AfterCreate;

use Illuminate\Queue\SerializesModels;

class ArticlesUsersAfterCreate extends AfterCreate {
    use SerializesModels;
}
