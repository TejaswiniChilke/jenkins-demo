<?php
namespace Uab\Events\AfterCreate;

use Uab\Http\Models\Base\BaseModel;

class AfterCreate implements AfterCreateInterface {
    public $model;
    public $object;

    /**
     * AfterCreate constructor.
     *
     * @param BaseModel $model
     */
    public function __construct($model) {
        $this->model = $model->getTable();
        $this->object = $model;
    }

    /**
     * @return BaseModel
     */
    function getModel() {
        return $this->object;
    }

    /**
     * @return string
     */
    function getTable() {
        return $this->model;
    }
}
