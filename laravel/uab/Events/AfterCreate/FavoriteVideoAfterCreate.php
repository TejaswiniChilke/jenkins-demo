<?php
namespace Uab\Events\AfterCreate;

use Illuminate\Queue\SerializesModels;

class FavoriteVideoAfterCreate extends AfterCreate {
    use SerializesModels;
}
