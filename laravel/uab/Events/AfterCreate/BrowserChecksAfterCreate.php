<?php
namespace Uab\Events\AfterCreate;

use Illuminate\Queue\SerializesModels;

class BrowserChecksAfterCreate extends AfterCreate {
    use SerializesModels;
}
