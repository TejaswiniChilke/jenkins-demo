<?php
namespace Uab\Events\AfterCreate;

use Illuminate\Queue\SerializesModels;

class PostLikesAfterCreate extends AfterCreate {
    use SerializesModels;
}
