<?php
namespace Uab\Events\AfterCreate;

use Illuminate\Queue\SerializesModels;

class EmailsAfterCreate extends AfterCreate {
    use SerializesModels;
}
