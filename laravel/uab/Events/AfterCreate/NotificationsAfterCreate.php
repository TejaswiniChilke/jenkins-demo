<?php
namespace Uab\Events\AfterCreate;

use Illuminate\Queue\SerializesModels;

class NotificationsAfterCreate extends AfterCreate {
    use SerializesModels;
}
