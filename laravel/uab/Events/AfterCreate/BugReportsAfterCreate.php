<?php
namespace Uab\Events\AfterCreate;

use Illuminate\Queue\SerializesModels;

class BugReportsAfterCreate extends AfterCreate {
    use SerializesModels;
}
