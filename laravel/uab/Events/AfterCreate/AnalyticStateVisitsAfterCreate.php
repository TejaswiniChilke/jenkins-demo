<?php
namespace Uab\Events\AfterCreate;

use Illuminate\Queue\SerializesModels;

class AnalyticStateVisitsAfterCreate extends AfterCreate {
    use SerializesModels;
}
