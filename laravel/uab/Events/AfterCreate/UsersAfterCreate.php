<?php
namespace Uab\Events\AfterCreate;

use Illuminate\Queue\SerializesModels;

class UsersAfterCreate extends AfterCreate {
    use SerializesModels;
}
