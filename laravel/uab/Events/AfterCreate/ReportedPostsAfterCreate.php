<?php
namespace Uab\Events\AfterCreate;

use Illuminate\Queue\SerializesModels;

class ReportedPostsAfterCreate extends AfterCreate {
    use SerializesModels;
}
