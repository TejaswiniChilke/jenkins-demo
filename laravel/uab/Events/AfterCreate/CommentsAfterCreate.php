<?php
namespace Uab\Events\AfterCreate;

use Illuminate\Queue\SerializesModels;

class CommentsAfterCreate extends AfterCreate {
    use SerializesModels;
}
