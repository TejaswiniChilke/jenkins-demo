<?php
namespace Uab\Events\AfterCreate;

use Illuminate\Queue\SerializesModels;

class EventsAfterCreate extends AfterCreate {
    use SerializesModels;
}
