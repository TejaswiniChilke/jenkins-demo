<?php
namespace Uab\Events\AfterCreate;

use Illuminate\Queue\SerializesModels;

class AnalyticVideosAfterCreate extends AfterCreate {
    use SerializesModels;
}
