<?php
namespace Uab\Events\AfterCreate;

use Illuminate\Queue\SerializesModels;

class BadgeConditionsUsersAfterCreate extends AfterCreate {
    use SerializesModels;
}
