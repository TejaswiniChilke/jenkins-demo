<?php
namespace Uab\Events\AfterCreate;

use Illuminate\Queue\SerializesModels;

class PostsAfterCreate extends AfterCreate {
    use SerializesModels;
}
