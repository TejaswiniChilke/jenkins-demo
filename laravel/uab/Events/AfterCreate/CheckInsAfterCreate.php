<?php
namespace Uab\Events\AfterCreate;

use Illuminate\Queue\SerializesModels;

class CheckInsAfterCreate extends AfterCreate {
    use SerializesModels;
}
