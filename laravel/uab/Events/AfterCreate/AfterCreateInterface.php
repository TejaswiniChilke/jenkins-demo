<?php
namespace Uab\Events\AfterCreate;

use Uab\Http\Models\Base\BaseModel;

interface AfterCreateInterface {
    /**
     * AfterCreate constructor.
     *
     * @param BaseModel $model
     */
    function __construct($model);

    function getModel();
    function getTable();
}
