<?php
namespace Uab\Events\AfterCreate;

use Illuminate\Queue\SerializesModels;

class ContactStaffMessagesAfterCreate extends AfterCreate {
    use SerializesModels;
}
