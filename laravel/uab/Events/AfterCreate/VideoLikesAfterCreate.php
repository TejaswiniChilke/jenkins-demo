<?php
namespace Uab\Events\AfterCreate;

use Illuminate\Queue\SerializesModels;

class VideoLikesAfterCreate extends AfterCreate {
    use SerializesModels;
}
