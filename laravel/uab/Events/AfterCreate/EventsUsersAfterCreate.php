<?php
namespace Uab\Events\AfterCreate;

use Illuminate\Queue\SerializesModels;

class EventsUsersAfterCreate extends AfterCreate {
    use SerializesModels;
}
