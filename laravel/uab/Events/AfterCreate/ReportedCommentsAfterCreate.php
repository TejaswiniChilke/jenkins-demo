<?php
namespace Uab\Events\AfterCreate;

use Illuminate\Queue\SerializesModels;

class ReportedCommentsAfterCreate extends AfterCreate {
    use SerializesModels;
}
