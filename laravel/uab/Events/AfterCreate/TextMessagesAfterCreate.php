<?php
namespace Uab\Events\AfterCreate;

use Illuminate\Queue\SerializesModels;

class TextMessagesAfterCreate extends AfterCreate {
    use SerializesModels;
}
