<?php
namespace Uab\Events\BeforeEdit;

use Uab\Http\Models\Base\BaseModel;

interface BeforeEditInterface {
    /**
     * AfterCreate constructor.
     *
     * @param BaseModel $model
     */
    function __construct($model);

    function getDirty();
    function getId();
    function getTable();
}
