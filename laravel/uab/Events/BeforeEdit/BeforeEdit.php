<?php
namespace Uab\Events\BeforeEdit;

use Uab\Http\Models\Base\BaseModel;

class BeforeEdit implements BeforeEditInterface {
    public $dirty;
    public $id;
    public $table;

    /**
     * AfterEdit constructor.
     *
     * @param BaseModel $model
     */
    public function __construct($model) {
        $this->id = $model->safeDecode($model->id);
        $this->dirty = $model->getDirty();
        $this->table = $model->getTable();
    }

    /**
     * @return mixed[]
     */
    function getDirty() {
        return $this->dirty;
    }

    /**
     * @return string
     */
    function getId() {
        return $this->id;
    }

    /**
     * @return string
     */
    function getTable() {
        return $this->table;
    }
}
