<?php
namespace Uab\Events\Chat;

use Uab\Events\Traits\UserBroadcastTrait;
use Uab\Http\Models\Users;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UserJoinedChat implements ShouldBroadcast {
    use Dispatchable, InteractsWithSockets, SerializesModels, UserBroadcastTrait;

    public $user;

    /**
     * Create a new event instance.
     *
     * @param Users $user
     *
     * @return void
     */
    public function __construct(Users $user) {
        $this->user = $user;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel[]
     */
    public function broadcastOn() {
        return [
            new Channel('chat')
        ];
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith() {
        return $this->broadcastWithUser();
    }
}
