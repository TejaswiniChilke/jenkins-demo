<?php
namespace Uab\Events\Chat;

use Carbon\Carbon;
use Uab\Events\Traits\UserBroadcastTrait;
use Uab\Http\Enums\QueueTypesEnum;
use Uab\Http\Models\ChatMessages;
use Uab\Http\Models\Users;
use Uab\Jobs\Base\InsertObjectJob;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UserSentChat implements ShouldBroadcast {
    use Dispatchable, InteractsWithSockets, SerializesModels, UserBroadcastTrait;

    public $message;
    public $user;

    /**
     * Create a new event instance.
     *
     * @param Users $user
     * @param string $message
     *
     * @return void
     */
    public function __construct(Users $user, $message) {
        $this->message = $message;
        $this->user = $user;

        $chatMessage = new ChatMessages();
        $chatMessage->message = $message;
        $chatMessage->user_id = $user->id;
        $chatMessage->sent_on = Carbon::now();

        InsertObjectJob::dispatch($chatMessage)->onQueue(QueueTypesEnum::NORMAL);
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel[]
     */
    public function broadcastOn() {
        return [
            new Channel('chat')
        ];
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith() {
        $returnVar = $this->broadcastWithUser();
        $returnVar['message'] = $this->message;

        return $returnVar;
    }
}
