<?php
namespace Uab\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Support\Arr;

class IsTestScope implements Scope {
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  Builder  $builder
     * @param  Model  $model
     *
     * @return void
     */
    public function apply(Builder $builder, Model $model) {
        $env = strtolower(config('APP_ENV'));
        if (!Arr::has(['test', 'testing'], $env)) {
            if ($model->getConnection()->getSchemaBuilder()->hasColumn($model->getTable(), 'is_test')) {
                $builder->where('is_test', '!=', 1)->orWhereNull('is_test');
            }
        }
    }
}
