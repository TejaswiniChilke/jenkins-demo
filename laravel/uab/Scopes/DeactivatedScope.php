<?php
namespace Uab\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Uab\Http\Enums\UserStatusesEnum;

class DeactivatedScope implements Scope {
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  Builder  $builder
     * @param  Model  $model
     *
     * @return void
     */
    public function apply(Builder $builder, Model $model) {
        if ($model->getConnection()->getSchemaBuilder()->hasColumn($model->getTable(), 'status')) {
            $builder->where('status', '!=', UserStatusesEnum::DEACTIVATED);
        }
    }
}
