<?php
namespace Uab\Broadcasts;

use Illuminate\Broadcasting\Channel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class BroadcastNotificationCount implements ShouldBroadcast {
    use Dispatchable;
    use InteractsWithSockets;

    public $count;
    public $userSlug;

    /**
     * Create a new event instance.
     *
     * @param string $userSlug
     * @param number $count
     *
     * @return void
     */
    public function __construct($userSlug, $count) {
        $this->userSlug = $userSlug;
        $this->count = $count;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel[]
     */
    public function broadcastOn() {
        $channel = new Channel('notifications.' . $this->userSlug);

        return [
            $channel
        ];
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith() {
        return [
            'count' => $this->count
        ];
    }
}
