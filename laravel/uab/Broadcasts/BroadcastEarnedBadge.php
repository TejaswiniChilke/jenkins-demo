<?php
namespace Uab\Broadcasts;

use Uab\Http\Models\Users;
use Illuminate\Broadcasting\Channel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class BroadcastEarnedBadge implements ShouldBroadcast {
    use Dispatchable;
    use InteractsWithSockets;

    public $badges;
    public $userId;

    /**
     * Create a new event instance.
     *
     * @param $userId
     * @param $badges
     *
     * @return void
     */
    public function __construct($userId, $badges) {
        $this->userId = $userId;
        $this->badges = $badges;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel[]
     */
    public function broadcastOn() {
        /** @var Users $model */
        $model = Users::query()->find($this->userId);

        if (!is_null($model)) {
            $slug = $model->slug();

            $channel = new Channel('earned-badges.' . $slug);

            return [
                $channel
            ];
        }

        return [];
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith() {
        return [
            'badges' => $this->badges
        ];
    }
}
