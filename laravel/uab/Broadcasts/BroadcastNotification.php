<?php
namespace Uab\Broadcasts;

use Uab\Http\Models\Notifications;
use Illuminate\Broadcasting\Channel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Uab\Http\Models\Users;

class BroadcastNotification implements ShouldBroadcast {
    use Dispatchable, InteractsWithSockets;

    public $notification;

    /**
     * Create a new event instance.
     *
     * @param Notifications $notification
     *
     * @return void
     */
    public function __construct(Notifications $notification) {
        $this->notification = $notification;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel[]
     */
    public function broadcastOn() {
        /** @var Users $user */
        $user = Users::query()->find($this->notification->user_id);

        if (!is_null($user)) {
            $slug = $user->slug();

            return [
                'notifications.' . $slug
            ];
        }

        return [];
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith() {
        return [
            'notification' => [
                'id' => $this->notification->id,
                'message' => $this->notification->message,
                'reference_name' => $this->notification->reference_name,
                'reference_value' => $this->notification->reference_value,
                'type' => $this->notification->type
            ]
        ];
    }
}
