<?php
namespace Uab\Jobs\Notifications;

use Uab\Http\Enums\QueueTypesEnum;
use Uab\Http\Models\Emails;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Uab\Http\Models\TextMessages;
use Uab\Http\Models\Users;
use Exception;
use Uab\Jobs\Emails\SendAndCreateEmail;
use Uab\Jobs\Emails\SendEmail;
use Uab\Providers\TextMessageServiceProvider;
use Uab\Providers\UsersServiceProvider;

class ContactUserType implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $subject;
    private $message;
    private $type;
    private $userTypes;

    /**
     * Create a new job instance.
     *
     * @param string|string[] $userTypes
     * @param string          $message
     * @param string|null     $subject
     * @param string          $type
     */
    public function __construct($userTypes, $message, $subject = null, $type = 'custom') {
        $this->subject = is_null($subject) ? 'New Notification' : $subject;

        $this->message = $message;

        if (!is_array($userTypes)) {
            $userTypes = [
                $userTypes
            ];
        }

        $this->type = $type;

        $this->userTypes = $userTypes;
    }

    /**
     * Execute the job.
     *
     * @return void
     *
     * @throws Exception
     */
    public function handle() {
        $service = new UsersServiceProvider();

        /** @var Users[] $users */
        $users = $service->getUsersByUserTypeNames($this->userTypes);

        foreach ($users as $user) {
            switch ($user->preferred_contact_type) {
                case 'text':
                    if (!is_null($user->mobile_number)) {
                        $this->sendText($user);
                        break;
                    }
                case 'email':
                default:
                    if (!is_null($user->email)) {
                        $this->sendEmail($user);
                    }
                    break;
            }
        }
    }

    /**
     * @param Users $user
     */
    private function sendEmail(Users $user) {
        $email = new Emails();
        $email->subject = $this->subject;
        $email->message = $this->message;
        $email->to_email = $user->email;
        $email->to_user_id = $user->id;
        $email->save();

        SendEmail::dispatch($email)->onQueue(QueueTypesEnum::NORMAL);
    }

    /**
     * @param Users $user
     *
     * @throws Exception
     */
    private function sendText($user) {
        $textMessageService = new TextMessageServiceProvider();

        $textMessage = new TextMessages();
        $textMessage->to_user_id = $user->id;
        $textMessage->to_number = $user->mobile_number;
        $textMessage->message = $this->message;
        $textMessage->save();

        $textMessage = $textMessageService->sendSms($textMessage);

        if (is_null($textMessage) || !is_null($textMessage->error_code)) {
            throw new Exception('Unable to send text message.');
        }
    }
}
