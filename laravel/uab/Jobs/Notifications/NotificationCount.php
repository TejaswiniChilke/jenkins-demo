<?php
namespace Uab\Jobs\Notifications;

use Carbon\Carbon;
use Uab\Broadcasts\BroadcastNotificationCount;
use Uab\Http\Models\Notifications;
use Uab\Http\Models\Users;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use \Exception;

class NotificationCount implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $created;
    private $userId;

    /**
     * Create a new job instance.
     *
     * @param string|number $userId
     */
    public function __construct($userId) {
        $this->created = Carbon::now();
        $this->userId = Users::safeDecodeStatic($userId);
    }

    /**
     * Execute the job.
     *
     * @return void
     *
     * @throws Exception
     */
    public function handle() {
        $expires = $this->created->addMinutes(5);

        if (Carbon::now()->isBefore($expires)) {
            $model = Users::query()->find($this->userId);

            if (!is_null($model)) {
                $slug = $model->slug();

                $count = Notifications::query()
                    ->where(
                        [
                            'has_read' => false,
                            'user_id' => $this->userId
                        ]
                    )->count();

                event(new BroadcastNotificationCount($slug, $count));
            }
        }
    }

    public function fail($exception = null) {
        return;
    }
}
