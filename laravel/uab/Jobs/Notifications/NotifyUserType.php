<?php
namespace Uab\Jobs\Notifications;

use Uab\Http\Enums\QueueTypesEnum;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Uab\Http\Models\Notifications;
use Uab\Http\Models\Users;
use Exception;
use Uab\Jobs\Base\InsertObjectJob;
use Uab\Providers\UsersServiceProvider;

class NotifyUserType implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $subject;
    private $message;
    private $type;
    private $userTypes;

    /**
     * Create a new job instance.
     *
     * @param string|string[] $userTypes
     * @param string          $message
     * @param string|null     $subject
     * @param string          $type
     */
    public function __construct($userTypes, $message, $subject = null, $type = 'custom') {
        $this->subject = is_null($subject) ? 'New Notification' : $subject;

        $this->message = $message;

        if (!is_array($userTypes)) {
            $userTypes = [
                $userTypes
            ];
        }

        $this->type = $type;

        $this->userTypes = $userTypes;
    }

    /**
     * Execute the job.
     *
     * @return void
     *
     * @throws Exception
     */
    public function handle() {
        $service = new UsersServiceProvider();

        /** @var Users[] $users */
        $users = $service->getUsersByUserTypeNames($this->userTypes);

        foreach ($users as $user) {
            $this->sendNotification($user);
        }
    }

    /**
     * @param Users $user
     */
    private function sendNotification($user) {
        $notification = new Notifications();
        $notification->message = $this->message;
//        $notification->reference_value = $this->referenceValue; // TODO
        $notification->type = $this->type;
        $notification->has_read = false;
        $notification->user_id = $user->id;

        InsertObjectJob::dispatch($notification)->onQueue(QueueTypesEnum::NORMAL);
    }
}
