<?php
namespace Uab\Jobs\Emails;

use Uab\Http\Enums\QueueTypesEnum;
use Uab\Http\Models\Emails;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Uab\Jobs\Base\UpdateObjectJob;

class MarkEmailRead implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable;

    private $id;
    private $time;

    /**
     * Create a new job instance.
     *
     * @param string $id
     * @param string $time
     */
    public function __construct($id, $time) {
        $this->id = $id;
        $this->time = $time;
    }

    /**
     * Execute the job.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function handle() {
        /** @var Emails $email */
        $email = Emails::findBySlug($this->id);

        if (!is_null($email)) {
            $email->read_at = $this->time;

            UpdateObjectJob::dispatch($email)->onQueue(QueueTypesEnum::LOW);
        }
    }
}
