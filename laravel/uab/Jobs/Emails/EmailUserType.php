<?php
namespace Uab\Jobs\Emails;

use Uab\Http\Enums\QueueTypesEnum;
use Uab\Http\Models\Emails;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Uab\Http\Models\Users;
use Exception;
use Uab\Providers\UsersServiceProvider;

class EmailUserType implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $subject;
    private $message;
    private $userTypes;

    /**
     * Create a new job instance.
     *
     * @param string|string[] $userTypes
     * @param string          $message
     * @param string|null     $subject
     */
    public function __construct($userTypes, string $message, ?string $subject = null) {
        $this->subject = is_null($subject) ? 'New Notification' : $subject;

        $this->message = $message;

        if (!is_array($userTypes)) {
            $userTypes = [
                $userTypes
            ];
        }

        $this->userTypes = $userTypes;
    }

    /**
     * Execute the job.
     *
     * @return void
     *
     * @throws Exception
     */
    public function handle() {
        $service = new UsersServiceProvider();

        /** @var Users[] $users */
        $users = $service->getUsersByUserTypeNames($this->userTypes);

        foreach ($users as $user) {
            if (!is_null($user->email)) {
                $email = new Emails();
                $email->subject = $this->subject;
                $email->message = $this->message;
                $email->to_email = $user->email;
                $email->to_user_id = $user->id;
                $email->save();

                SendEmail::dispatch($email)->onQueue(QueueTypesEnum::NORMAL);
            }
        }
    }
}
