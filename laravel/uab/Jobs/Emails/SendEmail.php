<?php
namespace Uab\Jobs\Emails;

use Illuminate\Support\Arr;
use Uab\Http\Models\EmailAttachments;
use Uab\Http\Models\Emails;
use Uab\Providers\EmailServiceProvider;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Exception;

class SendEmail implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $email;
    private $attachments = [];

    /**
     * Create a new job instance.
     *
     * @param Emails                              $email
     * @param EmailAttachments|EmailAttachments[] $attachments
     */
    public function __construct(Emails $email, $attachments = []) {
        if (!is_array($attachments)) {
            $attachments = [$attachments];
        }

        foreach ($attachments as $attachment) {
            $this->attachments[] = $attachment->toArray();
        }

        $this->email = $email->toArray();
    }

    /**
     * Execute the job.
     *
     * @return void
     *
     * @throws Exception
     */
    public function handle() {
        $service = new EmailServiceProvider();

        $email = new Emails();
        if (Arr::has($this->email, 'id')) {
            $id = Arr::get($this->email, 'id');

            $email = Emails::query()->find($id);

            if (is_null($email)) {
                throw new Exception(
                    'Unable to send email ('.$id.') - ' . json_encode($this->email)
                );
            }
        } else {
            $email->setRawAttributes($this->email);
        }

        $email = $service->send($email, $this->attachments);
        if (is_null($email)) {
            throw new Exception('Unable to send email from queue.');
        }
    }

    public function fail($exception = null) {
        $this->release(10);
    }
}
