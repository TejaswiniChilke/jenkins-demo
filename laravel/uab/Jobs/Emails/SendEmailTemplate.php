<?php
namespace Uab\Jobs\Emails;

use Uab\Http\Models\Emails;
use Uab\Http\Models\EmailTemplateLogs;
use Uab\Http\Models\EmailTemplates;
use Uab\Http\Models\Users;
use Uab\Jobs\Base\InsertObjectJob;
use Uab\Providers\EmailServiceProvider;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Uab\Http\Enums\QueueTypesEnum;

class SendEmailTemplate implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var EmailTemplates */
    private $template;

    /** @var Users  */
    private $user;

    /**
     * Create a new job instance.
     *
     * @param EmailTemplates $template
     * @param Users          $user
     */
    public function __construct($template, $user) {
        $this->template = $template;
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function handle() {
        $service = new EmailServiceProvider();

        if  (!$service->hasSentTemplate($this->template, $this->user)) {
            $email = new Emails();
            $email->to_user_id = $this->user->id;
            $email->to_email = $this->user->email;
            $email->subject = $this->template->subject;
            $email->message = $this->template->message;
            $email->save();

            SendEmail::dispatch($email)->onQueue(QueueTypesEnum::EMAIL);

            $log = new EmailTemplateLogs();
            $log->email_template_id = $this->template->id;
            $log->user_id = $this->user->id;

            InsertObjectJob::dispatch($log)->onQueue(QueueTypesEnum::LOW);
        }
    }
}
