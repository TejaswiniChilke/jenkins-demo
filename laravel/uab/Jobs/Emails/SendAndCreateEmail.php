<?php
namespace Uab\Jobs\Emails;

use Uab\Http\Enums\QueueTypesEnum;
use Uab\Http\Models\EmailAttachments;
use Uab\Http\Models\Emails;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Exception;

class SendAndCreateEmail implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $email;
    private $attachments = [];

    /**
     * Create a new job instance.
     *
     * @param Emails                              $email
     * @param EmailAttachments|EmailAttachments[] $attachments
     */
    public function __construct(Emails $email, $attachments = []) {
        if (!is_array($attachments)) {
            $attachments = [$attachments];
        }

        foreach ($attachments as $attachment) {
            $this->attachments[] = $attachment->getDirty();
        }

        $this->email = $email->getDirty();
    }

    /**
     * Execute the job.
     *
     * @return void
     *
     * @throws Exception
     */
    public function handle() {
        $email = new Emails();
        $email->setRawAttributes($this->email);

        $success = $email->save();

        if ($success) {
            foreach ($this->attachments as $i => $attachmentAttributes) {
                $attachment = new EmailAttachments();
                $attachment->setRawAttributes($attachmentAttributes);

                $success = $attachment->save();

                if ($success) {
                    $this->attachments[$i] = $attachment;
                } else {
                    throw new Exception(
                        'Unable to create the email attachment.'
                    );
                }
            }

            SendEmail::dispatch($email, $this->attachments)->onQueue(QueueTypesEnum::EMAIL);
        } else {
            throw new Exception(
                'Unable to create the email.'
            );
        }
    }

    public function fail($exception = null) {
        $this->release(10);
    }
}
