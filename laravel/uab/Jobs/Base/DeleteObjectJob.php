<?php
namespace Uab\Jobs\Base;

use Uab\Http\Models\Base\BaseModel;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use \Exception;
use Uab\Providers\ModelServiceProvider;

class DeleteObjectJob implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $id;
    private $object;
    private $table;

    /**
     * Create a new job instance.
     *
     * @param BaseModel $object
     */
    public function __construct($object) {
        $this->id = $object->id;
        $this->table = $object->getTable();
        $this->object = $object->getDirty();
    }

    /**
     * Execute the job.
     *
     * @return void
     *
     * @throws Exception
     */
    public function handle() {
        $success = false;

        if (is_string($this->table) && is_array($this->object)) {
            $modelService = new ModelServiceProvider();

            /** @var BaseModel $model */
            $model = $modelService->getModel($this->table);
            $model = $model::query()->find($this->id);

            if (!is_null($model)) {
                $model = $model->setRawAttributes($this->object);

                $success = $model->delete();
            }
        }

        if (!$success) {
            throw new Exception(
                'Unable to delete object ('.$this->table.')'
            );
        }
    }

    public function fail($exception = null) {
        $this->release(10);
    }
}
