<?php
namespace Uab\Jobs\Base;

use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use DB;

class UpdateFieldsJob implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $fields;
    private $table;

    /**
     * Create a new job instance.
     *
     * @param string  $table
     * @param mixed[] $fields
     */
    public function __construct($table, $fields) {
        $this->table = $table;
        $this->fields = $fields;
    }

    /**
     * Execute the job.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function handle() {
        $table = DB::table($this->table);

        $success = $table->update($this->fields);
        if ($success === false) {
            throw new Exception(
                'Unable to insert object from queue ('.$this->table.')'
            );
        }
    }

    public function fail($exception = null) {
        $this->release(10);
    }
}
