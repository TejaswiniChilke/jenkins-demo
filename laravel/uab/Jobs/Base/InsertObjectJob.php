<?php
namespace Uab\Jobs\Base;

use Uab\Http\Models\Base\BaseModel;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use \Exception;
use Uab\Providers\UpdateServiceProvider;

class InsertObjectJob implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $object;
    private $table;

    /**
     * Create a new job instance.
     *
     * @param BaseModel $object
     */
    public function __construct($object) {
        $this->table = $object->getTable();
        $this->object = $object->getDirty();
    }

    /**
     * Execute the job.
     *
     * @return void
     *
     * @throws Exception
     */
    public function handle() {
        $success = false;

        if (is_string($this->table) && is_array($this->object)) {
            $service = new UpdateServiceProvider();

            $success = $service->create($this->table, $this->object);
        }

        if (!$success) {
            throw new Exception(
                'Unable to insert object from queue ('.$this->table.')'
            );
        }
    }

    public function fail($exception = null) {
        $this->release(10);
    }
}
