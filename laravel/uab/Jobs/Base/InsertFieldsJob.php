<?php
namespace Uab\Jobs\Base;

use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Uab\Providers\UpdateServiceProvider;

class InsertFieldsJob implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $fields;
    private $table;

    /**
     * Create a new job instance.
     *
     * @param string  $table
     * @param mixed[] $fields
     */
    public function __construct($table, $fields) {
        $this->table = $table;
        $this->fields = $fields;
    }

    /**
     * Execute the job.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function handle() {
        $success = false;

        if (is_string($this->table) && is_array($this->fields)) {
            $service = new UpdateServiceProvider();

            $success = $service->create($this->table, $this->fields);
        }

        if (!$success) {
            throw new Exception(
                'Unable to insert fields from queue ('.$this->table.')'
            );
        }
    }

    public function fail($exception = null) {
        $this->release(10);
    }
}
