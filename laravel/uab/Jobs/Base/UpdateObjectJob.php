<?php
namespace Uab\Jobs\Base;

use Exception;
use Uab\Http\Models\Base\BaseModel;
use Uab\Providers\ModelServiceProvider;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class UpdateObjectJob implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $id;
    private $object;
    private $model;

    /**
     * Create a new job instance.
     *
     * @param BaseModel $object
     */
    public function __construct($object) {
        $this->id = $object->id;
        $this->model = $object->getTable();
        $this->object = $object->getDirty();
    }

    /**
     * Execute the job.
     *
     * @return void
     *
     * @throws Exception
     */
    public function handle() {
        $service = new ModelServiceProvider();

        /** @var BaseModel $model */
        $model = $service->getModel($this->model);
        $model = $model::query()->find($this->id);

        if (!is_null($model)) {
            $model = $model->setRawAttributes($this->object);

            $success = $model->save();

            if (!$success) {
                throw new Exception(
                    'Unable to update object from queue (' . $this->model . ')'
                );
            }
        }
    }

    public function fail($exception = null) {
        $this->release(30);
    }
}
