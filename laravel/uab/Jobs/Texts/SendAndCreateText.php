<?php
namespace Uab\Jobs\Texts;

use Uab\Http\Enums\QueueTypesEnum;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Exception;
use Uab\Http\Models\TextMessages;

class SendAndCreateText implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $text;

    /**
     * Create a new job instance.
     *
     * @param TextMessages $text
     */
    public function __construct(TextMessages $text) {
        $this->text = $text->getDirty();
    }

    /**
     * Execute the job.
     *
     * @return void
     *
     * @throws Exception
     */
    public function handle() {
        $text = new TextMessages();
        $text->setRawAttributes($this->text);

        $success = $text->save();

        if ($success) {
            SendText::dispatch($text)->onQueue(QueueTypesEnum::TEXT);
        } else {
            throw new Exception(
                'Unable to create the email.'
            );
        }
    }

    public function fail($exception = null) {
        $this->release(10);
    }
}
