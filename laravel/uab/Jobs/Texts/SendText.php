<?php
namespace Uab\Jobs\Texts;

use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Log;
use Uab\Http\Models\TextMessages;
use Uab\Providers\TextMessageServiceProvider;

class SendText implements ShouldQueue {
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    private $id;

    /**
     * Create a new job instance.
     *
     * @param TextMessages $text
     */
    public function __construct(TextMessages $text) {
        $this->id = $text->id;
    }

    /**
     * Execute the job.
     *
     * @return void
     *
     * @throws Exception
     */
    public function handle() {
        /** @var TextMessages|null $text */
        $text = TextMessages::query()->find($this->id);
        if (is_null($text)) {
            throw new Exception(
                'Unable to find text message for sending ('.$text->id.').'
            );
        }

        $service = new TextMessageServiceProvider();
        $sent = $service->sendSms($text);

        if (is_null($sent)) {
            Log::warning('Unable to send queued text message ('.$text->id.').');
        }
    }

    public function fail($exception = null) {
        $this->release(10);
    }
}
