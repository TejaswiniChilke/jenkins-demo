<?php
namespace Uab\Jobs\Texts;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Uab\Http\Models\TextMessages;
use Uab\Http\Models\Users;
use Exception;
use Uab\Providers\TextMessageServiceProvider;
use Uab\Providers\UsersServiceProvider;

class TextUserType implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $subject;
    private $message;
    private $type;
    private $userTypes;

    /**
     * Create a new job instance.
     *
     * @param string|string[] $userTypes
     * @param string          $message
     * @param string|null     $subject
     * @param string          $type
     */
    public function __construct($userTypes, $message) {
        $this->message = $message;

        if (!is_array($userTypes)) {
            $userTypes = [
                $userTypes
            ];
        }

        $this->userTypes = $userTypes;
    }

    /**
     * Execute the job.
     *
     * @return void
     *
     * @throws Exception
     */
    public function handle() {
        $service = new UsersServiceProvider();

        /** @var Users[] $users */
        $users = $service->getUsersByUserTypeNames($this->userTypes);

        foreach ($users as $user) {
            if (!is_null($user->mobile_number)) {
                $this->sendText($user);
                break;
            }
        }
    }

    /**
     * @param Users $user
     *
     * @throws Exception
     */
    private function sendText($user) {
        $textMessageService = new TextMessageServiceProvider();

        $textMessage = new TextMessages();
        $textMessage->to_user_id = $user->id;
        $textMessage->to_number = $user->mobile_number;
        $textMessage->message = $this->message;
        $textMessage->save();

        $textMessage = $textMessageService->sendSms($textMessage);

        if (is_null($textMessage) || !is_null($textMessage->error_code)) {
            throw new Exception('Unable to send text message.');
        }
    }
}
