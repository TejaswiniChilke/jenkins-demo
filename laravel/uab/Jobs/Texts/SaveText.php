<?php
namespace Uab\Jobs\Texts;

use Uab\Http\Enums\QueueTypesEnum;
use Uab\Http\Models\TextMessages;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Uab\Http\Models\TextMessageStops;
use Uab\Jobs\Base\DeleteObjectJob;
use Uab\Jobs\Base\InsertObjectJob;
use Uab\Providers\PhoneServiceProvider;
use \Exception;
use Uab\Providers\TextMessageServiceProvider;

class SaveText implements ShouldQueue {
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    private $text;

    /**
     * Create a new job instance.
     *
     * @param TextMessages $text
     */
    public function __construct($text) {
        $this->text = $text->getDirty();
    }

    /**
     * Execute the job.
     *
     * @return void
     *
     * @throws Exception
     */
    public function handle() {
        $text = new TextMessages();
        $text->setRawAttributes($this->text);

        $message = trim(strtolower($text->message));

        $textService = new TextMessageServiceProvider();
        $isStart = $textService->isStart($message);
        $isStop = $textService->isStop($message);

        if ($isStart || $isStop) {
            $phoneService = new PhoneServiceProvider();
            $number = $phoneService->formatNumber($text->from_number);

            $previousStop = null;
            if (!is_null($number)) {
                $previousStop = TextMessageStops::query()
                    ->where(
                        [
                            'from_number' => $number
                        ]
                    )->orderBy('created_at', 'DESC')
                    ->first();

                if ($isStop) {
                    if (!is_null($previousStop)) {
                        DeleteObjectJob::dispatch($previousStop)->onQueue(QueueTypesEnum::NORMAL);
                    }

                    $stop = new TextMessageStops();
                    $stop->from_number = $number;
                    $stop->stop_message = $text->message;

                    InsertObjectJob::dispatch($stop)->onQueue(QueueTypesEnum::HIGH);
                } else {
                    if (!is_null($previousStop)) {
                        $previousStop->start_message = $text->message;
                        $previousStop->save();

                        DeleteObjectJob::dispatch($previousStop)->onQueue(QueueTypesEnum::HIGH);
                    }
                }
            }
        }

        InsertObjectJob::dispatch($text)->onQueue(QueueTypesEnum::NORMAL);

        if (!$text) {
            throw new Exception(
                'Unable to send text message ('.$text->id.').'
            );
        }
    }

    public function fail($exception = null) {
        $this->release(10);
    }
}
