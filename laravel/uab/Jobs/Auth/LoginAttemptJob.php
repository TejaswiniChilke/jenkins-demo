<?php
namespace Uab\Jobs\Auth;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Jenssegers\Agent\Agent;
use Uab\Http\Enums\QueueTypesEnum;
use Uab\Http\Models\LoginAttempts;
use Uab\Http\Models\Users;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Uab\Jobs\Base\InsertObjectJob;

class LoginAttemptJob implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $agent;
    private $request;
    private $success;
    private $username;

    /**
     * Create a new job instance.
     *
     * @param string  $username
     * @param boolean $success
     */
    public function __construct($username, $success) {
        $this->agent = new Agent();
        $this->request = Request::capture();
        $this->success = $success;
        $this->username = $username;
    }

    /**
     * Execute the job.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function handle() {
        $userLoginAttempt = new LoginAttempts();

        /** @var Users|null $user */
        $user = Users::query()
            ->where('username', $this->username)
            ->orWhere('email', $this->username)
            ->first();

        if (!is_null($user)) {
            $userLoginAttempt->user_id = $user->id;
        }

        $userLoginAttempt->ip_address = $this->request->ip();
        $userLoginAttempt->username = $this->username;
        $userLoginAttempt->success = $this->success;
        $userLoginAttempt->attempted_at = Carbon::now();

        if ($this->agent->isRobot()){
            $userLoginAttempt->robot_name = $this->agent->robot();
        }

        $userLoginAttempt->languages = implode(',  ', $this->agent->languages());

        $browser = $this->agent->browser();
        if (strlen($browser) !== 0) {
            $userLoginAttempt->browser = $browser;

            $browserVersion = $this->agent->version($browser);
            if (strlen($browserVersion) !== 0) {
                $userLoginAttempt->browser = $userLoginAttempt->browser . '@' . $browserVersion;
            }
        }

        $platform = $this->agent->platform();
        if (strlen($platform) !== 0) {
            $userLoginAttempt->platform = $platform;

            $platformVersion = $this->agent->version($platform);
            if (strlen($platformVersion) !== 0) {
                $userLoginAttempt->platform = $userLoginAttempt->platform . '@' . $platformVersion;
            }
        }

        $userLoginAttempt->device = $this->agent->device();

        $userLoginAttempt->is_desktop = $this->agent->isDesktop();
        $userLoginAttempt->is_mobile = $this->agent->isMobile();
        $userLoginAttempt->is_phone = $this->agent->isPhone();
        $userLoginAttempt->is_tablet = $this->agent->isTablet();

        InsertObjectJob::dispatch($userLoginAttempt)->onQueue(QueueTypesEnum::LOW);
    }

    public function fail($exception = null) {
        $this->release(10);
    }
}
