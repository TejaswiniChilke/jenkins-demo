<?php
namespace Uab\Jobs\Qualtrics;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use \Exception;
use Uab\Http\Enums\QueueTypesEnum;
use Uab\Providers\Qualtrics\QualtricsServiceProvider;

class CheckQualtricsSurveyProgressJob implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $progressId;
    private $surveyId;

    /**
     * Create a new job instance.
     *
     * @param string $surveyId
     * @param string $progressId
     */
    public function __construct($surveyId, $progressId) {
        $this->surveyId = $surveyId;
        $this->progressId = $progressId;
    }

    /**
     * Execute the job.
     *
     * @return void
     *
     * @throws Exception
     */
    public function handle() {
        $client = new QualtricsServiceProvider();

        $response = $client->checkExportProgress($this->surveyId, $this->progressId);

        if (isset($response->fileId) && !is_null($response->fileId)) {
            ImportQualtricsSurveysJob::dispatch($this->surveyId, $response->fileId)
                ->onQueue(QueueTypesEnum::NORMAL);
        } else {
            CheckQualtricsSurveyProgressJob::dispatch($this->surveyId, $this->progressId)
                ->onQueue(QueueTypesEnum::NORMAL)
                ->delay(Carbon::now()->addMinutes(2));
        }
    }

    public function fail($exception = null) {
        $this->release(30);
    }
}
