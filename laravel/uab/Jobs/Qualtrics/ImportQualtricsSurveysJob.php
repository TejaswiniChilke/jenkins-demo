<?php
namespace Uab\Jobs\Qualtrics;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use \Exception;
use Uab\Http\Enums\QueueTypesEnum;
use Uab\Providers\Qualtrics\QualtricsServiceProvider;

class ImportQualtricsSurveysJob implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $fileId;
    private $surveyId;

    /**
     * Create a new job instance.
     *
     * @param string $surveyId
     * @param string $fileId
     */
    public function __construct($surveyId, $fileId) {
        $this->fileId = $fileId;
        $this->surveyId = $surveyId;
    }

    /**
     * Execute the job.
     *
     * @return void
     *
     * @throws Exception
     */
    public function handle() {
        $client = new QualtricsServiceProvider();
        $file = $client->getResponseExportFile($this->surveyId, $this->fileId);

        if (!is_null($file) && is_array($file->responses))  {
            foreach ($file->responses as $response) {
                // TODO
            }
        }
    }

    public function fail($exception = null) {
        $this->release(30);
    }
}
