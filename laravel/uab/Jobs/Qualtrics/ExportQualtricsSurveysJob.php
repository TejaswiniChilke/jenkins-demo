<?php
namespace Uab\Jobs\Qualtrics;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use \Exception;
use Uab\Http\Enums\QueueTypesEnum;
use Uab\Providers\Qualtrics\QualtricsServiceProvider;

class ExportQualtricsSurveysJob implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $surveyId;
    private $start;
    private $end;
    private $partial;

    /**
     * Create a new job instance.
     *
     * @param string $surveyId
     * @param Carbon $start
     * @param Carbon $end
     * @param boolean $partial
     */
    public function __construct($surveyId, $start, $end, $partial = false) {
        $this->surveyId = $surveyId;
        $this->start = $start->toRfc3339String();
        $this->end = $end->toRfc3339String();
        $this->partial = $partial;
    }

    /**
     * Execute the job.
     *
     * @return void
     *
     * @throws Exception
     */
    public function handle() {
        $parameters = [
            'format' => 'json',
            'startDate' => $this->start,
            'endDate' => $this->end,
            'exportResponsesInProgress' => $this->partial === true
        ];

        $client = new QualtricsServiceProvider();
        $response = $client->exportResponses($this->surveyId, $parameters);

        if (isset($response->progressId)) {
            CheckQualtricsSurveyProgressJob::dispatch($this->surveyId, $response->progressId)
                ->onQueue(QueueTypesEnum::NORMAL)
                ->delay(Carbon::now()->addMinutes(1));
        }
    }

    public function fail($exception = null) {
        $this->release(30);
    }
}
