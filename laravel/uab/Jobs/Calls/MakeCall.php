<?php
namespace Uab\Jobs\Calls;

use Uab\Http\Models\Users;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Uab\Providers\IvrServiceProvider;

class MakeCall implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $user;

    /**
     * Create a new job instance.
     *
     * @param Users $user
     */
    public function __construct($user) {
        $this->user = $user->toArray();
    }

    /**
     * Execute the job.
     *
     * @return void
     *
     * @throws \Exception
     */
    public function handle() {
        $service = new IvrServiceProvider();

        $service->makeCall($this->user);
    }
}
