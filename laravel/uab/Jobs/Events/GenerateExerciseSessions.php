<?php
namespace Uab\Jobs\Events;

use Carbon\Carbon;
use Uab\Http\Enums\QueueTypesEnum;
use Uab\Http\Models\EmailAttachments;
use Uab\Http\Models\Emails;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Exception;
use Uab\Http\Models\Events;
use Uab\Http\Models\EventsUsers;
use Uab\Http\Models\Users;
use Uab\Jobs\Emails\SendAndCreateEmail;
use Uab\Providers\IcsServiceProvider;

class GenerateExerciseSessions implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $sessions = [];
    private $user = [];

    /**
     * Create a new job instance.
     *
     * @param Users $user
     * @param mixed $sessions
     *
     * @return void
     */
    public function __construct(Users $user, $sessions) {
        if (!is_array($sessions)) {
            $sessions = [$sessions];
        }

        $this->sessions = $sessions;
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     *
     * @throws Exception
     */
    public function handle() {
        $events = [];

        $weekCount = config('app.weeks', 1);

        for ($week = 0; $week < $weekCount; $week++) {
            $sessionCount = count($this->sessions);

            for ($i = 0; $i < $sessionCount; $i++) {
                $session = $this->sessions[$i];

                if (array_key_exists('time', $session) && array_key_exists('day', $session)) {
                    $day = $this->getDayNumber($session['day']);

                    $start = Carbon::parse($this->user->start_time)
                        ->startOfWeek()
                        ->addWeeks($week)
                        ->addDays($day);

                    $time = $session['time'];
                    $time = str_replace('pm', '', str_replace('am', '', $time));
                    $time = trim($time);

                    $timePositions = explode(':', $time);

                    if (count($timePositions) === 2) {
                        $start = $start->hour($timePositions[0])->minute($timePositions[1]);
                    }

                    $startTime = Carbon::parse($this->user->start_time);
                    if ($start->isBefore($startTime)) {
                        $start = $start->addWeeks($weekCount - $week);
                    }

                    $minutes = config('event_duration', 60);
                    $end = $start->copy()->addMinutes($minutes);

                    $event = new Events();
                    $event->fill(
                        [
                            'start' => $start,
                            'end' => $end,
                            'title' => 'Exercise Session'
                        ]
                    );

                    $success = $event->save(
                        [
                            'AFTER_CREATE' => false,
                            'BEFORE_CREATE' => false
                        ]
                    );

                    if ($success) {
                        $events[] = $event;

                        $eventUsers = new EventsUsers();
                        $eventUsers->fill(
                            [
                                'event_id' => $event->id,
                                'user_id' => $this->user->id,
                            ]
                        );

                        $success = $eventUsers->save(
                            [
                                'AFTER_CREATE' => false,
                                'BEFORE_CREATE' => false
                            ]
                        );

                        if (!$success) {
                            throw new Exception('Error trying to create session\'s event user.');
                        }
                    } else {
                        throw new Exception('Error trying to create session\'s event.');
                    }
                }
            }
        }

        $service = new IcsServiceProvider();
        $ics = $service->getIcs($events);

        $email = new Emails();
        $email->to_user_id = $this->user->id;
        $email->to_email = $this->user->email;
        $email->subject = 'Calendar invitation for your program schedule';
        $email->message = 'Setting a regular program schedule is a great start!<br/><br/>By accepting the calendar invitation, you can easily see and manage the program schedule in the calendar linked with your email account! To accept the calendar invitation, please click "Download" below the program logo.';

        $attachment = new EmailAttachments();
        $attachment->path = $ics;

        SendAndCreateEmail::dispatch($email, $attachment)->onQueue(QueueTypesEnum::LOW);
    }

    /**
     * @param string $day
     *
     * @return int
     */
    private function getDayNumber(string $day):int {
        switch (trim(strtolower($day))) {
            case 'tuesday':
                $day = 1;
                break;
            case 'wednesday':
                $day = 2;
                break;
            case 'thursday':
                $day = 3;
                break;
            case 'friday':
                $day = 4;
                break;
            case 'saturday':
                $day = 5;
                break;
            case 'sunday':
                $day = 6;
                break;
            case 'monday':
            default:
                $day = 0;
                break;
        }

        return $day;
    }
}
