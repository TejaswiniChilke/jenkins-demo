<?php
namespace Uab\Jobs\Badges;

use Uab\Http\Enums\QueueTypesEnum;
use Uab\Http\Models\Badges;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use \Exception;
use Uab\Http\Models\UserTypes;
use Uab\Providers\UsersServiceProvider;

class CheckMissedBadge implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $badgeId;

    /**
     * Create a new job instance.
     *
     * @param string $badgeId
     */
    public function __construct(string $badgeId) {
        $this->badgeId = Badges::safeDecodeStatic($badgeId);
    }

    /**
     * Execute the job.
     *
     * @return void
     *
     * @throws Exception
     */
    public function handle() {
        $userTypes = UserTypes::query()
            ->distinct()
            ->select('user_types.*')
            ->rightJoin('badges_user_types', 'badges_user_types.user_type_id', '=', 'user_types.id')
            ->where('badges_user_types.badge_id', $this->badgeId)
            ->get();

        if (is_null($userTypes)) {
            return;
        }

        $userService = new UsersServiceProvider();
        $users = $userService->getUsersByUserTypes($userTypes);

        foreach ($users as $user) {
            CheckUserBadge::dispatch($user->id, $this->badgeId)
                ->onQueue(QueueTypesEnum::LOW);
        }
    }

    public function fail($exception = null) {
        return;
    }
}
