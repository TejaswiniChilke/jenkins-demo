<?php
namespace Uab\Jobs\Badges;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Uab\Broadcasts\BroadcastEarnedBadge;
use Uab\Http\Models\Badges;
use Uab\Http\Models\Users;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use \Exception;

class EarnedBadgeNotification implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $created;
    private $userId;

    /**
     * Create a new job instance.
     *
     * @param string|number $userId
     */
    public function __construct($userId) {
        $this->created = Carbon::now();
        $this->userId = Users::safeDecodeStatic($userId);
    }

    /**
     * Execute the job.
     *
     * @return void
     *
     * @throws Exception
     */
    public function handle() {
        $expires = $this->created->addMinutes(20);

        if (Carbon::now()->isBefore($expires)) {
            $query = Badges::query()
                ->select('badges.*')
                ->rightJoin(
                    'badges_users',
                    'badges_users.badge_id',
                    '=',
                    'badges.id'
                )->where(
                    [
                        'badges_users.has_read' => 0,
                        'badges_users.user_id'  => $this->userId
                    ]
                );

            /** @var Collection<Badges> $badges */
            $collection = $query->get();

            $badges = [];

            foreach ($collection as $badge) {
                $parsed = $badge->toArray();
                $parsed['id'] = $badge->slug();

                $image = $badge->image();
                $parsed['images'] = $image->toArray();
                $parsed['images']['id'] = $image->slug();

                $badges[] = $parsed;
            }

            event(new BroadcastEarnedBadge($this->userId, $badges));
        }
    }

    public function fail($exception = null) {
        return;
    }
}
