<?php
namespace Uab\Jobs\Badges;

use Illuminate\Support\Facades\Redis;
use Uab\Http\Models\Badges;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use \Exception;
use Uab\Http\Models\Users;
use Uab\Providers\BadgeServiceProvider;

class CheckUserBadge implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $badgeId;
    private $userId;

    /**
     * Create a new job instance.
     *
     * @param string $userId
     * @param string $badgeId
     */
    public function __construct(string $userId, string $badgeId) {
        $this->userId = Users::safeDecodeStatic($userId);
        $this->badgeId = Badges::safeDecodeStatic($badgeId);
    }

    /**
     * Execute the job.
     *
     * @return void
     *
     * @throws Exception
     */
    public function handle() {
        $userId = $this->userId;
        $badgeId = $this->badgeId;

        Redis::funnel('check-badge-' . $this->userId . '-' . $this->badgeId)->limit(1)->then(
            function () use ($userId, $badgeId) {
                /** @var Badges $badge */
                $badge = Badges::query()->find($badgeId);
                if (is_null($badge)) {
                    return;
                }

                /** @var Users $user */
                $user = Users::query()->find($userId);
                if (is_null($user)) {
                    return;
                }

                $badgeService = new BadgeServiceProvider();
                $badgeService->checkAndRewardBadge($badge, $user);
            },
            function() {
                $this->release(30);
            }
        );
    }
}
