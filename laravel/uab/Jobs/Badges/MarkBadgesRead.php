<?php
namespace Uab\Jobs\Badges;

use Pressutto\LaravelSlack\Facades\Slack;
use Uab\Http\Models\Badges;
use \Exception;
use Uab\Http\Models\BadgesUsers;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class MarkBadgesRead implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable;

    private $ids;
    private $userId;

    /**
     * Create a new job instance.
     *
     * @param string[] $ids
     * @param string $userId
     */
    public function __construct(array $ids, string $userId) {
        $this->ids = $ids;
        $this->userId = $userId;
        Slack::to(config('SLACK_CHANNEL'))->send('Pre User: ' . $this->userId);
        Slack::to(config('SLACK_CHANNEL'))->send('Pre Before: ' . json_encode($this->ids));
    }

    /**
     * Execute the job.
     *
     * @return void
     *
     * @throws Exception
     */
    public function handle() {
        $ids = $this->ids;

        Slack::to(config('SLACK_CHANNEL'))->send('User: ' . json_encode($this->userId));
        Slack::to(config('SLACK_CHANNEL'))->send('Before: ' . json_encode($this->ids));

        foreach ($ids as $i => $id) {
            $ids[$i] = Badges::decodeSlug($id);
        }

        Slack::to(config('SLACK_CHANNEL'))->send('After: ' . json_encode($ids));

        $query = BadgesUsers::query()
            ->whereIn(
                'badge_id',
                $ids
            )->where('user_id', '=', $this->userId);

        /** @var BadgesUsers[] $badgeUsers */
        $badgeUsers = $query->get();

        foreach ($badgeUsers as $badgeUser) {
            $badgeUser->has_read = true;
            $badgeUser->save();
        }
    }
}
