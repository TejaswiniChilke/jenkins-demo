<?php
namespace Uab\Listeners\Posts;

use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Uab\Events\AfterCreate\AfterCreateInterface;
use Uab\Events\AfterCreate\FavoriteVideoAfterCreate;
use Uab\Http\Models\FavoriteVideos;
use Uab\Http\Models\PostAttachments;

class CreateFavoriteVideoPost extends PostGenerate implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {

    }

    /**
     * Handle the event.
     *
     * @param FavoriteVideoAfterCreate $event
     *
     * @return void
     *
     * @throws \Exception
     * @throws \Throwable
     */
    public function handle(FavoriteVideoAfterCreate $event) {
        /** @var FavoriteVideos $favorite */
        $favorite = $event->getModel();

        $post = $this->getPost($favorite->user());
        $post->type = 'video'; // TODO: enum

        $post->message = 'Favorited a video.';
        $post->type = 'video';
        $success = $post->save();

        if (!$success) {
            throw new Exception(
                'Unable to create post for favorited video.'
            );
        }

        $attachment = new PostAttachments();
        $attachment->post_id = $post->id;
        $attachment->reference_key = 'videos';
        $attachment->reference_value = $favorite->video_id;
        $success = $attachment->save();

        if (!$success) {
            throw new Exception(
                'Unable to create post attachment for favorited video.'
            );
        }
    }

    /**
     * Handle a job failure.
     *
     * @param AfterCreateInterface $event
     * @param \Exception $exception
     *
     * @return void
     */
    public function failed(AfterCreateInterface $event, $exception) {
        //
    }
}
