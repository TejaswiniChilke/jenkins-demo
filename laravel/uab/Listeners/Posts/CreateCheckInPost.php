<?php
namespace Uab\Listeners\Posts;

use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Uab\Events\AfterCreate\AfterCreateInterface;
use Uab\Events\AfterCreate\CheckInsAfterCreate;
use Uab\Http\Enums\PostTypesEnum;
use Uab\Http\Models\Articles;
use Uab\Http\Models\CheckIns;
use Uab\Http\Models\Videos;
use DB;
use Uab\Providers\ModelServiceProvider;

class CreateCheckInPost extends PostGenerate implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {

    }

    /**
     * Handle the event.
     *
     * @param CheckInsAfterCreate $event
     *
     * @return void
     *
     * @throws \Exception
     * @throws \Throwable
     */
    public function handle(CheckInsAfterCreate $event) {
        /** @var CheckIns $checkIn */
        $checkIn = $event->getModel();

        $post = $this->getPost($checkIn->user());
        $post->message = $checkIn->message;
        $post->type = PostTypesEnum::CHECK_IN;

        $modelService = new ModelServiceProvider();
        $model = $modelService->getModel($checkIn->object_table);
        if (is_null($model)) {
            throw new Exception('Could not load model ('.$checkIn->object_table.') for check-in.');
        }

        $id = $model::safeDecodeStatic($checkIn->object_id);
        if (is_null($id)) {
            throw new Exception('Could not get ID ('.$checkIn->object_id.') for check-in.');
        }

        $object = $model::query()
            ->where(
                [
                    'id' => $id
                ]
            )->first();

        if (!is_null($object)) {
            switch ($model->getTable()) {
                case 'articles':
                    /** @var Articles $object */
                    $post->message = 'I read the article \'' . $object->title . '\'.';
                    $post->type = PostTypesEnum::ARTICLES;
                    break;
                case 'videos':
                    /** @var Videos $object */
                    $post->message = 'I watched the video \'' . $object->title . '\'.';
                    $post->type = PostTypesEnum::VIDEOS;
                    break;
            }
        }

        $this->generate($post);
    }

    /**
     * Handle a job failure.
     *
     * @param AfterCreateInterface $event
     * @param \Exception $exception
     *
     * @return void
     */
    public function failed(AfterCreateInterface $event, $exception) {
        //
    }
}
