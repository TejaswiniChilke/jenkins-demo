<?php
namespace Uab\Listeners\Posts;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Uab\Events\AfterCreate\AfterCreateInterface;
use Uab\Events\AfterCreate\VideoLikesAfterCreate;
use Uab\Http\Models\PostAttachments;
use Uab\Http\Models\VideoLikes;
use Exception;
use Throwable;

class CreateVideoLikePost extends PostGenerate implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {

    }

    /**
     * Handle the event.
     *
     * @param VideoLikesAfterCreate $event
     *
     * @return void
     *
     * @throws Exception
     * @throws Throwable
     */
    public function handle(VideoLikesAfterCreate $event) {
        /** @var VideoLikes $like */
        $like = $event->getModel();

        $post = $this->getPost($like->user());
        $post->type = 'video'; // TODO: enum
        $post->message = 'Liked a video.';
        $success = $post->save();

        if (!$success) {
            throw new Exception(
                'Unable to create post for liked video.'
            );
        }

        $attachment = new PostAttachments();
        $attachment->post_id = $post->id;
        $attachment->reference_key = 'videos';
        $attachment->reference_value = $like->video_id;
        $success = $attachment->save();

        if (!$success) {
            throw new Exception(
                'Unable to create post attachment for liked video.'
            );
        }
    }

    /**
     * Handle a job failure.
     *
     * @param AfterCreateInterface $event
     * @param \Exception $exception
     *
     * @return void
     */
    public function failed(AfterCreateInterface $event, $exception) {
        //
    }
}
