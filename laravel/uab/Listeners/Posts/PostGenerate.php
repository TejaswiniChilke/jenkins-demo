<?php
namespace Uab\Listeners\Posts;

use stdClass;
use Uab\Http\Enums\QueueTypesEnum;
use Uab\Http\Models\Posts;
use Uab\Http\Models\Users;
use Uab\Jobs\Base\InsertObjectJob;

class PostGenerate {
    /**
     * @param Posts $post
     */
    protected function generate($post) {
        InsertObjectJob::dispatch($post)->onQueue(QueueTypesEnum::NORMAL);
    }

    /**
     * @param Users|stdClass      $toUser
     * @param Users|stdClass|null $fromUser
     *
     * @return Posts
     */
    public function getPost($toUser, $fromUser = null) {
        $post = new Posts();

        $post->to_user_id = $toUser->id;
        $post->from_user_id = is_null($fromUser) ? $toUser->id : $fromUser->id;

        return $post;
    }
}
