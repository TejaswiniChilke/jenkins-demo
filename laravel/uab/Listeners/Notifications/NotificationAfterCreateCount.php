<?php
namespace Uab\Listeners\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Uab\Events\AfterCreate\NotificationsAfterCreate;
use Uab\Http\Enums\QueueTypesEnum;
use Uab\Http\Models\Notifications;
use Exception;
use Uab\Jobs\Notifications\NotificationCount;

class NotificationAfterCreateCount implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {

    }

    /**
     * Handle the event.
     *
     * @param NotificationsAfterCreate $afterCreate
     *
     * @return void
     *
     * @throws Exception
     */
    public function handle(NotificationsAfterCreate $afterCreate) {
        /** @var Notifications $notification */
        $notification = $afterCreate->getModel();

        NotificationCount::dispatch($notification->user_id)->onQueue(QueueTypesEnum::NORMAL);
    }

    /**
     * Handle a job failure.
     *
     * @param $event
     * @param Exception $exception
     *
     * @return void
     */
    public function failed($event, $exception) {
        //
    }
}
