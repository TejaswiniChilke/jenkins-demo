<?php
namespace Uab\Listeners\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Uab\Events\AfterCreate\AfterCreateInterface;
use Uab\Events\AfterCreate\CommentsAfterCreate;
use Uab\Http\Models\BlacklistedWords;
use Uab\Http\Models\Comments;
use Uab\Http\Models\ReportedComments;
use Exception;

class CheckCommentForBlacklisted extends NotificationGenerate implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {

    }

    /**
     * Handle the event.
     *
     * @param CommentsAfterCreate $afterCreate
     *
     * @return void
     *
     * @throws Exception
     */
    public function handle(CommentsAfterCreate $afterCreate) {
        /** @var Comments $comment */
        $comment = $afterCreate->getModel();

        /** @var BlacklistedWords[] $words */
        $words = BlacklistedWords::query()->get();

        $message = strtolower($comment->comment);

        foreach ($words as $word) {
            if (strstr($message, strtolower($word->word))) {
                $report = new ReportedComments();
                $report->comment_id = $comment->id;
                $report->save();

                break;
            }
        }
    }

    /**
     * Handle a job failure.
     *
     * @param AfterCreateInterface $event
     * @param \Exception $exception
     *
     * @return void
     */
    public function failed(AfterCreateInterface $event, $exception) {
        //
    }
}
