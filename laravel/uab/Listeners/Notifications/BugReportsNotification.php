<?php
namespace Uab\Listeners\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Uab\Events\AfterCreate\AfterCreateInterface;
use Uab\Http\Enums\NotificationTypesEnum;
use Uab\Http\Enums\QueueTypesEnum;
use Uab\Http\Enums\UserTypesEnum;
use Uab\Http\Models\BugReports;
use Uab\Jobs\Emails\EmailUserType;
use Uab\Jobs\Notifications\NotifyUserType;

class BugReportsNotification extends NotificationGenerate implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {

    }

    /**
     * Handle the event.
     *
     * @param AfterCreateInterface $afterCreate
     *
     * @return void
     *
     * @throws \Exception
     */
    public function handle(AfterCreateInterface $afterCreate) {
        /** @var BugReports $bugReport */
        $bugReport = $afterCreate->getModel();

        $reportingUser = $bugReport->user();

        NotifyUserType::dispatch(
            [
                UserTypesEnum::ADMIN,
                UserTypesEnum::SUPER_ADMIN
            ],
            $reportingUser->email.' Reported a Bug.',
            'Bug reported.',
            NotificationTypesEnum::NEW_BUG_REPORT
        )->onQueue(QueueTypesEnum::HIGH);

        EmailUserType::dispatch(
            [
                UserTypesEnum::ADMIN,
                UserTypesEnum::SUPER_ADMIN
            ],
            "Bug reported.<br/><br/>Link to Message: " . config('app.frontend') . "/#/bug-report-message/" . $bugReport->slug(),
            'Bug Reported.',
        )->onQueue(QueueTypesEnum::HIGH);
    }
}
