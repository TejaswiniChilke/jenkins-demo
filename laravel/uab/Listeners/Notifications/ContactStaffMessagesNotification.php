<?php
namespace Uab\Listeners\Notifications;

use \Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Uab\Events\AfterCreate\AfterCreateInterface;
use Uab\Http\Enums\NotificationTypesEnum;
use Uab\Http\Enums\QueueTypesEnum;
use Uab\Http\Enums\UserTypesEnum;
use Uab\Http\Models\ContactStaffMessages;
use Uab\Jobs\Emails\EmailUserType;
use Uab\Jobs\Notifications\NotifyUserType;

class ContactStaffMessagesNotification extends NotificationGenerate implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {

    }

    /**
     * Handle the event.
     *
     * @param AfterCreateInterface $afterCreate
     *
     * @return void
     *
     * @throws Exception
     */
    public function handle(AfterCreateInterface $afterCreate) {
        /** @var ContactStaffMessages $message */
        $message = $afterCreate->getModel();

        NotifyUserType::dispatch(
            [
                UserTypesEnum::ADMIN
            ],
            '\'Contact Staff\' Initiated.',
            null,
            NotificationTypesEnum::NEW_CONTACT_STAFF
        )->onQueue(QueueTypesEnum::HIGH);

        EmailUserType::dispatch(
            [
                UserTypesEnum::ADMIN
            ],
            "Contact staff initiated.<br/><br/>Link to Message: " . config('app.frontend') . "/#/contact-staff-message/" . $message->slug(),
            '\'Contact Staff\' Initiated.',
        )->onQueue(QueueTypesEnum::HIGH);
    }
}
