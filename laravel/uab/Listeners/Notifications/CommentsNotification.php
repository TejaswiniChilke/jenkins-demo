<?php
namespace Uab\Listeners\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Uab\Events\AfterCreate\AfterCreateInterface;
use Uab\Http\Models\Comments;

class CommentsNotification extends NotificationGenerate implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {

    }

    /**
     * Handle the event.
     *
     * @param AfterCreateInterface $afterCreate
     *
     * @return void
     *
     * @throws \Exception
     */
    public function handle(AfterCreateInterface $afterCreate) {
        /** @var Comments $comment */
        $comment = $afterCreate->getModel();

        $posts = $comment->getPosts();

        foreach ($posts as $post) {
            if (!is_null($post->from_user_id) && $post->from_user_id !== $comment->user_id) {
                $notification = $this->getNotification($afterCreate);

                $notification->message = 'Someone commented on your post.';
                $notification->user_id = $post->from_user_id;

                $this->generate($notification);
            }

            if (!is_null($post->to_user_id) && $post->to_user_id !== $comment->user_id && $post->to_user_id !== $post->from_user_id) {
                $notification = $this->getNotification($afterCreate);

                $notification->message = 'Someone commented on a post on your wall.';
                $notification->user_id = $post->to_user_id;

                $this->generate($notification);
            }

            $commentUserIds = $post->getComments()
                ->unique('user_id')
                ->pluck('user_id')
                ->toArray();

            $usedIds = [
                $comment->user_id,
                $post->to_user_id,
                $post->from_user_id
            ];

            foreach ($commentUserIds as $userId) {
                if (!in_array($userId, $usedIds)) {
                    $notification = $this->getNotification($afterCreate);

                    $notification->message = 'Someone responded on a post that you commented on.';
                    $notification->user_id = $post->to_user_id;

                    $this->generate($notification);
                }
            }
        }
    }

    /**
     * Handle a job failure.
     *
     * @param AfterCreateInterface $event
     * @param \Exception $exception
     *
     * @return void
     */
    public function failed(AfterCreateInterface $event, $exception) {
        //
    }
}
