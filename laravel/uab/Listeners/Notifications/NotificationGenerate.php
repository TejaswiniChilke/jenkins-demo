<?php
namespace Uab\Listeners\Notifications;

use Illuminate\Support\Str;
use Uab\Http\Enums\QueueTypesEnum;
use Uab\Jobs\Base\InsertObjectJob;
use Uab\Events\AfterCreate\AfterCreateInterface;
use Uab\Http\Models\Base\BaseModel;
use Uab\Http\Models\Notifications;

class NotificationGenerate {
    protected function generate($notification) {
        InsertObjectJob::dispatch($notification)
            ->onQueue(QueueTypesEnum::HIGH);
    }

    /**
     * @param AfterCreateInterface $afterCreate
     *
     * @return Notifications
     */
    public function getNotification($afterCreate) {
        /** @var BaseModel $object */
        $object = $afterCreate->getModel();

        $notification = new Notifications();
        $notification->message = 'You have a notification.';
        $notification->reference_name = 'new-'.$object->getTable();
        $notification->reference_value = $object->id;
        $notification->type = Str::studly($object->getTable());

        return $notification;
    }
}
