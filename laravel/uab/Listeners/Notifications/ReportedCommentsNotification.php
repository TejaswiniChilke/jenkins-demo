<?php
namespace Uab\Listeners\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Log;
use Uab\Events\AfterCreate\AfterCreateInterface;
use Uab\Http\Enums\NotificationTypesEnum;
use Uab\Http\Enums\QueueTypesEnum;
use Uab\Http\Enums\UserTypesEnum;
use Uab\Http\Models\ReportedComments;
use Uab\Http\Models\Users;
use Uab\Jobs\Emails\EmailUserType;
use Uab\Jobs\Notifications\NotifyUserType;
use \Exception;

class ReportedCommentsNotification extends NotificationGenerate implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {

    }

    /**
     * Handle the event.
     *
     * @param AfterCreateInterface $afterCreate
     *
     * @return void
     *
     * @throws \Exception
     */
    public function handle(AfterCreateInterface $afterCreate) {
        /** @var ReportedComments $commentReport */
        $commentReport = $afterCreate->getModel();

        /** @var Users $reportedUser */
        $reportingUser = $commentReport->user();
        if (is_null($reportingUser)) {
            $subject = 'A comment has been reported.';
        } else {
            $subject = 'A user reported a comment.';
        }

        $comment = $commentReport->comment();
        if (is_null($comment)) {
            throw new Exception('Comment reported but not linked to report (' . $commentReport->id . ')');
        }

        $url = null;

        $type = $commentReport->getReferredTypeAttribute();
        $object = $commentReport->getReferredObjectAttribute();

        switch ($type) {
            case 'articles':
                $url = '<b>Article RL:</b> ' . config('app.frontend') . '/#/article/' . $object->slug();
                break;
            case 'badges':
                $url = '<b>Badge URL:</b> ' . config('app.frontend') . '/#/badge/' . $object->slug();
                break;
            case 'posts':
                $url = '<b>Post URL:</b> ' . config('app.frontend') . '/#/post/' . $object->slug();
                break;
            case 'videos':
                $url = '<b>Video URL:</b> ' . config('app.frontend') . '/#/video/' . $object->slug();
                break;
            default:
                Log::error('Unable to find model for comment ' . $comment->id . ' for report ' . $commentReport->id);
                return;
        }

        NotifyUserType::dispatch(
            [
                UserTypesEnum::ADMIN
            ],
            $subject,
            $subject,
            NotificationTypesEnum::REPORTED_COMMENT
        )->onQueue(QueueTypesEnum::HIGH);

        $message = $subject . '<br/><br/>' . $url;

        EmailUserType::dispatch(
            [
                UserTypesEnum::ADMIN
            ],
            $message,
            $subject,
        )->onQueue(QueueTypesEnum::HIGH);
    }
}
