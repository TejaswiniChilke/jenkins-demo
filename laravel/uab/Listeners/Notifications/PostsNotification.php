<?php
namespace Uab\Listeners\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Uab\Events\AfterCreate\AfterCreateInterface;
use Uab\Http\Models\Posts;
use Uab\Http\Models\Users;

class PostsNotification extends NotificationGenerate implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {

    }

    /**
     * Handle the event.
     *
     * @param AfterCreateInterface $afterCreate
     *
     * @return void
     *
     * @throws \Exception
     */
    public function handle(AfterCreateInterface $afterCreate) {
        /** @var Posts $post */
        $post = $afterCreate->getModel();

        if (!is_null($post->to_user_id) && $post->to_user_id !== $post->from_user_id) {
            /** @var Users $fromUser */
            $fromUser = $post->fromUser();

            $notification = $this->getNotification($afterCreate);

            $notification->message = $fromUser->username.' sent you a post.';
            $notification->user_id = $post->to_user_id;

            $this->generate($notification);
        }
    }

    /**
     * Handle a job failure.
     *
     * @param AfterCreateInterface $event
     * @param \Exception $exception
     *
     * @return void
     */
    public function failed(AfterCreateInterface $event, $exception) {
        //
    }
}
