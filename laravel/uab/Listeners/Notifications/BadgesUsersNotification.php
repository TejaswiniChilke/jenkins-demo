<?php
namespace Uab\Listeners\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Uab\Events\AfterCreate\AfterCreateInterface;
use Uab\Events\AfterCreate\BadgesUsersAfterCreate;
use Uab\Http\Models\BadgesUsers;
use \Exception;

class BadgesUsersNotification extends NotificationGenerate implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {

    }

    /**
     * Handle the event.
     *
     * @param BadgesUsersAfterCreate $afterCreate
     *
     * @return void
     *
     * @throws Exception
     */
    public function handle(BadgesUsersAfterCreate $afterCreate) {
        /** @var BadgesUsers $badgeUser */
        $badgeUser = $afterCreate->getModel();

        $badge = $badgeUser->badge();

        $notification = $this->getNotification($afterCreate);

        $notification->reference_value = $badge->slug();
        $notification->message = 'You\'ve been awarded the \''.$badge->title.'\' badge!';
        $notification->user_id = $badgeUser->user_id;

        $this->generate($notification);
    }

    /**
     * Handle a job failure.
     *
     * @param AfterCreateInterface $event
     * @param Exception $exception
     *
     * @return void
     */
    public function failed(AfterCreateInterface $event, $exception) {
        //
    }
}
