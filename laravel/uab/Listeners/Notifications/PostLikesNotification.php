<?php
namespace Uab\Listeners\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Uab\Events\AfterCreate\AfterCreateInterface;
use Uab\Events\AfterCreate\PostLikesAfterCreate;
use Uab\Http\Models\PostLikes;
use Uab\Http\Models\Posts;
use Uab\Http\Models\Users;
use Exception;

class PostLikesNotification extends NotificationGenerate implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {

    }

    /**
     * Handle the event.
     *
     * @param PostLikesAfterCreate $afterCreate
     *
     * @return void
     *
     * @throws Exception
     */
    public function handle(PostLikesAfterCreate $afterCreate) {
        /** @var PostLikes $postLike */
        $postLike = $afterCreate->getModel();
        $postLikeUserId = Users::safeDecodeStatic($postLike->user_id);

        /** @var Posts $post */
        $post = $postLike->post();
        $postUserId = Users::safeDecodeStatic($post->from_user_id);

        if ($postUserId !== $postLikeUserId) {
            /** @var Users $fromUser */
            $fromUser = $postLike->user();

            $notification = $this->getNotification($afterCreate);

            $notification->message = $fromUser->username . ' liked your post.';
            $notification->user_id = $post->from_user_id;

            $this->generate($notification);
        }
    }

    /**
     * Handle a job failure.
     *
     * @param AfterCreateInterface $event
     * @param \Exception $exception
     *
     * @return void
     */
    public function failed(AfterCreateInterface $event, $exception) {
        //
    }
}
