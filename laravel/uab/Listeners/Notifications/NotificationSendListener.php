<?php
namespace Uab\Listeners\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Uab\Events\AfterCreate\AfterCreateInterface;
use Uab\Events\AfterCreate\NotificationsAfterCreate;
use Uab\Broadcasts\BroadcastNotification;
use Uab\Http\Models\Notifications;

class NotificationSendListener extends NotificationGenerate implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {

    }

    /**
     * Handle the event.
     *
     * @param NotificationsAfterCreate $afterCreate
     *
     * @return void
     *
     * @throws \Exception
     */
    public function handle(NotificationsAfterCreate $afterCreate) {
        /** @var Notifications $notification */
        $notification = $afterCreate->getModel();

        $this->sendSocketNotification($notification);
    }

    /**
     * @param Notifications $notification
     *
     * @return void
     */
    private function sendSocketNotification(Notifications $notification) {
        event(new BroadcastNotification($notification));
    }
}
