<?php
namespace Uab\Listeners\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;
use Uab\Events\AfterEdit\NotificationsAfterEdit;
use Uab\Http\Enums\QueueTypesEnum;
use Uab\Http\Models\Notifications;
use Exception;
use Uab\Jobs\Notifications\NotificationCount;

class NotificationAfterEditCount implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {

    }

    /**
     * Handle the event.
     *
     * @param NotificationsAfterEdit $afterEdit
     *
     * @return void
     *
     * @throws Exception
     */
    public function handle(NotificationsAfterEdit $afterEdit) {
        $fields = $afterEdit->getFields();

        $hasRead = Arr::get($fields, 'has_read', null);
        if (!is_null($hasRead)) {
            $id = Arr::get($fields, 'id', null);
            if (!is_null($id)) {
                /** @var Notifications|null $notification */
                $notification = Notifications::query()->find($id);

                if (!is_null($notification)) {
                    NotificationCount::dispatch($notification->user_id)
                        ->onQueue(QueueTypesEnum::NORMAL);
                }
            }
        }
    }

    /**
     * Handle a job failure.
     *
     * @param $event
     * @param Exception $exception
     *
     * @return void
     */
    public function failed($event, $exception) {
        //
    }
}
