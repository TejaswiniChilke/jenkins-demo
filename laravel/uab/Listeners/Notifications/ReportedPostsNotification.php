<?php
namespace Uab\Listeners\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Str;
use Uab\Events\AfterCreate\AfterCreateInterface;
use Uab\Http\Enums\NotificationTypesEnum;
use Uab\Http\Enums\QueueTypesEnum;
use Uab\Http\Enums\UserTypesEnum;
use Uab\Http\Models\ReportedPosts;
use Uab\Http\Models\Users;
use Uab\Jobs\Emails\EmailUserType;
use Uab\Jobs\Notifications\NotifyUserType;
use \Exception;

class ReportedPostsNotification extends NotificationGenerate implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {

    }

    /**
     * Handle the event.
     *
     * @param AfterCreateInterface $afterCreate
     *
     * @return void
     *
     * @throws \Exception
     */
    public function handle(AfterCreateInterface $afterCreate) {
        /** @var ReportedPosts $postReport */
        $postReport = $afterCreate->getModel();

        /** @var Users $reportedUser */
        $reportingUser = $postReport->user();

        if (is_null($reportingUser)) {
            $subject = 'A post has been reported.';
        } else {
            $subject = 'A user reported a post.';
        }

        $post = $postReport->post();
        if (!is_null($post)) {
            NotifyUserType::dispatch(
                [
                    UserTypesEnum::ADMIN
                ],
                $subject,
                $subject,
                NotificationTypesEnum::REPORTED_POST
            )->onQueue(QueueTypesEnum::HIGH);

            $message = $subject . '<br/><br/><b>Post URL:</b> ' . config('app.frontend') . '/#/post/' . $post->slug();

            EmailUserType::dispatch(
                [
                    UserTypesEnum::ADMIN
                ],
                $message,
                $subject
            )->onQueue(QueueTypesEnum::HIGH);
        } else {
            throw new Exception('Post reported but not linked to report (' . $postReport->id . ')');
        }
    }

    /**
     * Handle a job failure.
     *
     * @param AfterCreateInterface $event
     * @param Exception $exception
     *
     * @return void
     */
    public function failed(AfterCreateInterface $event, $exception) {
        //
    }
}
