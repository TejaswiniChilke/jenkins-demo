<?php
namespace Uab\Listeners\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Uab\Events\AfterCreate\AfterCreateInterface;
use Uab\Events\AfterCreate\PostsAfterCreate;
use Uab\Http\Models\BlacklistedWords;
use Uab\Http\Models\Posts;
use Uab\Http\Models\ReportedPosts;
use Exception;

class CheckPostForBlacklisted extends NotificationGenerate implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {

    }

    /**
     * Handle the event.
     *
     * @param PostsAfterCreate $afterCreate
     *
     * @return void
     *
     * @throws Exception
     */
    public function handle(PostsAfterCreate $afterCreate) {
        /** @var Posts $post */
        $post = $afterCreate->getModel();

        $message = strtolower($post->message);

        /** @var BlacklistedWords[] $words */
        $words = BlacklistedWords::query()->get();
        foreach ($words as $word) {
            if (strstr($message, strtolower($word->word))) {
                $report = new ReportedPosts();
                $report->post_id = $post->id;
                $report->save();

                break;
            }
        }
    }

    /**
     * Handle a job failure.
     *
     * @param AfterCreateInterface $event
     * @param \Exception $exception
     *
     * @return void
     */
    public function failed(AfterCreateInterface $event, $exception) {
        //
    }
}
