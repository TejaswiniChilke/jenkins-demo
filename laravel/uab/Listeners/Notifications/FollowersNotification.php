<?php
namespace Uab\Listeners\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Str;
use Uab\Events\AfterCreate\AfterCreateInterface;
use Uab\Http\Models\Followers;
use Uab\Http\Models\Users;
use Uab\Providers\FriendServiceProvider;

class FollowersNotification extends NotificationGenerate implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {

    }

    /**
     * Handle the event.
     *
     * @param AfterCreateInterface $afterCreate
     *
     * @return void
     *
     * @throws \Exception
     */
    public function handle(AfterCreateInterface $afterCreate) {
        $service = new FriendServiceProvider();

        /** @var Followers $follower */
        $follower = $afterCreate->getModel();

        if ($service->isFriends($follower->user_id, $follower->following_id)) {
            /** @var Users $user */
            $user = $follower->user();

            $notification = $this->getNotification($afterCreate);

            $notification->message = Str::title($user->getDisplayNameAttribute()).' accepted your friend\'s request.';
            $notification->user_id = $follower->following_id;

            $this->generate($notification);
        } else {
            /** @var Users $user */
            $user = $follower->user();

            $notification = $this->getNotification($afterCreate);

            $notification->message = Str::title($user->getDisplayNameAttribute()).' requested to be your friend.';
            $notification->user_id = $follower->following_id;

            $this->generate($notification);
        }
    }

    /**
     * Handle a job failure.
     *
     * @param AfterCreateInterface $event
     * @param \Exception $exception
     *
     * @return void
     */
    public function failed(AfterCreateInterface $event, $exception) {
        //
    }
}
