<?php
namespace Uab\Listeners\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Uab\Events\AfterCreate\AfterCreateInterface;
use Uab\Http\Enums\UserStatusesEnum;
use Uab\Http\Models\Users;
use Uab\Providers\UsersServiceProvider;

class UsersNotification extends NotificationGenerate implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {

    }

    /**
     * Handle the event.
     *
     * @param AfterCreateInterface $afterCreate
     *
     * @return void
     *
     * @throws \Exception
     */
    public function handle(AfterCreateInterface $afterCreate) {
        /** @var Users $user */
        $user = $afterCreate->getModel();

        if ($user->status === UserStatusesEnum::APPLICATION_PENDING) {
            $notification = $this->getNotification($afterCreate);

            $service = new UsersServiceProvider();

            $admins = $service->getUsersByUserTypeNames(\Uab\Http\Enums\UserTypesEnum::ADMIN);

            foreach ($admins as $admin) {
                $notification->message = 'A new user has submitted an application for approval.';
                $notification->user_id = $admin->id;
            }

            $this->generate($notification);
        }
    }

    /**
     * Handle a job failure.
     *
     * @param AfterCreateInterface $event
     * @param \Exception $exception
     *
     * @return void
     */
    public function failed(AfterCreateInterface $event, $exception) {
        //
    }
}
