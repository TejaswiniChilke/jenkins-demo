<?php
namespace Uab\Listeners\Notifications;

use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Uab\Events\AfterCreate\AfterCreateInterface;
use Uab\Events\AfterCreate\BadgesUsersAfterCreate;
use Uab\Http\Enums\QueueTypesEnum;
use Uab\Http\Models\BadgesUsers;
use Uab\Jobs\Badges\EarnedBadgeNotification;

class BadgesUsersEarnedBadge extends NotificationGenerate implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {

    }

    /**
     * Handle the event.
     *
     * @param BadgesUsersAfterCreate $afterCreate
     *
     * @return void
     *
     * @throws Exception
     */
    public function handle(BadgesUsersAfterCreate $afterCreate) {
        /** @var BadgesUsers $badgeUser */
        $badgeUser = $afterCreate->getModel();

        EarnedBadgeNotification::dispatch($badgeUser->user_id)
            ->onQueue(QueueTypesEnum::HIGH);
    }

    /**
     * Handle a job failure.
     *
     * @param AfterCreateInterface $event
     * @param Exception $exception
     *
     * @return void
     */
    public function failed(AfterCreateInterface $event, $exception) {
        //
    }
}
