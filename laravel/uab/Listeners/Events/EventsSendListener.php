<?php
namespace Uab\Listeners\Events;

use \Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Uab\Events\AfterCreate\EventsAfterCreate;
use Uab\Http\Enums\QueueTypesEnum;
use Uab\Http\Models\Emails;
use Uab\Http\Models\Events;
use Uab\Jobs\Emails\SendEmail;

class EventsSendListener implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {

    }

    /**
     * Handle the event.
     *
     * @param EventsAfterCreate $afterCreate
     *
     * @return void
     *
     * @throws Exception
     */
    public function handle(EventsAfterCreate $afterCreate) {
        /** @var Events $event */
        $event = $afterCreate->getModel();

        $attendees = $event->getAttendees();

        foreach ($attendees as $attendee) {
            $email = new Emails();

            $email->setAttribute(
                'subject',
                'You have been invited to an event.'
            );

            $email->setAttribute(
                'message',
                'You have been added to an event. Please add it to your calendar.'
            );

            $email->setAttribute(
                'to_user_id',
                $attendee->getAttributeValue('id')
            );

            $email->save();

            // TODO: Check settings whether to send email
            if (true) {
                SendEmail::dispatch($email)->onQueue(QueueTypesEnum::EMAIL);
            }
        }
    }

    /**
     * Handle a job failure.
     *
     * @param EventsSendListener $event
     * @param Exception $exception
     *
     * @return void
     */
    public function failed(EventsSendListener $event, $exception) {
        //
    }
}
