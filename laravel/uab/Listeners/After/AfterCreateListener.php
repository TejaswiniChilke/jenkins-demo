<?php
namespace Uab\Listeners\After;

use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Str;
use Uab\Events\AfterCreate\AfterCreate;

class AfterCreateListener implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {

    }

    /**
     * Handle the event.
     *
     * @param AfterCreate $event
     *
     * @return void
     *
     * @throws Exception
     */
    public function handle(AfterCreate $event) {
        $table = $event->getTable();
        $model = $event->getModel();

        $this->fireEvent(
            $table,
            'AfterCreate',
            'AfterCreate',
            $model
        );
    }

    /**
     * Handle a job failure.
     *
     * @param AfterCreate $event
     * @param \Exception $exception
     *
     * @return void
     */
    public function failed(AfterCreate $event, $exception) {
        //
    }

    private function fireEvent($table, $eventDirectory, $classSuffix, $data) {
        $eventClass = 'Uab\\Events\\'.$eventDirectory.'\\'.Str::studly($table).$classSuffix;

        if (class_exists($eventClass)) {
            event(new $eventClass($data));
        }
    }
}
