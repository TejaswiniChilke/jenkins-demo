<?php
namespace Uab\Listeners\After;

use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Str;
use Uab\Events\AfterEdit\AfterEdit;

class AfterEditListener implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $service;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {

    }

    /**
     * Handle the event.
     *
     * @param AfterEdit $event
     *
     * @return void
     *
     * @throws Exception
     */
    public function handle(AfterEdit $event) {
        $dirty = $event->getDirty();
        $id = $event->getId();
        $table = $event->getTable();

        $dirty['id'] = $id;

        $this->fireEvent(
            $table,
            'AfterEdit',
            'AfterEdit',
            $dirty
        );
    }

    /**
     * Handle a job failure.
     *
     * @param AfterEdit $event
     * @param \Exception $exception
     *
     * @return void
     */
    public function failed(AfterEdit $event, $exception) {
        //
    }

    private function fireEvent($table, $eventDirectory, $classSuffix, $data) {
        $camelCaseTable = Str::studly($table);

        $eventClass = 'Uab\\Events\\'.$eventDirectory.'\\'.$camelCaseTable.$classSuffix;

        if (class_exists($eventClass)) {
            event(new $eventClass($data));
        }
    }
}
