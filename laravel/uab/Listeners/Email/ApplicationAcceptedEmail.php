<?php
namespace Uab\Listeners\Email;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Uab\Events\AfterCreate\AfterCreateInterface;
use Uab\Events\AfterEdit\UsersAfterEdit;
use Uab\Http\Enums\UserStatusesEnum;
use Uab\Http\Models\Users;
use \Exception;
use Uab\Providers\R3\R3EmailServiceProvider;

class ApplicationAcceptedEmail implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {

    }

    /**
     * Handle the event.
     *
     * @param UsersAfterEdit $afterEdit
     *
     * @return void
     *
     * @throws Exception
     */
    public function handle(UsersAfterEdit $afterEdit) {
        $fields = $afterEdit->getFields();

        if (array_key_exists('status', $fields) && $fields['status'] === UserStatusesEnum::APPLICATION_APPROVED) {
            /** @var Users $user */
            $user = Users::query()->find($fields['id']);

            if (!is_null($user)) {
                $service = new R3EmailServiceProvider();

                $service->sendAcceptedEmail($user);
            }
        }
    }
}
