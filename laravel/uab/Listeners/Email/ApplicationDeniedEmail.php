<?php
namespace Uab\Listeners\Email;

use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Uab\Events\AfterEdit\UsersAfterEdit;
use Uab\Http\Enums\QueueTypesEnum;
use Uab\Http\Enums\UserStatusesEnum;
use Uab\Http\Models\Emails;
use Uab\Http\Models\Users;
use Uab\Jobs\Emails\SendEmail;

class ApplicationDeniedEmail implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {

    }

    /**
     * Handle the event.
     *
     * @param UsersAfterEdit $afterEdit
     *
     * @return void
     *
     * @throws Exception
     */
    public function handle(UsersAfterEdit $afterEdit) {
        $fields = $afterEdit->getFields();

        /** @var Users|null $user */
        $user = Users::query()->find($fields['id']);

        if (is_null($user)) {
            throw new Exception('Could not send email. User not found - ' . json_encode($user));
        }

        if (array_key_exists('status', $fields) && $fields['status'] === UserStatusesEnum::APPLICATION_DENIED) {
            $email = new Emails();
            $email->subject = 'Application not approved';
            $email->message = 'Thank you for your interest in the SCIPE study. Based on your response, you are not eligible for this study. Please feel free to contact us for more information at scipe@uab.edu. We appreciate your time and have a good day!';
            $email->to_user_id = $user->id;
            $email->to_email = $user->email;
            $email->save();

            SendEmail::dispatch($email)->onQueue(QueueTypesEnum::NORMAL);
        }
    }
}
