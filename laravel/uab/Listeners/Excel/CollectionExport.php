<?php
namespace Uab\Listeners\Excel;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Cell\StringValueBinder;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class CollectionExport extends StringValueBinder
    implements FromCollection, ShouldAutoSize, WithColumnFormatting, WithHeadings, WithHeadingRow, WithStyles {
    use Exportable;

    public $collection;

    /**
     * QueryExport constructor.
     *
     * @param Collection $collection
     *
     * @return void
     */
    public function __construct($collection) {
        $this->collection = $collection;
    }

    public function collection() {
        return $this->collection;
    }

    public function columnFormats(): array {
        return [
            'A' => NumberFormat::FORMAT_TEXT,
            'B' => NumberFormat::FORMAT_NUMBER,
            'C' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            'D' => NumberFormat::FORMAT_TEXT,
            'E' => NumberFormat::FORMAT_TEXT,
            'F' => NumberFormat::FORMAT_TEXT
        ];
    }

    /**
     * @return array
     */
    public function headings():array {
        return [
            'Email',
            'Phone',
            'Group',
            'Date Taken',
            'Survey',
            'Question',
            'Answer'
        ];
    }

    public function styles(Worksheet $sheet) {
        return [
            1 => [
                'font' => [
                    'bold' => true,
                    'size' => 16
                ]
            ],
        ];
    }
}
