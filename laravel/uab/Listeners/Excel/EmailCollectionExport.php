<?php
namespace Uab\Listeners\Excel;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Collection;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Uab\Events\AfterCreate\AfterCreateInterface;
use Uab\Http\Enums\ExportFormatsEnum;
use \Exception;
use \Throwable;
use Uab\Providers\ExportServiceProvider;

class EmailCollectionExport implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var Collection $collection */
    private $collection;

    /** @var string string */
    private $email;

    /** @var string string */
    private $format;

    /**
     * Create the event listener.
     *
     * @param Collection $collection
     * @param string $email
     * @param string|null $format
     */
    public function __construct(Collection $collection, string $email, $format = null) {
        $this->collection = $collection;
        $this->email = $email;

        if (!in_array($format, ExportFormatsEnum::getAll())) {
            $format = 'xlsx';
        }

        $this->format = $format;
    }

    /**
     * Handle the event.
     *
     * @return void
     *
     * @throws Exception
     * @throws Throwable
     */
    public function handle() {
        $exportService = new ExportServiceProvider();

        $path = $exportService->toExcel($this->collection, $this->format);

        $exportService->emailExport($this->email, $path);
    }

    /**
     * Handle a job failure.
     *
     * @param AfterCreateInterface $event
     *
     * @return void
     */
    public function failed(AfterCreateInterface $event) {

    }
}
