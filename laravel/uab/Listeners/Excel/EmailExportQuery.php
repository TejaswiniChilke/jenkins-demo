<?php
namespace Uab\Listeners\Excel;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Uab\Events\AfterCreate\AfterCreateInterface;
use Uab\Events\Excel\ExportQuery;
use Uab\Http\Enums\QueueTypesEnum;
use Uab\Http\Models\EmailAttachments;
use Uab\Http\Models\Emails;
use Uab\Http\Models\EmailsEmailAttachments;
use Uab\Jobs\Emails\SendEmail;
use DB;
use \Exception;
use \Throwable;

class EmailExportQuery implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {

    }

    /**
     * Handle the event.
     *
     * @param ExportQuery $event
     *
     * @return void
     *
     * @throws Exception
     * @throws Throwable
     */
    public function handle(ExportQuery $event) {
        $path = storage_path('excel/'.uniqid().'.'.$event->format);

        $export = new QueryExport($event->query);

        if ($event->email) {
            $export->store($path);

            DB::transaction(
                function() use ($event, $path) {
                    $email = new Emails();
                    $email->to_email = $event->email;
                    $email->message = 'Attached is the export you requested.';
                    $email->subject = 'Requested Export';

                    $success = $email->save();

                    if ($success) {
                        $attachment = new EmailAttachments();
                        $attachment->path = $path;
                        $success = $attachment->save();

                        if ($success) {
                            $manyToMany = new EmailsEmailAttachments();
                            $manyToMany->email_attachment_id = $attachment->id;
                            $manyToMany->email_id = $email->id;
                            $success = $manyToMany->save();

                            if ($success) {
                                SendEmail::dispatch($email)->onQueue(QueueTypesEnum::EMAIL);
                            } else {
                                $attachment->delete();
                                $email->delete();

                                throw new Exception(
                                    'Unable to create the email\'s email attachment for the export'
                                );
                            }
                        } else {
                            $email->delete();

                            throw new Exception(
                                'Unable to create the email attachment for the export'
                            );
                        }
                    } else {
                        throw new Exception(
                            'Unable to create the email for the export'
                        );
                    }
                }
            );
        } else {
            $export->download($path);
        }
    }

    /**
     * Handle a job failure.
     *
     * @param AfterCreateInterface $event
     * @param Exception $exception
     *
     * @return void
     */
    public function failed(AfterCreateInterface $event, $exception) {
        //
    }
}
