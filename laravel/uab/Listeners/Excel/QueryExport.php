<?php
namespace Uab\Listeners\Excel;

use Illuminate\Database\Eloquent\Builder;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;

class QueryExport implements FromQuery {
    use Exportable;

    public $query;

    /**
     * QueryExport constructor.
     *
     * @param Builder $query
     */
    public function __construct($query) {
        $this->query = $query;
    }

    public function query() {
        return $this->query;
    }
}
