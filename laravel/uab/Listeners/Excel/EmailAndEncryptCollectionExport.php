<?php
namespace Uab\Listeners\Excel;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Collection;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Storage;
use Uab\Events\AfterCreate\AfterCreateInterface;
use Uab\Http\Enums\ExportFormatsEnum;
use \Exception;
use \Throwable;
use Uab\Providers\ExportServiceProvider;
use ZipArchive;

class EmailAndEncryptCollectionExport implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var Collection $collection */
    private $collection;

    /** @var string string */
    private $email;

    /** @var string string */
    private $format;

    /**
     * Create the event listener.
     *
     * @param Collection $collection
     * @param string $email
     * @param string|null $format
     */
    public function __construct(Collection $collection, string $email, $format = null) {
        $this->collection = $collection;
        $this->email = $email;

        if (!in_array($format, ExportFormatsEnum::getAll())) {
            $format = 'xlsx';
        }

        $this->format = $format;
    }

    /**
     * Handle the event.
     *
     * @return void
     *
     * @throws Exception
     * @throws Throwable
     */
    public function handle() {
        $exportService = new ExportServiceProvider();

        $path = $exportService->toExcel($this->collection, $this->format);

        $zipName = $this->encryptExport($path);

        $exportService->emailExport($this->email, $zipName);
    }

    /**
     * @param string $fileName
     *
     * @return false|string
     */
    private function encryptExport(string $fileName) {
        $zipName = 'export-' . time() . '.zip';
        $zipPath = Storage::path('/excel/') . $zipName;

        $zip = new ZipArchive();
        if ($zip->open($zipPath, ZipArchive::CREATE) === TRUE) {
            $filePath = Storage::path($fileName);

            $zip->setPassword('password');

            $zip->addFile($filePath, $fileName);
            $zip->setEncryptionName($fileName, ZipArchive::EM_AES_256);

            $zip->close();

            return $zipPath;
        } else {
            return false;
        }
    }

    /**
     * Handle a job failure.
     *
     * @param AfterCreateInterface $event
     *
     * @return void
     */
    public function failed(AfterCreateInterface $event) {

    }
}
