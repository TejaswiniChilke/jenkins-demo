<?php
namespace Uab\Listeners\Slack;

use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Pressutto\LaravelSlack\Facades\Slack;
use Uab\Events\AfterCreate\AfterCreateInterface;
use Uab\Http\Models\BugReports;
use Uab\Http\Models\Users;

class BugReportsSlack implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {

    }

    /**
     * Handle the event.
     *
     * @param AfterCreateInterface $afterCreate
     *
     * @return void
     *
     * @throws \Exception
     */
    public function handle(AfterCreateInterface $afterCreate) {
        /** @var BugReports $bugReport */
        $bugReport = $afterCreate->getModel();

        /** @var Users $reportingUser */
        $reportingUser = $bugReport->user();

        $message = $reportingUser->username.' reported a bug ('.url('/').')';

        Slack::to(config('SLACK_CHANNEL'))->send($message);
    }

    /**
     * Handle a job failure.
     *
     * @param AfterCreateInterface $event
     * @param Exception $exception
     *
     * @return void
     */
    public function failed(AfterCreateInterface $event, $exception) {
        //
    }
}
