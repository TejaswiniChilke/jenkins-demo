<?php
namespace Uab\Listeners\BadgeCheck;

use Carbon\Carbon;
use DB;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Uab\Events\AfterCreate\AfterCreateInterface;
use Uab\Events\AfterCreate\PostLikesAfterCreate;
use Uab\Http\Enums\QueueTypesEnum;
use Uab\Http\Models\BadgeConditionsUsers;
use Uab\Http\Models\BadgeConditionTypes;
use Uab\Http\Models\Badges;
use Uab\Http\Models\PostLikes;
use Uab\Http\Models\Users;
use Uab\Jobs\Base\InsertObjectJob;
use Uab\Providers\BadgeServiceProvider;
use Uab\Providers\ModelServiceProvider;

class PostLiked implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $service;
    private $modelService;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
        $this->modelService = new ModelServiceProvider();
        $this->service = new BadgeServiceProvider();
    }

    /**
     * Handle the event.
     *
     * @param PostLikesAfterCreate $afterCreate
     *
     * @return void
     */
    public function handle(PostLikesAfterCreate $afterCreate) {
        /** @var PostLikes $postLike */
        $postLike = $afterCreate->getModel();

        $user = $postLike->user();

        $badgeConditionType = $this->service->getConditionTypeByName('createPostLike');

        $badges = $this->service->getUserTypeBadges($user);

        $week = $user->getWeek();
        $badges->where('badges.start_week', '<=', $week)
            ->where('badges.end_week', '>=', $week);

        foreach ($badges as $badge) {
            $conditions = collect($this->service->getConditionsByBadgeId($badge))->pluck(
                'badge_condition_type_id',
                $badgeConditionType->id
            )->toArray();

            foreach ($conditions as $condition) {
                $userId = Users::decodeSlug($user->id);

                $query = DB::table('post_likes as p')
                    ->where(
                        [
                            'p.user_id' => $userId
                        ]
                    );

                if (!is_null($condition['days'])) {
                    $query->where(
                        'p.created',
                        '>=',
                        'CURDATE() - INTERVAL DAYOFWEEK(CURDATE())-'.$condition['days'].' DAY'
                    );
                }

                $likeCount = $query->count();

                if ($condition['value'] <= $likeCount) {
                    $userCondition = new BadgeConditionsUsers();
                    $userCondition->badge_condition_id = $condition->id;
                    $userCondition->user_id = $user->id;
                    $userCondition->earned_at = Carbon::now();
                    InsertObjectJob::dispatch($userCondition)->onQueue(QueueTypesEnum::HIGH);
                }
            }
        }
    }

    /**
     * Handle a job failure.
     *
     * @param AfterCreateInterface $event
     * @param \Exception $exception
     *
     * @return void
     */
    public function failed(AfterCreateInterface $event, $exception) {
        //
    }
}
