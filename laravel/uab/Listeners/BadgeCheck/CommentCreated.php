<?php
namespace Uab\Listeners\BadgeCheck;

use \Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Uab\Events\AfterCreate\AfterCreateInterface;
use Uab\Events\AfterCreate\CommentsAfterCreate;
use Uab\Http\Enums\QueueTypesEnum;
use Uab\Http\Models\BadgeConditionsUsers;
use Uab\Http\Models\BadgeConditionTypes;
use Uab\Http\Models\Badges;
use Uab\Http\Models\Comments;
use Uab\Http\Models\Users;
use Uab\Jobs\Base\InsertObjectJob;
use Uab\Providers\BadgeServiceProvider;

class CommentCreated implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {

    }

    /**
     * Handle the event.
     *
     * @param CommentsAfterCreate $afterCreate
     *
     * @return void
     *
     * @throws Exception
     */
    public function handle(CommentsAfterCreate $afterCreate) {
        $service = new BadgeServiceProvider();

        /** @var Comments $comment */
        $comment = $afterCreate->getModel();

        $user = $comment->user();

        /** @var BadgeConditionTypes $badgeConditionType */
        $badgeConditionType = $service->getConditionTypeByName('createComment');

        $badges = $service->getUserTypeBadges($user);

        $week = $user->getWeek();
        $badges->where('badges.start_week', '<=', $week)
            ->where('badges.end_week', '>=', $week);


        foreach ($badges as $badge) {
            $conditions = $service->getConditionsByBadgeId($badge->id);
            foreach ($conditions as $condition) {
                if ($condition->badge_condition_type_id === $badgeConditionType->id) {
                    $userId = Users::decodeSlug($user->id);

                    $query = Comments::query()
                        ->where(
                            [
                                'comments.user_id' => $userId
                            ]
                        );

                    if (!is_null($condition->days)) {
                        $query->where(
                            'created',
                            '>=',
                            'CURDATE() - INTERVAL DAYOFWEEK(CURDATE())-' . $condition->days . ' DAY'
                        );
                    }

                    $commentCount = $query->count();

                    if ($condition->value > $commentCount) {
                        continue;
                    }

                    // TODO: Check if maxed out conditions
                    if (true) {
                        $userCondition = new BadgeConditionsUsers();
                        $userCondition->badge_condition_id = $condition->id;
                        $userCondition->user_id = $user->id;
                        $userCondition->earned_at = $comment->created;
                        InsertObjectJob::dispatch($userCondition)->onQueue(QueueTypesEnum::HIGH);
                    }
                }
            }
        }
    }

    /**
     * Handle a job failure.
     *
     * @param AfterCreateInterface $event
     * @param Exception $exception
     *
     * @return void
     */
    public function failed(AfterCreateInterface $event, $exception) {
        //
    }
}
