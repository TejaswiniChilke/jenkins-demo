<?php
namespace Uab\Listeners\BadgeCheck;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Uab\Events\AfterCreate\AfterCreateInterface;
use Uab\Events\AfterCreate\EventsAfterCreate;
use Uab\Http\Enums\QueueTypesEnum;
use Uab\Http\Models\BadgeConditions;
use Uab\Http\Models\BadgeConditionsUsers;
use Uab\Http\Models\BadgeConditionTypes;
use Uab\Http\Models\Badges;
use Uab\Http\Models\Events;
use Uab\Http\Models\Users;
use Uab\Jobs\Base\InsertObjectJob;
use Uab\Providers\BadgeServiceProvider;
use Uab\Providers\ModelServiceProvider;

class EventCreated implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $service;
    private $modelService;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
        $this->modelService = new ModelServiceProvider();
        $this->service = new BadgeServiceProvider();
    }

    /**
     * Handle the event.
     *
     * @param EventsAfterCreate $afterCreate
     *
     * @return void
     */
    public function handle(EventsAfterCreate $afterCreate) {
        /** @var Events $event */
        $event = $afterCreate->getModel();

        $user = $event->creator();

        if ($user !== null) {
            $user = $this->modelService->stdToModel(
                $user,
                new Users()
            );
            /** @var Users $user */

            $badgeConditionType = $this->service->getConditionTypeByName('createEvent');

            $badges = $this->service->getUserTypeBadges($user);

            $week = $user->getWeek();
            $badges->where('badges.start_week', '<=', $week)
                ->where('badges.end_week', '>=', $week);

            foreach ($badges as $badge) {
                /** @var BadgeConditions[] $conditions */
                $conditions = collect($this->service->getConditionsByBadgeId($badge))->pluck(
                    'badge_condition_type_id',
                    $badgeConditionType->id
                )->toArray();

                foreach ($conditions as $condition) {
                    $userId = Users::decodeSlug($user->id);

                    $query = Events::query()
                        ->where(
                            [
                                'events.creator_id' => $userId
                            ]
                        );

                    if (!is_null($condition->days)) {
                        $query->where(
                            'events.created',
                            '>=',
                            'CURDATE() - INTERVAL DAYOFWEEK(CURDATE())-'.$condition->days.' DAY'
                        );
                    }

                    $likeCount = $query->count();

                    if ($condition->value <= $likeCount) {
                        // TODO: Check if maxed out conditions
                        if (true) {
                            $userCondition = new BadgeConditionsUsers();
                            $userCondition->badge_condition_id = $condition->id;
                            $userCondition->user_id = $user->id;
                            $userCondition->earned_at = $event->created;
                            InsertObjectJob::dispatch($userCondition)->onQueue(QueueTypesEnum::HIGH);
                        }
                    }
                }
            }
        }
    }

    /**
     * Handle a job failure.
     *
     * @param AfterCreateInterface $event
     * @param \Exception $exception
     *
     * @return void
     */
    public function failed(AfterCreateInterface $event, $exception) {
        //
    }
}
