<?php
namespace Uab\Listeners\BadgeCheck;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Redis;
use Uab\Events\AfterCreate\BadgeConditionsUsersAfterCreate;
use Uab\Http\Models\BadgeConditions;
use Uab\Http\Models\BadgeConditionsUsers;
use Uab\Http\Models\Badges;
use Uab\Http\Models\BadgesUsers;
use Uab\Http\Models\Users;
use Uab\Providers\BadgeServiceProvider;
use DateTime;
use \Exception;

class AwardBadges implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Handle the event.
     *
     * @param BadgeConditionsUsersAfterCreate $afterCreate
     *
     * @return void
     *
     * @throws Exception
     */
    public function handle(BadgeConditionsUsersAfterCreate $afterCreate) {
        /** @var BadgeConditionsUsers $badgeConditionUser */
        $badgeConditionUser = $afterCreate->getModel();

        $badge = $this->getBadge($badgeConditionUser);
        if (is_null($badge)) {
            return;
        }

        $user = $badgeConditionUser->user();
        if (is_null($user)) {
            return;
        }

        Redis::funnel('award-badge-' . $user->id . '-' . $badge->id)->limit(1)->then(
            function () use ($badgeConditionUser, $badge, $user) {
                $reward = $this->hasNotMaxedOut($badge, $user);
                $reward = $reward && $this->checkConditions($badge);

                if ($reward) {
                    $this->awardBadge($badge, $user, $badgeConditionUser->earned_at);
                }
            },
            function () {
                $this->release(30);
            }
        );
    }

    /**
     * @param Badges $badge
     * @param Users $user
     * @param Carbon|string|null $earnedAt
     *
     * @return void
     */
    private function awardBadge(Badges $badge, Users $user, $earnedAt = null) {
        $badgeUser = new BadgesUsers();
        $badgeUser->badge_id = $badge->id;
        $badgeUser->user_id = $user->id;
        $badgeUser->earned_at = is_null($earnedAt) ? Carbon::now() : $earnedAt;
        $badgeUser->save();
    }

    /**
     * @param Badges $badge
     * @param Users $user
     *
     * @return bool
     */
    private function hasNotMaxedOut(Badges $badge, Users $user):bool {
        $badgeService = new BadgeServiceProvider();

        $badgeUsers = $badgeService->getCompletedBadges($badge, $user);

        $count = $badgeUsers->count();

        return $count < $badge->max_completions;
    }

    /**
     * @param Badges $badge
     *
     * @return bool
     */
    private function checkConditions(Badges $badge):bool {
        $badgeService = new BadgeServiceProvider();

        $completed = [];

        /** @var BadgeConditions[] $badgeConditions */
        $badgeConditions = $badgeService->getConditionsByBadgeId($badge->id);
        foreach ($badgeConditions as $badgeCondition) {
            $badgeConditionUsers = $badgeService->getCompletedConditions(
                $badgeCondition
            );

            $count = $badgeConditionUsers->count();
            if ($count === 0) {
                continue;
            }

            $withinDays = $this->isWithinDays($badgeCondition);
            if ($withinDays) {
                $completed[$badgeCondition->id] = true;
            }
        }

        $isCompleteAny = $badge->can_complete_any;
        $completedCount = count($completed);
        $conditionCount = count($badgeConditions);

        return $isCompleteAny ?
            $completedCount !== 0 : $completedCount === $conditionCount;
    }

    /**
     * @param BadgeConditionsUsers $badgeConditionUser
     *
     * @return Badges
     */
    private function getBadge($badgeConditionUser) {
        $badgeCondition = $badgeConditionUser->badgeCondition();

        /** @var Badges $badge */
        $badge = $badgeCondition->badge();

        return $badge;
    }

    /**
     * @param BadgeConditions $badgeCondition
     *
     * @return bool
     */
    private function isWithinDays(BadgeConditions $badgeCondition):bool {
        if (true && $badgeCondition->days === 0) { // TODO
            return true;
        }

        $badgeConditionUser = $badgeCondition->user();

        try {
            $earliestPossible = new DateTime(
                '-' . $badgeCondition->days . ' days'
            );

            $earned = new DateTime(
                $badgeConditionUser->created
            );

            $withinDays = $earliestPossible <= $earned;
        } catch (Exception $e) {
            $withinDays = false;
        }

        return $withinDays;
    }
}
