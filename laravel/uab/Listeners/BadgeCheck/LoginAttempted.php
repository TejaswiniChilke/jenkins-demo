<?php
namespace Uab\Listeners\BadgeCheck;

use \Exception;
use Uab\Events\AfterCreate\LoginAttemptsAfterCreate;
use Uab\Http\Enums\BadgeConditionTypesEnum;
use Uab\Http\Models\LoginAttempts;

class LoginAttempted extends BadgeCheck {
    /**
     * Handle the event.
     *
     * @param LoginAttemptsAfterCreate $afterCreate
     *
     * @return void
     *
     * @throws Exception
     */
    public function handle(LoginAttemptsAfterCreate $afterCreate) {
        /** @var LoginAttempts $attempt */
        $attempt = $afterCreate->getModel();

        if ($attempt->success) {
            $user = $attempt->user();
            if (is_null($user)) {
                return;
            }

            $this->checkMatchingConditionsOfType($user, BadgeConditionTypesEnum::LOGIN);
        }
    }
}
