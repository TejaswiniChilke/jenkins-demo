<?php
namespace Uab\Listeners\BadgeCheck;

use \Exception;
use Uab\Events\AfterCreate\AnalyticVideosAfterCreate;
use Uab\Http\Enums\BadgeConditionTypesEnum;
use Uab\Http\Models\AnalyticVideos;

class VideoWatched extends BadgeCheck {
    /**
     * Handle the event.
     *
     * @param AnalyticVideosAfterCreate $afterCreate
     *
     * @return void
     *
     * @throws Exception
     */
    public function handle(AnalyticVideosAfterCreate $afterCreate) {
        /** @var AnalyticVideos $analytic */
        $analytic = $afterCreate->getModel();

        $user = $analytic->user();
        if (is_null($user)) {
            return;
        }

        $this->checkMatchingConditionsOfType($user, BadgeConditionTypesEnum::VIDEO_ID);
    }
}
