<?php
namespace Uab\Listeners\BadgeCheck;

use \Exception;
use Uab\Events\AfterCreate\AnalyticArticlesAfterCreate;
use Uab\Http\Enums\BadgeConditionTypesEnum;
use Uab\Http\Models\AnalyticArticles;

class ArticleRead extends BadgeCheck {
    /**
     * Handle the event.
     *
     * @param AnalyticArticlesAfterCreate $afterCreate
     *
     * @return void
     *
     * @throws Exception
     */
    public function handle(AnalyticArticlesAfterCreate $afterCreate) {
        /** @var AnalyticArticles $analytic */
        $analytic = $afterCreate->getModel();

        $user = $analytic->user();
        if (is_null($user)) {
            return;
        }

        $this->checkMatchingConditionsOfType($user, BadgeConditionTypesEnum::ARTICLE_ID);
    }
}
