<?php
namespace Uab\Listeners\BadgeCheck;

use DB;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Uab\Events\AfterCreate\AfterCreateInterface;
use Uab\Events\AfterCreate\PostsAfterCreate;
use Uab\Http\Enums\QueueTypesEnum;
use Uab\Http\Models\BadgeConditionsUsers;
use Uab\Http\Models\Posts;
use Uab\Http\Models\Users;
use Uab\Jobs\Base\InsertObjectJob;
use Uab\Providers\BadgeServiceProvider;

class PostCreated implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $service;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
        $this->service = new BadgeServiceProvider();
    }

    /**
     * Handle the event.
     *
     * @param PostsAfterCreate $afterCreate
     *
     * @return void
     *
     * @throws \Exception
     */
    public function handle(PostsAfterCreate $afterCreate) {
        /** @var Posts $post */
        $post = $afterCreate->getModel();

        $from = $post->fromUser();
        if (!is_null($from)) {
            /** @var Users $user */
            $user = Users::query()->find($from->id);

            $badgeConditionType = $this->service->getConditionTypeByName('createPost');

            $badges = $this->service->getUserTypeBadges($user);

            $week = $user->getWeek();
            $badges->where('badges.start_week', '<=', $week)
                ->where('badges.end_week', '>=', $week);

            foreach ($badges as $badge) {
                $conditions = $this->service->getConditionsByBadgeId($badge)->pluck(
                    'badge_condition_type_id',
                    $badgeConditionType->id
                )->toArray();

                foreach ($conditions as $condition) {
                    $userId = Users::decodeSlug($user->id);

                    $query = Posts::query()
                        ->where(
                            [
                                'posts.from_user_id' => $userId
                            ]
                        );

                    if (is_array($condition)) {
                        if (!is_null($condition['days'])) {
                            $query->where(
                                'posts.created',
                                '>=',
                                'CURDATE() - INTERVAL DAYOFWEEK(CURDATE())-'.$condition['days'].' DAY'
                            );
                        }

                        $postsCount = $query->count();

                        if ($condition['value'] <= $postsCount) {
                            $userCondition = new BadgeConditionsUsers();
                            $userCondition->badge_condition_id = $condition['id'];
                            $userCondition->user_id = $user->id;
                            $userCondition->earned_at = $post->created;
                            InsertObjectJob::dispatch($userCondition)->onQueue(QueueTypesEnum::HIGH);
                        }
                    }
                }
            }
        }
    }

    /**
     * Handle a job failure.
     *
     * @param AfterCreateInterface $event
     * @param \Exception $exception
     *
     * @return void
     */
    public function failed(AfterCreateInterface $event, $exception) {
        //
    }
}
