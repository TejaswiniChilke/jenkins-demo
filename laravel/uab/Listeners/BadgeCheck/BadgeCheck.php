<?php
namespace Uab\Listeners\BadgeCheck;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Uab\Http\Enums\QueueTypesEnum;
use Uab\Http\Models\Users;
use Uab\Jobs\Badges\CheckUserBadge;
use Uab\Providers\BadgeServiceProvider;

class BadgeCheck implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {

    }

    /**
     * Handle the event.
     *
     * @param Users $user
     * @param string $typeName
     *
     * @return void
     */
    public function checkMatchingConditionsOfType(Users $user, string $typeName) {
        $badgeService = new BadgeServiceProvider();
        $badgeConditionType = $badgeService->getConditionTypeByName($typeName);

        $candidateBadges = $badgeService->getUserTypeBadges($user->user_type_id, $badgeConditionType);
        foreach ($candidateBadges as $candidateBadge) {
            CheckUserBadge::dispatch($user->id, $candidateBadge->id)
                ->onQueue(QueueTypesEnum::HIGH);
        }
    }
}
