<?php
namespace Uab\Listeners\Users;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Uab\Events\AfterCreate\UsersAfterCreate;
use Uab\Events\AfterEdit\UsersAfterEdit;
use Uab\Http\Enums\QueueTypesEnum;
use Uab\Http\Models\UserStatusChanges;
use Uab\Jobs\Base\InsertObjectJob;
use \Exception;

class UserStatusChanged implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {

    }

    /**
     * Handle the event.
     *
     * @param UsersAfterEdit|UsersAfterCreate $after
     *
     * @return void
     *
     * @throws Exception
     */
    public function handle($after) {
        $fields = [];
        if (get_class($after) === UsersAfterCreate::class) {
            /** @var UsersAfterCreate $after */
            $fields = $after->getModel()->toArray();
        } else if (get_class($after) === UsersAfterEdit::class) {
            /** @var UsersAfterEdit $after */
            $fields = $after->getFields();
        }

        if (array_key_exists('id', $fields)) {
            if (array_key_exists('status', $fields) && !is_null($fields['status'])) {
                $log = new UserStatusChanges();
                $log->user_id = $fields['id'];
                $log->status = $fields['status'];

                InsertObjectJob::dispatch($log)->onQueue(QueueTypesEnum::LOW);
            }
        }
    }
}
