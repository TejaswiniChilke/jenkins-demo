<?php
namespace Uab\Http\Response\Macros;

use Log;
use Response;
use Uab\Http\Enums\HttpResponseCodesEnum;

class ResponseErrorMacro {
    /**
     * Register the application's response macros.
     *
     * @return void
     */
    public function __construct() {
        $this->error();
    }

    /**
     * @param string|null       $errorMessage
     * @param HttpResponseCodesEnum $responseCode
     */
    public function error($errorMessage = null, $responseCode = null) {
        Response::macro(
            'error',
            function($message, $status = HttpResponseCodesEnum::SERVER_ERROR_INTERNAL_SERVER_ERROR) {
                Log::error($message);

                return Response::make(
                    [
                        'error' => $message
                    ],
                    $status
                );
            }
        );
    }
}
