<?php
namespace Uab\Http\Response;

use Uab\Http\Response\Macros\ResponseErrorMacro;

class ResponseMacroFactory {
    public function __construct() {

    }

    public function boot() {
        new ResponseErrorMacro();
    }
}
