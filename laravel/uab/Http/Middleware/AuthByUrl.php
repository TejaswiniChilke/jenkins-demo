<?php
namespace Uab\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Illuminate\Auth\AuthenticationException;
use Laravel\Passport\Token;
use Uab\Exceptions\TokenExpired;
use Uab\Exceptions\TokenInvalid;
use Uab\Exceptions\TokenMissing;
use Uab\Providers\Auth;
use Uab\Providers\OAuthServiceProvider;

class AuthByUrl {
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     *
     * @return mixed
     *
     * @throws AuthenticationException
     */
    public function handle($request, Closure $next) {
        $token = $request->input('token', null);
        if (is_null($token)) {
            throw new TokenMissing();
        } else {
            /** @var Token $token */
            $token = Token::query()
                ->where(
                    [
                        'id' => $token,
                        'revoked' => 0
                    ]
                )->first();

            if (is_null($token)) {
                throw new TokenInvalid();
            } else {
                $expiresIn = Carbon::parse($token->expires_at);

                if ($expiresIn->isBefore(Carbon::now())) {
                    throw new TokenExpired();
                }
            }

            $user = $token->user()->first();

            if (is_null($user)) {
                throw new AuthenticationException('Unauthenticated.');
            } else {
                $user->withAccessToken($token);

                Auth::login($user);

                if (!config('auth.options.lifetime')) {
                    $oauthService = new OAuthServiceProvider();

                    $oauthService->renewTokenExpirationTime();
                }

                return $next($request);
            }
        }
    }
}
