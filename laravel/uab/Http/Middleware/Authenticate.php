<?php
namespace Uab\Http\Middleware;

use Closure;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Uab\Providers\OAuthServiceProvider;
use \Illuminate\Http\Request;
use Illuminate\Auth\AuthenticationException;

class Authenticate extends Middleware {
    const EXCLUDE_ROUTES = [
        'color_templates' => [
            'get'
        ],
        'lingos' => [
            'get'
        ],
        'settings' => [
            'get'
        ]
    ];

    /**
     * @param Request $request
     * @param Closure $next
     * @param mixed ...$guards
     *
     * @return mixed
     *
     * @throws AuthenticationException
     */
    public function handle($request, Closure $next, ...$guards) {
        $shouldAuthenticate = true;

        $segments = $request->segments();

        if (count($segments) >= 3) {
            $model = $segments[1];
            $action = $segments[2];

            if (array_key_exists($model, self::EXCLUDE_ROUTES)) {
                if (in_array($action, self::EXCLUDE_ROUTES[$model])) {
                    $shouldAuthenticate = false;
                }
            }
        }

        if ($shouldAuthenticate) {
            $this->authenticate($request, $guards);
        }

        return $next($request);
    }

    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  Request  $request
     *
     * @return string
     */
    protected function redirectTo($request) {
        return;
    }

    /**
     * @param Request $request
     * @param array $guards
     *
     * @throws AuthenticationException
     */
    protected function authenticate($request, array $guards) {
        parent::authenticate($request, $guards);

        if (!config('auth.options.lifetime')) {
            $oauthService = new OAuthServiceProvider();

            $oauthService->renewTokenExpirationTime();
        }
    }
}


