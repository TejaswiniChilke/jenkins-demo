<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\SurveyPages as GeneratedModel;

/**
 * Class SurveyPages
 *
 * @package Uab\Http\Models
 */
class SurveyPages extends GeneratedModel {
    use HasHashSlug;

}
