<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\UsersWeeklyReminders as GeneratedModel;

/**
 * Class UsersWeeklyReminders
 *
 * @package Uab\Http\Models
 */
class UsersWeeklyReminders extends GeneratedModel {
    use HasHashSlug;

}
