<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\ContactStaffMessages as GeneratedModel;

/**
 * Class ContactStaffMessages
 *
 * @package Uab\Http\Models
 */
class ContactStaffMessages extends GeneratedModel {
    use HasHashSlug;

    public $defaultDataTableColumns = [
        'created',
        'email',
        'message'
    ];
}
