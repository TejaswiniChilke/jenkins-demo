<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\Migrations as GeneratedModel;

/**
 * Class Migrations
 *
 * @package Uab\Http\Models
 */
class Migrations extends GeneratedModel {
    use HasHashSlug;

}
