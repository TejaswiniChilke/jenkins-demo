<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\FavoriteVideos as GeneratedModel;

/**
 * Class FavoriteVideos
 *
 * @package Uab\Http\Models
 */
class FavoriteVideos extends GeneratedModel {
    use HasHashSlug;

}
