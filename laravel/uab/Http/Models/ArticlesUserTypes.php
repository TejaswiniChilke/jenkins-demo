<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\ArticlesUserTypes as GeneratedModel;

/**
 * Class ArticlesUserTypes
 *
 * @package Uab\Http\Models
 */
class ArticlesUserTypes extends GeneratedModel {
    use HasHashSlug;

}
