<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\TermsAndConditionsUsers as GeneratedModel;

/**
 * Class TermsAndConditionsUsers
 *
 * @package Uab\Http\Models
 */
class TermsAndConditionsUsers extends GeneratedModel {
    use HasHashSlug;

}
