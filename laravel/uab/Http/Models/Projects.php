<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\Projects as GeneratedModel;

/**
 * Class Projects
 *
 * @package Uab\Http\Models
 */
class Projects extends GeneratedModel {
    use HasHashSlug;

    protected $testColumn = 'name';
}
