<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\WebsocketsStatisticsEntries as GeneratedModel;

/**
 * Class WebsocketsStatisticsEntries
 *
 * @package Uab\Http\Models
 */
class WebsocketsStatisticsEntries extends GeneratedModel {
    use HasHashSlug;

}
