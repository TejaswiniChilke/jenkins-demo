<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\Lingos as GeneratedModel;

/**
 * Class Lingos
 *
 * @package Uab\Http\Models
 */
class Lingos extends GeneratedModel {
    use HasHashSlug;

}
