<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\States as GeneratedModel;

/**
 * Class States
 *
 * @package Uab\Http\Models
 */
class States extends GeneratedModel {
    use HasHashSlug;

}
