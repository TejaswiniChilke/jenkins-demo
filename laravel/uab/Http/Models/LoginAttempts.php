<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\LoginAttempts as GeneratedModel;

/**
 * Class LoginAttempts
 *
 * @package Uab\Http\Models
 */
class LoginAttempts extends GeneratedModel {
    use HasHashSlug;

    public $defaultDataTableColumns = [
        'username',
        'user_id',
        'success',
        'attempted_at',
        'ip_address',
        'device',
    ];
}
