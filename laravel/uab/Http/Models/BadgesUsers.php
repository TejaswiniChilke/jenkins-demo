<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\BadgesUsers as GeneratedModel;

/**
 * Class BadgesUsers
 *
 * @package Uab\Http\Models
 */
class BadgesUsers extends GeneratedModel {
    use HasHashSlug;

}
