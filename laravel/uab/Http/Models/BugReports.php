<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\BugReports as GeneratedModel;

/**
 * Class BugReports
 *
 * @package Uab\Http\Models
 */
class BugReports extends GeneratedModel {
    use HasHashSlug;

    public $defaultDataTableColumns = [
        'created',
        'user_id',
        'message'
    ];
}
