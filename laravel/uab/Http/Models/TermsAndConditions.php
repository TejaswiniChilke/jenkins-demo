<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\TermsAndConditions as GeneratedModel;

/**
 * Class TermsAndConditions
 *
 * @package Uab\Http\Models
 */
class TermsAndConditions extends GeneratedModel {
    use HasHashSlug;

}
