<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\Files as GeneratedModel;

/**
 * Class Files
 *
 * @package Uab\Http\Models
 */
class Files extends GeneratedModel {
    use HasHashSlug;

}
