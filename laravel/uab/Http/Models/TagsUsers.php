<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\TagsUsers as GeneratedModel;

/**
 * Class TagsUsers
 *
 * @package Uab\Http\Models
 */
class TagsUsers extends GeneratedModel {
    use HasHashSlug;

}
