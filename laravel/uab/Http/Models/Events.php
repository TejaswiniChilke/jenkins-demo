<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\Events as GeneratedModel;

/**
 * Class Events
 *
 * @package Uab\Http\Models
 */
class Events extends GeneratedModel {
    use HasHashSlug;

    public $relationshipTypes = [
        'to_users'   => 'users',
        'from_users' => 'users'
    ];

    public function setFromUserIdAttribute($fromUserId) {
        $fromEmail = $this->getAttributeValue('from_email');
        if (is_null($fromEmail)) {
            $user = Users::query()
                ->select()
                ->where(
                    [
                        'id' => $fromUserId
                    ]
                )->get()
                ->toArray();

            if (is_array($user) && count($user) !== 0) {
                $user = $user[0];

                $this->attributes['from_email'] = $user->email;
            }
        }

        $this->attributes['from_user_id'] = $fromUserId;
    }

    public function setToUserIdAttribute($toUserId) {
        $toEmail = $this->getAttributeValue('to_email');
        if (is_null($toEmail)) {
            $user = Users::query()
                ->select()
                ->where(
                    [
                        'id' => $toUserId
                    ]
                )->get()
                ->toArray();

            if (is_array($user) && count($user) !== 0) {
                $user = $user[0];

                $this->attributes['to_email'] = $user->email;
            }
        }

        $this->attributes['to_user_id'] = $toUserId;
    }

    private $attendees;

    /**
     * @return Users[]
     */
    public function getAttendees() {
        if (!isset($this->attendees)) {
            $this->attendees = EventsUsers::query()
                ->select()
                ->where(
                    [
                        'event_id' => $this->attributes['id']
                    ]
                )->get()
                ->toArray();
        }

        return $this->attendees;
    }
}
