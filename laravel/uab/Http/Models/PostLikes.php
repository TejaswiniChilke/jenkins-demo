<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\PostLikes as GeneratedModel;

/**
 * Class PostLikes
 *
 * @package Uab\Http\Models
 */
class PostLikes extends GeneratedModel {
    use HasHashSlug;

}
