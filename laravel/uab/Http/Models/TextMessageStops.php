<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\TextMessageStops as GeneratedModel;

/**
 * Class TextMessageStops
 *
 * @package Uab\Http\Models
 */
class TextMessageStops extends GeneratedModel {
    use HasHashSlug;

}
