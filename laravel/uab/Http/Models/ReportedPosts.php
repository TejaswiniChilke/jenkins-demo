<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\ReportedPosts as GeneratedModel;

/**
 * Class ReportedPosts
 *
 * @package Uab\Http\Models
 */
class ReportedPosts extends GeneratedModel {
    use HasHashSlug;

    public $relationshipTypes = [
        'verified_users' => 'users'
    ];
}
