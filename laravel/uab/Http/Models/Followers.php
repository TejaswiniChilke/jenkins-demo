<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\Followers as GeneratedModel;

/**
 * Class Followers
 *
 * @package Uab\Http\Models
 */
class Followers extends GeneratedModel {
    use HasHashSlug;

    public $relationshipTypes = [
        'followings'   => 'users',
    ];
}
