<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Illuminate\Database\Eloquent\Builder;
use Uab\Http\Models\Generated\Videos as GeneratedModel;
use Uab\Http\Models\Traits\Videos\HasFavorited;
use Uab\Http\Models\Traits\Videos\HasLiked;
use Uab\Http\Models\Traits\Videos\TagCsv;
use Uab\Providers\Auth;

/**
 * Class Videos
 *
 * @package Uab\Http\Models
 */
class Videos extends GeneratedModel {
    use HasHashSlug;
    use HasLiked;
    use HasFavorited;
    use TagCsv;

    public $appends = [
        'backup_image',
        'has_favorited',
        'has_liked',
        'tag_csv'
    ];

    protected $defaultDataTableColumns = [
        'title',
        'file_path',
        'short_description',
        'week',
        'end_week',
        'tag_csv'
    ];

    public function getBackupImageAttribute() {
        /** @var YoutubeVideoInfos|null $youtubeInfo */
        $youtubeInfo = YoutubeVideoInfos::query()
            ->where(
                [
                    'video_id' => $this->getAttribute('id')
                ]
            )->first();

        $backupImage = null;
        if (!is_null($youtubeInfo)) {
            $backupImage = $youtubeInfo->thumbnail_default;
            if (is_null($backupImage)) {
                $backupImage = $youtubeInfo->thumbnail_standard;
            }
            if (is_null($backupImage)) {
                $backupImage = $youtubeInfo->thumbnail_medium;
            }
            if (is_null($backupImage)) {
                $backupImage = $youtubeInfo->thumbnail_high;
            }
            if (is_null($backupImage)) {
                $backupImage = $youtubeInfo->thumbnail_max;
            }
        }

        return $backupImage;
    }
}
