<?php
namespace Uab\Http\Models\Generated;

use Uab\Http\Models\Base\BaseModel;


/**
 * Class Zipcodes
 *
 * @property number id
 * @property string created
 * @property string modified
 * @property string zipcode
 * @property string name
 * @property string state
 * @property string latitude
 * @property string longitude
 * @property string city
 * @property string timezone
 * @property boolean has_daylight_savings
 * @property number population
 *
 * @package Uab\Http\Models\Generated
 */
class Zipcodes extends BaseModel {

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';


    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created' => null,
        'modified' => null,
        'name' => null,
        'state' => null,
        'latitude' => null,
        'longitude' => null,
        'city' => null,
        'timezone' => null,
        'has_daylight_savings' => 1,
        'population' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime',
		'zipcode' => 'string',
		'name' => 'string',
		'state' => 'string',
		'latitude' => 'string',
		'longitude' => 'string',
		'city' => 'string',
		'timezone' => 'string',
		'has_daylight_savings' => 'boolean',
		'population' => 'integer'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'created',
		'modified',
		'zipcode',
		'name',
		'state',
		'latitude',
		'longitude',
		'city',
		'timezone',
		'has_daylight_savings',
		'population'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'zipcodes';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'zipcode',
		'name',
		'state',
		'latitude',
		'longitude',
		'city',
		'timezone',
		'has_daylight_savings',
		'population'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [

    ];

}
