<?php
namespace Uab\Http\Models\Generated;

use Uab\Events\AfterCreate\AfterCreate;
use Uab\Events\AfterEdit\AfterEdit;
use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use stdClass;

/**
 * Class BadgesUserTypes
 *
 * @property number id
 * @property string created
 * @property string modified
 * @property number badge_id
 * @property number user_type_id
 *
 * @package Uab\Http\Models\Generated
 */
class BadgesUserTypes extends BaseModel {
    
    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created' => null,
        'modified' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime',
		'badge_id' => 'integer',
		'user_type_id' => 'integer'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'created',
		'modified',
		'badge_id',
		'user_type_id'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'badges_user_types';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'badge_id',
		'user_type_id'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'badges' => 'badge_id',
		'user_types' => 'user_type_id'
    ];

	/**
	 * @return stdClass
	*/
	public function badge() {
		return DB::table('badges')->where(
			[
				'id' => $this->getAttributeValue('badge_id')
			]
		)->first();
	}

	/**
	 * @return stdClass
	*/
	public function userType() {
		return DB::table('user_types')->where(
			[
				'id' => $this->getAttributeValue('user_type_id')
			]
		)->first();
	}

}
