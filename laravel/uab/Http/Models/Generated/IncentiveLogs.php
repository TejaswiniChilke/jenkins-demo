<?php
namespace Uab\Http\Models\Generated;

use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use stdClass;

/**
 * Class IncentiveLogs
 *
 * @property number id
 * @property string created
 * @property string modified
 * @property string deleted
 * @property number amount
 * @property mixed date_earned
 * @property string event
 *
 * @package Uab\Http\Models\Generated
 */
class IncentiveLogs extends BaseModel {
    
    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted';
    


    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created' => null,
        'modified' => null,
        'deleted' => null,
        'amount' => null,
        'date_earned' => null,
        'event' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime',
		'deleted' => 'datetime',
		'amount' => 'decimal:12',
		'date_earned' => 'date',
		'event' => 'string'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'created',
		'modified',
		'deleted',
		'amount',
		'date_earned',
		'event'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'incentive_logs';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'deleted',
		'amount',
		'date_earned',
		'event'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified',
		'deleted',
		'date_earned'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        
    ];

}
