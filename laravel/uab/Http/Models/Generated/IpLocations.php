<?php
namespace Uab\Http\Models\Generated;

use Uab\Http\Models\Base\BaseModel;


/**
 * Class IpLocations
 *
 * @property number ip_from
 * @property number ip_to
 * @property mixed country_code
 * @property string country_name
 * @property string region_name
 * @property string city_name
 * @property mixed latitude
 * @property mixed longitude
 * @property string zipcode
 * @property number id
 * @property string created
 * @property string modified
 * @property string state_abbreviation
 *
 * @package Uab\Http\Models\Generated
 */
class IpLocations extends BaseModel {
    
    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';
    

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'ip_from' => null,
        'ip_to' => null,
        'country_code' => null,
        'country_name' => null,
        'region_name' => null,
        'city_name' => null,
        'latitude' => null,
        'longitude' => null,
        'zipcode' => null,
        'created' => null,
        'modified' => null,
        'state_abbreviation' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'ip_from' => 'integer',
		'ip_to' => 'integer',
		'country_code' => 'string',
		'country_name' => 'string',
		'region_name' => 'string',
		'city_name' => 'string',
		'latitude' => 'string',
		'longitude' => 'string',
		'zipcode' => 'string',
		'id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime',
		'state_abbreviation' => 'string'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'ip_from',
		'ip_to',
		'country_code',
		'country_name',
		'region_name',
		'city_name',
		'latitude',
		'longitude',
		'zipcode',
		'id',
		'created',
		'modified',
		'state_abbreviation'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ip_locations';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'ip_from',
		'ip_to',
		'country_code',
		'country_name',
		'region_name',
		'city_name',
		'latitude',
		'longitude',
		'zipcode',
		'state_abbreviation'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        
    ];

}
