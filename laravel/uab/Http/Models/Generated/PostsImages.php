<?php
namespace Uab\Http\Models\Generated;

use Uab\Events\AfterCreate\AfterCreate;
use Uab\Events\AfterEdit\AfterEdit;
use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use stdClass;

/**
 * Class PostsImages
 *
 * @property number id
 * @property string created
 * @property string modified
 * @property number post_id
 * @property number image_id
 *
 * @package Uab\Http\Models\Generated
 */
class PostsImages extends BaseModel {
    
    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created' => null,
        'modified' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime',
		'post_id' => 'integer',
		'image_id' => 'integer'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'created',
		'modified',
		'post_id',
		'image_id'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'posts_images';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'post_id',
		'image_id'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'posts' => 'post_id',
		'images' => 'image_id'
    ];

	/**
	 * @return stdClass
	*/
	public function post() {
		return DB::table('posts')->where(
			[
				'id' => $this->getAttributeValue('post_id')
			]
		)->first();
	}

	/**
	 * @return stdClass
	*/
	public function image() {
		return DB::table('images')->where(
			[
				'id' => $this->getAttributeValue('image_id')
			]
		)->first();
	}

}
