<?php
namespace Uab\Http\Models\Generated;

use Uab\Http\Models\Base\BaseModel;
use DB;
use stdClass;

/**
 * Class OauthRefreshTokens
 *
 * @property string id
 * @property string access_token_id
 * @property boolean revoked
 * @property string expires_at
 *
 * @package Uab\Http\Models\Generated
 */
class OauthRefreshTokens extends BaseModel {

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'modified_at';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'expires_at' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'string',
		'access_token_id' => 'string',
		'revoked' => 'boolean',
		'expires_at' => 'datetime'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'access_token_id',
		'revoked',
		'expires_at'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'oauth_refresh_tokens';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'access_token_id',
		'revoked',
		'expires_at'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'expires_at'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'access_tokens' => 'access_token_id'
    ];

	/**
	 * @return stdClass
	*/
	public function accessToken() {
		return DB::table('access_tokens')->where(
			[
				'id' => $this->getAttributeValue('access_token_id')
			]
		)->first();
	}

}
