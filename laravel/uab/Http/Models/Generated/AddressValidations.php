<?php
namespace Uab\Http\Models\Generated;

use Uab\Http\Models\Base\BaseModel;

/**
 * Class AddressValidations
 *
 * @property number id
 * @property string created
 * @property string modified
 * @property string requested_location
 * @property string requested_address_line_one
 * @property string requested_address_line_two
 * @property string requested_city
 * @property string requested_county
 * @property string requested_place_id
 * @property string requested_state
 * @property string requested_state_abbreviation
 * @property string requested_zipcode
 * @property string requested_zipcode_nine
 * @property string requested_zipcode_suffix
 * @property string requested_latitude
 * @property string requested_longitude
 * @property string returned_location
 * @property string returned_city
 * @property string returned_address_line_one
 * @property string returned_address_line_two
 * @property string returned_latitude
 * @property string returned_longitude
 * @property string returned_street_number
 * @property string returned_place_id
 * @property string returned_county
 * @property string returned_state
 * @property string returned_state_abbreviation
 * @property string returned_zipcode
 * @property string returned_zipcode_nine
 * @property string returned_zipcode_suffix
 * @property string source
 *
 * @package Uab\Http\Models\Generated
 */
class AddressValidations extends BaseModel {

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';


    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created' => null,
        'modified' => null,
        'requested_location' => null,
        'requested_place_id' => null,
        'requested_state_abbreviation' => null,
        'requested_zipcode' => null,
        'requested_latitude' => null,
        'requested_longitude' => null,
        'returned_location' => null,
        'returned_city' => null,
        'returned_address_line_one' => null,
        'returned_address_line_two' => null,
        'returned_latitude' => null,
        'returned_longitude' => null,
        'returned_street_number' => null,
        'returned_place_id' => null,
        'returned_county' => null,
        'returned_state_abbreviation' => null,
        'returned_zipcode' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime',
		'requested_location' => 'string',
		'requested_address_line_one' => 'string',
		'requested_address_line_two' => 'string',
		'requested_city' => 'string',
		'requested_county' => 'string',
		'requested_place_id' => 'string',
		'requested_state' => 'string',
		'requested_state_abbreviation' => 'string',
		'requested_zipcode' => 'string',
		'requested_zipcode_nine' => 'string',
		'requested_zipcode_suffix' => 'string',
		'requested_latitude' => 'string',
		'requested_longitude' => 'string',
		'returned_location' => 'string',
		'returned_city' => 'string',
		'returned_address_line_one' => 'string',
		'returned_address_line_two' => 'string',
		'returned_latitude' => 'string',
		'returned_longitude' => 'string',
		'returned_street_number' => 'string',
		'returned_place_id' => 'string',
		'returned_county' => 'string',
		'returned_state' => 'string',
		'returned_state_abbreviation' => 'string',
		'returned_zipcode' => 'string',
		'returned_zipcode_nine' => 'string',
		'returned_zipcode_suffix' => 'string',
		'source' => 'string'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'created',
		'modified',
		'requested_location',
		'requested_address_line_one',
		'requested_address_line_two',
		'requested_city',
		'requested_county',
		'requested_place_id',
		'requested_state',
		'requested_state_abbreviation',
		'requested_zipcode',
		'requested_zipcode_nine',
		'requested_zipcode_suffix',
		'requested_latitude',
		'requested_longitude',
		'returned_location',
		'returned_city',
		'returned_address_line_one',
		'returned_address_line_two',
		'returned_latitude',
		'returned_longitude',
		'returned_street_number',
		'returned_place_id',
		'returned_county',
		'returned_state',
		'returned_state_abbreviation',
		'returned_zipcode',
		'returned_zipcode_nine',
		'returned_zipcode_suffix',
		'source'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'address_validations';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'requested_location',
		'requested_address_line_one',
		'requested_address_line_two',
		'requested_city',
		'requested_county',
		'requested_place_id',
		'requested_state',
		'requested_state_abbreviation',
		'requested_zipcode',
		'requested_zipcode_nine',
		'requested_zipcode_suffix',
		'requested_latitude',
		'requested_longitude',
		'returned_location',
		'returned_city',
		'returned_address_line_one',
		'returned_address_line_two',
		'returned_latitude',
		'returned_longitude',
		'returned_street_number',
		'returned_place_id',
		'returned_county',
		'returned_state',
		'returned_state_abbreviation',
		'returned_zipcode',
		'returned_zipcode_nine',
		'returned_zipcode_suffix',
		'source'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [

    ];
}
