<?php
namespace Uab\Http\Models\Generated;

use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use stdClass;

/**
 * Class Badges
 *
 * @property number id
 * @property string description
 * @property string title
 * @property number image_id
 * @property string created
 * @property string modified
 * @property number max_completions
 * @property number cash
 * @property number start_week
 * @property number end_week
 * @property boolean can_complete_any
 *
 * @package Uab\Http\Models\Generated
 */
class Badges extends BaseModel {

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';



    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'description' => '',
        'image_id' => null,
        'created' => null,
        'modified' => null,
        'max_completions' => 0,
        'cash' => 0,
        'start_week' => 0,
        'end_week' => null,
        'can_complete_any' => 0
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'description' => 'string',
		'title' => 'string',
		'image_id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime',
		'max_completions' => 'integer',
		'cash' => 'integer',
		'start_week' => 'integer',
		'end_week' => 'integer',
		'can_complete_any' => 'boolean'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'description',
		'title',
		'image_id',
		'created',
		'modified',
		'max_completions',
		'cash',
		'start_week',
		'end_week',
		'can_complete_any'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'badges';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'description',
		'title',
		'image_id',
		'max_completions',
		'cash',
		'start_week',
		'end_week',
		'can_complete_any'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'images' => 'image_id'
    ];

	/**
	 * @return \Uab\Http\Models\Images|null
	*/
	public function image() {
		return \Uab\Http\Models\Images::find($this->getAttributeValue('image_id'));
	}

}
