<?php
namespace Uab\Http\Models\Generated;

use Uab\Events\AfterCreate\AfterCreate;
use Uab\Events\AfterEdit\AfterEdit;
use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use stdClass;

/**
 * Class PostAttachments
 *
 * @property number id
 * @property string modified
 * @property string created
 * @property number post_id
 * @property string reference_key
 * @property number reference_value
 *
 * @package Uab\Http\Models\Generated
 */
class PostAttachments extends BaseModel {
    
    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'modified' => null,
        'created' => null,
        'reference_value' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'modified' => 'datetime',
		'created' => 'datetime',
		'post_id' => 'integer',
		'reference_key' => 'string',
		'reference_value' => 'integer'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'modified',
		'created',
		'post_id',
		'reference_key',
		'reference_value'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'post_attachments';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'post_id',
		'reference_key',
		'reference_value'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'modified',
		'created'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'posts' => 'post_id'
    ];

	/**
	 * @return stdClass
	*/
	public function post() {
		return DB::table('posts')->where(
			[
				'id' => $this->getAttributeValue('post_id')
			]
		)->first();
	}

}
