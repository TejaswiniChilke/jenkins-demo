<?php
namespace Uab\Http\Models\Generated;

use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TextMessageStops
 *
 * @property number id
 * @property string created_at
 * @property string modified_at
 * @property string deleted_at
 * @property string stop_message
 * @property string start_message
 * @property string from_number
 *
 * @package Uab\Http\Models\Generated
 */
class TextMessageStops extends BaseModel {
    use SoftDeletes;

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'modified_at';
    const DELETED_AT = 'deleted_at';
    

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created_at' => null,
        'modified_at' => null,
        'deleted_at' => null,
        'stop_message' => null,
        'start_message' => null,
        'from_number' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'created_at' => 'datetime',
		'modified_at' => 'datetime',
		'deleted_at' => 'datetime',
		'stop_message' => 'string',
		'start_message' => 'string',
		'from_number' => 'string'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'created_at',
		'modified_at',
		'deleted_at',
		'stop_message',
		'start_message',
		'from_number'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'text_message_stops';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'created_at',
		'modified_at',
		'deleted_at',
		'stop_message',
		'start_message',
		'from_number'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created_at',
		'modified_at',
		'deleted_at'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        
    ];

}
