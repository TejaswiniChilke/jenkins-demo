<?php
namespace Uab\Http\Models\Generated;

use Uab\Http\Models\Base\BaseModel;
use Uab\Http\Models\Users;

/**
 * Class LoginAttempts
 *
 * @property number id
 * @property string username
 * @property boolean success
 * @property string created
 * @property string modified
 * @property string attempted_at
 * @property number user_id
 * @property string ip_address
 * @property string robot_name
 * @property string languages
 * @property string browser
 * @property string platform
 * @property string device
 * @property boolean is_desktop
 * @property boolean is_mobile
 * @property boolean is_tablet
 * @property boolean is_phone
 *
 * @package Uab\Http\Models\Generated
 */
class LoginAttempts extends BaseModel {

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created' => null,
        'modified' => null,
        'attempted_at' => null,
        'user_id' => null,
        'ip_address' => null,
        'robot_name' => null,
        'languages' => null,
        'browser' => null,
        'platform' => null,
        'device' => null,
        'is_desktop' => 0,
        'is_mobile' => 0,
        'is_tablet' => 0,
        'is_phone' => 0
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'username' => 'string',
		'success' => 'boolean',
		'created' => 'datetime',
		'modified' => 'datetime',
		'attempted_at' => 'datetime',
		'user_id' => 'integer',
		'ip_address' => 'string',
		'robot_name' => 'string',
		'languages' => 'string',
		'browser' => 'string',
		'platform' => 'string',
		'device' => 'string',
		'is_desktop' => 'boolean',
		'is_mobile' => 'boolean',
		'is_tablet' => 'boolean',
		'is_phone' => 'boolean'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'username',
		'success',
		'created',
		'modified',
		'attempted_at',
		'user_id',
		'ip_address',
		'robot_name',
		'languages',
		'browser',
		'platform',
		'device',
		'is_desktop',
		'is_mobile',
		'is_tablet',
		'is_phone'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'login_attempts';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'username',
		'success',
		'attempted_at',
		'user_id',
		'ip_address',
		'robot_name',
		'languages',
		'browser',
		'platform',
		'device',
		'is_desktop',
		'is_mobile',
		'is_tablet',
		'is_phone'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified',
		'attempted_at'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'users' => 'user_id'
    ];

	/**
	 * @return Users|null
	*/
	public function user() {
		return Users::query()->where(
			[
				'id' => $this->getAttributeValue('user_id')
			]
		)->first();
	}

}
