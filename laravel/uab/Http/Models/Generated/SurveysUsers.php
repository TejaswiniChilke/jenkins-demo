<?php
namespace Uab\Http\Models\Generated;

use Uab\Events\AfterCreate\AfterCreate;
use Uab\Events\AfterEdit\AfterEdit;
use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use stdClass;

/**
 * Class SurveysUsers
 *
 * @property number id
 * @property string created
 * @property string modified
 * @property string last_resent
 * @property number survey_id
 * @property number user_id
 * @property number response_order
 * @property boolean is_locked
 * @property mixed deleted_at
 *
 * @package Uab\Http\Models\Generated
 */
class SurveysUsers extends BaseModel {
    use SoftDeletes;

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created' => null,
        'modified' => null,
        'last_resent' => null,
        'survey_id' => null,
        'user_id' => null,
        'response_order' => 0,
        'is_locked' => 0,
        'deleted_at' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime',
		'last_resent' => 'datetime',
		'survey_id' => 'integer',
		'user_id' => 'integer',
		'response_order' => 'integer',
		'is_locked' => 'boolean',
		'deleted_at' => 'timestamp'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'created',
		'modified',
		'last_resent',
		'survey_id',
		'user_id',
		'response_order',
		'is_locked',
		'deleted_at'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'surveys_users';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'last_resent',
		'survey_id',
		'user_id',
		'response_order',
		'is_locked',
		'deleted_at'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified',
		'last_resent',
		'deleted_at'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'surveys' => 'survey_id',
		'users' => 'user_id'
    ];

	/**
	 * @return \Uab\Http\Models\Surveys
	*/
	public function survey() {
		return \Uab\Http\Models\Surveys::query()->where(
			[
				'id' => $this->getAttributeValue('survey_id')
			]
		)->first();
	}

	/**
	 * @return stdClass
	*/
	public function user() {
		return DB::table('users')->where(
			[
				'id' => $this->getAttributeValue('user_id')
			]
		)->first();
	}

}
