<?php
namespace Uab\Http\Models\Generated;

use Uab\Events\AfterCreate\AfterCreate;
use Uab\Events\AfterEdit\AfterEdit;
use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use stdClass;

/**
 * Class Settings
 *
 * @property number id
 * @property string name
 * @property string display_name
 * @property string description
 * @property string default_value
 * @property string value_type
 * @property string setting_type
 * @property string created
 * @property string modified
 *
 * @package Uab\Http\Models\Generated
 */
class Settings extends BaseModel {
    
    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'description' => null,
        'created' => null,
        'modified' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'name' => 'string',
		'display_name' => 'string',
		'description' => 'string',
		'default_value' => 'string',
		'value_type' => 'string',
		'setting_type' => 'string',
		'created' => 'datetime',
		'modified' => 'datetime'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'name',
		'display_name',
		'description',
		'default_value',
		'value_type',
		'setting_type',
		'created',
		'modified'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'settings';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'name',
		'display_name',
		'description',
		'default_value',
		'value_type',
		'setting_type'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        
    ];

}
