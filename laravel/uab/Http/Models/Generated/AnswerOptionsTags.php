<?php
namespace Uab\Http\Models\Generated;

use Uab\Events\AfterCreate\AfterCreate;
use Uab\Events\AfterEdit\AfterEdit;
use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use stdClass;

/**
 * Class AnswerOptionsTags
 *
 * @property number id
 * @property number answer_option_id
 * @property number tag_id
 * @property string created
 * @property string modified
 * @property boolean exclusive
 *
 * @package Uab\Http\Models\Generated
 */
class AnswerOptionsTags extends BaseModel {

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created' => null,
        'modified' => null,
        'exclusive' => 0
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'answer_option_id' => 'integer',
		'tag_id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime',
		'exclusive' => 'boolean'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'answer_option_id',
		'tag_id',
		'created',
		'modified',
		'exclusive'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'answer_options_tags';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'answer_option_id',
		'tag_id',
		'exclusive'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'answer_options' => 'answer_option_id',
		'tags' => 'tag_id'
    ];

	/**
	 * @return stdClass
	*/
	public function answerOption() {
		return DB::table('answer_options')->where(
			[
				'id' => $this->getAttributeValue('answer_option_id')
			]
		)->first();
	}

	/**
	 * @return stdClass
	*/
	public function tag() {
		return DB::table('tags')->where(
			[
				'id' => $this->getAttributeValue('tag_id')
			]
		)->first();
	}

}
