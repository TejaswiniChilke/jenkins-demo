<?php
namespace Uab\Http\Models\Generated;

use Uab\Events\AfterCreate\AfterCreate;
use Uab\Events\AfterEdit\AfterEdit;
use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use stdClass;

/**
 * Class VerifiedPosts
 *
 * @property number id
 * @property string created
 * @property string modified
 * @property number reported_post_id
 * @property number user_id
 * @property string message
 *
 * @package Uab\Http\Models\Generated
 */
class VerifiedPosts extends BaseModel {
    
    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created' => null,
        'modified' => null,
        'message' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime',
		'reported_post_id' => 'integer',
		'user_id' => 'integer',
		'message' => 'string'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'created',
		'modified',
		'reported_post_id',
		'user_id',
		'message'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'verified_posts';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'reported_post_id',
		'user_id',
		'message'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'reported_posts' => 'reported_post_id',
		'users' => 'user_id'
    ];

	/**
	 * @return stdClass
	*/
	public function reportedPost() {
		return DB::table('reported_posts')->where(
			[
				'id' => $this->getAttributeValue('reported_post_id')
			]
		)->first();
	}

	/**
	 * @return stdClass
	*/
	public function user() {
		return DB::table('users')->where(
			[
				'id' => $this->getAttributeValue('user_id')
			]
		)->first();
	}

}
