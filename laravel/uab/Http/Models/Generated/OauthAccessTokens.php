<?php
namespace Uab\Http\Models\Generated;

use Illuminate\Database\Eloquent\Builder;
use Uab\Http\Models\Base\BaseModel;
use DB;
use stdClass;

/**
 * Class OauthAccessTokens
 *
 * @property string id
 * @property number user_id
 * @property number client_id
 * @property string name
 * @property string scopes
 * @property boolean revoked
 * @property mixed created_at
 * @property mixed updated_at
 * @property string expires_at
 *
 * @package Uab\Http\Models\Generated
 */
class OauthAccessTokens extends BaseModel {

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'user_id' => null,
        'name' => null,
        'scopes' => null,
        'created_at' => null,
        'updated_at' => null,
        'expires_at' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'string',
		'user_id' => 'integer',
		'client_id' => 'integer',
		'name' => 'string',
		'scopes' => 'string',
		'revoked' => 'boolean',
		'created_at' => 'timestamp',
		'updated_at' => 'timestamp',
		'expires_at' => 'datetime'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'user_id',
		'client_id',
		'name',
		'scopes',
		'revoked',
		'created_at',
		'updated_at',
		'expires_at'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'oauth_access_tokens';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'user_id',
		'client_id',
		'name',
		'scopes',
		'revoked',
		'created_at',
		'updated_at',
		'expires_at'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created_at',
		'updated_at',
		'expires_at'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'users' => 'user_id',
		'clients' => 'client_id'
    ];

	/**
	 * @return Builder|BaseModel|\Uab\Http\Models\Users
	*/
	public function user() {
		return Users::query()
            ->where(
                [
                    'id' => $this->getAttributeValue('user_id')
                ]
            )->first();
	}

	/**
	 * @return stdClass
	*/
	public function client() {
		return DB::table('clients')->where(
			[
				'id' => $this->getAttributeValue('client_id')
			]
		)->first();
	}

}
