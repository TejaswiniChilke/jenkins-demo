<?php
namespace Uab\Http\Models\Generated;

use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class AnalyticVideos
 *
 * @property number id
 * @property string created
 * @property string modified
 * @property string play_time
 * @property string stop_time
 * @property number user_id
 * @property number video_id
 * @property string watch_datetime
 * @property string deleted_at
 *
 * @package Uab\Http\Models\Generated
 */
class AnalyticVideos extends BaseModel {
    use SoftDeletes;

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created' => null,
        'modified' => null,
        'play_time' => null,
        'stop_time' => null,
        'watch_datetime' => null,
        'deleted_at' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime',
		'play_time' => 'string',
		'stop_time' => 'string',
		'user_id' => 'integer',
		'video_id' => 'integer',
		'watch_datetime' => 'datetime',
		'deleted_at' => 'datetime'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'created',
		'modified',
		'play_time',
		'stop_time',
		'user_id',
		'video_id',
		'watch_datetime',
		'deleted_at'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'analytic_videos';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'play_time',
		'stop_time',
		'user_id',
		'video_id',
		'watch_datetime',
		'deleted_at'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified',
		'watch_datetime',
		'deleted_at'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'users' => 'user_id',
		'videos' => 'video_id'
    ];

    /**
     * @return \Uab\Http\Models\Users|null
     */
    public function user() {
        return \Uab\Http\Models\Users::query()->where(
            [
                'id' => $this->getAttributeValue('user_id')
            ]
        )->first();
    }

	/**
	 * @return \Uab\Http\Models\Videos
	*/
	public function video() {
		return \Uab\Http\Models\Videos::query()->where(
            [
                'id' => $this->getAttributeValue('video_id')
            ]
        )->first();
	}

}
