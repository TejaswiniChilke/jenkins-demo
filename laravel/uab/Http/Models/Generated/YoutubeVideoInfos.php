<?php
namespace Uab\Http\Models\Generated;

use Uab\Http\Models\Base\BaseModel;
use DB;
use stdClass;

/**
 * Class YoutubeVideoInfos
 *
 * @property number id
 * @property string created
 * @property string modified
 * @property string youtube_id
 * @property string kind
 * @property string etag
 * @property string published_at
 * @property string channel_id
 * @property string title
 * @property string description
 * @property string thumbnail_default
 * @property string thumbnail_medium
 * @property string thumbnail_high
 * @property string thumbnail_standard
 * @property string thumbnail_max
 * @property string channel_title
 * @property string tags
 * @property string youtube_category_id
 * @property string live_broadcast_content
 * @property string localized_title
 * @property string localized_description
 * @property string duration
 * @property string dimension
 * @property string definition
 * @property string caption
 * @property string licensed_content
 * @property string projection
 * @property string upload_status
 * @property string privacy_status
 * @property string license
 * @property boolean embeddable
 * @property boolean public_stats_viewable
 * @property string view_count
 * @property string like_count
 * @property string dislike_count
 * @property string favorite_count
 * @property string comment_count
 * @property string embed_html
 * @property number video_id
 *
 * @package Uab\Http\Models\Generated
 */
class YoutubeVideoInfos extends BaseModel {

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created' => null,
        'modified' => null,
        'youtube_id' => null,
        'kind' => null,
        'etag' => null,
        'published_at' => null,
        'channel_id' => null,
        'title' => null,
        'description' => null,
        'thumbnail_default' => null,
        'thumbnail_medium' => null,
        'thumbnail_high' => null,
        'thumbnail_standard' => null,
        'thumbnail_max' => null,
        'channel_title' => null,
        'tags' => null,
        'youtube_category_id' => null,
        'live_broadcast_content' => null,
        'localized_title' => null,
        'localized_description' => null,
        'duration' => null,
        'dimension' => null,
        'definition' => null,
        'caption' => null,
        'licensed_content' => null,
        'projection' => null,
        'upload_status' => null,
        'privacy_status' => null,
        'license' => null,
        'embeddable' => 0,
        'public_stats_viewable' => 0,
        'view_count' => null,
        'like_count' => null,
        'dislike_count' => null,
        'favorite_count' => null,
        'comment_count' => null,
        'embed_html' => null,
        'video_id' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime',
		'youtube_id' => 'string',
		'kind' => 'string',
		'etag' => 'string',
		'published_at' => 'string',
		'channel_id' => 'string',
		'title' => 'string',
		'description' => 'string',
		'thumbnail_default' => 'string',
		'thumbnail_medium' => 'string',
		'thumbnail_high' => 'string',
		'thumbnail_standard' => 'string',
		'thumbnail_max' => 'string',
		'channel_title' => 'string',
		'tags' => 'string',
		'youtube_category_id' => 'string',
		'live_broadcast_content' => 'string',
		'localized_title' => 'string',
		'localized_description' => 'string',
		'duration' => 'string',
		'dimension' => 'string',
		'definition' => 'string',
		'caption' => 'string',
		'licensed_content' => 'string',
		'projection' => 'string',
		'upload_status' => 'string',
		'privacy_status' => 'string',
		'license' => 'string',
		'embeddable' => 'boolean',
		'public_stats_viewable' => 'boolean',
		'view_count' => 'string',
		'like_count' => 'string',
		'dislike_count' => 'string',
		'favorite_count' => 'string',
		'comment_count' => 'string',
		'embed_html' => 'string',
		'video_id' => 'integer'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'created',
		'modified',
		'youtube_id',
		'kind',
		'etag',
		'published_at',
		'channel_id',
		'title',
		'description',
		'thumbnail_default',
		'thumbnail_medium',
		'thumbnail_high',
		'thumbnail_standard',
		'thumbnail_max',
		'channel_title',
		'tags',
		'youtube_category_id',
		'live_broadcast_content',
		'localized_title',
		'localized_description',
		'duration',
		'dimension',
		'definition',
		'caption',
		'licensed_content',
		'projection',
		'upload_status',
		'privacy_status',
		'license',
		'embeddable',
		'public_stats_viewable',
		'view_count',
		'like_count',
		'dislike_count',
		'favorite_count',
		'comment_count',
		'embed_html',
		'video_id'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'youtube_video_infos';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'youtube_id',
		'kind',
		'etag',
		'published_at',
		'channel_id',
		'title',
		'description',
		'thumbnail_default',
		'thumbnail_medium',
		'thumbnail_high',
		'thumbnail_standard',
		'thumbnail_max',
		'channel_title',
		'tags',
		'youtube_category_id',
		'live_broadcast_content',
		'localized_title',
		'localized_description',
		'duration',
		'dimension',
		'definition',
		'caption',
		'licensed_content',
		'projection',
		'upload_status',
		'privacy_status',
		'license',
		'embeddable',
		'public_stats_viewable',
		'view_count',
		'like_count',
		'dislike_count',
		'favorite_count',
		'comment_count',
		'embed_html',
		'video_id'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
		'videos' => 'video_id'
    ];

	/**
	 * @return stdClass
	*/
	public function video() {
		return DB::table('videos')->where(
			[
				'id' => $this->getAttributeValue('video_id')
			]
		)->first();
	}

}
