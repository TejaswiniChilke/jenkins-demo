<?php
namespace Uab\Http\Models\Generated;

use Uab\Http\Models\Base\BaseModel;

/**
 * Class WebhookRequests
 *
 * @property number id
 * @property string created
 * @property string modified
 * @property string webhook
 * @property string response
 * @property string response_code
 * @property string request_data
 * @property string url
 * @property string domain
 * @property string response_headers
 * @property string response_protocol
 * @property string response_reason
 * @property string request_headers
 *
 * @package Uab\Http\Models\Generated
 */
class WebhookRequests extends BaseModel {

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';


    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created' => null,
        'modified' => null,
        'webhook' => null,
        'response' => null,
        'response_code' => null,
        'request_data' => null,
        'url' => null,
        'domain' => null,
        'response_headers' => null,
        'response_protocol' => null,
        'response_reason' => null,
        'request_headers' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime',
		'webhook' => 'string',
		'response' => 'string',
		'response_code' => 'string',
		'request_data' => 'string',
		'url' => 'string',
		'domain' => 'string',
		'response_headers' => 'string',
		'response_protocol' => 'string',
		'response_reason' => 'string',
		'request_headers' => 'string'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'created',
		'modified',
		'webhook',
		'response',
		'response_code',
		'request_data',
		'url',
		'domain',
		'response_headers',
		'response_protocol',
		'response_reason',
		'request_headers'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'webhook_requests';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'webhook',
		'response',
		'response_code',
		'request_data',
		'url',
		'domain',
		'response_headers',
		'response_protocol',
		'response_reason',
		'request_headers'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [

    ];

}
