<?php
namespace Uab\Http\Models\Generated;

use Uab\Events\AfterCreate\AfterCreate;
use Uab\Events\AfterEdit\AfterEdit;
use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use stdClass;

/**
 * Class EmailTemplates
 *
 * @property number id
 * @property string created
 * @property string modified
 * @property number week
 * @property string message
 * @property string subject
 * @property number user_type_id
 *
 * @package Uab\Http\Models\Generated
 */
class EmailTemplates extends BaseModel {

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created' => null,
        'modified' => null,
        'week' => null,
        'message' => null,
        'subject' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime',
		'week' => 'integer',
		'message' => 'string',
		'subject' => 'string',
		'user_type_id' => 'integer'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'created',
		'modified',
		'week',
		'message',
		'subject',
		'user_type_id'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'email_templates';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'week',
		'message',
		'subject',
		'user_type_id'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'user_types' => 'user_type_id'
    ];

	/**
	 * @return stdClass
	*/
	public function userType() {
		return DB::table('user_types')->where(
			[
				'id' => $this->getAttributeValue('user_type_id')
			]
		)->first();
	}

}
