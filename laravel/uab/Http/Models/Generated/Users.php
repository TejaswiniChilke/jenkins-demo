<?php
namespace Uab\Http\Models\Generated;

use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Uab\Http\Models\Images;
use Uab\Http\Models\UserTypes;
use Uab\Http\Models\States;

/**
 * Class Users
 *
 * @property number id
 * @property string email
 * @property string username
 * @property string nickname
 * @property string password
 * @property string first_name
 * @property string last_name
 * @property number image_id
 * @property number user_type_id
 * @property number group_id
 * @property number login_count
 * @property number logout_count
 * @property string security_question
 * @property string security_answer
 * @property boolean gender
 * @property string mobile_number
 * @property string start_time
 * @property boolean wheelchair
 * @property string preferred_time
 * @property string home_number
 * @property string a1c
 * @property string type_diabetes
 * @property string years_since_diagnosis
 * @property string remember_token
 * @property string email_verified_at
 * @property boolean requires_approval
 * @property string address_line_one
 * @property string address_line_two
 * @property number state_id
 * @property string city
 * @property string zipcode
 * @property string approved_on
 * @property string declined_on
 * @property string activation_email_sent_date
 * @property number speed_check
 * @property string browser_check
 * @property string email_confirmed_on
 * @property mixed dob
 * @property string preferred_contact_time
 * @property string preferred_contact_type
 * @property string created
 * @property string modified
 * @property mixed deleted_at
 * @property number coach_id
 * @property boolean has_sci
 * @property string status
 * @property string profile_bio
 * @property string profile_city
 * @property string profile_state
 * @property string requires_medical_clearance
 * @property string preferred_session_times
 * @property number spb_score
 * @property string last_called
 * @property string preferred_session_days
 * @property number pincode
 * @property boolean is_in_age_range
 * @property string state_text
 *
 * @package Uab\Http\Models\Generated
 */
class Users extends BaseModel {
    use SoftDeletes;

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';

	protected $hidden = [
		'password',
		'remember_token'
	];

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'email' => null,
        'username' => null,
        'nickname' => null,
        'password' => null,
        'first_name' => null,
        'last_name' => null,
        'image_id' => null,
        'user_type_id' => null,
        'group_id' => null,
        'login_count' => 0,
        'logout_count' => 0,
        'security_question' => null,
        'security_answer' => null,
        'gender' => null,
        'mobile_number' => null,
        'start_time' => null,
        'wheelchair' => 0,
        'preferred_time' => null,
        'home_number' => null,
        'a1c' => null,
        'type_diabetes' => null,
        'years_since_diagnosis' => null,
        'remember_token' => null,
        'email_verified_at' => null,
        'requires_approval' => 0,
        'address_line_one' => '',
        'address_line_two' => '',
        'state_id' => null,
        'city' => '',
        'zipcode' => null,
        'approved_on' => null,
        'declined_on' => null,
        'activation_email_sent_date' => null,
        'speed_check' => null,
        'browser_check' => null,
        'email_confirmed_on' => null,
        'dob' => null,
        'preferred_contact_time' => null,
        'preferred_contact_type' => null,
        'created' => null,
        'modified' => null,
        'deleted_at' => null,
        'coach_id' => null,
        'has_sci' => null,
        'status' => '',
        'profile_bio' => null,
        'profile_city' => '',
        'profile_state' => '',
        'requires_medical_clearance' => 0,
        'preferred_session_times' => null,
        'spb_score' => 0,
        'last_called' => null,
        'preferred_session_days' => '',
        'pincode' => null,
        'is_in_age_range' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'email' => 'string',
		'username' => 'string',
		'nickname' => 'string',
		'password' => 'string',
		'first_name' => 'string',
		'last_name' => 'string',
		'image_id' => 'integer',
		'user_type_id' => 'integer',
		'group_id' => 'integer',
		'login_count' => 'integer',
		'logout_count' => 'integer',
		'security_question' => 'string',
		'security_answer' => 'string',
		'gender' => 'boolean',
		'mobile_number' => 'string',
		'start_time' => 'datetime',
		'wheelchair' => 'boolean',
		'preferred_time' => 'datetime',
		'home_number' => 'string',
		'a1c' => 'string',
		'type_diabetes' => 'string',
		'years_since_diagnosis' => 'string',
		'remember_token' => 'string',
		'email_verified_at' => 'datetime',
		'requires_approval' => 'boolean',
		'address_line_one' => 'string',
		'address_line_two' => 'string',
		'state_id' => 'integer',
		'city' => 'string',
		'zipcode' => 'string',
		'approved_on' => 'datetime',
		'declined_on' => 'datetime',
		'activation_email_sent_date' => 'datetime',
		'speed_check' => 'integer',
		'browser_check' => 'string',
		'email_confirmed_on' => 'datetime',
		'dob' => 'date',
		'preferred_contact_time' => 'string',
		'preferred_contact_type' => 'string',
		'created' => 'datetime',
		'modified' => 'datetime',
		'deleted_at' => 'timestamp',
		'coach_id' => 'integer',
		'has_sci' => 'boolean',
		'status' => 'string',
		'profile_bio' => 'string',
		'profile_city' => 'string',
		'profile_state' => 'string',
		'requires_medical_clearance' => 'string',
		'preferred_session_times' => 'string',
		'spb_score' => 'integer',
		'last_called' => 'string',
		'preferred_session_days' => 'string',
        'pincode' => 'integer',
        'is_in_age_range' => 'boolean',
		'state_text' => 'string'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'email',
		'username',
		'nickname',
		'password',
		'first_name',
		'last_name',
		'image_id',
		'user_type_id',
		'group_id',
		'login_count',
		'logout_count',
		'security_question',
		'security_answer',
		'gender',
		'mobile_number',
		'start_time',
		'wheelchair',
		'preferred_time',
		'home_number',
		'a1c',
		'type_diabetes',
		'years_since_diagnosis',
		'remember_token',
		'email_verified_at',
		'requires_approval',
		'address_line_one',
		'address_line_two',
		'state_id',
		'city',
		'zipcode',
		'approved_on',
		'declined_on',
		'activation_email_sent_date',
		'speed_check',
		'browser_check',
		'email_confirmed_on',
		'dob',
		'preferred_contact_time',
		'preferred_contact_type',
		'created',
		'modified',
		'deleted_at',
		'coach_id',
		'has_sci',
		'status',
		'profile_bio',
		'profile_city',
		'profile_state',
		'requires_medical_clearance',
		'preferred_session_times',
		'spb_score',
		'last_called',
		'preferred_session_days',
		'pincode',
        'is_in_age_range',
        'state_text'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'email',
		'username',
		'nickname',
		'password',
		'first_name',
		'last_name',
		'image_id',
		'user_type_id',
		'group_id',
		'login_count',
		'logout_count',
		'security_question',
		'security_answer',
		'gender',
		'mobile_number',
		'start_time',
		'wheelchair',
		'preferred_time',
		'home_number',
		'a1c',
		'type_diabetes',
		'years_since_diagnosis',
		'remember_token',
		'email_verified_at',
		'requires_approval',
		'address_line_one',
		'address_line_two',
		'state_id',
		'city',
		'zipcode',
		'approved_on',
		'declined_on',
		'activation_email_sent_date',
		'speed_check',
		'browser_check',
		'email_confirmed_on',
		'dob',
		'preferred_contact_time',
		'preferred_contact_type',
		'deleted_at',
		'coach_id',
		'has_sci',
		'status',
		'profile_bio',
		'profile_city',
		'profile_state',
		'requires_medical_clearance',
		'preferred_session_times',
		'spb_score',
		'last_called',
		'preferred_session_days',
		'pincode',
        'is_in_age_range',
		'state_text'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'start_time',
		'preferred_time',
		'email_verified_at',
		'approved_on',
		'declined_on',
		'activation_email_sent_date',
		'email_confirmed_on',
		'dob',
		'created',
		'modified',
		'deleted_at'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'images' => 'image_id',
		'user_types' => 'user_type_id',
		'states' => 'state_id'
    ];

	/**
	 * @return Images|null
	*/
	public function image() {
		$id = $this->getAttributeValue('image_id');

		if (is_null($id)) {
			return null;
		}

		return Images::query()->find($id);
	}

	/**
	 * @return UserTypes|null
	*/
	public function userType() {
		$id = $this->getAttributeValue('user_type_id');

		if (is_null($id)) {
			return null;
		}

		return UserTypes::query()->find($id);
	}

	/**
	 * @return States|null
	*/
	public function state() {
		$id = $this->getAttributeValue('state_id');

		if (is_null($id)) {
			return null;
		}

		return States::query()->find($id);
	}

}
