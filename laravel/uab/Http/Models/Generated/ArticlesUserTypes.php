<?php
namespace Uab\Http\Models\Generated;

use Uab\Events\AfterCreate\AfterCreate;
use Uab\Events\AfterEdit\AfterEdit;
use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use stdClass;

/**
 * Class ArticlesUserTypes
 *
 * @property number id
 * @property string modified
 * @property string created
 * @property number article_id
 * @property number user_type_id
 * @property number week
 *
 * @package Uab\Http\Models\Generated
 */
class ArticlesUserTypes extends BaseModel {
    
    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'modified' => null,
        'created' => null,
        'week' => 0
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'modified' => 'datetime',
		'created' => 'datetime',
		'article_id' => 'integer',
		'user_type_id' => 'integer',
		'week' => 'integer'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'modified',
		'created',
		'article_id',
		'user_type_id',
		'week'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'articles_user_types';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'article_id',
		'user_type_id',
		'week'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'modified',
		'created'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'articles' => 'article_id',
		'user_types' => 'user_type_id'
    ];

	/**
	 * @return stdClass
	*/
	public function article() {
		return DB::table('articles')->where(
			[
				'id' => $this->getAttributeValue('article_id')
			]
		)->first();
	}

	/**
	 * @return stdClass
	*/
	public function userType() {
		return DB::table('user_types')->where(
			[
				'id' => $this->getAttributeValue('user_type_id')
			]
		)->first();
	}

}
