<?php
namespace Uab\Http\Models\Generated;

use Uab\Events\AfterCreate\AfterCreate;
use Uab\Events\AfterEdit\AfterEdit;
use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use stdClass;

/**
 * Class TelescopeEntries
 *
 * @property mixed sequence
 * @property mixed uuid
 * @property mixed batch_id
 * @property string family_hash
 * @property boolean should_display_on_index
 * @property string type
 * @property mixed content
 * @property string created_at
 *
 * @package Uab\Http\Models\Generated
 */
class TelescopeEntries extends BaseModel {

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'modified_at';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'family_hash' => null,
        'should_display_on_index' => 1,
        'created_at' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'sequence' => 'string',
		'uuid' => 'string',
		'batch_id' => 'string',
		'family_hash' => 'string',
		'should_display_on_index' => 'boolean',
		'type' => 'string',
		'content' => 'string',
		'created_at' => 'datetime'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'sequence',
		'uuid',
		'batch_id',
		'family_hash',
		'should_display_on_index',
		'type',
		'content',
		'created_at'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'telescope_entries';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'sequence',
		'uuid',
		'batch_id',
		'family_hash',
		'should_display_on_index',
		'type',
		'content',
		'created_at'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created_at'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'batches' => 'batch_id'
    ];

	/**
	 * @return stdClass
	*/
	public function batch() {
		return DB::table('batches')->where(
			[
				'id' => $this->getAttributeValue('batch_id')
			]
		)->first();
	}

}
