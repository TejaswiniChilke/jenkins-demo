<?php
namespace Uab\Http\Models\Generated;

use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Uab\Http\Models\Users;

/**
 * Class Emails
 *
 * @property number id
 * @property number to_user_id
 * @property number from_user_id
 * @property string message
 * @property string created
 * @property string modified
 * @property string from_email
 * @property string to_email
 * @property string sent
 * @property string subject
 * @property string headers
 * @property string deleted_at
 * @property string read_at
 *
 * @package Uab\Http\Models\Generated
 */
class Emails extends BaseModel {
    use SoftDeletes;

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';


    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'to_user_id' => null,
        'from_user_id' => null,
        'message' => null,
        'created' => null,
        'modified' => null,
        'from_email' => null,
        'to_email' => null,
        'sent' => null,
        'subject' => null,
        'headers' => null,
        'deleted_at' => null,
        'read_at' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'to_user_id' => 'integer',
		'from_user_id' => 'integer',
		'message' => 'string',
		'created' => 'datetime',
		'modified' => 'datetime',
		'from_email' => 'string',
		'to_email' => 'string',
		'sent' => 'datetime',
		'subject' => 'string',
		'headers' => 'string',
		'deleted_at' => 'datetime',
		'read_at' => 'datetime'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'to_user_id',
		'from_user_id',
		'message',
		'created',
		'modified',
		'from_email',
		'to_email',
		'sent',
		'subject',
		'headers',
		'deleted_at',
		'read_at'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'emails';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'to_user_id',
		'from_user_id',
		'message',
		'from_email',
		'to_email',
		'sent',
		'subject',
		'headers',
		'deleted_at',
		'read_at'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified',
		'sent',
		'deleted_at',
		'read_at'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'to_users' => 'to_user_id',
		'from_users' => 'from_user_id'
    ];

	/**
	 * @return Users|Model|null
	*/
	public function toUser() {
		$id = $this->getAttributeValue('to_user_id');

		if (is_null($id)) {
			return null;
		}

		return Users::query()->find($id);
	}

	/**
	 * @return Users|Model|null
	*/
	public function fromUser() {
		$id = $this->getAttributeValue('from_user_id');

		if (is_null($id)) {
			return null;
		}

		return Users::query()->find($id);
	}

}
