<?php
namespace Uab\Http\Models\Generated;

use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Uab\Http\Models\Badges;
use Uab\Http\Models\Users;

/**
 * Class BadgesUsers
 *
 * @property number id
 * @property string created
 * @property string modified
 * @property number badge_id
 * @property number user_id
 * @property boolean notified
 * @property string deleted_at
 * @property boolean has_read
 * @property string earned_at
 *
 * @package Uab\Http\Models\Generated
 */
class BadgesUsers extends BaseModel {
    use SoftDeletes;

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';


    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created' => null,
        'modified' => null,
        'notified' => 0,
        'deleted_at' => null,
        'has_read' => 0,
        'earned_at' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime',
		'badge_id' => 'integer',
		'user_id' => 'integer',
		'notified' => 'boolean',
		'deleted_at' => 'datetime',
		'has_read' => 'boolean',
		'earned_at' => 'datetime'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'created',
		'modified',
		'badge_id',
		'user_id',
		'notified',
		'deleted_at',
		'has_read',
		'earned_at'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'badges_users';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'badge_id',
		'user_id',
		'notified',
		'deleted_at',
		'has_read',
		'earned_at'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified',
		'deleted_at',
		'earned_at'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'badges' => 'badge_id',
		'users' => 'user_id'
    ];

	/**
	 * @return Badges|null
	*/
	public function badge() {
		$id = $this->getAttributeValue('badge_id');

		if (is_null($id)) {
			return null;
		}

		return Badges::query()->find($id);
	}

	/**
	 * @return Users|null
	*/
	public function user() {
		$id = $this->getAttributeValue('user_id');

		if (is_null($id)) {
			return null;
		}

		return Users::query()->find($id);
	}

}
