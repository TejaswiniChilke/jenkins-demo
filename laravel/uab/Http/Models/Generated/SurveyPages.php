<?php
namespace Uab\Http\Models\Generated;

use Illuminate\Support\Collection;
use Uab\Http\Models\Base\BaseModel;
use DB;
use stdClass;

/**
 * Class SurveyPages
 *
 * @property number id
 * @property string created
 * @property string modified
 * @property number survey_id
 * @property string name
 * @property number position
 * @property string description
 *
 * @package Uab\Http\Models\Generated
 */
class SurveyPages extends BaseModel {

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';



    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created' => null,
        'modified' => null,
        'name' => '',
        'position' => 0,
        'description' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime',
		'survey_id' => 'integer',
		'name' => 'string',
		'position' => 'integer',
		'description' => 'string'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'created',
		'modified',
		'survey_id',
		'name',
		'position',
		'description'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'survey_pages';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'survey_id',
		'name',
		'position',
		'description'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'surveys' => 'survey_id',
        'questions' => null
    ];

	/**
	 * @return stdClass
	*/
	public function survey() {
		return \Uab\Http\Models\Surveys::query()->where(
			[
				'id' => $this->getAttributeValue('survey_id')
			]
		)->first();
	}

	/**
	 * @return Collection<\Uab\Http\Models\Questions>
	*/
	public function questions() {
		return \Uab\Http\Models\Questions::query()->where(
			[
				'id' => $this->getAttributeValue('survey_id')
			]
		)->get();
	}

}
