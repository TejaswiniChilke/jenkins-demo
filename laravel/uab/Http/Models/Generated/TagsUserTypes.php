<?php
namespace Uab\Http\Models\Generated;

use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use stdClass;

/**
 * Class TagsUserTypes
 *
 * @property number id
 * @property string created
 * @property string modified
 * @property number tag_id
 * @property number user_type_id
 * @property boolean exclusive
 *
 * @package Uab\Http\Models\Generated
 */
class TagsUserTypes extends BaseModel {
    
    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';
    


    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created' => null,
        'modified' => null,
        'exclusive' => 0
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime',
		'tag_id' => 'integer',
		'user_type_id' => 'integer',
		'exclusive' => 'boolean'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'created',
		'modified',
		'tag_id',
		'user_type_id',
		'exclusive'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tags_user_types';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'tag_id',
		'user_type_id',
		'exclusive'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'tags' => 'tag_id',
		'user_types' => 'user_type_id'
    ];

	/**
	 * @return stdClass
	*/
	public function tag() {
		return DB::table('tags')->where(
			[
				'id' => $this->getAttributeValue('tag_id')
			]
		)->first();
	}

	/**
	 * @return stdClass
	*/
	public function userType() {
		return DB::table('user_types')->where(
			[
				'id' => $this->getAttributeValue('user_type_id')
			]
		)->first();
	}

}
