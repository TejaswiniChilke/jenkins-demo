<?php
namespace Uab\Http\Models\Generated;

use Uab\Events\AfterCreate\AfterCreate;
use Uab\Events\AfterEdit\AfterEdit;
use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use stdClass;

/**
 * Class Sections
 *
 * @property number id
 * @property string created
 * @property string modified
 * @property string title
 * @property number article_id
 * @property number video_id
 *
 * @package Uab\Http\Models\Generated
 */
class Sections extends BaseModel {
    
    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created' => null,
        'modified' => null,
        'title' => '',
        'article_id' => null,
        'video_id' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime',
		'title' => 'string',
		'article_id' => 'integer',
		'video_id' => 'integer'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'created',
		'modified',
		'title',
		'article_id',
		'video_id'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sections';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'title',
		'article_id',
		'video_id'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'articles' => 'article_id',
		'videos' => 'video_id'
    ];

	/**
	 * @return stdClass
	*/
	public function article() {
		return DB::table('articles')->where(
			[
				'id' => $this->getAttributeValue('article_id')
			]
		)->first();
	}

	/**
	 * @return stdClass
	*/
	public function video() {
		return DB::table('videos')->where(
			[
				'id' => $this->getAttributeValue('video_id')
			]
		)->first();
	}

}
