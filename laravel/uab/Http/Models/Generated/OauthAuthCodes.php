<?php
namespace Uab\Http\Models\Generated;

use Uab\Http\Models\Base\BaseModel;
use DB;
use stdClass;

/**
 * Class OauthAuthCodes
 *
 * @property string id
 * @property number user_id
 * @property number client_id
 * @property string scopes
 * @property boolean revoked
 * @property string expires_at
 *
 * @package Uab\Http\Models\Generated
 */
class OauthAuthCodes extends BaseModel {

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'modified_at';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'scopes' => null,
        'expires_at' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'string',
		'user_id' => 'integer',
		'client_id' => 'integer',
		'scopes' => 'string',
		'revoked' => 'boolean',
		'expires_at' => 'datetime'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'user_id',
		'client_id',
		'scopes',
		'revoked',
		'expires_at'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'oauth_auth_codes';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'user_id',
		'client_id',
		'scopes',
		'revoked',
		'expires_at'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'expires_at'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'users' => 'user_id',
		'clients' => 'client_id'
    ];

	/**
	 * @return stdClass
	*/
	public function user() {
		return DB::table('users')->where(
			[
				'id' => $this->getAttributeValue('user_id')
			]
		)->first();
	}

	/**
	 * @return stdClass
	*/
	public function client() {
		return DB::table('clients')->where(
			[
				'id' => $this->getAttributeValue('client_id')
			]
		)->first();
	}

}
