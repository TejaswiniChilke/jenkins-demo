<?php
namespace Uab\Http\Models\Generated;

use Uab\Events\AfterCreate\AfterCreate;
use Uab\Events\AfterEdit\AfterEdit;
use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use stdClass;

/**
 * Class Followers
 *
 * @property number id
 * @property string created
 * @property string modified
 * @property number user_id
 * @property number following_id
 * @property string deleted_at
 *
 * @package Uab\Http\Models\Generated
 */
class Followers extends BaseModel {
    use SoftDeletes;

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created' => null,
        'modified' => null,
        'deleted_at' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime',
		'user_id' => 'integer',
		'following_id' => 'integer',
		'deleted_at' => 'datetime'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'created',
		'modified',
		'user_id',
		'following_id',
		'deleted_at'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'followers';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'user_id',
		'following_id',
		'deleted_at'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified',
		'deleted_at'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'users' => 'user_id',
		'followings' => 'following_id'
    ];

	/**
	 * @return \Uab\Http\Models\Users
	*/
	public function user() {
		return \Uab\Http\Models\Users::query()
            ->where(
                [
                    'id' => $this->getAttributeValue('user_id')
                ]
            )->first();
	}

	/**
	 * @return stdClass
	*/
	public function following() {
		return DB::table('followings')->where(
			[
				'id' => $this->getAttributeValue('following_id')
			]
		)->first();
	}

}
