<?php
namespace Uab\Http\Models\Generated;

use Uab\Events\AfterCreate\AfterCreate;
use Uab\Events\AfterEdit\AfterEdit;
use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use stdClass;

/**
 * Class Notes
 *
 * @property number id
 * @property string created
 * @property string modified
 * @property number note_category_id
 * @property number user_id
 * @property number participant_id
 * @property string note
 * @property string title
 *
 * @package Uab\Http\Models\Generated
 */
class Notes extends BaseModel {
    
    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created' => null,
        'modified' => null,
        'note_category_id' => null,
        'participant_id' => null,
        'note' => null,
        'title' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime',
		'note_category_id' => 'integer',
		'user_id' => 'integer',
		'participant_id' => 'integer',
		'note' => 'string',
		'title' => 'string'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'created',
		'modified',
		'note_category_id',
		'user_id',
		'participant_id',
		'note',
		'title'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'notes';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'note_category_id',
		'user_id',
		'participant_id',
		'note',
		'title'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'note_categories' => 'note_category_id',
		'users' => 'user_id',
		'participants' => 'participant_id'
    ];

	/**
	 * @return stdClass
	*/
	public function noteCategory() {
		return DB::table('note_categories')->where(
			[
				'id' => $this->getAttributeValue('note_category_id')
			]
		)->first();
	}

	/**
	 * @return stdClass
	*/
	public function user() {
		return DB::table('users')->where(
			[
				'id' => $this->getAttributeValue('user_id')
			]
		)->first();
	}

	/**
	 * @return stdClass
	*/
	public function participant() {
		return DB::table('participants')->where(
			[
				'id' => $this->getAttributeValue('participant_id')
			]
		)->first();
	}

}
