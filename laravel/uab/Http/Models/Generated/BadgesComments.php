<?php
namespace Uab\Http\Models\Generated;

use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use stdClass;

/**
 * Class BadgesComments
 *
 * @property number id
 * @property string created
 * @property string modified
 * @property number comment_id
 * @property number badge_id
 * @property string deleted_at
 *
 * @package Uab\Http\Models\Generated
 */
class BadgesComments extends BaseModel {
    use SoftDeletes;

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';
    


    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created' => null,
        'modified' => null,
        'deleted_at' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime',
		'comment_id' => 'integer',
		'badge_id' => 'integer',
		'deleted_at' => 'datetime'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'created',
		'modified',
		'comment_id',
		'badge_id',
		'deleted_at'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'badges_comments';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'comment_id',
		'badge_id',
		'deleted_at'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified',
		'deleted_at'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'comments' => 'comment_id',
		'badges' => 'badge_id'
    ];

	/**
	 * @return stdClass
	*/
	public function comment() {
		return DB::table('comments')->where(
			[
				'id' => $this->getAttributeValue('comment_id')
			]
		)->first();
	}

	/**
	 * @return stdClass
	*/
	public function badge() {
		return DB::table('badges')->where(
			[
				'id' => $this->getAttributeValue('badge_id')
			]
		)->first();
	}

}
