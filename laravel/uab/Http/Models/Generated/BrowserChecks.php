<?php
namespace Uab\Http\Models\Generated;

use Uab\Http\Models\Base\BaseModel;

use Uab\Http\Models\Users;

/**
 * Class BrowserChecks
 *
 * @property number id
 * @property string created
 * @property string modified
 * @property number user_id
 * @property string browser
 * @property boolean is_supported
 * @property string checked_at
 * @property string ip_address
 *
 * @package Uab\Http\Models\Generated
 */
class BrowserChecks extends BaseModel {
    
    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';
    

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created' => null,
        'modified' => null,
        'user_id' => null,
        'browser' => null,
        'is_supported' => 1,
        'checked_at' => null,
        'ip_address' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime',
		'user_id' => 'integer',
		'browser' => 'string',
		'is_supported' => 'boolean',
		'checked_at' => 'datetime',
		'ip_address' => 'string'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'created',
		'modified',
		'user_id',
		'browser',
		'is_supported',
		'checked_at',
		'ip_address'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'browser_checks';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'user_id',
		'browser',
		'is_supported',
		'checked_at',
		'ip_address'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified',
		'checked_at'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'users' => 'user_id'
    ];

	/**
	 * @return Users|null
	*/
	public function user() {
		$id = $this->getAttributeValue('user_id');
		
		if (is_null($id)) {
			return null;
		}

		return Users::find($id);
	}

}
