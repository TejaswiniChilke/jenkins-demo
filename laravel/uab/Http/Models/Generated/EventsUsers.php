<?php
namespace Uab\Http\Models\Generated;

use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use stdClass;

/**
 * Class EventsUsers
 *
 * @property number id
 * @property number user_id
 * @property number event_id
 * @property string created
 * @property string modified
 * @property boolean is_complete
 * @property string deleted_at
 *
 * @package Uab\Http\Models\Generated
 */
class EventsUsers extends BaseModel {
    use SoftDeletes;

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';
    


    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created' => null,
        'modified' => null,
        'is_complete' => 0,
        'deleted_at' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'user_id' => 'integer',
		'event_id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime',
		'is_complete' => 'boolean',
		'deleted_at' => 'datetime'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'user_id',
		'event_id',
		'created',
		'modified',
		'is_complete',
		'deleted_at'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'events_users';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'user_id',
		'event_id',
		'is_complete',
		'deleted_at'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified',
		'deleted_at'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'users' => 'user_id',
		'events' => 'event_id'
    ];

	/**
	 * @return stdClass
	*/
	public function user() {
		return DB::table('users')->where(
			[
				'id' => $this->getAttributeValue('user_id')
			]
		)->first();
	}

	/**
	 * @return stdClass
	*/
	public function event() {
		return DB::table('events')->where(
			[
				'id' => $this->getAttributeValue('event_id')
			]
		)->first();
	}

}
