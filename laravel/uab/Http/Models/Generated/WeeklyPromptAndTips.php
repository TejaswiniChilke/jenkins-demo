<?php
namespace Uab\Http\Models\Generated;

use Uab\Http\Models\Base\BaseModel;


/**
 * Class WeeklyPromptAndTips
 *
 * @property number id
 * @property string content_prompt
 * @property string tip_intro
 * @property string message
 * @property number month
 * @property number week
 *
 * @package Uab\Http\Models\Generated
 */
class WeeklyPromptAndTips extends BaseModel {
    
    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'modified_at';
    const DELETED_AT = 'deleted_at';
    

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'content_prompt' => null,
        'tip_intro' => null,
        'message' => null,
        'month' => null,
        'week' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'content_prompt' => 'string',
		'tip_intro' => 'string',
		'message' => 'string',
		'month' => 'integer',
		'week' => 'integer'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'content_prompt',
		'tip_intro',
		'message',
		'month',
		'week'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'weekly_prompt_and_tips';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'content_prompt',
		'tip_intro',
		'message',
		'month',
		'week'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [

	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        
    ];

}
