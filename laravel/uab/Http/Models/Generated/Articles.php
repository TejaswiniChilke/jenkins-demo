<?php
namespace Uab\Http\Models\Generated;

use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\Model;
use Uab\Http\Models\Images;

/**
 * Class Articles
 *
 * @property number id
 * @property string created
 * @property string modified
 * @property string title
 * @property string description
 * @property string content
 * @property number image_id
 * @property number week
 * @property number content_image_id
 * @property string short_description
 * @property number end_week
 *
 * @package Uab\Http\Models\Generated
 */
class Articles extends BaseModel {

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';


    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created' => null,
        'modified' => null,
        'description' => null,
        'content' => null,
        'image_id' => null,
        'week' => 0,
        'content_image_id' => null,
        'short_description' => null,
        'end_week' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime',
		'title' => 'string',
		'description' => 'string',
		'content' => 'string',
		'image_id' => 'integer',
		'week' => 'integer',
		'content_image_id' => 'integer',
		'short_description' => 'string',
		'end_week' => 'integer'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'created',
		'modified',
		'title',
		'description',
		'content',
		'image_id',
		'week',
		'content_image_id',
		'short_description',
		'end_week'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'articles';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'title',
		'description',
		'content',
		'image_id',
		'week',
		'content_image_id',
		'short_description',
		'end_week'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'images' => 'image_id',
		'content_images' => 'content_image_id',
        'articles_tags' => null,
    ];

	/**
	 * @return Images|Model|null
	*/
	public function image() {
		$id = $this->getAttributeValue('image_id');

		if (is_null($id)) {
			return null;
		}

		return Images::query()->find($id);
	}

	/**
	 * @return Images|Model|null
	*/
	public function contentImage() {
		$id = $this->getAttributeValue('content_image_id');

		if (is_null($id)) {
			return null;
		}

		return Images::query()->find($id);
	}

}
