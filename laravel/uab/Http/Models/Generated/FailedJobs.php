<?php
namespace Uab\Http\Models\Generated;

use Uab\Events\AfterCreate\AfterCreate;
use Uab\Events\AfterEdit\AfterEdit;
use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use stdClass;

/**
 * Class FailedJobs
 *
 * @property mixed id
 * @property string connection
 * @property string queue
 * @property mixed payload
 * @property mixed exception
 * @property mixed failed_at
 *
 * @package Uab\Http\Models\Generated
 */
class FailedJobs extends BaseModel {
    
    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'modified_at';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'failed_at' => 'CURRENT_TIMESTAMP'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'string',
		'connection' => 'string',
		'queue' => 'string',
		'payload' => 'string',
		'exception' => 'string',
		'failed_at' => 'timestamp'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'connection',
		'queue',
		'payload',
		'exception',
		'failed_at'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'failed_jobs';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'connection',
		'queue',
		'payload',
		'exception',
		'failed_at'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'failed_at'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        
    ];

}
