<?php
namespace Uab\Http\Models\Generated;

use DB;
use stdClass;
use Uab\Http\Models\Base\BaseModel;

/**
 * Class UserTypes
 *
 * @property number id
 * @property string name
 * @property string modified
 * @property string display_name
 * @property string created
 * @property string color
 * @property number project_id
 *
 * @package Uab\Http\Models\Generated
 */
class UserTypes extends BaseModel {

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';



    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'modified' => null,
        'created' => null,
        'color' => null,
        'project_id' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'name' => 'string',
		'modified' => 'datetime',
		'display_name' => 'string',
		'created' => 'datetime',
		'color' => 'string',
		'project_id' => 'integer'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'name',
		'modified',
		'display_name',
		'created',
		'color',
		'project_id'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_types';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'name',
		'display_name',
		'color',
		'project_id'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'modified',
		'created'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'projects' => 'project_id'
    ];

	/**
	 * @return stdClass
	*/
	public function project() {
		return DB::table('projects')->where(
			[
				'id' => $this->getAttributeValue('project_id')
			]
		)->first();
	}

}
