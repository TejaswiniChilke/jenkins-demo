<?php
namespace Uab\Http\Models\Generated;

use Uab\Http\Models\Base\BaseModel;

use Uab\Http\Models\Externals;
use Uab\Http\Models\ExternalObjects;

/**
 * Class Hospitals
 *
 * @property number id
 * @property string created
 * @property string modified
 * @property string latitude
 * @property string longitude
 * @property string external_id
 * @property string external_object_id
 * @property string name
 * @property string address_line_one
 * @property string city
 * @property string state
 * @property string zipcode
 * @property string zipcode_four
 * @property string telephone
 * @property string type
 * @property boolean is_open
 * @property string population
 * @property string county
 * @property string county_fpis
 * @property string country
 * @property string naics_code
 * @property string naics_description
 * @property string source
 * @property mixed source_date
 * @property string validation_date
 * @property string validation_method
 * @property string website
 * @property string alternative_name
 * @property string owner
 * @property string total_staff
 * @property string beds
 * @property string trauma
 * @property boolean has_helipad
 *
 * @package Uab\Http\Models\Generated
 */
class Hospitals extends BaseModel {

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';


    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created' => null,
        'modified' => null,
        'latitude' => null,
        'longitude' => null,
        'external_id' => null,
        'external_object_id' => null,
        'name' => null,
        'address_line_one' => null,
        'city' => null,
        'state' => null,
        'zipcode' => null,
        'zipcode_four' => null,
        'telephone' => null,
        'type' => null,
        'is_open' => null,
        'population' => null,
        'county' => null,
        'county_fpis' => null,
        'country' => null,
        'naics_code' => null,
        'naics_description' => null,
        'source' => null,
        'source_date' => null,
        'validation_date' => null,
        'validation_method' => null,
        'website' => null,
        'alternative_name' => null,
        'owner' => null,
        'total_staff' => null,
        'beds' => null,
        'trauma' => null,
        'has_helipad' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime',
		'latitude' => 'string',
		'longitude' => 'string',
		'external_id' => 'string',
		'external_object_id' => 'string',
		'name' => 'string',
		'address_line_one' => 'string',
		'city' => 'string',
		'state' => 'string',
		'zipcode' => 'string',
		'zipcode_four' => 'string',
		'telephone' => 'string',
		'type' => 'string',
		'is_open' => 'boolean',
		'population' => 'string',
		'county' => 'string',
		'county_fpis' => 'string',
		'country' => 'string',
		'naics_code' => 'string',
		'naics_description' => 'string',
		'source' => 'string',
		'source_date' => 'date',
		'validation_date' => 'string',
		'validation_method' => 'string',
		'website' => 'string',
		'alternative_name' => 'string',
		'owner' => 'string',
		'total_staff' => 'string',
		'beds' => 'string',
		'trauma' => 'string',
		'has_helipad' => 'boolean'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'created',
		'modified',
		'latitude',
		'longitude',
		'external_id',
		'external_object_id',
		'name',
		'address_line_one',
		'city',
		'state',
		'zipcode',
		'zipcode_four',
		'telephone',
		'type',
		'is_open',
		'population',
		'county',
		'county_fpis',
		'country',
		'naics_code',
		'naics_description',
		'source',
		'source_date',
		'validation_date',
		'validation_method',
		'website',
		'alternative_name',
		'owner',
		'total_staff',
		'beds',
		'trauma',
		'has_helipad'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'hospitals';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'latitude',
		'longitude',
		'external_id',
		'external_object_id',
		'name',
		'address_line_one',
		'city',
		'state',
		'zipcode',
		'zipcode_four',
		'telephone',
		'type',
		'is_open',
		'population',
		'county',
		'county_fpis',
		'country',
		'naics_code',
		'naics_description',
		'source',
		'source_date',
		'validation_date',
		'validation_method',
		'website',
		'alternative_name',
		'owner',
		'total_staff',
		'beds',
		'trauma',
		'has_helipad'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified',
		'source_date'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [

    ];

}
