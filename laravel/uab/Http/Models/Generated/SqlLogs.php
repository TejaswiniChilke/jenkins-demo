<?php
namespace Uab\Http\Models\Generated;

use Uab\Http\Models\Base\BaseModel;
use DB;
use stdClass;

/**
 * Class SqlLogs
 *
 * @property number id
 * @property number user_id
 * @property string query
 * @property string params
 * @property string executed_at
 * @property string created
 * @property string modified
 *
 * @package Uab\Http\Models\Generated
 */
class SqlLogs extends BaseModel {

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';



    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'user_id' => null,
        'created' => null,
        'modified' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'user_id' => 'integer',
		'query' => 'string',
		'params' => 'string',
		'executed_at' => 'datetime',
		'created' => 'datetime',
		'modified' => 'datetime'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'user_id',
		'query',
		'params',
		'executed_at',
		'created',
		'modified'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sql_logs';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'user_id',
		'query',
		'params',
		'executed_at'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'executed_at',
		'created',
		'modified'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'users' => 'user_id'
    ];

	/**
	 * @return stdClass
	*/
	public function user() {
		return DB::table('users')->where(
			[
				'id' => $this->getAttributeValue('user_id')
			]
		)->first();
	}

}
