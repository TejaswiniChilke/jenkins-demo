<?php
namespace Uab\Http\Models\Generated;

use Uab\Events\AfterCreate\AfterCreate;
use Uab\Events\AfterEdit\AfterEdit;
use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use stdClass;

/**
 * Class ImagesVideos
 *
 * @property number id
 * @property number position
 * @property number video_id
 * @property string created
 * @property string modified
 * @property number image_id
 * @property string text
 *
 * @package Uab\Http\Models\Generated
 */
class ImagesVideos extends BaseModel {
    
    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created' => null,
        'modified' => null,
        'text' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'position' => 'integer',
		'video_id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime',
		'image_id' => 'integer',
		'text' => 'string'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'position',
		'video_id',
		'created',
		'modified',
		'image_id',
		'text'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'images_videos';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'position',
		'video_id',
		'image_id',
		'text'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'videos' => 'video_id',
		'images' => 'image_id'
    ];

	/**
	 * @return stdClass
	*/
	public function video() {
		return DB::table('videos')->where(
			[
				'id' => $this->getAttributeValue('video_id')
			]
		)->first();
	}

	/**
	 * @return stdClass
	*/
	public function image() {
		return DB::table('images')->where(
			[
				'id' => $this->getAttributeValue('image_id')
			]
		)->first();
	}

}
