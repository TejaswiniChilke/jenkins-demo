<?php
namespace Uab\Http\Models\Generated;

use Uab\Events\AfterCreate\AfterCreate;
use Uab\Events\AfterEdit\AfterEdit;
use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use stdClass;

/**
 * Class Notifications
 *
 * @property number id
 * @property string created
 * @property string modified
 * @property string message
 * @property string type
 * @property number user_id
 * @property boolean has_read
 * @property number image_id
 * @property string reference_name
 * @property string reference_value
 *
 * @package Uab\Http\Models\Generated
 */
class Notifications extends BaseModel {
    
    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created' => null,
        'modified' => null,
        'type' => null,
        'has_read' => 0,
        'image_id' => null,
        'reference_name' => null,
        'reference_value' => 0
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime',
		'message' => 'string',
		'type' => 'string',
		'user_id' => 'integer',
		'has_read' => 'boolean',
		'image_id' => 'integer',
		'reference_name' => 'string',
		'reference_value' => 'string'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'created',
		'modified',
		'message',
		'type',
		'user_id',
		'has_read',
		'image_id',
		'reference_name',
		'reference_value'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'notifications';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'message',
		'type',
		'user_id',
		'has_read',
		'image_id',
		'reference_name',
		'reference_value'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'users' => 'user_id',
		'images' => 'image_id'
    ];

	/**
	 * @return stdClass
	*/
	public function user() {
		return DB::table('users')->where(
			[
				'id' => $this->getAttributeValue('user_id')
			]
		)->first();
	}

	/**
	 * @return stdClass
	*/
	public function image() {
		return DB::table('images')->where(
			[
				'id' => $this->getAttributeValue('image_id')
			]
		)->first();
	}

}
