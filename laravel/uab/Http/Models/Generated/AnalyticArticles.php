<?php
namespace Uab\Http\Models\Generated;

use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class AnalyticArticles
 *
 * @property number id
 * @property string created
 * @property string modified
 * @property number article_id
 * @property number user_id
 * @property string read_on
 * @property number read_duration
 * @property string deleted_at
 *
 * @package Uab\Http\Models\Generated
 */
class AnalyticArticles extends BaseModel {
    use SoftDeletes;

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created' => null,
        'modified' => null,
        'deleted_at' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime',
		'article_id' => 'integer',
		'user_id' => 'integer',
		'read_on' => 'datetime',
		'read_duration' => 'integer',
		'deleted_at' => 'datetime'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'created',
		'modified',
		'article_id',
		'user_id',
		'read_on',
		'read_duration',
		'deleted_at'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'analytic_articles';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'article_id',
		'user_id',
		'read_on',
		'read_duration',
		'deleted_at'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified',
		'read_on',
		'deleted_at'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'articles' => 'article_id',
		'users' => 'user_id'
    ];

	/**
	 * @return \Uab\Http\Models\Articles|null
	*/
	public function article() {
		return \Uab\Http\Models\Articles::query()->where(
			[
				'id' => $this->getAttributeValue('article_id')
			]
		)->first();
	}

	/**
	 * @return \Uab\Http\Models\Users|null
	*/
	public function user() {
		return \Uab\Http\Models\Users::query()->where(
			[
				'id' => $this->getAttributeValue('user_id')
			]
		)->first();
	}

}
