<?php
namespace Uab\Http\Models\Generated;

use Uab\Http\Models\Base\BaseModel;

/**
 * Class SecurityQuestions
 *
 * @property number id
 * @property string modified
 * @property string created
 * @property string question
 *
 * @package Uab\Http\Models\Generated
 */
class SecurityQuestions extends BaseModel {

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'modified' => null,
        'created' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'modified' => 'datetime',
		'created' => 'datetime',
		'question' => 'string'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'modified',
		'created',
		'question'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'security_questions';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'question'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'modified',
		'created'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [

    ];

}
