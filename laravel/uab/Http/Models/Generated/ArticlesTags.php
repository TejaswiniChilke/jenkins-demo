<?php
namespace Uab\Http\Models\Generated;

use Uab\Events\AfterCreate\AfterCreate;
use Uab\Events\AfterEdit\AfterEdit;
use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use stdClass;

/**
 * Class ArticlesTags
 *
 * @property number id
 * @property string created
 * @property string modified
 * @property number article_id
 * @property number tag_id
 * @property boolean exclusive
 *
 * @package Uab\Http\Models\Generated
 */
class ArticlesTags extends BaseModel {

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created' => null,
        'modified' => null,
        'exclusive' => 0
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime',
		'article_id' => 'integer',
		'tag_id' => 'integer',
		'exclusive' => 'boolean'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'created',
		'modified',
		'article_id',
		'tag_id',
		'exclusive'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'articles_tags';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'article_id',
		'tag_id',
		'exclusive'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'articles' => 'article_id',
		'tags' => 'tag_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
	public function article() {
		return \Uab\Http\Models\Articles::query()->where(
			[
				'id' => $this->getAttributeValue('article_id')
			]
		)->first();
	}

	/**
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
	public function tag() {
		return \Uab\Http\Models\Tags::query()->where(
			[
				'id' => $this->getAttributeValue('tag_id')
			]
		)->first();
	}

}
