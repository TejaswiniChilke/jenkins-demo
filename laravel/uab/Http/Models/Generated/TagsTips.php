<?php
namespace Uab\Http\Models\Generated;

use Uab\Events\AfterCreate\AfterCreate;
use Uab\Events\AfterEdit\AfterEdit;
use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use stdClass;

/**
 * Class TagsTips
 *
 * @property number id
 * @property string created
 * @property string modified
 * @property number tag_id
 * @property number tip_id
 *
 * @package Uab\Http\Models\Generated
 */
class TagsTips extends BaseModel {
    
    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created' => null,
        'modified' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime',
		'tag_id' => 'integer',
		'tip_id' => 'integer'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'created',
		'modified',
		'tag_id',
		'tip_id'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tags_tips';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'tag_id',
		'tip_id'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'tags' => 'tag_id',
		'tips' => 'tip_id'
    ];

	/**
	 * @return stdClass
	*/
	public function tag() {
		return DB::table('tags')->where(
			[
				'id' => $this->getAttributeValue('tag_id')
			]
		)->first();
	}

	/**
	 * @return stdClass
	*/
	public function tip() {
		return DB::table('tips')->where(
			[
				'id' => $this->getAttributeValue('tip_id')
			]
		)->first();
	}

}
