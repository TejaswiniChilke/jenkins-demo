<?php
namespace Uab\Http\Models\Generated;

use Uab\Events\AfterCreate\AfterCreate;
use Uab\Events\AfterEdit\AfterEdit;
use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use stdClass;

/**
 * Class SettingsUsers
 *
 * @property number id
 * @property string value
 * @property number user_id
 * @property number setting_id
 * @property string modified
 * @property string created
 *
 * @package Uab\Http\Models\Generated
 */
class SettingsUsers extends BaseModel {
    
    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'modified' => null,
        'created' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'value' => 'string',
		'user_id' => 'integer',
		'setting_id' => 'integer',
		'modified' => 'datetime',
		'created' => 'datetime'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'value',
		'user_id',
		'setting_id',
		'modified',
		'created'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'settings_users';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'value',
		'user_id',
		'setting_id'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'modified',
		'created'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'users' => 'user_id',
		'settings' => 'setting_id'
    ];

	/**
	 * @return stdClass
	*/
	public function user() {
		return DB::table('users')->where(
			[
				'id' => $this->getAttributeValue('user_id')
			]
		)->first();
	}

	/**
	 * @return stdClass
	*/
	public function setting() {
		return DB::table('settings')->where(
			[
				'id' => $this->getAttributeValue('setting_id')
			]
		)->first();
	}

}
