<?php
namespace Uab\Http\Models\Generated;

use Uab\Events\AfterCreate\AfterCreate;
use Uab\Events\AfterEdit\AfterEdit;
use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use stdClass;

/**
 * Class ColorTemplates
 *
 * @property number id
 * @property string created
 * @property string modified
 * @property string accept_button_color
 * @property string accept_button_text_color
 * @property string decline_button_color
 * @property string decline_button_text_color
 * @property string neutral_button_color
 * @property string neutral_button_text_color
 * @property string primary_color
 * @property string primary_text_color
 * @property string secondary_color
 * @property string secondary_text_color
 * @property number project_id
 *
 * @package Uab\Http\Models\Generated
 */
class ColorTemplates extends BaseModel {
    
    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created' => null,
        'modified' => null,
        'accept_button_color' => null,
        'accept_button_text_color' => null,
        'decline_button_color' => null,
        'decline_button_text_color' => null,
        'neutral_button_color' => null,
        'neutral_button_text_color' => null,
        'primary_color' => null,
        'primary_text_color' => null,
        'secondary_color' => null,
        'secondary_text_color' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime',
		'accept_button_color' => 'string',
		'accept_button_text_color' => 'string',
		'decline_button_color' => 'string',
		'decline_button_text_color' => 'string',
		'neutral_button_color' => 'string',
		'neutral_button_text_color' => 'string',
		'primary_color' => 'string',
		'primary_text_color' => 'string',
		'secondary_color' => 'string',
		'secondary_text_color' => 'string',
		'project_id' => 'integer'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'created',
		'modified',
		'accept_button_color',
		'accept_button_text_color',
		'decline_button_color',
		'decline_button_text_color',
		'neutral_button_color',
		'neutral_button_text_color',
		'primary_color',
		'primary_text_color',
		'secondary_color',
		'secondary_text_color',
		'project_id'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'color_templates';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'accept_button_color',
		'accept_button_text_color',
		'decline_button_color',
		'decline_button_text_color',
		'neutral_button_color',
		'neutral_button_text_color',
		'primary_color',
		'primary_text_color',
		'secondary_color',
		'secondary_text_color',
		'project_id'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'projects' => 'project_id'
    ];

	/**
	 * @return stdClass
	*/
	public function project() {
		return DB::table('projects')->where(
			[
				'id' => $this->getAttributeValue('project_id')
			]
		)->first();
	}

}
