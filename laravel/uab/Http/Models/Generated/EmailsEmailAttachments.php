<?php
namespace Uab\Http\Models\Generated;

use Uab\Events\AfterCreate\AfterCreate;
use Uab\Events\AfterEdit\AfterEdit;
use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use stdClass;

/**
 * Class EmailsEmailAttachments
 *
 * @property number id
 * @property number email_id
 * @property number email_attachment_id
 * @property string created
 * @property string modified
 *
 * @package Uab\Http\Models\Generated
 */
class EmailsEmailAttachments extends BaseModel {

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created' => null,
        'modified' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'email_id' => 'integer',
		'email_attachment_id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'email_id',
		'email_attachment_id',
		'created',
		'modified'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'emails_email_attachments';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'email_id',
		'email_attachment_id'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'emails' => 'email_id',
		'email_attachments' => 'email_attachment_id'
    ];

	/**
	 * @return stdClass
	*/
	public function email() {
		return DB::table('emails')->where(
			[
				'id' => $this->getAttributeValue('email_id')
			]
		)->first();
	}

	/**
	 * @return stdClass
	*/
	public function emailAttachment() {
		return DB::table('email_attachments')->where(
			[
				'id' => $this->getAttributeValue('email_attachment_id')
			]
		)->first();
	}

}
