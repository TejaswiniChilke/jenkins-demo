<?php
namespace Uab\Http\Models\Generated;

use Uab\Http\Models\Base\BaseModel;
use stdClass;

/**
 * Class Videos
 *
 * @property number id
 * @property string file_path
 * @property string description
 * @property string title
 * @property string created
 * @property string modified
 * @property number image_id
 * @property number week
 * @property number seconds
 * @property number end_week
 * @property string short_description
 *
 * @package Uab\Http\Models\Generated
 */
class Videos extends BaseModel {

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'description' => null,
        'title' => null,
        'created' => null,
        'modified' => null,
        'image_id' => null,
        'week' => 0,
        'seconds' => 0,
        'end_week' => null,
        'short_description' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'file_path' => 'string',
		'description' => 'string',
		'title' => 'string',
		'created' => 'datetime',
		'modified' => 'datetime',
		'image_id' => 'integer',
		'week' => 'integer',
		'seconds' => 'integer',
		'end_week' => 'integer',
		'short_description' => 'string'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'file_path',
		'description',
		'title',
		'created',
		'modified',
		'image_id',
		'week',
		'seconds',
		'end_week',
		'short_description'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'videos';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'file_path',
		'description',
		'title',
		'image_id',
		'week',
		'seconds',
		'end_week',
		'short_description'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'tags_videos' => null,
        'images' => 'image_id'
    ];

	/**
	 * @return stdClass
	*/
	public function image() {
		return Images::query()->where(
			[
				'id' => $this->getAttributeValue('image_id')
			]
		)->first();
	}

	public function tags() {
        return \Uab\Http\Models\Tags::query()
            ->select('tags.*')
            ->distinct()
            ->join('tags_videos','tags_videos.tag_id', '=', 'tags.id')
            ->where(
                [
                    'tags_videos.video_id' => $this->getAttributeValue('id')
                ]
            )->get();
    }
}
