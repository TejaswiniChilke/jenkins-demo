<?php
namespace Uab\Http\Models\Generated;

use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use stdClass;

/**
 * Class Events
 *
 * @property number id
 * @property string start
 * @property string end
 * @property string title
 * @property string description
 * @property number creator_id
 * @property string created
 * @property string modified
 * @property boolean allDay
 * @property boolean is_cancelled
 * @property boolean is_complete
 * @property string deleted_at
 *
 * @package Uab\Http\Models\Generated
 */
class Events extends BaseModel {
    use SoftDeletes;

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';



    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'end' => null,
        'description' => null,
        'creator_id' => null,
        'created' => null,
        'modified' => null,
        'allDay' => 0,
        'is_cancelled' => 0,
        'is_complete' => 0,
        'deleted_at' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'start' => 'datetime',
		'end' => 'datetime',
		'title' => 'string',
		'description' => 'string',
		'creator_id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime',
		'allDay' => 'boolean',
		'is_cancelled' => 'boolean',
		'is_complete' => 'boolean',
		'deleted_at' => 'datetime'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'start',
		'end',
		'title',
		'description',
		'creator_id',
		'created',
		'modified',
		'allDay',
		'is_cancelled',
		'is_complete',
		'deleted_at'
    ];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'events';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'start',
		'end',
		'title',
		'description',
		'creator_id',
		'allDay',
		'is_cancelled',
		'is_complete',
		'deleted_at'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'start',
		'end',
		'created',
		'modified',
		'deleted_at'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'creators' => 'creator_id'
    ];

	/**
	 * @return stdClass
	*/
	public function creator() {
		return DB::table('users')->where(
			[
				'id' => $this->getAttributeValue('creator_id')
			]
		)->first();
	}

}
