<?php
namespace Uab\Http\Models\Generated;

use Uab\Http\Models\Base\BaseModel;

use Uab\Http\Models\AnswerInputTypes;
use Uab\Http\Models\Images;
use Uab\Http\Models\SurveyPages;

/**
 * Class Questions
 *
 * @property number id
 * @property number position
 * @property string phrasing
 * @property string created
 * @property string modified
 * @property number answer_input_type_id
 * @property boolean required
 * @property number image_id
 * @property number survey_page_id
 * @property string placeholder
 * @property string next_question_function
 *
 * @package Uab\Http\Models\Generated
 */
class Questions extends BaseModel {

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'position' => 0,
        'created' => null,
        'modified' => null,
        'answer_input_type_id' => null,
        'required' => 0,
        'image_id' => null,
        'survey_page_id' => null,
        'placeholder' => '',
        'next_question_function' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'position' => 'integer',
		'phrasing' => 'string',
		'created' => 'datetime',
		'modified' => 'datetime',
		'answer_input_type_id' => 'integer',
		'required' => 'boolean',
		'image_id' => 'integer',
		'survey_page_id' => 'integer',
		'placeholder' => 'string',
		'next_question_function' => 'string'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'position',
		'phrasing',
		'created',
		'modified',
		'answer_input_type_id',
		'required',
		'image_id',
		'survey_page_id',
		'placeholder',
		'next_question_function'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'questions';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'position',
		'phrasing',
		'answer_input_type_id',
		'required',
		'image_id',
		'survey_page_id',
		'placeholder',
		'next_question_function'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'answer_input_types' => 'answer_input_type_id',
        'answer_options' => null,
		'images' => 'image_id',
		'survey_pages' => 'survey_page_id'
    ];

	/**
	 * @return AnswerInputTypes|null
	*/
	public function answerInputType() {
		$id = $this->getAttributeValue('answer_input_type_id');
		
		if (is_null($id)) {
			return null;
		}

		return AnswerInputTypes::query()->find($id);
	}

	/**
	 * @return Images|null
	*/
	public function image() {
		$id = $this->getAttributeValue('image_id');
		
		if (is_null($id)) {
			return null;
		}

		return Images::query()->find($id);
	}

	/**
	 * @return SurveyPages|null
	*/
	public function surveyPage() {
		$id = $this->getAttributeValue('survey_page_id');
		
		if (is_null($id)) {
			return null;
		}

		return SurveyPages::query()->find($id);
	}

}
