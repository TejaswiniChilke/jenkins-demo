<?php
namespace Uab\Http\Models\Generated;

use Uab\Events\AfterCreate\AfterCreate;
use Uab\Events\AfterEdit\AfterEdit;
use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use stdClass;

/**
 * Class CommentsPosts
 *
 * @property number id
 * @property number post_id
 * @property string created
 * @property string modified
 * @property number comment_id
 * @property string deleted_at
 *
 * @package Uab\Http\Models\Generated
 */
class CommentsPosts extends BaseModel {
    use SoftDeletes;

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created' => null,
        'modified' => null,
        'deleted_at' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'post_id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime',
		'comment_id' => 'integer',
		'deleted_at' => 'datetime'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'post_id',
		'created',
		'modified',
		'comment_id',
		'deleted_at'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'comments_posts';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'post_id',
		'comment_id',
		'deleted_at'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified',
		'deleted_at'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'posts' => 'post_id',
		'comments' => 'comment_id'
    ];

	/**
	 * @return stdClass
	*/
	public function post() {
		return DB::table('posts')->where(
			[
				'id' => $this->getAttributeValue('post_id')
			]
		)->first();
	}

	/**
	 * @return stdClass
	*/
	public function comment() {
		return DB::table('comments')->where(
			[
				'id' => $this->getAttributeValue('comment_id')
			]
		)->first();
	}

}
