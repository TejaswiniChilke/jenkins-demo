<?php
namespace Uab\Http\Models\Generated;

use Uab\Events\AfterCreate\AfterCreate;
use Uab\Events\AfterEdit\AfterEdit;
use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use stdClass;

/**
 * Class Posts
 *
 * @property number id
 * @property number from_user_id
 * @property number to_user_id
 * @property string message
 * @property string created
 * @property string modified
 * @property string stickied
 * @property number image_id
 * @property mixed deleted_at
 * @property string type
 *
 * @package Uab\Http\Models\Generated
 */
class Posts extends BaseModel {
    use SoftDeletes;

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'from_user_id' => null,
        'to_user_id' => null,
        'created' => null,
        'modified' => null,
        'stickied' => null,
        'image_id' => null,
        'deleted_at' => null,
        'type' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'from_user_id' => 'integer',
		'to_user_id' => 'integer',
		'message' => 'string',
		'created' => 'datetime',
		'modified' => 'datetime',
		'stickied' => 'datetime',
		'image_id' => 'integer',
		'deleted_at' => 'timestamp',
		'type' => 'string'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'from_user_id',
		'to_user_id',
		'message',
		'created',
		'modified',
		'stickied',
		'image_id',
		'deleted_at',
		'type'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'posts';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'from_user_id',
		'to_user_id',
		'message',
		'stickied',
		'image_id',
		'deleted_at',
		'type'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified',
		'stickied',
		'deleted_at'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'comments' => null,
        'from_users' => 'from_user_id',
		'to_users' => 'to_user_id',
		'images' => 'image_id'
    ];

	/**
	 * @return stdClass
	*/
	public function fromUser() {
		return DB::table('users')->where(
			[
				'id' => $this->getAttributeValue('from_user_id')
			]
		)->first();
	}

	/**
	 * @return stdClass
	*/
	public function toUser() {
		return DB::table('users')->where(
			[
				'id' => $this->getAttributeValue('to_user_id')
			]
		)->first();
	}

	/**
	 * @return stdClass
	*/
	public function image() {
		return DB::table('images')->where(
			[
				'id' => $this->getAttributeValue('image_id')
			]
		)->first();
	}

}
