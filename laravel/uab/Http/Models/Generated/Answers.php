<?php
namespace Uab\Http\Models\Generated;

use Uab\Events\AfterCreate\AfterCreate;
use Uab\Events\AfterEdit\AfterEdit;
use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use stdClass;

/**
 * Class Answers
 *
 * @property number id
 * @property string response
 * @property string created
 * @property string modified
 * @property number answer_option_id
 * @property number surveys_users_id
 * @property number question_id
 * @property string deleted_at
 *
 * @package Uab\Http\Models\Generated
 */
class Answers extends BaseModel {
    use SoftDeletes;

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'response' => null,
        'created' => null,
        'modified' => null,
        'answer_option_id' => null,
        'surveys_users_id' => null,
        'deleted_at' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'response' => 'string',
		'created' => 'datetime',
		'modified' => 'datetime',
		'answer_option_id' => 'integer',
		'surveys_users_id' => 'integer',
		'question_id' => 'integer',
		'deleted_at' => 'datetime'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'response',
		'created',
		'modified',
		'answer_option_id',
		'surveys_users_id',
		'question_id',
		'deleted_at'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'answers';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'response',
		'answer_option_id',
		'surveys_users_id',
		'question_id',
		'deleted_at'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified',
		'deleted_at'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'answer_options' => 'answer_option_id',
		'surveys_users' => 'surveys_users_id',
		'questions' => 'question_id'
    ];

	/**
	 * @return stdClass
	*/
	public function answerOption() {
		return DB::table('answer_options')->where(
			[
				'id' => $this->getAttributeValue('answer_option_id')
			]
		)->first();
	}

	/**
	 * @return stdClass
	*/
	public function surveysUsers() {
		return DB::table('surveys_users')->where(
			[
				'id' => $this->getAttributeValue('surveys_users_id')
			]
		)->first();
	}

	/**
	 * @return stdClass
	*/
	public function question() {
		return DB::table('questions')->where(
			[
				'id' => $this->getAttributeValue('question_id')
			]
		)->first();
	}

}
