<?php
namespace Uab\Http\Models\Generated;

use Illuminate\Support\Collection;
use Uab\Http\Models\Base\BaseModel;
use DB;
use stdClass;

/**
 * Class Playlists
 *
 * @property number id
 * @property string title
 * @property string created
 * @property string modified
 * @property number image_id
 * @property string description
 * @property number week
 * @property number end_week
 * @property string short_description
 *
 * @package Uab\Http\Models\Generated
 */
class Playlists extends BaseModel {

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created' => null,
        'modified' => null,
        'image_id' => null,
        'description' => null,
        'week' => 0,
        'end_week' => 0,
        'short_description' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'title' => 'string',
		'created' => 'datetime',
		'modified' => 'datetime',
		'image_id' => 'integer',
		'description' => 'string',
		'week' => 'integer',
		'end_week' => 'integer',
		'short_description' => 'string'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'title',
		'created',
		'modified',
		'image_id',
		'description',
		'week',
		'end_week',
		'short_description'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'playlists';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'title',
		'image_id',
		'description',
		'week',
		'end_week',
		'short_description'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'playlists_tags' => null,
        'images' => 'image_id',
        'playlists_videos' => null,
        'videos' => null
    ];

	/**
	 * @return stdClass
	*/
	public function image() {
		return \Uab\Http\Models\Images::query()->where(
			[
				'id' => $this->getAttributeValue('image_id')
			]
		)->first();
	}

    /**
     * @return \Uab\Http\Models\Videos|Collection|\Uab\Http\Models\Videos[]
     */
	public function videos() {
        return \Uab\Http\Models\Videos::query()
            ->select('videos.*')
            ->join('playlists_videos','playlists_videos.video_id', '=', 'videos.id')
            ->where(
                [
                    'playlists_videos.playlist_id' => $this->getAttributeValue('id')
                ]
            )->get();
    }
}
