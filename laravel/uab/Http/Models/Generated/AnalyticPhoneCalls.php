<?php
namespace Uab\Http\Models\Generated;

use Uab\Http\Models\Base\BaseModel;

use Uab\Http\Models\Users;

/**
 * Class AnalyticPhoneCalls
 *
 * @property number id
 * @property string created
 * @property string deleted
 * @property string modified
 * @property mixed start_time
 * @property mixed end_time
 * @property number to_user_id
 * @property number to_number
 * @property number from_number
 * @property number from_user_id
 * @property string direction
 * @property string status
 *
 * @package Uab\Http\Models\Generated
 */
class AnalyticPhoneCalls extends BaseModel {

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted';


    /**
     * All default attributes
     *
     * @var array
     */
    protected $attributes = [
        'created' => null,
        'deleted' => null,
        'modified' => null,
        'start_time' => null,
        'end_time' => null,
        'direction' => null,
        'status' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'created' => 'datetime',
        'deleted' => 'datetime',
        'modified' => 'datetime',
        'start_time' => 'string',
        'end_time' => 'string',
        'to_user_id' => 'integer',
        'to_number' => 'integer',
        'from_number' => 'integer',
        'from_user_id' => 'integer',
        'direction' => 'string',
        'status' => 'string'
    ];

    /**
     * All columns
     *
     * @var array
     */
    protected $columns = [
        'id',
        'created',
        'deleted',
        'modified',
        'start_time',
        'end_time',
        'to_user_id',
        'to_number',
        'from_number',
        'from_user_id',
        'direction',
        'status'
    ];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'analytic_phone_calls';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'deleted',
        'start_time',
        'end_time',
        'to_user_id',
        'to_number',
        'from_number',
        'from_user_id',
        'direction',
        'status'
    ];

    /**
     * Date field attributes
     *
     * @var array
     */
    protected $dates = [
        'created',
        'deleted',
        'modified'
    ];

    /**
     * List of all relationships (other_table => current_table)
     *
     * @var array
     */
    public $relationships = [
        'to_users' => 'to_user_id',
        'from_users' => 'from_user_id'
    ];

    /**
     * @return Users|null
     */
    public function toUser() {
        $id = $this->getAttributeValue('to_user_id');

        if (is_null($id)) {
            return null;
        }

        return Users::query()->find($id);
    }

    /**
     * @return Users|null
     */
    public function fromUser() {
        $id = $this->getAttributeValue('from_user_id');

        if (is_null($id)) {
            return null;
        }

        return Users::query()->find($id);
    }

}
