<?php
namespace Uab\Http\Models\Generated;

use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Uab\Http\Models\ReturnedPlaces;

/**
 * Class googleAddresses
 *
 * @property number id
 * @property string created
 * @property string modified
 * @property string deleted_at
 * @property string requested_location
 * @property string requested_city
 * @property string requested_state
 * @property string returned_address
 * @property string returned_lat
 * @property string returned_long
 * @property string returned_zipcode
 * @property string returned_place_id
 * @property string returned_state_short_name
 * @property string returned_state_long_name
 * @property string returned_county
 * @property string returned_locality
 * @property string returned_route
 * @property string returned_street_number
 *
 * @package Uab\Http\Models\Generated
 */
class GoogleAddresses extends BaseModel {
    use SoftDeletes;

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';


    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created' => null,
        'modified' => null,
        'deleted_at' => null,
        'requested_location' => null,
        'requested_city' => null,
        'requested_state' => null,
        'returned_address' => null,
        'returned_lat' => null,
        'returned_long' => null,
        'returned_zipcode' => null,
        'returned_place_id' => null,
        'returned_state_short_name' => null,
        'returned_state_long_name' => null,
        'returned_county' => null,
        'returned_locality' => null,
        'returned_route' => null,
        'returned_street_number' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime',
		'deleted_at' => 'datetime',
		'requested_location' => 'string',
		'requested_city' => 'string',
		'requested_state' => 'string',
		'returned_address' => 'string',
		'returned_lat' => 'string',
		'returned_long' => 'string',
		'returned_zipcode' => 'string',
		'returned_place_id' => 'string',
		'returned_state_short_name' => 'string',
		'returned_state_long_name' => 'string',
		'returned_county' => 'string',
		'returned_locality' => 'string',
		'returned_route' => 'string',
		'returned_street_number' => 'string'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'created',
		'modified',
		'deleted_at',
		'requested_location',
		'requested_city',
		'requested_state',
		'returned_address',
		'returned_lat',
		'returned_long',
		'returned_zipcode',
		'returned_place_id',
		'returned_state_short_name',
		'returned_state_long_name',
		'returned_county',
		'returned_locality',
		'returned_route',
		'returned_street_number'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'google_addresses';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'deleted_at',
		'requested_location',
		'requested_city',
		'requested_state',
		'returned_address',
		'returned_lat',
		'returned_long',
		'returned_zipcode',
		'returned_place_id',
		'returned_state_short_name',
		'returned_state_long_name',
		'returned_county',
		'returned_locality',
		'returned_route',
		'returned_street_number'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified',
		'deleted_at'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'returned_places' => 'returned_place_id'
    ];

	/**
	 * @return ReturnedPlaces|null
	*/
	public function returnedPlace() {
		$id = $this->getAttributeValue('returned_place_id');

		if (is_null($id)) {
			return null;
		}

		return ReturnedPlaces::find($id);
	}

}
