<?php
namespace Uab\Http\Models\Generated;

use Uab\Events\AfterCreate\AfterCreate;
use Uab\Events\AfterEdit\AfterEdit;
use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use stdClass;

/**
 * Class MessageParticipants
 *
 * @property number id
 * @property number thread_id
 * @property number user_id
 * @property mixed last_read
 * @property mixed created_at
 * @property mixed updated_at
 * @property mixed deleted_at
 *
 * @package Uab\Http\Models\Generated
 */
class MessageParticipants extends BaseModel {
    use SoftDeletes;

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'modified_at';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'last_read' => null,
        'created_at' => null,
        'updated_at' => null,
        'deleted_at' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'thread_id' => 'integer',
		'user_id' => 'integer',
		'last_read' => 'timestamp',
		'created_at' => 'timestamp',
		'updated_at' => 'timestamp',
		'deleted_at' => 'timestamp'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'thread_id',
		'user_id',
		'last_read',
		'created_at',
		'updated_at',
		'deleted_at'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'message_participants';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'thread_id',
		'user_id',
		'last_read',
		'created_at',
		'updated_at',
		'deleted_at'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'last_read',
		'created_at',
		'updated_at',
		'deleted_at'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'threads' => 'thread_id',
		'users' => 'user_id'
    ];

	/**
	 * @return stdClass
	*/
	public function thread() {
		return DB::table('threads')->where(
			[
				'id' => $this->getAttributeValue('thread_id')
			]
		)->first();
	}

	/**
	 * @return stdClass
	*/
	public function user() {
		return DB::table('users')->where(
			[
				'id' => $this->getAttributeValue('user_id')
			]
		)->first();
	}

}
