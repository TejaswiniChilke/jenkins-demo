<?php
namespace Uab\Http\Models\Generated;

use Uab\Events\AfterCreate\AfterCreate;
use Uab\Events\AfterEdit\AfterEdit;
use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use stdClass;

/**
 * Class PlaylistsVideos
 *
 * @property number id
 * @property number playlist_id
 * @property number video_id
 * @property string created
 * @property string modified
 * @property number position
 *
 * @package Uab\Http\Models\Generated
 */
class PlaylistsVideos extends BaseModel {
    
    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'position' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'playlist_id' => 'integer',
		'video_id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime',
		'position' => 'integer'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'playlist_id',
		'video_id',
		'created',
		'modified',
		'position'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'playlists_videos';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'playlist_id',
		'video_id',
		'position'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'playlists' => 'playlist_id',
		'videos' => 'video_id'
    ];

	/**
	 * @return stdClass
	*/
	public function playlist() {
		return DB::table('playlists')->where(
			[
				'id' => $this->getAttributeValue('playlist_id')
			]
		)->first();
	}

	/**
	 * @return stdClass
	*/
	public function video() {
		return DB::table('videos')->where(
			[
				'id' => $this->getAttributeValue('video_id')
			]
		)->first();
	}

}
