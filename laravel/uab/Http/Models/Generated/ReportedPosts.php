<?php
namespace Uab\Http\Models\Generated;

use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use \Uab\Http\Models\Users;
use stdClass;

/**
 * Class ReportedPosts
 *
 * @property number id
 * @property string created
 * @property string modified
 * @property number user_id
 * @property number post_id
 * @property string verified_at
 * @property number verified_user_id
 * @property string deleted_at
 *
 * @package Uab\Http\Models\Generated
 */
class ReportedPosts extends BaseModel {
    use SoftDeletes;

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created' => null,
        'modified' => null,
        'user_id' => null,
        'verified_user_id' => null,
        'deleted_at' => null,
        'verified_at' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime',
		'user_id' => 'integer',
		'post_id' => 'integer',
		'verified_user_id' => 'integer',
		'verified_at' => 'datetime',
		'deleted_at' => 'datetime'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'created',
		'modified',
		'user_id',
		'post_id',
		'verified_user_id',
		'verified_at',
		'deleted_at'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'reported_posts';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'user_id',
		'post_id',
		'verified_user_id',
		'verified_at',
		'deleted_at'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified',
		'verified_at',
		'deleted_at'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'users' => 'user_id',
		'posts' => 'post_id',
		'verified_users' => 'verified_user_id'
    ];

	/**
	 * @return Users|null
	*/
	public function user() {
		return Users::query()
            ->where(
                [
                    'id' => $this->getAttributeValue('user_id')
                ]
            )->first();
	}

	/**
	 * @return \Uab\Http\Models\Posts
	*/
	public function post() {
		return \Uab\Http\Models\Posts::query()->where(
			[
				'id' => $this->getAttributeValue('post_id')
			]
		)->first();
	}

	/**
	 * @return Users
	*/
	public function verifiedUser() {
		return Users::query()->where(
			[
				'id' => $this->getAttributeValue('verified_user_id')
			]
		)->first();
	}

}
