<?php
namespace Uab\Http\Models\Generated;

use Uab\Events\AfterCreate\AfterCreate;
use Uab\Events\AfterEdit\AfterEdit;
use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use stdClass;

/**
 * Class AnalyticStateVisits
 *
 * @property number id
 * @property string created
 * @property string modified
 * @property number user_id
 * @property string to_state
 * @property string params
 * @property string transition_datetime
 * @property number seconds
 * @property string from_state
 * @property string deleted_at
 *
 * @package Uab\Http\Models\Generated
 */
class AnalyticStateVisits extends BaseModel {
    use SoftDeletes;

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created' => null,
        'modified' => null,
        'to_state' => '',
        'params' => '',
        'transition_datetime' => null,
        'seconds' => 0,
        'from_state' => '',
        'deleted_at' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime',
		'user_id' => 'integer',
		'to_state' => 'string',
		'params' => 'string',
		'transition_datetime' => 'datetime',
		'seconds' => 'decimal:12',
		'from_state' => 'string',
		'deleted_at' => 'datetime'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'created',
		'modified',
		'user_id',
		'to_state',
		'params',
		'transition_datetime',
		'seconds',
		'from_state',
		'deleted_at'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'analytic_state_visits';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'user_id',
		'to_state',
		'params',
		'transition_datetime',
		'seconds',
		'from_state',
		'deleted_at'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified',
		'transition_datetime',
		'deleted_at'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'users' => 'user_id'
    ];

	/**
	 * @return stdClass
	*/
	public function user() {
		return DB::table('users')->where(
			[
				'id' => $this->getAttributeValue('user_id')
			]
		)->first();
	}

}
