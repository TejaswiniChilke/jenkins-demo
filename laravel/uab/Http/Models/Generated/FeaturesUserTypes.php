<?php
namespace Uab\Http\Models\Generated;

use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use stdClass;

/**
 * Class FeaturesUserTypes
 *
 * @property number id
 * @property string created
 * @property string modified
 * @property number feature_id
 * @property number user_type_id
 * @property number position
 * @property string name
 * @property string sref
 * @property string icon
 * @property number start_week
 * @property number end_week
 * @property string socket
 *
 * @package Uab\Http\Models\Generated
 */
class FeaturesUserTypes extends BaseModel {
    
    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';
    


    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created' => null,
        'modified' => null,
        'feature_id' => null,
        'position' => 0,
        'sref' => null,
        'icon' => null,
        'start_week' => null,
        'end_week' => null,
        'socket' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime',
		'feature_id' => 'integer',
		'user_type_id' => 'integer',
		'position' => 'integer',
		'name' => 'string',
		'sref' => 'string',
		'icon' => 'string',
		'start_week' => 'integer',
		'end_week' => 'integer',
		'socket' => 'string'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'created',
		'modified',
		'feature_id',
		'user_type_id',
		'position',
		'name',
		'sref',
		'icon',
		'start_week',
		'end_week',
		'socket'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'features_user_types';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'feature_id',
		'user_type_id',
		'position',
		'name',
		'sref',
		'icon',
		'start_week',
		'end_week',
		'socket'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'features' => 'feature_id',
		'user_types' => 'user_type_id'
    ];

	/**
	 * @return stdClass
	*/
	public function feature() {
		return DB::table('features')->where(
			[
				'id' => $this->getAttributeValue('feature_id')
			]
		)->first();
	}

	/**
	 * @return stdClass
	*/
	public function userType() {
		return DB::table('user_types')->where(
			[
				'id' => $this->getAttributeValue('user_type_id')
			]
		)->first();
	}

}
