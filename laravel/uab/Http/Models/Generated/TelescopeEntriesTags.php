<?php
namespace Uab\Http\Models\Generated;

use Uab\Events\AfterCreate\AfterCreate;
use Uab\Events\AfterEdit\AfterEdit;
use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use stdClass;

/**
 * Class TelescopeEntriesTags
 *
 * @property mixed entry_uuid
 * @property string tag
 *
 * @package Uab\Http\Models\Generated
 */
class TelescopeEntriesTags extends BaseModel {
    
    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'modified_at';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [

    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'entry_uuid' => 'string',
		'tag' => 'string'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'entry_uuid',
		'tag'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'telescope_entries_tags';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'entry_uuid',
		'tag'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [

	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        
    ];

}
