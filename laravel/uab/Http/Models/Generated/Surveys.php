<?php
namespace Uab\Http\Models\Generated;

use Illuminate\Support\Collection;
use Uab\Events\AfterCreate\AfterCreate;
use Uab\Events\AfterEdit\AfterEdit;
use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use stdClass;

/**
 * Class Surveys
 *
 * @property number id
 * @property string name
 * @property string created
 * @property string updated
 * @property number week
 * @property boolean required_on_start
 * @property number image_id
 *
 * @package Uab\Http\Models\Generated
 */
class Surveys extends BaseModel {

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created' => null,
        'updated' => null,
        'week' => 0,
        'required_on_start' => 0,
        'image_id' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'name' => 'string',
		'created' => 'datetime',
		'updated' => 'datetime',
		'week' => 'integer',
		'required_on_start' => 'boolean',
		'image_id' => 'integer'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'name',
		'created',
		'updated',
		'week',
		'required_on_start',
		'image_id'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'surveys';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'name',
		'updated',
		'week',
		'required_on_start',
		'image_id'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'updated'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'images' => 'image_id',
        'survey_pages' => null
    ];

	/**
	 * @return stdClass
	*/
	public function image() {
		return DB::table('images')->where(
			[
				'id' => $this->getAttributeValue('image_id')
			]
		)->first();
	}

	/**
	 * @return Collection<\Uab\Http\Models\SurveyPages>
	*/
	public function surveyPages() {
		return \Uab\Http\Models\SurveyPages::query()->where(
			[
				'id' => $this->getAttributeValue('image_id')
			]
		)->get();
	}

}
