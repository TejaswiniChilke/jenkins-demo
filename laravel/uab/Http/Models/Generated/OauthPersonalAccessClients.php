<?php
namespace Uab\Http\Models\Generated;

use Uab\Http\Models\Base\BaseModel;
use DB;
use stdClass;

/**
 * Class OauthPersonalAccessClients
 *
 * @property number id
 * @property number client_id
 * @property mixed created_at
 * @property mixed updated_at
 *
 * @package Uab\Http\Models\Generated
 */
class OauthPersonalAccessClients extends BaseModel {

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'modified_at';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created_at' => null,
        'updated_at' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'client_id' => 'integer',
		'created_at' => 'timestamp',
		'updated_at' => 'timestamp'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'client_id',
		'created_at',
		'updated_at'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'oauth_personal_access_clients';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'client_id',
		'created_at',
		'updated_at'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created_at',
		'updated_at'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'clients' => 'client_id'
    ];

	/**
	 * @return stdClass
	*/
	public function client() {
		return DB::table('clients')->where(
			[
				'id' => $this->getAttributeValue('client_id')
			]
		)->first();
	}

}
