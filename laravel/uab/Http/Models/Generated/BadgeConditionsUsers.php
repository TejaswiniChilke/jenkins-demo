<?php
namespace Uab\Http\Models\Generated;

use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Uab\Http\Models\BadgeConditions;
use Uab\Http\Models\Users;

/**
 * Class Uab\BadgeConditionsUsers
 *
 * @property number id
 * @property string modified
 * @property string created
 * @property number badge_condition_id
 * @property number user_id
 * @property string deleted_at
 * @property string earned_at
 *
 * @package Uab\Http\Models\Generated
 */
class BadgeConditionsUsers extends BaseModel {
    use SoftDeletes;

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';


    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'modified' => null,
        'created' => null,
        'deleted_at' => null,
        'earned_at' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'modified' => 'datetime',
		'created' => 'datetime',
		'badge_condition_id' => 'integer',
		'user_id' => 'integer',
		'deleted_at' => 'datetime',
		'earned_at' => 'datetime'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'modified',
		'created',
		'badge_condition_id',
		'user_id',
		'deleted_at',
		'earned_at'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'badge_conditions_users';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'badge_condition_id',
		'user_id',
		'deleted_at',
		'earned_at'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'modified',
		'created',
		'deleted_at',
		'earned_at'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'badge_conditions' => 'badge_condition_id',
		'users' => 'user_id'
    ];

	/**
	 * @return BadgeConditions|null
	*/
	public function badgeCondition():?BadgeConditions {
		$id = $this->getAttributeValue('badge_condition_id');

		if (is_null($id)) {
			return null;
		}

		return BadgeConditions::query()->find($id);
	}

	/**
	 * @return Users|null
	*/
	public function user() {
		$id = $this->getAttributeValue('user_id');

		if (is_null($id)) {
			return null;
		}

		return Users::query()->find($id);
	}

}
