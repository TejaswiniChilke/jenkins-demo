<?php
namespace Uab\Http\Models\Generated;

use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use stdClass;

/**
 * Class CheckIns
 *
 * @property number id
 * @property string created_at
 * @property string modified_at
 * @property string deleted_at
 * @property number user_id
 * @property string object_id
 * @property string object_table
 * @property string message
 *
 * @package Uab\Http\Models\Generated
 */
class CheckIns extends BaseModel {
    use SoftDeletes;

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'modified_at';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created_at' => null,
        'modified_at' => null,
        'deleted_at' => null,
        'message' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'created_at' => 'datetime',
		'modified_at' => 'datetime',
		'deleted_at' => 'datetime',
		'user_id' => 'integer',
		'object_id' => 'string',
		'object_table' => 'string',
		'message' => 'string'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'created_at',
		'modified_at',
		'deleted_at',
		'user_id',
		'object_id',
		'object_table',
		'message'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'check_ins';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'created_at',
		'modified_at',
		'deleted_at',
		'user_id',
		'object_id',
		'object_table',
		'message'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created_at',
		'modified_at',
		'deleted_at'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'users' => 'user_id'
    ];

	/**
	 * @return Users|null
	*/
	public function user() {
		return \Uab\Http\Models\Users::query()->where(
			[
				'id' => $this->getAttributeValue('user_id')
			]
		)->first();
	}

}
