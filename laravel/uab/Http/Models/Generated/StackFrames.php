<?php
namespace Uab\Http\Models\Generated;

use Uab\Events\AfterCreate\AfterCreate;
use Uab\Events\AfterEdit\AfterEdit;
use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use stdClass;

/**
 * Class StackFrames
 *
 * @property number id
 * @property string created
 * @property string modified
 * @property number bug_report_id
 * @property string column_number
 * @property string file_name
 * @property string function_name
 * @property string line_number
 * @property number position
 *
 * @package Uab\Http\Models\Generated
 */
class StackFrames extends BaseModel {
    
    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created' => null,
        'modified' => null,
        'column_number' => null,
        'file_name' => null,
        'function_name' => null,
        'line_number' => null,
        'position' => 0
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime',
		'bug_report_id' => 'integer',
		'column_number' => 'string',
		'file_name' => 'string',
		'function_name' => 'string',
		'line_number' => 'string',
		'position' => 'integer'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'created',
		'modified',
		'bug_report_id',
		'column_number',
		'file_name',
		'function_name',
		'line_number',
		'position'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'stack_frames';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'bug_report_id',
		'column_number',
		'file_name',
		'function_name',
		'line_number',
		'position'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'bug_reports' => 'bug_report_id'
    ];

	/**
	 * @return stdClass
	*/
	public function bugReport() {
		return DB::table('bug_reports')->where(
			[
				'id' => $this->getAttributeValue('bug_report_id')
			]
		)->first();
	}

}
