<?php
namespace Uab\Http\Models\Generated;

use Uab\Http\Models\Base\BaseModel;

use Uab\Http\Models\Badges;
use Uab\Http\Models\BadgeConditionTypes;

/**
 * Class BadgeConditions
 *
 * @property number id
 * @property string created
 * @property string modified
 * @property number badge_id
 * @property string value
 * @property number days
 * @property number badge_condition_type_id
 * @property number repeat
 *
 * @package Uab\Http\Models\Generated
 */
class BadgeConditions extends BaseModel {

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';


    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created' => null,
        'modified' => null,
        'value' => null,
        'days' => 0,
        'repeat' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime',
		'badge_id' => 'integer',
		'value' => 'string',
		'days' => 'integer',
		'badge_condition_type_id' => 'integer',
		'repeat' => 'integer'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'created',
		'modified',
		'badge_id',
		'value',
		'days',
		'badge_condition_type_id',
		'repeat'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'badge_conditions';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'badge_id',
		'value',
		'days',
		'badge_condition_type_id',
		'repeat'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'badges' => 'badge_id',
		'badge_condition_types' => 'badge_condition_type_id'
    ];

	/**
	 * @return Badges|null
	*/
	public function badge() {
		$id = $this->getAttributeValue('badge_id');

		if (is_null($id)) {
			return null;
		}

		return Badges::query()->find($id);
	}

	/**
	 * @return BadgeConditionTypes|null
	*/
	public function badgeConditionType() {
		$id = $this->getAttributeValue('badge_condition_type_id');

		if (is_null($id)) {
			return null;
		}

		return BadgeConditionTypes::query()->find($id);
	}

}
