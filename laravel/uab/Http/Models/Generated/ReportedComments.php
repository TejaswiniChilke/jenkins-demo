<?php
namespace Uab\Http\Models\Generated;

use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Uab\Http\Models\Users;
use Uab\Http\Models\Comments;

/**
 * Class ReportedComments
 *
 * @property number id
 * @property string created
 * @property string modified
 * @property number user_id
 * @property number comment_id
 * @property number verified_user_id
 * @property string deleted_at
 * @property string verified_at
 *
 * @package Uab\Http\Models\Generated
 */
class ReportedComments extends BaseModel {
    use SoftDeletes;

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';


    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created' => null,
        'modified' => null,
        'user_id' => null,
        'verified_user_id' => null,
        'deleted_at' => null,
        'verified_at' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime',
		'user_id' => 'integer',
		'comment_id' => 'integer',
		'verified_user_id' => 'integer',
		'deleted_at' => 'datetime',
		'verified_at' => 'datetime'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'created',
		'modified',
		'user_id',
		'comment_id',
		'verified_user_id',
		'deleted_at',
		'verified_at'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'reported_comments';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'user_id',
		'comment_id',
		'verified_user_id',
		'deleted_at',
		'verified_at'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified',
		'deleted_at',
		'verified_at'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'users' => 'user_id',
		'comments' => 'comment_id',
		'verified_users' => 'verified_user_id'
    ];

	/**
	 * @return Users|null
	*/
	public function user() {
		$id = $this->getAttributeValue('user_id');

		if (is_null($id)) {
			return null;
		}

		return Users::query()->find($id);
	}

	/**
	 * @return Comments|null
	*/
	public function comment() {
		$id = $this->getAttributeValue('comment_id');

		if (is_null($id)) {
			return null;
		}

		return Comments::query()->find($id);
	}

	/**
	 * @return Users|null
	*/
	public function verifiedUser() {
		$id = $this->getAttributeValue('verified_user_id');

		if (is_null($id)) {
			return null;
		}

		return Users::query()->find($id);
	}

}
