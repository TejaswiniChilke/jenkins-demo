<?php
namespace Uab\Http\Models\Generated;

use Uab\Http\Models\Base\BaseModel;

use Uab\Http\Models\Images;
use Uab\Http\Models\Questions;
use Uab\Http\Models\SurveyPages;

/**
 * Class AnswerOptions
 *
 * @property number id
 * @property string answer
 * @property string created
 * @property string modified
 * @property number parent_question_id
 * @property number position
 * @property number image_id
 * @property number next_question_id
 * @property string next_question_function
 * @property string response
 * @property number next_survey_page_id
 *
 * @package Uab\Http\Models\Generated
 */
class AnswerOptions extends BaseModel {

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';


    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created' => null,
        'modified' => null,
        'position' => 0,
        'image_id' => null,
        'next_question_id' => null,
        'next_question_function' => null,
        'response' => null,
        'next_survey_page_id' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'answer' => 'string',
		'created' => 'datetime',
		'modified' => 'datetime',
		'parent_question_id' => 'integer',
		'position' => 'integer',
		'image_id' => 'integer',
		'next_question_id' => 'integer',
		'response' => 'string',
		'next_question_function' => 'string',
		'next_survey_page_id' => 'integer'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'answer',
		'created',
		'modified',
		'parent_question_id',
		'position',
		'image_id',
		'next_question_id',
		'response',
		'next_question_function',
		'next_survey_page_id'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'answer_options';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'answer',
		'parent_question_id',
		'position',
		'image_id',
		'next_question_id',
		'next_question_function',
		'response',
		'next_survey_page_id'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'parent_questions' => 'parent_question_id',
		'images' => 'image_id',
		'next_questions' => 'next_question_id',
		'next_survey_pages' => 'next_survey_page_id'
    ];

	/**
	 * @return Questions|null
	*/
	public function parentQuestion() {
		$id = $this->getAttributeValue('parent_question_id');

		if (is_null($id)) {
			return null;
		}

		return Questions::query()->find($id);
	}

	/**
	 * @return Images|null
	*/
	public function image() {
		$id = $this->getAttributeValue('image_id');

		if (is_null($id)) {
			return null;
		}

		return Images::query()->find($id);
	}

	/**
	 * @return Questions|null
	*/
	public function nextQuestion() {
		$id = $this->getAttributeValue('next_question_id');

		if (is_null($id)) {
			return null;
		}

		return Questions::query()->find($id);
	}

	/**
	 * @return SurveyPages|null
	*/
	public function nextSurveyPage() {
		$id = $this->getAttributeValue('next_survey_page_id');

		if (is_null($id)) {
			return null;
		}

		return SurveyPages::query()->find($id);
	}

}
