<?php
namespace Uab\Http\Models\Generated;

use Uab\Http\Models\Base\BaseModel;
use DB;
use stdClass;

/**
 * Class WebsocketsStatisticsEntries
 *
 * @property number id
 * @property string app_id
 * @property number peak_connection_count
 * @property number websocket_message_count
 * @property number api_message_count
 * @property mixed created_at
 * @property mixed updated_at
 *
 * @package Uab\Http\Models\Generated
 */
class WebsocketsStatisticsEntries extends BaseModel {

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'modified_at';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created_at' => null,
        'updated_at' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'app_id' => 'string',
		'peak_connection_count' => 'integer',
		'websocket_message_count' => 'integer',
		'api_message_count' => 'integer',
		'created_at' => 'timestamp',
		'updated_at' => 'timestamp'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'app_id',
		'peak_connection_count',
		'websocket_message_count',
		'api_message_count',
		'created_at',
		'updated_at'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'websockets_statistics_entries';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'app_id',
		'peak_connection_count',
		'websocket_message_count',
		'api_message_count',
		'created_at',
		'updated_at'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created_at',
		'updated_at'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'apps' => 'app_id'
    ];

	/**
	 * @return stdClass
	*/
	public function app() {
		return DB::table('apps')->where(
			[
				'id' => $this->getAttributeValue('app_id')
			]
		)->first();
	}

}
