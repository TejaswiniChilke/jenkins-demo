<?php
namespace Uab\Http\Models\Generated;

use Uab\Events\AfterCreate\AfterCreate;
use Uab\Events\AfterEdit\AfterEdit;
use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use stdClass;

/**
 * Class Hangouts
 *
 * @property number id
 * @property string created
 * @property string modified
 * @property string title
 * @property string description
 * @property string url
 * @property number project_id
 * @property string start
 * @property string end
 * @property number start_week
 * @property number end_week
 *
 * @package Uab\Http\Models\Generated
 */
class Hangouts extends BaseModel {
    
    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created' => null,
        'modified' => null,
        'title' => '',
        'description' => null,
        'url' => '',
        'start' => null,
        'end' => null,
        'start_week' => 0,
        'end_week' => 0
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime',
		'title' => 'string',
		'description' => 'string',
		'url' => 'string',
		'project_id' => 'integer',
		'start' => 'datetime',
		'end' => 'datetime',
		'start_week' => 'integer',
		'end_week' => 'integer'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'created',
		'modified',
		'title',
		'description',
		'url',
		'project_id',
		'start',
		'end',
		'start_week',
		'end_week'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'hangouts';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'title',
		'description',
		'url',
		'project_id',
		'start',
		'end',
		'start_week',
		'end_week'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified',
		'start',
		'end'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'projects' => 'project_id'
    ];

	/**
	 * @return stdClass
	*/
	public function project() {
		return DB::table('projects')->where(
			[
				'id' => $this->getAttributeValue('project_id')
			]
		)->first();
	}

}
