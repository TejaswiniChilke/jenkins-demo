<?php
namespace Uab\Http\Models\Generated;

use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class UspsAddresses
 *
 * @property number id
 * @property string created
 * @property string modified
 * @property string og_address_line_1
 * @property string og_address_line_2
 * @property string og_city
 * @property string og_state
 * @property string og_zipcode
 * @property string og_zipcode_suffix
 * @property string return_text
 * @property string attributes
 * @property string new_address_line_1
 * @property string new_address_line_2
 * @property string new_city
 * @property string new_state
 * @property string new_zipcode
 * @property string new_zipcode_suffix
 * @property string deleted_at
 *
 * @package Uab\Http\Models\Generated
 */
class UspsAddresses extends BaseModel {
    use SoftDeletes;

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';
    

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'og_address_line_1' => null,
        'og_address_line_2' => null,
        'og_city' => null,
        'og_state' => null,
        'og_zipcode' => null,
        'og_zipcode_suffix' => null,
        'return_text' => null,
        'attributes' => null,
        'new_address_line_1' => null,
        'new_address_line_2' => null,
        'new_city' => null,
        'new_state' => null,
        'new_zipcode' => null,
        'new_zipcode_suffix' => null,
        'deleted_at' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime',
		'og_address_line_1' => 'string',
		'og_address_line_2' => 'string',
		'og_city' => 'string',
		'og_state' => 'string',
		'og_zipcode' => 'string',
		'og_zipcode_suffix' => 'string',
		'return_text' => 'string',
		'attributes' => 'string',
		'new_address_line_1' => 'string',
		'new_address_line_2' => 'string',
		'new_city' => 'string',
		'new_state' => 'string',
		'new_zipcode' => 'string',
		'new_zipcode_suffix' => 'string',
		'deleted_at' => 'datetime'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'created',
		'modified',
		'og_address_line_1',
		'og_address_line_2',
		'og_city',
		'og_state',
		'og_zipcode',
		'og_zipcode_suffix',
		'return_text',
		'attributes',
		'new_address_line_1',
		'new_address_line_2',
		'new_city',
		'new_state',
		'new_zipcode',
		'new_zipcode_suffix',
		'deleted_at'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'usps_addresses';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'og_address_line_1',
		'og_address_line_2',
		'og_city',
		'og_state',
		'og_zipcode',
		'og_zipcode_suffix',
		'return_text',
		'attributes',
		'new_address_line_1',
		'new_address_line_2',
		'new_city',
		'new_state',
		'new_zipcode',
		'new_zipcode_suffix',
		'deleted_at'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified',
		'deleted_at'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        
    ];

}
