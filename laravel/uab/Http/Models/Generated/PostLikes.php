<?php
namespace Uab\Http\Models\Generated;

use Uab\Events\AfterCreate\AfterCreate;
use Uab\Events\AfterEdit\AfterEdit;
use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use stdClass;

/**
 * Class PostLikes
 *
 * @property number id
 * @property string created
 * @property string modified
 * @property number post_id
 * @property number user_id
 * @property mixed deleted_at
 *
 * @package Uab\Http\Models\Generated
 */
class PostLikes extends BaseModel {
    use SoftDeletes;

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created' => null,
        'modified' => null,
        'user_id' => null,
        'deleted_at' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime',
		'post_id' => 'integer',
		'user_id' => 'integer',
		'deleted_at' => 'timestamp'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'created',
		'modified',
		'post_id',
		'user_id',
		'deleted_at'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'post_likes';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'post_id',
		'user_id',
		'deleted_at'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified',
		'deleted_at'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'posts' => 'post_id',
		'users' => 'user_id'
    ];

	/**
	 * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|\Uab\Http\Models\Posts|null
	*/
	public function post() {
		return \Uab\Http\Models\Posts::query()->where(
			[
				'id' => $this->getAttributeValue('post_id')
			]
		)->first();
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|\Uab\Http\Models\Users
	*/
	public function user() {
		return \Uab\Http\Models\Users::query()->where(
			[
				'id' => $this->getAttributeValue('user_id')
			]
		)->first();
	}

}
