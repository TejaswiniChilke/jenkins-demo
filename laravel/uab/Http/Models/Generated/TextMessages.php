<?php
namespace Uab\Http\Models\Generated;

use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Uab\Http\Models\Users;

/**
 * Class TextMessages
 *
 * @property number id
 * @property number to_user_id
 * @property number from_user_id
 * @property string message
 * @property string created
 * @property string modified
 * @property string date_created
 * @property string date_sent
 * @property string date_updated
 * @property string direction
 * @property string error_code
 * @property string error_message
 * @property string price
 * @property string uri
 * @property string from_number
 * @property string num_media
 * @property string num_segments
 * @property string price_unit
 * @property string sid
 * @property string to_number
 * @property mixed deleted_at
 * @property string status
 * @property string sms_id
 * @property string account_sid
 * @property string message_sid
 *
 * @package Uab\Http\Models\Generated
 */
class TextMessages extends BaseModel {
    use SoftDeletes;

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';


    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'to_user_id' => null,
        'from_user_id' => null,
        'message' => null,
        'created' => null,
        'modified' => null,
        'date_created' => null,
        'date_sent' => null,
        'date_updated' => null,
        'direction' => null,
        'error_code' => null,
        'error_message' => null,
        'price' => null,
        'uri' => null,
        'from_number' => null,
        'num_media' => null,
        'num_segments' => null,
        'price_unit' => null,
        'sid' => null,
        'to_number' => null,
        'deleted_at' => null,
        'status' => null,
        'sms_id' => null,
        'account_sid' => null,
        'message_sid' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'to_user_id' => 'integer',
		'from_user_id' => 'integer',
		'message' => 'string',
		'created' => 'datetime',
		'modified' => 'datetime',
		'date_created' => 'datetime',
		'date_sent' => 'datetime',
		'date_updated' => 'datetime',
		'direction' => 'string',
		'error_code' => 'string',
		'error_message' => 'string',
		'price' => 'string',
		'uri' => 'string',
		'from_number' => 'string',
		'num_media' => 'string',
		'num_segments' => 'string',
		'price_unit' => 'string',
		'sid' => 'string',
		'to_number' => 'string',
		'deleted_at' => 'timestamp',
		'status' => 'string',
		'sms_id' => 'string',
		'account_sid' => 'string',
		'message_sid' => 'string'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'to_user_id',
		'from_user_id',
		'message',
		'created',
		'modified',
		'date_created',
		'date_sent',
		'date_updated',
		'direction',
		'error_code',
		'error_message',
		'price',
		'uri',
		'from_number',
		'num_media',
		'num_segments',
		'price_unit',
		'sid',
		'to_number',
		'deleted_at',
		'status',
		'sms_id',
		'account_sid',
		'message_sid'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'text_messages';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'to_user_id',
		'from_user_id',
		'message',
		'date_created',
		'date_sent',
		'date_updated',
		'direction',
		'error_code',
		'error_message',
		'price',
		'uri',
		'from_number',
		'num_media',
		'num_segments',
		'price_unit',
		'sid',
		'to_number',
		'deleted_at',
		'status',
		'sms_id',
		'account_sid',
		'message_sid'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified',
		'date_created',
		'date_sent',
		'date_updated',
		'deleted_at'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'to_users' => 'to_user_id',
		'from_users' => 'from_user_id'
    ];

	/**
	 * @return Users
	*/
	public function toUser() {
		$id = $this->getAttributeValue('to_user_id');

		if (is_null($id)) {
			return null;
		}

		return Users::find($id);
	}

	/**
	 * @return Users|null
	*/
	public function fromUser() {
		$id = $this->getAttributeValue('from_user_id');

		if (is_null($id)) {
			return null;
		}

		return Users::find($id);
	}

}
