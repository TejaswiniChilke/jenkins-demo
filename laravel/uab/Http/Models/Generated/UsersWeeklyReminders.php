<?php
namespace Uab\Http\Models\Generated;

use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use stdClass;

/**
 * Class UsersWeeklyReminders
 *
 * @property number id
 * @property string created
 * @property string modified
 * @property number user_id
 * @property number weekly_reminder_id
 * @property number week
 *
 * @package Uab\Http\Models\Generated
 */
class UsersWeeklyReminders extends BaseModel {
    
    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';
    


    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created' => null,
        'modified' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime',
		'user_id' => 'integer',
		'weekly_reminder_id' => 'integer',
		'week' => 'integer'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'created',
		'modified',
		'user_id',
		'weekly_reminder_id',
		'week'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users_weekly_reminders';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'user_id',
		'weekly_reminder_id',
		'week'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'users' => 'user_id',
		'weekly_reminders' => 'weekly_reminder_id'
    ];

	/**
	 * @return stdClass
	*/
	public function user() {
		return DB::table('users')->where(
			[
				'id' => $this->getAttributeValue('user_id')
			]
		)->first();
	}

	/**
	 * @return stdClass
	*/
	public function weeklyReminder() {
		return DB::table('weekly_reminders')->where(
			[
				'id' => $this->getAttributeValue('weekly_reminder_id')
			]
		)->first();
	}

}
