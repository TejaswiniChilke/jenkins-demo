<?php
namespace Uab\Http\Models\Generated;

use Uab\Events\AfterCreate\AfterCreate;
use Uab\Events\AfterEdit\AfterEdit;
use Uab\Http\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use stdClass;

/**
 * Class SettingsUserTypes
 *
 * @property number id
 * @property string value
 * @property number user_type_id
 * @property number setting_id
 * @property string created
 * @property string modified
 *
 * @package Uab\Http\Models\Generated
 */
class SettingsUserTypes extends BaseModel {
    
    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'created' => null,
        'modified' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'integer',
		'value' => 'string',
		'user_type_id' => 'integer',
		'setting_id' => 'integer',
		'created' => 'datetime',
		'modified' => 'datetime'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'value',
		'user_type_id',
		'setting_id',
		'created',
		'modified'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'settings_user_types';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'value',
		'user_type_id',
		'setting_id'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [
		'created',
		'modified'
	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [
        'user_types' => 'user_type_id',
		'settings' => 'setting_id'
    ];

	/**
	 * @return stdClass
	*/
	public function userType() {
		return DB::table('user_types')->where(
			[
				'id' => $this->getAttributeValue('user_type_id')
			]
		)->first();
	}

	/**
	 * @return stdClass
	*/
	public function setting() {
		return DB::table('settings')->where(
			[
				'id' => $this->getAttributeValue('setting_id')
			]
		)->first();
	}

}
