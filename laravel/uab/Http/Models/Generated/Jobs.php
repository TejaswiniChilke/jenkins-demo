<?php
namespace Uab\Http\Models\Generated;

use Uab\Http\Models\Base\BaseModel;

/**
 * Class Jobs
 *
 * @property mixed id
 * @property string queue
 * @property mixed payload
 * @property boolean attempts
 * @property number reserved_at
 * @property number available_at
 * @property number created_at
 *
 * @package Uab\Http\Models\Generated
 */
class Jobs extends BaseModel {

    /**
     * Timestamps used by the table
     */
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'modified_at';
    const DELETED_AT = 'deleted_at';

    /**
    * All default attributes
    *
    * @var array
    */
    protected $attributes = [
        'reserved_at' => null
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
		'id' => 'string',
		'queue' => 'string',
		'payload' => 'string',
		'attempts' => 'boolean',
		'reserved_at' => 'integer',
		'available_at' => 'integer',
		'created_at' => 'integer'
    ];

    /**
    * All columns
    *
    * @var array
    */
    protected $columns = [
		'id',
		'queue',
		'payload',
		'attempts',
		'reserved_at',
		'available_at',
		'created_at'
	];

    /**
     * The database table's primary key
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'jobs';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
		'queue',
		'payload',
		'attempts',
		'reserved_at',
		'available_at',
		'created_at'
	];

    /**
    * Date field attributes
    *
    * @var array
    */
    protected $dates = [

	];

    /**
    * List of all relationships (other_table => current_table)
    *
    * @var array
    */
    public $relationships = [

    ];

}
