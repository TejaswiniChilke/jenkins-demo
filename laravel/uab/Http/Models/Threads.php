<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Uab\Http\Models\Generated\Threads as GeneratedModel;

/**
 * Class Threads
 *
 * @package Uab\Http\Models
 */
class Threads extends GeneratedModel {
    use HasHashSlug;

    public $appends = [
        // 'images'
    ];

    public function getImagesAttribute() {
        $query = Threads::query()
            ->select(
                [
                    'users.image_id'
                ]
            )->where(
                [
                    'threads.id' => $this->id
                ]
            )->join(
                'messages',
                'messages.thread_id',
                '=',
                'threads.id'
            )->join(
                'message_participants',
                'message_participants.thread_id',
                '=',
                'threads.id'
            )->join(
                'users as u',
                'users.id',
                '=',
                'message_participants.user_id'
            )->orderBy(
                'messages.created_at',
                'desc'
            )->limit(1);

        $results = $query->get()->toArray();

        if (count($results) !== 0) {
            $imageId = $results[0]->image_id;

            if (!is_null($imageId)) {
                return Images::query()
                    ->where(
                        [
                            'id' => $imageId
                        ]
                    )->get();
            }
        }

        return null;
    }

    public function isUnread($userId):bool {
        try {
            $participant = $this->getParticipantFromUser($userId);

            if ($participant->last_read === null) {
                return true;
            }

            if (!is_null($this->updated_at)) {
                $lastRead = Carbon::parse($participant->last_read);
                $updatedAt = Carbon::parse($this->updated_at);

                if ($updatedAt->isAfter($lastRead)) {

                }
                return true;
            }
        } catch (ModelNotFoundException $e) {
            // @codeCoverageIgnore
            // do nothing
        }

        return false;
    }
}
