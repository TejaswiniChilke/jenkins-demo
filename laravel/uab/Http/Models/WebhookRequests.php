<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\WebhookRequests as GeneratedModel;

/**
 * Class WebhookRequests
 *
 * @package Uab\Http\Models
 */
class WebhookRequests extends GeneratedModel {
    use HasHashSlug;

}
