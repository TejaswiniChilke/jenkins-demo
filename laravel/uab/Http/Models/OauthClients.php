<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\OauthClients as GeneratedModel;

/**
 * Class OauthClients
 *
 * @package Uab\Http\Models
 */
class OauthClients extends GeneratedModel {
    use HasHashSlug;

}
