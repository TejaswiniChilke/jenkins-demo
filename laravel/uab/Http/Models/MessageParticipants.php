<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\MessageParticipants as GeneratedModel;

/**
 * Class MessageParticipants
 *
 * @package Uab\Http\Models
 */
class MessageParticipants extends GeneratedModel {
    use HasHashSlug;

}
