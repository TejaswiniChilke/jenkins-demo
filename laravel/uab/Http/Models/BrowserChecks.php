<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\BrowserChecks as GeneratedModel;

/**
 * Class BrowserChecks
 *
 * @package Uab\Http\Models
 */
class BrowserChecks extends GeneratedModel {
    use HasHashSlug;

}
