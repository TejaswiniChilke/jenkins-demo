<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Carbon\Carbon;
use DateTimeInterface;
use Uab\Http\Models\Traits\Users\AgeTrait;
use Uab\Http\Models\Traits\Users\DaysInStatusTrait;
use Uab\Http\Models\Traits\Users\FriendTrait;
use Uab\Http\Models\Traits\Users\LastActiveTrait;
use Hash;
use Exception;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\MustVerifyEmail;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Cmgmyr\Messenger\Traits\Messagable;
use Uab\Http\Models\Generated\Users as GeneratedModel;
use Uab\Providers\DurationServiceProvider;
use Uab\Providers\PhoneServiceProvider;
use Uab\Providers\UsersServiceProvider;

/**
 * Class Users
 *
 * @package Uab\Http\Models
 */
class Users extends GeneratedModel implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract {
    use AgeTrait;
    use DaysInStatusTrait;
    use HasHashSlug;
    use FriendTrait;
    use Authenticatable;
    use Authorizable;
    use CanResetPassword;
    use HasApiTokens;
    use LastActiveTrait;
    use Messagable;
    use MustVerifyEmail;
    use Notifiable;

    protected $appends = [
        'age',
        'days_in_status',
        'display_name',
        'full_name',
        'is_friend',
        'is_following',
        'is_followed_by',
        'last_active',
        'user_type_name'
    ];

    public $defaultDataTableColumns = [
        'email',
        'first_name',
        'last_name',
        'status',
        'days_in_status'
    ];

    protected $similarWeights = [
        'dob'           => 0.20,
        'email'         => 0.20,
        'first_name'    => 0.10,
        'last_name'     => 0.10,
        'mobile_number' => 0.20,
        'zipcode'       => 0.20,
    ];

    public $relationshipTypes = [
        'coaches'    => 'users',
        'from_users' => 'users',
        'to_users'   => 'users',
    ];

    protected $testColumn = 'username';
    protected $testKey = 'dusk-test';

    public function __construct(array $attributes = []) {
        parent::__construct($attributes);
    }

    /**
     * @param string $pass
     */
    public function setPasswordAttribute(string $pass) {
        $this->attributes['password'] = Hash::make($pass);
    }

    /**
     * @param string|null $number
     */
    public function setMobileNumberAttribute(?string $number) {
        if (!is_null($number) && is_string($number)) {
            try {
                $phoneService = new PhoneServiceProvider();
                $phone = $phoneService->formatNumber($number);

                if (!is_null($phone)) {
                    $number = $phone;
                }
            } catch (Exception $e) {

            }
        }

        $this->attributes['mobile_number'] = $number;
    }

    /**
     * @return string|null
     */
    public function getDisplayNameAttribute():?string {
        $userService = new UsersServiceProvider();

        $isAdmin = $userService->isAdmin();

        $settingName = $isAdmin ?
            'full_name' : 'nickname';

        switch ($settingName) {
            case 'full_name':
                return $this->getFullNameAttribute();
            case 'nickname':
                return $this->getAttributeValue('nickname');
            case 'username':
                return $this->getAttributeValue('username');
        }

        return null;
    }

    /**
     * @return string
     */
    public function getFullNameAttribute():string {
        $firstName = $this->getAttributeValue('first_name');
        if (is_null($firstName)) {
            $firstName = '';
        }

        $lastName = $this->getAttributeValue('last_name');
        if (is_null($lastName)) {
            $lastName = '';
        }

        return trim($firstName.' '.$lastName);
    }

    /**
     * @return int|null
     */
    public function getWeek():?int {
        $startTime = $this->getAttributeValue('start_time');

        if (!is_null($startTime)) {
            $durationService = new DurationServiceProvider();

            return $durationService->getWeek($startTime);
        }

        return null;
    }

    /**
     * @return string|null
     */
    public function getUserTypeNameAttribute():?string {
        $userType = $this->userType();
        if (!is_null($userType)) {
            return $userType->name;
        }

        return null;
    }

    /**
     * @param DateTimeInterface|string|null $date
     */
    public function setStartTimeAttribute($date) {
        $this->attributes['start_time'] = is_null($date) || empty($date) ? null : Carbon::parse($date);
    }

    /**
     * @param DateTimeInterface|string|null $date
     */
    public function setDobAttribute($date) {
        $this->attributes['dob'] = is_null($date) || empty($date) ? null : Carbon::parse($date);
    }

    public function findForPassport($username) {
        /** @var Users $user */
        $user = $this->query()->where('username', $username)->first();
        if (is_null($user)) {
            $user = $this->query()->where('email', $username)->first();
        }

        return $user;
    }

    public function setEmailAttribute($email) {
        if (is_string($email)) {
            $email = strtolower($email);
        }

        $this->attributes['email'] = $email;
    }
}
