<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\SettingsUsers as GeneratedModel;

/**
 * Class SettingsUsers
 *
 * @package Uab\Http\Models
 */
class SettingsUsers extends GeneratedModel {
    use HasHashSlug;

}
