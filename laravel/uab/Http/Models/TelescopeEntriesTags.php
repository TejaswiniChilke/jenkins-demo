<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\TelescopeEntriesTags as GeneratedModel;

/**
 * Class TelescopeEntriesTags
 *
 * @package Uab\Http\Models
 */
class TelescopeEntriesTags extends GeneratedModel {
    use HasHashSlug;

}
