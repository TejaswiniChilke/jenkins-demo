<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\AnalyticVideos as GeneratedModel;

/**
 * Class AnalyticVideos
 *
 * @package Uab\Http\Models
 */
class AnalyticVideos extends GeneratedModel {
    use HasHashSlug;

    protected $shouldQueue = true;

    public $defaultDataTableColumns = [
        'user_id',
        'video_id',
        'play_time',
        'stop_time',
        'watch_datetime'
    ];
}
