<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\EmailTemplates as GeneratedModel;

/**
 * Class EmailTemplates
 *
 * @package Uab\Http\Models
 */
class EmailTemplates extends GeneratedModel {
    use HasHashSlug;

}
