<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\Recurrings as GeneratedModel;

/**
 * Class Recurrings
 *
 * @package Uab\Http\Models
 */
class Recurrings extends GeneratedModel {
    use HasHashSlug;

}
