<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\UserStatusChanges as GeneratedModel;

/**
 * Class UserStatusChanges
 *
 * @package Uab\Http\Models
 */
class UserStatusChanges extends GeneratedModel {
    use HasHashSlug;

}
