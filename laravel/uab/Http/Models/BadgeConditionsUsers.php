<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\BadgeConditionsUsers as GeneratedModel;

/**
 * Class BadgeConditionsUsers
 *
 * @package Uab\Http\Models
 */
class BadgeConditionsUsers extends GeneratedModel {
    use HasHashSlug;

}
