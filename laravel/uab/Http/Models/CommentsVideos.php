<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\CommentsVideos as GeneratedModel;

/**
 * Class CommentsVideos
 *
 * @package Uab\Http\Models
 */
class CommentsVideos extends GeneratedModel {
    use HasHashSlug;

}
