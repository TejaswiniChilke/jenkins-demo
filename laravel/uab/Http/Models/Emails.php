<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Carbon\Carbon;
use Uab\Http\Models\Generated\Emails as GeneratedModel;

/**
 * Class Emails
 *
 * @package Uab\Http\Models
 */
class Emails extends GeneratedModel {
    use HasHashSlug;

    public $relationshipTypes = [
        'to_users'   => 'users',
        'from_users' => 'users'
    ];

    public function getSentAttribute() {
        $sent = $this->attributes['sent'];

        return is_null($sent) || empty($sent) ? null : Carbon::parse($sent);
    }

    public function setSentAttribute($sent) {
        $this->attributes['sent'] = is_null($sent) || empty($sent) ? null : Carbon::parse($sent);
    }
}
