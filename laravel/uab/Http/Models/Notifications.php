<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\Notifications as GeneratedModel;

/**
 * Class Notifications
 *
 * @package Uab\Http\Models
 */
class Notifications extends GeneratedModel {
    use HasHashSlug;

    public $defaultDataTableColumns = [
        'id',
        'user_id',
        'message',
        'has_read'
    ];
}
