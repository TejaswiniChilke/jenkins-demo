<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\FeaturesUserTypes as GeneratedModel;

/**
 * Class FeaturesUserTypes
 *
 * @package Uab\Http\Models
 */
class FeaturesUserTypes extends GeneratedModel {
    use HasHashSlug;

    /**
     * @return Features|null
     */
    private function getParentFeature() {
        return Features::where(
            [
                'id' => $this->getAttributeValue('feature_id')
            ]
        )->first();
    }

    public function getIconAttribute() {
        $icon = $this->attributes['icon'];

        if (isset($parent->sref) && !is_null($icon)) {
            $parent = $this->getParentFeature();

            if (isset($parent->icon) && !is_null($parent->icon)) {
                $icon = $parent->icon;
            }
        }

        return $icon;
    }

    public function getNameAttribute() {
        $name = $this->attributes['name'];

        if (is_null($name)) {
            $parent = $this->getParentFeature();

            if (isset($parent->name) && !is_null($parent->name)) {
                $name = $parent->name;
            }
        }

        return $name;
    }

    public function getSrefAttribute() {
        $sref = $this->attributes['sref'];

        if (is_null($sref)) {
            $parent = $this->getParentFeature();

            if (isset($parent->sref) && !is_null($parent->sref)) {
                $sref = $parent->sref;
            }
        }

        return $sref;
    }

    public function setIconAttribute($icon) {
        if (is_null($icon)) {
            $parent = $this->getParentFeature();

            if (isset($parent->sref) && !is_null($parent->icon)) {
                $icon = $parent->icon;
            }
        }

        $this->attributes['icon'] = $icon;
    }

    public function setNameAttribute($name) {
        if (is_null($name)) {
            $parent = $this->getParentFeature();

            if (isset($parent->name) && !is_null($parent->name)) {
                $name = $parent->name;
            }
        }

        $this->attributes['name'] = $name;
    }

    public function setSrefAttribute($sref) {
        if (is_null($sref)) {
            $parent = $this->getParentFeature();

            if (isset($parent->sref) && !is_null($parent->sref)) {
                $sref = $parent->sref;
            }
        }

        $this->attributes['sref'] = $sref;
    }
}
