<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\SecurityQuestions as GeneratedModel;

/**
 * Class SecurityQuestions
 *
 * @package Uab\Http\Models
 */
class SecurityQuestions extends GeneratedModel {
    use HasHashSlug;

    public $defaultDataTableColumns = [
        'id',
        'question'
    ];
}
