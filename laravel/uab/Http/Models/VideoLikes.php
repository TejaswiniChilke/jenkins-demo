<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\VideoLikes as GeneratedModel;

/**
 * Class VideoLikes
 *
 * @package Uab\Http\Models
 */
class VideoLikes extends GeneratedModel {
    use HasHashSlug;

}
