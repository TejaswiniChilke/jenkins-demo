<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\TelescopeMonitorings as GeneratedModel;

/**
 * Class TelescopeMonitoring
 *
 * @package Uab\Http\Models
 */
class TelescopeMonitorings extends GeneratedModel {
    use HasHashSlug;

}
