<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\IpLocations as GeneratedModel;

/**
 * Class IpLocations
 *
 * @package Uab\Http\Models
 */
class IpLocations extends GeneratedModel {
    use HasHashSlug;

}
