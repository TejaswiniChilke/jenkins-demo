<?php
namespace Uab\Http\Models\Base;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Uab\Events\AfterCreate\AfterCreate;
use Balping\HashSlug\HasHashSlug;
use Illuminate\Notifications\Notifiable;
use Uab\Events\BeforeCreate\BeforeCreate;
use Uab\Events\AfterEdit\AfterEdit;
use Uab\Events\BeforeEdit\BeforeEdit;
use Uab\Http\Models\Traits\DataTableTrait;
use Uab\Scopes\IsTestScope;

/**
 * Class BaseModel
 *
 * @property mixed id
 * @property mixed created
 * @property mixed modified
 *
 * @package App\Http\Models\Base
 */
class BaseModel extends Model {
    use DataTableTrait;
    use HasHashSlug;
    use Notifiable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = '';

    protected $childKeys = [];

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $columns = [];

    protected $fillable = [];

    protected $attributes = [];

    protected $dates = [];

    protected $relationships = [];

    protected $relationshipTypes = [];

    protected $shouldQueue = false;

    protected $similarWeights;

    protected $testColumn = null;
    protected $testKey = 'Dusk Test';

    protected static function boot() {
        parent::boot();

        static::addGlobalScope(new IsTestScope());
    }

    public function getTestColumn() {
        return $this->testColumn;
    }

    public function getTestKey() {
        return $this->testKey;
    }

    public static function forceTruncate() {
        return self::query()->whereNotNull('id')->forceDelete();
    }

    public function getChildKey($childTable) {
        if (array_key_exists($childTable, $this->childKeys)) {
            return $this->childKeys[$childTable];
        }

        return null;
    }

    public function getColumns() {
        return $this->columns;
    }

    public function getSimilarWeights() {
        if (!isset($this->similarWeights)) {
            $columns = $this->getColumns();

            $count = count($columns) - count($this->getHidden());

            $weight = 1 / $count;

            $this->similarWeights = [];
            foreach ($columns as $column) {
                $this->similarWeights[$column] = $weight;
            }
        }

        return $this->similarWeights;
    }

    public function getRelationship($relation) {
        $relationships = $this->getRelationships();

        if (array_key_exists($relation, $relationships)) {
            return $relationships[$relation];
        }

        return null;
    }

    public function getRelationships() {
        return $this->relationships;
    }

    public function getRelationshipType($relationship) {
        if (array_key_exists($relationship, $this->relationshipTypes)) {
            return $this->relationshipTypes[$relationship];
        } else {
            return $relationship;
        }
    }

    /**
     * @param string $column
     *
     * @return string|null
     */
    public function getRelatedTable($column) {
        $relationships = $this->getRelationships();

        foreach ($relationships as $table => $relationship) {
            if (strtolower($relationship) === strtolower($column)) {
                return $table;
            }
        }

        return null;
    }

    public function getRelationshipTypes() {
        return $this->relationshipTypes;
    }

    public function getVirtualFields() {
        return $this->appends;
    }

    public function hasColumn($column) {
        $column = strtolower($column);

        $columns = $this->getColumns();

        foreach ($columns as $candidate) {
            $candidate = strtolower($candidate);

            if ($column === $candidate) {
                return true;
            }
        }

        return false;
    }

    public function hasRelationship($relationship) {
        $relationships = $this->getRelationships();

        return Arr::has($relationships, $relationship);
    }

    public function isVirtualField($column) {
        $virtualFields = $this->getVirtualFields();

        foreach ($virtualFields as $virtualField) {
            if (strtolower($virtualField) === strtolower($column)) {
                return true;
            }
        }

        return false;
    }

    public function isRelationship($column) {
        $relationships = $this->getRelationships();

        foreach ($relationships as $relationship) {
            if (strtolower($relationship) === strtolower($column)) {
                return true;
            }
        }

        return false;
    }

    public function safeDecode($slug = null) {
        if (is_null($slug)) {
            $slug = $this->slug();
        }

        $id = $this->decodeSlug($slug);

        return is_null($id) ? $slug : $id;
    }

    /**
     * Decodes slug to id
     * @param  string $slug
     * @return int|null
     */
    public static function safeDecodeStatic($slug) {
        $id = self::decodeSlug($slug);

        return is_null($id) ? $slug : $id;
    }

    public function save(array $options = []) {
        $alreadyExists = $this->exists;

        $dirty = [];

        if ($alreadyExists) {
            $dirty = $this->getDirty();

            if (!array_key_exists('BEFORE_EDIT', $options) || $options['BEFORE_EDIT'] !== false) {
                $eventClass = 'Uab\\Events\\BeforeEdit\\'.Str::studly($this->table).'BeforeEdit';

                if (class_exists($eventClass)) {
                    event(new BeforeEdit($this));
                }
            }
        } else {
            if (!array_key_exists('BEFORE_CREATE', $options) || $options['BEFORE_CREATE'] !== false) {
                $eventClass = 'Uab\\Events\\BeforeCreate\\'.Str::studly($this->table).'BeforeCreate';

                if (class_exists($eventClass)) {
                    event(new BeforeCreate($this));
                }
            }
        }

        $response = parent::save($options);

        if ($response) {
            if ($alreadyExists) {
                if (!array_key_exists('AFTER_EDIT', $options) || $options['AFTER_EDIT'] !== false) {
                    $eventClass = 'Uab\\Events\\AfterEdit\\'.Str::studly($this->table).'AfterEdit';

                    if (class_exists($eventClass)) {
                        event(new AfterEdit($this, $dirty));
                    }
                }
            } else {
                if (!array_key_exists('AFTER_CREATE', $options) || $options['AFTER_CREATE'] !== false) {
                    $eventClass = 'Uab\\Events\\AfterCreate\\'.Str::studly($this->table).'AfterCreate';

                    if (class_exists($eventClass)) {
                        event(new AfterCreate($this));
                    }
                }
            }
        }

        return $response;
    }

    public function shouldQueue() {
        return $this->shouldQueue;
    }

    public function resolveChildRouteBinding($childType, $value, $field) {
        // TODO: Implement resolveChildRouteBinding() method.
    }
}
