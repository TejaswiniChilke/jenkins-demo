<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\OauthAccessTokens as GeneratedModel;

/**
 * Class OauthAccessTokens
 *
 * @package Uab\Http\Models
 */
class OauthAccessTokens extends GeneratedModel {
    use HasHashSlug;

}
