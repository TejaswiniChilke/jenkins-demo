<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\SqlLogs as GeneratedModel;

/**
 * Class SqlLogs
 *
 * @package Uab\Http\Models
 */
class SqlLogs extends GeneratedModel {
    use HasHashSlug;

    public $defaultDataTableColumns = [
        'user_id',
        'query',
        'executed_at',
        'created'
    ];
}
