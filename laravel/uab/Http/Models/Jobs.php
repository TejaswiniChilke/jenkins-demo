<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\Jobs as GeneratedModel;

/**
 * Class Jobs
 *
 * @package Uab\Http\Models
 */
class Jobs extends GeneratedModel {
    use HasHashSlug;

}
