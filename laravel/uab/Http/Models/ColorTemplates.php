<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\ColorTemplates as GeneratedModel;

/**
 * Class ColorTemplates
 *
 * @package Uab\Http\Models
 */
class ColorTemplates extends GeneratedModel {
    use HasHashSlug;

}
