<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Base\BaseModel;
use Uab\Http\Models\Generated\ReportedComments as GeneratedModel;

/**
 * Class ReportedComments
 *
 * @package Uab\Http\Models
 */
class ReportedComments extends GeneratedModel {
    use HasHashSlug;

    public $relationshipTypes = [
        'verified_users' => 'users',
    ];

    public $appends = [
        'referred_type',
        'referred_object',
    ];

    private $referredObject = null;

    public function getReferredTypeAttribute() {
        $object = $this->getReferredObjectAttribute();
        if (is_null($object)) {
            return null;
        }

        return $object->getTable();
    }

    /**
     * @return BaseModel|null
     */
    public function getReferredObjectAttribute() {
        if (!isset($this->referredObject)) {
            $comment = $this->comment();
            if (is_null($comment)) {
                $this->referredObject = null;
            } else {
                $post = $comment->getPosts()->first();
                if (is_null($post)) {
                    $video = $comment->getVideos()->first();
                    if (is_null($video)) {
                        $badge = $comment->getBadges()->first();
                        if (is_null($badge)) {
                            $article = $comment->getArticles()->first();
                            if (is_null($article)) {
                                $this->referredObject = null;
                            } else {
                                $this->referredObject = $article;
                            }
                        } else {
                            $this->referredObject = $badge;
                        }
                    } else {
                        $this->referredObject = $video;
                    }
                } else {
                    $this->referredObject = $post;
                }
            }
        }

        return $this->referredObject;
    }
}
