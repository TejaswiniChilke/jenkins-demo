<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\AnswerInputTypes as GeneratedModel;

/**
 * Class AnswerInputTypes
 *
 * @package Uab\Http\Models
 */
class AnswerInputTypes extends GeneratedModel {
    use HasHashSlug;

}
