<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\OauthAuthCodes as GeneratedModel;

/**
 * Class OauthAuthCodes
 *
 * @package Uab\Http\Models
 */
class OauthAuthCodes extends GeneratedModel {
    use HasHashSlug;

}
