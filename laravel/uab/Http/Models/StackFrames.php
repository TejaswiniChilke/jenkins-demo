<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\StackFrames as GeneratedModel;

/**
 * Class StackFrames
 *
 * @package Uab\Http\Models
 */
class StackFrames extends GeneratedModel {
    use HasHashSlug;

}
