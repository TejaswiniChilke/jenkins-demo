<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\BadgeConditionTypes as GeneratedModel;

/**
 * Class BadgeConditionTypes
 *
 * @package Uab\Http\Models
 */
class BadgeConditionTypes extends GeneratedModel {
    use HasHashSlug;

}
