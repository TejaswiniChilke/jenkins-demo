<?php
namespace Uab\Http\Models;

use Carbon\Carbon;
use Uab\Providers\Auth;
use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\Posts as GeneratedModel;

/**
 * Class Posts
 *
 * @package Uab\Http\Models
 */
class Posts extends GeneratedModel {
    use HasHashSlug;

    public $relationshipTypes = [
        'to_users'   => 'users',
        'from_users' => 'users'
    ];

    protected $appends = [
        'comment_count',
        'has_flagged',
        'has_liked',
        'like_count'
    ];

    public function getHasLikedAttribute() {
        $activeUser = Auth::user();

        if (!is_null($activeUser)) {
            return PostLikes::query()
                ->where(
                    [
                        'post_id' => $this->getAttributeValue('id'),
                        'user_id' => $activeUser->id
                    ]
                )->exists();
        }

        return false;
    }

    public function getHasFlaggedAttribute() {
        $activeUser = Auth::user();

        if (!is_null($activeUser)) {
            return ReportedPosts::query()
                ->where(
                    [
                        'post_id' => $this->getAttributeValue('id'),
                        'user_id' => $activeUser->id
                    ]
                )->exists();
        }

        return false;
    }

    public function getLikeCountAttribute() {
        return PostLikes::query()
            ->where(
                [
                    'post_id' => $this->getAttributeValue('id')
                ]
            )->count();
    }

    /**
     * @return int
     */
    public function getCommentCountAttribute() {
        return $this->getComments()->count();
    }

    public function getComments() {
        $commentIds = CommentsPosts::query()
            ->where(
                [
                    'post_id' => $this->getAttributeValue('id')
                ]
            )->get()->pluck('comment_id');

        return Comments::query()
            ->whereIn('id', $commentIds)
            ->get();
    }

    public function setStickiedAttribute($date) {
        $this->attributes['stickied'] = is_null($date) || empty($date) ? null : Carbon::parse($date);
    }
}
