<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\FailedJobs as GeneratedModel;

/**
 * Class FailedJobs
 *
 * @package Uab\Http\Models
 */
class FailedJobs extends GeneratedModel {
    use HasHashSlug;

}
