<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\Sections as GeneratedModel;

/**
 * Class Sections
 *
 * @package Uab\Http\Models
 */
class Sections extends GeneratedModel {
    use HasHashSlug;

}
