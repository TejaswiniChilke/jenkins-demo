<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\Questions as GeneratedModel;
use Uab\Providers\TemplateServiceProvider;

/**
 * Class Questions
 *
 * @package Uab\Http\Models
 */
class Questions extends GeneratedModel {
    use HasHashSlug;

    protected $childKeys = [
        'answer_options' => 'parent_question_id'
    ];

    public function getPhrasingAttribute($phrasing) {
        $service = new TemplateServiceProvider();

        $phrasing = $service->parse($phrasing);

        return $phrasing;
    }

    public function getNextQuestionFunctionAttribute($nextQuestionFunction) {
        $nextQuestionId = null;
        if (!is_null($nextQuestionFunction)) {
            $service = new TemplateServiceProvider();

            $parsed = $service->parse($nextQuestionFunction, $this);

            $nextQuestionId = intval($parsed);
        }

        return $nextQuestionId;
    }
}
