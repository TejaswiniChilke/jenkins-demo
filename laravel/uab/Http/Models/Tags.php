<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\Tags as GeneratedModel;

/**
 * Class Tags
 *
 * @package Uab\Http\Models
 */
class Tags extends GeneratedModel {
    use HasHashSlug;

    public $defaultDataTableColumns = [
        'id',
        'name'
    ];
}
