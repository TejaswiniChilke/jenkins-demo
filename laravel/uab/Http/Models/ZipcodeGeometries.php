<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\ZipcodeGeometries as GeneratedModel;

/**
 * Class ZipcodeGeometries
 *
 * @package Uab\Http\Models
 */
class ZipcodeGeometries extends GeneratedModel {
    use HasHashSlug;

    /**
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null|Zipcodes
     */
    public function zipcode() {
        return Zipcodes::query()->where('zipcode', '=', $this->key)->first();
    }
}
