<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\CheckIns as GeneratedModel;

/**
 * Class CheckIns
 *
 * @package Uab\Http\Models
 */
class CheckIns extends GeneratedModel {
    use HasHashSlug;

}
