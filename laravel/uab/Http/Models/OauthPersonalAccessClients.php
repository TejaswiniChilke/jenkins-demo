<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\OauthPersonalAccessClients as GeneratedModel;

/**
 * Class OauthPersonalAccessClients
 *
 * @package Uab\Http\Models
 */
class OauthPersonalAccessClients extends GeneratedModel {
    use HasHashSlug;

}
