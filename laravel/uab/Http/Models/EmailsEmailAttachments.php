<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\EmailsEmailAttachments as GeneratedModel;

/**
 * Class EmailsEmailAttachments
 *
 * @package Uab\Http\Models
 */
class EmailsEmailAttachments extends GeneratedModel {
    use HasHashSlug;

}
