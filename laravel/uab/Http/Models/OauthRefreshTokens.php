<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\OauthRefreshTokens as GeneratedModel;

/**
 * Class OauthRefreshTokens
 *
 * @package Uab\Http\Models
 */
class OauthRefreshTokens extends GeneratedModel {
    use HasHashSlug;

}
