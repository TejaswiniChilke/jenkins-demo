<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\UserTypes as GeneratedModel;

/**
 * Class UserTypes
 *
 * @package Uab\Http\Models
 */
class UserTypes extends GeneratedModel {
    use HasHashSlug;

    protected $testColumn = 'display_name';

    public $defaultDataTableColumns = [
        'id',
        'name'
    ];
}
