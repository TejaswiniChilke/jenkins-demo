<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\BadgesComments as GeneratedModel;

/**
 * Class BadgesComments
 *
 * @package Uab\Http\Models
 */
class BadgesComments extends GeneratedModel {
    use HasHashSlug;

}
