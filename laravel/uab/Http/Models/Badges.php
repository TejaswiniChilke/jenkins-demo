<?php
namespace Uab\Http\Models;

use Uab\Providers\Auth;
use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\Badges as GeneratedModel;

/**
 * Class Badges
 *
 * @package Uab\Http\Models
 */
class Badges extends GeneratedModel {
    use HasHashSlug;

    protected $appends = [
        'has_earned'
    ];

    public $defaultDataTableColumns = [
        'id',
        'title',
        'start_week',
        'end_week',
        'description'
    ];

    public function getHasEarnedAttribute() {
        $user = Auth::user();

        if (is_null($user)) {
            return null;
        }

        return BadgesUsers::query()
            ->where(
                [
                    'badge_id' => $this->getAttributeValue('id'),
                    'user_id'  => $user->id
                ]
            )->exists();
    }
}
