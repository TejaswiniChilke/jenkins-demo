<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\ArticlesTags as GeneratedModel;

/**
 * Class ArticlesTags
 *
 * @package Uab\Http\Models
 */
class ArticlesTags extends GeneratedModel {
    use HasHashSlug;

}
