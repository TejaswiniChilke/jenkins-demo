<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\GoogleAddresses as GeneratedModel;

/**
 * Class GoogleAddresses
 *
 * @package Uab\Http\Models
 */
class GoogleAddresses extends GeneratedModel {
    use HasHashSlug;

}
