<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\WeeklyReminders as GeneratedModel;

/**
 * Class WeeklyReminders
 *
 * @package Uab\Http\Models
 */
class WeeklyReminders extends GeneratedModel {
    use HasHashSlug;

}
