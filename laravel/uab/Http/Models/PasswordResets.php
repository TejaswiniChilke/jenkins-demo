<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\PasswordResets as GeneratedModel;

/**
 * Class PasswordResets
 *
 * @package Uab\Http\Models
 */
class PasswordResets extends GeneratedModel {
    use HasHashSlug;

}
