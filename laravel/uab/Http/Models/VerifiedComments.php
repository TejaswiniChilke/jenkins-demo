<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\VerifiedComments as GeneratedModel;

/**
 * Class VerifiedComments
 *
 * @package Uab\Http\Models
 */
class VerifiedComments extends GeneratedModel {
    use HasHashSlug;

}
