<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\BadgesUserTypes as GeneratedModel;

/**
 * Class BadgesUserTypes
 *
 * @package Uab\Http\Models
 */
class BadgesUserTypes extends GeneratedModel {
    use HasHashSlug;

}
