<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\PlaylistsVideos as GeneratedModel;

/**
 * Class PlaylistsVideos
 *
 * @package Uab\Http\Models
 */
class PlaylistsVideos extends GeneratedModel {
    use HasHashSlug;

}
