<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\Zipcodes as GeneratedModel;

/**
 * Class Zipcodes
 *
 * @package Uab\Http\Models
 */
class Zipcodes extends GeneratedModel {
    use HasHashSlug;

}
