<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\AnalyticTrackCalls as GeneratedModel;

/**
 * Class AnalyticTrackCalls
 *
 * @package Uab\Http\Models
 */
class AnalyticTrackCalls extends GeneratedModel {
    use HasHashSlug;

    protected $shouldQueue = true;
}
