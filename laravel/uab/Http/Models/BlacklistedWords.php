<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\BlacklistedWords as GeneratedModel;

/**
 * Class BlacklistedWords
 *
 * @package Uab\Http\Models
 */
class BlacklistedWords extends GeneratedModel {
    use HasHashSlug;

    public $defaultDataTableColumns = [
        'id',
        'word'
    ];
}
