<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\Settings as GeneratedModel;

/**
 * Class Settings
 *
 * @package Uab\Http\Models
 */
class Settings extends GeneratedModel {
    use HasHashSlug;

}
