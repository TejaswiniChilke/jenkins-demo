<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\Surveys as GeneratedModel;

/**
 * Class Surveys
 *
 * @package Uab\Http\Models
 */
class Surveys extends GeneratedModel {
    use HasHashSlug;

    public $defaultDataTableColumns = [
        'id',
        'name',
        'week',
        'required_on_start'
    ];
}
