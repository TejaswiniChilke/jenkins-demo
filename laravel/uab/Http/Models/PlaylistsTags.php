<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\PlaylistsTags as GeneratedModel;

/**
 * Class PlaylistsTags
 *
 * @package Uab\Http\Models
 */
class PlaylistsTags extends GeneratedModel {
    use HasHashSlug;

}
