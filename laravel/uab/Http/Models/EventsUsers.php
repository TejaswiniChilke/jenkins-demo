<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\EventsUsers as GeneratedModel;

/**
 * Class EventsUsers
 *
 * @package Uab\Http\Models
 */
class EventsUsers extends GeneratedModel {
    use HasHashSlug;

}
