<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\TextMessages as GeneratedModel;

/**
 * Class TextMessages
 *
 * @package Uab\Http\Models
 */
class TextMessages extends GeneratedModel {
    use HasHashSlug;

}
