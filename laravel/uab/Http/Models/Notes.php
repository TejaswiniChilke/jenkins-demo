<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\Notes as GeneratedModel;

/**
 * Class Notes
 *
 * @package Uab\Http\Models
 */
class Notes extends GeneratedModel {
    use HasHashSlug;

    public $relationshipTypes = [
        'participants' => 'users'
    ];

}
