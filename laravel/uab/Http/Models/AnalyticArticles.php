<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\AnalyticArticles as GeneratedModel;

/**
 * Class AnalyticArticles
 *
 * @package Uab\Http\Models
 */
class AnalyticArticles extends GeneratedModel {
    use HasHashSlug;

    protected $shouldQueue = true;

    public $defaultDataTableColumns = [
        'article_id',
        'user_id',
        'read_on',
        'read_duration'
    ];
}
