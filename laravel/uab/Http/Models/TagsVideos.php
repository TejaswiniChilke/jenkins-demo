<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\TagsVideos as GeneratedModel;

/**
 * Class TagsVideos
 *
 * @package Uab\Http\Models
 */
class TagsVideos extends GeneratedModel {
    use HasHashSlug;

}
