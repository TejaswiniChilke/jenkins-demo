<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\Images as GeneratedModel;

/**
 * Class Images
 *
 * @package Uab\Http\Models
 */
class Images extends GeneratedModel {
    use HasHashSlug;

}
