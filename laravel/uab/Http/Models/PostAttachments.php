<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\PostAttachments as GeneratedModel;
use Uab\Providers\PostAttachmentServiceProvider;

/**
 * Class PostAttachments
 *
 * @package Uab\Http\Models
 */
class PostAttachments extends GeneratedModel {
    use HasHashSlug;

    public $appends = [
        'reference_object',
        'unlocks_week'
    ];

    public function getReferenceObjectAttribute() {
        $service = new PostAttachmentServiceProvider();

        return $service->getReferenceObject($this);
    }

    public function getUnlocksWeekAttribute() {
        $service = new PostAttachmentServiceProvider();

        $object = $service->getReferenceObject($this);

        if (!is_null($object)) {
            if ($object->hasColumn('week')) {
                return $object->getAttributeValue('week');
            } else if ($object->hasColumn('start_week')) {
                return $object->getAttributeValue('start_week');
            }
        }

        return null;
    }
}
