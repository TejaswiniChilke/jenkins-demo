<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Illuminate\Database\Eloquent\Builder;
use Uab\Http\Models\Generated\BadgeConditions as GeneratedModel;
use Uab\Providers\ModelServiceProvider;
use Uab\Http\Enums\BadgeConditionTypesEnum as BadgeConditionTypesEnum;
use Uab\Providers\QueryServiceProvider;

/**
 * Class BadgeConditions
 *
 * @package Uab\Http\Models
 */
class BadgeConditions extends GeneratedModel {
    use HasHashSlug;

    protected $appends = [
        'object'
    ];

    /**
     * @return Builder|Articles|Playlists|Videos|null
     */
    public function getObjectAttribute() {
        $modelService = new ModelServiceProvider();

        $type = $this->badgeConditionType();
        if (!is_null($type)) {
            $type = $type->name;
        }

        $table = null;
        switch ($type) {
            case BadgeConditionTypesEnum::ARTICLE_ID:
                $table = 'articles';
                break;
            case BadgeConditionTypesEnum::PLAYLIST_ID:
                $table = 'playlists';
                break;
            case BadgeConditionTypesEnum::VIDEO_ID:
                $table = 'videos';
                break;
        }

        if (!is_null($table)) {
            $model = $modelService->getModel($table);

            $objectId = $model::safeDecodeStatic($this->value);

            $object = $model::query()->find($objectId);
            if (!is_null($object)) {
                $queryService = new QueryServiceProvider();

                return $queryService->slugIds($model, $object);
            }
        }

        return null;
    }
}
