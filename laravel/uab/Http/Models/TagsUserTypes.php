<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\TagsUserTypes as GeneratedModel;

/**
 * Class TagsUserTypes
 *
 * @package Uab\Http\Models
 */
class TagsUserTypes extends GeneratedModel {
    use HasHashSlug;

}
