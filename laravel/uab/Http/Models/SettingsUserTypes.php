<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\SettingsUserTypes as GeneratedModel;

/**
 * Class SettingsUserTypes
 *
 * @package Uab\Http\Models
 */
class SettingsUserTypes extends GeneratedModel {
    use HasHashSlug;

}
