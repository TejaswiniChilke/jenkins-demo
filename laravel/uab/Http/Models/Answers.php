<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\Answers as GeneratedModel;

/**
 * Class Answers
 *
 * @package Uab\Http\Models
 */
class Answers extends GeneratedModel {
    use HasHashSlug;

}
