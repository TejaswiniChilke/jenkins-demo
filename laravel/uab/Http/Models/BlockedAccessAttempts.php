<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\BlockedAccessAttempts as GeneratedModel;

/**
 * Class BlockedAccessAttempts
 *
 * @package Uab\Http\Models
 */
class BlockedAccessAttempts extends GeneratedModel {
    use HasHashSlug;

}
