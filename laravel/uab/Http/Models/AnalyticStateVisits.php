<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\AnalyticStateVisits as GeneratedModel;

/**
 * Class AnalyticStateVisits
 *
 * @package Uab\Http\Models
 */
class AnalyticStateVisits extends GeneratedModel {
    use HasHashSlug;

    protected $shouldQueue = true;

    public $defaultDataTableColumns = [
        'user_id',
        'from_state',
        'to_state',
        'seconds',
        'transition_datetime'
    ];
}
