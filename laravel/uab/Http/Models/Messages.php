<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\Messages as GeneratedModel;

/**
 * Class Messages
 *
 * @package Uab\Http\Models
 */
class Messages extends GeneratedModel {
    use HasHashSlug;

}
