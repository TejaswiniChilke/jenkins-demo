<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\DataChangeLogs as GeneratedModel;

/**
 * Class DataChangeLogs
 *
 * @package Uab\Http\Models
 */
class DataChangeLogs extends GeneratedModel {
    use HasHashSlug;

}
