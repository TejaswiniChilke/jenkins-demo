<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\UspsAddresses as GeneratedModel;

/**
 * Class UspsAddresses
 *
 * @package Uab\Http\Models
 */
class UspsAddresses extends GeneratedModel {
    use HasHashSlug;

}
