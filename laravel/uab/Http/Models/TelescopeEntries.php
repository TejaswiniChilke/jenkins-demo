<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\TelescopeEntries as GeneratedModel;

/**
 * Class TelescopeEntries
 *
 * @package Uab\Http\Models
 */
class TelescopeEntries extends GeneratedModel {
    use HasHashSlug;

}
