<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\YoutubeVideoInfos as GeneratedModel;

/**
 * Class YoutubeVideoInfos
 *
 * @package Uab\Http\Models
 */
class YoutubeVideoInfos extends GeneratedModel {
    use HasHashSlug;

}
