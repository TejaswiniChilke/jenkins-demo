<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Illuminate\Support\Collection;
use Uab\Http\Models\Generated\Comments as GeneratedModel;
use Uab\Providers\Auth;

/**
 * Class Comments
 *
 * @package Uab\Http\Models
 */
class Comments extends GeneratedModel {
    use HasHashSlug;

    protected $appends = [
        'has_flagged'
    ];

    public function getHasFlaggedAttribute() {
        $activeUser = Auth::user();

        if (!is_null($activeUser)) {
            return ReportedComments::where(
                [
                    'comment_id' => $this->getAttributeValue('id'),
                    'user_id'    => $activeUser->id
                ]
            )->exists();
        }

        return false;
    }

    /**
     * @return Articles[]|Collection
     */
    public function getArticles() {
        $commentArticles = ArticlesComments::query()
            ->where(
                [
                    'comment_id' => $this->id
                ]
            )->get();

        $commentArticleIds = $commentArticles->pluck('article_id');

        return Articles::query()
            ->whereIn('id', $commentArticleIds)
            ->get();
    }

    /**
     * @return Posts[]|Collection
     */
    public function getPosts() {
        $commentPosts = CommentsPosts::query()
            ->where(
                [
                    'comment_id' => $this->id
                ]
            )->get();

        $commentPostIds = $commentPosts->pluck('post_id');

        return Posts::query()
            ->whereIn('id', $commentPostIds)
            ->get();
    }

    /**
     * @return Badges[]|Collection
     */
    public function getBadges() {
        $commentBadges = BadgesComments::query()
            ->where(
                [
                    'comment_id' => $this->id
                ]
            )->get();

        $commentBadgeIds = $commentBadges->pluck('badge_id');

        return Badges::query()
            ->whereIn('id', $commentBadgeIds)
            ->get();
    }

    /**
     * @return Videos[]|Collection
     */
    public function getVideos() {
        $commentVideos = CommentsVideos::query()
            ->where(
                [
                    'comment_id' => $this->id
                ]
            )->get();

        $commentVideoIds = $commentVideos->pluck('video_id');

        return Videos::query()
            ->whereIn('id', $commentVideoIds)
            ->get();
    }
}
