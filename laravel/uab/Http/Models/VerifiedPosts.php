<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\VerifiedPosts as GeneratedModel;

/**
 * Class VerifiedPosts
 *
 * @package Uab\Http\Models
 */
class VerifiedPosts extends GeneratedModel {
    use HasHashSlug;

}
