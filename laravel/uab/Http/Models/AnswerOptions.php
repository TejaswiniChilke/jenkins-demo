<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\AnswerOptions as GeneratedModel;
use Uab\Providers\TemplateServiceProvider;

/**
 * Class AnswerOptions
 *
 * @package Uab\Http\Models
 */
class AnswerOptions extends GeneratedModel {
    use HasHashSlug;

    public $relationshipTypes = [
        'parent_questions' => 'questions',
        'next_questions' => 'questions',
        'next_survey_pages' => 'survey_pages'
    ];

    public function getNextQuestionIdAttribute($nextQuestionId) {
        if (is_null($nextQuestionId)) {
            $nextQuestionFunction = $this->getAttribute('next_question_function');

            if (!is_null($nextQuestionFunction)) {
                $service = new TemplateServiceProvider();

                $parsed = $service->parse($nextQuestionFunction);

                $nextQuestionId = intval($parsed);
            }
        }

        return $nextQuestionId;
    }

    public function getResponseAttribute($response) {
        $service = new TemplateServiceProvider();

        $response = $service->parse($response);

        return $response;
    }
}
