<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\ArticlesUsers as GeneratedModel;

/**
 * Class ArticlesUsers
 *
 * @package Uab\Http\Models
 */
class ArticlesUsers extends GeneratedModel {
    use HasHashSlug;

}
