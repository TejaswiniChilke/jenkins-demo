<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\Features as GeneratedModel;

/**
 * Class Features
 *
 * @package Uab\Http\Models
 */
class Features extends GeneratedModel {
    use HasHashSlug;

}
