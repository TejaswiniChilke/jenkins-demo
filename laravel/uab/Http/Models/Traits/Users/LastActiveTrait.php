<?php
namespace Uab\Http\Models\Traits\Users;

use Uab\Http\Models\AnalyticStateVisits;

trait LastActiveTrait {
    abstract public function getAttributeValue($key);

    public function getLastActiveAttribute() {
        $lastActive = null;

        /** @var AnalyticStateVisits|null $analytic */
        $analytic = AnalyticStateVisits::query()
            ->where(
                [
                    'user_id' =>  $this->getAttributeValue('id'),
                ]
            )->whereNotNull('transition_datetime')
            ->orderBy('transition_datetime', 'desc')
            ->first();

        if (!is_null($analytic)) {
            $lastActive = $analytic->transition_datetime;
        }

        return $lastActive;
    }
}
