<?php
namespace Uab\Http\Models\Traits\Users;

use Uab\Providers\Auth;
use Uab\Providers\FriendServiceProvider;

/**
 * Trait FriendTrait
 *
 * @package Uab\Http\Models\Traits\Users
 *
 * @property $id string
 */
trait FriendTrait {
    public function getIsFriendAttribute() {
        $friendService = new FriendServiceProvider();

        $activeUser = Auth::user();

        if (is_null($activeUser)) {
            return null;
        }

        return $friendService->isFriends($activeUser->id, $this->id);
    }

    public function getIsFollowingAttribute() {
        $friendService = new FriendServiceProvider();

        $activeUser = Auth::user();

        if (is_null($activeUser)) {
            return null;
        }

        return $friendService->isFollowing($activeUser->id, $this->id);
    }

    public function getIsFollowedByAttribute() {
        $friendService = new FriendServiceProvider();

        $activeUser = Auth::user();

        if (is_null($activeUser)) {
            return null;
        }

        return $friendService->isFollowedBy($activeUser->id, $this->id);
    }
}
