<?php
namespace Uab\Http\Models\Traits\Users;

use Carbon\Carbon;

trait AgeTrait {
    abstract public function getAttributeValue($key);

    public function getAgeAttribute() {
        $age = null;

        $dob = $this->getAttributeValue('dob');

        if (!is_null($dob)) {
            try {
                $dob = new Carbon($dob);

                $age = $dob->diffInYears();
            } catch (\Exception $e) {

            }
        }

        return $age;
    }
}
