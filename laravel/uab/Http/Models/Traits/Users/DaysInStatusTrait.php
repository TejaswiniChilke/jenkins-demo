<?php
namespace Uab\Http\Models\Traits\Users;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Uab\Http\Models\UserStatusChanges;

trait DaysInStatusTrait {
    abstract public function getAttributeValue($key);

    /**
     * @return int|null
     */
    public function getDaysInStatusAttribute():?int {
        /** @var UserStatusChanges[]|Collection $statusHistory */
        $query = UserStatusChanges::query()
            ->where(
                [
                    'user_id' => $this->getAttributeValue('id')
                ]
            )->orderBy('created', 'desc')
            ->limit(2);

        /** @var UserStatusChanges[]|Collection $statusHistory */
        $statusHistories = $query->get();

        $currentStatus = $statusHistories->get(0);
        $previousStatus = $statusHistories->get(1);

        $statusStart = null;
        if (is_null($currentStatus) || is_null($currentStatus->created)) {
            $statusStart = $this->created;
        } else {
            $statusStart = $currentStatus->created;
        }

        if (is_null($statusStart)) {
            return null;
        }

        $statusEnd = null;
        if (is_null($previousStatus) || is_null($previousStatus->created)) {
            $statusEnd = $this->created;
        } else {
            $statusEnd = $previousStatus->created;
        }

        if (is_null($statusEnd)) {
            return null;
        }

        return Carbon::parse($statusEnd)->startOfDay()->daysUntil(Carbon::parse($statusStart)->startOfDay())->count();
    }
}
