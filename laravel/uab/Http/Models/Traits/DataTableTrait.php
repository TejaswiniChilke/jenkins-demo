<?php
namespace Uab\Http\Models\Traits;

use stdClass;

/**
 * Trait DataTableTrait
 *
 * @property string[] $defaultDataTableColumns
 *
 * @package Uab\Http\Models\Traits
 */
trait DataTableTrait {
    abstract public function getCasts();
    abstract public function getRelatedTable($column);
    abstract public function getVirtualFields();
    abstract public function hasColumn($column);
    abstract public function isFillable($column);

    /**
     * @return array
     */
    public function getDataTableColumns():array {
        $columns = $this->getCasts();

        foreach ($columns as $column => $type) {
            $related = $this->getRelatedTable($column);

            if (!is_null($related)) {
                $columns[$column] = $related;
            }
        }

        $virtualFields = $this->getVirtualFields();

        foreach ($virtualFields as $virtualField) {
            $columns[$virtualField] = 'string';
        }

        $results = [];

        foreach ($columns as $column => $type) {
            $result = new stdClass();
            $result->name = $column;
            $result->type = $type;
            $result->editable = $this->isFillable($column);
            $result->enabled = $this->isDefaultDataColumn($column);
            $result->position = $this->getDataTablePosition($column);
            $result->orderable = $this->hasColumn($column);

            $results[] = $result;
        }

        return $results;
    }

    /**
     * @param string $column
     *
     * @return int
     */
    public function getDataTablePosition(string $column):int {
        if ($this->isDefaultDataColumn($column)) {
            $defaults = $this->getDefaultDataColumns();

            return array_search($column, $defaults) + 1;
        }

        return 0;
    }

    /**
     * @return array
     */
    public function getDefaultDataColumns():array {
        return $this->defaultDataTableColumns;
    }

    /**
     * @param string $column
     *
     * @return boolean
     */
    public function isDefaultDataColumn(string $column):bool {
        $defaults = $this->getDefaultDataColumns();

        if (!is_null($defaults)) {
            if (count($defaults) === 0) {
                return !in_array(
                    $column,
                    [
                        'created',
                        'created_at',
                        'deleted',
                        'deleted_at',
                        'modified',
                        'modified_at',
                    ]
                );
            } else {
                foreach ($defaults as $default) {
                    if (strtolower($default) === strtolower($column)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }
}
