<?php
namespace Uab\Http\Models\Traits\Videos;

use Uab\Http\Models\Tags;

trait TagCsv {
    abstract public function getAttributeValue($key);

    public function getTagCsvAttribute() {
        $query = Tags::query()
            ->join(
                'tags_videos',
                'tags_videos.tag_id',
                '=',
                'tags.id'
            )->where(
                [
                    'tags_videos.video_id' => $this->getAttributeValue('id')
                ]
            );

        $names = $query->get()->unique()->pluck('name')->toArray();

        return implode(',', $names);
    }
}
