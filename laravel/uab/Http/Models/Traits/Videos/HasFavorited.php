<?php
namespace Uab\Http\Models\Traits\Videos;

use Uab\Http\Models\FavoriteVideos;
use Uab\Providers\Auth;

trait HasFavorited {
    abstract public function getAttributeValue($key);

    public function getHasFavoritedAttribute() {
        $activeUser = Auth::user();

        if (!is_null($activeUser)) {
            return FavoriteVideos::query()
                ->where(
                    [
                        'video_id' => $this->getAttributeValue('id'),
                        'user_id'  => $activeUser->id
                    ]
                )->exists();
        }

        return false;
    }
}
