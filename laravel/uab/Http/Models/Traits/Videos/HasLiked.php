<?php
namespace Uab\Http\Models\Traits\Videos;

use Uab\Http\Models\VideoLikes;
use Uab\Providers\Auth;

trait HasLiked {
    abstract public function getAttributeValue($key);

    public function getHasLikedAttribute() {
        $activeUser = Auth::user();

        if (!is_null($activeUser)) {
            return VideoLikes::query()
                ->where(
                    [
                        'video_id' => $this->getAttributeValue('id'),
                        'user_id'  => $activeUser->id
                    ]
                )->exists();
        }

        return false;
    }
}
