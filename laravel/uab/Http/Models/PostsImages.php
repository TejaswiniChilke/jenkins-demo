<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\PostsImages as GeneratedModel;

/**
 * Class PostsImages
 *
 * @package Uab\Http\Models
 */
class PostsImages extends GeneratedModel {
    use HasHashSlug;

}
