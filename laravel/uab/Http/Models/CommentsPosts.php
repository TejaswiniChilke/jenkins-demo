<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\CommentsPosts as GeneratedModel;

/**
 * Class CommentsPosts
 *
 * @package Uab\Http\Models
 */
class CommentsPosts extends GeneratedModel {
    use HasHashSlug;

}
