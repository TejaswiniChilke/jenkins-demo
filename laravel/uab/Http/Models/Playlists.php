<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Illuminate\Database\Eloquent\Builder;
use Uab\Http\Models\Generated\Playlists as GeneratedModel;

/**
 * Class Playlists
 *
 * @package Uab\Http\Models
 */
class Playlists extends GeneratedModel {
    use HasHashSlug;

    protected $appends = [
        'tag_csv',
        'video_count'
    ];

    protected $defaultDataTableColumns = [
        'title',
        'week',
        'end_week',
        'video_count',
        'tag_csv'
    ];

    public function getImageIdAttribute($imageId) {
        if (isset($imageId)) {
            return $imageId;
        }

        $id = $this->getAttribute('id');

        if (!is_null($id)) {
            /** @var Builder $query */
            $query = Videos::query()
                ->select('videos.*')->join(
                    'playlists_videos',
                    'playlists_videos.video_id',
                    '=',
                    'videos.id'
                )->where(
                    [
                        'playlists_videos.playlist_id' => $id
                    ]
                )->orderBy('playlists_videos.position');

            $videos = $query->get();

            foreach ($videos as $video) {
                if (isset($video->image_id)) {
                    return $video->image_id;
                }
            }
        }

        return null;
    }

    public function getTagCsvAttribute() {
        /** @var Builder $query */
        $query = Tags::join(
            'playlists_tags',
            'playlists_tags.tag_id',
            '=',
            'tags.id'
        );
        $query->where(
            [
                'playlists_tags.playlist_id' => $this->getAttributeValue('id')
            ]
        );

        $names = $query->get()->unique()->pluck('name')->toArray();

        return implode(',', $names);
    }

    public function getVideoCountAttribute() {
        return PlaylistsVideos::where(
            [
                'playlist_id' => $this->getAttributeValue('id')
            ]
        )->count();
    }
}
