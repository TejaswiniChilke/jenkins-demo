<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\EmailAttachments as GeneratedModel;

/**
 * Class EmailAttachments
 *
 * @package Uab\Http\Models
 */
class EmailAttachments extends GeneratedModel {
    use HasHashSlug;

}
