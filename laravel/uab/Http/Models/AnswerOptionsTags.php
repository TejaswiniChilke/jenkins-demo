<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\AnswerOptionsTags as GeneratedModel;

/**
 * Class AnswerOptionsTags
 *
 * @package Uab\Http\Models
 */
class AnswerOptionsTags extends GeneratedModel {
    use HasHashSlug;

}
