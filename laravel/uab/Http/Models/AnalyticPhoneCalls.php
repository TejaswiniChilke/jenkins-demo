<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\AnalyticPhoneCalls as GeneratedModel;

/**
 * Class AnalyticPhoneCalls
 *
 * @package Uab\Http\Models
 */
class AnalyticPhoneCalls extends GeneratedModel {
    use HasHashSlug;

    public $relationships = [
        'to_users' => 'users',
        'from_users' => 'users'
    ];
}
