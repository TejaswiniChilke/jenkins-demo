<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\AddressValidations as GeneratedModel;

/**
 * Class AddressValidations
 *
 * @package Uab\Http\Models
 */
class AddressValidations extends GeneratedModel {
    use HasHashSlug;

}
