<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\ImagesVideos as GeneratedModel;

/**
 * Class ImagesVideos
 *
 * @package Uab\Http\Models
 */
class ImagesVideos extends GeneratedModel {
    use HasHashSlug;

}
