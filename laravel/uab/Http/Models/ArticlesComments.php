<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\ArticlesComments as GeneratedModel;

/**
 * Class ArticlesComments
 *
 * @package Uab\Http\Models
 */
class ArticlesComments extends GeneratedModel {
    use HasHashSlug;

}
