<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\EmailTemplateLogs as GeneratedModel;

/**
 * Class EmailTemplateLogs
 *
 * @package Uab\Http\Models
 */
class EmailTemplateLogs extends GeneratedModel {
    use HasHashSlug;

}
