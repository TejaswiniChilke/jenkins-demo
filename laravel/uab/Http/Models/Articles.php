<?php
namespace Uab\Http\Models;

use Balping\HashSlug\HasHashSlug;
use Uab\Http\Models\Generated\Articles as GeneratedModel;

/**
 * Class Articles
 *
 * @package Uab\Http\Models
 */
class Articles extends GeneratedModel {
    use HasHashSlug;

    protected $testColumn = 'title';

    public $relationshipTypes = [
        'content_images' => 'images',
        'images' => 'images'
    ];

    public $appends = [
        'has_image',
        'has_pdf'
    ];

    public $defaultDataTableColumns = [
        'id',
        'title',
        'week',
        'end_week',
        'description'
    ];

    private function isPdf() {
        $image = $this->contentImage();
        if (isset($image->file_path)) {
            $fileName = $image->file_path;

            $matches = [];
            preg_match('/(?:\.([^.]+))?$/', $fileName, $matches);

            if (is_array($matches)) {
                foreach ($matches as $match) {
                    if  (strstr($match, 'pdf')) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public function getHasImageAttribute() {
        if ($this->content_image_id !== null) {
            return !$this->isPdf();
        }

        return false;
    }

    public function getHasPdfAttribute() {
        if ($this->content_image_id !== null) {
            return $this->isPdf();
        }

        return false;
    }
}
