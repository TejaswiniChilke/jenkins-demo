<?php
namespace Uab\Http\Enums;

class UserTypesEnum {
    const ADMIN = 'admin';
    const CONTROL = 'control';
    const M2M = 'm2m';
    const PARTICIPANT = 'participant';
    const SET = 'set';
    const SUPER_ADMIN = 'super-admin';

    const SAME_GENDER = 'same-gender';
    const OPPOSITE_GENDER = 'opposite-gender';
    const RANDOM_GENDER = 'random-gender';
}
