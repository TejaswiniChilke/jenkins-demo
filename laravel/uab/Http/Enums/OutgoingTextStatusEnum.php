<?php
namespace Uab\Http\Enums;

class OutgoingTextStatusEnum {
    const ACCEPTED = 'accepted';
    const DELIVERED = 'delivered';
    const FAILED = 'failed';
    const QUEUED = 'queued';
    const READ = 'read';
    const RECEIVED = 'received';
    const RECEIVING = 'receiving';
    const SENDING = 'sending';
    const SENT = 'sent';
    const UNDELIVERED = 'undelivered';
}
