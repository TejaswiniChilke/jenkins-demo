<?php
namespace Uab\Http\Enums;

class GenderEnums {
    const FEMALE = 'female';
    const MALE = 'male';
}
