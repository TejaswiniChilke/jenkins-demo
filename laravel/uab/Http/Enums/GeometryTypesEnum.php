<?php
namespace Uab\Http\Enums;

class GeometryTypesEnum {
    const POINT = 'Point';
    const POLYGON = 'Polygon';
}
