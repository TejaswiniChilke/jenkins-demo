<?php
namespace Uab\Http\Enums;

use Uab\Http\Enums\Traits\GetAllEnumsTrait;

class BadgeConditionTypesEnum {
    use GetAllEnumsTrait;

    const ARTICLE_ID = 'articleId';
    const PLAYLIST_ID = 'playlistId';
    const VIDEO_ID = 'videoId';

    const CALORIES = 'calories';
    const FLOORS = 'floors';
    const STEPS = 'steps';

    const CREATE_COMMENT = 'createComment';
    const CREATE_EVENT = 'createEvent';
    const CREATE_POST = 'createPostLike';

    const LIKE_VIDEO = 'createPost';
    const FAVORITE_VIDEO = 'favoriteVideo';

    const LOGIN = 'login';
}
