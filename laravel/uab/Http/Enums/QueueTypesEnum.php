<?php
namespace Uab\Http\Enums;

class QueueTypesEnum {
    const HIGH = 'high';
    const LOW = 'low';
    const NORMAL = 'normal';

    const CALL = 'call';
    const EMAIL = 'email';
    const TEXT = 'text';
}
