<?php
namespace Uab\Http\Enums;

use Uab\Http\Enums\Traits\GetAllEnumsTrait;

class ExportFormatsEnum {
    use GetAllEnumsTrait;

    CONST CSV = 'csv';
    CONST HTML = 'html';
    CONST TSV = 'tsv';
    CONST ODS = 'ods';
    CONST XLS = 'xls';
    CONST XLSX = 'xlsx';
}
