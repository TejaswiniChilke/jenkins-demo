<?php
namespace Uab\Http\Enums;

class TwilioTextEnums {
    const ACCOUNT_SID = 'AccountSid';
    const BODY = 'Body';
    const FROM = 'From';
    const MESSAGE_SID = 'MessageSid';
    const NUM_MEDIA = 'NumMedia';
    const NUM_SEGMENTS = 'NumSegments';
    const TO = 'To';
}
