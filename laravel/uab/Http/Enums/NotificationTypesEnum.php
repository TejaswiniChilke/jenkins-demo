<?php
namespace Uab\Http\Enums;

class NotificationTypesEnum {
    const CUSTOM = 'custom';

    const FAILED_BROWSER_CHECK = 'failedBrowserCheck';
    const FAILED_SPEED_CHECK = 'failedSpeedCheck';
    const REPORTED_COMMENT = 'reportedComment';
    const REPORTED_POST = 'reportedPost';

    const NEW_APPLICATION = 'NewApplication';
    const NEW_BADGE = 'BadgesUsers';
    const NEW_BUG_REPORT = 'NewBugReport';
    const NEW_COMMENT = 'new-comments';
    const NEW_CONTACT_STAFF = 'ContactStaffMessage';
    const NEW_EVENT = 'EventsUsers';
    const NEW_FOLLOWER = 'Followers';
    const NEW_FRIEND = 'newFriend';
    const NEW_LIKE = 'PostLikes';
    const NEW_POST = 'new-posts';

    const USERS = 'Users';
    const TEXT = 'text';
    const VIDEOS = 'video';
}
