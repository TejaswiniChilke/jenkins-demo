<?php
namespace Uab\Http\Enums;

class AddressProvidersEnum {
    const GOOGLE = 'google';
    const USPS = 'usps';
}
