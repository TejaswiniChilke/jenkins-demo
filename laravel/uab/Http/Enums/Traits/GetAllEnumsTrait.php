<?php
namespace Uab\Http\Enums\Traits;

use Illuminate\Support\Arr;
use ReflectionClass;

trait GetAllEnumsTrait {
    /**
     * @return array
     */
    public static function getAll():array {
        $reflect = new ReflectionClass(self::class);

        return array_values($reflect->getConstants());
    }

    /**
     * @return mixed
     */
    public static function random() {
        $enum = Arr::random(self::getAll(), 1);

        return Arr::first($enum);
    }
}
