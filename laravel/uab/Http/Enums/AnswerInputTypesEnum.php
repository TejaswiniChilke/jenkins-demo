<?php
namespace Uab\Http\Enums;

class AnswerInputTypesEnum {
    const CHECK = 'CHECK';
    const DATE = 'DATE';
    const DATETIME = 'DATETIME';
    const DESCRIPTION = 'DESCRIPTION';
    const DROP = 'DROP';
    const GOAL_STEPS = 'GOAL-STEPS';
    const INTEGER = 'INTEGER';
    const MINUTES = 'MINUTES';
    const RADIO = 'RADIO';
    const RPE_BUTTONS = 'RPE-BUTTON-ICONS';
    const RPE_LIST = 'RPE-LARGE-PRINT';
    const SHORT_TEXT = 'SHORT-TEXT';
    const SLIDER = 'SLIDER';
    const STEPS = 'STEPS';
    const TEXT = 'TEXT';
    const TIME = 'TIME';
    const TITLE = 'TITLE';

    public static function getAll() {
        return [
            self::CHECK,
            self::DATE,
            self::DATETIME,
            self::DESCRIPTION,
            self::DROP,
            self::GOAL_STEPS,
            self::INTEGER,
            self::MINUTES,
            self::RADIO,
            self::RPE_BUTTONS,
            self::RPE_LIST,
            self::SHORT_TEXT,
            self::SLIDER,
            self::STEPS,
            self::TEXT,
            self::TIME,
            self::TITLE,
        ];
    }
}
