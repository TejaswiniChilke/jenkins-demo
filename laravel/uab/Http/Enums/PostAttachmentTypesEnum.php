<?php
namespace Uab\Http\Enums;

class PostAttachmentTypesEnum {
    const ARTICLES = 'articles';
    const VIDEOS = 'videos';
}
