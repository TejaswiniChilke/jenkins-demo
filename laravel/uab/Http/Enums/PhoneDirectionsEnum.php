<?php
namespace Uab\Http\Enums;

class PhoneDirectionsEnum {
    const INCOMING = 'incoming';
    const OUTGOING = 'outgoing';
    const OUTBOUND_API = 'outbound-api';
}
