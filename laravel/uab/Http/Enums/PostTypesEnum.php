<?php
namespace Uab\Http\Enums;

class PostTypesEnum {
    const ARTICLES = 'article';
    const CHECK_IN = 'check-in';
    const TEXT = 'text';
    const VIDEOS = 'video';
}
