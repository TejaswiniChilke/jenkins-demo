<?php
namespace Uab\Http\Enums;

use Uab\Http\Enums\Traits\GetAllEnumsTrait;

class UserStatusesEnum {
    use GetAllEnumsTrait;

    const APPLICATION_APPROVED = 'application-approved';
    const APPLICATION_DENIED = 'application-denied';
    const APPLICATION_PENDING = 'application-pending';

    const APPLICATION_REJECTED_AGE = 'rejected-by-age';
    const APPLICATION_REJECTED_SCI = 'rejected-by-sci';

    const AWAITING_EMAIL_CONFIRMATION = 'awaiting-email-confirmation';
    const EMAIL_CONFIRMED = 'email-confirmed';

    const BROWSER_CONFIRMED = 'browser-confirmed';

    const DEACTIVATED = 'deactivated';

    const PROFILE_COMPLETE = 'profile-complete';

    const NEED_PROFILE_COMPLETE = 'need-profile-complete';

    const SPEED_CONFIRMED = 'speed-confirmed';
}
