<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBadgeConditionsUsersTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('badge_conditions_users', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->dateTime('modified')->nullable()->default('');
			$table->dateTime('created')->nullable()->default('');
			$table->integer('badge_condition_id', 255);
			$table->integer('user_id', 255);
			$table->dateTime('deleted_at')->nullable()->default('');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('badge_conditions_users');
    }
}
