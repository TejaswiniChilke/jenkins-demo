<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTypesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('user_types', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->char('name', 256);
			$table->dateTime('modified')->nullable()->default('');
			$table->char('display_name', 256);
			$table->dateTime('created')->nullable()->default('');
			$table->char('color', 45)->nullable()->default('');
			$table->integer('project_id', 255)->nullable()->default('');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('user_types');
    }
}
