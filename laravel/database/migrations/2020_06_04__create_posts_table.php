<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('posts', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->integer('from_user_id', 255)->nullable()->default('');
			$table->integer('to_user_id', 255)->nullable()->default('');
			$table->text('message');
			$table->dateTime('created')->nullable()->default('');
			$table->dateTime('modified')->nullable()->default('');
			$table->dateTime('stickied')->nullable()->default('');
			$table->integer('image_id', 255)->nullable()->default('');
			$table->timestamp('deleted_at')->nullable()->default('');
			$table->char('type', 255)->nullable()->default('');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('posts');
    }
}
