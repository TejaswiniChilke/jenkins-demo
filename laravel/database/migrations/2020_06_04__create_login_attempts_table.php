<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoginAttemptsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('login_attempts', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->char('username', 255);
			$table->boolean('success');
			$table->dateTime('created')->nullable()->default('');
			$table->dateTime('modified')->nullable()->default('');
			$table->dateTime('attempted_at')->nullable()->default('');
			$table->integer('user_id', 255)->nullable()->default('');
			$table->char('ip_address', 255)->nullable()->default('');
			$table->char('robot_name', 255)->nullable()->default('');
			$table->char('languages', 255)->nullable()->default('');
			$table->char('browser', 255)->nullable()->default('');
			$table->char('platform', 255)->nullable()->default('');
			$table->char('device', 255)->nullable()->default('');
			$table->boolean('is_desktop')->nullable()->default('0');
			$table->boolean('is_mobile')->nullable()->default('0');
			$table->boolean('is_tablet')->nullable()->default('0');
			$table->boolean('is_phone')->nullable()->default('0');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('login_attempts');
    }
}
