<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('users', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->char('email', 255)->nullable()->default('');
			$table->char('username', 255)->nullable()->default('');
			$table->char('nickname', 255)->nullable()->default('');
			$table->char('password', 255)->nullable()->default('');
			$table->char('first_name', 255)->nullable()->default('');
			$table->char('last_name', 255)->nullable()->default('');
			$table->integer('image_id', 255)->nullable()->default('');
			$table->integer('user_type_id', 255)->nullable()->default('');
			$table->integer('group_id', 255)->nullable()->default('');
			$table->integer('login_count', 255)->default('0');
			$table->integer('logout_count', 255)->default('0');
			$table->char('security_question', 255)->nullable()->default('');
			$table->char('security_answer', 255)->nullable()->default('');
			$table->boolean('gender')->nullable()->default('');
			$table->char('mobile_number', 45)->nullable()->default('');
			$table->dateTime('start_time')->nullable()->default('');
			$table->boolean('wheelchair')->default('0');
			$table->dateTime('preferred_time')->nullable()->default('');
			$table->char('home_number', 45)->nullable()->default('');
			$table->char('a1c', 255)->nullable()->default('');
			$table->char('type_diabetes', 255)->nullable()->default('');
			$table->char('years_since_diagnosis', 255)->nullable()->default('');
			$table->char('remember_token', 100)->nullable()->default('');
			$table->dateTime('email_verified_at')->nullable()->default('');
			$table->boolean('requires_approval')->default('0');
			$table->char('address_line_one', 255)->nullable()->default('');
			$table->char('address_line_two', 255)->nullable()->default('');
			$table->integer('state_id', 255)->nullable()->default('');
			$table->char('city', 255)->nullable()->default('');
			$table->char('zipcode', 45)->nullable()->default('');
			$table->dateTime('approved_on')->nullable()->default('');
			$table->dateTime('declined_on')->nullable()->default('');
			$table->dateTime('activation_email_sent_date')->nullable()->default('');
			$table->integer('speed_check', 255)->nullable()->default('');
			$table->char('browser_check', 255)->nullable()->default('');
			$table->dateTime('email_confirmed_on')->nullable()->default('');
			$table->date('dob')->nullable()->default('');
			$table->char('preferred_contact_time', 255)->nullable()->default('');
			$table->char('preferred_contact_type', 255)->nullable()->default('');
			$table->dateTime('created')->nullable()->default('');
			$table->dateTime('modified')->nullable()->default('');
			$table->timestamp('deleted_at')->nullable()->default('');
			$table->integer('coach_id', 255)->nullable()->default('');
			$table->boolean('has_sci')->nullable()->default('');
			$table->char('status', 255)->nullable()->default('');
			$table->text('profile_bio')->nullable()->default('');
			$table->char('profile_city', 255)->nullable()->default('');
			$table->char('profile_state', 255)->nullable()->default('');
			$table->char('requires_medical_clearance', 45)->default('0');
			$table->text('preferred_session_times')->nullable()->default('');
			$table->integer('spb_score', 255)->nullable()->default('0');
			$table->char('last_called', 255)->nullable()->default('');
			$table->char('preferred_session_days', 255)->nullable()->default('');
			$table->char('pincode', 45)->nullable()->default('');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('users');
    }
}
