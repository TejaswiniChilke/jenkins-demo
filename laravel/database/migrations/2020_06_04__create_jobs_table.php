<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('jobs', function(Blueprint $table) {
			;
			$table->char('queue', 255);
			;
			$table->boolean('attempts');
			$table->integer('reserved_at', 255)->nullable()->default('');
			$table->integer('available_at', 255);
			$table->integer('created_at', 255);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('jobs');
    }
}
