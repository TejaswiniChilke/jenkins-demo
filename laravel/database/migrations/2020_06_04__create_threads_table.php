<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateThreadsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('threads', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->char('subject', 255);
			$table->timestamp('created_at')->nullable()->default('');
			$table->timestamp('updated_at')->nullable()->default('');
			$table->timestamp('deleted_at')->nullable()->default('');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('threads');
    }
}
