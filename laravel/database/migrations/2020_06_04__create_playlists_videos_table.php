<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlaylistsVideosTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('playlists_videos', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->integer('playlist_id', 255);
			$table->integer('video_id', 255);
			$table->dateTime('created');
			$table->dateTime('modified');
			$table->integer('position', 255)->nullable()->default('');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('playlists_videos');
    }
}
