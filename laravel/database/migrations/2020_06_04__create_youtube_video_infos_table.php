<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateYoutubeVideoInfosTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('youtube_video_infos', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->dateTime('created')->nullable()->default('');
			$table->dateTime('modified')->nullable()->default('');
			$table->char('youtube_id', 45)->nullable()->default('');
			$table->char('kind', 45)->nullable()->default('');
			$table->text('etag')->nullable()->default('');
			$table->char('published_at', 45)->nullable()->default('');
			$table->char('channel_id', 45)->nullable()->default('');
			$table->char('title', 255)->nullable()->default('');
			$table->text('description')->nullable()->default('');
			$table->char('thumbnail_default', 255)->nullable()->default('');
			$table->char('thumbnail_medium', 255)->nullable()->default('');
			$table->char('thumbnail_high', 255)->nullable()->default('');
			$table->char('thumbnail_standard', 255)->nullable()->default('');
			$table->char('thumbnail_max', 255)->nullable()->default('');
			$table->char('channel_title', 255)->nullable()->default('');
			$table->text('tags')->nullable()->default('');
			$table->char('youtube_category_id', 45)->nullable()->default('');
			$table->char('live_broadcast_content', 255)->nullable()->default('');
			$table->char('localized_title', 255)->nullable()->default('');
			$table->text('localized_description')->nullable()->default('');
			$table->char('duration', 45)->nullable()->default('');
			$table->char('dimension', 45)->nullable()->default('');
			$table->char('definition', 45)->nullable()->default('');
			$table->char('caption', 45)->nullable()->default('');
			$table->char('licensed_content', 45)->nullable()->default('');
			$table->char('projection', 45)->nullable()->default('');
			$table->char('upload_status', 45)->nullable()->default('');
			$table->char('privacy_status', 45)->nullable()->default('');
			$table->char('license', 45)->nullable()->default('');
			$table->boolean('embeddable')->nullable()->default('0');
			$table->boolean('public_stats_viewable')->nullable()->default('0');
			$table->char('view_count', 45)->nullable()->default('');
			$table->char('like_count', 45)->nullable()->default('');
			$table->char('dislike_count', 45)->nullable()->default('');
			$table->char('favorite_count', 45)->nullable()->default('');
			$table->char('comment_count', 45)->nullable()->default('');
			$table->text('embed_html')->nullable()->default('');
			$table->integer('video_id', 255)->nullable()->default('');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('youtube_video_infos');
    }
}
