<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('files', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->dateTime('created')->nullable()->default('');
			$table->dateTime('modified')->nullable()->default('');
			$table->char('path', 255);
			$table->char('title', 255)->nullable()->default('');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('files');
    }
}
