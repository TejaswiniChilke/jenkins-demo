<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBadgesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('badges', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->char('description', 255)->nullable()->default('');
			$table->char('title', 255);
			$table->integer('image_id', 255)->nullable()->default('');
			$table->dateTime('created')->nullable()->default('');
			$table->dateTime('modified')->nullable()->default('');
			$table->integer('max_completions', 255)->nullable()->default('0');
			$table->integer('cash', 255)->default('0');
			$table->integer('start_week', 255)->default('0');
			$table->integer('end_week', 255)->nullable()->default('');
			$table->boolean('can_complete_any')->nullable()->default('0');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('badges');
    }
}
