<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTelescopeEntriesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('telescope_entries', function(Blueprint $table) {
			$table->bigIncrements('id');
			;
			$table->char('uuid', 36);
			$table->char('batch_id', 36);
			$table->char('family_hash', 255)->nullable()->default('');
			$table->boolean('should_display_on_index')->default('1');
			$table->char('type', 20);
			;
			$table->dateTime('created_at')->nullable()->default('');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('telescope_entries');
    }
}
