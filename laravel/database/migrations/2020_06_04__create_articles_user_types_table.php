<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesUserTypesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('articles_user_types', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->dateTime('modified')->nullable()->default('');
			$table->dateTime('created')->nullable()->default('');
			$table->integer('article_id', 255);
			$table->integer('user_type_id', 255);
			$table->integer('week', 255)->default('0');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('articles_user_types');
    }
}
