<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTextMessageStopsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('text_message_stops', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->dateTime('created_at')->nullable()->default('');
			$table->dateTime('modified_at')->nullable()->default('');
			$table->dateTime('deleted_at')->nullable()->default('');
			$table->char('stop_message', 255)->nullable()->default('');
			$table->char('start_message', 255)->nullable()->default('');
			$table->char('from_number', 45)->nullable()->default('');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('text_message_stops');
    }
}
