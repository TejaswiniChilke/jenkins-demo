<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesUsersTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('articles_users', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->dateTime('created')->nullable()->default('');
			$table->dateTime('modified')->nullable()->default('');
			$table->integer('article_id', 255);
			$table->integer('user_id', 255);
			$table->boolean('is_complete')->default('0');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('articles_users');
    }
}
