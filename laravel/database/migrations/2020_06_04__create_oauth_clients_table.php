<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOauthClientsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('oauth_clients', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->integer('user_id', 255)->nullable()->default('');
			$table->char('name', 255);
			$table->char('secret', 100);
			$table->text('redirect');
			$table->boolean('personal_access_client');
			$table->boolean('password_client');
			$table->boolean('revoked');
			$table->timestamp('created_at')->nullable()->default('');
			$table->timestamp('updated_at')->nullable()->default('');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('oauth_clients');
    }
}
