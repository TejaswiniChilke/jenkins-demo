<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostAttachmentsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('post_attachments', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->dateTime('modified')->nullable()->default('');
			$table->dateTime('created')->nullable()->default('');
			$table->integer('post_id', 255);
			$table->char('reference_key', 45);
			$table->integer('reference_value', 255)->nullable()->default('');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('post_attachments');
    }
}
