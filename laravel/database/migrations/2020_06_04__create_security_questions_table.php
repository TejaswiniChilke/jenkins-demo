<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSecurityQuestionsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('security_questions', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->dateTime('modified')->nullable()->default('');
			$table->dateTime('created')->nullable()->default('');
			$table->char('question', 255);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('security_questions');
    }
}
