<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('videos', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->char('file_path', 255);
			$table->text('description')->nullable()->default('');
			$table->char('title', 255)->nullable()->default('');
			$table->dateTime('created')->nullable()->default('');
			$table->dateTime('modified')->nullable()->default('');
			$table->integer('image_id', 255)->nullable()->default('');
			$table->integer('week', 255)->nullable()->default('0');
			$table->integer('seconds', 255)->nullable()->default('0');
			$table->integer('end_week', 255)->nullable()->default('');
			$table->text('short_description')->nullable()->default('');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('videos');
    }
}
