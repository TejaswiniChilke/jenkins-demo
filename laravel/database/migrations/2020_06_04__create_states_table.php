<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('states', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->char('abbreviation', 45);
			$table->char('name', 45);
			$table->dateTime('created')->nullable()->default('');
			$table->dateTime('modified')->nullable()->default('');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('states');
    }
}
