<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUspsAddressesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('usps_addresses', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->dateTime('created');
			$table->dateTime('modified');
			$table->char('og_address_line_1', 255);
			$table->char('og_address_line_2', 255);
			$table->char('og_city', 255);
			$table->char('og_state', 255);
			$table->char('og_zipcode', 45);
			$table->char('og_zipcode_suffix', 45);
			$table->text('return_text');
			$table->text('attributes');
			$table->char('new_address_line_1', 255);
			$table->char('new_address_line_2', 255);
			$table->char('new_city', 255);
			$table->char('new_state', 255);
			$table->char('new_zipcode', 45);
			$table->char('new_zipcode_suffix', 45);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('usps_addresses');
    }
}
