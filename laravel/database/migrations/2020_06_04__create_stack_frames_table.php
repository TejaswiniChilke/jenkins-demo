<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStackFramesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('stack_frames', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->dateTime('created')->nullable()->default('');
			$table->dateTime('modified')->nullable()->default('');
			$table->integer('bug_report_id', 255);
			$table->char('column_number', 45)->nullable()->default('');
			$table->char('file_name', 255)->nullable()->default('');
			$table->char('function_name', 255)->nullable()->default('');
			$table->char('line_number', 45)->nullable()->default('');
			$table->integer('position', 255)->nullable()->default('0');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('stack_frames');
    }
}
