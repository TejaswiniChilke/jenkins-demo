<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnalyticStateVisitsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('analytic_state_visits', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->dateTime('created')->nullable()->default('');
			$table->dateTime('modified')->nullable()->default('');
			$table->integer('user_id', 255);
			$table->char('to_state', 255)->nullable()->default('');
			$table->char('params', 255)->nullable()->default('');
			$table->dateTime('transition_datetime')->nullable()->default('');
			->nullable()->default('0');
			$table->char('from_state', 255)->nullable()->default('');
			$table->dateTime('deleted_at')->nullable()->default('');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('analytic_state_visits');
    }
}
