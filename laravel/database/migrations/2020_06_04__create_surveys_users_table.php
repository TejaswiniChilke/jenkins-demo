<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveysUsersTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('surveys_users', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->dateTime('created')->nullable()->default('');
			$table->dateTime('modified')->nullable()->default('');
			$table->dateTime('last_resent')->nullable()->default('');
			$table->integer('survey_id', 255)->nullable()->default('');
			$table->integer('user_id', 255)->nullable()->default('');
			$table->integer('response_order', 255)->nullable()->default('0');
			$table->boolean('is_locked')->nullable()->default('0');
			$table->timestamp('deleted_at')->nullable()->default('');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('surveys_users');
    }
}
