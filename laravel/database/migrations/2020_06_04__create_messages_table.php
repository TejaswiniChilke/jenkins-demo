<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('messages', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->integer('thread_id', 255);
			$table->integer('user_id', 255);
			$table->text('body');
			$table->timestamp('created_at')->nullable()->default('');
			$table->timestamp('updated_at')->nullable()->default('');
			$table->timestamp('deleted_at')->nullable()->default('');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('messages');
    }
}
