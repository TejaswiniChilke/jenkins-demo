<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCheckInsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('check_ins', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->dateTime('created_at')->nullable()->default('');
			$table->dateTime('modified_at')->nullable()->default('');
			$table->dateTime('deleted_at')->nullable()->default('');
			$table->integer('user_id', 255);
			$table->integer('object_id', 255);
			$table->char('object_table', 45);
			$table->text('message')->nullable()->default('');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('check_ins');
    }
}
