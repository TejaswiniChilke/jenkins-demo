<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeaturesUserTypesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('features_user_types', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->dateTime('created')->nullable()->default('');
			$table->dateTime('modified')->nullable()->default('');
			$table->integer('feature_id', 255)->nullable()->default('');
			$table->integer('user_type_id', 255);
			$table->integer('position', 255)->default('0');
			$table->char('name', 255);
			$table->char('sref', 255)->nullable()->default('');
			$table->char('icon', 255)->nullable()->default('');
			$table->integer('start_week', 255)->nullable()->default('');
			$table->integer('end_week', 255)->nullable()->default('');
			$table->char('socket', 45)->nullable()->default('');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('features_user_types');
    }
}
