<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('emails', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->integer('to_user_id', 255)->nullable()->default('');
			$table->integer('from_user_id', 255)->nullable()->default('');
			$table->text('message')->nullable()->default('');
			$table->dateTime('created')->nullable()->default('');
			$table->dateTime('modified')->nullable()->default('');
			$table->char('from_email', 255)->nullable()->default('');
			$table->char('to_email', 255)->nullable()->default('');
			$table->dateTime('sent')->nullable()->default('');
			$table->char('subject', 255)->nullable()->default('');
			$table->char('headers', 255)->nullable()->default('');
			$table->dateTime('deleted_at')->nullable()->default('');
			$table->dateTime('read_at')->nullable()->default('');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('emails');
    }
}
