<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOauthAuthCodesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('oauth_auth_codes', function(Blueprint $table) {
			$table->char('id', 100);
			$table->integer('user_id', 255);
			$table->integer('client_id', 255);
			$table->text('scopes')->nullable()->default('');
			$table->boolean('revoked');
			$table->dateTime('expires_at')->nullable()->default('');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('oauth_auth_codes');
    }
}
