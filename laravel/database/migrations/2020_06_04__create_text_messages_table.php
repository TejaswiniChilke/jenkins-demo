<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTextMessagesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('text_messages', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->integer('to_user_id', 255)->nullable()->default('');
			$table->integer('from_user_id', 255)->nullable()->default('');
			$table->char('message', 255)->nullable()->default('');
			$table->dateTime('created')->nullable()->default('');
			$table->dateTime('modified')->nullable()->default('');
			$table->dateTime('date_created')->nullable()->default('');
			$table->dateTime('date_sent')->nullable()->default('');
			$table->dateTime('date_updated')->nullable()->default('');
			$table->char('direction', 45)->nullable()->default('');
			$table->char('error_code', 45)->nullable()->default('');
			$table->char('error_message', 255)->nullable()->default('');
			$table->char('price', 45)->nullable()->default('');
			$table->char('uri', 255)->nullable()->default('');
			$table->char('from_number', 45)->nullable()->default('');
			$table->char('num_media', 45)->nullable()->default('');
			$table->char('num_segments', 45)->nullable()->default('');
			$table->char('price_unit', 45)->nullable()->default('');
			$table->char('sid', 45)->nullable()->default('');
			$table->char('to_number', 45)->nullable()->default('');
			$table->timestamp('deleted_at')->nullable()->default('');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('text_messages');
    }
}
