<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveysTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('surveys', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->char('name', 255);
			$table->dateTime('created')->nullable()->default('');
			$table->dateTime('updated')->nullable()->default('');
			$table->integer('week', 255)->nullable()->default('0');
			$table->boolean('required_on_start')->nullable()->default('0');
			$table->integer('image_id', 255)->nullable()->default('');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('surveys');
    }
}
