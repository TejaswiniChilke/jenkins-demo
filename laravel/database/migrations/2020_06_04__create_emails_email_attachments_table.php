<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailsEmailAttachmentsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('emails_email_attachments', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->integer('email_id', 255);
			$table->integer('email_attachment_id', 255);
			$table->dateTime('created')->nullable()->default('');
			$table->dateTime('modified')->nullable()->default('');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('emails_email_attachments');
    }
}
