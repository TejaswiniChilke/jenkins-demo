<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('notifications', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->dateTime('created')->nullable()->default('');
			$table->dateTime('modified')->nullable()->default('');
			$table->char('message', 255);
			$table->char('type', 255)->nullable()->default('');
			$table->integer('user_id', 255);
			$table->boolean('has_read')->nullable()->default('0');
			$table->integer('image_id', 255)->nullable()->default('');
			$table->char('reference_name', 45)->nullable()->default('');
			$table->char('reference_value', 45)->nullable()->default('0');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('notifications');
    }
}
