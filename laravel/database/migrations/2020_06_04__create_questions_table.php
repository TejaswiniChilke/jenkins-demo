<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('questions', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->integer('position', 255)->nullable()->default('0');
			$table->char('phrasing', 255);
			$table->dateTime('created')->nullable()->default('');
			$table->dateTime('modified')->nullable()->default('');
			$table->integer('answer_input_type_id', 255)->nullable()->default('');
			$table->boolean('required')->nullable()->default('0');
			$table->integer('image_id', 255)->nullable()->default('');
			$table->integer('survey_page_id', 255)->nullable()->default('');
			$table->char('placeholder', 255)->default('');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('questions');
    }
}
