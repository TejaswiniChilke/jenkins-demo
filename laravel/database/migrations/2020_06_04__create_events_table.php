<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('events', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->dateTime('start');
			$table->dateTime('end')->nullable()->default('');
			$table->char('title', 255);
			$table->char('description', 255)->nullable()->default('');
			$table->integer('creator_id', 255)->nullable()->default('');
			$table->dateTime('created')->nullable()->default('');
			$table->dateTime('modified')->nullable()->default('');
			$table->boolean('allDay')->nullable()->default('0');
			$table->boolean('is_cancelled')->nullable()->default('0');
			$table->boolean('is_complete')->nullable()->default('0');
			$table->dateTime('deleted_at')->nullable()->default('');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('events');
    }
}
