<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOauthAccessTokensTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('oauth_access_tokens', function(Blueprint $table) {
			$table->char('id', 100);
			$table->integer('user_id', 255)->nullable()->default('');
			$table->integer('client_id', 255);
			$table->char('name', 255)->nullable()->default('');
			$table->text('scopes')->nullable()->default('');
			$table->boolean('revoked');
			$table->timestamp('created_at')->nullable()->default('');
			$table->timestamp('updated_at')->nullable()->default('');
			$table->dateTime('expires_at')->nullable()->default('');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('oauth_access_tokens');
    }
}
