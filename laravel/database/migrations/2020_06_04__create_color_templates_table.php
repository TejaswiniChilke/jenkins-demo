<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColorTemplatesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('color_templates', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->dateTime('created')->nullable()->default('');
			$table->dateTime('modified')->nullable()->default('');
			$table->char('accept_button_color', 45)->nullable()->default('');
			$table->char('accept_button_text_color', 45)->nullable()->default('');
			$table->char('decline_button_color', 45)->nullable()->default('');
			$table->char('decline_button_text_color', 45)->nullable()->default('');
			$table->char('neutral_button_color', 45)->nullable()->default('');
			$table->char('neutral_button_text_color', 45)->nullable()->default('');
			$table->char('primary_color', 45)->nullable()->default('');
			$table->char('primary_text_color', 45)->nullable()->default('');
			$table->char('secondary_color', 45)->nullable()->default('');
			$table->char('secondary_text_color', 45)->nullable()->default('');
			$table->integer('project_id', 255);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('color_templates');
    }
}
