<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlockedAccessAttemptsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('blocked_access_attempts', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->dateTime('created')->nullable()->default('');
			$table->dateTime('modified')->nullable()->default('');
			$table->dateTime('deleted_at')->nullable()->default('');
			$table->integer('user_id', 255)->nullable()->default('');
			$table->char('ip_address', 45)->nullable()->default('');
			$table->char('browser', 45)->nullable()->default('');
			$table->char('reason', 45);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('blocked_access_attempts');
    }
}
