<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsUsersTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('settings_users', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->char('value', 256);
			$table->integer('user_id', 255);
			$table->integer('setting_id', 255);
			$table->dateTime('modified')->nullable()->default('');
			$table->dateTime('created')->nullable()->default('');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('settings_users');
    }
}
