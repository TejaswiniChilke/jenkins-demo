<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebhookRequestsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('webhook_requests', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->dateTime('created')->nullable()->default('');
			$table->dateTime('modified')->nullable()->default('');
			$table->char('webhook', 255)->nullable()->default('');
			$table->text('response')->nullable()->default('');
			$table->char('response_code', 45)->nullable()->default('');
			$table->text('request_data')->nullable()->default('');
			$table->char('url', 255)->nullable()->default('');
			$table->char('domain', 45)->nullable()->default('');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('webhook_requests');
    }
}
