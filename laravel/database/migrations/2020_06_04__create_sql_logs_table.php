<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSqlLogsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('sql_logs', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->integer('user_id', 255)->nullable()->default('');
			$table->text('query');
			$table->dateTime('executed_at');
			$table->dateTime('created')->nullable()->default('');
			$table->dateTime('modified')->nullable()->default('');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('sql_logs');
    }
}
