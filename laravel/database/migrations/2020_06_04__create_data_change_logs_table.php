<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataChangeLogsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('data_change_logs', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->dateTime('created')->nullable()->default('');
			$table->dateTime('modified')->nullable()->default('');
			$table->integer('user_id', 255)->nullable()->default('');
			$table->char('model', 45)->nullable()->default('');
			$table->text('changes')->nullable()->default('');
			$table->char('action', 45)->nullable()->default('');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('data_change_logs');
    }
}
