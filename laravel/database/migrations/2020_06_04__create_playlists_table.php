<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlaylistsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('playlists', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->char('title', 255);
			$table->dateTime('created')->nullable()->default('');
			$table->dateTime('modified')->nullable()->default('');
			$table->integer('image_id', 255)->nullable()->default('');
			$table->char('description', 255)->nullable()->default('');
			$table->integer('week', 255)->default('0');
			$table->text('short_description')->nullable()->default('');
			$table->integer('end_week', 255)->nullable()->default('');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('playlists');
    }
}
