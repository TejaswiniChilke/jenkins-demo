<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOauthPersonalAccessClientsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('oauth_personal_access_clients', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->integer('client_id', 255);
			$table->timestamp('created_at')->nullable()->default('');
			$table->timestamp('updated_at')->nullable()->default('');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('oauth_personal_access_clients');
    }
}
