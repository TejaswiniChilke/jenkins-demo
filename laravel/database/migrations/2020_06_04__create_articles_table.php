<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('articles', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->dateTime('created')->nullable()->default('');
			$table->dateTime('modified')->nullable()->default('');
			$table->char('title', 255);
			$table->text('description')->nullable()->default('');
			$table->text('content')->nullable()->default('');
			$table->integer('image_id', 255)->nullable()->default('');
			$table->integer('week', 255)->default('0');
			$table->integer('content_image_id', 255)->nullable()->default('');
			$table->text('short_description')->nullable()->default('');
			$table->integer('end_week', 255)->nullable()->default('');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('articles');
    }
}
