<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebsocketsStatisticsEntriesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('websockets_statistics_entries', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->char('app_id', 255);
			$table->integer('peak_connection_count', 255);
			$table->integer('websocket_message_count', 255);
			$table->integer('api_message_count', 255);
			$table->timestamp('created_at')->nullable()->default('');
			$table->timestamp('updated_at')->nullable()->default('');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('websockets_statistics_entries');
    }
}
