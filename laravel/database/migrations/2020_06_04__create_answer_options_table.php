<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswerOptionsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('answer_options', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->char('answer', 255);
			$table->dateTime('created')->nullable()->default('');
			$table->dateTime('modified')->nullable()->default('');
			$table->integer('parent_question_id', 255);
			$table->integer('position', 255)->nullable()->default('0');
			$table->integer('image_id', 255)->nullable()->default('');
			$table->integer('next_question_id', 255)->nullable()->default('');
			$table->integer('next_survey_page_id', 255)->nullable()->default('');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('answer_options');
    }
}
