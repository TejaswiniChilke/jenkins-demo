<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswerInputTypesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('answer_input_types', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->char('type', 255);
			$table->dateTime('created')->nullable()->default('');
			$table->dateTime('modified')->nullable()->default('');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('answer_input_types');
    }
}
