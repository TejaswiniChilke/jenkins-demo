<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportedCommentsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('reported_comments', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->dateTime('created')->nullable()->default('');
			$table->dateTime('modified')->nullable()->default('');
			$table->integer('user_id', 255)->nullable()->default('');
			$table->integer('comment_id', 255);
			$table->integer('verified_user_id', 255)->nullable()->default('');
			$table->dateTime('deleted_at')->nullable()->default('');
			$table->dateTime('verified_at')->nullable()->default('');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('reported_comments');
    }
}
