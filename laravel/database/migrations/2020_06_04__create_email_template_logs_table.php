<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailTemplateLogsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('email_template_logs', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->dateTime('created');
			$table->dateTime('modified');
			$table->integer('email_template_id', 255);
			$table->integer('user_id', 255);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('email_template_logs');
    }
}
