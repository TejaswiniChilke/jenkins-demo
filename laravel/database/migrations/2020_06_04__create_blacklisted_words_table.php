<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlacklistedWordsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('blacklisted_words', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->dateTime('created')->nullable()->default('');
			$table->dateTime('modified')->nullable()->default('');
			$table->char('word', 45);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('blacklisted_words');
    }
}
