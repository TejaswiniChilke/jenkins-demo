<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOauthRefreshTokensTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('oauth_refresh_tokens', function(Blueprint $table) {
			$table->char('id', 100);
			$table->char('access_token_id', 100);
			$table->boolean('revoked');
			$table->dateTime('expires_at')->nullable()->default('');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('oauth_refresh_tokens');
    }
}
