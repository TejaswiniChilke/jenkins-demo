<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePasswordResetsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('password_resets', function(Blueprint $table) {
			$table->char('email', 255);
			$table->char('token', 255);
			$table->dateTime('created_at')->nullable()->default('');
			$table->bigIncrements('id');
			$table->dateTime('modified_at')->nullable()->default('');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('password_resets');
    }
}
