<?php
namespace Uab\Database\Seeds;

use Illuminate\Database\Seeder;
use Uab\Database\Seeds\Traits\AnalyticsSeederTrait;
use Uab\Database\Seeds\Traits\ArticlesSeederTrait;
use Uab\Database\Seeds\Traits\BadgesSeederTrait;
use Uab\Database\Seeds\Traits\BlacklistedWordsSeederTrait;
use Uab\Database\Seeds\Traits\EmailsSeederTrait;
use Uab\Database\Seeds\Traits\EventsSeederTrait;
use Uab\Database\Seeds\Traits\FeaturesSeederTrait;
use Uab\Database\Seeds\Traits\JunkSeederTrait;
use Uab\Database\Seeds\Traits\PlaylistsSeederTrait;
use Uab\Database\Seeds\Traits\SurveysSeederTrait;
use Uab\Database\Seeds\Traits\UsersSeederTrait;
use Uab\Database\Seeds\Traits\VideosSeederTrait;

class BlankSeeder extends Seeder {
    use AnalyticsSeederTrait;
    use ArticlesSeederTrait;
    use BadgesSeederTrait;
    use BlacklistedWordsSeederTrait;
    use EmailsSeederTrait;
    use EventsSeederTrait;
    use FeaturesSeederTrait;
    use JunkSeederTrait;
    use PlaylistsSeederTrait;
    use SurveysSeederTrait;
    use UsersSeederTrait;
    use VideosSeederTrait;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $this->clearAll();
    }

    public function clearAll() {
        $this->clearArticleComments();
        $this->clearAnalytics();
        $this->clearBadgesFromUsers();
        $this->clearEmails();
        $this->clearEvents();
        $this->clearFeatures();
        $this->clearPlaylists();
        $this->clearSurveys();
        $this->clearTermsAndConditions();
        $this->clearTags();
        $this->clearUsers();

        $this->clearJunk();
    }
}
