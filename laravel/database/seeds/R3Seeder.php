<?php
namespace Uab\Database\Seeds;

use Illuminate\Support\Str;
use Uab\Database\Seeds\Traits\FeaturesSeederTrait;
use Uab\Database\Seeds\Traits\ProjectsSeederTrait;
use Uab\Database\Seeds\Traits\SurveysSeederTrait;
use Uab\Http\Models\AnswerOptions;
use Uab\Http\Models\AnswerOptionsTags;
use Uab\Http\Models\Generated\SurveyPages;
use Uab\Http\Models\Lingos;
use Uab\Http\Models\Questions;
use Uab\Http\Models\Surveys;
use Uab\Http\Models\Tags;
use Uab\Http\Models\Users;
use Exception;
use \Uab\Http\Enums\UserTypesEnum;
use Uab\Http\Models\UserTypes;

class R3Seeder extends BaseProjectSeeder {
    use FeaturesSeederTrait;
    use ProjectsSeederTrait;
    use SurveysSeederTrait;

    /** @var UserTypes|null */ public $control = null;
    /** @var UserTypes|null */ public $m2m = null;
    /** @var UserTypes|null */ public $set = null;

    /**
     * @throws Exception
     */
    public function run() {
        parent::run();

        parent::clearAnalytics();
        parent::clearUsers();

        $this->createAnswerInputTypes();
        $this->createSurveys();
    }

    public function addAdminFeatures() {
        $data = [
            UserTypesEnum::ADMIN => [
                // Administrator
                'To Do' => 'section-header',
                'Applications' => 'application-management',
                'Failed Applicants' => 'fail-user-management',
                'Notes' => 'notes',

                // Participant Interaction
                'Participant Interaction' => 'section-header',
                'Contact Staff Messages' => 'contact-staff-message-list',
                'Messages' => 'inbox',
                'Newsfeed' => 'Newsfeed',
                'My Profile' => 'profile',

                // Monitoring
                'Analytics' => 'section-header',
                'Article Analytics' => 'article-analytic-management',
                'State Analytics' => 'state-analytic-management',
                'Video Analytics' => 'video-analytic-management',

                // Content Management
                'Content Management' => 'section-header',
                'Article Management' => 'article-management',
                'Badge Management' => 'badge-management',
                'Notification Management' => 'notification-management',
                'Playlist Management' => 'playlist-management',
                'Survey Management' => 'survey-management',
                'Tag Management' => 'tag-management',
                'User Management' => 'user-management',
                'User Type Management' => 'user-type-management',
                'Video Management' => 'video-management',

                // Site Management
                'Site Management' => 'section-header',
                'Blacklisted Word Management' => 'blacklisted-word-management',
                'Security Question Management' => 'security-question-management',

                // Help
                'Help' => 'section-header',
                'Contact Staff' => 'contact-staff',
                'Report a Bug' => 'bug-report',

//                 Bottom
                'Logout' => 'logout',
            ],
        ];

        $this->createFeatureUserTypesFromArray($data);
    }

    public function addParticipantFeatures() {
        $data  = [
            UserTypesEnum::M2M => [
                'Home' => 'home',
                'Latest Videos' => 'current-playlist',
                'Previous Videos' => 'playlist-archive-list',
                'Articles' => 'article-list-week',
                'Badges' => 'badge-list',
                'Messages' => 'inbox',
                'My Friends' => 'friend-and-request-list',
                'My Profile' => 'profile',
                'All Users' => 'user-list',
                'Settings' => 'contact-preferences',
                'Report a Bug' => 'bug-report',
                'Contact Staff' => 'contact-staff',

                // Bottom
                'Logout' => 'logout',
            ]
        ];

        $this->createFeatureUserTypesFromArray($data);

        $data = [
            UserTypesEnum::SET => $data[UserTypesEnum::M2M]
        ];

        $this->createFeatureUserTypesFromArray($data);

        $data  = [
            UserTypesEnum::CONTROL => [
                'Logout' => 'logout',
            ]
        ];

        $this->createFeatureUserTypesFromArray($data);
    }

    public function createBlacklistedWords() {
        return;
    }

    public function createLingos() {
        $lingos = [
            'home.recent-articles.title'  => 'My Articles',
            'home.recent-playlists.title' => 'My Exercise Videos',
            'article.page.checkIn.button.checkIn.label' => 'Mark as Read',
            'article.page.checkIn.button.checkingIn.label' => 'Marking as Read...',
            'article.page.checkIn.button.checkedIn.label' => 'Already Read',
            'article.page.checkIn.error' => 'There was an error marking the article as read.',
            'article.page.checkIn.success' => 'You\'ve successfully marked the article as read.',
            'complete-profile.modal.title' => 'Function and Pain Survey',
            'complete-profile.modal.bio.placeholder' => 'Hello all, my name is Kim. I am a mom of three children (dogs): Pink, Cooper, and Gus. I live in Birmingham and am currently working as a party coordinator. In my spare time, I enjoy cooking, knitting, and watching movies with my family and friends. The goals that I want to get out of this program are being more active and making more friends!',
            'complete-profile.page.bio.placeholder' => 'Hello all, my name is Kim. I am a mom of three children (dogs): Pink, Cooper, and Gus. I live in Birmingham and am currently working as a party coordinator. In my spare time, I enjoy cooking, knitting, and watching movies with my family and friends. The goals that I want to get out of this program are being more active and making more friends!',
            'profile-edit.page.bio.placeholder' => 'Hello all, my name is Kim. I am a mom of three children (dogs): Pink, Cooper, and Gus. I live in Birmingham and am currently working as a party coordinator. In my spare time, I enjoy cooking, knitting, and watching movies with my family and friends. The goals that I want to get out of this program are being more active and making more friends!',
            'video.page.checkIn.button.checkIn.label' => 'Mark as Watched',
            'video.page.checkIn.button.checkingIn.label' => 'Marking as Watched...',
            'video.page.checkIn.button.checkedIn.label' => 'Already Watched',
            'video.page.checkIn.error' => 'There was an error marking the video as watched.',
            'video.page.checkIn.success' => 'You\'ve successfully marked the video as watched.',
        ];

        foreach ($lingos as $key => $value) {
            $lingo = new Lingos();
            $lingo->key = $key;
            $lingo->value = $value;
            $lingo->save();
        }
    }

    public function createProjects() {
        $this->createProject('R3');
    }

    public function createSurveys() {
        $survey = new Surveys();
        $survey->fill(
            [
                'name'              => 'Function and Pain Survey',
                'week'              => 0,
                'required_on_start' => 1,
            ]
        );
        $survey->save();

        $surveyPage = new SurveyPages();
        $surveyPage->fill(
            [
                'survey_id' => $survey->id,
                'name'      => 'Function and Pain Survey',
                'position'  => 0,
            ]
        );
        $surveyPage->save();

        $questionDetails = [
            [
                'question' => [
                    'phrasing'         => 'Please select one that describes your functional level.',
                    'required'         => 1,
                    'answer_input_type_id' => $this->findAnswerInputType('drop')->id,
                ],
                'answers' => [
                    [
                        'answer' => [
                            'answer' => 'I can use both arms and legs to exercise.',
                        ],
                        'tags' => [
                            'arms-and-legs',
                        ]
                    ],
                    [
                        'answer' => [
                            'answer' => 'I can use both arms to exercise with good trunk control.',
                        ],
                        'tags' => [
                            'arms-and-good-trunk',
                        ]
                    ],
                    [
                        'answer' => [
                            'answer' => 'I can use both arms with little or no trunk control.',
                        ],
                        'tags' => [
                            'arms-and-no-trunk',
                        ]
                    ],
                ],
            ],
            [
                'question' => [
                    'phrasing'         => 'Do you have pain in your lower back?',
                    'required'         => 1,
                    'answer_input_type_id' => $this->findAnswerInputType('drop')->id,
                ],
                'answers' => [
                    [
                        'answer' => [
                            'answer' => 'Yes',
                        ],
                        'tags' => [
                            'pain-in-lower-back',
                        ]
                    ],
                    [
                        'answer' => [
                            'answer' => 'No',
                        ],
                        'tags' => [
                            'no-pain-in-lower-back',
                        ]
                    ],
                ]
            ],
            [
                'question' => [
                    'phrasing'         => 'Do you have pain in your shoulder(s)?',
                    'required'         => 1,
                    'answer_input_type_id' => $this->findAnswerInputType('drop')->id,
                ],
                'answers' => [
                    [
                        'answer' => [
                            'answer' => 'Yes',
                        ],
                        'tags' => [
                            'pain-in-shoulders',
                        ]
                    ],
                    [
                        'answer' => [
                            'answer' => 'No',
                        ],
                        'tags' => [
                            'no-pain-in-shoulders',
                        ]
                    ],
                ]
            ],
            [
                'question' => [
                    'phrasing'             => 'Do you have pain in your knee(s)?',
                    'required'             => 1,
                    'answer_input_type_id' => $this->findAnswerInputType('drop')->id,
                ],
                'answers' => [
                    [
                        'answer' => [
                            'answer' => 'Yes',
                        ],
                        'tags' => [
                            'pain-in-knees',
                        ]
                    ],
                    [
                        'answer' => [
                            'answer' => 'No',
                        ],
                        'tags' => [
                            'no-pain-in-knees',
                        ]
                    ],
                ]
            ],
            [
                'question' => [
                    'phrasing'         => 'Would you like to include balance exercises in your exercise sessions?',
                    'required'         => 1,
                    'answer_input_type_id' => $this->findAnswerInputType('drop')->id,
                ],
                'answers' => [
                    [
                        'answer' => [
                            'answer' => 'Yes',
                        ],
                        'tags' => [
                            'balance-yes',
                        ]
                    ],
                    [
                        'answer' => [
                            'answer' => 'No',
                        ],
                        'tags' => [
                            'balance-no',
                        ]
                    ],
                ]
            ],
        ];

        foreach ($questionDetails as $questionPosition => $details) {
            $details['question']['survey_page_id'] = $surveyPage->id;
            $details['question']['position'] = $questionPosition;

            $question = new Questions();
            $question->fill($details['question']);
            $question->save();

            foreach ($details['answers'] as $answerPosition => $answerDetails) {
                $answerDetails['answer']['parent_question_id'] = $question->id;
                $answerDetails['answer']['position'] = $answerPosition;

                $answer = new AnswerOptions();
                $answer->fill($answerDetails['answer']);
                $answer->save();

                foreach ($answerDetails['tags'] as $tagName) {
                    $tag = $this->findOrCreateTags($tagName);

                    $answerTag = new AnswerOptionsTags();
                    $answerTag->fill(
                        [
                            'tag_id'           => $tag->id,
                            'answer_option_id' => $answer->id,
                        ]
                    );
                }
            }
        }
    }

    public function createUsers() {
        parent::createUsers();

        $user = new Users();
        $user->fill(
            [
                'username'          => 'sangee',
                'nickname'          => 'sangee',
                'email'             => 'sangee@uab.edu',
                'password'          => 'sangee',
                'first_name'        => 'Sangee',
                'last_name'         => '',
                'mobile_number'     => '',
                'user_type_id'      => $this->admin->id,
                'requires_approval' => false,
            ]
        );
        $user->save();

        $user = new Users();
        $user->fill(
            [
                'username'          => 'yumi',
                'nickname'          => 'yumi',
                'email'             => 'yumikim@uab.edu',
                'password'          => 'yumi',
                'first_name'        => 'Yumi',
                'last_name'         => '',
                'mobile_number'     => '',
                'user_type_id'      => $this->admin->id,
                'requires_approval' => false,
            ]
        );
        $user->save();

        $user = new Users();
        $user->fill(
            [
                'username'          => 'zoe',
                'nickname'          => 'zoe',
                'email'             => 'hjyoung@uab.edu',
                'password'          => 'zoe',
                'first_name'        => 'Zoe',
                'last_name'         => '',
                'mobile_number'     => '',
                'user_type_id'      => $this->admin->id,
                'requires_approval' => false,
            ]
        );
        $user->save();
    }

    /**
     * @param $name
     *
     * @return Tags
     */
    public function findOrCreateTags($name) {
        $tag = Tags::where(
            [
                'name' => $name
            ]
        )->first();

        if (is_null($tag)) {
            $tag = new Tags();
            $tag->fill(
                [
                    'name' => $name
                ]
            );
            $tag->save();
        }

        return $tag;
    }

    public function createUserTypes() {
        $this->superAdmin = new UserTypes();
        $this->superAdmin->fill(
            [
                'name'         => UserTypesEnum::SUPER_ADMIN,
                'display_name' => Str::title(UserTypesEnum::SUPER_ADMIN),
                'project_id'   => $this->project->id,
            ]
        );
        $this->superAdmin->save();

        $this->admin = new UserTypes();
        $this->admin->fill(
            [
                'name'         => UserTypesEnum::ADMIN,
                'display_name' => Str::title(UserTypesEnum::ADMIN),
                'project_id'   => $this->project->id,
            ]
        );
        $this->admin->save();

        $this->m2m = new UserTypes();
        $this->m2m->fill(
            [
                'name'         => 'm2m',
                'display_name' => 'M2M',
                'project_id'   => $this->project->id,
            ]
        );
        $this->m2m->save();

        $this->set = new UserTypes();
        $this->set->fill(
            [
                'name'         => 'set',
                'display_name' => 'Set',
                'project_id'   => $this->project->id,
            ]
        );
        $this->set->save();

        $this->control = new UserTypes();
        $this->control->fill(
            [
                'name'         => 'control',
                'display_name' => 'Control',
                'project_id'   => $this->project->id,
            ]
        );
        $this->control->save();
    }
}
