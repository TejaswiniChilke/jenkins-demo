<?php
namespace Uab\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Exception;
use Uab\Database\Seeds\Traits\AnalyticsSeederTrait;
use Uab\Database\Seeds\Traits\EmailsSeederTrait;
use Uab\Database\Seeds\Traits\JunkSeederTrait;
use Uab\Database\Seeds\Traits\StatesSeederTrait;
use Uab\Database\Seeds\Traits\SurveysSeederTrait;
use Uab\Database\Seeds\Traits\BadgeConditionTypesSeederTrait;
use Uab\Database\Seeds\Traits\BlacklistedWordsSeederTrait;
use Uab\Database\Seeds\Traits\FeaturesSeederTrait;
use Uab\Database\Seeds\Traits\LeaderboardTypesSeederTrait;
use Uab\Database\Seeds\Traits\ProjectsSeederTrait;
use Uab\Database\Seeds\Traits\SecurityQuestionsSeederTrait;
use Uab\Database\Seeds\Traits\UsersSeederTrait;
use Uab\Http\Models\Projects;
use Uab\Http\Models\UserTypes;
use \Uab\Http\Enums\UserTypesEnum;

abstract class BaseProjectSeeder extends Seeder implements BaseProjectInterfaceSeeder {
    use AnalyticsSeederTrait;
    use BadgeConditionTypesSeederTrait;
    use BlacklistedWordsSeederTrait;
    use EmailsSeederTrait;
    use FeaturesSeederTrait;
    use JunkSeederTrait;
    use LeaderboardTypesSeederTrait;
    use ProjectsSeederTrait;
    use SecurityQuestionsSeederTrait;
    use StatesSeederTrait;
    use SurveysSeederTrait;
    use UsersSeederTrait;

    /** @var Projects|null */ public $project = null;

    /** @var UserTypes|null */ public $admin = null;
    /** @var UserTypes|null */ public $participant = null;
    /** @var UserTypes|null */ public $superAdmin = null;

    /**
     * @throws Exception
     */
    public function run() {
        $this->clear();

        $this->createAnswerInputTypes();
        $this->createBadgeConditionTypes();
        $this->createBlacklistedWords();
        $this->createLeaderboardTypes();
        $this->createProjects();
        $this->createSecurityQuestions();
        $this->createStates();
        $this->createUserTypes();
        $this->createUsers();

        $this->createFeatures();
        $this->addFeaturesToUserTypes();
    }

    public function clear() {
        $this->clearArticleComments();
        $this->clearAnalytics();
        $this->clearBadgesFromUsers();
        $this->clearBlacklistedWords();
        $this->clearEmails();
        $this->clearEvents();
        $this->clearFeatures();
        $this->clearStates();

        $this->clearJunk();
    }

    abstract public function createFeatures();

    abstract public function createProjects();

    public function createUserTypes() {
        $this->superAdmin = new UserTypes();
        $this->superAdmin->fill(
            [
                'name'         => UserTypesEnum::SUPER_ADMIN,
                'display_name' => Str::title(UserTypesEnum::SUPER_ADMIN),
                'project_id'   => $this->project->id,
            ]
        );
        $this->superAdmin->save();

        $this->admin = new UserTypes();
        $this->admin->fill(
            [
                'name'         => UserTypesEnum::ADMIN,
                'display_name' => Str::title(UserTypesEnum::ADMIN),
                'project_id'   => $this->project->id,
            ]
        );
        $this->admin->save();
    }
}
