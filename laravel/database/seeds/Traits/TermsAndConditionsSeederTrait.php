<?php
namespace Uab\Database\Seeds\Traits;

use Uab\Http\Models\TermsAndConditions;
use Uab\Http\Models\TermsAndConditionsUsers;
use Uab\Providers\LoremIpsumServiceProvider;

trait TermsAndConditionsSeederTrait {
    /**
     * @return void
     */
    public function clearTermsAndConditions() {
        $this->clearTermsAndConditionsFromUsers();

        TermsAndConditions::forceTruncate();
    }

    /**
     * @return void
     */
    public function clearTermsAndConditionsFromUsers() {
        TermsAndConditionsUsers::forceTruncate();
    }

    /**
     * @return void
     */
    public function createTermsAndConditions() {
        $loremService = new LoremIpsumServiceProvider();

        $count = rand(2, 5);

        for ($i = 0; $i < $count; $i++) {
            $terms = new TermsAndConditions();
            $terms->terms = $loremService->get(rand(3, 10));
            $terms->save();
        }
    }
}
