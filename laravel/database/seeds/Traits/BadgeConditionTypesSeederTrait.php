<?php
namespace Uab\Database\Seeds\Traits;

use Uab\Http\Models\BadgeConditionTypes;
use Uab\Http\Enums\BadgeConditionTypesEnum as BadgeConditionTypesEnum;

trait BadgeConditionTypesSeederTrait {
    /**
     * @return void
     */
    public function createBadgeConditionTypes() {
        $types = [
            BadgeConditionTypesEnum::ARTICLE_ID       => 'Read specific article.',
            BadgeConditionTypesEnum::CALORIES         => 'Total number of calories burned.',
            BadgeConditionTypesEnum::CREATE_COMMENT   => 'Create any type of comment.',
            BadgeConditionTypesEnum::CREATE_EVENT     => 'Create an event.',
            BadgeConditionTypesEnum::CREATE_POST      => 'Create a post.',
            BadgeConditionTypesEnum::FAVORITE_VIDEO   => 'Favorite any video.',
            BadgeConditionTypesEnum::FLOORS           => 'Total number of floors climbed.',
            BadgeConditionTypesEnum::LIKE_VIDEO       => 'Create post likes.',
            BadgeConditionTypesEnum::LOGIN            => 'Successful login attempts.',
            BadgeConditionTypesEnum::PLAYLIST_ID      => 'Watch all the videos in a playlist.',
            BadgeConditionTypesEnum::STEPS            => 'Total number of steps/pushes.',
            BadgeConditionTypesEnum::VIDEO_ID         => 'Watch specific video.',
        ];

        $this->createBadgeConditionTypesFromArray($types);
    }

    /**
     * @param array $types
     *
     * @return void
     */
    public function createBadgeConditionTypesFromArray(array $types):void {
        foreach ($types as $name => $description) {
            $badgeConditionType = new BadgeConditionTypes();
            $badgeConditionType->fill(
                [
                    'name' => $name,
                    'description' => $description
                ]
            );
            $badgeConditionType->save();
        }
    }
}
