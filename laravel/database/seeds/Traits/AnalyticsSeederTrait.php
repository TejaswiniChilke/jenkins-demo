<?php
namespace Uab\Database\Seeds\Traits;

use Uab\Http\Models\AnalyticArticles;
use Uab\Http\Models\AnalyticStateVisits;
use Uab\Http\Models\AnalyticVideos;

trait AnalyticsSeederTrait {
    /**
     * @return void
     */
    public function clearAnalytics() {
        AnalyticArticles::forceTruncate();
        AnalyticStateVisits::forceTruncate();
        AnalyticVideos::forceTruncate();
    }
}
