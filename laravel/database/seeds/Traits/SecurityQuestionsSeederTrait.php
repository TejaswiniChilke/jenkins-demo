<?php
namespace Uab\Database\Seeds\Traits;

use Uab\Http\Models\SecurityQuestions;

trait SecurityQuestionsSeederTrait {
    /**
     * @return void
     */
    public function createSecurityQuestions() {
        $questions = [
            'What is the model of the first car',
            'What is your maiden\'s name?',
            'What was the name of the first school you attended?',
            'Where were you born?',
            'Who was your first crush/boyfriend/girlfriend?',
        ];

        foreach ($questions as $question) {
            $securityQuestion = new SecurityQuestions();
            $securityQuestion->fill(
                [
                    'question' => $question
                ]
            );
            $securityQuestion->save();
        }
    }
}
