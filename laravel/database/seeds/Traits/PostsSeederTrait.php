<?php
namespace Uab\Database\Seeds\Traits;

use Uab\Http\Models\Posts;
use Uab\Http\Models\Users;
use Uab\Providers\FriendServiceProvider;
use Uab\Providers\LoremIpsumServiceProvider;

trait PostsSeederTrait {
    /**
     * @return void
     */
    public function createPosts() {
        $this->createProfilePosts();
        $this->createPostsToFriends();
    }

    /**
     * @return void
     */
    public function createProfilePosts() {
        $loremService = new LoremIpsumServiceProvider();

        /** @var Users[] $users */
        $users = Users::query()->get();

        foreach ($users as $user) {
            $count = rand(0, 50);

            for ($i = 0; $i < $count; $i++) {
                $sentences = rand(0, 100) > 85 ? rand(1, 3) : 1;

                $post = new Posts();

                $post->from_user_id =  $user->id;
                $post->to_user_id =  $user->id;
                $post->message = $loremService->get($sentences);
                $post->type = 'text';

                $post->save();
            }
        }
    }

    /**
     * @return void
     */
    public function createPostsToFriends() {
        $friendService = new FriendServiceProvider();
        $loremService = new LoremIpsumServiceProvider();

        /** @var Users[] $users */
        $users = Users::query()->get();

        foreach ($users as $user)  {
            $friends = $friendService->getFriends($user);

            foreach ($friends as $friend) {
                $count = rand(0, 10);

                for ($i = 0; $i < $count; $i++) {
                    $sentences = rand(0, 100) > 85 ? rand(1, 3) : 1;

                    $post = new Posts();

                    $post->from_user_id = $user->id;
                    $post->to_user_id = $friend->id;
                    $post->message = $loremService->get($sentences);
                    $post->type = 'text';

                    $post->save();
                }
            }
        }
    }
}
