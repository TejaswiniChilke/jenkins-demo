<?php
namespace Uab\Database\Seeds\Traits;

use Illuminate\Support\Arr;
use Uab\Http\Enums\AnswerInputTypesEnum;
use Uab\Http\Models\AnswerInputTypes;
use Uab\Http\Models\AnswerOptions;
use Uab\Http\Models\AnswerOptionsTags;
use Uab\Http\Models\Answers;
use Uab\Http\Models\Questions;
use Uab\Http\Models\SurveyPages;
use Uab\Http\Models\Surveys;
use Uab\Http\Models\SurveysUsers;
use Uab\Http\Models\Tags;

trait SurveysSeederTrait {
    private $answerInputTypes = [];

    /**
     * @return void
     */
    public function clearSurveys() {
        $this->clearSurveysFromUsers();

        AnswerOptions::forceTruncate();
        AnswerOptionsTags::forceTruncate();

        Questions::forceTruncate();

        SurveyPages::forceTruncate();

        Surveys::forceTruncate();
    }

    /**
     * @return void
     */
    public function clearSurveysFromUsers() {
        Answers::forceTruncate();
        SurveysUsers::forceTruncate();
    }

    /**
     * @return void
     */
    public function createAnswerInputTypes() {
        foreach (AnswerInputTypesEnum::getAll() as $type) {
            $answerInputType = new AnswerInputTypes();
            $answerInputType->fill(
                [
                    'type' => $type
                ]
            );
            $answerInputType->save();
        }
    }

    /**
     * @param $type
     *
     * @return AnswerInputTypes|null
     */
    public function findAnswerInputType($type) {
        if (!array_key_exists($type, $this->answerInputTypes)) {
            $tag = AnswerInputTypes::query()
                ->where(
                    [
                        'type' => $type
                    ]
                )->first();

            $this->answerInputTypes[$type] = $tag;
        }

        return $this->answerInputTypes[$type];
    }

    public function createSurvey($surveyArray) {
        $pages = $surveyArray['pages'];

        unset($surveyArray['pages']);

        $survey = new Surveys();
        $survey->setRawAttributes($surveyArray);
        $survey->save();

        $this->createPages($survey, $pages);
    }

    public function createPages($survey, $pagesArray) {
        $pages = [];

        foreach ($pagesArray as $pageI => $pageData) {
            $questions = $pageData['questions'];
            unset($pageData['questions']);

            $page = new SurveyPages();
            $page->setRawAttributes($pageData);
            $page->survey_id = $survey->id;
            $success = $page->save();

            if ($success) {
                $pages[] = $page;

                $this->createQuestions($page, $questions);
            } else {
                echo 'Error creating page ('.$pageI.').';
            }
        }
    }

    public function createQuestions($page, $questionsArray) {
        $questions = [];

        foreach ($questionsArray as $questionI => $questionArray) {
            $questionData = $questionArray;
            unset($questionData['answers']);

            if (!array_key_exists('position', $questionData)) {
                $questionData['position'] = $questionI;
            }

            $question = new Questions();
            $question->setRawAttributes($questionData);
            $question->survey_page_id = $page->id;
            $success = $question->save();

            if ($success) {
                $questions[] = $question;
            } else {
                echo 'Error creating question (' . $questionI . ').';
            }
        }

        foreach ($questionsArray as $questionI => $questionData) {
            $answers = Arr::get($questionData, 'answers', []);

            $this->createAnswerOptions($questions, $questions[$questionI], $answers);
        }
    }

    public function createAnswerOptions($questions, $question, $answersData) {
        $answers = [];

        foreach ($answersData as $answerI => $answerData) {
            $answerData['parent_question_id'] = $question->id;

            if (array_key_exists('next_question_position', $answerData)) {
                $position = $answerData['next_question_position'];
                unset($answerData['next_question_position']);

                $answerData['next_question_id'] = $questions[$position]->id;
            } else {
                if (count($questions) > ($question->position + 1)) {
                    $answerData['next_question_id'] = $questions[$question->position + 1]->id;
                }
            }

            $tagArrays = [];
            if (array_key_exists('tags', $answerData)) {
                $tagArrays = $answerData['tags'];
            }

            unset($answerData['tags']);

            $answer = new AnswerOptions();
            $answer->setRawAttributes($answerData);
            $success = $answer->save();

            if ($success) {
                $answers[] = $answer->id;

                foreach ($tagArrays as $tagName) {
                    $tag = $this->findOrCreateTags($tagName);

                    $answerOptionTag = new AnswerOptionsTags();
                    $answerOptionTag->answer_option_id = $answer->id;
                    $answerOptionTag->tag_id = $tag->id;
                    $answerOptionTag->save();
                }
            } else {
                echo 'Error creating answer option ('.$answerI.').';
            }
        }
    }

    /**
     * @param string|string[] $names
     *
     * @return Tags|Tags[]
     */
    public function findOrCreateTags($names) {
        $single = false;

        if (!is_array($names)) {
            $single = true;
            $names = [
                $names
            ];
        }

        $tags = [];

        foreach ($names as $name) {
            $tag = Tags::query()
                ->where(
                    [
                        'name' => $name
                    ]
                )->first();

            if (is_null($tag)) {
                $tag = new Tags();
                $tag->fill(
                    [
                        'name' => $name
                    ]
                );
                $tag->save();
            }

            $tags[] = $tag;
        }

        return $single ? $tags[0] : $tags;
    }
}
