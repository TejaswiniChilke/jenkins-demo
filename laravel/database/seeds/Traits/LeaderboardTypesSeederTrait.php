<?php
namespace Uab\Database\Seeds\Traits;

use Uab\Http\Models\LeaderboardTypes;

trait LeaderboardTypesSeederTrait {
    /**
     * @return void
     */
    public function createLeaderboardTypes() {
        $leaderboardTypeDetails = [
            [
                'type'   => 'Average Heart Rate',
                'column' => 'average_heart_rate',
                'unit'   => 'bpm'
            ],
            [
                'type'   => 'Calories',
                'column' => 'calories',
                'unit'   => 'calories'
            ],
            [
                'type'   => 'Distance',
                'column' => 'distance',
                'unit'   => 'miles'
            ],
            [
                'type'   => 'Speed',
                'column' => 'speed',
                'unit'   => 'mph'
            ],
            [
                'type'   => 'Steps/Pushes',
                'column' => 'steps',
                'unit'   => 'steps'
            ],
            [
                'type'   => 'Active Minutes',
                'column' => 'active_duration',
                'unit'   => 'minutes'
            ],
        ];

        $this->createLeaderboardTypesFromArray($leaderboardTypeDetails);
    }

    /**
     * @param array $leaderboardTypeDetails
     */
    public function createLeaderboardTypesFromArray(array $leaderboardTypeDetails):void {
        foreach ($leaderboardTypeDetails as $details) {
            $leaderboardType = new LeaderboardTypes();
            $leaderboardType->fill($details);
            $leaderboardType->save();
        }
    }
}
