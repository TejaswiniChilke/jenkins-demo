<?php
namespace Uab\Database\Seeds\Traits;

use Illuminate\Support\Str;
use Uab\Http\Models\Articles;
use Uab\Http\Models\ArticlesComments;
use Uab\Http\Models\ArticlesTags;
use Uab\Http\Models\ArticlesUsers;
use Uab\Http\Models\ArticlesUserTypes;
use Uab\Http\Models\Tags;
use Uab\Providers\LoremIpsumServiceProvider;

trait ArticlesSeederTrait {
    private $tags = null;

    /**
     * @return void
     */
    public function clearArticles() {
        $this->clearArticleComments();
        $this->clearArticlesFromUsers();

        ArticlesTags::forceTruncate();
        ArticlesUserTypes::forceTruncate();
        Articles::forceTruncate();
    }

    /**
     * @return void
     */
    public function clearArticleComments() {
        ArticlesComments::forceTruncate();
    }

    public function clearArticlesFromUsers() {
        ArticlesUsers::forceTruncate();
    }

    /**
     * @param integer $weeks
     *
     * @return void
     */
    public function createWeeklyArticles($weeks = 16) {
        $loremService = new LoremIpsumServiceProvider();

        for ($week = 1; $week <= $weeks; $week++)  {
            $article = new Articles();
            $article->title = 'Week '.$week;
            $article->week = $week;
            $article->end_week = $weeks;
            $article->description = $loremService->get(1);
            $article->content = $loremService->get(50);
            $article->short_description = $loremService->get(1);
        }
    }

    /**
     * @return void
     */
    public function createArticlesFromTags() {
        $loremService = new LoremIpsumServiceProvider();

        $tags = $this->getTags();

        foreach ($tags as $tag) {
            $article = new Articles();
            $article->title = 'Tag '.Str::title($tag->name);
            $article->week = 0;
            $article->end_week = null;
            $article->description = $loremService->get(1);
            $article->content = $loremService->get(50);
            $article->short_description = $loremService->get(1);
        }
    }

    private function getTags() {
        if (is_null($this->tags)) {
            $this->tags = Tags::get();
        }

        return $this->tags;
    }
}
