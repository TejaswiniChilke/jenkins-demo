<?php
namespace Uab\Database\Seeds\Traits;

use Exception;
use Uab\Http\Models\Features;
use Uab\Http\Models\FeaturesUserTypes;
use Uab\Http\Models\UserTypes;
use \Uab\Http\Enums\UserTypesEnum as UserTypesEnum;

trait FeaturesSeederTrait {
    /**
     * @return void
     */
    public function clearFeatures() {
        FeaturesUserTypes::forceTruncate();
        Features::forceTruncate();
    }

    /**
     * @return void
     */
    public function createFeatures() {
        $featuresDetails = [
            [
                'name'        => 'Admin',
                'description' => 'Adds a locked page for admins to control user settings and information.',
                'sref'        => 'admin',
                'icon'        => 'admin',
                'key'         => 'admin',
            ],
            [
                'name'        => 'Application Management',
                'description' => 'Gives the user the ability to Approve or Deny applications.',
                'sref'        => 'application-management',
                'icon'        => 'application-management',
                'key'         => 'application-management',
            ],
            [
                'name'        => 'Apply',
                'description' => 'A page for applying to the study.',
                'sref'        => 'apply',
                'icon'        => 'apply',
                'key'         => 'apply',
            ],
            [
                'name'        => 'Articles (All)',
                'description' => 'Adds a list of articles that the user can read.',
                'sref'        => 'article-list',
                'icon'        => 'article-list',
                'key'         => 'article-list'
            ],
            [
                'name'        => 'Articles (by Tags)',
                'description' => 'Gives the user a list of articles based on the user\'s tags and week number.',
                'sref'        => 'article-list-matching',
                'icon'        => 'article-list',
                'key'         => 'article-list-matching',
            ],
            [
                'name'        => 'Articles (by Week)',
                'description' => 'Gives the user a list of articles based on the user\'s week number.',
                'sref'        => 'article-list-week',
                'icon'        => 'article-list',
                'key'         => 'article-list-week',
            ],
            [
                'name'        => 'Article Management',
                'description' => 'Gives the user the ability to view, create, edit, and delete articles.',
                'sref'        => 'article-management',
                'icon'        => 'article-management',
                'key'         => 'article-management',
            ],
            [
                'name'        => 'Article Analytic Management',
                'description' => 'Gives the user the ability to view and manage article analytics.',
                'sref'        => 'article-analytic-management',
                'icon'        => 'article-analytic-management',
                'key'         => 'article-analytic-management',
            ],
            [
                'name'        => 'Article User Type Management',
                'description' => 'Gives the user the ability to assign articles to users based on user type.',
                'sref'        => 'article-user-type-management',
                'icon'        => 'article-user-type-management',
                'key'         => 'article-user-type-management',
            ],
            [
                'name'        => 'Badges',
                'description' => 'Gives the user the ability to earn badges.',
                'sref'        => 'badge-list',
                'icon'        => 'badge-list',
                'key'         => 'badge-list',
            ],
            [
                'name'        => 'Badge Management',
                'description' => 'Gives the user the ability to view, create, edit, and delete badges.',
                'sref'        => 'badge-management',
                'icon'        => 'badge-management',
                'key'         => 'badge-management',
            ],
            [
                'name'        => 'Blacklisted Word Management',
                'description' => 'Gives the user the ability to view, create, edit, and delete blacklisted words.',
                'sref'        => 'blacklisted-word-management',
                'icon'        => 'blacklisted-word-management',
                'key'         => 'blacklisted-word-management',
            ],
            [
                'name'        => 'Calendar',
                'description' => 'Adds a calendar the user can use to keep track of events.',
                'sref'        => 'calendar',
                'icon'        => 'calendar',
                'key'         => 'calendar'
            ],
            [
                'name'        => 'Chat',
                'description' => 'Gives the user a chat page.',
                'sref'        => 'chat',
                'icon'        => 'chat',
                'key'         => 'chat',
            ],
            [
                'name'        => 'Color Management',
                'description' => 'Gives the user the ability to view, create, edit, and delete the project\'s color template.',
                'sref'        => 'color-management',
                'icon'        => 'color-management',
                'key'         => 'color-management',
            ],
            [
                'name'        => 'Contact Preferences',
                'description' => 'Lets the user set their contact preferences.',
                'sref'        => 'contact-preferences',
                'icon'        => 'settings',
                'key'         => 'contact-preferences',
            ],
            [
                'name'        => 'Contact Staff',
                'description' => 'Gives the user the ability to contact the admins.',
                'sref'        => 'contact-staff',
                'icon'        => 'contact-staff',
                'key'         => 'contact-staff',
            ],
            [
                'name'        => 'Contact Staff Message List',
                'description' => 'Gives the user the ability to see messages to staff.',
                'sref'        => 'contact-staff-message-list',
                'icon'        => 'contact-staff',
                'key'         => 'contact-staff-message-list',
            ],
            [
                'name'        => 'Current Playlist',
                'description' => 'Takes the user directly to their current playlist.',
                'sref'        => 'current-playlist',
                'icon'        => 'playlist-list',
                'key'         => 'current-playlist',
            ],
            [
                'name'        => 'Debug Variables',
                'description' => 'Gives the users the ability to change variables that help with testing.',
                'sref'        => 'debugVariables',
                'icon'        => 'debug-variables',
                'key'         => 'debug-variables',
            ],
            [
                'name'        => 'Email Management',
                'description' => 'Gives the user the ability to view and send emails',
                'sref'        => 'email-management',
                'icon'        => 'email-management',
                'key'         => 'email-management',
            ],
            [
                'name'        => 'Help Management',
                'description' => 'Gives the user the ability to add, delete, or edit help sections.',
                'sref'        => 'help-management',
                'icon'        => 'help-management',
                'key'         => 'help-management',
            ],
            [
                'name'        => 'Inbox',
                'description' => 'Adds the ability for users to send and recieve private messages.',
                'sref'        => 'inbox',
                'icon'        => 'inbox',
                'key'         => 'inbox',
                'socket'      => 'inbox',
            ],
            [
                'name'        => 'Instructor Videos',
                'description' => '',
                'sref'        => 'instructor-video-list',
                'icon'        => 'videos',
                'key'         => 'instructor-video-list',
            ],
            [
                'name'        => 'Leaderboard',
                'description' => 'Adds a leaderboard for user\'s to compare fitbit data.',
                'sref'        => 'leaderboard',
                'icon'        => 'leaderboard',
                'key'         => 'leaderboard',
            ],
            [
                'name'        => 'Logout',
                'description' => 'Gives the user the ability to logout.',
                'sref'        => 'logout',
                'icon'        => 'logout',
                'key'         => 'logout',
            ],
            [
                'name'        => 'Newsfeed',
                'description' => 'Displays posts to the user from other users in the project.',
                'sref'        => 'newsfeed',
                'icon'        => 'newsfeed',
                'key'         => 'newsfeed',
            ],
            [
                'name'        => 'Notes',
                'description' => 'Ability to write notes.',
                'sref'        => 'notes',
                'icon'        => 'notes',
                'key'         => 'notes',
            ],
            [
                'name'        => 'Playlists',
                'description' => 'List of playlists.',
                'sref'        => 'playlist-list',
                'icon'        => 'playlists',
                'key'         => 'playlist-list',
            ],
            [
                'name'        => 'Playlists (Archive)',
                'description' => 'Displays the expired playlists.',
                'sref'        => 'playlist-archive-list',
                'icon'        => 'playlist-list',
                'key'         => 'playlist-archive-list',
            ],
            [
                'name'        => 'Playlist Management',
                'description' => 'Gives the user the ability to view, create, edit, and delete playlists.',
                'sref'        => 'playlist-management',
                'icon'        => 'playlist-management',
                'key'         => 'playlist-management',
            ],
            [
                'name'        => 'Profile',
                'description' => 'Gives the user a public profile.',
                'sref'        => 'profile',
                'icon'        => 'profile',
                'key'         => 'profile',
            ],
            [
                'name'        => 'Report Bug',
                'description' => 'Allows users to submit bug reports. The report collects data about their app ussage to help find and fix the problem.',
                'sref'        => 'bug-report',
                'icon'        => 'report-bug',
                'key'         => 'bug-report',
            ],
            [
                'name'        => 'Section Header',
                'description' => 'Add section header to list of features in the menu.',
                'sref'        => '',
                'icon'        => null,
                'key'         => 'section-header',
            ],
            [
                'name'        => 'Settings',
                'description' => 'Gives the user the ability to change settings. Participants will only see their user specific settings, not group or project.',
                'sref'        => 'settings',
                'icon'        => 'settings',
                'key'         => 'settings',
            ],
            [
                'name'        => 'Surveys',
                'description' => 'Adds surveys that the user can answer.',
                'sref'        => 'survey-list',
                'icon'        => 'survey-list',
                'key'         => 'survey-list'
            ],
            [
                'name'        => 'Survey Management',
                'description' => 'Gives the user the ability to add, delete, or edit surveys, questions, and answer options.',
                'sref'        => 'survey-management',
                'icon'        => 'survey-management',
                'key'         => 'survey-management',
            ],
            [
                'name'        => 'Users (All)',
                'description' => 'List of all users in the project. Gives the user an easy way to find friends.',
                'sref'        => 'user-list',
                'icon'        => 'user-list',
                'key'         => 'user-list',
            ],
            [
                'name'        => 'User Management',
                'description' => 'Gives the user the ability to view, create, edit, and delete users.',
                'sref'        => 'user-management',
                'icon'        => 'user-management',
                'key'         => 'user-management',
            ],
            [
                'name'        => 'Failed Applicants',
                'description' => 'Gives the user the ability to view failed applicantsfail-user-management.',
                'sref'        => 'fail-user-management',
                'icon'        => 'user-management',
                'key'         => 'fail-user-management',
            ],
            [
                'name'        => 'User Type Management',
                'description' => 'Gives the user the ability to view, create, edit, and delete user types.',
                'sref'        => 'user-type-management',
                'icon'        => 'user-type-management',
                'key'         => 'user-type-management',
            ],
            [
                'name'        => 'Videos',
                'description' => 'List of videos.',
                'sref'        => 'video-list',
                'icon'        => 'videos',
                'key'         => 'video-list'
            ],
            [
                'name'        => 'Help',
                'description' => 'Gives the user a page for viewing videos and articles tagged as \'help-page\'',
                'sref'        => 'help',
                'icon'        => 'help',
                'key'         => 'help',
            ],
            [
                'name'        => 'Home',
                'description' => 'A home page with relevant information for the users such as recent notifications, articles, and videos.',
                'sref'        => 'home',
                'icon'        => 'home',
                'key'         => 'home',
            ],
            [
                'name'        => 'Video Management',
                'description' => 'Gives the user the ability to view, create, edit, and delete videos.',
                'sref'        => 'video-management',
                'icon'        => 'video-management',
                'key'         => 'video-management',
            ],
            [
                'name'        => 'Friends',
                'description' => 'Gives the user a page for viewing their friends',
                'sref'        => 'friend-list',
                'icon'        => 'friend-list',
                'key'         => 'friend-list',
            ],
            [
                'name'        => 'Friend and Requests',
                'description' => 'Gives the user a page for viewing their friends and friend requests.',
                'sref'        => 'friend-and-request-list',
                'icon'        => 'friend-and-request-list',
                'key'         => 'friend-and-request-list',
            ],
            [
                'name'        => 'Horizon',
                'description' => 'Gives the user access to Laravel Horizon.',
                'sref'        => 'horizon',
                'icon'        => 'horizon',
                'key'         => 'horizon',
            ],
            [
                'name'        => 'Notification Management',
                'description' => 'Gives the user the ability to view and send notifications.',
                'sref'        => 'notification-management',
                'icon'        => 'notification-management',
                'key'         => 'notification-management',
            ],
            [
                'name'        => 'Notification List',
                'description' => 'Gives the user a page for viewing their notifications.',
                'sref'        => 'notifications',
                'icon'        => 'notifications',
                'key'         => 'notifications',
                'socket'      => 'notification',
            ],
            [
                'name'        => 'Playlists by Tags',
                'description' => 'Gives the user a list of playlists based on the user\'s tags and week number.',
                'sref'        => 'playlist-list-matching',
                'icon'        => 'playlists',
                'key'         => 'playlist-list-matching',
            ],
            [
                'name'        => 'Playlists by Week',
                'description' => 'Gives the user a list of articles based on the user\'s week number.',
                'sref'        => 'playlist-list-week',
                'icon'        => 'playlists',
                'key'         => 'playlist-list-week',
            ],
            [
                'name'        => 'Security Question Management',
                'description' => 'Gives the user the ability to manage security questions.',
                'sref'        => 'security-question-management',
                'icon'        => 'security-question',
                'key'         => 'security-question-management',
            ],
            [
                'name'        => 'State Analytic Management',
                'description' => 'Gives the user the ability to view state analytics.',
                'sref'        => 'state-analytic-management',
                'icon'        => 'state-analytic-management',
                'key'         => 'state-analytic-management',
            ],
            [
                'name'        => 'Surveys by Week',
                'description' => 'Gives the user a list of surveys based on their week.',
                'sref'        => 'survey-list-week',
                'icon'        => 'survey-list',
                'key'         => 'survey-list-week',
            ],
            [
                'name'        => 'Sql Log Management',
                'description' => 'Gives the user the ability to view the SQL log.',
                'sref'        => 'sql-log-management',
                'icon'        => 'sql-log-management',
                'key'         => 'sql-log-management',
            ],
            [
                'name'        => 'Tag Management',
                'description' => 'Gives the user the ability to manage tags.',
                'sref'        => 'tag-management',
                'icon'        => 'tag-management',
                'key'         => 'tag-management',
            ],
            [
                'name'        => 'User Analytic Management',
                'description' => 'Gives access to analytic management.',
                'sref'        => 'user-analytic-management',
                'icon'        => 'user-analytic-management',
                'key'         => 'user-analytic-management',
            ],
            [
                'name'        => 'Videos by Tags',
                'description' => 'Gives the user a list of videos based on their tags.',
                'sref'        => 'video-list-matching',
                'icon'        => 'videos',
                'key'         => 'video-list-matching',
            ],
            [
                'name'        => 'Videos by Week',
                'description' => 'Gives the user a list of videos based on their week.',
                'sref'        => 'video-list-week',
                'icon'        => 'videos',
                'key'         => 'video-list-week',
            ],
            [
                'name'        => 'Video Analytic Management',
                'description' => 'Gives the user access to video analytics.',
                'sref'        => 'video-analytic-management',
                'icon'        => 'video-analytic-management',
                'key'         => 'video-analytic-management',
            ],
            [
                'name'        => 'Wiki',
                'description' => 'Redirects to the NCHPAD wiki.',
                'sref'        => 'http://www.wiki.nchpad.org',
                'icon'        => 'wiki',
                'key'         => 'wiki',
            ],
            [
                'name'        => 'Horizon',
                'description' => 'Gives the user access to Laravel Horizon.',
                'sref'        => '',
                'icon'        => 'horizion',
                'key'         => 'horizion',
            ],
            [
                'name'        => 'Telescope',
                'description' => 'Gives the user access to Laravel telescope.',
                'sref'        => '',
                'icon'        => 'telescope',
                'key'         => 'telescope',
            ],
            [
                'name'        => 'Jenkins',
                'description' => 'Redirects to Jenkins.',
                'sref'        => 'http://www.nchpad.org',
                'icon'        => 'jenkins',
                'key'         => 'jenkins',
            ],
            [
                'name'        => 'Approved User Management',
                'description' => 'Gives the user access to the approved user management page.',
                'sref'        => 'approved-user-management',
                'icon'        => 'user-management',
                'key'         => 'approved-user-management',
            ],
            [
                'name'        => 'Contact Staff Messages',
                'description' => 'Gives all the messages from staff',
                'sref'        => 'contact-staff',
                'icon'        => 'contact-staff',
                'key'         => 'contact-staff'
            ],
            [
                'name'        => 'Prequel',
                'description' => 'Laravel Prequel',
                'sref'        => '',
                'icon'        => 'prequel',
                'key'         => 'prequel'
            ],
            [
            'name'        => 'Videos by Tags',
            'description' => '',
            'sref'        => 'video-list-by-tags',
            'icon'        => 'videos',
            'key'         => 'video-list-by-tags',
        ],
        ];

        $this->createFeaturesFromArray($featuresDetails);
    }

    /**
     * @param array $featuresDetails
     */
    public function createFeaturesFromArray(array $featuresDetails):void {
        foreach ($featuresDetails as $details) {
            $feature = new Features();
            $feature->fill($details);
            $feature->save();
        }
    }

    /**
     * @throws Exception
     */
    public function addFeaturesToUserTypes() {
        $this->addAdminFeatures();
        $this->addParticipantFeatures();
        $this->addSuperAdminFeatures();
    }

    /**
     * @throws Exception
     */
    public function addAdminFeatures() {
        $data = [
            UserTypesEnum::ADMIN => [
                // Participants
                'Admin' => 'admin',
                'Participant Features' => 'section-header',
                'Home' => 'home',
                'Articles (All)' => 'article-list',
                'Articles by Tag' => 'article-list-matching',
                'Articles by Week' => 'article-list-week',
                'Badges' => 'badge-list',
                'Calendar' => 'calendar',
                'Chat' => 'chat',
                'Contact Preferences' => 'contact-preferences',
                'Contact Staff' => 'contact-staff',
                'Help' => 'help',
                'Friends' => 'friend-list',
                'Leaderboard' => 'leaderboard',
                'Messages' => 'inbox',
                'Notifications' => 'notifications',
                'Playlist (Current)' => 'current-playlist',
                'Playlists (All)' => 'playlist-list',
                'Playlists by Tag' => 'playlist-list-matching',
                'Playlists by Week' => 'playlist-list-week',
                'Playlists Archive' => 'playlist-archive-list',
                'Profile' => 'profile',
                'Report a Bug' => 'bug-report',
                'Surveys (All)' => 'survey-list',
                'Surveys by Week' => 'survey-list-week',
                'Users (All)' => 'user-list',
                'Videos (All)' => 'video-list',
                'Videos by Tag' => 'video-list-matching',
                'Videos by Week' => 'video-list-week',

                // Administrator
                'Administration' => 'section-header',
                'Applications' => 'application-management',
                'Contact Staff Messages' => 'contact-staff-message-list',
                'Newsfeed' => 'Newsfeed',
                'Notes' => 'notes',
                'Settings' => 'settings',

                // Analytics
                'Analytics' => 'section-header',
                'Article Analytics' => 'article-analytic-management',
                'State Analytics' => 'state-analytic-management',
                'Video Analytics' => 'video-analytic-management',

                // Content Management
                'Content Management' => 'section-header',
                'Article Management' => 'article-management',
                'Assign Articles to User Types' => 'article-user-type-management',
                'Badge Management' => 'badge-management',
                'Blacklisted Word Management' => 'blacklisted-word-management',
                'Color Management' => 'color-management',
                'Email Management' => 'email-management',
                'Hangout Management' => 'hangout-management',
                'Help Management' => 'help-management',
                'Notification Management' => 'notification-management',
                'Playlist Management' => 'playlist-management',
                'Security Question Management' => 'security-question-management',
                'Survey Management' => 'survey-management',
                'Tag Management' => 'tag-management',
                'User Management' => 'user-management',
                'User Type Management' => 'user-type-management',
                'Video Management' => 'video-management',

                // Bottom
//                'Logout' => 'logout',
            ],
        ];

        $this->createFeatureUserTypesFromArray($data);
    }

    /**
     * @throws Exception
     */
    public function addParticipantFeatures() {
        $data  = [
            'participant' => [
                // Participants
                'Admin' => 'admin',
                'Participant Features' => 'section-header',
                'Home' => 'home',
                'Articles (All)' => 'article-list',
                'Articles by Tag' => 'article-list-matching',
                'Articles by Week' => 'article-list-week',
                'Badges' => 'badge-list',
                'Calendar' => 'calendar',
                'Chat' => 'chat',
                'Contact Preferences' => 'contact-preferences',
                'Contact Staff' => 'contact-staff',
                'Help' => 'help',
                'Friends' => 'friend-list',
                'Leaderboard' => 'leaderboard',
                'Messages' => 'inbox',
                'Notifications' => 'notifications',
                'Playlist (Current)' => 'current-playlist',
                'Playlists (All)' => 'playlist-list',
                'Playlists by Tag' => 'playlist-list-matching',
                'Playlists by Week' => 'playlist-list-week',
                'Playlists Archive' => 'playlist-archive-list',
                'Profile' => 'profile',
                'Report a Bug' => 'bug-report',
                'Surveys (All)' => 'survey-list',
                'Surveys by Week' => 'survey-list-week',
                'Users (All)' => 'user-list',
                'Videos (All)' => 'video-list',
                'Videos by Tag' => 'video-list-matching',
                'Videos by Week' => 'video-list-week',

                // Bottom
//                'Logout' => 'logout',
            ],
        ];

        $this->createFeatureUserTypesFromArray($data);
    }

    /**
     * @throws Exception
     */
    public function addSuperAdminFeatures() {
        $data = [
            UserTypesEnum::SUPER_ADMIN => [
                // Participants
                'Admin' => 'admin',
                'Participant Features' => 'section-header',
                'Home' => 'home',
                'Articles (All)' => 'article-list',
                'Articles by Tag' => 'article-list-matching',
                'Articles by Week' => 'article-list-week',
                'Badges' => 'badge-list',
                'Calendar' => 'calendar',
                'Chat' => 'chat',
                'Contact Preferences' => 'contact-preferences',
                'Contact Staff' => 'contact-staff',
                'Help' => 'help',
                'Friends' => 'friend-list',
                'Leaderboard' => 'leaderboard',
                'Messages' => 'inbox',
                'Notifications' => 'notifications',
                'Playlist (Current)' => 'current-playlist',
                'Playlists (All)' => 'playlist-list',
                'Playlists by Tag' => 'playlist-list-matching',
                'Playlists by Week' => 'playlist-list-week',
                'Playlists Archive' => 'playlist-archive-list',
                'Profile' => 'profile',
                'Report a Bug' => 'bug-report',
                'Surveys (All)' => 'survey-list',
                'Surveys by Week' => 'survey-list-week',
                'Users (All)' => 'user-list',
                'Videos (All)' => 'video-list',
                'Videos by Tag' => 'video-list-matching',
                'Videos by Week' => 'video-list-week',

                // Administrator
                'Administration'         => 'section-header',
                'Applications'           => 'application-management',
                'Contact Staff Messages' => 'contact-staff-message-list',
                'Newsfeed'               => 'newsfeed',
                'Notes'                  => 'notes',
                'Settings'               => 'settings',

                // Analytics
                'Analytics'         => 'section-header',
                'Article Analytics' => 'article-analytic-management',
                'State Analytics'   => 'state-analytic-management',
                'Video Analytics'   => 'video-analytic-management',

                // Content Management
                'Content Management'            => 'section-header',
                'Article Management'            => 'article-management',
                'Assign Articles to User Types' => 'article-user-type-management',
                'Badge Management'              => 'badge-management',
                'Blacklisted Word Management'   => 'blacklisted-word-management',
                'Color Management'              => 'color-management',
                'Email Management'              => 'email-management',
                'Event Management'              => 'event-management',
                'Hangout Management'            => 'hangout-management',
                'Help Management'               => 'help-management',
                'Notification Management'       => 'notification-management',
                'Playlist Management'           => 'playlist-management',
                'Security Question Management'  => 'security-question-management',
                'Survey Management'             => 'survey-management',
                'Tag Management'                => 'tag-management',
                'User Management'               => 'user-management',
                'User Type Management'          => 'user-type-management',
                'Video Management'              => 'video-management',
                'Zoom Meeting Management'       => 'zoom-meeting-management',

                // Super
                'Super Admin'     => 'section-header',
                'Debug Variables' => 'debug-variables',
                'Horizon'         => 'horizon',
                'Jenkins'         => 'jenkins',
                'Prequel'         => 'prequel',
                'SQL Logs'        => 'sql-log-management',
                'Telescope'       => 'telescope',
                'Wiki'            => 'wiki',

                // Bottom
//                'Logout' => 'logout',
            ]
        ];

        $this->createFeatureUserTypesFromArray($data);
    }

    /**
     * @param $data
     *
     * @throws Exception
     */
    public function createFeatureUserTypesFromArray($data) {
        foreach ($data as $userType => $features) {
            /** @var UserTypes $userType */
            $userType = UserTypes::where(
                [
                    'name' => $userType
                ]
            )->first();

            $count = 0;

            foreach ($features as $name => $key) {
                /** @var Features $feature */
                $feature = Features::where(
                    [
                        'key' => $key
                    ]
                )->first();

                if (is_null($feature)) {
                    throw new Exception(
                        'Feature not already created ('.$key.').'
                    );
                }

                $featureUserType = new FeaturesUserTypes();
                $featureUserType->feature_id = $feature->id;
                $featureUserType->user_type_id = $userType->id;
                $featureUserType->position = $count;
                $featureUserType->name = $name;

                $featureUserType->save();

                $count++;
            }
        }
    }
}
