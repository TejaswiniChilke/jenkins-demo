<?php
namespace Uab\Database\Seeds\Traits;

use Uab\Http\Models\Projects;

trait ProjectsSeederTrait {
    public $project;

    /**
     * @param string $name
     *
     * @return void
     */
    public function createProject($name) {
        $this->project = new Projects();
        $this->project->fill(
            [
                'name' => $name
            ]
        );
        $this->project->save();
    }
}
