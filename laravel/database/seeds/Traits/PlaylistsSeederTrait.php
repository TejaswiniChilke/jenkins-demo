<?php
namespace Uab\Database\Seeds\Traits;

use Illuminate\Support\Str;
use Uab\Http\Models\Playlists;
use Uab\Http\Models\PlaylistsTags;
use Uab\Http\Models\PlaylistsVideos;
use Uab\Http\Models\Tags;
use Uab\Http\Models\Videos;
use Uab\Providers\LoremIpsumServiceProvider;
use Exception;

trait PlaylistsSeederTrait {
    /**
     * @param Playlists $playlist
     *
     * @throws Exception
     */
    public function addVideosToPlaylist($playlist) {
        $videos = rand(2, 5);

        for ($i = 0; $i < $videos; $i++) {
            $video = $following = Videos::inRandomOrder()->first();

            if (is_null($video)) {
                throw new Exception('No videos exist for weekly playlist.');
            }

            $playlistVideo = new PlaylistsVideos();
            $playlistVideo->playlist_id = $playlist->id;
            $playlistVideo->video_id = $video->id;
            $playlistVideo->position = $i;
            $playlistVideo->save();
        }
    }

    public function clearPlaylists() {
        PlaylistsTags::forceTruncate();
        PlaylistsVideos::forceTruncate();
        Playlists::forceTruncate();
    }

    /**
     * @param integer $weeks
     *
     * @return void
     *
     * @throws Exception
     */
    public function createWeeklyPlaylists($weeks = 16) {
        $loremService = new LoremIpsumServiceProvider();

        for ($week = 1; $week <= $weeks; $week++)  {
            $playlist = new Playlists();
            $playlist->title = 'Week '.$week;
            $playlist->week = $week;
            $playlist->end_week = $weeks;
            $playlist->description = $loremService->get(1);
            $playlist->short_description = $loremService->get(1);
            $success = $playlist->save();

            if (!$success) {
                throw new Exception('Could not create weekly playlist.');
            }

            $this->addVideosToPlaylist($playlist);
        }
    }

    /**
     * @throws Exception
     */
    public function createPlaylistsFromTags() {
        $loremService = new LoremIpsumServiceProvider();

        /** @var Tags[] $tags */
        $tags = Tags::get();

        foreach ($tags as $tag) {
            $playlist = new Playlists();
            $playlist->title = 'Tag '.Str::title($tag->name);
            $playlist->week = 0;
            $playlist->end_week = 1000;
            $playlist->description = $loremService->get(1);
            $playlist->short_description = $loremService->get(1);
            $success = $playlist->save();

            if (!$success) {
                throw new Exception(
                    'Could not create playlist for tag ('.$tag->name.')'
                );
            }

            $this->addVideosToPlaylist($playlist);
        }
    }
}
