<?php
namespace Uab\Database\Seeds\Traits;

use Uab\Http\Models\EmailAttachments;
use Uab\Http\Models\Emails;
use Uab\Http\Models\EmailsEmailAttachments;

trait EmailsSeederTrait {
    /**
     * @return void
     */
    public function clearEmails() {
        EmailsEmailAttachments::forceTruncate();
        EmailAttachments::forceTruncate();
        Emails::forceTruncate();
    }
}
