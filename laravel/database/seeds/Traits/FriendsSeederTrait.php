<?php
namespace Uab\Database\Seeds\Traits;

use Uab\Http\Models\Followers;
use Uab\Http\Models\Users;

trait FriendsSeederTrait {
    private $count = 0;
    private $exists = [];
    private $users = null;

    /**
     * @return void
     */
    public function createFollowers() {
        $users = $this->getUsers();

        foreach ($users as $user) {
            $followers = rand(1, 3);

            for ($i = 0; $i < $followers; $i++) {
                $follower = new Followers();
                $follower->user_id = $user->id;

                $following = $this->getRandomUser();
                if (!is_null($following)) {
                    $follower->following_id = $following->id;
                }
            }
        }
    }

    public function createFriends() {
        $users = $this->getUsers();

        foreach ($users as $user) {
            $friends = rand(1, 3);

            for ($i = 0; $i < $friends; $i++) {
                $following = $this->getRandomUser();

                if (!is_null($following)) {
                    $exists = $this->hasFollower($following->id, $user->id);

                    if ($exists) {
                        $follower = new Followers();
                        $follower->user_id = $user->id;
                        $follower->following_id = $following->id;
                        $follower->save();
                    }

                    $exists = $this->hasFollower($user->id, $following->id);

                    if ($exists) {
                        $follower = new Followers();
                        $follower->user_id = $following->id;
                        $follower->following_id = $user->id;
                        $follower->save();
                    }
                }
            }
        }
    }

    /**
     * @return Users
     */
    private function getRandomUser() {
        return $this->getUsers()[rand(0, $this->count - 1)];
    }

    /**
     * @return Users[]
     */
    private function getUsers() {
        if (is_null($this->users)) {
            $this->users = Users::get();

            $this->count = count($this->users);
        }

        return $this->users;
    }

    /**
     * @param integer $followingId
     * @param integer $userId
     *
     * @return boolean
     */
    private function hasFollower($followingId, $userId) {
        if (!array_key_exists($followingId, $this->exists)) {
            $this->exists[$followingId] = [$userId];
        }

        if (!array_key_exists($userId, $this->exists[$followingId])) {
            $this->exists[$followingId][$userId] = Followers::where(
                [
                    'following_id' => $followingId,
                    'user_id'      => $userId
                ]
            )->exists();
        }

        return $this->exists[$followingId][$userId];
    }
}
