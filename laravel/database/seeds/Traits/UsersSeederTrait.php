<?php
namespace Uab\Database\Seeds\Traits;

use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Support\Str;
use Uab\Http\Enums\UserStatusesEnum;
use Uab\Http\Models\SettingsUsers;
use Uab\Http\Models\TagsUsers;
use Uab\Http\Models\TipsUsers;
use Uab\Http\Models\Users;
use Uab\Http\Models\WeightLogs;

trait UsersSeederTrait {
    use ArticlesSeederTrait;
    use BadgesSeederTrait;
    use EventsSeederTrait;
    use TermsAndConditionsSeederTrait;

    private $count = 0;
    private $tags = null;

    public function clearUsers() {
        $this->clearArticlesFromUsers();
        $this->clearEventsFromUsers();
        $this->clearTermsAndConditionsFromUsers();

        SettingsUsers::forceTruncate();
        TipsUsers::forceTruncate();
        WeightLogs::forceTruncate();

        Users::forceTruncate();
    }

    public function createTestUsers() {
        $superAdminUserProperties = [
            [
                'username'          => 'jrquick',
                'nickname'          => 'jrquick',
                'email'             => 'jrquick@uab.edu',
                'password'          => 'admin',
                'first_name'        => 'Jeremy',
                'last_name'         => 'Quick',
                'mobile_number'     => '2053320786',
                'user_type_id'      => $this->superAdmin->id,
                'requires_approval' => false,
            ],
            [
                'username'          => 'sai',
                'nickname'          => 'sai',
                'email'             => 'kireeti7u@uab.edu',
                'password'          => 'admin',
                'first_name'        => 'Kireeti',
                'last_name'         => '',
                'mobile_number'     => '',
                'user_type_id'      => $this->superAdmin->id,
                'requires_approval' => false,
            ],
            [
                'username'          => 'venkat',
                'nickname'          => 'venkat',
                'email'             => 'vr1221@uab.edu',
                'password'          => 'admin',
                'first_name'        => 'Venkat',
                'last_name'         => '',
                'mobile_number'     => '',
                'user_type_id'      => $this->superAdmin->id,
                'requires_approval' => false,
            ]
        ];

        $adminUserProperties = [
            [
                'username'          => 'admin',
                'nickname'          => 'admin',
                'email'             => 'admin@test.com',
                'password'          => 'admin',
                'first_name'        => 'Admin',
                'last_name'         => 'Tester',
                'mobile_number'     => '',
                'user_type_id'      => $this->admin->id,
                'requires_approval' => false,
            ],
            [
                'username'          => 'mohan',
                'nickname'          => 'mohan',
                'email'             => 'mohan@uab.edu',
                'password'          => 'mohan',
                'first_name'        => 'Mohan',
                'last_name'         => '',
                'mobile_number'     => '',
                'user_type_id'      => $this->admin->id,
                'requires_approval' => false,
            ],
        ];

        $participantUserProperties = [
            [
                'username'          => 'participant',
                'nickname'          => 'participant',
                'email'             => 'participant@test.com',
                'password'          => 'participant',
                'first_name'        => 'Participant',
                'last_name'         => 'Johnson',
                'mobile_number'     => '2055550786',
                'user_type_id'      => $this->participant->id,
                'requires_approval' => true,
            ],
        ];

        $userProperties = array_merge(
            $superAdminUserProperties,
            $adminUserProperties,
            $participantUserProperties
        );

        $this->createUsersFromArray($userProperties);
    }

    /**
     * @return void
     */
    public function createUsers() {
        $this->createTestUsers();
//        $this->createStatusUsers();
//        $this->createWeeklyUsers();
    }

    public function createStatusUsers() {
        $faker = Factory::create();

        $users = [];

        $statuses = get_class_vars(UserStatusesEnum::class);
        foreach ($statuses as $status) {
            $isApproved = in_array(
                $status,
                [
                    UserStatusesEnum::APPLICATION_APPROVED,
                    UserStatusesEnum::NEED_PROFILE_COMPLETE,
                    UserStatusesEnum::PROFILE_COMPLETE
                ]
            );

            $users[] = [
                'username' => strtolower($status),
                'nickname' => strtolower($status),
                'email' => strtolower($status).'@test.com',
                'password' => 'participant',
                'first_name' => Str::title($status),
                'last_name' => Str::title($status),
                'mobile_number' => $faker->phoneNumber,
                'user_type_id' => $this->participant->id,
                'requires_approval' => true,
                'approved_on' => $isApproved ? Carbon::now()->subtract('weeks', 3) : null
            ];
        }

        $this->createUsersFromArray($users);
    }

    /**
     * @param array $userProperties
     *
     * @return Users[]
     */
    public function createUsersFromArray(array $userProperties) {
        $users = [];

        foreach ($userProperties as $properties) {
            $user = new Users();
            $user->fill($properties);
            $user->save();

            $users[] = $user;
        }

        return $users;
    }

    public function createWeeklyUsers() {
        $faker = Factory::create();

        $users = [];

        for ($i = 1; $i <= 20; $i++) {
            $users[] = [
                'username' => 'week' . $i,
                'nickname' => 'Week ' . $i . ' User',
                'email' => 'week'.$i.'@test.com',
                'password' => 'participant',
                'first_name' => 'Week',
                'last_name' => $i,
                'mobile_number' => $faker->phoneNumber,
                'user_type_id' => $this->participant->id,
                'requires_approval' => true,
                'approved_on' => Carbon::now()->subtract('weeks', $i)
            ];
        }

        $this->createUsersFromArray($users);
    }

    public function createApplicants() {
        $faker = Factory::create();

        $users = [];

        $statuses = [
            UserStatusesEnum::APPLICATION_APPROVED,
            UserStatusesEnum::APPLICATION_DENIED,
            UserStatusesEnum::APPLICATION_REJECTED_AGE,
            UserStatusesEnum::APPLICATION_REJECTED_SCI,
            UserStatusesEnum::APPLICATION_PENDING
        ];

        foreach ($statuses as $status) {
            $count = rand(3, 20);

            for ($i = 1; $i <= $count; $i++) {
                $fullname = $faker->name;

                $users[] = [
                    'username' => 'applicant-' . rand(),
                    'nickname' => 'applicant-' . rand(),
                    'email' => 'applicant-' . rand() . '@test.com',
                    'password' => 'participant',
                    'first_name' => explode(' ', $fullname)[0],
                    'last_name' => explode(' ', $fullname)[1],
                    'mobile_number' => $faker->phoneNumber,
                    'user_type_id' => $this->participant->id,
                    'requires_approval' => true,
                    'approved_on' => Carbon::now()->subtract('weeks', $i),
                    'status' => $status
                ];
            }
        }

        $this->createUsersFromArray($users);
    }

    public function createUsersWithTags() {
        $faker = Factory::create();

        $users = [];

        $count = rand(10, 20);

        for ($i = 1; $i <= $count; $i++) {
            $fullname = $faker->name;
            $username = Str::slug($fullname);

            $users[] = [
                'username' => $username,
                'nickname' => $username,
                'email' => $username . '@test.com',
                'password' => 'participant',
                'first_name' => explode(' ', $fullname)[0],
                'last_name' => explode(' ', $fullname)[1],
                'mobile_number' => $faker->phoneNumber,
                'user_type_id' => $this->participant->id,
                'requires_approval' => true,
                'approved_on' => Carbon::now()->subtract('weeks', rand(1, 10))
            ];
        }

        $users = $this->createUsersFromArray($users);

        foreach ($users as $user) {
            $tag = $this->getRandomTag();

            $userTag = new TagsUsers();
            $userTag->user_id = $user->id;
            $userTag->tag_id = $tag->id;
            $userTag->exclusive = false;
            $userTag->save();
        }
    }
}
