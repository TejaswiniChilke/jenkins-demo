<?php
namespace Uab\Database\Seeds\Traits;

use Uab\Http\Models\AnswerOptionsTags;
use Uab\Http\Models\ArticlesTags;
use Uab\Http\Models\BloodGlucoseLevels;
use Uab\Http\Models\BugReports;
use Uab\Http\Models\ChatMessages;
use Uab\Http\Models\CheckIns;
use Uab\Http\Models\Comments;
use Uab\Http\Models\CommentsLeaderboardTypes;
use Uab\Http\Models\CommentsPosts;
use Uab\Http\Models\CommentsVideos;
use Uab\Http\Models\ContactStaffMessages;
use Uab\Http\Models\DataChangeLogs;
use Uab\Http\Models\FailedJobs;
use Uab\Http\Models\FavoriteVideos;
use Uab\Http\Models\Followers;
use Uab\Http\Models\Jobs;
use Uab\Http\Models\Lingos;
use Uab\Http\Models\LoginAttempts;
use Uab\Http\Models\MessageParticipants;
use Uab\Http\Models\Messages;
use Uab\Http\Models\Migrations;
use Uab\Http\Models\Notes;
use Uab\Http\Models\Notifications;
use Uab\Http\Models\Operations;
use Uab\Http\Models\OperationsProjects;
use Uab\Http\Models\PasswordResets;
use Uab\Http\Models\PlaylistsTags;
use Uab\Http\Models\PostAttachments;
use Uab\Http\Models\PostLikes;
use Uab\Http\Models\PostsImages;
use Uab\Http\Models\Projects;
use Uab\Http\Models\Recurrings;
use Uab\Http\Models\ReportedComments;
use Uab\Http\Models\ReportedPosts;
use Uab\Http\Models\Resources;
use Uab\Http\Models\ResourcesTips;
use Uab\Http\Models\Sections;
use Uab\Http\Models\SecurityQuestions;
use Uab\Http\Models\Settings;
use Uab\Http\Models\SqlLogs;
use Uab\Http\Models\StackFrames;
use Uab\Http\Models\Tags;
use Uab\Http\Models\TagsUsers;
use Uab\Http\Models\TagsVideos;
use Uab\Http\Models\TextMessages;
use Uab\Http\Models\Threads;
use Uab\Http\Models\Tips;
use Uab\Http\Models\UserTypes;
use Uab\Http\Models\VerifiedComments;
use Uab\Http\Models\VerifiedPosts;
use Uab\Http\Models\WebsocketsStatisticsEntries;

trait JunkSeederTrait {
    /**
     * @return void
     */
    public function clearJunk() {
        BugReports::forceTruncate();
        CheckIns::forceTruncate();
        Comments::forceTruncate();
        CommentsPosts::forceTruncate();
        CommentsVideos::forceTruncate();
        ContactStaffMessages::forceTruncate();
        DataChangeLogs::forceTruncate();
        FailedJobs::forceTruncate();
        FavoriteVideos::forceTruncate();
        Followers::forceTruncate();
        Jobs::forceTruncate();
        Lingos::forceTruncate();
        LoginAttempts::forceTruncate();
        MessageParticipants::forceTruncate();
        Messages::forceTruncate();
        Migrations::forceTruncate();
        Notes::forceTruncate();
        Notifications::forceTruncate();
        PasswordResets::forceTruncate();
        PostLikes::forceTruncate();
        PostAttachments::forceTruncate();
        PostsImages::forceTruncate();
        Projects::forceTruncate();
        Recurrings::forceTruncate();
        ReportedComments::forceTruncate();
        ReportedPosts::forceTruncate();
        Sections::forceTruncate();
        SecurityQuestions::forceTruncate();
        Settings::forceTruncate();
        SqlLogs::forceTruncate();
        StackFrames::forceTruncate();
        TextMessages::forceTruncate();
        Threads::forceTruncate();
        UserTypes::forceTruncate();
        VerifiedComments::forceTruncate();
        VerifiedPosts::forceTruncate();
        WebsocketsStatisticsEntries::forceTruncate();
    }

    public function clearTags() {
        $this->clearTagsFromUsers();

        AnswerOptionsTags::forceTruncate();
        ArticlesTags::forceTruncate();
        PlaylistsTags::forceTruncate();
        TagsVideos::forceTruncate();

        Tags::forceTruncate();
    }

    public function clearTagsFromUsers() {
        TagsUsers::forceTruncate();
    }
}
