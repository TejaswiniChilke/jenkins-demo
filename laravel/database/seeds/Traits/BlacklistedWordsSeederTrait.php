<?php
namespace Uab\Database\Seeds\Traits;

use Uab\Http\Models\BlacklistedWords;

trait BlacklistedWordsSeederTrait {
    public function clearBlacklistedWords() {
        BlacklistedWords::forceTruncate();
    }

    /**
     * @return void
     */
    public function createBlacklistedWords() {
        $words = [
            'ass',
            'boobs',
            'cock',
            'cunt',
            'fuck',
            'nigger',
            'penis',
            'pussy',
            'retard',
            'tits',
            'vagina',
        ];

        $this->createBlacklistedWordsFromArray($words);
    }

    /**
     * @param array $words
     */
    public function createBlacklistedWordsFromArray(array $words):void {
        foreach ($words as $word) {
            $blacklistedWord = new BlacklistedWords();
            $blacklistedWord->fill(
                [
                    'word' => $word
                ]
            );
            $blacklistedWord->save();
        }
    }
}
