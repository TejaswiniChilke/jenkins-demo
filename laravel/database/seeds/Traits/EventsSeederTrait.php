<?php
namespace Uab\Database\Seeds\Traits;

use Uab\Http\Models\Events;
use Uab\Http\Models\EventsUsers;

trait EventsSeederTrait {
    /**
     * @return void
     */
    public function clearEvents() {
        $this->clearEventsFromUsers();

        Events::forceTruncate();
    }

    /**
     * @return void
     */
    public function clearEventsFromUsers() {
        EventsUsers::forceTruncate();
    }
}
