<?php
namespace Uab\Database\Seeds\Traits;

use Illuminate\Support\Str;
use Uab\Http\Models\FavoriteVideos;
use Uab\Http\Models\Tags;
use Uab\Http\Models\Users;
use Uab\Http\Models\VideoLikes;
use Uab\Http\Models\Videos;
use Uab\Providers\LoremIpsumServiceProvider;

trait VideosSeederTrait {
    /**
     * @return void
     */
    public function clearVideos() {
        VideoLikes::forceTruncate();
        Videos::forceTruncate();
    }

    public function createVideoLikes() {
        $users = Users::get();

        foreach ($users as $user) {
            $count = rand(0, 7);

            for ($i = 0; $i < $count; $i++) {
                /** @var Videos $video */
                $video = Videos::inRandomOrder()->first();

                $like = new VideoLikes();
                $like->video_id = $video->id;
                $like->user_id = $user->id;
                $like->save();
            }
        }
    }

    public function createFavoriteVideos() {
        $users = Users::get();

        foreach ($users as $user) {
            $count = rand(0, 3);

            for ($i = 0; $i < $count; $i++) {
                /** @var Videos $video */
                $video = Videos::inRandomOrder()->first();

                $favorite = new FavoriteVideos();
                $favorite->video_id = $video->id;
                $favorite->user_id = $user->id;
                $favorite->save();
            }
        }
    }

    /**
     * @param integer $weeks
     *
     * @return void
     */
    public function createWeeklyVideos($weeks = 16) {
        $loremService = new LoremIpsumServiceProvider();

        for ($week = 1; $week <= $weeks; $week++)  {
            $video = new Videos();
            $video->title = 'Week '.$week;
            $video->week = $week;
            $video->end_week = $weeks;
            $video->description = $loremService->get(1);
            $video->seconds = rand(60, 60*10);
            $video->short_description = $loremService->get(1);
            $video->file_path = 'https://www.youtube.com/watch?v=g5wa9aej7_E';
        }
    }

    public function createVideosFromTags() {
        $loremService = new LoremIpsumServiceProvider();

        /** @var Tags[] $tags */
        $tags = Tags::get();

        foreach ($tags as $tag) {
            $video = new Videos();
            $video->title = 'Tag '.Str::title($tag->name);
            $video->week = 0;
            $video->end_week = 1000;
            $video->description = $loremService->get(1);
            $video->seconds = rand(60, 60*10);
            $video->short_description = $loremService->get(1);
            $video->file_path = 'https://www.youtube.com/watch?v=g5wa9aej7_E';
        }
    }
}
