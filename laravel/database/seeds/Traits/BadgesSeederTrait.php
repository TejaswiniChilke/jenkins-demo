<?php
namespace Uab\Database\Seeds\Traits;

use Exception;
use Illuminate\Support\Str;
use Uab\Http\Models\Articles;
use Uab\Http\Models\BadgeConditions;
use Uab\Http\Models\BadgeConditionsUsers;
use Uab\Http\Models\BadgeConditionTypes;
use Uab\Http\Models\Badges;
use Uab\Http\Models\BadgesUsers;
use Uab\Http\Models\BadgesUserTypes;
use Uab\Http\Models\Playlists;
use Uab\Http\Models\UserTypes;
use Uab\Http\Models\Videos;

trait BadgesSeederTrait {
    /**
     * @return void
     */
    public function assignBadgesToUserTypes() {
        /** @var Badges $badges */
        $badges = Badges::get();

        /** @var UserTypes $userTypes */
        $userTypes = UserTypes::where('name', '!=', 'control')->get();

        foreach ($userTypes as $userType) {
            foreach ($badges as $badge) {
                $badgeUserType = new BadgesUserTypes();
                $badgeUserType->user_type_id = $userType->id;
                $badgeUserType->badge_id = $badge->id;
                $badgeUserType->save();
            }
        }
    }

    /**
     * @return void
     */
    public function clearBadges() {
        BadgeConditionsUsers::forceTruncate();
        BadgeConditions::forceTruncate();

        BadgesUsers::forceTruncate();
        BadgesUserTypes::forceTruncate();
        Badges::forceTruncate();
    }

    /**
     * @return void
     */
    public function clearBadgesFromUsers() {
        BadgeConditionsUsers::forceTruncate();
        BadgesUsers::forceTruncate();
    }

    /**
     * @return void
     */
    /**
     * @throws Exception
     */
    public function createBadges() {
        $this->createArticleBadges();
        $this->createPlaylistBadges();
        $this->createVideoBadges();

        $this->assignBadgesToUserTypes();
    }

    /**
     * @throws Exception
     */
    public function createArticleBadges() {
        /** @var BadgeConditionTypes $conditionType */
        $conditionType = BadgeConditionTypes::where(
            [
                'name' => 'articleId'
            ]
        )->first();

        if (!is_null($conditionType)) {
            /** @var Articles $articles */
            $articles = Articles::get();

            foreach ($articles as $article) {
                $badge = new Badges();
                $badge->start_week = 0;
                $badge->end_week = 1000;
                $badge->title = 'Read ' . Str::title($article->title);
                $badge->description = 'Read the article titled \'' . Str::title($article->title) . '\'';
                $success =  $badge->save();

                if ($success) {
                    $condition = new BadgeConditions();
                    $condition->badge_id = $badge->id;
                    $condition->badge_condition_type_id = $conditionType->id;
                    $condition->value = $article->id;
                    $success = $condition->save();

                    if (!$success) {
                        throw new Exception(
                            'Could not create article badge condition.'
                        );
                    }
                } else {
                    throw new Exception(
                        'Could not create article badge.'
                    );
                }
            }
        } else {
            throw new Exception(
                'Could not find badge condition type for article badges.'
            );
        }
    }

    /**
     * @throws Exception
     */
    public function createPlaylistBadges() {
        /** @var BadgeConditionTypes $conditionType */
        $conditionType = BadgeConditionTypes::where(
            [
                'name' => 'playlistId'
            ]
        )->first();

        if (!is_null($conditionType)) {
            /** @var Playlists $playlists */
            $playlists = Playlists::get();

            foreach ($playlists as $playlist) {
                $badge = new Badges();
                $badge->start_week = 0;
                $badge->end_week = 1000;
                $badge->title = 'Watch ' . Str::title($playlist->title);
                $badge->description = 'Watch all of the videos in the playlist titled \'' . Str::title($playlist->title) . '\'';
                $success =  $badge->save();

                if ($success) {
                    $condition = new BadgeConditions();
                    $condition->badge_id = $badge->id;
                    $condition->badge_condition_type_id = $conditionType->id;
                    $condition->value = $playlist->id;
                    $success = $condition->save();

                    if (!$success) {
                        throw new Exception(
                            'Could not create playlist badge condition.'
                        );
                    }
                } else {
                    throw new Exception(
                        'Could not create playlist badge.'
                    );
                }
            }
        } else {
            throw new Exception(
                'Could not find badge condition type for playlist badges.'
            );
        }
    }

    /**
     * @throws Exception
     */
    public function createVideoBadges() {
        /** @var BadgeConditionTypes $conditionType */
        $conditionType = BadgeConditionTypes::where(
            [
                'name' => 'videoId'
            ]
        )->first();

        if (!is_null($conditionType)) {
            /** @var Videos $videos */
            $videos = Videos::get();

            foreach ($videos as $video) {
                $badge = new Badges();
                $badge->start_week = 0;
                $badge->end_week = 1000;
                $badge->title = 'Watch ' . Str::title($video->title);
                $badge->description = 'Watch the video titled \'' . Str::title($video->title) . '\'';
                $success =  $badge->save();

                if ($success) {
                    $condition = new BadgeConditions();
                    $condition->badge_id = $badge->id;
                    $condition->badge_condition_type_id = $conditionType->id;
                    $condition->value = $video->id;
                    $success = $condition->save();

                    if (!$success) {
                        throw new Exception(
                            'Could not create video badge condition.'
                        );
                    }
                } else {
                    throw new Exception(
                        'Could not create video badge.'
                    );
                }
            }
        } else {
            throw new Exception(
                'Could not find badge condition type for video badges.'
            );
        }
    }
}
