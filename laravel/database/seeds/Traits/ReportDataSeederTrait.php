<?php
namespace Uab\Database\Seeds\Traits;

use Uab\Http\Enums\UserTypesEnum;
use Uab\Http\Models\AnalyticVideos;
use Uab\Http\Models\LoginAttempts;
use Uab\Http\Models\Users;
use Uab\Providers\MatchingServiceProvider;
use Uab\Providers\PlaylistServiceProvider;
use Uab\Providers\UsersServiceProvider;

trait ReportDataSeederTrait {
    /**
     * @return void
     */
    public function createLoginAttempts() {
        /** @var Users[] $users */
        $users = Users::query()->get();

        foreach ($users as $user) {
            $attempts = rand(0, 10 * 7);

            for ($i = 0; $i < $attempts; $i++) {
                $attempt = new LoginAttempts();
                $attempt->user_id = $user->id;
                $attempt->attempted_at = now()->subDays(rand(0, 7));
                $attempt->ip_address = rand(1, 255).'.'.rand(0, 255).'.'.rand(0, 255).'.'.rand(0, 255);
                $attempt->success = rand(1, 100) > 90 ? 0 : 1;
                $attempt->languages = 'en-us,  en';
                $attempt->browser = 'Chrome@75.0.3770.100';
                $attempt->platform = 'OS X@10_14_5';
                $attempt->device = 'Macintosh';

                $device = rand(0, 4);
                if ($device === 0) {
                    $attempt->is_desktop = 1;
                } else if ($device === 1) {
                    $attempt->is_mobile = 1;
                } else if ($device === 2) {
                    $attempt->is_phone = 1;
                } else {
                    $attempt->is_tablet = 1;
                }

                $attempt->save();
            }
        }
    }

    /**
     * @return void
     */
    public function createPlaylistAdherence() {
        $userService = new UsersServiceProvider();
        $matchingService = new MatchingServiceProvider();
        $playlistService = new PlaylistServiceProvider();

        $users = $userService->getUsersByNotUserTypeNames(
            [
                UserTypesEnum::ADMIN,
                UserTypesEnum::CONTROL,
                UserTypesEnum::SUPER_ADMIN,
            ]
        );

        foreach ($users as $user) {
            $playlists = $matchingService->getMatching('playlists', [], $user->id);
            if (array_key_exists('playlists', $playlists)) {
                $playlists = $playlists['playlists'];
            }

            foreach ($playlists as $playlist) {
                $videos = $playlistService->getVideos($playlist->id);

                foreach ($videos as $video) {
                    $maxStop = is_null($video->seconds) || $video->seconds === 0 ?
                        100 : $video->seconds;

                    $watchedVideo = rand(0, 100) > 90;

                    if ($watchedVideo) {
                        $count = rand(1, 3);

                        for ($i = 0; $i < $count; $i++) {
                            $analytic = new AnalyticVideos();
                            $analytic->play_time = rand(0, 100) > 95 ? 0 : rand(0, $maxStop);
                            $analytic->stop_time = rand(0, 100) > 70 ? $maxStop : rand($analytic->play_time + 1, $maxStop);
                            $analytic->user_id = $user->id;
                            $analytic->video_id = $video->id;
                            $analytic->save();
                        }
                    } else {
                        break;
                    }
                }
            }
        }
    }
}
