<?php
namespace Uab\Database\Seeds\Traits;

use Uab\Http\Models\Articles;
use Uab\Http\Models\CheckIns;
use Uab\Http\Models\Users;
use Uab\Http\Models\Videos;

trait CheckInsSeederTrait {
    /**
     * @return void
     */
    public function createArticleCheckIns() {
        /** @var Users $users */
        $users = Users::get();

        foreach ($users as $user) {
            $checkIns = rand(0, 7);

            for ($i = 0; $i < $checkIns; $i++) {
                $checkIn = new CheckIns();
                $checkIn->user_id = $user->id;
                $checkIn->message = $user->display_name.' read an article.';
                $checkIn->object_table = 'articles';

                $article = Articles::inRandomOrder()->first();
                if (!is_null($article)) {
                    $checkIn->object_id = $article->id;
                    $checkIn->save();
                }
            }
        }
    }

    /**
     * @return void
     */
    public function createCheckIns() {
        $this->createArticleCheckIns();
        $this->createVideoCheckIns();
    }

    /**
     * @return void
     */
    public function createVideoCheckIns() {
        /** @var Users $users */
        $users = Users::get();

        foreach ($users as $user) {
            $checkIns = rand(0, 7);

            for ($i = 0; $i < $checkIns; $i++) {
                $checkIn = new CheckIns();
                $checkIn->user_id = $user->id;
                $checkIn->message = $user->display_name.' watched a video.';
                $checkIn->object_table = 'videos';

                $video = Videos::inRandomOrder()->first();
                if (!is_null($video)) {
                    $checkIn->object_id = $video->id;
                    $checkIn->save();
                }
            }
        }
    }
}
