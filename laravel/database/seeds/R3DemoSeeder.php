<?php
namespace Uab\Database\Seeds;

use Exception;
use Uab\Database\Seeds\Traits\CheckInsSeederTrait;
use Uab\Database\Seeds\Traits\FriendsSeederTrait;
use Uab\Database\Seeds\Traits\PlaylistsSeederTrait;
use Uab\Database\Seeds\Traits\PostsSeederTrait;
use Uab\Database\Seeds\Traits\ReportDataSeederTrait;
use Uab\Database\Seeds\Traits\UsersSeederTrait;
use Uab\Database\Seeds\Traits\VideosSeederTrait;

class R3DemoSeeder extends R3Seeder {
    use CheckInsSeederTrait;
    use FriendsSeederTrait;
    use PlaylistsSeederTrait;
    use PostsSeederTrait;
    use ReportDataSeederTrait;
    use UsersSeederTrait;
    use VideosSeederTrait;

    /**
     * @throws Exception
     */
    public function run() {
        parent::run();

        $this->createApplicants();
        $this->createStatusUsers();
        $this->createWeeklyUsers();

        $this->createFollowers();
        $this->createFriends();
        $this->createUsersWithTags();

        $this->createCheckIns();
        $this->createPosts();

        $this->createLoginAttempts();
        $this->createPlaylistAdherence();

        $this->createVideoLikes();
        $this->createFavoriteVideos();
    }

    public function createProjects() {
        $this->createProject('R3 Project');
    }

    public function record($message, $function) {
        echo $message;

        $time = time();

        $function();

        $endTime = time();

        $finish = $endTime - $time;
        if ($finish >= 60) {
            $finish = ($finish / 60) . ' minutes.';
        } else {
            $finish .= ' seconds.';
        }

        echo 'Finished in '.$finish;
    }
}
