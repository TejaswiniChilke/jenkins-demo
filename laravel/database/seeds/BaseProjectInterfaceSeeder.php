<?php
namespace Uab\Database\Seeds;

use Illuminate\Database\Seeder;
use Exception;
use Uab\Http\Models\AnswerInputTypes;
use Uab\Http\Models\BadgeConditionTypes;
use Uab\Http\Models\BlacklistedWords;
use Uab\Http\Models\Features;
use Uab\Http\Models\FeaturesUserTypes;
use Uab\Http\Models\LeaderboardTypes;
use Uab\Http\Models\Projects;
use Uab\Http\Models\SecurityQuestions;
use Uab\Http\Models\States;
use Uab\Http\Models\Users;
use Uab\Http\Models\UserTypes;

interface BaseProjectInterfaceSeeder {
    public function run();

    public function createProjects();
    public function createUserTypes();
}
