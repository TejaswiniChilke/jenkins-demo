<?php
namespace Database\Factories;

use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Uab\Http\Models\BlacklistedWords;

/** @var Factory $factory */
$factory->define(BlacklistedWords::class, function(Faker $faker) {
    return [
        'word' => $faker->word
    ];
});
