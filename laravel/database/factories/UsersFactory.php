<?php

namespace Database\Factories;

use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Uab\Http\Models\Users;
use Uab\Http\Models\Images;
use Uab\Http\Models\States;

/** @var Factory $factory */
$factory->define(Users::class, function (Faker $faker) {
    return [
        'email' => 'dusk-test-' . uniqid() . '@test.com',
        'username' => 'dusk-test-' . uniqid(),
        'password' => uniqid(),
        'image_id' => factory(Images::class)->make(),
        'state_id' => States::query()->get()->random(1)->first()->id,
        ];
});
