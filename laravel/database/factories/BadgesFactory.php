<?php
namespace Database\Factories;

use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Uab\Http\Models\Badges;
use Uab\Http\Models\Images;

/** @var Factory $factory */
$factory->define(Badges::class, function(Faker $faker) {
    return [
        'title' => 'Dusk Test ' . uniqid(),
		'image_id' => factory(Images::class)->make(),
     ];
});
