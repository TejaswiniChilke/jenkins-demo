<?php
namespace Database\Factories;

use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\Str;
use Uab\Http\Models\Notifications;
use Uab\Http\Models\Users;
use Uab\Http\Models\Images;

/** @var Factory $factory */
$factory->define(Notifications::class, function(Faker $faker) {
    return [
        'message' => $faker->text,
		'user_id' => factory(Users::class)->make(),
		'image_id' => factory(Images::class)->make(),
     ];
});
