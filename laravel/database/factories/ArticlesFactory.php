<?php
namespace Database\Factories;

use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\Str;
use Uab\Http\Models\Articles;
use Uab\Http\Models\Images;

/** @var Factory $factory */
$factory->define(Articles::class, function(Faker $faker) {
    return [
		'image_id' => factory(Images::class)->make(),
		'content_image_id' => factory(Images::class)->make(),
        'title' => Str::title('dusk test ' . uniqid()),
        'content' => $faker->text
     ];
});
