<?php
namespace Database\Factories;

use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\Str;
use Uab\Http\Models\BugReports;
use Uab\Http\Models\Users;

/** @var Factory $factory */
$factory->define(BugReports::class, function(Faker $faker) {
    return [
        'message' => $faker->text,
		'user_id' => factory(Users::class)->make(),
     ];
});
