<?php
namespace Database\Factories;

use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\Str;
use Uab\Http\Models\Emails;
use Uab\Http\Models\Users;

/** @var Factory $factory */
$factory->define(Emails::class, function(Faker $faker) {
    return [
		'to_user_id' => factory(Users::class)->make(),
		'from_user_id' => factory(Users::class)->make(),
     ];
});
