<?php
namespace Database\Factories;

use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\Str;
use Uab\Http\Models\TermsAndConditions;

/** @var Factory $factory */
$factory->define(TermsAndConditions::class, function(Faker $faker) {
    return [
        'terms' => $faker->text
    ];
});
