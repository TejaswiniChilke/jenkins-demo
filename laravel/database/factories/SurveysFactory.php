<?php
namespace Database\Factories;

use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\Str;
use Uab\Http\Models\Surveys;
use Uab\Http\Models\Images;

/** @var Factory $factory */
$factory->define(Surveys::class, function(Faker $faker) {
    return [
        'title' => 'Dusk Test ' . uniqid(),
		'image_id' => factory(Images::class)->make(),
     ];
});
