<?php
namespace Database\Factories;

use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Uab\Http\Models\Posts;
use Uab\Http\Models\Users;
use Uab\Http\Models\Images;

/** @var Factory $factory */
$factory->define(Posts::class, function(Faker $faker) {
    return [
        'message' => $faker->text,
		'from_user_id' => factory(Users::class)->make(),
		'to_user_id' => factory(Users::class)->make(),
		'image_id' => factory(Images::class)->make(),
     ];
});
