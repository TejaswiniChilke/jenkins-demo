# Changelog
All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.13.1]
### Added
* IDs and classes to newsfeed elements for automation

### Changed
* Removed ion-card from within uab-post-create component

### Removed
* Walk thru

## [Released]

## [0.13.0]

### Added
* Walk Thru
* Route for getting comments
* Route for getting instructor videos
* Error logs when playlists are missing during tag matching process
* Sentry to socket server
* Blacklisted comment support for articles, badges, and videos
* Virtual field to reported_comments for loading and caching the referred object
* Virtual field to reported_comments for getting the referred type
* Ability to view comment from reported comment management when not a post's comment
* Pluralize function to string service
* Order video by tags page by tags in custom order
* Socket event when a notification.has_read has been edited
* Quality check cron job
* Quality check for setting and getting variables from Redis 
* ID to menu buttons for automation testing 
* Prefix to Redis keys
* Scrolling to the walk-thru
* Trait to users for calculating the number of days since their status changed
* Field days_in_status to management pages
* Message about applying to the login page
* Style to introjs buttons
* Make walkthru change based on user type
* Make walkthru change based on device 
* Monitor the screen site's frontend
* Route to record text message statuses

### Fixed
* Only send one email per post/comment auto flagged with multiple blacklisted words
* Badge check for new comments
* Decode hashed IDs when creating check-in posts
* Playlists IDs not decoding
* View on Reported Post Management not redirecting
* WYSIWYG requiring additional click to type
* Article image missing from home page
* Login modal's vertical alignment
* Verify buttons on Reported Comment Management
* Friend sort
* Post missing when posting on your own profile
* Singularize service for words ending in 'e'
* Copy on Playlists is not including videos 
* Approved User Management not loading
* Duplicate threads within the inbox
* Do not allow blank posts

### Changed
* check_ins->object_id into a string to handle slugs
* Sort archived videos by title
* Generate ICS events around user's start_time
* Filter instructor videos from Archive Videos
* Style of icons over videos and playlists
* Accept friend text
* Link in weekly reminders
* Stop listening to notification socket on logout
* Do not log errors related to user login errors
* Do not log missing file errors from endpoint ie images
* Thread field from is_read to has_read
* Increased the limit on user management pages
* Order user management pages by modified descending
* Reduce number of processes running queues
* Supervisod logging location

### Removed
* Removed models for groups, tips, resources, and operations
* [type] from friend-list component
* Images from articles when printed
* Sentry's error tracing from socket

### Updated
* Traits for quality check jobs
* Log quality checks to Slack
* Send texts for failed quality checks

## [0.12.1]

### Added
* Event for emailing all users with specific user type
* Event for texting all users with specific user type
* Post URL to reported posts email
* Post URL to reported comments email
* UserTypeServiceProvider
* Command to crontab for clearing expired oauth tokens
* Find or Create job
* Hash service
* Null check to badges week before awarding
* Comment count to posts
* Route to capture incoming text messages
* Route to capture errors from Twilio
* Sentry error tracing
* Filter by user type to login report
* Params to sql_logs
* jQuery injection for Dusk browser tests
* ID to Terms & Conditions 'Apply' and 'Resend Email' buttons
* Clickable links to email address in application rejected responses
* Confirmation before deleting a badge

### Fixed
* toUnderscore function in string service when dealing with ALL CAPS
* Limit active user list based on user type
* WordPress redirect
* Missing buttons on reported comment and post management pages
* Profile header info getting clipped on iPhone
* Fixed state and video analytic management pages 
* Users current week
* Badge popup is not triggered from socket
* Videos without images causing errors on the archive videos page
* Include virtual fields with matching requests
* Redirect after creating a user type
* Last Called not persisting on login report
* Hide text about Resend Email button when button is not present
* Order Instructor Videos by title
* Order Archive Videos by title

### Removed
* WYSIWYG from messaging
* Redis prefix
* Auth from text routes

### Updated
* Email and texting capabilities to match covid
* Profile UI
* Rename 'My Readings' to 'My Articles'
* Handle kill events within socket
* Twilio credentials
* Redirect back or home when a post is deleted from the post page
* Wrong email/password message on login failure
* Order instructor videos by start week
* Socket headers to handle Sentry's trace header
* Backup file location
* Moved error message for invalid email on terms and conditions page

### Moved
* State analytic service to next-base
* HasLiked into a trait for Videos
* HasFavorited into a trait for Videos
* TagCsv into a trait for Videos

## [0.12.0]

### Converted
* App into PWA

### Added
* Add reminder messages based on user type and remembered within local storage
* Autocomplete tags to profile edit and complete
* Error handler to force refresh the page when a user has an outdated version of the app
* Deploy script
* Update script
* Page for viewing user tags
* Artisan command for installing base dependencies
* Artisan command for building r3 apps
* Additional upload image errors 
* Email field to contact staff message page
* Email field to admin's bug report page
* Job to lock surveys after 24 hours
* Function to check if user has been texted in 24 hours

### Fixed
* toUnderscore function in string service when dealing with ALL CAPS
* Friend buttons when user cancels confirmation
* Start time picker alignment on user create and edit pages
* Link in email to confirm email address
* Missing email button for 'Application Approved' user status management tab
* Automated notifications are not going out

### Updated
* Dynamically change title font size on progress bar
* Contact staff page text
* Accepted email text (based on user type)

### Removed
* Applicant model
* My Exercise Videos and Newsfeed from control participant's home page
* Groups (and related) models

### Reverted
* WYSIWYG

## [0.11.2]

### Added
* Zipcode component

### Updated
* Bug report instructions
* Contact staff description
* Centered version number
* Cancel friend request text from profile page

### Fixed
* Datatable icons on modal
* sendEmail route 
* Remove non-numeric characters from phone number input
* WYSIWYG editor not working when empty (replaced library again)
* Clipped edge on badge icon in header
* ICS times exported to users' calendars

## [0.11.1]

### Added
* Filter comments by user type

### Fixed
* User status autocomplete not showing existing value
* Buttons missing from user status management components 
* One hour length of exercises 
* Form input sizes on apply page
* Duplicate ICS events
* Adherence graph colors

## [0.11.0]

### Added
* Rate limit to Sentry error reporting
* New weekly reminder for control participants
* Set password email for control participants
* Filter posts to only show posts from users with the same user type or all if admin
* Filter user list to only users with the same user type or if admin

### Fixed
* Complete profile survey
* Buttons on user management by status tabs
* Login report off by one week

### Removed
* Email block for control participants

### Updated
* Reminder text

## [0.10.1]

### Removed
* Sentry popup on error

### Fixed
* Application accept/deny buttons not displaying
* Security question select (ie user create and edit pages)
* Missing participants on message list
* Article text not saving
* 'No messages' text showing while still loading inbox messages
* Custom placeholders within selectors
* Fix friend buttons when relationship is pending

### Changed
* Email text

### Refactor
* Move any-all toggle, state select, and icon select to next-forms

## [0.10.0]

### Removed
* Removed number next the Messages on side menu

### Fixed
* Spacing before article infographic
* Popup styling on date, datetime, and time pickers (ie application and complete profile pages)

### Added
* Control user type
* Messages header to Bug Report page
* Page for viewing the bug report (accessed from Bug Report Management)

### Refactor
* Split email controller

## [0.9.0]

### Fixed
* Missing notification for an inappropriate post (Post reported)
* Missing liked post notification
* WYSIWYG editor showing HTML (replaced library)
* Time validation on application page

### Added
* Progress bar matching PRECISE

### Update
* To Angular 10
* To Ionic 5
* URLs in the reminder emails
