(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-version-version-module"],{

/***/ "RaOe":
/*!*************************************************!*\
  !*** ./src/app/pages/version/version.module.ts ***!
  \*************************************************/
/*! exports provided: VersionPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VersionPageModule", function() { return VersionPageModule; });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @uab.lakeshore.collaborative/next-base */ "Ozaj");
/* harmony import */ var _version_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./version.page */ "dCrP");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ "fXoL");








class VersionPageModule {
}
VersionPageModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineNgModule"]({ type: VersionPageModule });
VersionPageModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineInjector"]({ factory: function VersionPageModule_Factory(t) { return new (t || VersionPageModule)(); }, imports: [[
            _angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"],
            _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_4__["HeaderModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild([
                {
                    path: '',
                    component: _version_page__WEBPACK_IMPORTED_MODULE_5__["VersionPage"]
                }
            ]),
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵsetNgModuleScope"](VersionPageModule, { declarations: [_version_page__WEBPACK_IMPORTED_MODULE_5__["VersionPage"]], imports: [_angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"],
        _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_4__["HeaderModule"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]] }); })();


/***/ }),

/***/ "dCrP":
/*!***********************************************!*\
  !*** ./src/app/pages/version/version.page.ts ***!
  \***********************************************/
/*! exports provided: VersionPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VersionPage", function() { return VersionPage; });
/* harmony import */ var _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @uab.lakeshore.collaborative/next-base */ "Ozaj");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "TEn/");





class VersionPage {
    constructor(environment, lingo) {
        this.environment = environment;
        this.lingo = lingo;
        this.loadLingo();
        this.reset();
    }
    loadLingo() {
        this.pageTitle = this.lingo.get('version.page.title', 'Version');
    }
    reset() {
        this.version = this.environment.getString('version', '');
    }
}
VersionPage.ɵfac = function VersionPage_Factory(t) { return new (t || VersionPage)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_0__["EnvironmentService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_0__["LingoService"])); };
VersionPage.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({ type: VersionPage, selectors: [["uab-version-page"]], decls: 5, vars: 2, consts: [[3, "pageTitle"], [1, "ion-text-center"], ["id", "app-version"]], template: function VersionPage_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "uab-header", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "ion-content");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "ion-card", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "h1", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("pageTitle", ctx.pageTitle);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("Version: ", ctx.version, "");
    } }, directives: [_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_0__["ɵf"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonContent"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonCard"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJ2ZXJzaW9uLnBhZ2Uuc2NzcyJ9 */"] });


/***/ })

}]);
//# sourceMappingURL=pages-version-version-module-es2015.767f3669ed84b6cbd355.js.map