(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"], {
    /***/
    0:
    /*!***************************!*\
      !*** multi ./src/main.ts ***!
      \***************************/

    /*! no static exports found */

    /***/
    function _(module, exports, __webpack_require__) {
      module.exports = __webpack_require__(
      /*! /Users/jrquick/development/uabshp/r3/apply/src/main.ts */
      "zUnb");
      /***/
    },

    /***/
    1:
    /*!********************!*\
      !*** ws (ignored) ***!
      \********************/

    /*! no static exports found */

    /***/
    function _(module, exports) {
      /* (ignored) */

      /***/
    },

    /***/
    2:
    /*!**********************!*\
      !*** zlib (ignored) ***!
      \**********************/

    /*! no static exports found */

    /***/
    function _(module, exports) {
      /* (ignored) */

      /***/
    },

    /***/
    3:
    /*!********************!*\
      !*** fs (ignored) ***!
      \********************/

    /*! no static exports found */

    /***/
    function _(module, exports) {
      /* (ignored) */

      /***/
    },

    /***/
    4:
    /*!**********************!*\
      !*** http (ignored) ***!
      \**********************/

    /*! no static exports found */

    /***/
    function _(module, exports) {
      /* (ignored) */

      /***/
    },

    /***/
    5:
    /*!***********************!*\
      !*** https (ignored) ***!
      \***********************/

    /*! no static exports found */

    /***/
    function _(module, exports) {
      /* (ignored) */

      /***/
    },

    /***/
    6:
    /*!*********************!*\
      !*** url (ignored) ***!
      \*********************/

    /*! no static exports found */

    /***/
    function _(module, exports) {
      /* (ignored) */

      /***/
    },

    /***/
    "AytR":
    /*!*****************************************!*\
      !*** ./src/environments/environment.ts ***!
      \*****************************************/

    /*! exports provided: environment */

    /***/
    function AytR(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "environment", function () {
        return environment;
      });

      var environment = {
        environment: 'prod',
        frontendUrl: 'https://screentest.scipe.org',
        backendUrl: 'https://screentest.scipe.org/laravel',
        socketUrl: 'https://app.scipe.org:3000',
        project: 'r3',
        version: '0.5.1'
      };
      /***/
    },

    /***/
    "HmaL":
    /*!*****************************************!*\
      !*** ./src/app/services/pwa.service.ts ***!
      \*****************************************/

    /*! exports provided: PwaService */

    /***/
    function HmaL(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PwaService", function () {
        return PwaService;
      });
      /* harmony import */


      var _angular_service_worker__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/service-worker */
      "Jho9");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var PwaService = function PwaService(swUpdate) {
        _classCallCheck(this, PwaService);

        this.swUpdate = swUpdate;
        swUpdate.available.subscribe(function (event) {
          window.location.reload();
        });
      };

      PwaService.ɵfac = function PwaService_Factory(t) {
        return new (t || PwaService)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_angular_service_worker__WEBPACK_IMPORTED_MODULE_0__["SwUpdate"]));
      };

      PwaService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({
        token: PwaService,
        factory: PwaService.ɵfac
      });
      /***/
    },

    /***/
    "PuLO":
    /*!***************************************************!*\
      !*** ./src/app/components/chunk-error-handler.ts ***!
      \***************************************************/

    /*! exports provided: ChunkErrorHandler */

    /***/
    function PuLO(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ChunkErrorHandler", function () {
        return ChunkErrorHandler;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var ChunkErrorHandler = /*#__PURE__*/function () {
        function ChunkErrorHandler() {
          _classCallCheck(this, ChunkErrorHandler);
        }

        _createClass(ChunkErrorHandler, [{
          key: "handleError",
          value: function handleError(error) {
            var chunkFailedMessage = /Loading chunk [\d]+ failed/;

            if (chunkFailedMessage.test(error.message)) {
              window.location.reload();
            }
          }
        }]);

        return ChunkErrorHandler;
      }();

      ChunkErrorHandler.ɵfac = function ChunkErrorHandler_Factory(t) {
        return new (t || ChunkErrorHandler)();
      };

      ChunkErrorHandler.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
        token: ChunkErrorHandler,
        factory: ChunkErrorHandler.ɵfac
      });
      /***/
    },

    /***/
    "RnhZ":
    /*!**************************************************!*\
      !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
      \**************************************************/

    /*! no static exports found */

    /***/
    function RnhZ(module, exports, __webpack_require__) {
      var map = {
        "./af": "K/tc",
        "./af.js": "K/tc",
        "./ar": "jnO4",
        "./ar-dz": "o1bE",
        "./ar-dz.js": "o1bE",
        "./ar-kw": "Qj4J",
        "./ar-kw.js": "Qj4J",
        "./ar-ly": "HP3h",
        "./ar-ly.js": "HP3h",
        "./ar-ma": "CoRJ",
        "./ar-ma.js": "CoRJ",
        "./ar-sa": "gjCT",
        "./ar-sa.js": "gjCT",
        "./ar-tn": "bYM6",
        "./ar-tn.js": "bYM6",
        "./ar.js": "jnO4",
        "./az": "SFxW",
        "./az.js": "SFxW",
        "./be": "H8ED",
        "./be.js": "H8ED",
        "./bg": "hKrs",
        "./bg.js": "hKrs",
        "./bm": "p/rL",
        "./bm.js": "p/rL",
        "./bn": "kEOa",
        "./bn-bd": "loYQ",
        "./bn-bd.js": "loYQ",
        "./bn.js": "kEOa",
        "./bo": "0mo+",
        "./bo.js": "0mo+",
        "./br": "aIdf",
        "./br.js": "aIdf",
        "./bs": "JVSJ",
        "./bs.js": "JVSJ",
        "./ca": "1xZ4",
        "./ca.js": "1xZ4",
        "./cs": "PA2r",
        "./cs.js": "PA2r",
        "./cv": "A+xa",
        "./cv.js": "A+xa",
        "./cy": "l5ep",
        "./cy.js": "l5ep",
        "./da": "DxQv",
        "./da.js": "DxQv",
        "./de": "tGlX",
        "./de-at": "s+uk",
        "./de-at.js": "s+uk",
        "./de-ch": "u3GI",
        "./de-ch.js": "u3GI",
        "./de.js": "tGlX",
        "./dv": "WYrj",
        "./dv.js": "WYrj",
        "./el": "jUeY",
        "./el.js": "jUeY",
        "./en-au": "Dmvi",
        "./en-au.js": "Dmvi",
        "./en-ca": "OIYi",
        "./en-ca.js": "OIYi",
        "./en-gb": "Oaa7",
        "./en-gb.js": "Oaa7",
        "./en-ie": "4dOw",
        "./en-ie.js": "4dOw",
        "./en-il": "czMo",
        "./en-il.js": "czMo",
        "./en-in": "7C5Q",
        "./en-in.js": "7C5Q",
        "./en-nz": "b1Dy",
        "./en-nz.js": "b1Dy",
        "./en-sg": "t+mt",
        "./en-sg.js": "t+mt",
        "./eo": "Zduo",
        "./eo.js": "Zduo",
        "./es": "iYuL",
        "./es-do": "CjzT",
        "./es-do.js": "CjzT",
        "./es-mx": "tbfe",
        "./es-mx.js": "tbfe",
        "./es-us": "Vclq",
        "./es-us.js": "Vclq",
        "./es.js": "iYuL",
        "./et": "7BjC",
        "./et.js": "7BjC",
        "./eu": "D/JM",
        "./eu.js": "D/JM",
        "./fa": "jfSC",
        "./fa.js": "jfSC",
        "./fi": "gekB",
        "./fi.js": "gekB",
        "./fil": "1ppg",
        "./fil.js": "1ppg",
        "./fo": "ByF4",
        "./fo.js": "ByF4",
        "./fr": "nyYc",
        "./fr-ca": "2fjn",
        "./fr-ca.js": "2fjn",
        "./fr-ch": "Dkky",
        "./fr-ch.js": "Dkky",
        "./fr.js": "nyYc",
        "./fy": "cRix",
        "./fy.js": "cRix",
        "./ga": "USCx",
        "./ga.js": "USCx",
        "./gd": "9rRi",
        "./gd.js": "9rRi",
        "./gl": "iEDd",
        "./gl.js": "iEDd",
        "./gom-deva": "qvJo",
        "./gom-deva.js": "qvJo",
        "./gom-latn": "DKr+",
        "./gom-latn.js": "DKr+",
        "./gu": "4MV3",
        "./gu.js": "4MV3",
        "./he": "x6pH",
        "./he.js": "x6pH",
        "./hi": "3E1r",
        "./hi.js": "3E1r",
        "./hr": "S6ln",
        "./hr.js": "S6ln",
        "./hu": "WxRl",
        "./hu.js": "WxRl",
        "./hy-am": "1rYy",
        "./hy-am.js": "1rYy",
        "./id": "UDhR",
        "./id.js": "UDhR",
        "./is": "BVg3",
        "./is.js": "BVg3",
        "./it": "bpih",
        "./it-ch": "bxKX",
        "./it-ch.js": "bxKX",
        "./it.js": "bpih",
        "./ja": "B55N",
        "./ja.js": "B55N",
        "./jv": "tUCv",
        "./jv.js": "tUCv",
        "./ka": "IBtZ",
        "./ka.js": "IBtZ",
        "./kk": "bXm7",
        "./kk.js": "bXm7",
        "./km": "6B0Y",
        "./km.js": "6B0Y",
        "./kn": "PpIw",
        "./kn.js": "PpIw",
        "./ko": "Ivi+",
        "./ko.js": "Ivi+",
        "./ku": "JCF/",
        "./ku.js": "JCF/",
        "./ky": "lgnt",
        "./ky.js": "lgnt",
        "./lb": "RAwQ",
        "./lb.js": "RAwQ",
        "./lo": "sp3z",
        "./lo.js": "sp3z",
        "./lt": "JvlW",
        "./lt.js": "JvlW",
        "./lv": "uXwI",
        "./lv.js": "uXwI",
        "./me": "KTz0",
        "./me.js": "KTz0",
        "./mi": "aIsn",
        "./mi.js": "aIsn",
        "./mk": "aQkU",
        "./mk.js": "aQkU",
        "./ml": "AvvY",
        "./ml.js": "AvvY",
        "./mn": "lYtQ",
        "./mn.js": "lYtQ",
        "./mr": "Ob0Z",
        "./mr.js": "Ob0Z",
        "./ms": "6+QB",
        "./ms-my": "ZAMP",
        "./ms-my.js": "ZAMP",
        "./ms.js": "6+QB",
        "./mt": "G0Uy",
        "./mt.js": "G0Uy",
        "./my": "honF",
        "./my.js": "honF",
        "./nb": "bOMt",
        "./nb.js": "bOMt",
        "./ne": "OjkT",
        "./ne.js": "OjkT",
        "./nl": "+s0g",
        "./nl-be": "2ykv",
        "./nl-be.js": "2ykv",
        "./nl.js": "+s0g",
        "./nn": "uEye",
        "./nn.js": "uEye",
        "./oc-lnc": "Fnuy",
        "./oc-lnc.js": "Fnuy",
        "./pa-in": "8/+R",
        "./pa-in.js": "8/+R",
        "./pl": "jVdC",
        "./pl.js": "jVdC",
        "./pt": "8mBD",
        "./pt-br": "0tRk",
        "./pt-br.js": "0tRk",
        "./pt.js": "8mBD",
        "./ro": "lyxo",
        "./ro.js": "lyxo",
        "./ru": "lXzo",
        "./ru.js": "lXzo",
        "./sd": "Z4QM",
        "./sd.js": "Z4QM",
        "./se": "//9w",
        "./se.js": "//9w",
        "./si": "7aV9",
        "./si.js": "7aV9",
        "./sk": "e+ae",
        "./sk.js": "e+ae",
        "./sl": "gVVK",
        "./sl.js": "gVVK",
        "./sq": "yPMs",
        "./sq.js": "yPMs",
        "./sr": "zx6S",
        "./sr-cyrl": "E+lV",
        "./sr-cyrl.js": "E+lV",
        "./sr.js": "zx6S",
        "./ss": "Ur1D",
        "./ss.js": "Ur1D",
        "./sv": "X709",
        "./sv.js": "X709",
        "./sw": "dNwA",
        "./sw.js": "dNwA",
        "./ta": "PeUW",
        "./ta.js": "PeUW",
        "./te": "XLvN",
        "./te.js": "XLvN",
        "./tet": "V2x9",
        "./tet.js": "V2x9",
        "./tg": "Oxv6",
        "./tg.js": "Oxv6",
        "./th": "EOgW",
        "./th.js": "EOgW",
        "./tk": "Wv91",
        "./tk.js": "Wv91",
        "./tl-ph": "Dzi0",
        "./tl-ph.js": "Dzi0",
        "./tlh": "z3Vd",
        "./tlh.js": "z3Vd",
        "./tr": "DoHr",
        "./tr.js": "DoHr",
        "./tzl": "z1FC",
        "./tzl.js": "z1FC",
        "./tzm": "wQk9",
        "./tzm-latn": "tT3J",
        "./tzm-latn.js": "tT3J",
        "./tzm.js": "wQk9",
        "./ug-cn": "YRex",
        "./ug-cn.js": "YRex",
        "./uk": "raLr",
        "./uk.js": "raLr",
        "./ur": "UpQW",
        "./ur.js": "UpQW",
        "./uz": "Loxo",
        "./uz-latn": "AQ68",
        "./uz-latn.js": "AQ68",
        "./uz.js": "Loxo",
        "./vi": "KSF8",
        "./vi.js": "KSF8",
        "./x-pseudo": "/X5v",
        "./x-pseudo.js": "/X5v",
        "./yo": "fzPg",
        "./yo.js": "fzPg",
        "./zh-cn": "XDpg",
        "./zh-cn.js": "XDpg",
        "./zh-hk": "SatO",
        "./zh-hk.js": "SatO",
        "./zh-mo": "OmwH",
        "./zh-mo.js": "OmwH",
        "./zh-tw": "kOpN",
        "./zh-tw.js": "kOpN"
      };

      function webpackContext(req) {
        var id = webpackContextResolve(req);
        return __webpack_require__(id);
      }

      function webpackContextResolve(req) {
        if (!__webpack_require__.o(map, req)) {
          var e = new Error("Cannot find module '" + req + "'");
          e.code = 'MODULE_NOT_FOUND';
          throw e;
        }

        return map[req];
      }

      webpackContext.keys = function webpackContextKeys() {
        return Object.keys(map);
      };

      webpackContext.resolve = webpackContextResolve;
      module.exports = webpackContext;
      webpackContext.id = "RnhZ";
      /***/
    },

    /***/
    "Sy1n":
    /*!**********************************!*\
      !*** ./src/app/app.component.ts ***!
      \**********************************/

    /*! exports provided: AppComponent */

    /***/
    function Sy1n(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AppComponent", function () {
        return AppComponent;
      });
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! rxjs/operators */
      "kU1M");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic-native/status-bar/ngx */
      "VYYF");
      /* harmony import */


      var _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @uab.lakeshore.collaborative/next-base */
      "Ozaj");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var AppComponent = /*#__PURE__*/function () {
        function AppComponent(accessibility, analytics, loginService, platformService, platform, registry, requestService, route, splashService, statusBar) {
          var _this = this;

          _classCallCheck(this, AppComponent);

          this.accessibility = accessibility;
          this.analytics = analytics;
          this.loginService = loginService;
          this.platformService = platformService;
          this.platform = platform;
          this.registry = registry;
          this.requestService = requestService;
          this.route = route;
          this.splashService = splashService;
          this.statusBar = statusBar;
          this.isLoadingFeatures = true;
          this.showSplitPane = false;
          this.analytics.bootstrap().subscribe();
          this.loginService.registerLoggingIn(function () {
            _this.reset();

            _this.initializeApp();
          });
          this.reset();
          this.initializeApp();
        }

        _createClass(AppComponent, [{
          key: "completeFeatures",
          value: function completeFeatures() {
            this.isLoadingFeatures = false;
            this.toggleSplitPane();
          }
        }, {
          key: "toggleSplitPane",
          value: function toggleSplitPane() {
            this.showSplitPane = !this.showSplitPane;
          }
        }, {
          key: "initializeApp",
          value: function initializeApp() {
            var _this2 = this;

            this.splashService.show('app-component-initializing');
            this.platform.ready().then(function () {
              if (_this2.platformService.isApplication()) {
                _this2.statusBar.styleDefault();
              }

              _this2.accessibility.loadAccessibilitySettings();

              _this2.loginService.registerLogin(function () {
                _this2.loadActiveUser();
              });

              if (_this2.loginService.isLoggedIn()) {
                _this2.loadActiveUser();
              }

              _this2.splashService.hide('app-component-initializing');
            });
          }
        }, {
          key: "loadActiveUser",
          value: function loadActiveUser() {
            var _this3 = this;

            this.isLoadingActiveUser = true;
            this.splashService.show('app-component-loading-active-user');
            this.loginService.getActiveUser().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["finalize"])(function () {
              _this3.isLoadingActiveUser = false;

              _this3.splashService.hide('app-component-loading-active-user');
            })).subscribe(function (activeUser) {
              if (activeUser) {
                _this3.activeUser = activeUser;
                _this3.isLoggedIn = true;
              }
            });
          }
        }, {
          key: "onLogout",
          value: function onLogout() {
            this.loginService.logout().subscribe();
          }
        }, {
          key: "reset",
          value: function reset() {
            this.activeUser = false;
            this.isLoadingActiveUser = false;
            this.isLoadingSections = false;
            this.isLoggedIn = false;
            this.resetRequest();
          }
        }, {
          key: "resetRequest",
          value: function resetRequest() {
            this.request = this.requestService.getRequest();
            this.request = this.requestService.setJoins(this.request, ['>features']);
            this.request = this.requestService.setLimit(this.request, 100);
          }
        }]);

        return AppComponent;
      }();

      AppComponent.ɵfac = function AppComponent_Factory(t) {
        return new (t || AppComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_3__["AccessibilityService"]), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_3__["AnalyticStateVisitService"]), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_3__["LoginService"]), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_3__["PlatformService"]), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["Platform"]), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_3__["RegistryService"]), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_3__["RequestService"]), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_3__["RouteService"]), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_3__["SplashScreenService"]), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_2__["StatusBar"]));
      };

      AppComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineComponent"]({
        type: AppComponent,
        selectors: [["uab-root"]],
        decls: 4,
        vars: 0,
        consts: [[1, "global-width"], ["id", "menu-content"]],
        template: function AppComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "ion-app", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](1, "ion-router-outlet", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](2, "uab-splash-screen");

            _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](3, "uab-logging-out-modal");
          }
        },
        directives: [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonApp"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonRouterOutlet"], _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_3__["ɵu"], _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_3__["ɵk"]],
        encapsulation: 2
      });
      /***/
    },

    /***/
    "ZAI4":
    /*!*******************************!*\
      !*** ./src/app/app.module.ts ***!
      \*******************************/

    /*! exports provided: ConfigLoader, AppModule */

    /***/
    function ZAI4(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ConfigLoader", function () {
        return ConfigLoader;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AppModule", function () {
        return AppModule;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/platform-browser */
      "jhN1");
      /* harmony import */


      var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/platform-browser/animations */
      "R1ws");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var _angular_service_worker__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/service-worker */
      "Jho9");
      /* harmony import */


      var _ionic_native_photo_viewer_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @ionic-native/photo-viewer/ngx */
      "U3FU");
      /* harmony import */


      var _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! @ionic-native/status-bar/ngx */
      "VYYF");
      /* harmony import */


      var _ionic_native_toast_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! @ionic-native/toast/ngx */
      "SmVF");
      /* harmony import */


      var _sentry_angular__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! @sentry/angular */
      "UH2p");
      /* harmony import */


      var ngx_img_fallback__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! ngx-img-fallback */
      "oZYh");
      /* harmony import */


      var ng_pluralize__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
      /*! ng-pluralize */
      "oYQ6");
      /* harmony import */


      var _environments_environment__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
      /*! ../environments/environment */
      "AytR");
      /* harmony import */


      var _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
      /*! @uab.lakeshore.collaborative/next-base */
      "Ozaj");
      /* harmony import */


      var _uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
      /*! @uab.lakeshore.collaborative/next-forms */
      "eH97");
      /* harmony import */


      var _uab_lakeshore_collaborative_next_notifications__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
      /*! @uab.lakeshore.collaborative/next-notifications */
      "VHzt");
      /* harmony import */


      var _uab_lakeshore_collaborative_next_tech__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(
      /*! @uab.lakeshore.collaborative/next-tech */
      "zGAH");
      /* harmony import */


      var _app_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(
      /*! ./app.component */
      "Sy1n");
      /* harmony import */


      var _app_routing_module__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(
      /*! ./app-routing.module */
      "vY5A");
      /* harmony import */


      var _components_chunk_error_handler__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(
      /*! ./components/chunk-error-handler */
      "PuLO");
      /* harmony import */


      var _services_pwa_service__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(
      /*! ./services/pwa.service */
      "HmaL"); // TODO: Remove, import where needed
      // TODO: Remove, import where needed
      // TODO: Remove, import where needed
      // TODO: Remove, import where needed


      function ConfigLoader(configService, colorTemplateService, lingoService) {
        return function () {
          return configService.load('/assets/config.json', _environments_environment__WEBPACK_IMPORTED_MODULE_13__["environment"]).then(function (config) {
            var url = config.backendUrl;
            colorTemplateService.bootstrap(url).subscribe(function () {
              lingoService.init(url).subscribe();
            });
          });
        };
      }

      var AppModule = function AppModule() {
        _classCallCheck(this, AppModule);
      };

      AppModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
        type: AppModule,
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_18__["AppComponent"]]
      });
      AppModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
        factory: function AppModule_Factory(t) {
          return new (t || AppModule)();
        },
        providers: [{
          provide: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ErrorHandler"],
          useClass: _components_chunk_error_handler__WEBPACK_IMPORTED_MODULE_20__["ChunkErrorHandler"]
        }, {
          provide: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ErrorHandler"],
          useValue: _sentry_angular__WEBPACK_IMPORTED_MODULE_10__["createErrorHandler"]({
            showDialog: false
          })
        }, {
          provide: _sentry_angular__WEBPACK_IMPORTED_MODULE_10__["TraceService"],
          deps: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]]
        }, {
          provide: _angular_core__WEBPACK_IMPORTED_MODULE_0__["APP_INITIALIZER"],
          useFactory: function useFactory() {
            return function () {};
          },
          deps: [_sentry_angular__WEBPACK_IMPORTED_MODULE_10__["TraceService"]],
          multi: true
        }, {
          deps: [_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_14__["EnvironmentConfigService"], _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_14__["ColorTemplateService"], _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_14__["LingoService"], _sentry_angular__WEBPACK_IMPORTED_MODULE_10__["TraceService"]],
          provide: _angular_core__WEBPACK_IMPORTED_MODULE_0__["APP_INITIALIZER"],
          multi: true,
          useFactory: ConfigLoader
        }, {
          provide: _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouteReuseStrategy"],
          useClass: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonicRouteStrategy"]
        }, ngx_img_fallback__WEBPACK_IMPORTED_MODULE_11__["ImgFallbackModule"], _ionic_native_photo_viewer_ngx__WEBPACK_IMPORTED_MODULE_7__["PhotoViewer"], _services_pwa_service__WEBPACK_IMPORTED_MODULE_21__["PwaService"], _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_8__["StatusBar"], _ionic_native_toast_ngx__WEBPACK_IMPORTED_MODULE_9__["Toast"]],
        imports: [[_app_routing_module__WEBPACK_IMPORTED_MODULE_19__["AppRoutingModule"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["BrowserModule"], _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_4__["BrowserAnimationsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClientModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonicModule"].forRoot(), _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_14__["NextBaseModule"].forRoot(), _uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_15__["NextFormsModule"].forRoot(), _uab_lakeshore_collaborative_next_notifications__WEBPACK_IMPORTED_MODULE_16__["NextNotificationsModule"].forRoot(), _uab_lakeshore_collaborative_next_tech__WEBPACK_IMPORTED_MODULE_17__["NextTechModule"].forRoot(), ng_pluralize__WEBPACK_IMPORTED_MODULE_12__["NgPluralizeModule"], _angular_service_worker__WEBPACK_IMPORTED_MODULE_6__["ServiceWorkerModule"].register('ngsw-worker.js', {
          enabled: _environments_environment__WEBPACK_IMPORTED_MODULE_13__["environment"].environment !== 'local'
        }), _angular_service_worker__WEBPACK_IMPORTED_MODULE_6__["ServiceWorkerModule"].register('ngsw-worker.js', {
          enabled: _environments_environment__WEBPACK_IMPORTED_MODULE_13__["environment"].environment !== 'local'
        })]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](AppModule, {
          declarations: [_app_component__WEBPACK_IMPORTED_MODULE_18__["AppComponent"]],
          imports: [_app_routing_module__WEBPACK_IMPORTED_MODULE_19__["AppRoutingModule"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["BrowserModule"], _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_4__["BrowserAnimationsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClientModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonicModule"], _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_14__["NextBaseModule"], _uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_15__["NextFormsModule"], _uab_lakeshore_collaborative_next_notifications__WEBPACK_IMPORTED_MODULE_16__["NextNotificationsModule"], _uab_lakeshore_collaborative_next_tech__WEBPACK_IMPORTED_MODULE_17__["NextTechModule"], ng_pluralize__WEBPACK_IMPORTED_MODULE_12__["NgPluralizeModule"], _angular_service_worker__WEBPACK_IMPORTED_MODULE_6__["ServiceWorkerModule"], _angular_service_worker__WEBPACK_IMPORTED_MODULE_6__["ServiceWorkerModule"]]
        });
      })();
      /***/

    },

    /***/
    "kLfG":
    /*!*****************************************************************************************************************************************!*\
      !*** ./node_modules/@ionic/core/dist/esm lazy ^\.\/.*\.entry\.js$ include: \.entry\.js$ exclude: \.system\.entry\.js$ namespace object ***!
      \*****************************************************************************************************************************************/

    /*! no static exports found */

    /***/
    function kLfG(module, exports, __webpack_require__) {
      var map = {
        "./ion-action-sheet.entry.js": ["dUtr", "common", 0],
        "./ion-alert.entry.js": ["Q8AI", "common", 1],
        "./ion-app_8.entry.js": ["hgI1", "common", 2],
        "./ion-avatar_3.entry.js": ["CfoV", "common", 3],
        "./ion-back-button.entry.js": ["Nt02", "common", 4],
        "./ion-backdrop.entry.js": ["Q2Bp", 5],
        "./ion-button_2.entry.js": ["0Pbj", "common", 6],
        "./ion-card_5.entry.js": ["ydQj", "common", 7],
        "./ion-checkbox.entry.js": ["4fMi", "common", 8],
        "./ion-chip.entry.js": ["czK9", "common", 9],
        "./ion-col_3.entry.js": ["/CAe", 10],
        "./ion-datetime_3.entry.js": ["WgF3", "common", 11],
        "./ion-fab_3.entry.js": ["uQcF", "common", 12],
        "./ion-img.entry.js": ["wHD8", 13],
        "./ion-infinite-scroll_2.entry.js": ["2lz6", 14],
        "./ion-input.entry.js": ["ercB", "common", 15],
        "./ion-item-option_3.entry.js": ["MGMP", "common", 16],
        "./ion-item_8.entry.js": ["9bur", "common", 17],
        "./ion-loading.entry.js": ["cABk", "common", 18],
        "./ion-menu_3.entry.js": ["kyFE", "common", 19],
        "./ion-modal.entry.js": ["TvZU", "common", 20],
        "./ion-nav_2.entry.js": ["vnES", "common", 21],
        "./ion-popover.entry.js": ["qCuA", "common", 22],
        "./ion-progress-bar.entry.js": ["0tOe", "common", 23],
        "./ion-radio_2.entry.js": ["h11V", "common", 24],
        "./ion-range.entry.js": ["XGij", "common", 25],
        "./ion-refresher_2.entry.js": ["nYbb", "common", 26],
        "./ion-reorder_2.entry.js": ["smMY", "common", 27],
        "./ion-ripple-effect.entry.js": ["STjf", 28],
        "./ion-route_4.entry.js": ["k5eQ", "common", 29],
        "./ion-searchbar.entry.js": ["OR5t", "common", 30],
        "./ion-segment_2.entry.js": ["fSgp", "common", 31],
        "./ion-select_3.entry.js": ["lfGF", "common", 32],
        "./ion-slide_2.entry.js": ["5xYT", 33],
        "./ion-spinner.entry.js": ["nI0H", "common", 34],
        "./ion-split-pane.entry.js": ["NAQR", 35],
        "./ion-tab-bar_2.entry.js": ["knkW", "common", 36],
        "./ion-tab_2.entry.js": ["TpdJ", "common", 37],
        "./ion-text.entry.js": ["ISmu", "common", 38],
        "./ion-textarea.entry.js": ["U7LX", "common", 39],
        "./ion-toast.entry.js": ["L3sA", "common", 40],
        "./ion-toggle.entry.js": ["IUOf", "common", 41],
        "./ion-virtual-scroll.entry.js": ["8Mb5", 42]
      };

      function webpackAsyncContext(req) {
        if (!__webpack_require__.o(map, req)) {
          return Promise.resolve().then(function () {
            var e = new Error("Cannot find module '" + req + "'");
            e.code = 'MODULE_NOT_FOUND';
            throw e;
          });
        }

        var ids = map[req],
            id = ids[0];
        return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function () {
          return __webpack_require__(id);
        });
      }

      webpackAsyncContext.keys = function webpackAsyncContextKeys() {
        return Object.keys(map);
      };

      webpackAsyncContext.id = "kLfG";
      module.exports = webpackAsyncContext;
      /***/
    },

    /***/
    "vY5A":
    /*!***************************************!*\
      !*** ./src/app/app-routing.module.ts ***!
      \***************************************/

    /*! exports provided: AppRoutingModule */

    /***/
    function vY5A(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function () {
        return AppRoutingModule;
      });
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @uab.lakeshore.collaborative/next-base */
      "Ozaj");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var routes = [{
        path: 'apply',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | pages-apply-apply-module */
          "pages-apply-apply-module").then(__webpack_require__.bind(null,
          /*! ./pages/apply/apply.module */
          "helL")).then(function (m) {
            return m.ApplyPageModule;
          });
        },
        canActivate: [_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_1__["LogoutGuard"]]
      }, {
        path: 'apply/:userId',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | pages-apply-apply-module */
          "pages-apply-apply-module").then(__webpack_require__.bind(null,
          /*! ./pages/apply/apply.module */
          "helL")).then(function (m) {
            return m.ApplyPageModule;
          });
        }
      }, {
        path: 'confirm-email',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | pages-confirm-email-confirm-email-module */
          "pages-confirm-email-confirm-email-module").then(__webpack_require__.bind(null,
          /*! ./pages/confirm-email/confirm-email.module */
          "jDIF")).then(function (m) {
            return m.ConfirmEmailPageModule;
          });
        }
      }, {
        path: 'confirm-email/:userId',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | pages-confirm-email-confirm-email-module */
          "pages-confirm-email-confirm-email-module").then(__webpack_require__.bind(null,
          /*! ./pages/confirm-email/confirm-email.module */
          "jDIF")).then(function (m) {
            return m.ConfirmEmailPageModule;
          });
        }
      }, {
        path: 'logout',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | pages-logout-logout-module */
          "pages-logout-logout-module").then(__webpack_require__.bind(null,
          /*! ./pages/logout/logout.module */
          "4rJG")).then(function (m) {
            return m.LogoutPageModule;
          });
        },
        data: {
          username: '',
          password: ''
        },
        canActivate: [_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_1__["LoginGuard"]]
      }, {
        path: 'version',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | pages-version-version-module */
          "pages-version-version-module").then(__webpack_require__.bind(null,
          /*! ./pages/version/version.module */
          "RaOe")).then(function (m) {
            return m.VersionPageModule;
          });
        }
      }, {
        path: '',
        redirectTo: 'apply',
        pathMatch: 'full'
      }, {
        path: '**',
        redirectTo: 'apply',
        pathMatch: 'full'
      }];

      var AppRoutingModule = function AppRoutingModule() {
        _classCallCheck(this, AppRoutingModule);
      };

      AppRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineNgModule"]({
        type: AppRoutingModule
      });
      AppRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjector"]({
        factory: function AppRoutingModule_Factory(t) {
          return new (t || AppRoutingModule)();
        },
        imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"].forRoot(routes, {
          enableTracing: false,
          useHash: true,
          relativeLinkResolution: 'legacy'
        })], _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵsetNgModuleScope"](AppRoutingModule, {
          imports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]],
          exports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]]
        });
      })();
      /***/

    },

    /***/
    "zUnb":
    /*!*********************!*\
      !*** ./src/main.ts ***!
      \*********************/

    /*! no exports provided */

    /***/
    function zUnb(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony import */


      var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/platform-browser */
      "jhN1");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _sentry_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @sentry/angular */
      "UH2p");
      /* harmony import */


      var _sentry_rrweb__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @sentry/rrweb */
      "PvjB");
      /* harmony import */


      var _sentry_tracing__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @sentry/tracing */
      "aI3+");
      /* harmony import */


      var moment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! moment */
      "wd/R");
      /* harmony import */


      var moment__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_5__);
      /* harmony import */


      var _app_app_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./app/app.module */
      "ZAI4");
      /* harmony import */


      var _environments_environment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./environments/environment */
      "AytR");

      var ignoreErrors = ['top.GLOBALS', 'originalCreateNotification', 'canvas.contentDocument', 'MyApp_RemoveAllHighlights', 'http://tt.epicplay.com', 'Can\'t find variable: ZiteReader', 'jigsaw is not defined', 'ComboSearch is not defined', 'http://loading.retry.widdit.com/', 'atomicFindClose', 'fb_xd_fragment', 'bmi_SafeAddOnload', 'EBCallBackMessageReceived', 'conduitPage', /ChunkLoadError/i, /Cannot read property 'isProxied' of undefined/i, /evaluating 'o.isProxied'/i, /User not allowed to login/i, /Notification is not defined/i, /Can't find variable: Notification/i];

      _sentry_angular__WEBPACK_IMPORTED_MODULE_2__["init"]({
        environment: _environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].environment,
        release: _environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].project + '@' + _environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].version,
        dsn: 'https://ab54e64694ba4a24b9e67142ade02bcb@o412548.ingest.sentry.io/5514562',
        integrations: [new _sentry_tracing__WEBPACK_IMPORTED_MODULE_4__["Integrations"].BrowserTracing({
          tracingOrigins: [_environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].frontendUrl],
          routingInstrumentation: _sentry_angular__WEBPACK_IMPORTED_MODULE_2__["routingInstrumentation"]
        }), new _sentry_rrweb__WEBPACK_IMPORTED_MODULE_3__["default"]()],
        tracesSampleRate: 1.0,
        ignoreErrors: ignoreErrors,
        denyUrls: [/graph\.facebook\.com/i, /connect\.facebook\.net\/en_US\/all\.js/i, /eatdifferent\.com\.woopra-ns\.com/i, /static\.woopra\.com\/js\/woopra\.js/i, /extensions\//i, /^chrome:\/\//i, /127\.0\.0\.1:4001\/isrunning/i, /webappstoolbarba\.texthelp\.com\//i, /metrics\.itunes\.apple\.com\.edgesuite\.net\//i, /localhost/i],
        beforeSend: function beforeSend(event, hint) {
          // Block local
          if (_environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].environment === 'local') {
            return null;
          } // Block test


          if (_environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].environment === 'test') {
            return null;
          } // Filter errors


          var errorLength = ignoreErrors.length;

          for (var i = 0; i < errorLength; i++) {
            if (event.message.match(ignoreErrors[i])) {
              return null;
            }
          } // Rate limit


          var errorLog = localStorage.getItem('errorLog');

          if (errorLog === null) {
            errorLog = {};
          } else {
            errorLog = JSON.parse(errorLog);
          }

          var errorKey = 'error-' + event.fingerprint;

          if (typeof errorLog[errorKey] === 'undefined') {
            errorLog[errorKey] = {
              count: 0,
              last: new Date()
            };
          }

          if (moment__WEBPACK_IMPORTED_MODULE_5__(errorLog[errorKey].last).isBefore(moment__WEBPACK_IMPORTED_MODULE_5__(30, 'm').add())) {
            errorLog[errorKey].count = 0;
          }

          errorLog[errorKey].last = new Date();
          errorLog[errorKey].count++;
          localStorage.setItem('errorLog', JSON.stringify(errorLog));

          if (errorLog[errorKey].count > 10) {
            return;
          }

          return event;
        }
      });

      if (_environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].environment === 'prod') {
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
      }

      _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["platformBrowser"]().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_6__["AppModule"]).then(function (ref) {
        if (window['ngRef']) {
          window['ngRef'].destroy();
        }

        window['ngRef'] = ref;
      })["catch"](function (err) {
        console.log(err);
      });
      /***/

    },

    /***/
    "zn8P":
    /*!******************************************************!*\
      !*** ./$$_lazy_route_resource lazy namespace object ***!
      \******************************************************/

    /*! no static exports found */

    /***/
    function zn8P(module, exports) {
      function webpackEmptyAsyncContext(req) {
        // Here Promise.resolve().then() is used instead of new Promise() to prevent
        // uncaught exception popping up in devtools
        return Promise.resolve().then(function () {
          var e = new Error("Cannot find module '" + req + "'");
          e.code = 'MODULE_NOT_FOUND';
          throw e;
        });
      }

      webpackEmptyAsyncContext.keys = function () {
        return [];
      };

      webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
      module.exports = webpackEmptyAsyncContext;
      webpackEmptyAsyncContext.id = "zn8P";
      /***/
    }
  }, [[0, "runtime", "vendor"]]]);
})();
//# sourceMappingURL=main-es5.af0b6d5a38cef9eb5dc4.js.map