(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

  function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

  function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

  function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

  function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

  function _possibleConstructorReturn(self, call) { if (call && (typeof call === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

  function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

  function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

  function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-apply-apply-module"], {
    /***/
    "1Vig":
    /*!****************************************************************!*\
      !*** ./src/app/components/accept-terms/accept-terms.module.ts ***!
      \****************************************************************/

    /*! exports provided: AcceptTermsModule */

    /***/
    function Vig(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AcceptTermsModule", function () {
        return AcceptTermsModule;
      });
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @uab.lakeshore.collaborative/next-base */
      "Ozaj");
      /* harmony import */


      var _uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @uab.lakeshore.collaborative/next-forms */
      "eH97");
      /* harmony import */


      var _services_application_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../../services/application.service */
      "AQhQ");
      /* harmony import */


      var _accept_terms_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./accept-terms.component */
      "XWdq");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var AcceptTermsModule = function AcceptTermsModule() {
        _classCallCheck(this, AcceptTermsModule);
      };

      AcceptTermsModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵdefineNgModule"]({
        type: AcceptTermsModule
      });
      AcceptTermsModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵdefineInjector"]({
        factory: function AcceptTermsModule_Factory(t) {
          return new (t || AcceptTermsModule)();
        },
        providers: [_services_application_service__WEBPACK_IMPORTED_MODULE_5__["ApplicationService"]],
        imports: [[_uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_4__["ButtonGroupModule"], _angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonicModule"], _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_3__["SkeletonParagraphModule"], _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_3__["RangeModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ReactiveFormsModule"], _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_3__["SafeHtmlModule"]]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵsetNgModuleScope"](AcceptTermsModule, {
          declarations: [_accept_terms_component__WEBPACK_IMPORTED_MODULE_6__["AcceptTermsComponent"]],
          imports: [_uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_4__["ButtonGroupModule"], _angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonicModule"], _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_3__["SkeletonParagraphModule"], _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_3__["RangeModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ReactiveFormsModule"], _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_3__["SafeHtmlModule"]],
          exports: [_accept_terms_component__WEBPACK_IMPORTED_MODULE_6__["AcceptTermsComponent"]]
        });
      })();
      /***/

    },

    /***/
    "AQhQ":
    /*!*************************************************!*\
      !*** ./src/app/services/application.service.ts ***!
      \*************************************************/

    /*! exports provided: ApplicationService */

    /***/
    function AQhQ(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ApplicationService", function () {
        return ApplicationService;
      });
      /* harmony import */


      var _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @uab.lakeshore.collaborative/next-base */
      "Ozaj");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var ApplicationService = /*#__PURE__*/function (_uab_lakeshore_collab) {
        _inherits(ApplicationService, _uab_lakeshore_collab);

        var _super = _createSuper(ApplicationService);

        function ApplicationService(date, variable, environment, local, login, list, request, string) {
          var _this;

          _classCallCheck(this, ApplicationService);

          _this = _super.call(this, variable, environment, list, request, string);
          _this.date = date;
          _this.variable = variable;
          _this.environment = environment;
          _this.local = local;
          _this.login = login;
          _this.list = list;
          _this.request = request;
          _this.string = string;
          _this.model = false;
          return _this;
        }

        _createClass(ApplicationService, [{
          key: "apply",
          value: function apply(newUser, request) {
            request = this.request.getRequest(request);
            request = this.request.setPrefix(request, 'onboarding');
            request = this.request.setModel(request, 'users');
            request = this.request.setAction(request, 'apply');
            return _get(_getPrototypeOf(ApplicationService.prototype), "_findOrCreate", this).call(this, newUser, request);
          }
        }, {
          key: "getTermsAndConditions",
          value: function getTermsAndConditions(request) {
            request = this.request.getRequest(request);
            request = this.request.setPrefix(request, 'onboarding');
            request = this.request.setModel(request, 'terms_and_conditions');
            request = this.request.setLimit(request, 100);
            return _get(_getPrototypeOf(ApplicationService.prototype), "_getMore", this).call(this, request);
          }
        }, {
          key: "getColumns",
          value: function getColumns(request) {
            return _get(_getPrototypeOf(ApplicationService.prototype), "_getColumns", this).call(this, request);
          }
        }, {
          key: "getMore",
          value: function getMore(request) {
            return this.getTermsAndConditions(request);
          }
        }, {
          key: "getPage",
          value: function getPage(request) {
            return this.getTermsAndConditions(request);
          }
        }]);

        return ApplicationService;
      }(_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_0__["BaseCrudService"]);

      ApplicationService.ɵfac = function ApplicationService_Factory(t) {
        return new (t || ApplicationService)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_0__["DateService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_0__["VariableService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_0__["EnvironmentService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_0__["LocalStorageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_0__["LoginService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_0__["ListService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_0__["RequestService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_0__["StringService"]));
      };

      ApplicationService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({
        token: ApplicationService,
        factory: ApplicationService.ɵfac
      });
      /***/
    },

    /***/
    "JtTH":
    /*!*******************************************!*\
      !*** ./src/app/pages/apply/apply.page.ts ***!
      \*******************************************/

    /*! exports provided: ApplyPage */

    /***/
    function JtTH(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ApplyPage", function () {
        return ApplyPage;
      });
      /* harmony import */


      var _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @uab.lakeshore.collaborative/next-base */
      "Ozaj");
      /* harmony import */


      var _uab_lakeshore_collaborative_next_tech__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @uab.lakeshore.collaborative/next-tech */
      "zGAH");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! rxjs/operators */
      "kU1M");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _components_accept_terms_accept_terms_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ../../components/accept-terms/accept-terms.component */
      "XWdq");
      /* harmony import */


      var _uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @uab.lakeshore.collaborative/next-forms */
      "eH97");
      /* harmony import */


      var _components_sci_application_sci_application_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ../../components/sci-application/sci-application.component */
      "ccew");

      function ApplyPage_ion_card_2_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "ion-card", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "ion-card-header");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](2, "ion-card-title", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](3, " Welcome ");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](4, "ion-card-content", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](5, "div", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](6, " Welcome to the SCIPE study! ");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](7, "div", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](8, " To begin your application process, please enter your primary email address and click the ACCEPT button below. You will receive an email shortly to verify your email address and direct you to the next step (application). If you have any questions about the SCIPE study, please re-visit ");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](9, "a", 16);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](10, "https://www.scipe.org");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](11, ". You may directly contact our SCIPE team at ");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](12, "a", 17);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](13, "(205) 209-2245");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](14, " or ");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](15, "a", 18);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](16, "scipe@uab.edu");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](17, ". We would love to help! ");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        }
      }

      function ApplyPage_div_3_ion_card_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "ion-card", 20);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](1, "uab-skeleton-block");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        }
      }

      var _c0 = function _c0() {
        return [];
      };

      function ApplyPage_div_3_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](1, ApplyPage_div_3_ion_card_1_Template, 2, 0, "ion-card", 19);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵpipe"](2, "range");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngForOf", _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵpipeBind2"](2, 1, _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵpureFunction0"](4, _c0), 5));
        }
      }

      function ApplyPage_ion_card_4_Template(rf, ctx) {
        if (rf & 1) {
          var _r17 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "ion-card", 21);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "ion-card-header");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](2, "ion-card-title", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](3, " Terms and Conditions ");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](4, "uab-accept-terms", 22);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("onAccept", function ApplyPage_ion_card_4_Template_uab_accept_terms_onAccept_4_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵrestoreView"](_r17);

            var ctx_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"]();

            return ctx_r16.setApplicant($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("acceptButtonText", ctx_r2.acceptTermsButtonText)("acceptingButtonText", ctx_r2.acceptingButtonText);
        }
      }

      function ApplyPage_ion_card_5_ion_icon_2_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](0, "ion-icon", 25);
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("slot", "start")("src", "assets/icons/not-complete.svg");
        }
      }

      function ApplyPage_ion_card_5_ion_icon_3_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](0, "ion-icon", 25);
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("slot", "start")("src", "assets/icons/complete.svg");
        }
      }

      function ApplyPage_ion_card_5_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "ion-card", 21);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "ion-item");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](2, ApplyPage_ion_card_5_ion_icon_2_Template, 1, 2, "ion-icon", 23);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](3, ApplyPage_ion_card_5_ion_icon_3_Template, 1, 2, "ion-icon", 23);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](4, "ion-label", 24);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](5, " Terms and Conditions ");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", !ctx_r3.hasAcceptedTerms);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r3.hasAcceptedTerms);
        }
      }

      function ApplyPage_ion_card_6_b_14_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](1, "If you do not receive the email then click the button below to resend it.");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        }
      }

      function ApplyPage_ion_card_6_uab_button_group_15_Template(rf, ctx) {
        if (rf & 1) {
          var _r23 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "uab-button-group", 29);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("neutralOnClick", function ApplyPage_ion_card_6_uab_button_group_15_Template_uab_button_group_neutralOnClick_0_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵrestoreView"](_r23);

            var ctx_r22 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](2);

            return ctx_r22.resend();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r21 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("neutralButtonId", "resend-button")("neutralDisabled", ctx_r21.isResending)("neutralActive", ctx_r21.isResending)("neutralText", "Resend Email")("neutralActiveText", "Resending...");
        }
      }

      function ApplyPage_ion_card_6_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "ion-card", 26);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "ion-card-header");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](2, "ion-card-title", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](3, " Confirm Email ");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](4, "ion-card-content", 27);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](5, " An email has been sent to ");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](6, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](7);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](8, ". ");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](9, "br");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](10, "br");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](11, " To continue the application process go to your inbox and click the link inside the email to confirm your email address. The email may take a few minutes to appear. Be sure to check both your inbox and spam folder for the email. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](12, "br");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](13, "br");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](14, ApplyPage_ion_card_6_b_14_Template, 2, 0, "b", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](15, ApplyPage_ion_card_6_uab_button_group_15_Template, 1, 5, "uab-button-group", 28);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](7);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtextInterpolate"](ctx_r4.applicant.email);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](7);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r4.showResend);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r4.showResend);
        }
      }

      function ApplyPage_ion_card_7_ion_icon_2_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](0, "ion-icon", 25);
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("slot", "start")("src", "assets/icons/not-complete.svg");
        }
      }

      function ApplyPage_ion_card_7_ion_icon_3_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](0, "ion-icon", 25);
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("slot", "start")("src", "assets/icons/complete.svg");
        }
      }

      function ApplyPage_ion_card_7_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "ion-card", 30);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "ion-item");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](2, ApplyPage_ion_card_7_ion_icon_2_Template, 1, 2, "ion-icon", 23);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](3, ApplyPage_ion_card_7_ion_icon_3_Template, 1, 2, "ion-icon", 23);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](4, "ion-label", 24);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](5, " Confirm Email ");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", !ctx_r5.hasConfirmedEmail);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r5.hasConfirmedEmail);
        }
      }

      function ApplyPage_ion_card_8_ion_icon_2_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](0, "ion-icon", 25);
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("slot", "start")("src", "assets/icons/not-complete.svg");
        }
      }

      function ApplyPage_ion_card_8_ion_icon_3_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](0, "ion-icon", 25);
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("slot", "start")("src", "assets/icons/complete.svg");
        }
      }

      function ApplyPage_ion_card_8_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "ion-card", 31);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "ion-item");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](2, ApplyPage_ion_card_8_ion_icon_2_Template, 1, 2, "ion-icon", 23);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](3, ApplyPage_ion_card_8_ion_icon_3_Template, 1, 2, "ion-icon", 23);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](4, "ion-label", 24);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](5, " Application ");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", !ctx_r6.applicationComplete);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r6.applicationComplete);
        }
      }

      function ApplyPage_ion_card_9_Template(rf, ctx) {
        if (rf & 1) {
          var _r29 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "ion-card", 32);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "ion-card-header");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](2, "ion-card-title", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](3, " Application ");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](4, "uab-sci-application", 33);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("onApply", function ApplyPage_ion_card_9_Template_uab_sci_application_onApply_4_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵrestoreView"](_r29);

            var ctx_r28 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"]();

            return ctx_r28.completeApplication($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("showCancel", false)("applicant", ctx_r7.applicant)("addressLineOneText", ctx_r7.addressLineOneText)("addressLineTwoText", ctx_r7.addressLineTwoText)("cancelText", ctx_r7.cancelText)("cityText", ctx_r7.cityText)("applyText", ctx_r7.applyText)("applyingText", ctx_r7.applyingText)("emailText", ctx_r7.emailText)("firstNameText", ctx_r7.firstNameText)("lastNameText", ctx_r7.lastNameText)("mobileNumberText", ctx_r7.mobileNumberText)("stateLabel", ctx_r7.stateLabel);
        }
      }

      function ApplyPage_ion_card_10_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "ion-card", 31);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "ion-card-content", 34);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](2, " Thank you for your interest in the SCIPE study. Based on your response, you are not eligible for this study. Please feel free to contact us for more information at ");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](3, "a", 18);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](4, "scipe@uab.edu");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](5, ". We appreciate your time and have a good day! ");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        }
      }

      function ApplyPage_ion_card_11_Template(rf, ctx) {
        if (rf & 1) {
          var _r31 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "ion-card", 35);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "ion-card-header");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](2, "ion-card-title", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](3, " Browser Check ");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](4, "uab-browser-check", 36);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("onComplete", function ApplyPage_ion_card_11_Template_uab_browser_check_onComplete_4_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵrestoreView"](_r31);

            var ctx_r30 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"]();

            return ctx_r30.setApplicant($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("activeUser", ctx_r9.applicant)("checkText", ctx_r9.browserCheckText)("checkingText", ctx_r9.browserCheckingText)("nextText", ctx_r9.nextText)("runImmediately", true);
        }
      }

      function ApplyPage_ion_card_12_ion_icon_2_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](0, "ion-icon", 25);
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("slot", "start")("src", "assets/icons/not-complete.svg");
        }
      }

      function ApplyPage_ion_card_12_ion_icon_3_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](0, "ion-icon", 25);
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("slot", "start")("src", "assets/icons/complete.svg");
        }
      }

      function ApplyPage_ion_card_12_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "ion-card", 35);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "ion-item");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](2, ApplyPage_ion_card_12_ion_icon_2_Template, 1, 2, "ion-icon", 23);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](3, ApplyPage_ion_card_12_ion_icon_3_Template, 1, 2, "ion-icon", 23);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](4, "ion-label", 24);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](5, " Browser Check ");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", !ctx_r10.hasCheckedBrowser);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r10.hasCheckedBrowser);
        }
      }

      function ApplyPage_ion_card_13_Template(rf, ctx) {
        if (rf & 1) {
          var _r35 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "ion-card", 37);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "ion-card-header");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](2, "ion-card-title", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](3, " Speed Check ");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](4, "uab-speed-check", 38);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("onCancel", function ApplyPage_ion_card_13_Template_uab_speed_check_onCancel_4_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵrestoreView"](_r35);

            var ctx_r34 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"]();

            return ctx_r34.cancelSpeedCheck();
          })("onComplete", function ApplyPage_ion_card_13_Template_uab_speed_check_onComplete_4_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵrestoreView"](_r35);

            var ctx_r36 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"]();

            return ctx_r36.setApplicant($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("runImmediately", true)("user", ctx_r11.applicant)("checkText", ctx_r11.speedCheckText)("checkingText", ctx_r11.speedCheckingText)("nextText", ctx_r11.nextText);
        }
      }

      function ApplyPage_ion_card_14_ion_icon_2_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](0, "ion-icon", 25);
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("slot", "start")("src", "assets/icons/not-complete.svg");
        }
      }

      function ApplyPage_ion_card_14_ion_icon_3_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](0, "ion-icon", 25);
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("slot", "start")("src", "assets/icons/complete.svg");
        }
      }

      function ApplyPage_ion_card_14_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "ion-card", 37);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "ion-item");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](2, ApplyPage_ion_card_14_ion_icon_2_Template, 1, 2, "ion-icon", 23);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](3, ApplyPage_ion_card_14_ion_icon_3_Template, 1, 2, "ion-icon", 23);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](4, "ion-label", 24);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](5, " Speed Check ");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r12 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", !ctx_r12.hasCheckedSpeed);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r12.hasCheckedSpeed);
        }
      }

      function ApplyPage_ion_card_15_span_5_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "span");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](1, " You have already applied to the study. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        }
      }

      function ApplyPage_ion_card_15_div_6_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](2, " We will review the application within 24 hours and send a follow-up survey link (Participant Screening Form) via e-mail. This will help you to know whether or not you need a physician\u2019s approval before participating in an exercise program. Please complete the survey at your earliest convenience. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](3, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](4, " Thank you again for your interest in our study. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        }
      }

      function ApplyPage_ion_card_15_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "ion-card", 39);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "ion-card-header");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](2, "ion-card-title", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](3, " Your application has been successfully submitted to the program staff! ");

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](4, "ion-card-content", 40);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](5, ApplyPage_ion_card_15_span_5_Template, 2, 0, "span", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](6, ApplyPage_ion_card_15_div_6_Template, 5, 0, "div", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r13 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r13.isReturning);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", !ctx_r13.isReturning);
        }
      }

      var ApplyPage = /*#__PURE__*/function () {
        function ApplyPage(alert, lingo, request, route, tech, userService, variable) {
          _classCallCheck(this, ApplyPage);

          this.alert = alert;
          this.lingo = lingo;
          this.request = request;
          this.route = route;
          this.tech = tech;
          this.userService = userService;
          this.variable = variable;
          this.showResend = true;
          this.loadLingo();
          this.reset();
          this.loadUser();
        }

        _createClass(ApplyPage, [{
          key: "apply",
          value: function apply() {
            var _this2 = this;

            this.isApplying = true;
            var request = this.request.getRequest();
            request = this.request.setPrefix(request, 'onboarding');
            request = this.request.setAction(request, 'finalize', this.applicant.id);
            request = this.request.setJoins(request, ['<images', '<states']);
            this.userService.loadUser(request).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["finalize"])(function () {
              _this2.isApplying = false;
            })).subscribe();
          }
        }, {
          key: "cancelApplication",
          value: function cancelApplication() {
            this.showApplication = true;
            this.showBrowserCheck = false;
            this.showRejected = false;
            this.showSpeedCheck = false;
          }
        }, {
          key: "cancelSpeedCheck",
          value: function cancelSpeedCheck() {
            this.showApplication = false;
            this.showBrowserCheck = true;
            this.showRejected = false;
            this.showSpeedCheck = false;
          }
        }, {
          key: "completeApplication",
          value: function completeApplication(applicant) {
            this.applicationComplete = true;
            this.setApplicant(applicant);
          }
        }, {
          key: "loadLingo",
          value: function loadLingo() {
            this.addressLineOneText = this.lingo.get('apply.terms-and-conditions.address-line-one.label', 'Address Line 1');
            this.addressLineTwoText = this.lingo.get('apply.terms-and-conditions.address-line-two.label', 'Address Line 2');
            this.acceptTermsButtonText = this.lingo.get('apply.terms-and-conditions.accept.button.label', 'Accept');
            this.acceptingButtonText = this.lingo.get('apply.terms-and-conditions.accepting.button.label', 'Accepting...');
            this.adminSectionText = this.lingo.get('apply.section.title.loginDetails', 'Admin');
            this.cancelText = this.lingo.get('apply.button.cancel.label', 'Cancel');
            this.browserCheckText = this.lingo.get('apply.browser.check.label', 'Check Browser');
            this.browserCheckingText = this.lingo.get('apply.browser.checking.label', 'Checking...');
            this.speedCheckText = this.lingo.get('apply.speed.check.label', 'Check Internet');
            this.speedCheckingText = this.lingo.get('apply.speed.check.label', 'Checking...');
            this.cityText = this.lingo.get('apply.column.city.label', 'City');
            this.applyText = this.lingo.get('apply.button.apply.label', 'Apply');
            this.applyingText = this.lingo.get('apply.button.applying.label', 'Applying...');
            this.contactSectionText = this.lingo.get('apply.section.title.contactDetails', 'Contact');
            this.emailText = this.lingo.get('apply.column.email.label', 'Email');
            this.firstNameText = this.lingo.get('apply.column.firstName.label', 'First Name');
            this.lastNameText = this.lingo.get('apply.column.lastName.label', 'Last Name');
            this.loginSectionText = this.lingo.get('apply.section.title.loginDetails', 'Login Details');
            this.mobileNumberText = this.lingo.get('apply.column.mobileNumber.label', 'Mobile Number');
            this.nextText = this.lingo.get('apply.button.continue.label', 'Continue');
            this.passwordText = this.lingo.get('apply.column.password.label', 'Password');
            this.personalSectionText = this.lingo.get('apply.section.title.personalDetails', 'Personal Details');
            this.stateLabel = this.lingo.get('apply.column.state.label', 'State');
            this.pageTitle = this.lingo.get('apply.page.title', 'Apply');
            this.usernameText = this.lingo.get('apply.column.username.label', 'Username');
            this.securityQuestionSectionText = this.lingo.get('apply.section.title.securityQuestionDetails', 'Security Question');
            this.securityQuestionText = this.lingo.get('apply.column.securityQuestion.label', 'Security Question');
            this.securityAnswerText = this.lingo.get('apply.column.securityAnswer.label', 'Security Answer');
          }
        }, {
          key: "loadUser",
          value: function loadUser() {
            var _this3 = this;

            this.isLoadingUser = true;
            var id = this.route.getStringParam('userId');

            if (id !== '') {
              var request = this.request.getRequest();
              request = this.request.setPrefix(request, 'open');
              request = this.request.setId(request, id);
              request = this.request.setJoins(request, ['<images', '<states']);
              this.userService.loadUser(request).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["finalize"])(function () {
                _this3.isLoadingUser = false;
              })).subscribe(function (user) {
                if (user) {
                  _this3.setApplicant(user);
                }
              });
            } else {
              this.isLoadingUser = false;
            }
          }
        }, {
          key: "openApplication",
          value: function openApplication() {
            if (this.hasConfirmedEmail && this.hasCheckedBrowser && this.hasCheckedSpeed) {
              this.showApplication = true;
              this.showBrowserCheck = false;
              this.showRejected = false;
              this.showSpeedCheck = false;
            }
          }
        }, {
          key: "openBrowserCheck",
          value: function openBrowserCheck() {
            if (this.hasConfirmedEmail) {
              this.showApplication = false;
              this.showBrowserCheck = true;
              this.showRejected = false;
              this.showSpeedCheck = false;
            }
          }
        }, {
          key: "openSpeedCheck",
          value: function openSpeedCheck() {
            if (this.hasConfirmedEmail && this.hasCheckedBrowser) {
              this.showApplication = false;
              this.showBrowserCheck = false;
              this.showSpeedCheck = true;
            }
          }
        }, {
          key: "resend",
          value: function resend() {
            var _this4 = this;

            this.isResending = true;
            var request = this.request.getRequest();
            request = this.request.setPrefix(request, 'open');
            request = this.request.setModel(request, 'confirm');
            request = this.request.setAction(request, 'resend', this.applicant.id);
            this.userService.loadUser(request).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["finalize"])(function () {
              _this4.isResending = false;
            })).subscribe(function (user) {
              if (user) {
                _this4.alert.addMessage('Confirmation email has been resent.');

                _this4.showResend = false;
              } else {
                _this4.alert.addError('Unable to resend email. Try again later.');
              }
            });
          }
        }, {
          key: "reset",
          value: function reset() {
            this.applicationComplete = false;
            this.hasAcceptedTerms = false;
            this.hasCheckedBrowser = false;
            this.hasCheckedSpeed = false;
            this.hasCompleted = false;
            this.hasConfirmedEmail = false;
            this.isApplying = false;
            this.isLoadingUser = true;
            this.isResending = false;
            this.isReturning = false;
            this.showApplication = false;
            this.showBrowserCheck = false;
            this.showEmailConfirmation = false;
            this.showSpeedCheck = false;
            this.showTerms = true;
            this.applicant = new _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_0__["User"]();
          }
        }, {
          key: "setApplicant",
          value: function setApplicant(applicant) {
            this.applicant = applicant;
            this.showApplication = false;
            this.showBrowserCheck = false;
            this.showRejected = false;
            this.showSpeedCheck = false;
            this.showTerms = false;

            if (this.applicant.email) {
              this.hasAcceptedTerms = true;

              if (this.applicant.status === _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_0__["UserStatuses"].AWAITING_EMAIL_CONFIRMATION) {
                this.showEmailConfirmation = true;
              } else {
                this.hasConfirmedEmail = true;

                if (this.applicant.status === _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_0__["UserStatuses"].EMAIL_CONFIRMED) {
                  this.showApplication = true;
                } else {
                  this.applicationComplete = true;

                  if (this.applicant.status === _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_0__["UserStatuses"].APPLICATION_REJECTED_AGE || this.applicant.status === _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_0__["UserStatuses"].APPLICATION_REJECTED_SCI || this.applicant.status === _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_0__["UserStatuses"].DEACTIVATED || this.applicant.status === _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_0__["UserStatuses"].APPLICATION_DENIED) {
                    this.showRejected = true;
                  } else {
                    applicant.browser_check = this.variable.getString(applicant.browser_check);
                    var browserCheck = applicant.browser_check.split('@');
                    this.hasCheckedBrowser = browserCheck.length === 2 && this.tech.isSupportedBrowser(this.variable.getString(browserCheck[0]), this.variable.getInteger(browserCheck[1]));

                    if (this.hasCheckedBrowser) {
                      var speed = this.variable.getInteger(this.applicant.speed_check, -1);

                      if (this.tech.isSpeedSupported(speed)) {
                        this.hasCheckedSpeed = true;
                        this.hasCompleted = true;

                        if (this.applicant.status === _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_0__["UserStatuses"].SPEED_CONFIRMED) {
                          this.isReturning = false;
                          this.apply();
                        } else {
                          this.isReturning = true;
                        }
                      } else {
                        this.showSpeedCheck = true;
                      }
                    } else {
                      this.showBrowserCheck = true;
                    }
                  }
                }
              }
            } else {
              this.showTerms = true;
            }
          }
        }]);

        return ApplyPage;
      }();

      ApplyPage.ɵfac = function ApplyPage_Factory(t) {
        return new (t || ApplyPage)(_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_0__["AlertService"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_0__["LingoService"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_0__["RequestService"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_0__["RouteService"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_uab_lakeshore_collaborative_next_tech__WEBPACK_IMPORTED_MODULE_1__["TechCheckService"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_0__["UserService"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_0__["VariableService"]));
      };

      ApplyPage.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineComponent"]({
        type: ApplyPage,
        selectors: [["uab-apply-page"]],
        decls: 16,
        vars: 15,
        consts: [[3, "pageTitle"], ["class", "animated fadeIn", "style", "animation-delay: 100ms", 4, "ngIf"], [4, "ngIf"], ["class", "animated fadeIn", "style", "animation-delay: 200ms", 4, "ngIf"], ["class", "animated fadeIn confirm-email-card", "style", "animation-delay: 300ms", 4, "ngIf"], ["class", "animated fadeIn", "style", "animation-delay: 300ms", 4, "ngIf"], ["class", "animated fadeIn", "style", "animation-delay: 600ms", 4, "ngIf"], ["class", "animated fadeIn", "style", "animation-delay: 600ms; padding: 10px 20px;", 4, "ngIf"], ["class", "animated fadeIn", "style", "animation-delay: 400ms", 4, "ngIf"], ["class", "animated fadeIn", "style", "animation-delay: 500ms", 4, "ngIf"], ["class", "animated fadeIn", "style", "animation-delay: 700ms", 4, "ngIf"], [1, "animated", "fadeIn", 2, "animation-delay", "100ms"], [1, "text-center"], [1, "ion-margin"], [1, "ion-text-center", 2, "font-weight", "500", "text-align", "center", "width", "100%", "margin-bottom", "10px"], [1, "ion-text-left", 2, "text-align", "left", "width", "100%"], ["href", "https://www.scipe.org"], ["href", "tel:+12052092245"], ["href", "mailto: scipe@uab.edu"], ["class", "apply-section-skeleton animated fadeIn", 4, "ngFor", "ngForOf"], [1, "apply-section-skeleton", "animated", "fadeIn"], [1, "animated", "fadeIn", 2, "animation-delay", "200ms"], [3, "acceptButtonText", "acceptingButtonText", "onAccept"], [3, "slot", "src", 4, "ngIf"], [1, "text-center", "text-disabled"], [3, "slot", "src"], [1, "animated", "fadeIn", "confirm-email-card", 2, "animation-delay", "300ms"], [1, "ion-margin", "text-center"], [3, "neutralButtonId", "neutralDisabled", "neutralActive", "neutralText", "neutralActiveText", "neutralOnClick", 4, "ngIf"], [3, "neutralButtonId", "neutralDisabled", "neutralActive", "neutralText", "neutralActiveText", "neutralOnClick"], [1, "animated", "fadeIn", 2, "animation-delay", "300ms"], [1, "animated", "fadeIn", 2, "animation-delay", "600ms"], [1, "animated", "fadeIn", 2, "animation-delay", "600ms", "padding", "10px 20px"], [3, "showCancel", "applicant", "addressLineOneText", "addressLineTwoText", "cancelText", "cityText", "applyText", "applyingText", "emailText", "firstNameText", "lastNameText", "mobileNumberText", "stateLabel", "onApply"], [1, "ion-margin", "ion-text-center"], [1, "animated", "fadeIn", 2, "animation-delay", "400ms"], [3, "activeUser", "checkText", "checkingText", "nextText", "runImmediately", "onComplete"], [1, "animated", "fadeIn", 2, "animation-delay", "500ms"], [3, "runImmediately", "user", "checkText", "checkingText", "nextText", "onCancel", "onComplete"], [1, "animated", "fadeIn", 2, "animation-delay", "700ms"], [1, "ion-margin", "text-center", 2, "color", "dimgray", "font-weight", "500"]],
        template: function ApplyPage_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](0, "uab-header", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "ion-content");

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](2, ApplyPage_ion_card_2_Template, 18, 0, "ion-card", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](3, ApplyPage_div_3_Template, 3, 5, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](4, ApplyPage_ion_card_4_Template, 5, 2, "ion-card", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](5, ApplyPage_ion_card_5_Template, 6, 2, "ion-card", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](6, ApplyPage_ion_card_6_Template, 16, 3, "ion-card", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](7, ApplyPage_ion_card_7_Template, 6, 2, "ion-card", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](8, ApplyPage_ion_card_8_Template, 6, 2, "ion-card", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](9, ApplyPage_ion_card_9_Template, 5, 13, "ion-card", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](10, ApplyPage_ion_card_10_Template, 6, 0, "ion-card", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](11, ApplyPage_ion_card_11_Template, 5, 5, "ion-card", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](12, ApplyPage_ion_card_12_Template, 6, 2, "ion-card", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](13, ApplyPage_ion_card_13_Template, 5, 5, "ion-card", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](14, ApplyPage_ion_card_14_Template, 6, 2, "ion-card", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](15, ApplyPage_ion_card_15_Template, 7, 2, "ion-card", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("pageTitle", ctx.pageTitle);

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", !ctx.hasCompleted);

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx.isLoadingUser);

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx.showTerms && !ctx.isLoadingUser);

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", !ctx.showTerms && !ctx.isLoadingUser);

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx.showEmailConfirmation && !ctx.isLoadingUser);

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", !ctx.showEmailConfirmation && !ctx.isLoadingUser);

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", !ctx.showApplication && !ctx.isLoadingUser);

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx.showApplication && !ctx.isLoadingUser);

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx.showRejected && !ctx.isLoadingUser);

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx.showBrowserCheck && !ctx.isLoadingUser);

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", !ctx.showBrowserCheck && !ctx.isLoadingUser);

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx.showSpeedCheck && !ctx.isLoadingUser);

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", !ctx.showSpeedCheck && !ctx.isLoadingUser);

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx.hasCompleted);
          }
        },
        directives: [_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_0__["ɵf"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonContent"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["NgIf"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonCard"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonCardHeader"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonCardTitle"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonCardContent"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["NgForOf"], _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_0__["ɵd"], _components_accept_terms_accept_terms_component__WEBPACK_IMPORTED_MODULE_6__["AcceptTermsComponent"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonItem"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonLabel"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonIcon"], _uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_7__["ɵd"], _components_sci_application_sci_application_component__WEBPACK_IMPORTED_MODULE_8__["SciApplicationComponent"], _uab_lakeshore_collaborative_next_tech__WEBPACK_IMPORTED_MODULE_1__["ɵb"], _uab_lakeshore_collaborative_next_tech__WEBPACK_IMPORTED_MODULE_1__["ɵc"]],
        pipes: [_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_0__["ɵb"]],
        styles: [".apply-section-skeleton[_ngcontent-%COMP%] {\n  height: 48px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL2FwcGx5LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFlBQUE7QUFDRiIsImZpbGUiOiJhcHBseS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYXBwbHktc2VjdGlvbi1za2VsZXRvbiB7XG4gIGhlaWdodDogNDhweDtcbn1cbiJdfQ== */"]
      });
      /***/
    },

    /***/
    "Ry4W":
    /*!**********************************************************************!*\
      !*** ./src/app/components/sci-application/sci-application.module.ts ***!
      \**********************************************************************/

    /*! exports provided: SciApplicationModule */

    /***/
    function Ry4W(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SciApplicationModule", function () {
        return SciApplicationModule;
      });
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _sci_application_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./sci-application.component */
      "ccew");
      /* harmony import */


      var _uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @uab.lakeshore.collaborative/next-forms */
      "eH97");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var SciApplicationModule = function SciApplicationModule() {
        _classCallCheck(this, SciApplicationModule);
      };

      SciApplicationModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineNgModule"]({
        type: SciApplicationModule
      });
      SciApplicationModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineInjector"]({
        factory: function SciApplicationModule_Factory(t) {
          return new (t || SciApplicationModule)();
        },
        imports: [[_uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_4__["ButtonGroupModule"], _angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"], _uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_4__["DateModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"], _uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_4__["GenderToggleModule"], _uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_4__["AgeToggleModule"], _uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_4__["InputModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonicModule"], _uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_4__["PhoneNumberInputModule"], _uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_4__["PreferredContactToggleModule"], _uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_4__["SciToggleModule"], _uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_4__["StateSelectModule"], _uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_4__["TimeModule"], _uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_4__["ZipcodeInputModule"]]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵsetNgModuleScope"](SciApplicationModule, {
          declarations: [_sci_application_component__WEBPACK_IMPORTED_MODULE_3__["SciApplicationComponent"]],
          imports: [_uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_4__["ButtonGroupModule"], _angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"], _uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_4__["DateModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"], _uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_4__["GenderToggleModule"], _uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_4__["AgeToggleModule"], _uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_4__["InputModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonicModule"], _uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_4__["PhoneNumberInputModule"], _uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_4__["PreferredContactToggleModule"], _uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_4__["SciToggleModule"], _uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_4__["StateSelectModule"], _uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_4__["TimeModule"], _uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_4__["ZipcodeInputModule"]],
          exports: [_sci_application_component__WEBPACK_IMPORTED_MODULE_3__["SciApplicationComponent"]]
        });
      })();
      /***/

    },

    /***/
    "XWdq":
    /*!*******************************************************************!*\
      !*** ./src/app/components/accept-terms/accept-terms.component.ts ***!
      \*******************************************************************/

    /*! exports provided: AcceptTermsComponent */

    /***/
    function XWdq(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AcceptTermsComponent", function () {
        return AcceptTermsComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! rxjs/operators */
      "kU1M");
      /* harmony import */


      var _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @uab.lakeshore.collaborative/next-base */
      "Ozaj");
      /* harmony import */


      var _services_application_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../../services/application.service */
      "AQhQ");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @uab.lakeshore.collaborative/next-forms */
      "eH97");

      function AcceptTermsComponent_ion_card_0_div_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "uab-skeleton-paragraph");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "ion-item-divider");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      var _c0 = function _c0() {
        return [];
      };

      function AcceptTermsComponent_ion_card_0_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-card", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, AcceptTermsComponent_ion_card_0_div_1_Template, 3, 0, "div", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](2, "range");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind2"](2, 1, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](4, _c0), 3));
        }
      }

      function AcceptTermsComponent_ion_card_1_div_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ion-text");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](3, "safeHtml");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var tc_r6 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("innerHTML", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](3, 1, tc_r6.terms), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeHtml"]);
        }
      }

      function AcceptTermsComponent_ion_card_1_form_3_div_6_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx_r7.validEmailText, " ");
        }
      }

      function AcceptTermsComponent_ion_card_1_form_3_Template(rf, ctx) {
        if (rf & 1) {
          var _r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "form", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ion-item", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "ion-label", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "ion-input", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keyup.enter", function AcceptTermsComponent_ion_card_1_form_3_Template_ion_input_keyup_enter_4_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r9);

            var ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

            return ctx_r8.onSubmit();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "uab-button-group", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("acceptOnClick", function AcceptTermsComponent_ion_card_1_form_3_Template_uab_button_group_acceptOnClick_5_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r9);

            var ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

            return ctx_r10.onSubmit();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](6, AcceptTermsComponent_ion_card_1_form_3_div_6_Template, 2, 1, "div", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx_r5.acceptTermsForm);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("position", "stacked");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx_r5.emailLabelText, " ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("placeholder", ctx_r5.emailPlaceholderText);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("acceptButtonId", "accept-button")("acceptDisabled", ctx_r5.isDisabled())("acceptDisabledTooltip", "A valid email address is required")("acceptActive", ctx_r5.isCreatingUser)("acceptText", ctx_r5.acceptButtonText)("acceptActiveText", ctx_r5.acceptingButtonText);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r5.acceptTermsForm.get("emailAddress").invalid && (ctx_r5.acceptTermsForm.get("emailAddress").dirty || ctx_r5.acceptTermsForm.get("emailAddress").touched));
        }
      }

      function AcceptTermsComponent_ion_card_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-card", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, AcceptTermsComponent_ion_card_1_div_1_Template, 4, 3, "div", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "ion-item-divider");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, AcceptTermsComponent_ion_card_1_form_3_Template, 7, 11, "form", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r1.termsAndConditions);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r1.acceptTermsForm);
        }
      }

      var AcceptTermsComponent = /*#__PURE__*/function () {
        function AcceptTermsComponent(applicationService, formBuilder, variable) {
          _classCallCheck(this, AcceptTermsComponent);

          this.applicationService = applicationService;
          this.formBuilder = formBuilder;
          this.variable = variable;
          this.acceptButtonText = 'Accept';
          this.acceptingButtonText = 'Accepting...';
          this.emailLabelText = 'Enter your email address';
          this.emailPlaceholderText = 'Email address';
          this.validEmailText = 'Please enter a valid email';
          this.onAccept = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
          this.reset();
          this.loadTermsAndCondition();
          this.acceptTermsForm = this.formBuilder.group({
            emailAddress: [null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, this.emailValidator]]
          });
        }

        _createClass(AcceptTermsComponent, [{
          key: "reset",
          value: function reset() {
            this.applicant = new _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_3__["User"]();
            this.isCreatingUser = false;
            this.isLoading = false;
          }
        }, {
          key: "emailValidator",
          value: function emailValidator(control) {
            var returnVar = null;

            if (control.value) {
              var matches = control.value.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/i);

              if (!matches) {
                returnVar = {
                  'invalidEmail': true
                };
              }
            }

            return returnVar;
          }
        }, {
          key: "isDisabled",
          value: function isDisabled() {
            return !this.acceptTermsForm.valid;
          }
        }, {
          key: "loadTermsAndCondition",
          value: function loadTermsAndCondition() {
            var _this5 = this;

            this.isLoading = true;
            this.applicationService.getTermsAndConditions().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["finalize"])(function () {
              _this5.isLoading = false;
            })).subscribe(function (response) {
              if (!response.errors) {
                _this5.termsAndConditions = response.data;
              }
            });
          }
        }, {
          key: "onSubmit",
          value: function onSubmit() {
            var _this6 = this;

            if (!this.isDisabled()) {
              this.isCreatingUser = true;
              this.applicant.email = this.acceptTermsForm.get('emailAddress').value;
              this.applicant.approved_on = null;
              this.applicationService.apply(this.applicant).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["finalize"])(function () {
                _this6.isCreatingUser = false;
              })).subscribe(function (response) {
                if (!response.errors) {
                  var applicant = response.data;

                  if (_this6.variable.isArray(applicant)) {
                    if (applicant.length !== 0) {
                      applicant = applicant[0];
                    }
                  }

                  _this6.applicant = applicant;

                  _this6.onAccept.emit(_this6.applicant);
                }
              });
            }
          }
        }]);

        return AcceptTermsComponent;
      }();

      AcceptTermsComponent.ɵfac = function AcceptTermsComponent_Factory(t) {
        return new (t || AcceptTermsComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_application_service__WEBPACK_IMPORTED_MODULE_4__["ApplicationService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_3__["VariableService"]));
      };

      AcceptTermsComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: AcceptTermsComponent,
        selectors: [["uab-accept-terms"]],
        inputs: {
          acceptButtonText: "acceptButtonText",
          acceptingButtonText: "acceptingButtonText",
          emailLabelText: "emailLabelText",
          emailPlaceholderText: "emailPlaceholderText",
          validEmailText: "validEmailText"
        },
        outputs: {
          onAccept: "onAccept"
        },
        decls: 2,
        vars: 2,
        consts: [["class", "terms-and-conditions-skeleton", 4, "ngIf"], ["class", "terms-and-conditions", 4, "ngIf"], [1, "terms-and-conditions-skeleton"], [4, "ngFor", "ngForOf"], [1, "terms-and-conditions"], ["class", "ion-margin", 4, "ngFor", "ngForOf"], [3, "formGroup", 4, "ngIf"], [1, "ion-margin"], [3, "innerHTML"], [3, "formGroup"], [2, "margin-left", "20px", "margin-right", "20px"], [3, "position"], ["name", "email", "type", "email", "formControlName", "emailAddress", 1, "form-control", 3, "placeholder", "keyup.enter"], [3, "acceptButtonId", "acceptDisabled", "acceptDisabledTooltip", "acceptActive", "acceptText", "acceptActiveText", "acceptOnClick"], ["class", "ion-text-center", "style", "color:red; font-size: 17px; font-weight: 500; width: 100%;", 4, "ngIf"], [1, "ion-text-center", 2, "color", "red", "font-size", "17px", "font-weight", "500", "width", "100%"]],
        template: function AcceptTermsComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, AcceptTermsComponent_ion_card_0_Template, 3, 5, "ion-card", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, AcceptTermsComponent_ion_card_1_Template, 4, 2, "ion-card", 1);
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.isLoading);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.isLoading);
          }
        },
        directives: [_angular_common__WEBPACK_IMPORTED_MODULE_5__["NgIf"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonCard"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["NgForOf"], _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_3__["ɵr"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonItemDivider"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonText"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonItem"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonLabel"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonInput"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["TextValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"], _uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_7__["ɵd"]],
        pipes: [_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_3__["ɵb"], _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_3__["ɵc"]],
        styles: ["ion-card-content[_ngcontent-%COMP%]   div[_ngcontent-%COMP%] {\n  padding-top: 20px;\n  padding-bottom: 12px;\n}\nion-card-content[_ngcontent-%COMP%]   div[_ngcontent-%COMP%]   ion-text[_ngcontent-%COMP%] {\n  padding-left: 8px;\n}\nion-card-content[_ngcontent-%COMP%]   ion-button[_ngcontent-%COMP%] {\n  margin-top: 24px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL2FjY2VwdC10ZXJtcy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDRTtFQUNFLGlCQUFBO0VBQ0Esb0JBQUE7QUFBSjtBQUVJO0VBQ0UsaUJBQUE7QUFBTjtBQUlFO0VBQ0UsZ0JBQUE7QUFGSiIsImZpbGUiOiJhY2NlcHQtdGVybXMuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY2FyZC1jb250ZW50IHtcbiAgZGl2IHtcbiAgICBwYWRkaW5nLXRvcDogMjBweDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMTJweDtcblxuICAgIGlvbi10ZXh0IHtcbiAgICAgIHBhZGRpbmctbGVmdDogOHB4O1xuICAgIH1cbiAgfVxuXG4gIGlvbi1idXR0b24ge1xuICAgIG1hcmdpbi10b3A6IDI0cHg7XG4gIH1cbn1cbiJdfQ== */"]
      });
      /***/
    },

    /***/
    "ccew":
    /*!*************************************************************************!*\
      !*** ./src/app/components/sci-application/sci-application.component.ts ***!
      \*************************************************************************/

    /*! exports provided: SciApplicationComponent */

    /***/
    function ccew(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SciApplicationComponent", function () {
        return SciApplicationComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! rxjs/operators */
      "kU1M");
      /* harmony import */


      var _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @uab.lakeshore.collaborative/next-base */
      "Ozaj");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @uab.lakeshore.collaborative/next-forms */
      "eH97");

      function SciApplicationComponent_div_0_div_25_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 16);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " Please select a time between 9 AM and 6 PM. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function SciApplicationComponent_div_0_Template(rf, ctx) {
        if (rf & 1) {
          var _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ion-text", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, " (*) required field ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "ion-item-divider");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, " Basic Information ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "ion-row");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "ion-col", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "uab-input", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("modelChange", function SciApplicationComponent_div_0_Template_uab_input_modelChange_7_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4);

            var ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r3.user.first_name = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "ion-col", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "uab-input", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("modelChange", function SciApplicationComponent_div_0_Template_uab_input_modelChange_9_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4);

            var ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r5.user.last_name = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "ion-row");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "ion-col", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "uab-age-toggle", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("modelChange", function SciApplicationComponent_div_0_Template_uab_age_toggle_modelChange_12_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4);

            var ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r6.user.is_in_age_range = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "ion-row");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "ion-col", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "uab-gender-toggle", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("modelChange", function SciApplicationComponent_div_0_Template_uab_gender_toggle_modelChange_15_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4);

            var ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r7.user.gender = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "ion-col", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "uab-sci-toggle", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("modelChange", function SciApplicationComponent_div_0_Template_uab_sci_toggle_modelChange_17_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4);

            var ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r8.user.has_sci = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "ion-item-divider");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, " Contact Details ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "ion-row");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "ion-col", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "uab-phone-number-input", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("modelChange", function SciApplicationComponent_div_0_Template_uab_phone_number_input_modelChange_22_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4);

            var ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r9.user.mobile_number = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "ion-row");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "ion-col", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](25, SciApplicationComponent_div_0_div_25_Template, 2, 0, "div", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "uab-time", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("modelChange", function SciApplicationComponent_div_0_Template_uab_time_modelChange_26_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4);

            var ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r10.checkContactPreference($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "ion-col", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "uab-preferred-contact-toggle", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("modelChange", function SciApplicationComponent_div_0_Template_uab_preferred_contact_toggle_modelChange_28_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4);

            var ctx_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r11.user.preferred_contact_type = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "ion-item-divider");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30, " Location ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "ion-row");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "ion-col", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "uab-input", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("modelChange", function SciApplicationComponent_div_0_Template_uab_input_modelChange_33_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4);

            var ctx_r12 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r12.user.address_line_one = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "ion-col", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "uab-input", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("modelChange", function SciApplicationComponent_div_0_Template_uab_input_modelChange_35_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4);

            var ctx_r13 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r13.user.address_line_two = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "ion-row");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "ion-col", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "uab-input", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("modelChange", function SciApplicationComponent_div_0_Template_uab_input_modelChange_38_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4);

            var ctx_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r14.user.city = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "ion-col", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "uab-state-select", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("modelChange", function SciApplicationComponent_div_0_Template_uab_state_select_modelChange_40_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4);

            var ctx_r15 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r15.user.state_text = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "ion-col", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "uab-zipcode-input", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("modelChange", function SciApplicationComponent_div_0_Template_uab_zipcode_input_modelChange_42_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4);

            var ctx_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r16.user.zipcode = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "uab-button-group", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("neutralOnClick", function SciApplicationComponent_div_0_Template_uab_button_group_neutralOnClick_43_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4);

            var ctx_r17 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r17.cancel();
          })("acceptOnClick", function SciApplicationComponent_div_0_Template_uab_button_group_acceptOnClick_43_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4);

            var ctx_r18 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r18.applyUser();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("size", 12)("sizeLg", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("id", "firstName")("model", ctx_r0.user.first_name)("autocomplete", "given-name")("label", ctx_r0.firstNameText)("required", true);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("size", 12)("sizeLg", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("id", "lastName")("model", ctx_r0.user.last_name)("autocomplete", "family-name")("label", ctx_r0.lastNameText)("required", true);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("size", 12)("sizeLg", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("id", "ageToggle")("model", ctx_r0.user.is_in_age_range)("required", true);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("size", 12)("sizeLg", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("id", "genderToggle")("model", ctx_r0.user.gender)("required", true);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("size", 12)("sizeLg", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("id", "sciToggle")("model", ctx_r0.user.has_sci)("required", true);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("size", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("id", "mobileNumber")("label", "Mobile Phone")("required", true)("placeholder", "(XXX) XXX-XXXX")("model", ctx_r0.user.mobile_number);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("size", 12)("sizeLg", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r0.contactPreferenceIsDirty() && !ctx_r0.checkContactPreference());

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("id", "timePreference")("required", true)("placeholder", ctx_r0.contactTimePreferencePlaceholder)("label", ctx_r0.contactTimePreferenceLabel)("model", ctx_r0.user.preferred_contact_time);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("size", 12)("sizeLg", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("id", "contactPreferenceToggle")("model", ctx_r0.user.preferred_contact_type)("required", true)("label", "How would you like to be notified when new exercise content is available on the website?");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("size", 12)("sizeLg", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("id", "addressLineOne")("model", ctx_r0.user.address_line_one)("label", ctx_r0.addressLineOneText)("autocomplete", "address-line1")("required", true);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("size", 12)("sizeLg", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("id", "addressLineTwo")("model", ctx_r0.user.address_line_two)("label", ctx_r0.addressLineTwoText)("autocomplete", "address-line2")("required", false);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("size", 12)("sizeLg", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("id", "city")("model", ctx_r0.user.city)("autocomplete", "address-level2")("label", ctx_r0.cityText)("required", true);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("size", 12)("sizeLg", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("id", "state")("model", ctx_r0.user.state_text)("isAutocomplete", ctx_r0.isStateAutoComplete)("required", true)("label", ctx_r0.stateLabel)("placeholder", ctx_r0.statePlaceholder);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("size", 12)("sizeLg", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("id", "zipcode")("model", ctx_r0.user.zipcode)("label", ctx_r0.zipcodeText)("required", true);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("neutralButtonId", "cancelButton")("showNeutralButton", ctx_r0.showCancel)("neutralDisabled", ctx_r0.isApplying)("neutralText", ctx_r0.cancelText)("acceptButtonId", "applyButton")("acceptDisabled", ctx_r0.isApplying || !ctx_r0.formIsFilled())("acceptDisabledTooltip", "You must answer all of the required fields.")("acceptActive", ctx_r0.isApplying)("acceptText", ctx_r0.applyText)("acceptActiveText", ctx_r0.applyingText);
        }
      }

      function SciApplicationComponent_ion_list_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-list");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ion-card", 17);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "ion-card-header");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "ion-card-title", 18);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, " Sorry ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "ion-card-content", 19);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, " Thank you for your interest in the SCIPE study. Based on your response, you are not eligible for this study. Please feel free to contact us for more information at ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "a", 20);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "scipe@uab.edu");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, ". We appreciate your time and have a good day! ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      var SciApplicationComponent = /*#__PURE__*/function () {
        function SciApplicationComponent(alert, date, request, userService, variable) {
          _classCallCheck(this, SciApplicationComponent);

          this.alert = alert;
          this.date = date;
          this.request = request;
          this.userService = userService;
          this.variable = variable;
          this.zipcodeText = 'Zip Code';
          this.showCancel = true;
          this.addressLineOneText = 'Address Line One';
          this.addressLineTwoText = 'Address Line Two';
          this.cancelText = 'Cancel';
          this.cityText = 'City';
          this.contactTimePreferenceLabel = 'Please select a preferred contact time if the study staff needs to reach you out via phone call.';
          this.contactTimePreferencePlaceholder = 'Preferred Contact Time';
          this.applyText = 'Apply';
          this.applyingText = 'Applying';
          this.dobText = 'Date of Birth';
          this.emailText = 'Email Address';
          this.firstNameText = 'First Name';
          this.lastNameText = 'Last Name';
          this.mobileNumberText = 'Mobile Number';
          this.stateLabel = 'State';
          this.statePlaceholder = 'State';
          this.contactPreferenceIsValid = false;
          this.isStateAutoComplete = false;
          this.onApply = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
          this.onCancel = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
          this.reset();
        }

        _createClass(SciApplicationComponent, [{
          key: "applicant",
          set: function set(applicant) {
            var _this7 = this;

            this.user = applicant;

            if (this.formIsFilled()) {
              this.checkRequirements();

              if (!this.isDenied) {
                setTimeout(function () {
                  _this7.onApply.emit(_this7.user);
                }, 1000);
              }
            }
          }
        }, {
          key: "applyUser",
          value: function applyUser() {
            var _this8 = this;

            this.isApplying = true;
            var request = this.request.getRequest();
            request = this.request.setPrefix(request, 'open');
            request = this.request.setSubAction(request, this.user.id);
            this.user.status = this.checkRequirements();
            this.userService.editUser(this.user, request).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["finalize"])(function () {
              _this8.isApplying = false;
            })).subscribe(function (response) {
              if (_this8.variable.isObject(response)) {
                _this8.user = response;

                if (!_this8.isDenied) {
                  _this8.onApply.emit(_this8.user);
                }
              } else {
                _this8.alert.addError('Unable to submit application. Please try again later.');
              }
            });
          }
        }, {
          key: "cancel",
          value: function cancel() {
            this.onCancel.emit(true);
          }
        }, {
          key: "checkRequirements",
          value: function checkRequirements() {
            /*    const dob = this.date.moment(
                  this.variable.getString(this.user.dob)
                );*/

            /*    const age = this.date.moment().diff(dob, 'years', false);
                if (age < 18 || age > 65) {
                  this.isDenied = true;
            
                  return UserStatuses.APPLICATION_REJECTED_AGE;
                }*/
            var is_in_age_range = this.variable.getBoolean(this.user.is_in_age_range, false);

            if (!is_in_age_range) {
              this.isDenied = true;
              return _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_2__["UserStatuses"].APPLICATION_REJECTED_AGE;
            }

            var hasSci = this.variable.getBoolean(this.user.has_sci, false);

            if (!hasSci) {
              this.isDenied = true;
              return _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_2__["UserStatuses"].APPLICATION_REJECTED_SCI;
            }

            return _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_2__["UserStatuses"].APPLICATION_FILLED;
          }
        }, {
          key: "checkContactPreference",
          value: function checkContactPreference(preference) {
            if (this.variable.isString(preference)) {
              preference = preference.replace('undefined', '');
              preference = preference.replace(' am', '');
              preference = preference.replace(' pm', '');
              preference = preference.trim();

              if (preference.indexOf('-') !== -1) {
                var moment = this.date.moment(preference);
                var hour = moment.hour();
                var minute = moment.minute();
                preference = hour + ':' + minute;
              }

              this.user.preferred_contact_time = preference;
            }

            preference = this.variable.getString(this.user.preferred_contact_time);
            console.log('BEFORE: ' + preference);
            preference = this.date.toTwelveHourTime(preference);
            console.log('AFTER: ' + preference);
            var today = this.date.moment().format('YYYY/MM/DD');
            var timeMoment = this.date.moment(today + ' ' + preference);
            var isAfterMin = timeMoment.isAfter(this.date.moment(today + ' 8:59 am'));
            var isBeforeMax = timeMoment.isBefore(this.date.moment(today + ' 6:01 pm'));
            this.contactPreferenceIsValid = isAfterMin && isBeforeMax;
            return this.contactPreferenceIsValid;
          }
        }, {
          key: "contactPreferenceIsDirty",
          value: function contactPreferenceIsDirty() {
            return typeof this.user.preferred_contact_time !== 'undefined' && this.user.preferred_contact_time !== null;
          }
        }, {
          key: "formIsFilled",
          value: function formIsFilled() {
            return this.contactPreferenceIsValid && this.variable.isString(this.user.first_name) && this.user.first_name.length !== 0 && this.variable.isString(this.user.last_name) && this.user.last_name.length !== 0 && this.variable.isString(this.user.address_line_one) && this.user.address_line_one.length !== 0 && this.variable.isString(this.user.city) && this.user.city.length !== 0 && this.variable.isString(this.user.state_text) && this.user.state_text.length !== 0 && this.variable.isString(this.user.zipcode) && this.user.zipcode.length !== 0 && this.variable.isString(this.user.mobile_number) && this.user.mobile_number.length !== 0 && this.variable.isString(this.user.preferred_contact_type) && this.user.preferred_contact_type.length !== 0 && this.variable.isString(this.user.email) && this.user.email.length !== 0 && this.user.has_sci !== null && this.user.is_in_age_range !== null;
          }
        }, {
          key: "reset",
          value: function reset() {
            this.isDenied = false;
            this.isApplying = false;
          }
        }]);

        return SciApplicationComponent;
      }();

      SciApplicationComponent.ɵfac = function SciApplicationComponent_Factory(t) {
        return new (t || SciApplicationComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_2__["AlertService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_2__["DateService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_2__["RequestService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_2__["UserService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_2__["VariableService"]));
      };

      SciApplicationComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: SciApplicationComponent,
        selectors: [["uab-sci-application"]],
        inputs: {
          applicant: "applicant",
          showCancel: "showCancel",
          addressLineOneText: "addressLineOneText",
          addressLineTwoText: "addressLineTwoText",
          cancelText: "cancelText",
          cityText: "cityText",
          contactTimePreferenceLabel: "contactTimePreferenceLabel",
          contactTimePreferencePlaceholder: "contactTimePreferencePlaceholder",
          applyText: "applyText",
          applyingText: "applyingText",
          dobText: "dobText",
          emailText: "emailText",
          firstNameText: "firstNameText",
          lastNameText: "lastNameText",
          mobileNumberText: "mobileNumberText",
          stateLabel: "stateLabel",
          statePlaceholder: "statePlaceholder"
        },
        outputs: {
          onApply: "onApply",
          onCancel: "onCancel"
        },
        decls: 2,
        vars: 2,
        consts: [["class", "form-list", 4, "ngIf"], [4, "ngIf"], [1, "form-list"], [1, "ion-float-right", 2, "color", "red"], [3, "size", "sizeLg"], [3, "id", "model", "autocomplete", "label", "required", "modelChange"], [3, "id", "model", "required", "modelChange"], [3, "size"], [3, "id", "label", "required", "placeholder", "model", "modelChange"], ["style", "color: red; margin-left: 20px;", 4, "ngIf"], [3, "id", "required", "placeholder", "label", "model", "modelChange"], [3, "id", "model", "required", "label", "modelChange"], [3, "id", "model", "label", "autocomplete", "required", "modelChange"], [3, "id", "model", "isAutocomplete", "required", "label", "placeholder", "modelChange"], [3, "id", "model", "label", "required", "modelChange"], [3, "neutralButtonId", "showNeutralButton", "neutralDisabled", "neutralText", "acceptButtonId", "acceptDisabled", "acceptDisabledTooltip", "acceptActive", "acceptText", "acceptActiveText", "neutralOnClick", "acceptOnClick"], [2, "color", "red", "margin-left", "20px"], [1, "animated", "fadeIn"], [1, "text-center"], [1, "ion-margin", "text-center"], ["href", "mailto: scipe@uab.edu"]],
        template: function SciApplicationComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, SciApplicationComponent_div_0_Template, 44, 94, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, SciApplicationComponent_ion_list_1_Template, 10, 0, "ion-list", 1);
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.isDenied);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.isDenied);
          }
        },
        directives: [_angular_common__WEBPACK_IMPORTED_MODULE_3__["NgIf"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonText"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonItemDivider"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonRow"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonCol"], _uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_5__["ɵq"], _uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_5__["ɵa"], _uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_5__["ɵh"], _uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_5__["ɵn"], _uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_5__["ɵj"], _uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_5__["ɵs"], _uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_5__["ɵk"], _uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_5__["ɵp"], _uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_5__["ɵv"], _uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_5__["ɵd"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonList"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonCard"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonCardHeader"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonCardTitle"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonCardContent"]],
        styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzY2ktYXBwbGljYXRpb24uY29tcG9uZW50LnNjc3MifQ== */"]
      });
      /***/
    },

    /***/
    "helL":
    /*!*********************************************!*\
      !*** ./src/app/pages/apply/apply.module.ts ***!
      \*********************************************/

    /*! exports provided: ApplyPageModule */

    /***/
    function helL(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ApplyPageModule", function () {
        return ApplyPageModule;
      });
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @uab.lakeshore.collaborative/next-base */
      "Ozaj");
      /* harmony import */


      var _uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @uab.lakeshore.collaborative/next-forms */
      "eH97");
      /* harmony import */


      var _uab_lakeshore_collaborative_next_tech__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @uab.lakeshore.collaborative/next-tech */
      "zGAH");
      /* harmony import */


      var _components_accept_terms_accept_terms_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../../components/accept-terms/accept-terms.module */
      "1Vig");
      /* harmony import */


      var _components_sci_application_sci_application_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ../../components/sci-application/sci-application.module */
      "Ry4W");
      /* harmony import */


      var _apply_page__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ./apply.page */
      "JtTH");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var ApplyPageModule = function ApplyPageModule() {
        _classCallCheck(this, ApplyPageModule);
      };

      ApplyPageModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵdefineNgModule"]({
        type: ApplyPageModule
      });
      ApplyPageModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵdefineInjector"]({
        factory: function ApplyPageModule_Factory(t) {
          return new (t || ApplyPageModule)();
        },
        providers: [_uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_5__["StateSelectProvider"], _uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_5__["StateListService"]],
        imports: [[_components_accept_terms_accept_terms_module__WEBPACK_IMPORTED_MODULE_7__["AcceptTermsModule"], _uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_5__["ButtonGroupModule"], _uab_lakeshore_collaborative_next_tech__WEBPACK_IMPORTED_MODULE_6__["BrowserCheckModule"], _angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"], _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_4__["HeaderModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonicModule"], _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_4__["RangeModule"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild([{
          path: '',
          component: _apply_page__WEBPACK_IMPORTED_MODULE_9__["ApplyPage"]
        }]), _components_sci_application_sci_application_module__WEBPACK_IMPORTED_MODULE_8__["SciApplicationModule"], _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_4__["SkeletonBlockModule"], _uab_lakeshore_collaborative_next_tech__WEBPACK_IMPORTED_MODULE_6__["SpeedCheckModule"]]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵsetNgModuleScope"](ApplyPageModule, {
          declarations: [_apply_page__WEBPACK_IMPORTED_MODULE_9__["ApplyPage"]],
          imports: [_components_accept_terms_accept_terms_module__WEBPACK_IMPORTED_MODULE_7__["AcceptTermsModule"], _uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_5__["ButtonGroupModule"], _uab_lakeshore_collaborative_next_tech__WEBPACK_IMPORTED_MODULE_6__["BrowserCheckModule"], _angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"], _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_4__["HeaderModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonicModule"], _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_4__["RangeModule"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"], _components_sci_application_sci_application_module__WEBPACK_IMPORTED_MODULE_8__["SciApplicationModule"], _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_4__["SkeletonBlockModule"], _uab_lakeshore_collaborative_next_tech__WEBPACK_IMPORTED_MODULE_6__["SpeedCheckModule"]]
        });
      })();
      /***/

    }
  }]);
})();
//# sourceMappingURL=pages-apply-apply-module-es5.f2dc7e72d427a007436d.js.map