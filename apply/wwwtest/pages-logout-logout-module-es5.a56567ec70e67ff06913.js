(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-logout-logout-module"], {
    /***/
    "4rJG":
    /*!***********************************************!*\
      !*** ./src/app/pages/logout/logout.module.ts ***!
      \***********************************************/

    /*! exports provided: LogoutPageModule */

    /***/
    function rJG(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LogoutPageModule", function () {
        return LogoutPageModule;
      });
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _logout_page__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./logout.page */
      "EqXV");
      /* harmony import */


      var _uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @uab.lakeshore.collaborative/next-forms */
      "eH97");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var routes = [{
        path: '',
        component: _logout_page__WEBPACK_IMPORTED_MODULE_4__["LogoutPage"]
      }];

      var LogoutPageModule = function LogoutPageModule() {
        _classCallCheck(this, LogoutPageModule);
      };

      LogoutPageModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineNgModule"]({
        type: LogoutPageModule
      });
      LogoutPageModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineInjector"]({
        factory: function LogoutPageModule_Factory(t) {
          return new (t || LogoutPageModule)();
        },
        imports: [[_uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_5__["ButtonGroupModule"], _angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵsetNgModuleScope"](LogoutPageModule, {
          declarations: [_logout_page__WEBPACK_IMPORTED_MODULE_4__["LogoutPage"]],
          imports: [_uab_lakeshore_collaborative_next_forms__WEBPACK_IMPORTED_MODULE_5__["ButtonGroupModule"], _angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        });
      })();
      /***/

    },

    /***/
    "EqXV":
    /*!*********************************************!*\
      !*** ./src/app/pages/logout/logout.page.ts ***!
      \*********************************************/

    /*! exports provided: LogoutPage */

    /***/
    function EqXV(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LogoutPage", function () {
        return LogoutPage;
      });
      /* harmony import */


      var _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @uab.lakeshore.collaborative/next-base */
      "Ozaj");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");

      var LogoutPage = function LogoutPage(loginService, redirect) {
        _classCallCheck(this, LogoutPage);

        this.loginService = loginService;
        this.redirect = redirect;
        this.redirect.goTo('login');
        this.loginService.logout().subscribe();
      };

      LogoutPage.ɵfac = function LogoutPage_Factory(t) {
        return new (t || LogoutPage)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_0__["LoginService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_0__["RedirectService"]));
      };

      LogoutPage.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
        type: LogoutPage,
        selectors: [["page-logout"]],
        decls: 3,
        vars: 0,
        consts: [["id", "logout-page"], [1, "text-center"]],
        template: function LogoutPage_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "ion-content", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "ion-text", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, " Come back sometime! ");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
          }
        },
        directives: [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonContent"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonText"]],
        encapsulation: 2
      });
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-logout-logout-module-es5.a56567ec70e67ff06913.js.map