(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-confirm-email-confirm-email-module"],{

/***/ "CssF":
/*!***********************************************************!*\
  !*** ./src/app/pages/confirm-email/confirm-email.page.ts ***!
  \***********************************************************/
/*! exports provided: ConfirmEmailPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmEmailPage", function() { return ConfirmEmailPage; });
/* harmony import */ var _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @uab.lakeshore.collaborative/next-base */ "Ozaj");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _components_confirm_email_confirm_email_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../components/confirm-email/confirm-email.component */ "lhyC");






class ConfirmEmailPage {
    constructor(lingo, redirect) {
        this.lingo = lingo;
        this.redirect = redirect;
        this.loadLingo();
    }
    goToApplication(applicant) {
        this.redirect.goTo('apply/' + applicant.id);
    }
    reset() {
    }
    loadLingo() {
        this.pageTitle = this.lingo.get('confirm-email.page.title', 'Email Confirmation');
    }
}
ConfirmEmailPage.ɵfac = function ConfirmEmailPage_Factory(t) { return new (t || ConfirmEmailPage)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_0__["LingoService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_0__["RedirectService"])); };
ConfirmEmailPage.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({ type: ConfirmEmailPage, selectors: [["uab-confirm-email-page"]], decls: 3, vars: 1, consts: [[3, "pageTitle"], [3, "onConfirm"]], template: function ConfirmEmailPage_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "uab-header", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "ion-content");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "uab-confirm-email", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("onConfirm", function ConfirmEmailPage_Template_uab_confirm_email_onConfirm_2_listener($event) { return ctx.goToApplication($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("pageTitle", ctx.pageTitle);
    } }, directives: [_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_0__["ɵf"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonContent"], _components_confirm_email_confirm_email_component__WEBPACK_IMPORTED_MODULE_3__["ConfirmEmailComponent"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJjb25maXJtLWVtYWlsLnBhZ2Uuc2NzcyJ9 */"] });


/***/ }),

/***/ "XZ2S":
/*!******************************************************************!*\
  !*** ./src/app/components/confirm-email/confirm-email.module.ts ***!
  \******************************************************************/
/*! exports provided: ConfirmEmailModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmEmailModule", function() { return ConfirmEmailModule; });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @uab.lakeshore.collaborative/next-base */ "Ozaj");
/* harmony import */ var _confirm_email_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./confirm-email.component */ "lhyC");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ "fXoL");







class ConfirmEmailModule {
}
ConfirmEmailModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineNgModule"]({ type: ConfirmEmailModule });
ConfirmEmailModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineInjector"]({ factory: function ConfirmEmailModule_Factory(t) { return new (t || ConfirmEmailModule)(); }, providers: [
        _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_3__["UserService"]
    ], imports: [[
            _angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonicModule"],
            _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_3__["SkeletonParagraphModule"],
            _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_3__["RangeModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ReactiveFormsModule"]
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵsetNgModuleScope"](ConfirmEmailModule, { declarations: [_confirm_email_component__WEBPACK_IMPORTED_MODULE_4__["ConfirmEmailComponent"]], imports: [_angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonicModule"],
        _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_3__["SkeletonParagraphModule"],
        _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_3__["RangeModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ReactiveFormsModule"]], exports: [_confirm_email_component__WEBPACK_IMPORTED_MODULE_4__["ConfirmEmailComponent"]] }); })();


/***/ }),

/***/ "jDIF":
/*!*************************************************************!*\
  !*** ./src/app/pages/confirm-email/confirm-email.module.ts ***!
  \*************************************************************/
/*! exports provided: ConfirmEmailPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmEmailPageModule", function() { return ConfirmEmailPageModule; });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _confirm_email_page__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./confirm-email.page */ "CssF");
/* harmony import */ var _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @uab.lakeshore.collaborative/next-base */ "Ozaj");
/* harmony import */ var _components_confirm_email_confirm_email_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../components/confirm-email/confirm-email.module */ "XZ2S");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ "fXoL");









class ConfirmEmailPageModule {
}
ConfirmEmailPageModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵdefineNgModule"]({ type: ConfirmEmailPageModule });
ConfirmEmailPageModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵdefineInjector"]({ factory: function ConfirmEmailPageModule_Factory(t) { return new (t || ConfirmEmailPageModule)(); }, imports: [[
            _angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"],
            _components_confirm_email_confirm_email_module__WEBPACK_IMPORTED_MODULE_6__["ConfirmEmailModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"],
            _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_5__["HeaderModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild([
                {
                    path: '',
                    component: _confirm_email_page__WEBPACK_IMPORTED_MODULE_4__["ConfirmEmailPage"]
                }
            ])
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_7__["ɵɵsetNgModuleScope"](ConfirmEmailPageModule, { declarations: [_confirm_email_page__WEBPACK_IMPORTED_MODULE_4__["ConfirmEmailPage"]], imports: [_angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"],
        _components_confirm_email_confirm_email_module__WEBPACK_IMPORTED_MODULE_6__["ConfirmEmailModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"],
        _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_5__["HeaderModule"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"]] }); })();


/***/ }),

/***/ "lhyC":
/*!*********************************************************************!*\
  !*** ./src/app/components/confirm-email/confirm-email.component.ts ***!
  \*********************************************************************/
/*! exports provided: ConfirmEmailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmEmailComponent", function() { return ConfirmEmailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @uab.lakeshore.collaborative/next-base */ "Ozaj");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "kU1M");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ "ofXK");
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
















function ConfirmEmailComponent_ion_card_0_div_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "uab-skeleton-paragraph");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
const _c0 = function () { return []; };
function ConfirmEmailComponent_ion_card_0_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-card");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, ConfirmEmailComponent_ion_card_0_div_1_Template, 2, 0, "div", 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](2, "range");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind2"](2, 1, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](4, _c0), 1));
} }
function ConfirmEmailComponent_ion_card_1_span_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx_r4.errorText, " ");
} }
function ConfirmEmailComponent_ion_card_1_span_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx_r5.thankYouText, " ");
} }
function ConfirmEmailComponent_ion_card_1_span_4_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" This link has expired. If you have not completed the application process for the SCIPE study, please go back to the SCIPE website (", ctx_r6.baseUrl, "/#/apply) to apply by entering your e-mail address again. ");
} }
function ConfirmEmailComponent_ion_card_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-card", 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ion-text", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, ConfirmEmailComponent_ion_card_1_span_2_Template, 2, 1, "span", 0);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, ConfirmEmailComponent_ion_card_1_span_3_Template, 2, 1, "span", 0);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, ConfirmEmailComponent_ion_card_1_span_4_Template, 2, 1, "span", 0);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r1.hasError && !ctx_r1.hasExpired);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx_r1.hasError && !ctx_r1.hasExpired);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r1.hasExpired && !ctx_r1.hasError);
} }
class ConfirmEmailComponent {
    constructor(alert, date, environment, formBuilder, loadingController, request, route, userService, variable) {
        this.alert = alert;
        this.date = date;
        this.environment = environment;
        this.formBuilder = formBuilder;
        this.loadingController = loadingController;
        this.request = request;
        this.route = route;
        this.userService = userService;
        this.variable = variable;
        this.baseUrl = '';
        this.errorText = 'Something went wrong. Please copy and paste the URL from the email again.';
        this.thankYouText = 'Thank you for confirming your email.';
        this.onConfirm = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.reset();
        this.loadUser();
    }
    confirm() {
        this.isConfirming = true;
        if (this.variable.isDefined(this.user.email_confirmed_on === null)) {
            let request = this.request.getRequest();
            request = this.request.setPrefix(request, 'open');
            request = this.request.setModel(request, 'confirm');
            request = this.request.setAction(request, 'email', this.user.id);
            this.userService.editUser(this.user, request).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["finalize"])(() => {
                this.isConfirming = false;
            })).subscribe((user) => {
                if (user) {
                    this.alert.addToast(this.thankYouText).then();
                    this.onConfirm.emit(this.user);
                }
                else {
                    this.hasError = true;
                }
                this.hidePopup();
            });
        }
        else {
            this.isConfirming = false;
            this.hidePopup();
        }
    }
    allowedAccess(user) {
        if (user.status === _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_2__["UserStatuses"].AWAITING_EMAIL_CONFIRMATION) {
            return true;
        }
        return user.status === _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_2__["UserStatuses"].EMAIL_CONFIRMED;
    }
    loadUser() {
        this.isLoading = true;
        let request = this.request.getRequest();
        request = this.request.setPrefix(request, 'open');
        const id = this.route.getStringParam('userId');
        if (id !== '') {
            this.showPopup().then();
            request = this.request.setId(request, id);
            this.userService.loadUser(request).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["finalize"])(() => {
                this.isLoading = false;
            })).subscribe((user) => {
                if (user) {
                    if (this.allowedAccess(user)) {
                        this.user = user;
                        this.confirm();
                    }
                    else {
                        this.hasExpired = true;
                    }
                }
                else {
                    this.hasError = true;
                }
                this.isConfirming = false;
                this.hidePopup();
            });
        }
        else {
            this.hasError = true;
            this.isConfirming = false;
        }
    }
    reset() {
        this.baseUrl = this.environment.getString('frontendUrl', '');
        this.hasError = false;
        this.hasExpired = false;
        this.isConfirming = true;
        this.isLoading = false;
        this.user = new _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_2__["User"]();
    }
    hidePopup() {
        this.loadingElement.dismiss().then();
    }
    showPopup() {
        return __awaiter(this, void 0, void 0, function* () {
            this.loadingElement = yield this.loadingController.create({
                message: 'Confirming...',
                translucent: true
            });
            this.loadingElement.present().then();
        });
    }
}
ConfirmEmailComponent.ɵfac = function ConfirmEmailComponent_Factory(t) { return new (t || ConfirmEmailComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_2__["AlertService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_2__["DateService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_2__["EnvironmentService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_2__["RequestService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_2__["RouteService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_2__["UserService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_2__["VariableService"])); };
ConfirmEmailComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: ConfirmEmailComponent, selectors: [["uab-confirm-email"]], outputs: { onConfirm: "onConfirm" }, decls: 2, vars: 2, consts: [[4, "ngIf"], ["class", "ion-margin", 4, "ngIf"], [4, "ngFor", "ngForOf"], [1, "ion-margin"], [1, "ion-text-center", 2, "padding", "4px", "font-size", "x-large"]], template: function ConfirmEmailComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, ConfirmEmailComponent_ion_card_0_Template, 3, 5, "ion-card", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, ConfirmEmailComponent_ion_card_1_Template, 5, 3, "ion-card", 1);
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.isConfirming);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.isConfirming);
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_5__["NgIf"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonCard"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["NgForOf"], _uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_2__["ɵr"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonText"]], pipes: [_uab_lakeshore_collaborative_next_base__WEBPACK_IMPORTED_MODULE_2__["ɵb"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJjb25maXJtLWVtYWlsLmNvbXBvbmVudC5zY3NzIn0= */"] });


/***/ })

}]);
//# sourceMappingURL=pages-confirm-email-confirm-email-module-es2015.04e721da856670dd4095.js.map