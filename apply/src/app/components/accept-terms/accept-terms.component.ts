import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {TermsAndCondition} from '@uab.lakeshore.collaborative/next-base';
import {finalize} from 'rxjs/operators';
import {UserInterface} from '@uab.lakeshore.collaborative/next-base';
import {User} from '@uab.lakeshore.collaborative/next-base';
import {VariableService} from '@uab.lakeshore.collaborative/next-base';
import {ApplicationService} from '../../services/application.service';

@Component({
  selector:    'uab-accept-terms',
  templateUrl: 'accept-terms.component.html',
  styleUrls: [
    'accept-terms.component.scss'
  ]
})
export class AcceptTermsComponent {
  public acceptTermsForm:FormGroup;
  public isCreatingUser:boolean;
  public isLoading:boolean;
  public termsAndConditions:TermsAndCondition[];
  public applicant:User;

  @Input() acceptButtonText:string = 'Accept';
  @Input() acceptingButtonText:string = 'Accepting...';
  @Input() emailLabelText:string = 'Enter your email address';
  @Input() emailPlaceholderText:string = 'Email address';
  @Input() validEmailText:string = 'Please enter a valid email';

  @Output() onAccept:EventEmitter<UserInterface>;

  constructor(
    private applicationService:ApplicationService,
    private formBuilder:FormBuilder,
    private variable:VariableService
  ) {
    this.onAccept = new EventEmitter<UserInterface>();

    this.reset();
    this.loadTermsAndCondition();

    this.acceptTermsForm = this.formBuilder.group(
      {
        emailAddress: [
          null,
          [
            Validators.required,
            this.emailValidator
          ]
        ]
      }
    );
  }

  reset():void {
    this.applicant = new User();

    this.isCreatingUser = false;

    this.isLoading = false;
  }

  emailValidator(control:any):any {
    let returnVar = null;

    if (control.value) {
      const matches = control.value.match(
        /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/i
      );

      if (!matches) {
        returnVar = {
          'invalidEmail': true
        };
      }
    }

    return returnVar;
  }

  isDisabled():boolean {
    return !this.acceptTermsForm.valid;
  }

  loadTermsAndCondition():void {
    this.isLoading = true;

    this.applicationService.getTermsAndConditions().pipe(
      finalize(
        () => {
          this.isLoading = false;
        }
      )
    ).subscribe(
      (response) => {
        if (!response.errors) {
          this.termsAndConditions = <TermsAndCondition[]>response.data;
        }
      }
    );
  }

  onSubmit() {
    if (!this.isDisabled()) {
      this.isCreatingUser = true;

      this.applicant.email = this.acceptTermsForm.get('emailAddress').value;
      this.applicant.approved_on = null;

      this.applicationService.apply(<User><unknown>this.applicant).pipe(
        finalize(
          () => {
            this.isCreatingUser = false;
          }
        )
      ).subscribe(
        (response) => {
          if (!response.errors) {
            let applicant = response.data;
            if (this.variable.isArray(applicant)) {
              if (applicant.length !== 0) {
                applicant = applicant[0];
              }
            }

            this.applicant = applicant;

            this.onAccept.emit(this.applicant);
          }
        }
      );
    }
  }
}
