import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';

import {RangeModule, SkeletonParagraphModule} from '@uab.lakeshore.collaborative/next-base';
import {UserService} from '@uab.lakeshore.collaborative/next-base';

import {ConfirmEmailComponent} from './confirm-email.component';

@NgModule({
  exports: [
    ConfirmEmailComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SkeletonParagraphModule,
    RangeModule,
    ReactiveFormsModule
  ],
  declarations: [
    ConfirmEmailComponent
  ],
  providers: [
    UserService
  ]
})
export class ConfirmEmailModule {}
