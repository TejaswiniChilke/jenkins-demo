import {Component, EventEmitter, Output} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {EnvironmentService, UserInterface, UserService} from '@uab.lakeshore.collaborative/next-base';
import {finalize} from 'rxjs/operators';
import {RequestService, UserStatuses} from '@uab.lakeshore.collaborative/next-base';
import {VariableService} from '@uab.lakeshore.collaborative/next-base';
import {DateService} from '@uab.lakeshore.collaborative/next-base';
import {AlertService} from '@uab.lakeshore.collaborative/next-base';
import {LoadingController} from '@ionic/angular';
import {RouteService} from '@uab.lakeshore.collaborative/next-base';
import {User} from '@uab.lakeshore.collaborative/next-base';

@Component({
  selector:    'uab-confirm-email',
  templateUrl: 'confirm-email.component.html',
  styleUrls: [
    'confirm-email.component.scss'
  ]
})
export class ConfirmEmailComponent {
  public baseUrl:string = '';
  public hasError:boolean;
  public hasExpired:boolean;
  public isConfirming:boolean;
  public isLoading:boolean;
  public loadingElement:any;
  public user:User;
  public errorText:string = 'Something went wrong. Please copy and paste the URL from the email again.';
  public thankYouText:string = 'Thank you for confirming your email.';

  @Output() onConfirm:EventEmitter<User>;

  constructor(
    private alert:AlertService,
    private date:DateService,
    private environment:EnvironmentService,
    private formBuilder:FormBuilder,
    private loadingController:LoadingController,
    private request:RequestService,
    private route:RouteService,
    private userService:UserService,
    private variable:VariableService
  ) {
    this.onConfirm = new EventEmitter<User>();

    this.reset();

    this.loadUser();
  }

  confirm() {
    this.isConfirming = true;

    if (this.variable.isDefined(this.user.email_confirmed_on === null)) {
      let request = this.request.getRequest();

      request = this.request.setPrefix(request, 'open');
      request = this.request.setModel(request, 'confirm');
      request = this.request.setAction(request, 'email', this.user.id);

      this.userService.editUser(this.user, request).pipe(
        finalize(
          () => {
            this.isConfirming = false;
          }
        )
      ).subscribe(
        (user) => {
          if (user) {
            this.alert.addToast(this.thankYouText).then();

            this.onConfirm.emit(this.user);
          } else {
            this.hasError = true;
          }

          this.hidePopup();
        }
      );
    } else {
      this.isConfirming = false;

      this.hidePopup();
    }
  }

  allowedAccess(user:UserInterface):boolean {
    if (user.status === UserStatuses.AWAITING_EMAIL_CONFIRMATION) {
      return true;
    }
    return user.status === UserStatuses.EMAIL_CONFIRMED;
  }

  loadUser():void {
    this.isLoading = true;

    let request = this.request.getRequest();

    request = this.request.setPrefix(request, 'open');

    const id = this.route.getStringParam('userId');

    if (id !== '') {
      this.showPopup().then();

      request = this.request.setId(request, id);

      this.userService.loadUser(request).pipe(
        finalize(
          () => {
            this.isLoading = false;
          }
        )
      ).subscribe(
        (user) => {
          if (user) {
            if (this.allowedAccess(user)) {
              this.user = <any>user;

              this.confirm();
            } else {
              this.hasExpired = true;
            }
          } else {
            this.hasError = true;
          }


          this.isConfirming = false;
          this.hidePopup();
        }
      );
    } else {
      this.hasError = true;
      this.isConfirming = false;
    }
  }

  reset():void {
    this.baseUrl = this.environment.getString('frontendUrl', '');

    this.hasError = false;

    this.hasExpired = false;

    this.isConfirming = true;

    this.isLoading = false;

    this.user = new User();
  }

  hidePopup():void {
    this.loadingElement.dismiss().then();
  }

  async showPopup():Promise<void> {
    this.loadingElement = await this.loadingController.create({
      message:     'Confirming...',
      translucent: true
    });

    this.loadingElement.present().then();
  }
}
