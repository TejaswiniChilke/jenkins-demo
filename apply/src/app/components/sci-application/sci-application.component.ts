import {Component, EventEmitter, Input, Output} from '@angular/core';

import {finalize} from 'rxjs/operators';

import {UserService} from '@uab.lakeshore.collaborative/next-base';
import {AlertService} from '@uab.lakeshore.collaborative/next-base';
import {BaseComponent} from '@uab.lakeshore.collaborative/next-base';
import {User} from '@uab.lakeshore.collaborative/next-base';
import {VariableService} from '@uab.lakeshore.collaborative/next-base';
import {DateService} from '@uab.lakeshore.collaborative/next-base';
import {RequestService} from '@uab.lakeshore.collaborative/next-base';
import {UserStatuses} from '@uab.lakeshore.collaborative/next-base';

@Component({
  selector:    'uab-sci-application',
  templateUrl: 'sci-application.component.html',
  styleUrls: [
    'sci-application.component.scss'
  ]
})
export class SciApplicationComponent implements BaseComponent {
  public user:User;
  public zipcodeText:string = 'Zip Code';

  @Input()
  set applicant(applicant:User) {
    this.user = applicant;

    if (this.formIsFilled()) {
      this.checkRequirements();

      if (!this.isDenied) {
        setTimeout(
          () => {
            this.onApply.emit(this.user);
          },
          1000
        );
      }
    }
  }

  @Input() showCancel:boolean = true;

  @Input() addressLineOneText:string = 'Address Line One';
  @Input() addressLineTwoText:string = 'Address Line Two';
  @Input() cancelText:string = 'Cancel';
  @Input() cityText:string = 'City';
  @Input() contactTimePreferenceLabel:string = 'Please select a preferred contact time if the study staff needs to reach you out via phone call.';
  @Input() contactTimePreferencePlaceholder:string = 'Preferred Contact Time';
  @Input() applyText:string = 'Apply';
  @Input() applyingText:string = 'Applying';
  @Input() dobText:string = 'Date of Birth';
  @Input() emailText:string = 'Email Address';
  @Input() firstNameText:string = 'First Name';
  @Input() lastNameText:string = 'Last Name';
  @Input() mobileNumberText:string = 'Mobile Number';
  @Input() stateLabel:string = 'State';
  @Input() statePlaceholder:string = 'State';

  @Output() onApply:EventEmitter<User>;
  @Output() onCancel:EventEmitter<boolean>;

  public contactPreferenceIsValid:boolean = false;
  public isStateAutoComplete:boolean = false;
  public isApplying:boolean;
  public isDenied:boolean;

  constructor(
    private alert:AlertService,
    private date:DateService,
    private request:RequestService,
    private userService:UserService,
    private variable:VariableService
  ) {
    this.onApply = new EventEmitter<User>();
    this.onCancel = new EventEmitter<boolean>();

    this.reset();
  }

  applyUser():void {
    this.isApplying = true;

    let request = this.request.getRequest();

    request = this.request.setPrefix(request, 'open');
    request = this.request.setSubAction(request, this.user.id);

    this.user.status = this.checkRequirements();

    this.userService.editUser(<User><unknown>this.user, request).pipe(
      finalize(
        () => {
          this.isApplying = false;
        }
      )
    ).subscribe(
      (response) => {
        if (this.variable.isObject(response)) {
          this.user = <User>response;

          if (!this.isDenied) {
            this.onApply.emit(this.user);
          }
        } else {
          this.alert.addError('Unable to submit application. Please try again later.');
        }
      }
    );
  }

  cancel():void {
    this.onCancel.emit(true);
  }

  checkRequirements():string {
/*    const dob = this.date.moment(
      this.variable.getString(this.user.dob)
    );*/

/*    const age = this.date.moment().diff(dob, 'years', false);
    if (age < 18 || age > 65) {
      this.isDenied = true;

      return UserStatuses.APPLICATION_REJECTED_AGE;
    }*/
    const is_in_age_range = this.variable.getBoolean(this.user.is_in_age_range, false);
    if (!is_in_age_range) {
      this.isDenied = true;
      return UserStatuses.APPLICATION_REJECTED_AGE;
    }

    const hasSci = this.variable.getBoolean(this.user.has_sci, false);
    if (!hasSci) {
      this.isDenied = true;

      return UserStatuses.APPLICATION_REJECTED_SCI;
    }

    return UserStatuses.APPLICATION_FILLED;
  }

  checkContactPreference(preference?:string):boolean {
    if (this.variable.isString(preference)) {
      preference = preference.replace('undefined', '');
      preference = preference.replace(' am', '');
      preference = preference.replace(' pm', '');
      preference = preference.trim();

      if (preference.indexOf('-') !== -1) {
        const moment = this.date.moment(preference);

        const hour = moment.hour();
        const minute = moment.minute();

        preference = hour + ':' + minute;
      }

      this.user.preferred_contact_time = preference;
    }

    preference = this.variable.getString(
      this.user.preferred_contact_time
    );

    console.log('BEFORE: ' + preference);

    preference = this.date.toTwelveHourTime(preference);

    console.log('AFTER: ' + preference);

    const today = this.date.moment().format('YYYY/MM/DD');

    const timeMoment = this.date.moment(
      today + ' ' + preference
    );

    const isAfterMin = timeMoment.isAfter(
      this.date.moment(today + ' 8:59 am')
    );

    const isBeforeMax = timeMoment.isBefore(
      this.date.moment(today + ' 6:01 pm')
    );

    this.contactPreferenceIsValid = isAfterMin && isBeforeMax;

    return this.contactPreferenceIsValid;
  }

  contactPreferenceIsDirty():boolean {
    return typeof this.user.preferred_contact_time !== 'undefined'
      && this.user.preferred_contact_time !== null;
  }

  formIsFilled():boolean {
    return this.contactPreferenceIsValid
      && this.variable.isString(this.user.first_name) && this.user.first_name.length !== 0
      && this.variable.isString(this.user.last_name) && this.user.last_name.length !== 0
      && this.variable.isString(this.user.address_line_one) && this.user.address_line_one.length !== 0
      && this.variable.isString(this.user.city) && this.user.city.length !== 0
      && this.variable.isString(this.user.state_text) && this.user.state_text.length !== 0
      && this.variable.isString(this.user.zipcode) && this.user.zipcode.length !== 0
      && this.variable.isString(this.user.mobile_number) && this.user.mobile_number.length !== 0
      && this.variable.isString(this.user.preferred_contact_type) && this.user.preferred_contact_type.length !== 0
      && this.variable.isString(this.user.email) && this.user.email.length !== 0
      && this.user.has_sci !== null
      && this.user.is_in_age_range !== null;
  }

  reset():void {
    this.isDenied = false;

    this.isApplying = false;
  }
}
