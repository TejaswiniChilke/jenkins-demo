import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';

import {SciApplicationComponent} from './sci-application.component';
import {ButtonGroupModule, ZipcodeInputModule} from '@uab.lakeshore.collaborative/next-forms';
import {GenderToggleModule} from '@uab.lakeshore.collaborative/next-forms';
import {AgeToggleModule} from '@uab.lakeshore.collaborative/next-forms';
import {InputModule} from '@uab.lakeshore.collaborative/next-forms';
import {PhoneNumberInputModule} from '@uab.lakeshore.collaborative/next-forms';
import {PreferredContactToggleModule} from '@uab.lakeshore.collaborative/next-forms';
import {SciToggleModule} from '@uab.lakeshore.collaborative/next-forms';
import {StateSelectModule} from '@uab.lakeshore.collaborative/next-forms';
import {DateModule} from '@uab.lakeshore.collaborative/next-forms';
import {TimeModule} from '@uab.lakeshore.collaborative/next-forms';

@NgModule({
  declarations: [
    SciApplicationComponent
  ],
  exports: [
    SciApplicationComponent
  ],
  imports: [
    ButtonGroupModule,
    CommonModule,
    DateModule,
    FormsModule,
    GenderToggleModule,
    AgeToggleModule,
    InputModule,
    IonicModule,
    PhoneNumberInputModule,
    PreferredContactToggleModule,
    SciToggleModule,
    StateSelectModule,
    TimeModule,
    ZipcodeInputModule
  ]
})
export class SciApplicationModule {}
