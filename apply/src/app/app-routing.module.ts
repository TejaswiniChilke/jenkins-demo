import {NgModule} from '@angular/core';

import {Routes, RouterModule} from '@angular/router';

import {LoginGuard} from '@uab.lakeshore.collaborative/next-base';
import {LogoutGuard} from '@uab.lakeshore.collaborative/next-base';

const routes:Routes = [
  {
    path: 'apply',
    loadChildren: 
      () => import(
        './pages/apply/apply.module'
      ).then(
        m => m.ApplyPageModule
      ),
    canActivate: [
      LogoutGuard
    ]
  },
  {
    path: 'apply/:userId',
    loadChildren:
      () => import(
        './pages/apply/apply.module'
        ).then(
        m => m.ApplyPageModule
      )
  },
  {
    path: 'confirm-email',
    loadChildren: 
      () => import(
        './pages/confirm-email/confirm-email.module'
      ).then(
        m => m.ConfirmEmailPageModule
      )
  },
  {
    path: 'confirm-email/:userId',
    loadChildren: 
      () => import(
        './pages/confirm-email/confirm-email.module'
      ).then(
        m => m.ConfirmEmailPageModule
      )
  },
  {
    path: 'logout',
    loadChildren:
      () => import(
        './pages/logout/logout.module'
        ).then(
        m => m.LogoutPageModule
      ),
    data: {
      username: '',
      password: ''
    },
    canActivate: [
      LoginGuard
    ]
  },
  {
    path: 'version',
    loadChildren:
      () => import(
        './pages/version/version.module'
        ).then(
        m => m.VersionPageModule
      )
  },
  {
    path: '',
    redirectTo: 'apply',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: 'apply',
    pathMatch: 'full'
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(
      routes,
      { enableTracing: false, useHash: true, relativeLinkResolution: 'legacy' }
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}
