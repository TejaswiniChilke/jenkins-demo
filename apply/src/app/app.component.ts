import {Component} from '@angular/core';

import {finalize} from 'rxjs/operators';

import {Platform} from '@ionic/angular';
import {StatusBar} from '@ionic-native/status-bar/ngx';

import {AccessibilityService, ActiveUser, AnalyticStateVisitService} from '@uab.lakeshore.collaborative/next-base';

import {LoginService} from '@uab.lakeshore.collaborative/next-base';
import {FeatureUserType} from '@uab.lakeshore.collaborative/next-base';
import {Request} from '@uab.lakeshore.collaborative/next-base';
import {RequestService} from '@uab.lakeshore.collaborative/next-base';
import {RegistryService} from '@uab.lakeshore.collaborative/next-base';
import {PlatformService} from '@uab.lakeshore.collaborative/next-base';
import {RouteService} from '@uab.lakeshore.collaborative/next-base';
import {SplashScreenService} from '@uab.lakeshore.collaborative/next-base';

@Component({
  selector:    'uab-root',
  templateUrl: 'app.component.html',
})
export class AppComponent {
  public activeUser:ActiveUser|boolean;
  public isLoadingFeatures:boolean = true;
  public isLoadingActiveUser:boolean;
  public isLoadingSections:boolean;
  public isLoggedIn:boolean;
  public showSplitPane:boolean = false;
  public request:Request;

  public features:FeatureUserType[];

  constructor(
    private accessibility:AccessibilityService,
    private analytics:AnalyticStateVisitService,
    private loginService:LoginService,
    private platformService:PlatformService,
    private platform:Platform,
    private registry:RegistryService,
    private requestService:RequestService,
    private route:RouteService,
    private splashService:SplashScreenService,
    private statusBar:StatusBar,
  ) {
    this.analytics.bootstrap().subscribe();

    this.loginService.registerLoggingIn(
      () => {
        this.reset();

        this.initializeApp();
      }
    );

    this.reset();
    this.initializeApp();
  }

  completeFeatures() {
    this.isLoadingFeatures = false;

    this.toggleSplitPane();
  }

  toggleSplitPane() {
    this.showSplitPane = !this.showSplitPane;
  }

  initializeApp() {
    this.splashService.show('app-component-initializing');

    this.platform.ready().then(
      () => {
        if (this.platformService.isApplication()) {
          this.statusBar.styleDefault();
        }

        this.accessibility.loadAccessibilitySettings();

        this.loginService.registerLogin(
          () => {
            this.loadActiveUser();
          }
        );

        if (this.loginService.isLoggedIn()) {
          this.loadActiveUser();
        }

        this.splashService.hide('app-component-initializing');
      }
    );
  }

  loadActiveUser() {
    this.isLoadingActiveUser = true;

    this.splashService.show('app-component-loading-active-user');

    this.loginService.getActiveUser().pipe(
      finalize(
        () => {
          this.isLoadingActiveUser = false;

          this.splashService.hide('app-component-loading-active-user');
        }
      )
    ).subscribe(
      (activeUser) => {
        if (activeUser) {
          this.activeUser = activeUser;

          this.isLoggedIn = true;
        }
      }
    );
  }

  onLogout() {
    this.loginService.logout().subscribe();
  }

  reset() {
    this.activeUser = false;

    this.isLoadingActiveUser = false;

    this.isLoadingSections = false;

    this.isLoggedIn = false;

    this.resetRequest();
  }

  resetRequest() {
    this.request = this.requestService.getRequest();

    this.request = this.requestService.setJoins(
      this.request,
      [
        '>features'
      ]
    );

    this.request = this.requestService.setLimit(
      this.request,
      100
    );
  }
}
