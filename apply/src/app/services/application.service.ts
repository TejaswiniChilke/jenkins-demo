import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';

import {Request} from '@uab.lakeshore.collaborative/next-base';
import {User} from '@uab.lakeshore.collaborative/next-base';

import {DateService} from '@uab.lakeshore.collaborative/next-base';
import {VariableService} from '@uab.lakeshore.collaborative/next-base';
import {EnvironmentService} from '@uab.lakeshore.collaborative/next-base';
import {ListService} from '@uab.lakeshore.collaborative/next-base';
import {LocalStorageService} from '@uab.lakeshore.collaborative/next-base';
import {LoginService} from '@uab.lakeshore.collaborative/next-base';
import {RequestService} from '@uab.lakeshore.collaborative/next-base';
import {Response} from '@uab.lakeshore.collaborative/next-base';
import {BaseCrudService} from '@uab.lakeshore.collaborative/next-base';
import {StringService} from '@uab.lakeshore.collaborative/next-base';

@Injectable()
export class ApplicationService extends BaseCrudService {
  public model = false;

  constructor(
    private date:DateService,
    private variable:VariableService,
    private environment:EnvironmentService,
    private local:LocalStorageService,
    private login:LoginService,
    private list:ListService,
    private request:RequestService,
    private string:StringService
  ) {
    super(
      variable,
      environment,
      list,
      request,
      string
    );
  }

  apply(newUser:User, request?:Request) {
    request = this.request.getRequest(request);

    request = this.request.setPrefix(request, 'onboarding');
    request = this.request.setModel(request, 'users');
    request = this.request.setAction(request, 'apply');

    return super._findOrCreate(newUser, request);
  }

  getTermsAndConditions(request?:Request):Observable<Response> {
    request = this.request.getRequest(request);

    request = this.request.setPrefix(request, 'onboarding');
    request = this.request.setModel(request, 'terms_and_conditions');

    request = this.request.setLimit(request, 100);

    return super._getMore(request);
  }

  getColumns(request?:Request):Observable<Response> {
    return super._getColumns(request);
  }

  getMore(request?: Request) {
    return this.getTermsAndConditions(request);
  }

  getPage(request?: Request) {
    return this.getTermsAndConditions(request);
  }
}
