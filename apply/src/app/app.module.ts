import {APP_INITIALIZER, ErrorHandler, NgModule} from '@angular/core';

import {Router, RouteReuseStrategy} from '@angular/router';

import {IonicModule, IonicRouteStrategy} from '@ionic/angular';

import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {ServiceWorkerModule} from '@angular/service-worker';

import {PhotoViewer} from '@ionic-native/photo-viewer/ngx'; // TODO: Remove, import where needed
import {StatusBar} from '@ionic-native/status-bar/ngx'; // TODO: Remove, import where needed
import {Toast} from '@ionic-native/toast/ngx'; // TODO: Remove, import where needed

import * as Sentry from '@sentry/angular';
import {TraceService} from '@sentry/angular';

import {ImgFallbackModule} from 'ngx-img-fallback'; // TODO: Remove, import where needed

import {NgPluralizeModule} from 'ng-pluralize';

import {environment} from '../environments/environment';

import {NextBaseModule} from '@uab.lakeshore.collaborative/next-base';
import {NextFormsModule} from '@uab.lakeshore.collaborative/next-forms';
import {NextNotificationsModule} from '@uab.lakeshore.collaborative/next-notifications';
import {NextTechModule} from '@uab.lakeshore.collaborative/next-tech';

import {EnvironmentConfigService} from '@uab.lakeshore.collaborative/next-base';
import {ColorTemplateService} from '@uab.lakeshore.collaborative/next-base';
import {LingoService} from '@uab.lakeshore.collaborative/next-base';

export function ConfigLoader(
  configService:EnvironmentConfigService,
  colorTemplateService:ColorTemplateService,
  lingoService:LingoService
) {
  return () => {
    return configService.load(
      '/assets/config.json',
      environment
    ).then(
      (config) => {
        const url = config.backendUrl;

        colorTemplateService.bootstrap(url).subscribe(
          () => {
            lingoService.init(url).subscribe();
          }
        );
      }
    );
  };
}

import {AppComponent} from './app.component';

import {AppRoutingModule} from './app-routing.module';

import {ChunkErrorHandler} from './components/chunk-error-handler';

import {PwaService} from './services/pwa.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  entryComponents: [],
  exports: [],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    IonicModule.forRoot(),
    NextBaseModule.forRoot(),
    NextFormsModule.forRoot(),
    NextNotificationsModule.forRoot(),
    NextTechModule.forRoot(),
    NgPluralizeModule,
    ServiceWorkerModule.register(
      'ngsw-worker.js',
      {
        enabled: environment.environment !== 'local'
      }
    ),
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.environment !== 'local' })
  ],
  providers: [
    {
      provide:  ErrorHandler,
      useClass: ChunkErrorHandler
    },
    {
      provide: ErrorHandler,
      useValue: Sentry.createErrorHandler({
        showDialog: false,
      }),
    },
    {
      provide: TraceService,
      deps: [
        Router
      ],
    },
    {
      provide: APP_INITIALIZER,
      useFactory: () => () => {},
      deps: [Sentry.TraceService],
      multi: true,
    },
    {
      deps: [
        EnvironmentConfigService,
        ColorTemplateService,
        LingoService,
        TraceService
      ],
      provide:    APP_INITIALIZER,
      multi:      true,
      useFactory: ConfigLoader
    },
    {
      provide:  RouteReuseStrategy,
      useClass: IonicRouteStrategy
    },
    ImgFallbackModule,
    PhotoViewer,
    PwaService,
    StatusBar,
    Toast
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {}
