import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {RouterModule} from '@angular/router';

import {HeaderModule, RangeModule, SkeletonBlockModule} from '@uab.lakeshore.collaborative/next-base';

import {ButtonGroupModule} from '@uab.lakeshore.collaborative/next-forms';
import {StateListService} from '@uab.lakeshore.collaborative/next-forms';
import {StateSelectProvider} from '@uab.lakeshore.collaborative/next-forms';
import {BrowserCheckModule} from '@uab.lakeshore.collaborative/next-tech';
import {SpeedCheckModule} from '@uab.lakeshore.collaborative/next-tech';

import {AcceptTermsModule} from '../../components/accept-terms/accept-terms.module';
import {SciApplicationModule} from '../../components/sci-application/sci-application.module';

import {ApplyPage} from './apply.page';

@NgModule({
  imports: [
    AcceptTermsModule,
    ButtonGroupModule,
    BrowserCheckModule,
    CommonModule,
    FormsModule,
    HeaderModule,
    IonicModule,
    RangeModule,
    RouterModule.forChild([
      {
        path: '',
        component: ApplyPage
      }
    ]),
    SciApplicationModule,
    SkeletonBlockModule,
    SpeedCheckModule
  ],
  declarations: [
    ApplyPage
  ],
  providers: [
    StateSelectProvider,
    StateListService,
  ]
})
export class ApplyPageModule {}
