import {Component} from '@angular/core';

import {BasePage, User, UserInterface} from '@uab.lakeshore.collaborative/next-base';

import {UserService} from '@uab.lakeshore.collaborative/next-base';
import {AlertService} from '@uab.lakeshore.collaborative/next-base';
import {LingoService} from '@uab.lakeshore.collaborative/next-base';
import {VariableService} from '@uab.lakeshore.collaborative/next-base';
import {TechCheckService} from '@uab.lakeshore.collaborative/next-tech';
import {finalize} from 'rxjs/operators';
import {RequestService} from '@uab.lakeshore.collaborative/next-base';
import {UserStatuses} from '@uab.lakeshore.collaborative/next-base';
import {RouteService} from '@uab.lakeshore.collaborative/next-base';

@Component({
  selector:    'uab-apply-page',
  templateUrl: 'apply.page.html',
  styleUrls: [
    'apply.page.scss'
  ]
})
export class ApplyPage implements BasePage {
  public applicationComplete:boolean;
  public hasAcceptedTerms:boolean;
  public hasCheckedBrowser:boolean;
  public hasCheckedSpeed:boolean;
  public hasCompleted:boolean;
  public hasConfirmedEmail:boolean;
  public isApplying:boolean;
  public isLoadingUser:boolean;
  public isResending:boolean;
  public isReturning:boolean;
  public showApplication:boolean;
  public showBrowserCheck:boolean;
  public showEmailConfirmation:boolean;
  public showRejected:boolean;
  public showResend:boolean = true;
  public showSpeedCheck:boolean;
  public showTerms:boolean;
  public applicant:UserInterface;

  public pageTitle:string;
  public addressLineOneText:string;
  public addressLineTwoText:string;
  public acceptTermsButtonText:string;
  public acceptingButtonText:string;
  public browserCheckText:string;
  public browserCheckingText:string;
  public adminSectionText:string;
  public cancelText:string;
  public cityText:string;
  public contactSectionText:string;
  public applyText:string;
  public applyingText:string;
  public emailText:string;
  public firstNameText:string;
  public lastNameText:string;
  public nextText:string;
  public loginSectionText:string;
  public mobileNumberText:string;
  public passwordText:string;
  public personalSectionText:string;
  public usernameText:string;
  public securityQuestionSectionText:string;
  public securityQuestionText:string;
  public securityAnswerText:string;
  public speedCheckText:string;
  public speedCheckingText:string;
  public stateLabel:string;

  constructor(
    private alert:AlertService,
    private lingo:LingoService,
    private request:RequestService,
    private route:RouteService,
    private tech:TechCheckService,
    private userService:UserService,
    private variable:VariableService
  ) {
    this.loadLingo();

    this.reset();

    this.loadUser();
  }

  apply():void {
    this.isApplying = true;

    let request = this.request.getRequest();

    request = this.request.setPrefix(request, 'onboarding');
    request = this.request.setAction(request, 'finalize', this.applicant.id);

    request = this.request.setJoins(
      request,
      [
        '<images',
        '<states'
      ]
    );

    this.userService.loadUser(request).pipe(
      finalize(
        () => {
          this.isApplying = false;
        }
      )
    ).subscribe();
  }

  cancelApplication():void {
    this.showApplication = true;
    this.showBrowserCheck = false;
    this.showRejected = false;
    this.showSpeedCheck = false;
  }

  cancelSpeedCheck():void {
    this.showApplication = false;
    this.showBrowserCheck = true;
    this.showRejected = false;
    this.showSpeedCheck = false;
  }

  completeApplication(applicant:UserInterface):void {
    this.applicationComplete = true;

    this.setApplicant(applicant);
  }

  loadLingo():void {
    this.addressLineOneText = this.lingo.get(
      'apply.terms-and-conditions.address-line-one.label',
      'Address Line 1'
    );

    this.addressLineTwoText = this.lingo.get(
      'apply.terms-and-conditions.address-line-two.label',
      'Address Line 2'
    );

    this.acceptTermsButtonText = this.lingo.get(
      'apply.terms-and-conditions.accept.button.label',
      'Accept'
    );

    this.acceptingButtonText = this.lingo.get(
      'apply.terms-and-conditions.accepting.button.label',
      'Accepting...'
    );

    this.adminSectionText = this.lingo.get(
      'apply.section.title.loginDetails',
      'Admin'
    );

    this.cancelText = this.lingo.get(
      'apply.button.cancel.label',
      'Cancel'
    );

    this.browserCheckText = this.lingo.get(
      'apply.browser.check.label',
      'Check Browser'
    );

    this.browserCheckingText = this.lingo.get(
      'apply.browser.checking.label',
      'Checking...'
    );

    this.speedCheckText = this.lingo.get(
      'apply.speed.check.label',
      'Check Internet'
    );

    this.speedCheckingText = this.lingo.get(
      'apply.speed.check.label',
      'Checking...'
    );

    this.cityText = this.lingo.get(
      'apply.column.city.label',
      'City'
    );

    this.applyText = this.lingo.get(
      'apply.button.apply.label',
      'Apply'
    );

    this.applyingText = this.lingo.get(
      'apply.button.applying.label',
      'Applying...'
    );

    this.contactSectionText = this.lingo.get(
      'apply.section.title.contactDetails',
      'Contact'
    );

    this.emailText = this.lingo.get(
      'apply.column.email.label',
      'Email'
    );

    this.firstNameText = this.lingo.get(
      'apply.column.firstName.label',
      'First Name'
    );

    this.lastNameText = this.lingo.get(
      'apply.column.lastName.label',
      'Last Name'
    );

    this.loginSectionText = this.lingo.get(
      'apply.section.title.loginDetails',
      'Login Details'
    );

    this.mobileNumberText = this.lingo.get(
      'apply.column.mobileNumber.label',
      'Mobile Number'
    );

    this.nextText = this.lingo.get(
      'apply.button.continue.label',
      'Continue'
    );

    this.passwordText = this.lingo.get(
      'apply.column.password.label',
      'Password'
    );

    this.personalSectionText = this.lingo.get(
      'apply.section.title.personalDetails',
      'Personal Details'
    );

    this.stateLabel = this.lingo.get(
      'apply.column.state.label',
      'State'
    );

    this.pageTitle = this.lingo.get(
      'apply.page.title',
      'Apply'
    );

    this.usernameText = this.lingo.get(
      'apply.column.username.label',
      'Username'
    );

    this.securityQuestionSectionText = this.lingo.get(
      'apply.section.title.securityQuestionDetails',
      'Security Question'
    );

    this.securityQuestionText = this.lingo.get(
      'apply.column.securityQuestion.label',
      'Security Question'
    );

    this.securityAnswerText = this.lingo.get(
      'apply.column.securityAnswer.label',
      'Security Answer'
    );
  }

  loadUser():void {
    this.isLoadingUser = true;

    const id = this.route.getStringParam('userId');

    if (id !== '') {
      let request = this.request.getRequest();

      request = this.request.setPrefix(request, 'open');
      request = this.request.setId(request, id);

      request = this.request.setJoins(
        request,
        [
          '<images',
          '<states'
        ]
      );

      this.userService.loadUser(request).pipe(
        finalize(
          () => {
            this.isLoadingUser = false;
          }
        )
      ).subscribe(
        (user) => {
          if (user) {
            this.setApplicant(<any>user);
          }
        }
      );
    } else {
      this.isLoadingUser = false;
    }
  }

  openApplication():void {
    if (this.hasConfirmedEmail && this.hasCheckedBrowser && this.hasCheckedSpeed) {
      this.showApplication = true;
      this.showBrowserCheck = false;
      this.showRejected = false;
      this.showSpeedCheck = false;
    }
  }

  openBrowserCheck():void {
    if (this.hasConfirmedEmail) {
      this.showApplication = false;
      this.showBrowserCheck = true;
      this.showRejected = false;
      this.showSpeedCheck = false;
    }
  }

  openSpeedCheck():void {
    if (this.hasConfirmedEmail && this.hasCheckedBrowser) {
      this.showApplication = false;
      this.showBrowserCheck = false;
      this.showSpeedCheck = true;
    }
  }

  resend():void {
    this.isResending = true;

    let request = this.request.getRequest();

    request = this.request.setPrefix(request, 'open');
    request = this.request.setModel(request, 'confirm');
    request = this.request.setAction(request, 'resend', this.applicant.id);

    this.userService.loadUser(request).pipe(
      finalize(
        () => {
          this.isResending = false;
        }
      )
    ).subscribe(
      (user) => {
        if (user) {
          this.alert.addMessage('Confirmation email has been resent.');

          this.showResend = false;
        } else {
          this.alert.addError('Unable to resend email. Try again later.');
        }
      }
    );
  }

  reset():void {
    this.applicationComplete = false;

    this.hasAcceptedTerms = false;

    this.hasCheckedBrowser = false;

    this.hasCheckedSpeed = false;

    this.hasCompleted = false;

    this.hasConfirmedEmail = false;

    this.isApplying = false;

    this.isLoadingUser = true;

    this.isResending = false;

    this.isReturning = false;

    this.showApplication = false;

    this.showBrowserCheck = false;

    this.showEmailConfirmation = false;

    this.showSpeedCheck = false;

    this.showTerms = true;

    this.applicant = new User();
  }

  setApplicant(applicant:UserInterface):void {
    this.applicant = applicant;

    this.showApplication = false;
    this.showBrowserCheck = false;
    this.showRejected = false;
    this.showSpeedCheck = false;
    this.showTerms = false;

    if (this.applicant.email) {
      this.hasAcceptedTerms = true;

      if (this.applicant.status === UserStatuses.AWAITING_EMAIL_CONFIRMATION) {
        this.showEmailConfirmation = true;
      } else {
        this.hasConfirmedEmail = true;

        if (this.applicant.status === UserStatuses.EMAIL_CONFIRMED) {
          this.showApplication = true;
        } else {
          this.applicationComplete = true;

          if (
            this.applicant.status === UserStatuses.APPLICATION_REJECTED_AGE
            || this.applicant.status === UserStatuses.APPLICATION_REJECTED_SCI
            || this.applicant.status === UserStatuses.DEACTIVATED
            || this.applicant.status === UserStatuses.APPLICATION_DENIED
          ) {
            this.showRejected = true;
          } else {
            applicant.browser_check = this.variable.getString(
              applicant.browser_check
            );

            const browserCheck = applicant.browser_check.split('@');

            this.hasCheckedBrowser = browserCheck.length === 2 && this.tech.isSupportedBrowser(
              this.variable.getString(browserCheck[0]),
              this.variable.getInteger(browserCheck[1])
            );

            if (this.hasCheckedBrowser) {
              const speed = this.variable.getInteger(
                this.applicant.speed_check,
                -1
              );

              if (this.tech.isSpeedSupported(speed)) {
                this.hasCheckedSpeed = true;

                this.hasCompleted = true;

                if (this.applicant.status === UserStatuses.SPEED_CONFIRMED) {
                  this.isReturning = false;

                  this.apply();
                } else {
                  this.isReturning = true;
                }
              } else {
                this.showSpeedCheck = true;
              }
            } else {
              this.showBrowserCheck = true;
            }
          }
        }
      }
    } else {
      this.showTerms = true;
    }
  }
}
