import {Component} from '@angular/core';

import {LoginService} from '@uab.lakeshore.collaborative/next-base';
import {RedirectService} from '@uab.lakeshore.collaborative/next-base';

@Component({
  selector:    'page-logout',
  templateUrl: 'logout.page.html'
})
export class LogoutPage {
  constructor(
    private loginService:LoginService,
    private redirect:RedirectService
  ) {
    this.redirect.goTo('login');

    this.loginService.logout().subscribe();
  }
}
