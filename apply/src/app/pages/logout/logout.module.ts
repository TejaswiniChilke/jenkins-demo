import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';

import {IonicModule} from '@ionic/angular';

import {LogoutPage} from './logout.page';
import {ButtonGroupModule} from '@uab.lakeshore.collaborative/next-forms';

const routes: Routes = [
  {
  path:    '',
  component: LogoutPage
  }
];

@NgModule({
  declarations: [
    LogoutPage
  ],
  imports: [
    ButtonGroupModule,
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ]
})
export class LogoutPageModule {}
