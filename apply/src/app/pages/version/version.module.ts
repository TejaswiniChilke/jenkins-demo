import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

import {IonicModule} from '@ionic/angular';

import {HeaderModule} from '@uab.lakeshore.collaborative/next-base';

import {VersionPage} from './version.page';

@NgModule({
  declarations: [
    VersionPage
  ],
  imports: [
    CommonModule,
    FormsModule,
    HeaderModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: VersionPage
      }
    ]),
  ]
})
export class VersionPageModule {}
