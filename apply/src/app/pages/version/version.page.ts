import {Component} from '@angular/core';

import {BasePage} from '@uab.lakeshore.collaborative/next-base';

import {EnvironmentService} from '@uab.lakeshore.collaborative/next-base';
import {LingoService} from '@uab.lakeshore.collaborative/next-base';

@Component({
  selector:    'uab-version-page',
  templateUrl: 'version.page.html',
  styleUrls: [
    'version.page.scss'
  ]
})
export class VersionPage implements BasePage {
  public version:string;

  public pageTitle:string;

  constructor(
    private environment:EnvironmentService,
    private lingo:LingoService
  ) {
    this.loadLingo();

    this.reset();
  }

  loadLingo():void {
    this.pageTitle = this.lingo.get(
      'version.page.title',
      'Version'
    );
  }

  reset():void {
    this.version = this.environment.getString('version', '');
  }
}
