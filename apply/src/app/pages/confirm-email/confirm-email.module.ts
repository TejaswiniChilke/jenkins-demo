import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {RouterModule} from '@angular/router';

import {ConfirmEmailPage} from './confirm-email.page';

import {HeaderModule} from '@uab.lakeshore.collaborative/next-base';
import {ConfirmEmailModule} from '../../components/confirm-email/confirm-email.module';

@NgModule({
  imports: [
    CommonModule,
    ConfirmEmailModule,
    FormsModule,
    HeaderModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: ConfirmEmailPage
      }
    ])
  ],
  declarations: [
    ConfirmEmailPage
  ]
})
export class ConfirmEmailPageModule {}
