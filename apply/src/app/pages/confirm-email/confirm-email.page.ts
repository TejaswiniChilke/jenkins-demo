import {Component} from '@angular/core';
import {BasePage} from '@uab.lakeshore.collaborative/next-base';
import {LingoService} from '@uab.lakeshore.collaborative/next-base';
import {UserInterface} from '@uab.lakeshore.collaborative/next-base';
import {RedirectService} from '@uab.lakeshore.collaborative/next-base';

@Component({
  selector:    'uab-confirm-email-page',
  templateUrl: 'confirm-email.page.html',
  styleUrls: [
    'confirm-email.page.scss'
  ]
})
export class ConfirmEmailPage implements BasePage {
  public pageTitle:string;

  constructor(
    private lingo:LingoService,
    private redirect:RedirectService
) {
    this.loadLingo();
  }

  goToApplication(applicant:UserInterface) {
    this.redirect.goTo(
        'apply/' + applicant.id
    );
  }

  reset() {

  }

  loadLingo() {
    this.pageTitle = this.lingo.get(
      'confirm-email.page.title',
      'Email Confirmation'
    );
  }
}
