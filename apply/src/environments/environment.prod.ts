export const environment = {
  environment: 'prod',
  frontendUrl: 'https://screen.scipe.org',
  backendUrl:  'https://screen.scipe.org/laravel',
  socketUrl:   'https://screen.scipe.org:3000',
  project:     'r3',
  version:     '0.5.1'
};
