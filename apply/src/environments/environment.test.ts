export const environment = {
  environment: 'prod',
  frontendUrl: 'https://screentest.scipe.org',
  backendUrl:  'https://screentest.scipe.org/laravel',
  socketUrl:   'https://app.scipe.org:3000',
  project:     'r3',
  version:     '0.5.1'
};
