'use strict';


customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">r3-apply documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AcceptTermsModule.html" data-type="entity-link">AcceptTermsModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AcceptTermsModule-ea5832459c182d693b5bd202d4829191"' : 'data-target="#xs-components-links-module-AcceptTermsModule-ea5832459c182d693b5bd202d4829191"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AcceptTermsModule-ea5832459c182d693b5bd202d4829191"' :
                                            'id="xs-components-links-module-AcceptTermsModule-ea5832459c182d693b5bd202d4829191"' }>
                                            <li class="link">
                                                <a href="components/AcceptTermsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AcceptTermsComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-AcceptTermsModule-ea5832459c182d693b5bd202d4829191"' : 'data-target="#xs-injectables-links-module-AcceptTermsModule-ea5832459c182d693b5bd202d4829191"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AcceptTermsModule-ea5832459c182d693b5bd202d4829191"' :
                                        'id="xs-injectables-links-module-AcceptTermsModule-ea5832459c182d693b5bd202d4829191"' }>
                                        <li class="link">
                                            <a href="injectables/ApplicationService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>ApplicationService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/ApplyPageModule.html" data-type="entity-link">ApplyPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ApplyPageModule-a159170e0d61f943e310bda55bfcd1c8"' : 'data-target="#xs-components-links-module-ApplyPageModule-a159170e0d61f943e310bda55bfcd1c8"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ApplyPageModule-a159170e0d61f943e310bda55bfcd1c8"' :
                                            'id="xs-components-links-module-ApplyPageModule-a159170e0d61f943e310bda55bfcd1c8"' }>
                                            <li class="link">
                                                <a href="components/ApplyPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ApplyPage</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-ApplyPageModule-a159170e0d61f943e310bda55bfcd1c8"' : 'data-target="#xs-injectables-links-module-ApplyPageModule-a159170e0d61f943e310bda55bfcd1c8"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-ApplyPageModule-a159170e0d61f943e310bda55bfcd1c8"' :
                                        'id="xs-injectables-links-module-ApplyPageModule-a159170e0d61f943e310bda55bfcd1c8"' }>
                                        <li class="link">
                                            <a href="injectables/StateListService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>StateListService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/StateSelectProvider.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>StateSelectProvider</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link">AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-aeab4af3f75552c4b109c7cc52051485"' : 'data-target="#xs-components-links-module-AppModule-aeab4af3f75552c4b109c7cc52051485"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-aeab4af3f75552c4b109c7cc52051485"' :
                                            'id="xs-components-links-module-AppModule-aeab4af3f75552c4b109c7cc52051485"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AppComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link">AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ConfirmationModule.html" data-type="entity-link">ConfirmationModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ConfirmationModule-1f6ec81cc375a1ab2dee726f2266010c"' : 'data-target="#xs-components-links-module-ConfirmationModule-1f6ec81cc375a1ab2dee726f2266010c"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ConfirmationModule-1f6ec81cc375a1ab2dee726f2266010c"' :
                                            'id="xs-components-links-module-ConfirmationModule-1f6ec81cc375a1ab2dee726f2266010c"' }>
                                            <li class="link">
                                                <a href="components/ConfirmationComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ConfirmationComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-ConfirmationModule-1f6ec81cc375a1ab2dee726f2266010c"' : 'data-target="#xs-injectables-links-module-ConfirmationModule-1f6ec81cc375a1ab2dee726f2266010c"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-ConfirmationModule-1f6ec81cc375a1ab2dee726f2266010c"' :
                                        'id="xs-injectables-links-module-ConfirmationModule-1f6ec81cc375a1ab2dee726f2266010c"' }>
                                        <li class="link">
                                            <a href="injectables/ConfirmationComponentController.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>ConfirmationComponentController</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/ConfirmEmailModule.html" data-type="entity-link">ConfirmEmailModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ConfirmEmailModule-ee6e8f5aedb34defa5226d32ae9623e8"' : 'data-target="#xs-components-links-module-ConfirmEmailModule-ee6e8f5aedb34defa5226d32ae9623e8"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ConfirmEmailModule-ee6e8f5aedb34defa5226d32ae9623e8"' :
                                            'id="xs-components-links-module-ConfirmEmailModule-ee6e8f5aedb34defa5226d32ae9623e8"' }>
                                            <li class="link">
                                                <a href="components/ConfirmEmailComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ConfirmEmailComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ConfirmEmailPageModule.html" data-type="entity-link">ConfirmEmailPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ConfirmEmailPageModule-408e3629c448cfe2ebf1becea065ccdd"' : 'data-target="#xs-components-links-module-ConfirmEmailPageModule-408e3629c448cfe2ebf1becea065ccdd"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ConfirmEmailPageModule-408e3629c448cfe2ebf1becea065ccdd"' :
                                            'id="xs-components-links-module-ConfirmEmailPageModule-408e3629c448cfe2ebf1becea065ccdd"' }>
                                            <li class="link">
                                                <a href="components/ConfirmEmailPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ConfirmEmailPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/LogoutPageModule.html" data-type="entity-link">LogoutPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-LogoutPageModule-0a8a34008601cd082310dd7f8d91edd9"' : 'data-target="#xs-components-links-module-LogoutPageModule-0a8a34008601cd082310dd7f8d91edd9"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-LogoutPageModule-0a8a34008601cd082310dd7f8d91edd9"' :
                                            'id="xs-components-links-module-LogoutPageModule-0a8a34008601cd082310dd7f8d91edd9"' }>
                                            <li class="link">
                                                <a href="components/LogoutPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">LogoutPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/SciApplicationModule.html" data-type="entity-link">SciApplicationModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SciApplicationModule-89c4a8f9d76c2e89926df99a3caefa3f"' : 'data-target="#xs-components-links-module-SciApplicationModule-89c4a8f9d76c2e89926df99a3caefa3f"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SciApplicationModule-89c4a8f9d76c2e89926df99a3caefa3f"' :
                                            'id="xs-components-links-module-SciApplicationModule-89c4a8f9d76c2e89926df99a3caefa3f"' }>
                                            <li class="link">
                                                <a href="components/SciApplicationComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SciApplicationComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/SegmentHeaderModule.html" data-type="entity-link">SegmentHeaderModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SegmentHeaderModule-0e440b9117643a7a441560ad8c01d624"' : 'data-target="#xs-components-links-module-SegmentHeaderModule-0e440b9117643a7a441560ad8c01d624"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SegmentHeaderModule-0e440b9117643a7a441560ad8c01d624"' :
                                            'id="xs-components-links-module-SegmentHeaderModule-0e440b9117643a7a441560ad8c01d624"' }>
                                            <li class="link">
                                                <a href="components/SegmentHeaderComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SegmentHeaderComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/SkeletonBlockModule.html" data-type="entity-link">SkeletonBlockModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SkeletonBlockModule-9b05c01b21277e47724035d65ae454cb"' : 'data-target="#xs-components-links-module-SkeletonBlockModule-9b05c01b21277e47724035d65ae454cb"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SkeletonBlockModule-9b05c01b21277e47724035d65ae454cb"' :
                                            'id="xs-components-links-module-SkeletonBlockModule-9b05c01b21277e47724035d65ae454cb"' }>
                                            <li class="link">
                                                <a href="components/SkeletonBlockComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SkeletonBlockComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/SkeletonHeaderModule.html" data-type="entity-link">SkeletonHeaderModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SkeletonHeaderModule-29e508dea93a61f9b6a6f2409c7154e7"' : 'data-target="#xs-components-links-module-SkeletonHeaderModule-29e508dea93a61f9b6a6f2409c7154e7"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SkeletonHeaderModule-29e508dea93a61f9b6a6f2409c7154e7"' :
                                            'id="xs-components-links-module-SkeletonHeaderModule-29e508dea93a61f9b6a6f2409c7154e7"' }>
                                            <li class="link">
                                                <a href="components/SkeletonHeaderComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SkeletonHeaderComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/SkeletonParagraphModule.html" data-type="entity-link">SkeletonParagraphModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SkeletonParagraphModule-01232c4c611b85042ae0e408edfe5db0"' : 'data-target="#xs-components-links-module-SkeletonParagraphModule-01232c4c611b85042ae0e408edfe5db0"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SkeletonParagraphModule-01232c4c611b85042ae0e408edfe5db0"' :
                                            'id="xs-components-links-module-SkeletonParagraphModule-01232c4c611b85042ae0e408edfe5db0"' }>
                                            <li class="link">
                                                <a href="components/SkeletonParagraphComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SkeletonParagraphComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/StateSelectModule.html" data-type="entity-link">StateSelectModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-StateSelectModule-149d74a9218c77c615789ff38ca2866d"' : 'data-target="#xs-components-links-module-StateSelectModule-149d74a9218c77c615789ff38ca2866d"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-StateSelectModule-149d74a9218c77c615789ff38ca2866d"' :
                                            'id="xs-components-links-module-StateSelectModule-149d74a9218c77c615789ff38ca2866d"' }>
                                            <li class="link">
                                                <a href="components/StateSelectComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">StateSelectComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-StateSelectModule-149d74a9218c77c615789ff38ca2866d"' : 'data-target="#xs-injectables-links-module-StateSelectModule-149d74a9218c77c615789ff38ca2866d"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-StateSelectModule-149d74a9218c77c615789ff38ca2866d"' :
                                        'id="xs-injectables-links-module-StateSelectModule-149d74a9218c77c615789ff38ca2866d"' }>
                                        <li class="link">
                                            <a href="injectables/StateSelectProvider.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>StateSelectProvider</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/VersionPageModule.html" data-type="entity-link">VersionPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-VersionPageModule-8aeb3de805660666e618cb918e2105b1"' : 'data-target="#xs-components-links-module-VersionPageModule-8aeb3de805660666e618cb918e2105b1"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-VersionPageModule-8aeb3de805660666e618cb918e2105b1"' :
                                            'id="xs-components-links-module-VersionPageModule-8aeb3de805660666e618cb918e2105b1"' }>
                                            <li class="link">
                                                <a href="components/VersionPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">VersionPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                </ul>
                </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/BaseSelectProvider.html" data-type="entity-link">BaseSelectProvider</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/SentryErrorHandler.html" data-type="entity-link">SentryErrorHandler</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <a data-type="chapter-link" href="routes.html"><span class="icon ion-ios-git-branch"></span>Routes</a>
                        </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});