import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo(pat) {
    return browser.get('/' + pat);
  }

  getTitleText(id) {
    element(by.id('ion-title[id=title] title'));
  }

  locateFieldAndEnterValue(id, text) {
    const locatedField =  element(by.css('ion-input[name=' + id + '] input'));

      locatedField.getLocation()
          .then(function(location) {
              return browser.executeScript('window.scrollTo(' + location.x + ', ' + location.y + ');');
          });

      return locatedField.sendKeys(text);
  }

  // locateFieldAndEnterValue(id, text) {
  //     const scrolldown = element(by.css('ion-input[name=' + id + '] input'));
  //     browser.controlFlow().execute(function() {
  //         browser.executeScript('arguments[0].scrollIntoView(true)', scrolldown.getWebElement());
  //     });
  //     return scrolldown.sendKeys(text);
  // }

  locateAndClick(id) {
    const locatedField =  element(by.id(id));

    return locatedField.click();
  }

}
