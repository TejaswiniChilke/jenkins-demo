import { Before, Given, Then, When } from 'cucumber';
import { expect } from 'chai';

import { AppPage } from '../pages/app.po';
import {browser} from 'protractor';

let page: AppPage;

Before(() => {
  page = new AppPage();
  browser.driver.manage().window().maximize();
  browser.manage().timeouts().setScriptTimeout(50000);
});

Given(/^I am on the login page$/, async () => {
  await page.navigateTo('#/login');
});

When(/^I do enter email$/, {timeout: 5 * 5000}, async () => {
  await page.locateFieldAndEnterValue('username', 'venkat');
});

When(/^I do enter password$/, async () => {
  await page.locateFieldAndEnterValue('password', 'admin');
});

When(/^I click login button$/, async () => {
  await page.locateAndClick('login-button');
});

Then(/^I should see the home page title$/, {timeout: 5 * 5000}, async () => {
  expect(await page.getTitleText('pageTitle')).to.equal('Home');
});
